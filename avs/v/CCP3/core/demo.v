
flibrary Demo<NEeditable=1,compile_subs=0> {
  macro demo {
    CCP3.Core_Modules.Demo.DLV_demo DLV_demo<NEx=220.,NEy=154.>
    {
      month => <-.month;
      year => <-.year;
      expired = 0;
      warning = 0;
    };
    UIwarningDialog UIwarningDialog<NEx=363.,NEy=264.> {
#ifdef MSDOS
      visible = 0;
#else
      visible => <-.DLV_demo.warning;
#endif // MSDOS
      message => <-.wstring;
      title => "DLV demo expires soon";
      cancelButton = 0;
      okButton = 1;
      ok = 0;
      isModal = 1;
    };
    string wstring<NEportLevels=1,NEx=495.,NEy=154.> =>
      ("This demo copy of DLV expires at the end of this month.\n"
       + "If you wish to continue using DLV, please use the update\n"
       + "form on the web site to request a copy of the full release.");
    int month<NEportLevels=1,NEx=385.,NEy=44.> = 7;
    int year<NEportLevels=1,NEx=594.,NEy=44.> = 2014;
    string estring<NEportLevels=1> =>
      ("This demo copy of DLV has expired.\n"
       + "If you wish to continue using DLV, please use the update\n"
       + "form on the web site to request a copy of the full release.");
    UIerrorDialog UIerrorDialog<NEx=198.,NEy=319.> {
#ifdef MSDOS
      visible = 0;
#else
      visible => <-.copy_on_change.output;
#endif // MSDOS
      message => <-.estring;
      title => "DLV demo has expired";
      cancelButton = 0;
      okButton = 1;
      ok = 0;
      isModal = 1;
    };
    GMOD.exit_process exit_process<NEx=220.,NEy=407.> {
      do_exit => <-.error;
    };
    int error<NEportLevels=1,NEx=44.,NEy=396.> =>
      ((DLV_demo.expired == 1) && (UIerrorDialog.visible == 0));
#ifdef MSDOS
    GMOD.parse_v set_warning {
      on_inst = 0;
      relative => <-;
      v_commands = "UIwarningDialog.visible = DLV_demo.warning;";
      trigger => <-.DLV_demo.warning;
    };
    GMOD.parse_v set_expired {
      on_inst = 0;
      relative => <-;
      v_commands = "UIerrorDialog.visible = DLV_demo.expired;";
      trigger => <-.DLV_demo.expired;
    };
#else
    GMOD.copy_on_change copy_on_change<NEx=55.,NEy=264.> {
      input => <-.DLV_demo.expired;
    };
#endif // MSDOS
  };
};
