
flibrary Display<NEeditable=1,
#ifndef DLV_NO_DLLS
		 dyn_libs="libCCP3dlv",
#endif // DLV_NO_DLLS
		 export_cxx=1,
		 build_dir="avs/src/express",
		 cxx_hdr_files="fld/Xfld.h avs/src/express/misc.hxx",
		 out_hdr_file="display.hxx",
		 out_src_file="display.cxx"> {
  DataObjectNoTexture DataObjectNoTexture;
  GroupObject GroupObject;
};
