
flibrary Core_Macros {
  "../avs/v/CCP3/core/misc.v" Misc;
  "../avs/v/CCP3/core/display.v" Display;
  "../avs/v/CCP3/core/UI/macros.v" UI;
  "../avs/v/CCP3/core/core.v" Core;
  "../avs/v/CCP3/core/demo.v" Demo;
  "../avs/v/CCP3/core/calc_objs.v" CalcObjs;
  "../avs/v/CCP3/core/calculations.v" Calculations;
  "../avs/v/CCP3/core/project.v" Project;
};
