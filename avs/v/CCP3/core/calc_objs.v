
flibrary CalcObjs<NEeditable=1,
#ifndef DLV_NO_DLLS
		  dyn_libs="libCCP3dlv",
#endif // DLV_NO_DLLS
		  export_cxx=1,
		  build_dir="avs/src/express",
		  cxx_hdr_files="avs/src/express/ui_objs.hxx",
		  out_hdr_file="calcs.hxx",
		  out_src_file="calcs.cxx"> {
  macro calculation {
    UIcmdList+GMOD.hconnect UIcmdList<NEx=165.,NEy=99.> {
      direction = 1;
      offer = "calcCmds";
      offer_name = ".";
    };
    link preferences<NEportLevels=1,NEx=429.,NEy=99.>;
    link create<NEportLevels={2,1},NEx=693.,NEy=99.>;
    link active_menus<NEportLevels=1,NEx=165.,NEy=33.>;
    link UIparent<NEportLevels=1,NEx=429.,NEy=33.>;
    int dialog_visible<NEportLevels=1,NEx=693.,NEy=33.>;
    link model_type<NEportLevels=1,NEx=880.,NEy=33.>;
    link job_data<NEportLevels=1,NEx=880.,NEy=99.>;
  };
};
