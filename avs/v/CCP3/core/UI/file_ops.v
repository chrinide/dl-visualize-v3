
flibrary File_Ops<NEeditable=1,
		  export_cxx=1,
		  build_dir="avs/src/express/ui",
#ifdef DLV_NO_DLLS
		  dyn_libs="libCCP3dlv",
#endif // DLV_NO_DLLS
#ifdef MSDOS
		  cxx_hdr_files="nt_ui/ui_gen.h avs/src/express/ui_objs.hxx avs/src/core/model/make_gen.hxx avs/src/core/model/fin_gen.hxx avs/src/core/model/can_gen.hxx avs/src/core/model/load_gen.hxx avs/src/core/model/pdb_gen.hxx avs/src/core/model/def_gen.hxx avs/src/core/model/ptbl_gen.hxx avs/src/core/model/save_gen.hxx",
#else
		  cxx_hdr_files="motif_ui/ui/ui_core.h avs/src/express/ui_objs.hxx avs/src/core/model/make_gen.hxx avs/src/core/model/fin_gen.hxx avs/src/core/model/can_gen.hxx avs/src/core/model/load_gen.hxx avs/src/core/model/pdb_gen.hxx avs/src/core/model/def_gen.hxx avs/src/core/model/ptbl_gen.hxx avs/src/core/model/save_gen.hxx",
#endif // MSDOS
		  out_hdr_file="file_ops.hxx",
		  out_src_file="file_ops.cxx"> {
  macro periodic_table<NEx=539.,NEy=176.> {
    int size<NEportLevels=1,NEx=528.,NEy=55.> = 28;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=737.,NEy=55.>;
    mlink values<NEportLevels=1,NEx=528.,NEy=330.> => .UIbutton.do;
    link atom<NEportLevels=1,NEx=418.,NEy=165.>;
    UIpanel UIpanel<NEx=209.,NEy=121.> {
      parent<NEportLevels={3,0}>;
      width = (<-.size * 18);
      height = (<-.size * 10);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIbutton UIbutton<NEx=561.,NEy=231.>[] {
      parent => <-.UIpanel;
      width => <-.size;
      height => <-.size;
      &fontAttributes => <-.preferences.fontAttributes;
      color {
	foregroundColor => switch((index_of(<-.<-.UIbutton) == <-.<-.atom) + 1,
				  "black", "white");
	backgroundColor => <-.<-.preferences.colour.backgroundColor;
      };
      do = 0;
    } = {
      {
	x = 0,
	y = 0,
	label = "H"
      },
      {
	x => (.width * 17),
	y = 0,
	label = "He"
      },
      {
	x = 0,
	y => .height,
	label = "Li"
      },
      {
	x => .width,
	y => .height,
	label = "Be"
      },
      {
	x => (.width * 12),
	y => .height,
	label = "B"
      },
      {
	x => (.width * 13),
	y => .height,
	label = "C"
      },
      {
	x => (.width * 14),
	y => .height,
	label = "N"
      },
      {
	x => (.width * 15),
	y => .height,
	label = "O"
      },
      {
	x => (.width * 16),
	y => .height,
	label = "F"
      },
      {
	x => (.width * 17),
	y => .height,
	label = "Ne"
      },
      {
	x = 0,
	y => (.height * 2),
	label = "Na"
      },
      {
	x => .width,
	y => (.height * 2),
	label = "Mg"
      },
      {
	x => (.width * 12),
	y => (.height * 2),
	label = "Al"
      },
      {
	x => (.width * 13),
	y => (.height * 2),
	label = "Si"
      },
      {
	x => (.width * 14),
	y => (.height * 2),
	label = "P"
      },
      {
	x => (.width * 15),
	y => (.height * 2),
	label = "S"
      },
      {
	x => (.width * 16),
	y => (.height * 2),
	label = "Cl"
      },
      {
	x => (.width * 17),
	y => (.height * 2),
	label = "Ar"
      },
      {
	x = 0,
	y => (.height * 3),
	label = "K"
      },
      {
	x => .width,
	y => (.height * 3),
	label = "Ca"
      },
      {
	x => (.width * 2),
	y => (.height * 3),
	label = "Sc"
      },
      {
	x => (.width * 3),
	y => (.height * 3),
	label = "Ti"
      },
      {
	x => (.width * 4),
	y => (.height * 3),
	label = "V"
      },
      {
	x => (.width * 5),
	y => (.height * 3),
	label = "Cr"
      },
      {
	x => (.width * 6),
	y => (.height * 3),
	label = "Mn"
      },
      {
	x => (.width * 7),
	y => (.height * 3),
	label = "Fe"
      },
      {
	x => (.width * 8),
	y => (.height * 3),
	label = "Co"
      },
      {
	x => (.width * 9),
	y => (.height * 3),
	label = "Ni"
      },
      {
	x => (.width * 10),
	y => (.height * 3),
	label = "Cu"
      },
      {
	x => (.width * 11),
	y => (.height * 3),
	label = "Zn"
      },
      {
	x => (.width * 12),
	y => (.height * 3),
	label = "Ga"
      },
      {
	x => (.width * 13),
	y => (.height * 3),
	label = "Ge"
      },
      {
	x => (.width * 14),
	y => (.height * 3),
	label = "As"
      },
      {
	x => (.width * 15),
	y => (.height * 3),
	label = "Se"
      },
      {
	x => (.width * 16),
	y => (.height * 3),
	label = "Br"
      },
      {
	x => (.width * 17),
	y => (.height * 3),
	label = "Kr"
      },
      {
	x = 0,
	y => (.height * 4),
	label = "Rb"
      },
      {
	x => .width,
	y => (.height * 4),
	label = "Sr"
      },
      {
	x => (.width * 2),
	y => (.height * 4),
	label = "Y"
      },
      {
	x => (.width * 3),
	y => (.height * 4),
	label = "Zr"
      },
      {
	x => (.width * 4),
	y => (.height * 4),
	label = "Nb"
      },
      {
	x => (.width * 5),
	y => (.height * 4),
	label = "Mo"
      },
      {
	x => (.width * 6),
	y => (.height * 4),
	label = "Tc"
      },
      {
	x => (.width * 7),
	y => (.height * 4),
	label = "Ru"
      },
      {
	x => (.width * 8),
	y => (.height * 4),
	label = "Rh"
      },
      {
	x => (.width * 9),
	y => (.height * 4),
	label = "Pd"
      },
      {
	x => (.width * 10),
	y => (.height * 4),
	label = "Ag"
      },
      {
	x => (.width * 11),
	y => (.height * 4),
	label = "Cd"
      },
      {
	x => (.width * 12),
	y => (.height * 4),
	label = "In"
      },
      {
	x => (.width * 13),
	y => (.height * 4),
	label = "Sn"
      },
      {
	x => (.width * 14),
	y => (.height * 4),
	label = "Sb"
      },
      {
	x => (.width * 15),
	y => (.height * 4),
	label = "Te"
      },
      {
	x => (.width * 16),
	y => (.height * 4),
	label = "I"
      },
      {
	x => (.width * 17),
	y => (.height * 4),
	label = "Xe"
      },
      {
	x = 0,
	y => (.height * 5),
	label = "Cs"
      },
      {
	x => .width,
	y => (.height * 5),
	label = "Ba"
      },
      {
	x => (.width * 2),
	y => (.height * 5),
	label = "La"
      },
      {
	x => (.width * 3),
	y => (.height * 8),
	label = "Ce"
      },
      {
	x => (.width * 4),
	y => (.height * 8),
	label = "Pr"
      },
      {
	x => (.width * 5),
	y => (.height * 8),
	label = "Nd"
      },
      {
	x => (.width * 6),
	y => (.height * 8),
	label = "Pm"
      },
      {
	x => (.width * 7),
	y => (.height * 8),
	label = "Sm"
      },
      {
	x => (.width * 8),
	y => (.height * 8),
	label = "Eu"
      },
      {
	x => (.width * 9),
	y => (.height * 8),
	label = "Gd"
      },
      {
	x => (.width * 10),
	y => (.height * 8),
	label = "Tb"
      },
      {
	x => (.width * 11),
	y => (.height * 8),
	label = "Dy"
      },
      {
	x => (.width * 12),
	y => (.height * 8),
	label = "Ho"
      },
      {
	x => (.width * 13),
	y => (.height * 8),
	label = "Er"
      },
      {
	x => (.width * 14),
	y => (.height * 8),
	label = "Tm"
      },
      {
	x => (.width * 15),
	y => (.height * 8),
	label = "Yb"
      },
      {
	x => (.width * 16),
	y => (.height * 8),
	label = "Lu"
      },
      {
	x => (.width * 3),
	y => (.height * 5),
	label = "Hf"
      },
      {
	x => (.width * 4),
	y => (.height * 5),
	label = "Ta"
      },
      {
	x => (.width * 5),
	y => (.height * 5),
	label = "W"
      },
      {
	x => (.width * 6),
	y => (.height * 5),
	label = "Re"
      },
      {
	x => (.width * 7),
	y => (.height * 5),
	label = "Os"
      },
      {
	x => (.width * 8),
	y => (.height * 5),
	label = "Ir"
      },
      {
	x => (.width * 9),
	y => (.height * 5),
	label = "Pt"
      },
      {
	x => (.width * 10),
	y => (.height * 5),
	label = "Au"
      },
      {
	x => (.width * 11),
	y => (.height * 5),
	label = "Hg"
      },
      {
	x => (.width * 12),
	y => (.height * 5),
	label = "Tl"
      },
      {
	x => (.width * 13),
	y => (.height * 5),
	label = "Pb"
      },
      {
	x => (.width * 14),
	y => (.height * 5),
	label = "Bi"
      },
      {
	x => (.width * 15),
	y => (.height * 5),
	label = "Po"
      },
      {
	x => (.width * 16),
	y => (.height * 5),
	label = "At"
      },
      {
	x => (.width * 17),
	y => (.height * 5),
	label = "Rn"
      },
      {
	x = 0,
	y => (.height * 6),
	label = "Fr"
      },
      {
	x => .width,
	y => (.height * 6),
	label = "Ra"
      },
      {
	x => (.width * 2),
	y => (.height * 6),
	label = "Ac"
      },
      {
	x => (.width * 3),
	y => (.height * 9),
	label = "Th"
      },
      {
	x => (.width * 4),
	y => (.height * 9),
	label = "Pa"
      },
      {
	x => (.width * 5),
	y => (.height * 9),
	label = "U"
      },
      {
	x => (.width * 6),
	y => (.height * 9),
	label = "Np"
      },
      {
	x => (.width * 7),
	y => (.height * 9),
	label = "Pu"
      },
      {
	x => (.width * 8),
	y => (.height * 9),
	label = "Am"
      },
      {
	x => (.width * 9),
	y => (.height * 9),
	label = "Cm"
      },
      {
	x => (.width * 10),
	y => (.height * 9),
	label = "Bk"
      },
      {
	x => (.width * 11),
	y => (.height * 9),
	label = "Cf"
      },
      {
	x => (.width * 12),
	y => (.height * 9),
	label = "Es"
      },
      {
	x => (.width * 13),
	y => (.height * 9),
	label = "Fm"
      }
    };
    CCP3.Core_Modules.Model.periodic_table select<NEx=561.,NEy=451.> {
      array => <-.values;
      current => <-.atom;
    };
  };
  macro typeUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=165.,NEy=44.>;
    link UIparent<NEportLevels={2,1},NEx=715.,NEy=44.>;
    link model_choice<NEportLevels={2,1},NEx=660.,NEy=187.>;
    link panel_number<NEportLevels={2,1},NEx=429.,NEy=44.>;
    link name<NEportLevels=1,NEx=583.,NEy=319.>;
    UIpanel UIpanel<NEx=176.,NEy=154.> {
      visible => (<-.panel_number == 0);
      parent => <-.UIparent;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIlabel UIlabel<NEx=330.,NEy=429.> {
      parent => <-.UIpanel;
      x = 0;
      label = "Structure name";
      y = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=506.,NEy=429.> {
      parent => <-.UIpanel;
      x => (<-.UIlabel.width + 5);
      width => (parent.clientWidth - x);
      y = 0;
      text => <-.name;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    string models<NEportLevels=1,NEx=55.,NEy=209.>[] = {
      "Molecule","Slab","Crystal"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox model_choices<NEx=330.,NEy=253.> {
      parent => <-.UIpanel;
      labels => <-.models;
      selectedItem => <-.model_choice;
      title => "Structure type";
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      y => (<-.UIlabel.y + <-.UIlabel.height + 5);
      width => parent.clientWidth;
      UIradioBox {
	orientation = "horizontal";
	height => UIdata.UIfonts[0].lineHeight + 5;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=330.,NEy=352.> {
      parent => <-.UIpanel;
      label = "Open in new viewer";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => (<-.model_choices.y + <-.model_choices.height + 5);
      width => parent.clientWidth;
      //set => ;
    };
  };
  // Todo - don't need all this, but don't delete until requirements are clear
  group spacegroup {
    group lattice[] {
      string label;
      group centres[] {
	string label;
	group groupdata[] {
	  int index;
	  string setting;
	  int norigins;
	  string label;
	  string hall;
	};
      };
    };
  };
  spacegroup groups[] = {
    {
      lattice = {
	{
	  label = "Triclinic",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index =  1,
		  setting = "",
		  norigins = 1,
		  label = "C1",
		  hall = ""
		},
		{
		  index =  2,
		  setting = "",
		  norigins = 1,
		  label = "Ci",
		  hall = ""
		},
		{
		  index =  3,
		  setting = "",
		  norigins = 1,
		  label = "Cs",
		  hall = ""
		},
		{
		  index =  4,
		  setting = "",
		  norigins = 1,
		  label = "C2",
		  hall = ""
		},
		{
		  index =  5,
		  setting = "",
		  norigins = 1,
		  label = "C2v",
		  hall = ""
		},
		{
		  index =  6,
		  setting = "",
		  norigins = 1,
		  label = "C2h",
		  hall = ""
		},
		{
		  index =  7,
		  setting = "",
		  norigins = 1,
		  label = "D2",
		  hall = ""
		},
		{
		  index =  8,
		  setting = "",
		  norigins = 1,
		  label = "D2d",
		  hall = ""
		},
		{
		  index =  9,
		  setting = "",
		  norigins = 1,
		  label = "D2h",
		  hall = ""
		},
		{
		  index = 10,
		  setting = "",
		  norigins = 1,
		  label = "C3",
		  hall = ""
		},
		{
		  index = 11,
		  setting = "",
		  norigins = 1,
		  label = "C3v",
		  hall = ""
		},
		{
		  index = 12,
		  setting = "",
		  norigins = 1,
		  label = "C3h",
		  hall = ""
		},
		{
		  index = 13,
		  setting = "",
		  norigins = 1,
		  label = "C3i",
		  hall = ""
		},
		{
		  index = 14,
		  setting = "",
		  norigins = 1,
		  label = "D3",
		  hall = ""
		},
		{
		  index = 15,
		  setting = "",
		  norigins = 1,
		  label = "D3d",
		  hall = ""
		},
		{
		  index = 16,
		  setting = "",
		  norigins = 1,
		  label = "D3h",
		  hall = ""
		},
		{
		  index = 17,
		  setting = "",
		  norigins = 1,
		  label = "C4",
		  hall = ""
		},
		{
		  index = 18,
		  setting = "",
		  norigins = 1,
		  label = "C4v",
		  hall = ""
		},
		{
		  index = 20,
		  setting = "",
		  norigins = 1,
		  label = "C4h",
		  hall = ""
		},
		{
		  index = 21,
		  setting = "",
		  norigins = 1,
		  label = "S4",
		  hall = ""
		},
		{
		  index = 22,
		  setting = "",
		  norigins = 1,
		  label = "D4",
		  hall = ""
		},
		{
		  index = 23,
		  setting = "",
		  norigins = 1,
		  label = "D4d",
		  hall = ""
		},
		{
		  index = 24,
		  setting = "",
		  norigins = 1,
		  label = "D4h",
		  hall = ""
		},
		{
		  index = 25,
		  setting = "",
		  norigins = 1,
		  label = "C5",
		  hall = ""
		},
		{
		  index = 26,
		  setting = "",
		  norigins = 1,
		  label = "C5v",
		  hall = ""
		},
		{
		  index = 27,
		  setting = "",
		  norigins = 1,
		  label = "C5h",
		  hall = ""
		},
		{
		  index = 28,
		  setting = "",
		  norigins = 1,
		  label = "C5i",
		  hall = ""
		},
		{
		  index = 29,
		  setting = "",
		  norigins = 1,
		  label = "D5",
		  hall = ""
		},
		{
		  index = 30,
		  setting = "",
		  norigins = 1,
		  label = "D5d",
		  hall = ""
		},
		{
		  index = 31,
		  setting = "",
		  norigins = 1,
		  label = "D5h",
		  hall = ""
		},
		{
		  index = 32,
		  setting = "",
		  norigins = 1,
		  label = "C6",
		  hall = ""
		},
		{
		  index = 33,
		  setting = "",
		  norigins = 1,
		  label = "C6v",
		  hall = ""
		},
		{
		  index = 34,
		  setting = "",
		  norigins = 1,
		  label = "C6h",
		  hall = ""
		},
		{
		  index = 35,
		  setting = "",
		  norigins = 1,
		  label = "D6",
		  hall = ""
		},
		{
		  index = 36,
		  setting = "",
		  norigins = 1,
		  label = "D6d",
		  hall = ""
		},
		{
		  index = 37,
		  setting = "",
		  norigins = 1,
		  label = "D6h",
		  hall = ""
		},
		{
		  index = 38,
		  setting = "",
		  norigins = 1,
		  label = "T",
		  hall = ""
		},
		{
		  index = 39,
		  setting = "",
		  norigins = 1,
		  label = "Td",
		  hall = ""
		},
		{
		  index = 40,
		  setting = "",
		  norigins = 1,
		  label = "Th",
		  hall = ""
		},
		{
		  index = 41,
		  setting = "",
		  norigins = 1,
		  label = "O",
		  hall = ""
		},
		{
		  index = 42,
		  setting = "",
		  norigins = 1,
		  label = "Oh",
		  hall = ""
		},
		{
		  index = 43,
		  setting = "",
		  norigins = 1,
		  label = "I",
		  hall = ""
		},
		{
		  index = 44,
		  setting = "",
		  norigins = 1,
		  label = "Ih",
		  hall = ""
		}
	      }
	    }
	  }
	}
      }
    },
    {
      lattice = {
	{
	  label = "Triclinic",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index = 1,
		  setting = "",
		  norigins = 1,
		  label = "P1",
		  hall = ""
		}
	      }
	    }
	  }
	}
      }
    },
    {
      lattice = {
	{
	  label = "Oblique",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index =   1,
		  setting= "0",
		  norigins = 1,
		  label = "P 1",
		  hall = " P 1\0"
		},
		{
		  index =   2,
		  setting= "0",
		  norigins = 1,
		  label = "P -1",
		  hall = "-P 1\0"
		},
		{
		  index =   3,
		  setting = "qf::c",
		  norigins = 1,
		  label = "P 1 1 2",
		  hall = " P 2\0"
		},
		{
		  index =   6,
		  setting = "qf::c",
		  norigins = 1,
		  label = "P 1 1 m",
		  hall = " P -2\0"
		},
		{ // Todo? - it has other c settings
		  index =   7,
		  setting = "qf::c1",
		  norigins = 1,
		  label = "P 1 1 a",
		  hall = " P -2a\0"
		},
		{
		  index =  10,
		  setting = "qf::c",
		  norigins = 1,
		  label = "P 1 1 2/m",
		  hall = "-P 2\0"
		},
		{ // Todo? - it has other c settings
		  index =  13,
		  setting = "qf::c1",
		  norigins = 1,
		  label = "P 1 1 2/a",
		  hall = "-P 2a\0"
		}
	      }
	    }
	  }
	},
	{
	  label = "Rectangular",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index =   3,
		  setting = "qf::a",
		  norigins = 1,
		  label = "P 2 1 1",
		  hall = " P 2x\0"
		},
		{
		  index =   4,
		  setting = "qf::a",
		  norigins = 1,
		  label = "P 21 1 1",
		  hall = " P 2xa\0"
		},
		{
		  index =   6,
		  setting = "qf::a",
		  norigins = 1,
		  label = "P m 1 1",
		  hall = " P -2x\0"
		},
		{ // Todo? - other settings
		  index =   7,
		  setting = "qf::a1",
		  norigins = 1,
		  label = "P b 1 1",
		  hall = " P -2xb\0"
		},
		{
		  index =  10,
		  setting = "qf::a",
		  norigins = 1,
		  label = "P 2/m 1 1",
		  hall = "-P 2x\0"
		},
		{
		  index =  11,
		  setting = "qf::a",
		  norigins = 1,
		  label = "P 21/m 1 1",
		  hall = "-P 2xa\0"
		},
		{ // Todo? - other settings
		  index =  13,
		  setting = "qf::a1",
		  norigins = 1,
		  label = "P 2/b 1 1",
		  hall = "-P 2xb\0"
		},
		{ // Todo? - other settings
		  index =  14,
		  setting = "qf::a1",
		  norigins = 1,
		  label = "P 21/b 1 1",
		  hall = "-P 2xab\0"
		},
		{
		  index =  16,
		  setting= "0",
		  norigins = 1,
		  label = "P 2 2 2",
		  hall = " P 2 2\0"
		},
		{ // Todo - is this the correct setting?
		  index =  17,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 21 2 2",
		  hall = " P 2a 2a\0"
		},
		{
		  index =  18,
		  setting= "0",
		  norigins = 1,
		  label = "P 21 21 2",
		  hall = " P 2 2ab\0"
		},
		{
		  index =  25,
		  setting= "0",
		  norigins = 1,
		  label = "P m m 2",
		  hall = " P 2 -2\0"
		},
		{
		  index =  28,
		  setting= "0",
		  norigins = 1,
		  label = "P m a 2",
		  hall = " P 2 -2a\0"
		},
		{
		  index =  32,
		  setting= "0",
		  norigins = 1,
		  label = "P b a 2",
		  hall = " P 2 -2ab\0"
		},
		{
		  index =  25,
		  setting = "qf::cab",
		  label = "P 2 m m",
		  hall = " P -2 2\0"
		},
		{
		  index =  26,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P 21 a m",
		  hall = " P -2 2a\0"
		},
		{
		  index =  26,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P 21 a m",
		  hall = " P -2 2a\0"
		},
		{
		  index =  28,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 2 m b",
		  hall = " P -2b 2\0"
		},
		{
		  index =  31,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 21 m n",
		  hall = " P -2ab 2ab\0"
		},
		{
		  index =  27,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 2 a a",
		  hall = " P -2a 2\0"
		},
		{
		  index =  29,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 21 a b",
		  hall = " P -2b 2a\0"
		},
		{
		  index =  30,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P 2 a n",
		  hall = " P -2ab 2\0"
		},
		{
		  index =  47,
		  setting= "0",
		  norigins = 1,
		  label = "P m m m",
		  hall = "-P 2 2\0"
		},
		{
		  index =  51,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P m a m",
		  hall = "-P 2 2a\0"
		},
		{
		  index =  51,
		  setting= "0",
		  norigins = 1,
		  label = "P m m a",
		  hall = "-P 2a 2a\0"
		},
		{
		  index =  59,
		  setting= "0",
		  norigins = 1,
		  label = "P m m n",
		  hall = " P 2 2ab -1ab\0-P 2ab 2a\0"
		},
		{
		  index =  55,
		  setting= "0",
		  norigins = 1,
		  label = "P b a m",
		  hall = "-P 2 2ab\0"
		},
		{
		  index =  49,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P m a a",
		  hall = "-P 2a 2\0"
		},
		{
		  index =  53,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P m a n",
		  hall = "-P 2ab 2\0"
		},
		{
		  index =  57,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P b m a",
		  hall = "-P 2a 2ab\0"
		},
		{
		  index =  54,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P b a a",
		  hall = "-P 2a 2b\0"
		},
		{
		  index =  50,
		  setting= "0",
		  norigins = 1,
		  label = "P b a n",
		  hall = " P 2 2 -1ab\0-P 2ab 2b\0"
		}
	      }
	    },
	    {
	      label = "C",
	      groupdata = {
		{
		  index =   5,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "C 2 1 1",
		  hall = " C 2x\0"
		},
		{
		  index =   8,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "C m 1 1",
		  hall = " C -2x\0"
		},
		{
		  index =  12,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "C 2/m 1 1",
		  hall = "-C 2x\0"
		},
		{
		  index =  21,
		  setting= "0",
		  norigins = 1,
		  label = "C 2 2 2",
		  hall = " C 2 2\0"
		},
		{
		  index =  35,
		  setting= "0",
		  norigins = 1,
		  label = "C m m 2",
		  hall = " C 2 -2\0"
		},
		{
		  index =  38,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "C 2 m m",
		  hall = " C -2 2\0"
		},
		{
		  index =  39,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "C 2 m b",
		  hall = " C -2a 2\0"
		},
		{
		  index =  65,
		  setting= "0",
		  norigins = 1,
		  label = "C m m m",
		  hall = "-C 2 2\0"
		},
		{
		  index =  67,
		  setting= "0",
		  norigins = 1,
		  label = "C m m a",
		  hall = "-C 2a 2\0"
		}
	      }
	    }
	  }
	},
	{
	  label = "Triangular",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index = 143,
		  setting= "0",
		  norigins = 1,
		  label = "P 3",
		  hall = " P 3\0"
		},
		{
		  index = 147,
		  setting= "0",
		  norigins = 1,
		  label = "P -3",
		  hall = "-P 3\0"
		},
		{
		  index = 149,
		  setting= "0",
		  norigins = 1,
		  label = "P 3 1 2",
		  hall = " P 3 2\0"
		},
		{
		  index = 150,
		  setting= "0",
		  norigins = 1,
		  label = "P 3 2 1",
		  hall = " P 3 2\"\0"
		},
		{
		  index = 156,
		  setting= "0",
		  norigins = 1,
		  label = "P 3 m 1",
		  hall = " P 3 -2\"\0"
		},
		{
		  index = 157,
		  setting= "0",
		  norigins = 1,
		  label = "P 3 1 m",
		  hall = " P 3 -2\0"
		},
		{
		  index = 162,
		  setting= "0",
		  norigins = 1,
		  label = "P -3 1 m",
		  hall = "-P 3 2\0"
		},
		{
		  index = 164,
		  setting= "0",
		  norigins = 1,
		  label = "P -3 m 1",
		  hall = "-P 3 2\"\0"
		}
	      }
	    }
	  }
	},
	{
	  label = "Square",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index =  75,
		  setting= "0",
		  norigins = 1,
		  label = "P 4",
		  hall = " P 4\0"
		},
		{
		  index =  81,
		  setting= "0",
		  norigins = 1,
		  label = "P -4",
		  hall = " P -4\0"
		},
		{
		  index =  83,
		  setting= "0",
		  norigins = 1,
		  label = "P 4/m",
		  hall = "-P 4\0"
		},
		{
		  index =  85,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 4/n",
		  hall = " P 4ab -1ab\0-P 4a\0"
		},
		{
		  index =  89,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 2 2",
		  hall = " P 4 2\0"
		},
		{
		  index =  90,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 21 2",
		  hall = " P 4ab 2ab\0"
		},
		{
		  index =  99,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 m m",
		  hall = " P 4 -2\0"
		},
		{
		  index = 100,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 b m",
		  hall = " P 4 -2ab\0"
		},
		{
		  index = 111,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 2 m",
		  hall = " P -4 2\0"
		},
		{
		  index = 113,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 21 m",
		  hall = " P -4 2ab\0"
		},
		{
		  index = 115,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 m 2",
		  hall = " P -4 -2\0"
		},
		{
		  index = 117,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 b 2",
		  hall = " P -4 -2ab\0"
		},
		{
		  index = 123,
		  setting= "0",
		  norigins = 1,
		  label = "P 4/m m m",
		  hall = "-P 4 2\0"
		},
		{
		  index = 125,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 4/n b m",
		  hall = " P 4 2 -1ab\0-P 4a 2b\0"
		},
		{
		  index = 127,
		  setting= "0",
		  norigins = 1,
		  label = "P 4/m b m",
		  hall = "-P 4 2ab\0"
		},
		{
		  index = 129,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 4/n m m",
		  hall = " P 4ab 2ab -1ab\0-P 4a 2a\0"
		}
	      }
	    }
	  }
	},
	{
	  label = "Hexagonal",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index = 168,
		  setting= "0",
		  norigins = 1,
		  label = "P 6",
		  hall = " P 6\0"
		},
		{
		  index = 174,
		  setting= "0",
		  norigins = 1,
		  label = "P -6",
		  hall = " P -6\0"
		},
		{
		  index = 175,
		  setting= "0",
		  norigins = 1,
		  label = "P 6/m",
		  hall = "-P 6\0"
		},
		{
		  index = 177,
		  setting= "0",
		  norigins = 1,
		  label = "P 6 2 2",
		  hall = " P 6 2\0"
		},
		{
		  index = 183,
		  setting= "0",
		  norigins = 1,
		  label = "P 6 m m",
		  hall = " P 6 -2\0"
		},
		{
		  index = 187,
		  setting= "0",
		  norigins = 1,
		  label = "P -6 m 2",
		  hall = " P -6 2\0"
		},
		{
		  index = 189,
		  setting= "0",
		  norigins = 1,
		  label = "P -6 2 m",
		  hall = " P -6 -2\0"
		},
		{
		  index = 191,
		  setting= "0",
		  norigins = 1,
		  label = "P 6/m m m",
		  hall = "-P 6 2\0"
		}
	      }
	    }
	  }
	}
      }
    },
    {
      lattice = {
	{
	  // see cctbx::sgtbx::symbol.cpp (reference/hall for settings)
	  label = "Triclinic",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index =   1,
		  setting= "0",
		  norigins = 1,
		  label = "P 1",
		  hall = " P 1\0"
		},
		{
		  index =   2,
		  setting= "0",
		  norigins = 1,
		  label = "P -1",
		  hall = "-P 1\0"
		}
	      }
	    }
	  }
	},
	{
	  label = "Monoclinic",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index =   3,
		  setting = "qf::b",
		  norigins = 1,
		  label = "P 1 2 1",
		  hall = " P 2y\0"
		},
		{
		  index =   3,
		  setting = "qf::c",
		  norigins = 1,
		  label = "P 1 1 2",
		  hall = " P 2\0"
		},
		{
		  index =   3,
		  setting = "qf::a",
		  norigins = 1,
		  label = "P 2 1 1",
		  hall = " P 2x\0"
		},
		{
		  index =   4,
		  setting = "qf::b",
		  norigins = 1,
		  label = "P 1 21 1",
		  hall = " P 2yb\0"
		},
		{
		  index =   4,
		  setting = "qf::c",
		  norigins = 1,
		  label = "P 1 1 21",
		  hall = " P 2c\0"
		},
		{
		  index =   4,
		  setting = "qf::a",
		  norigins = 1,
		  label = "P 21 1 1",
		  hall = " P 2xa\0"
		},
		{
		  index =   6,
		  setting = "qf::b",
		  norigins = 1,
		  label = "P 1 m 1",
		  hall = " P -2y\0"
		},
		{
		  index =   6,
		  setting = "qf::c",
		  norigins = 1,
		  label = "P 1 1 m",
		  hall = " P -2\0"
		},
		{
		  index =   6,
		  setting = "qf::a",
		  norigins = 1,
		  label = "P m 1 1",
		  hall = " P -2x\0"
		},
		{
		  index =   7,
		  setting = "qf::b1",
		  norigins = 1,
		  label = "P 1 c 1",
		  hall = " P -2yc\0"
		},
		{
		  index =   7,
		  setting = "qf::b2",
		  norigins = 1,
		  label = "P 1 n 1",
		  hall = " P -2yac\0"
		},
		{
		  index =   7,
		  setting = "qf::b3",
		  norigins = 1,
		  label = "P 1 a 1",
		  hall = " P -2ya\0"
		},
		{
		  index =   7,
		  setting = "qf::c1",
		  norigins = 1,
		  label = "P 1 1 a",
		  hall = " P -2a\0"
		},
		{
		  index =   7,
		  setting = "qf::c2",
		  norigins = 1,
		  label = "P 1 1 n",
		  hall = " P -2ab\0"
		},
		{
		  index =   7,
		  setting = "qf::c3",
		  norigins = 1,
		  label = "P 1 1 b",
		  hall = " P -2b\0"
		},
		{
		  index =   7,
		  setting = "qf::a1",
		  label = "P b 1 1",
		  norigins = 1,
		  hall = " P -2xb\0"
		},
		{
		  index =   7,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "P n 1 1",
		  hall = " P -2xbc\0"
		},
		{
		  index =   7,
		  setting = "qf::a3",
		  norigins = 1,
		  label = "P c 1 1",
		  hall = " P -2xc\0"
		},
		{
		  index =  10,
		  setting = "qf::b",
		  norigins = 1,
		  label = "P 1 2/m 1",
		  hall = "-P 2y\0"
		},
		{
		  index =  10,
		  setting = "qf::c",
		  norigins = 1,
		  label = "P 1 1 2/m",
		  hall = "-P 2\0"
		},
		{
		  index =  10,
		  setting = "qf::a",
		  norigins = 1,
		  label = "P 2/m 1 1",
		  hall = "-P 2x\0"
		},
		{
		  index =  11,
		  setting = "qf::b",
		  norigins = 1,
		  label = "P 1 21/m 1",
		  hall = "-P 2yb\0"
		},
		{
		  index =  11,
		  setting = "qf::c",
		  norigins = 1,
		  label = "P 1 1 21/m",
		  hall = "-P 2c\0"
		},
		{
		  index =  11,
		  setting = "qf::a",
		  norigins = 1,
		  label = "P 21/m 1 1",
		  hall = "-P 2xa\0"
		},
		{
		  index =  13,
		  setting = "qf::b1",
		  norigins = 1,
		  label = "P 1 2/c 1",
		  hall = "-P 2yc\0"
		},
		{
		  index =  13,
		  setting = "qf::b2",
		  norigins = 1,
		  label = "P 1 2/n 1",
		  hall = "-P 2yac\0"
		},
		{
		  index =  13,
		  setting = "qf::b3",
		  norigins = 1,
		  label = "P 1 2/a 1",
		  hall = "-P 2ya\0"
		},
		{
		  index =  13,
		  setting = "qf::c1",
		  norigins = 1,
		  label = "P 1 1 2/a",
		  hall = "-P 2a\0"
		},
		{
		  index =  13,
		  setting = "qf::c2",
		  norigins = 1,
		  label = "P 1 1 2/n",
		  hall = "-P 2ab\0"
		},
		{
		  index =  13,
		  setting = "qf::c3",
		  norigins = 1,
		  label = "P 1 1 2/b",
		  hall = "-P 2b\0"
		},
		{
		  index =  13,
		  setting = "qf::a1",
		  norigins = 1,
		  label = "P 2/b 1 1",
		  hall = "-P 2xb\0"
		},
		{
		  index =  13,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "P 2/n 1 1",
		  hall = "-P 2xbc\0"
		},
		{
		  index =  13,
		  setting = "qf::a3",
		  norigins = 1,
		  label = "P 2/c 1 1",
		  hall = "-P 2xc\0"
		},
		{
		  index =  14,
		  setting = "qf::b1",
		  norigins = 1,
		  label = "P 1 21/c 1",
		  hall = "-P 2ybc\0"
		},
		{
		  index =  14,
		  setting = "qf::b2",
		  norigins = 1,
		  label = "P 1 21/n 1",
		  hall = "-P 2yn\0"
		},
		{
		  index =  14,
		  setting = "qf::b3",
		  norigins = 1,
		  label = "P 1 21/a 1",
		  hall = "-P 2yab\0"
		},
		{
		  index =  14,
		  setting = "qf::c1",
		  norigins = 1,
		  label = "P 1 1 21/a",
		  hall = "-P 2ac\0"
		},
		{
		  index =  14,
		  setting = "qf::c2",
		  norigins = 1,
		  label = "P 1 1 21/n",
		  hall = "-P 2n\0"
		},
		{
		  index =  14,
		  setting = "qf::c3",
		  norigins = 1,
		  label = "P 1 1 21/b",
		  hall = "-P 2bc\0"
		},
		{
		  index =  14,
		  setting = "qf::a1",
		  norigins = 1,
		  label = "P 21/b 1 1",
		  hall = "-P 2xab\0"
		},
		{
		  index =  14,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "P 21/n 1 1",
		  hall = "-P 2xn\0"
		},
		{
		  index =  14,
		  setting = "qf::a3",
		  norigins = 1,
		  label = "P 21/c 1 1",
		  hall = "-P 2xac\0"
		}
	      }
	    },
	    {
	      label = "A",
	      groupdata = {
		{
		  index =   5,
		  setting = "qf::b2",
		  norigins = 1,
		  label = "A 1 2 1",
		  hall = " A 2y\0"
		},
		{
		  index =   5,
		  setting = "qf::c1",
		  norigins = 1,
		  label = "A 1 1 2",
		  hall = " A 2\0"
		},
		{
		  index =   8,
		  setting = "qf::b2",
		  norigins = 1,
		  label = "A 1 m 1",
		  hall = " A -2y\0"
		},
		{
		  index =   8,
		  setting = "qf::c1",
		  norigins = 1,
		  label = "A 1 1 m",
		  hall = " A -2\0"
		},
		{
		  index =   9,
		  setting = "qf::b2",
		  norigins = 1,
		  label = "A 1 n 1",
		  hall = " A -2yab\0"
		},
		{
		  index =   9,
		  setting = "qf::mb1",
		  norigins = 1,
		  label = "A 1 a 1",
		  hall = " A -2ya\0"
		},
		{
		  index =   9,
		  setting = "qf::c1",
		  norigins = 1,
		  label = "A 1 1 a",
		  hall = " A -2a\0"
		},
		{
		  index =   9,
		  setting = "qf::mc2",
		  norigins = 1,
		  label = "A 1 1 n",
		  hall = " A -2ab\0"
		},
		{
		  index =  12,
		  setting = "qf::b2",
		  norigins = 1,
		  label = "A 1 2/m 1",
		  hall = "-A 2y\0"
		},
		{
		  index =  12,
		  setting = "qf::c1",
		  norigins = 1,
		  label = "A 1 1 2/m",
		  hall = "-A 2\0"
		},
		{
		  index =  15,
		  setting = "qf::b2",
		  norigins = 1,
		  label = "A 1 2/n 1",
		  hall = "-A 2yab\0"
		},
		{
		  index =  15,
		  setting = "qf::mb1",
		  norigins = 1,
		  label = "A 1 2/a 1",
		  hall = "-A 2ya\0"
		},
		{
		  index =  15,
		  setting = "qf::c1",
		  norigins = 1,
		  label = "A 1 1 2/a",
		  hall = "-A 2a\0"
		},
		{
		  index =  15,
		  setting = "qf::mc2",
		  norigins = 1,
		  label = "A 1 1 2/n",
		  hall = "-A 2ab\0"
		}
	      }
	    },
	    {
	      label = "B",
	      groupdata = {
		{
		  index =   5,
		  setting = "qf::c2",
		  norigins = 1,
		  label = "B 1 1 2",
		  hall = " B 2\0"
		},
		{
		  index =   5,
		  setting = "qf::a1",
		  norigins = 1,
		  label = "B 2 1 1",
		  hall = " B 2x\0"
		},
		{
		  index =   8,
		  setting = "qf::c2",
		  norigins = 1,
		  label = "B 1 1 m",
		  hall = " B -2\0"
		},
		{
		  index =   8,
		  setting = "qf::a1",
		  norigins = 1,
		  label = "B m 1 1",
		  hall = " B -2x\0"
		},
		{
		  index =   9,
		  setting = "qf::c2",
		  norigins = 1,
		  label = "B 1 1 n",
		  hall = " B -2ab\0"
		},
		{
		  index =   9,
		  setting = "qf::mc1",
		  norigins = 1,
		  label = "B 1 1 b",
		  hall = " B -2b\0"
		},
		{
		  index =   9,
		  setting = "qf::a1",
		  norigins = 1,
		  label = "B b 1 1",
		  hall = " B -2xb\0"
		},
		{
		  index =   9,
		  setting = "qf::ma2",
		  norigins = 1,
		  label = "B n 1 1",
		  hall = " B -2xab\0"
		},
		{
		  index =  12,
		  setting = "qf::c2",
		  norigins = 1,
		  label = "B 1 1 2/m",
		  hall = "-B 2\0"
		},
		{
		  index =  12,
		  setting = "qf::a1",
		  norigins = 1,
		  label = "B 2/m 1 1",
		  hall = "-B 2x\0"
		},
		{
		  index =  15,
		  setting = "qf::c2",
		  norigins = 1,
		  label = "B 1 1 2/n",
		  hall = "-B 2ab\0"
		},
		{
		  index =  15,
		  setting = "qf::mc1",
		  norigins = 1,
		  label = "B 1 1 2/b",
		  hall = "-B 2b\0"
		},
		{
		  index =  15,
		  setting = "qf::a1",
		  norigins = 1,
		  label = "B 2/b 1 1",
		  hall = "-B 2xb\0"
		},
		{
		  index =  15,
		  setting = "qf::ma2",
		  norigins = 1,
		  label = "B 2/n 1 1",
		  hall = "-B 2xab\0"
		}
	      }
	    },
	    {
	      label = "C",
	      groupdata = {
		{
		  index =   5,
		  setting = "qf::b1",
		  norigins = 1,
		  label = "C 1 2 1",
		  hall = " C 2y\0"
		},
		{
		  index =   5,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "C 2 1 1",
		  hall = " C 2x\0"
		},
		{
		  index =   8,
		  setting = "qf::b1",
		  norigins = 1,
		  label = "C 1 m 1",
		  hall = " C -2y\0"
		},
		{
		  index =   8,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "C m 1 1",
		  hall = " C -2x\0"
		},
		{
		  index =   9,
		  setting = "qf::b1",
		  norigins = 1,
		  label = "C 1 c 1",
		  hall = " C -2yc\0"
		},
		{
		  index =   9,
		  setting = "qf::mb2",
		  norigins = 1,
		  label = "C 1 n 1",
		  hall = " C -2yac\0"
		},
		{
		  index =   9,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "C n 1 1",
		  hall = " C -2xac\0"
		},
		{
		  index =   9,
		  setting = "qf::ma1",
		  norigins = 1,
		  label = "C c 1 1",
		  hall = " C -2xc\0"
		},
		{
		  index =  12,
		  setting = "qf::b1",
		  norigins = 1,
		  label = "C 1 2/m 1",
		  hall = "-C 2y\0"
		},
		{
		  index =  12,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "C 2/m 1 1",
		  hall = "-C 2x\0"
		},
		{
		  index =  15,
		  setting = "qf::b1",
		  norigins = 1,
		  label = "C 1 2/c 1",
		  hall = "-C 2yc\0"
		},
		{
		  index =  15,
		  setting = "qf::mb2",
		  norigins = 1,
		  label = "C 1 2/n 1",
		  hall = "-C 2yac\0"
		},
		{
		  index =  15,
		  setting = "qf::a2",
		  norigins = 1,
		  label = "C 2/n 1 1",
		  hall = "-C 2xac\0"
		},
		{
		  index =  15,
		  setting = "qf::ma1",
		  norigins = 1,
		  label = "C 2/c 1 1",
		  hall = "-C 2xc\0"
		}
	      }
	    },
	    {
	      label = "I",
	      groupdata = {
		{
		  index =   5,
		  setting = "qf::b3",
		  norigins = 1,
		  label = "I 1 2 1",
		  hall = " I 2y\0"
		},
		{
		  index =   5,
		  setting = "qf::c3",
		  norigins = 1,
		  label = "I 1 1 2",
		  hall = " I 2\0"
		},
		{
		  index =   5,
		  setting = "qf::a3",
		  norigins = 1,
		  label = "I 2 1 1",
		  hall = " I 2x\0"
		},
		{
		  index =   8,
		  setting = "qf::b3",
		  norigins = 1,
		  label = "I 1 m 1",
		  hall = " I -2y\0"
		},
		{
		  index =   8,
		  setting = "qf::c3",
		  norigins = 1,
		  label = "I 1 1 m",
		  hall = " I -2\0"
		},
		{
		  index =   8,
		  setting = "qf::a3",
		  norigins = 1,
		  label = "I m 1 1",
		  hall = " I -2x\0"
		},
		{
		  index =   9,
		  setting = "qf::b3",
		  norigins = 1,
		  label = "I 1 a 1",
		  hall = " I -2ya\0"
		},
		{
		  index =   9,
		  setting = "qf::mb3",
		  norigins = 1,
		  label = "I 1 c 1",
		  hall = " I -2yc\0"
		},
		{
		  index =   9,
		  setting = "qf::c3",
		  norigins = 1,
		  label = "I 1 1 b",
		  hall = " I -2b\0"
		},
		{
		  index =   9,
		  setting = "qf::mc3",
		  norigins = 1,
		  label = "I 1 1 a",
		  hall = " I -2a\0"
		},
		{
		  index =   9,
		  setting = "qf::a3",
		  norigins = 1,
		  label = "I c 1 1",
		  hall = " I -2xc\0"
		},
		{
		  index =   9,
		  setting = "qf::ma3",
		  norigins = 1,
		  label = "I b 1 1",
		  hall = " I -2xb\0"
		},
		{
		  index =  12,
		  setting = "qf::b3",
		  norigins = 1,
		  label = "I 1 2/m 1",
		  hall = "-I 2y\0"
		},
		{
		  index =  12,
		  setting = "qf::c3",
		  norigins = 1,
		  label = "I 1 1 2/m",
		  hall = "-I 2\0"
		},
		{
		  index =  12,
		  setting = "qf::a3",
		  norigins = 1,
		  label = "I 2/m 1 1",
		  hall = "-I 2x\0"
		},
		{
		  index =  15,
		  setting = "qf::b3",
		  norigins = 1,
		  label = "I 1 2/a 1",
		  hall = "-I 2ya\0"
		},
		{
		  index =  15,
		  setting = "qf::mb3",
		  norigins = 1,
		  label = "I 1 2/c 1",
		  hall = "-I 2yc\0"
		},
		{
		  index =  15,
		  setting = "qf::c3",
		  norigins = 1,
		  label = "I 1 1 2/b",
		  hall = "-I 2b\0"
		},
		{
		  index =  15,
		  setting = "qf::mc3",
		  norigins = 1,
		  label = "I 1 1 2/a",
		  hall = "-I 2a\0"
		},
		{
		  index =  15,
		  setting = "qf::a3",
		  norigins = 1,
		  label = "I 2/c 1 1",
		  hall = "-I 2xc\0"
		},
		{
		  index =  15,
		  setting = "qf::ma3",
		  norigins = 1,
		  label = "I 2/b 1 1",
		  hall = "-I 2xb\0"
		}
	      }
	    }
	  }
	},
	{
	  label = "Orthorhombic",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index =  16,
		  setting= "0",
		  norigins = 1,
		  label = "P 2 2 2",
		  hall = " P 2 2\0"
		},
		{
		  index =  17,
		  setting= "0",
		  norigins = 1,
		  label = "P 2 2 21",
		  hall = " P 2c 2\0"
		},
		{
		  index =  17,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 21 2 2",
		  hall = " P 2a 2a\0"
		},
		{
		  index =  17,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P 2 21 2",
		  hall = " P 2 2b\0"
		},
		{
		  index =  18,
		  setting= "0",
		  norigins = 1,
		  label = "P 21 21 2",
		  hall = " P 2 2ab\0"
		},
		{
		  index =  18,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 2 21 21",
		  hall = " P 2bc 2\0"
		},
		{
		  index =  18,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P 21 2 21",
		  hall = " P 2ac 2ac\0"
		},
		{
		  index =  19,
		  setting= "0",
		  norigins = 1,
		  label = "P 21 21 21",
		  hall = " P 2ac 2ab\0"
		},
		{
		  index =  25,
		  setting= "0",
		  norigins = 1,
		  label = "P m m 2",
		  hall = " P 2 -2\0"
		},
		{
		  index =  25,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 2 m m",
		  hall = " P -2 2\0"
		},
		{
		  index =  25,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P m 2 m",
		  hall = " P -2 -2\0"
		},
		{
		  index =  26,
		  setting= "0",
		  norigins = 1,
		  label = "P m c 21",
		  hall = " P 2c -2\0"
		},
		{
		  index =  26,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P c m 21",
		  hall = " P 2c -2c\0"
		},
		{
		  index =  26,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 21 m a",
		  hall = " P -2a 2a\0"
		},
		{
		  index =  26,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P 21 a m",
		  hall = " P -2 2a\0"
		},
		{
		  index =  26,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P b 21 m",
		  hall = " P -2 -2b\0"
		},
		{
		  index =  26,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P m 21 b",
		  hall = " P -2b -2\0"
		},
		{
		  index =  27,
		  setting= "0",
		  norigins = 1,
		  label = "P c c 2",
		  hall = " P 2 -2c\0"
		},
		{
		  index =  27,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 2 a a",
		  hall = " P -2a 2\0"
		},
		{
		  index =  27,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P b 2 b",
		  hall = " P -2b -2b\0"
		},
		{
		  index =  28,
		  setting= "0",
		  norigins = 1,
		  label = "P m a 2",
		  hall = " P 2 -2a\0"
		},
		{
		  index =  28,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P b m 2",
		  hall = " P 2 -2b\0"
		},
		{
		  index =  28,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 2 m b",
		  hall = " P -2b 2\0"
		},
		{
		  index =  28,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P 2 c m",
		  hall = " P -2c 2\0"
		},
		{
		  index =  28,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P c 2 m",
		  hall = " P -2c -2c\0"
		},
		{
		  index =  28,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P m 2 a",
		  hall = " P -2a -2a\0"
		},
		{
		  index =  29,
		  setting= "0",
		  norigins = 1,
		  label = "P c a 21",
		  hall = " P 2c -2ac\0"
		},
		{
		  index =  29,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P b c 21",
		  hall = " P 2c -2b\0"
		},
		{
		  index =  29,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 21 a b",
		  hall = " P -2b 2a\0"
		},
		{
		  index =  29,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P 21 c a",
		  hall = " P -2ac 2a\0"
		},
		{
		  index =  29,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P c 21 b",
		  hall = " P -2bc -2c\0"
		},
		{
		  index =  29,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P b 21 a",
		  hall = " P -2a -2ab\0"
		},
		{
		  index =  30,
		  setting= "0",
		  norigins = 1,
		  label = "P n c 2",
		  hall = " P 2 -2bc\0"
		},
		{
		  index =  30,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P c n 2",
		  hall = " P 2 -2ac\0"
		},
		{
		  index =  30,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 2 n a",
		  hall = " P -2ac 2\0"
		},
		{
		  index =  30,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P 2 a n",
		  hall = " P -2ab 2\0"
		},
		{
		  index =  30,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P b 2 n",
		  hall = " P -2ab -2ab\0"
		},
		{
		  index =  30,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P n 2 b",
		  hall = " P -2bc -2bc\0"
		},
		{
		  index =  31,
		  setting= "0",
		  norigins = 1,
		  label = "P m n 21",
		  hall = " P 2ac -2\0"
		},
		{
		  index =  31,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P n m 21",
		  hall = " P 2bc -2bc\0"
		},
		{
		  index =  31,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 21 m n",
		  hall = " P -2ab 2ab\0"
		},
		{
		  index =  31,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P 21 n m",
		  hall = " P -2 2ac\0"
		},
		{
		  index =  31,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P n 21 m",
		  hall = " P -2 -2bc\0"
		},
		{
		  index =  31,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P m 21 n",
		  hall = " P -2ab -2\0"
		},
		{
		  index =  32,
		  setting= "0",
		  norigins = 1,
		  label = "P b a 2",
		  hall = " P 2 -2ab\0"
		},
		{
		  index =  32,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 2 c b",
		  hall = " P -2bc 2\0"
		},
		{
		  index =  32,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P c 2 a",
		  hall = " P -2ac -2ac\0"
		},
		{
		  index =  33,
		  setting= "0",
		  norigins = 1,
		  label = "P n a 21",
		  hall = " P 2c -2n\0"
		},
		{
		  index =  33,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P b n 21",
		  hall = " P 2c -2ab\0"
		},
		{
		  index =  33,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 21 n b",
		  hall = " P -2bc 2a\0"
		},
		{
		  index =  33,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P 21 c n",
		  hall = " P -2n 2a\0"
		},
		{
		  index =  33,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P c 21 n",
		  hall = " P -2n -2ac\0"
		},
		{
		  index =  33,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P n 21 a",
		  hall = " P -2ac -2n\0"
		},
		{
		  index =  34,
		  setting= "0",
		  norigins = 1,
		  label = "P n n 2",
		  hall = " P 2 -2n\0"
		},
		{
		  index =  34,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P 2 n n",
		  hall = " P -2n 2\0"
		},
		{
		  index =  34,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P n 2 n",
		  hall = " P -2n -2n\0"
		},
		{
		  index =  47,
		  setting= "0",
		  norigins = 1,
		  label = "P m m m",
		  hall = "-P 2 2\0"
		},
		{
		  index =  48,
		  setting = "0:2",
		  norigins = 2,
		  label = "P n n n",
		  hall = " P 2 2 -1n\0-P 2ab 2bc\0"
		},
		{
		  index =  49,
		  setting= "0",
		  norigins = 1,
		  label = "P c c m",
		  hall = "-P 2 2c\0"
		},
		{
		  index =  49,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P m a a",
		  hall = "-P 2a 2\0"
		},
		{
		  index =  49,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P b m b",
		  hall = "-P 2b 2b\0"
		},
		{
		  index =  50,
		  setting= "0",
		  norigins = 1,
		  label = "P b a n",
		  hall = " P 2 2 -1ab\0-P 2ab 2b\0"
		},
		{
		  index =  50,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P n c b",
		  hall = " P 2 2 -1bc\0-P 2b 2bc\0"
		},
		{
		  index =  50,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P c n a",
		  hall = " P 2 2 -1ac\0-P 2a 2c\0"
		},
		{
		  index =  51,
		  setting= "0",
		  norigins = 1,
		  label = "P m m a",
		  hall = "-P 2a 2a\0"
		},
		{
		  index =  51,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P m m b",
		  hall = "-P 2b 2\0"
		},
		{
		  index =  51,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P b m m",
		  hall = "-P 2 2b\0"
		},
		{
		  index =  51,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P c m m",
		  hall = "-P 2c 2c\0"
		},
		{
		  index =  51,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P m c m",
		  hall = "-P 2c 2\0"
		},
		{
		  index =  51,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P m a m",
		  hall = "-P 2 2a\0"
		},
		{
		  index =  52,
		  setting= "0",
		  norigins = 1,
		  label = "P n n a",
		  hall = "-P 2a 2bc\0"
		},
		{
		  index =  52,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P n n b",
		  hall = "-P 2b 2n\0"
		},
		{
		  index =  52,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P b n n",
		  hall = "-P 2n 2b\0"
		},
		{
		  index =  52,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P c n n",
		  hall = "-P 2ab 2c\0"
		},
		{
		  index =  52,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P n c n",
		  hall = "-P 2ab 2n\0"
		},
		{
		  index =  52,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P n a n",
		  hall = "-P 2n 2bc\0"
		},
		{
		  index =  53,
		  setting= "0",
		  norigins = 1,
		  label = "P m n a",
		  hall = "-P 2ac 2\0"
		},
		{
		  index =  53,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P n m b",
		  hall = "-P 2bc 2bc\0"
		},
		{
		  index =  53,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P b m n",
		  hall = "-P 2ab 2ab\0"
		},
		{
		  index =  53,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P c n m",
		  hall = "-P 2 2ac\0"
		},
		{
		  index =  53,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P n c m",
		  hall = "-P 2 2bc\0"
		},
		{
		  index =  53,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P m a n",
		  hall = "-P 2ab 2\0"
		},
		{
		  index =  54,
		  setting= "0",
		  norigins = 1,
		  label = "P c c a",
		  hall = "-P 2a 2ac\0"
		},
		{
		  index =  54,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P c c b",
		  hall = "-P 2b 2c\0"
		},
		{
		  index =  54,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P b a a",
		  hall = "-P 2a 2b\0"
		},
		{
		  index =  54,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P c a a",
		  hall = "-P 2ac 2c\0"
		},
		{
		  index =  54,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P b c b",
		  hall = "-P 2bc 2b\0"
		},
		{
		  index =  54,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P b a b",
		  hall = "-P 2b 2ab\0"
		},
		{
		  index =  55,
		  setting= "0",
		  norigins = 1,
		  label = "P b a m",
		  hall = "-P 2 2ab\0"
		},
		{
		  index =  55,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P m c b",
		  hall = "-P 2bc 2\0"
		},
		{
		  index =  55,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P c m a",
		  hall = "-P 2ac 2ac\0"
		},
		{
		  index =  56,
		  setting= "0",
		  norigins = 1,
		  label = "P c c n",
		  hall = "-P 2ab 2ac\0"
		},
		{
		  index =  56,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P n a a",
		  hall = "-P 2ac 2bc\0"
		},
		{
		  index =  56,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P b n b",
		  hall = "-P 2bc 2ab\0"
		},
		{
		  index =  57,
		  setting= "0",
		  norigins = 1,
		  label = "P b c m",
		  hall = "-P 2c 2b\0"
		},
		{
		  index =  57,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P c a m",
		  hall = "-P 2c 2ac\0"
		},
		{
		  index =  57,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P m c a",
		  hall = "-P 2ac 2a\0"
		},
		{
		  index =  57,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P m a b",
		  hall = "-P 2b 2a\0"
		},
		{
		  index =  57,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P b m a",
		  hall = "-P 2a 2ab\0"
		},
		{
		  index =  57,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P c m b",
		  hall = "-P 2bc 2c\0"
		},
		{
		  index =  58,
		  setting= "0",
		  norigins = 1,
		  label = "P n n m",
		  hall = "-P 2 2n\0"
		},
		{
		  index =  58,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P m n n",
		  hall = "-P 2n 2\0"
		},
		{
		  index =  58,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P n m n",
		  hall = "-P 2n 2n\0"
		},
		{
		  index =  59,
		  setting= "0",
		  norigins = 1,
		  label = "P m m n",
		  hall = " P 2 2ab -1ab\0-P 2ab 2a\0"
		},
		{
		  index =  59,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P n m m",
		  hall = " P 2bc 2 -1bc\0-P 2c 2bc\0"
		},
		{
		  index =  59,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P m n m",
		  hall = " P 2ac 2ac -1ac\0-P 2c 2a\0"
		},
		{
		  index =  60,
		  setting= "0",
		  norigins = 1,
		  label = "P b c n",
		  hall = "-P 2n 2ab\0"
		},
		{
		  index =  60,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P c a n",
		  hall = "-P 2n 2c\0"
		},
		{
		  index =  60,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P n c a",
		  hall = "-P 2a 2n\0"
		},
		{
		  index =  60,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P n a b",
		  hall = "-P 2bc 2n\0"
		},
		{
		  index =  60,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P b n a",
		  hall = "-P 2ac 2b\0"
		},
		{
		  index =  60,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P c n b",
		  hall = "-P 2b 2ac\0"
		},
		{
		  index =  61,
		  setting= "0",
		  norigins = 1,
		  label = "P b c a",
		  hall = "-P 2ac 2ab\0"
		},
		{
		  index =  61,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P c a b",
		  hall = "-P 2bc 2ac\0"
		},
		{
		  index =  62,
		  setting= "0",
		  norigins = 1,
		  label = "P n m a",
		  hall = "-P 2ac 2n\0"
		},
		{
		  index =  62,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "P m n b",
		  hall = "-P 2bc 2a\0"
		},
		{
		  index =  62,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "P b n m",
		  hall = "-P 2c 2ab\0"
		},
		{
		  index =  62,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "P c m n",
		  hall = "-P 2n 2ac\0"
		},
		{
		  index =  62,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "P m c n",
		  hall = "-P 2n 2a\0"
		},
		{
		  index =  62,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "P n a m",
		  hall = "-P 2c 2n\0"
		}
	      }
	    },
	    {
	      label = "A",
	      groupdata = {
		{
		  index =  20,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A 21 2 2",
		  hall = " A 2a 2a\0"
		},
		{
		  index =  21,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A 2 2 2",
		  hall = " A 2 2\0"
		},
		{
		  index =  35,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A 2 m m",
		  hall = " A -2 2\0"
		},
		{
		  index =  36,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A 21 m a",
		  hall = " A -2a 2a\0"
		},
		{
		  index =  36,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "A 21 a m",
		  hall = " A -2 2a\0"
		},
		{
		  index =  37,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A 2 a a",
		  hall = " A -2a 2\0"
		},
		{
		  index =  38,
		  setting= "0",
		  norigins = 1,
		  label = "A m m 2",
		  hall = " A 2 -2\0"
		},
		{
		  index =  38,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "A m 2 m",
		  hall = " A -2 -2\0"
		},
		{
		  index =  39,
		  setting= "0",
		  norigins = 1,
		  label = "A b m 2",
		  hall = " A 2 -2b\0"
		},
		{
		  index =  39,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "A c 2 m",
		  hall = " A -2b -2b\0"
		},
		{
		  index =  40,
		  setting= "0",
		  norigins = 1,
		  label = "A m a 2",
		  hall = " A 2 -2a\0"
		},
		{
		  index =  40,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "A m 2 a",
		  hall = " A -2a -2a\0"
		},
		{
		  index =  41,
		  setting= "0",
		  norigins = 1,
		  label = "A b a 2",
		  hall = " A 2 -2ab\0"
		},
		{
		  index =  41,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "A c 2 a",
		  hall = " A -2ab -2ab\0"
		},
		{
		  index =  63,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A m m a",
		  hall = "-A 2a 2a\0"
		},
		{
		  index =  63,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "A m a m",
		  hall = "-A 2 2a\0"
		},
		{
		  index =  64,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A b m a",
		  hall = "-A 2ab 2ab\0"
		},
		{
		  index =  64,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "A c a m",
		  hall = "-A 2 2ab\0"
		},
		{
		  index =  65,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A m m m",
		  hall = "-A 2 2\0"
		},
		{
		  index =  66,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A m a a",
		  hall = "-A 2a 2\0"
		},
		{
		  index =  67,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A b m m",
		  hall = "-A 2b 2b\0"
		},
		{
		  index =  67,
		  setting = "qf::mcba",
		  label = "A c m m",
		  norigins = 1,
		  hall = "-A 2 2b\0"
		},
		{
		  index =  68,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "A b a a",
		  hall = " A 2 2 -1ab\0-A 2a 2b\0"
		},
		{
		  index =  68,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "A c a a",
		  hall = " A 2 2 -1ab\0-A 2ab 2b\0"
		}
	      }
	    },
	    {
	      label = "B",
	      groupdata = {
		{
		  index =  20,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B 2 21 2",
		  hall = " B 2 2b\0"
		},
		{
		  index =  21,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B 2 2 2",
		  hall = " B 2 2\0"
		},
		{
		  index =  35,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B m 2 m",
		  hall = " B -2 -2\0"
		},
		{
		  index =  36,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B b 21 m",
		  hall = " B -2 -2b\0"
		},
		{
		  index =  36,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "B m 21 b",
		  hall = " B -2b -2\0"
		},
		{
		  index =  37,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B b 2 b",
		  hall = " B -2b -2b\0"
		},
		{
		  index =  38,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "B m m 2",
		  hall = " B 2 -2\0"
		},
		{
		  index =  38,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "B 2 m m",
		  hall = " B -2 2\0"
		},
		{
		  index =  39,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "B m a 2",
		  hall = " B 2 -2a\0"
		},
		{
		  index =  39,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "B 2 c m",
		  hall = " B -2a 2\0"
		},
		{
		  index =  40,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "B b m 2",
		  hall = " B 2 -2b\0"
		},
		{
		  index =  40,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "B 2 m b",
		  hall = " B -2b 2\0"
		},
		{
		  index =  41,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "B b a 2",
		  hall = " B 2 -2ab\0"
		},
		{
		  index =  41,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "B 2 c b",
		  hall = " B -2ab 2\0"
		},
		{
		  index =  63,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B b m m",
		  hall = "-B 2 2b\0"
		},
		{
		  index =  63,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "B m m b",
		  hall = "-B 2b 2\0"
		},
		{
		  index =  64,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B b c m",
		  hall = "-B 2 2ab\0"
		},
		{
		  index =  64,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "B m a b",
		  hall = "-B 2ab 2\0"
		},
		{
		  index =  65,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B m m m",
		  hall = "-B 2 2\0"
		},
		{
		  index =  66,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B b m b",
		  hall = "-B 2b 2b\0"
		},
		{
		  index =  67,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B m c m",
		  hall = "-B 2 2a\0"
		},
		{
		  index =  67,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "B m a m",
		  hall = "-B 2a 2\0"
		},
		{
		  index =  68,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "B b c b",
		  hall = " B 2 2 -1ab\0-B 2ab 2b\0"
		},
		{
		  index =  68,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "B b a b",
		  hall = " B 2 2 -1ab\0-B 2b 2ab\0"
		}
	      }
	    },
	    {
	      label = "C",
	      groupdata = {
		{
		  index =  20,
		  setting= "0",
		  norigins = 1,
		  label = "C 2 2 21",
		  hall = " C 2c 2\0"
		},
		{
		  index =  21,
		  setting= "0",
		  norigins = 1,
		  label = "C 2 2 2",
		  hall = " C 2 2\0"
		},
		{
		  index =  35,
		  setting= "0",
		  norigins = 1,
		  label = "C m m 2",
		  hall = " C 2 -2\0"
		},
		{
		  index =  36,
		  setting= "0",
		  norigins = 1,
		  label = "C m c 21",
		  hall = " C 2c -2\0"
		},
		{
		  index =  36,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "C c m 21",
		  hall = " C 2c -2c\0"
		},
		{
		  index =  37,
		  setting= "0",
		  norigins = 1,
		  label = "C c c 2",
		  hall = " C 2 -2c\0"
		},
		{
		  index =  38,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "C 2 m m",
		  hall = " C -2 2\0"
		},
		{
		  index =  38,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "C m 2 m",
		  hall = " C -2 -2\0"
		},
		{
		  index =  39,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "C 2 m b",
		  hall = " C -2a 2\0"
		},
		{
		  index =  39,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "C m 2 a",
		  hall = " C -2a -2a\0"
		},
		{
		  index =  40,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "C 2 c m",
		  hall = " C -2c 2\0"
		},
		{
		  index =  40,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "C c 2 m",
		  hall = " C -2c -2c\0"
		},
		{
		  index =  41,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "C 2 c b",
		  hall = " C -2ac 2\0"
		},
		{
		  index =  41,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "C c 2 a",
		  hall = " C -2ac -2ac\0"
		},
		{
		  index =  63,
		  setting= "0",
		  norigins = 1,
		  label = "C m c m",
		  hall = "-C 2c 2\0"
		},
		{
		  index =  63,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "C c m m",
		  hall = "-C 2c 2c\0"
		},
		{
		  index =  64,
		  setting= "0",
		  norigins = 1,
		  label = "C m c a",
		  hall = "-C 2ac 2\0"
		},
		{
		  index =  64,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "C c m b",
		  hall = "-C 2ac 2ac\0"
		},
		{
		  index =  65,
		  setting= "0",
		  norigins = 1,
		  label = "C m m m",
		  hall = "-C 2 2\0"
		},
		{
		  index =  66,
		  setting= "0",
		  norigins = 1,
		  label = "C c c m",
		  hall = "-C 2 2c\0"
		},
		{
		  index =  67,
		  setting= "0",
		  norigins = 1,
		  label = "C m m a",
		  hall = "-C 2a 2\0"
		},
		{
		  index =  67,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "C m m b",
		  hall = "-C 2a 2a\0"
		},
		{
		  index =  68,
		  setting= "0",
		  norigins = 1,
		  label = "C c c a",
		  hall = " C 2 2 -1ac\0-C 2a 2ac\0"
		},
		{
		  index =  68,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "C c c b",
		  hall = " C 2 2 -1ac\0-C 2a 2c\0"
		}
	      }
	    },
	    {
	      label = "Face",
	      groupdata = {
		{
		  index =  22,
		  setting= "0",
		  norigins = 1,
		  label = "F 2 2 2",
		  hall = " F 2 2\0"
		},
		{
		  index =  42,
		  setting= "0",
		  norigins = 1,
		  label = "F m m 2",
		  hall = " F 2 -2\0"
		},
		{
		  index =  42,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "F 2 m m",
		  hall = " F -2 2\0"
		},
		{
		  index =  42,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "F m 2 m",
		  hall = " F -2 -2\0"
		},
		{
		  index =  43,
		  setting= "0",
		  norigins = 1,
		  label = "F d d 2",
		  hall = " F 2 -2d\0"
		},
		{
		  index =  43,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "F 2 d d",
		  hall = " F -2d 2\0"
		},
		{
		  index =  43,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "F d 2 d",
		  hall = " F -2d -2d\0"
		},
		{
		  index =  69,
		  setting= "0",
		  norigins = 1,
		  label = "F m m m",
		  hall = "-F 2 2\0"
		},
		{
		  index =  70,
		  setting = "0:2",
		  norigins = 2,
		  label = "F d d d",
		  hall = " F 2 2 -1d\0-F 2uv 2vw\0"
		}
	      }
	    },
	    {
	      label = "Body",
	      groupdata = {
		{
		  index =  23,
		  setting= "0",
		  norigins = 1,
		  label = "I 2 2 2",
		  hall = " I 2 2\0"
		},
		{
		  index =  24,
		  setting= "0",
		  norigins = 1,
		  label = "I 21 21 21",
		  hall = " I 2b 2c\0"
		},
		{
		  index =  44,
		  setting= "0",
		  norigins = 1,
		  label = "I m m 2",
		  hall = " I 2 -2\0"
		},
		{
		  index =  44,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "I 2 m m",
		  hall = " I -2 2\0"
		},
		{
		  index =  44,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "I m 2 m",
		  hall = " I -2 -2\0"
		},
		{
		  index =  45,
		  setting= "0",
		  norigins = 1,
		  label = "I b a 2",
		  hall = " I 2 -2c\0"
		},
		{
		  index =  45,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "I 2 c b",
		  hall = " I -2a 2\0"
		},
		{
		  index =  45,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "I c 2 a",
		  hall = " I -2b -2b\0"
		},
		{
		  index =  46,
		  setting= "0",
		  norigins = 1,
		  label = "I m a 2",
		  hall = " I 2 -2a\0"
		},
		{
		  index =  46,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "I b m 2",
		  hall = " I 2 -2b\0"
		},
		{
		  index =  46,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "I 2 m b",
		  hall = " I -2b 2\0"
		},
		{
		  index =  46,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "I 2 c m",
		  hall = " I -2c 2\0"
		},
		{
		  index =  46,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "I c 2 m",
		  hall = " I -2c -2c\0"
		},
		{
		  index =  46,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "I m 2 a",
		  hall = " I -2a -2a\0"
		},
		{
		  index =  71,
		  setting= "0",
		  norigins = 1,
		  label = "I m m m",
		  hall = "-I 2 2\0"
		},
		{
		  index =  72,
		  setting= "0",
		  norigins = 1,
		  label = "I b a m",
		  hall = "-I 2 2c\0"
		},
		{
		  index =  72,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "I m c b",
		  hall = "-I 2a 2\0"
		},
		{
		  index =  72,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "I c m a",
		  hall = "-I 2b 2b\0"
		},
		{
		  index =  73,
		  setting= "0",
		  norigins = 1,
		  label = "I b c a",
		  hall = "-I 2b 2c\0"
		},
		{
		  index =  73,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "I c a b",
		  hall = "-I 2a 2b\0"
		},
		{
		  index =  74,
		  setting= "0",
		  norigins = 1,
		  label = "I m m a",
		  hall = "-I 2b 2\0"
		},
		{
		  index =  74,
		  setting = "qf::bamc",
		  norigins = 1,
		  label = "I m m b",
		  hall = "-I 2a 2a\0"
		},
		{
		  index =  74,
		  setting = "qf::cab",
		  norigins = 1,
		  label = "I b m m",
		  hall = "-I 2c 2c\0"
		},
		{
		  index =  74,
		  setting = "qf::mcba",
		  norigins = 1,
		  label = "I c m m",
		  hall = "-I 2 2b\0"
		},
		{
		  index =  74,
		  setting = "qf::bca",
		  norigins = 1,
		  label = "I m c m",
		  hall = "-I 2 2a\0"
		},
		{
		  index =  74,
		  setting = "qf::amcb",
		  norigins = 1,
		  label = "I m a m",
		  hall = "-I 2c 2\0"
		}
	      }
	    }
	  }
	},
	{
	  label = "Tetragonal",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index =  75,
		  setting= "0",
		  norigins = 1,
		  label = "P 4",
		  hall = " P 4\0"
		},
		{
		  index =  76,
		  setting= "0",
		  norigins = 1,
		  label = "P 41",
		  hall = " P 4w\0"
		},
		{
		  index =  77,
		  setting= "0",
		  norigins = 1,
		  label = "P 42",
		  hall = " P 4c\0"
		},
		{
		  index =  78,
		  setting= "0",
		  norigins = 1,
		  label = "P 43",
		  hall = " P 4cw\0"
		},
		{
		  index =  81,
		  setting= "0",
		  norigins = 1,
		  label = "P -4",
		  hall = " P -4\0"
		},
		{
		  index =  83,
		  setting= "0",
		  norigins = 1,
		  label = "P 4/m",
		  hall = "-P 4\0"
		},
		{
		  index =  84,
		  setting= "0",
		  norigins = 1,
		  label = "P 42/m",
		  hall = "-P 4c\0"
		},
		{
		  index =  85,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 4/n",
		  hall = " P 4ab -1ab\0-P 4a\0"
		},
		{
		  index =  86,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 42/n",
		  hall = " P 4n -1n\0-P 4bc\0"
		},
		{
		  index =  89,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 2 2",
		  hall = " P 4 2\0"
		},
		{
		  index =  90,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 21 2",
		  hall = " P 4ab 2ab\0"
		},
		{
		  index =  91,
		  setting= "0",
		  norigins = 1,
		  label = "P 41 2 2",
		  hall = " P 4w 2c\0"
		},
		{
		  index =  92,
		  setting= "0",
		  norigins = 1,
		  label = "P 41 21 2",
		  hall = " P 4abw 2nw\0"
		},
		{
		  index =  93,
		  setting= "0",
		  norigins = 1,
		  label = "P 42 2 2",
		  hall = " P 4c 2\0"
		},
		{
		  index =  94,
		  setting= "0",
		  norigins = 1,
		  label = "P 42 21 2",
		  hall = " P 4n 2n\0"
		},
		{
		  index =  95,
		  setting= "0",
		  norigins = 1,
		  label = "P 43 2 2",
		  hall = " P 4cw 2c\0"
		},
		{
		  index =  96,
		  setting= "0",
		  norigins = 1,
		  label = "P 43 21 2",
		  hall = " P 4nw 2abw\0"
		},
		{
		  index =  99,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 m m",
		  hall = " P 4 -2\0"
		},
		{
		  index = 100,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 b m",
		  hall = " P 4 -2ab\0"
		},
		{
		  index = 101,
		  setting= "0",
		  norigins = 1,
		  label = "P 42 c m",
		  hall = " P 4c -2c\0"
		},
		{
		  index = 102,
		  setting= "0",
		  norigins = 1,
		  label = "P 42 n m",
		  hall = " P 4n -2n\0"
		},
		{
		  index = 103,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 c c",
		  hall = " P 4 -2c\0"
		},
		{
		  index = 104,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 n c",
		  hall = " P 4 -2n\0"
		},
		{
		  index = 105,
		  setting= "0",
		  norigins = 1,
		  label = "P 42 m c",
		  hall = " P 4c -2\0"
		},
		{
		  index = 106,
		  setting= "0",
		  norigins = 1,
		  label = "P 42 b c",
		  hall = " P 4c -2ab\0"
		},
		{
		  index = 111,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 2 m",
		  hall = " P -4 2\0"
		},
		{
		  index = 112,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 2 c",
		  hall = " P -4 2c\0"
		},
		{
		  index = 113,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 21 m",
		  hall = " P -4 2ab\0"
		},
		{
		  index = 114,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 21 c",
		  hall = " P -4 2n\0"
		},
		{
		  index = 115,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 m 2",
		  hall = " P -4 -2\0"
		},
		{
		  index = 116,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 c 2",
		  hall = " P -4 -2c\0"
		},
		{
		  index = 117,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 b 2",
		  hall = " P -4 -2ab\0"
		},
		{
		  index = 118,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 n 2",
		  hall = " P -4 -2n\0"
		},
		{
		  index = 123,
		  setting= "0",
		  norigins = 1,
		  label = "P 4/m m m",
		  hall = "-P 4 2\0"
		},
		{
		  index = 124,
		  setting= "0",
		  norigins = 1,
		  label = "P 4/m c c",
		  hall = "-P 4 2c\0"
		},
		{
		  index = 125,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 4/n b m",
		  hall = " P 4 2 -1ab\0-P 4a 2b\0"
		},
		{
		  index = 126,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 4/n n c",
		  hall = " P 4 2 -1n\0-P 4a 2bc\0"
		},
		{
		  index = 127,
		  setting= "0",
		  norigins = 1,
		  label = "P 4/m b m",
		  hall = "-P 4 2ab\0"
		},
		{
		  index = 128,
		  setting= "0",
		  norigins = 1,
		  label = "P 4/m n c",
		  hall = "-P 4 2n\0"
		},
		{
		  index = 129,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 4/n m m",
		  hall = " P 4ab 2ab -1ab\0-P 4a 2a\0"
		},
		{
		  index = 130,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 4/n c c",
		  hall = " P 4ab 2n -1ab\0-P 4a 2ac\0"
		},
		{
		  index = 131,
		  setting= "0",
		  norigins = 1,
		  label = "P 42/m m c",
		  hall = "-P 4c 2\0"
		},
		{
		  index = 132,
		  setting= "0",
		  norigins = 1,
		  label = "P 42/m c m",
		  hall = "-P 4c 2c\0"
		},
		{
		  index = 133,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 42/n b c",
		  hall = " P 4n 2c -1n\0-P 4ac 2b\0"
		},
		{
		  index = 134,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 42/n n m",
		  hall = " P 4n 2 -1n\0-P 4ac 2bc\0"
		},
		{
		  index = 135,
		  setting= "0",
		  norigins = 1,
		  label = "P 42/m b c",
		  hall = "-P 4c 2ab\0"
		},
		{
		  index = 136,
		  setting= "0",
		  norigins = 1,
		  label = "P 42/m n m",
		  hall = "-P 4n 2n\0"
		},
		{
		  index = 137,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 42/n m c",
		  hall = " P 4n 2n -1n\0-P 4ac 2a\0"
		},
		{
		  index = 138,
		  setting = "0:2",
		  norigins = 2,
		  label = "P 42/n c m",
		  hall = " P 4n 2ab -1n\0-P 4ac 2ac\0"
		}
	      }
	    },
	    {
	      label = "Body",
	      groupdata = {
		{
		  index =  79,
		  setting= "0",
		  norigins = 1,
		  label = "I 4",
		  hall = " I 4\0"
		},
		{
		  index =  80,
		  setting= "0",
		  norigins = 1,
		  label = "I 41",
		  hall = " I 4bw\0"
		},
		{
		  index =  82,
		  setting= "0",
		  norigins = 1,
		  label = "I -4",
		  hall = " I -4\0"
		},
		{
		  index =  87,
		  setting= "0",
		  norigins = 1,
		  label = "I 4/m",
		  hall = "-I 4\0"
		},
		{
		  index =  88,
		  setting = "0:2",
		  norigins = 2,
		  label = "I 41/a",
		  hall = " I 4bw -1bw\0-I 4ad\0"
		},
		{
		  index =  97,
		  setting= "0",
		  norigins = 1,
		  label = "I 4 2 2",
		  hall = " I 4 2\0"
		},
		{
		  index =  98,
		  setting= "0",
		  norigins = 1,
		  label = "I 41 2 2",
		  hall = " I 4bw 2bw\0"
		},
		{
		  index = 107,
		  setting= "0",
		  norigins = 1,
		  label = "I 4 m m",
		  hall = " I 4 -2\0"
		},
		{
		  index = 108,
		  setting= "0",
		  norigins = 1,
		  label = "I 4 c m",
		  hall = " I 4 -2c\0"
		},
		{
		  index = 109,
		  setting= "0",
		  norigins = 1,
		  label = "I 41 m d",
		  hall = " I 4bw -2\0"
		},
		{
		  index = 110,
		  setting= "0",
		  norigins = 1,
		  label = "I 41 c d",
		  hall = " I 4bw -2c\0"
		},
		{
		  index = 119,
		  setting= "0",
		  norigins = 1,
		  label = "I -4 m 2",
		  hall = " I -4 -2\0"
		},
		{
		  index = 120,
		  setting= "0",
		  norigins = 1,
		  label = "I -4 c 2",
		  hall = " I -4 -2c\0"
		},
		{
		  index = 121,
		  setting= "0",
		  norigins = 1,
		  label = "I -4 2 m",
		  hall = " I -4 2\0"
		},
		{
		  index = 122,
		  setting= "0",
		  norigins = 1,
		  label = "I -4 2 d",
		  hall = " I -4 2bw\0"
		},
		{
		  index = 139,
		  setting= "0",
		  norigins = 1,
		  label = "I 4/m m m",
		  hall = "-I 4 2\0"
		},
		{
		  index = 140,
		  setting= "0",
		  norigins = 1,
		  label = "I 4/m c m",
		  hall = "-I 4 2c\0"
		},
		{
		  index = 141,
		  setting = "0:2",
		  norigins = 2,
		  label = "I 41/a m d",
		  hall = " I 4bw 2bw -1bw\0-I 4bd 2\0"
		},
		{
		  index = 142,
		  setting = "0:2",
		  norigins = 2,
		  label = "I 41/a c d",
		  hall = " I 4bw 2aw -1bw\0-I 4bd 2c\0"
		}
	      }
	    }
	  }
	},
	{
	  label = "Trigonal",
	  centres = {
	    {
	      label = "Hexagonal",
	      groupdata = {
		{
		  index = 143,
		  setting= "0",
		  norigins = 1,
		  label = "P 3",
		  hall = " P 3\0"
		},
		{
		  index = 144,
		  setting= "0",
		  norigins = 1,
		  label = "P 31",
		  hall = " P 31\0"
		},
		{
		  index = 145,
		  setting= "0",
		  norigins = 1,
		  label = "P 32",
		  hall = " P 32\0"
		},
		{
		  index = 146,
		  setting= "0",
		  norigins = 1,
		  label = "R 3",
		  hall = " R 3\0 P 3*\0"
		},
		{
		  index = 147,
		  setting= "0",
		  norigins = 1,
		  label = "P -3",
		  hall = "-P 3\0"
		},
		{
		  index = 148,
		  setting= "0",
		  norigins = 1,
		  label = "R -3",
		  hall = "-R 3\0-P 3*\0"
		},
		{
		  index = 149,
		  setting= "0",
		  norigins = 1,
		  label = "P 3 1 2",
		  hall = " P 3 2\0"
		},
		{
		  index = 150,
		  setting= "0",
		  norigins = 1,
		  label = "P 3 2 1",
		  hall = " P 3 2\"\0"
		},
		{
		  index = 151,
		  setting= "0",
		  norigins = 1,
		  label = "P 31 1 2",
		  hall = " P 31 2 (0 0 4)\0"
		},
		{
		  index = 152,
		  setting= "0",
		  norigins = 1,
		  label = "P 31 2 1",
		  hall = " P 31 2\"\0"
		},
		{
		  index = 153,
		  setting= "0",
		  norigins = 1,
		  label = "P 32 1 2",
		  hall = " P 32 2 (0 0 2)\0"
		},
		{
		  index = 154,
		  setting= "0",
		  norigins = 1,
		  label = "P 32 2 1",
		  hall = " P 32 2\"\0"
		},
		{
		  index = 155,
		  setting= "0",
		  norigins = 1,
		  label = "R 3 2",
		  hall = " R 3 2\"\0 P 3* 2\0"
		},
		{
		  index = 156,
		  setting= "0",
		  norigins = 1,
		  label = "P 3 m 1",
		  hall = " P 3 -2\"\0"
		},
		{
		  index = 157,
		  setting= "0",
		  norigins = 1,
		  label = "P 3 1 m",
		  hall = " P 3 -2\0"
		},
		{
		  index = 158,
		  setting= "0",
		  norigins = 1,
		  label = "P 3 c 1",
		  hall = " P 3 -2\"c\0"
		},
		{
		  index = 159,
		  setting= "0",
		  norigins = 1,
		  label = "P 3 1 c",
		  hall = " P 3 -2c\0"
		},
		{
		  index = 160,
		  setting= "0",
		  norigins = 1,
		  label = "R 3 m",
		  hall = " R 3 -2\"\0 P 3* -2\0"
		},
		{
		  index = 161,
		  setting= "0",
		  norigins = 1,
		  label = "R 3 c",
		  hall = " R 3 -2\"c\0 P 3* -2n\0"
		},
		{
		  index = 162,
		  setting= "0",
		  norigins = 1,
		  label = "P -3 1 m",
		  hall = "-P 3 2\0"
		},
		{
		  index = 163,
		  setting= "0",
		  norigins = 1,
		  label = "P -3 1 c",
		  hall = "-P 3 2c\0"
		},
		{
		  index = 164,
		  setting= "0",
		  norigins = 1,
		  label = "P -3 m 1",
		  hall = "-P 3 2\"\0"
		},
		{
		  index = 165,
		  setting= "0",
		  norigins = 1,
		  label = "P -3 c 1",
		  hall = "-P 3 2\"c\0"
		},
		{
		  index = 166,
		  setting= "0",
		  norigins = 1,
		  label = "R -3 m",
		  hall = "-R 3 2\"\0-P 3* 2\0"
		},
		{
		  index = 167,
		  setting= "0",
		  norigins = 1,
		  label = "R -3 c",
		  hall = "-R 3 2\"c\0-P 3* 2n\0"
		}
	      }
	    },
	    {
	      label = "Rhombohedral",
	      groupdata = {
		{
		  index = 146,
		  setting= "0",
		  norigins = 1,
		  label = "R 3",
		  hall = " R 3\0 P 3*\0"
		},
		{
		  index = 148,
		  setting= "0",
		  norigins = 1,
		  label = "R -3",
		  hall = "-R 3\0-P 3*\0"
		},
		{
		  index = 155,
		  setting= "0",
		  norigins = 1,
		  label = "R 3 2",
		  hall = " R 3 2\"\0 P 3* 2\0"
		},
		{
		  index = 160,
		  setting= "0",
		  norigins = 1,
		  label = "R 3 m",
		  hall = " R 3 -2\"\0 P 3* -2\0"
		},
		{
		  index = 161,
		  setting= "0",
		  norigins = 1,
		  label = "R 3 c",
		  hall = " R 3 -2\"c\0 P 3* -2n\0"
		},
		{
		  index = 166,
		  setting= "0",
		  norigins = 1,
		  label = "R -3 m",
		  hall = "-R 3 2\"\0-P 3* 2\0"
		},
		{
		  index = 167,
		  setting= "0",
		  norigins = 1,
		  label = "R -3 c",
		  hall = "-R 3 2\"c\0-P 3* 2n\0"
		}
	      }
	    }
	  }
	},
	{
	  label = "Hexagonal",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index = 168,
		  setting= "0",
		  norigins = 1,
		  label = "P 6",
		  hall = " P 6\0"
		},
		{
		  index = 169,
		  setting= "0",
		  norigins = 1,
		  label = "P 61",
		  hall = " P 61\0"
		},
		{
		  index = 170,
		  setting= "0",
		  norigins = 1,
		  label = "P 65",
		  hall = " P 65\0"
		},
		{
		  index = 171,
		  setting= "0",
		  norigins = 1,
		  label = "P 62",
		  hall = " P 62\0"
		},
		{
		  index = 172,
		  setting= "0",
		  norigins = 1,
		  label = "P 64",
		  hall = " P 64\0"
		},
		{
		  index = 173,
		  setting= "0",
		  norigins = 1,
		  label = "P 63",
		  hall = " P 6c\0"
		},
		{
		  index = 174,
		  setting= "0",
		  norigins = 1,
		  label = "P -6",
		  hall = " P -6\0"
		},
		{
		  index = 175,
		  setting= "0",
		  norigins = 1,
		  label = "P 6/m",
		  hall = "-P 6\0"
		},
		{
		  index = 176,
		  setting= "0",
		  norigins = 1,
		  label = "P 63/m",
		  hall = "-P 6c\0"
		},
		{
		  index = 177,
		  setting= "0",
		  norigins = 1,
		  label = "P 6 2 2",
		  hall = " P 6 2\0"
		},
		{
		  index = 178,
		  setting= "0",
		  norigins = 1,
		  label = "P 61 2 2",
		  hall = " P 61 2 (0 0 5)\0"
		},
		{
		  index = 179,
		  setting= "0",
		  norigins = 1,
		  label = "P 65 2 2",
		  hall = " P 65 2 (0 0 1)\0"
		},
		{
		  index = 180,
		  setting= "0",
		  norigins = 1,
		  label = "P 62 2 2",
		  hall = " P 62 2 (0 0 4)\0"
		},
		{
		  index = 181,
		  setting= "0",
		  norigins = 1,
		  label = "P 64 2 2",
		  hall = " P 64 2 (0 0 2)\0"
		},
		{
		  index = 182,
		  setting= "0",
		  norigins = 1,
		  label = "P 63 2 2",
		  hall = " P 6c 2c\0"
		},
		{
		  index = 183,
		  setting= "0",
		  norigins = 1,
		  label = "P 6 m m",
		  hall = " P 6 -2\0"
		},
		{
		  index = 184,
		  setting= "0",
		  norigins = 1,
		  label = "P 6 c c",
		  hall = " P 6 -2c\0"
		},
		{
		  index = 185,
		  setting= "0",
		  norigins = 1,
		  label = "P 63 c m",
		  hall = " P 6c -2\0"
		},
		{
		  index = 186,
		  setting= "0",
		  norigins = 1,
		  label = "P 63 m c",
		  hall = " P 6c -2c\0"
		},
		{
		  index = 187,
		  setting= "0",
		  norigins = 1,
		  label = "P -6 m 2",
		  hall = " P -6 2\0"
		},
		{
		  index = 188,
		  setting= "0",
		  norigins = 1,
		  label = "P -6 c 2",
		  hall = " P -6c 2\0"
		},
		{
		  index = 189,
		  setting= "0",
		  norigins = 1,
		  label = "P -6 2 m",
		  hall = " P -6 -2\0"
		},
		{
		  index = 190,
		  setting= "0",
		  norigins = 1,
		  label = "P -6 2 c",
		  hall = " P -6c -2c\0"
		},
		{
		  index = 191,
		  setting= "0",
		  norigins = 1,
		  label = "P 6/m m m",
		  hall = "-P 6 2\0"
		},
		{
		  index = 192,
		  setting= "0",
		  norigins = 1,
		  label = "P 6/m c c",
		  hall = "-P 6 2c\0"
		},
		{
		  index = 193,
		  setting= "0",
		  norigins = 1,
		  label = "P 63/m c m",
		  hall = "-P 6c 2\0"
		},
		{
		  index = 194,
		  setting= "0",
		  norigins = 1,
		  label = "P 63/m m c",
		  hall = "-P 6c 2c\0"
		}
	      }
	    }
	  }
	},
	{
	  label = "Cubic",
	  centres = {
	    {
	      label = "Primitive",
	      groupdata = {
		{
		  index = 195,
		  setting= "0",
		  norigins = 1,
		  label = "P 2 3",
		  hall = " P 2 2 3\0"
		},
		{
		  index = 198,
		  setting= "0",
		  norigins = 1,
		  label = "P 21 3",
		  hall = " P 2ac 2ab 3\0"
		},
		{
		  index = 200,
		  setting= "0",
		  norigins = 1,
		  label = "P m -3",
		  hall = "-P 2 2 3\0"
		},
		{
		  index = 201,
		  setting = "0:2",
		  norigins = 2,
		  label = "P n -3",
		  hall = " P 2 2 3 -1n\0-P 2ab 2bc 3\0"
		},
		{
		  index = 205,
		  setting= "0",
		  norigins = 1,
		  label = "P a -3",
		  hall = "-P 2ac 2ab 3\0"
		},
		{
		  index = 207,
		  setting= "0",
		  norigins = 1,
		  label = "P 4 3 2",
		  hall = " P 4 2 3\0"
		},
		{
		  index = 208,
		  setting= "0",
		  norigins = 1,
		  label = "P 42 3 2",
		  hall = " P 4n 2 3\0"
		},
		{
		  index = 212,
		  setting= "0",
		  norigins = 1,
		  label = "P 43 3 2",
		  hall = " P 4acd 2ab 3\0"
		},
		{
		  index = 213,
		  setting= "0",
		  norigins = 1,
		  label = "P 41 3 2",
		  hall = " P 4bd 2ab 3\0"
		},
		{
		  index = 215,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 3 m",
		  hall = " P -4 2 3\0"
		},
		{
		  index = 218,
		  setting= "0",
		  norigins = 1,
		  label = "P -4 3 n",
		  hall = " P -4n 2 3\0"
		},
		{
		  index = 221,
		  setting= "0",
		  norigins = 1,
		  label = "P m -3 m",
		  hall = "-P 4 2 3\0"
		},
		{
		  index = 222,
		  setting = "0:2",
		  norigins = 2,
		  label = "P n -3 n",
		  hall = " P 4 2 3 -1n\0-P 4a 2bc 3\0"
		},
		{
		  index = 223,
		  setting= "0",
		  norigins = 1,
		  label = "P m -3 n",
		  hall = "-P 4n 2 3\0"
		},
		{
		  index = 224,
		  setting = "0:2",
		  norigins = 2,
		  label = "P n -3 m",
		  hall = " P 4n 2 3 -1n\0-P 4bc 2bc 3\0"
		}
	      }
	    },
	    {
	      label = "Face",
	      groupdata = {
		{
		  index = 196,
		  setting= "0",
		  norigins = 1,
		  label = "F 2 3",
		  hall = " F 2 2 3\0"
		},
		{
		  index = 202,
		  setting= "0",
		  norigins = 1,
		  label = "F m -3",
		  hall = "-F 2 2 3\0"
		},
		{
		  index = 203,
		  setting = "0:2",
		  norigins = 2,
		  label = "F d -3",
		  hall = " F 2 2 3 -1d\0-F 2uv 2vw 3\0"
		},
		{
		  index = 209,
		  setting= "0",
		  norigins = 1,
		  label = "F 4 3 2",
		  hall = " F 4 2 3\0"
		},
		{
		  index = 210,
		  setting= "0",
		  norigins = 1,
		  label = "F 41 3 2",
		  hall = " F 4d 2 3\0"
		},
		{
		  index = 216,
		  setting= "0",
		  norigins = 1,
		  label = "F -4 3 m",
		  hall = " F -4 2 3\0"
		},
		{
		  index = 219,
		  setting= "0",
		  norigins = 1,
		  label = "F -4 3 c",
		  hall = " F -4a 2 3\0"
		},
		{
		  index = 225,
		  setting= "0",
		  norigins = 1,
		  label = "F m -3 m",
		  hall = "-F 4 2 3\0"
		},
		{
		  index = 226,
		  setting= "0",
		  norigins = 1,
		  label = "F m -3 c",
		  hall = "-F 4a 2 3\0"
		},
		{
		  index = 227,
		  setting = "0:2",
		  norigins = 2,
		  label = "F d -3 m",
		  hall = " F 4d 2 3 -1d\0-F 4vw 2vw 3\0"
		},
		{
		  index = 228,
		  setting = "0:2",
		  norigins = 2,
		  label = "F d -3 c",
		  hall = " F 4d 2 3 -1ad\0-F 4ud 2vw 3\0"
		}
	      }
	    },
	    {
	      label = "Body",
	      groupdata = {
		{
		  index = 197,
		  setting= "0",
		  norigins = 1,
		  label = "I 2 3",
		  hall = " I 2 2 3\0"
		},
		{
		  index = 199,
		  setting= "0",
		  norigins = 1,
		  label = "I 21 3",
		  hall = " I 2b 2c 3\0"
		},
		{
		  index = 204,
		  setting= "0",
		  norigins = 1,
		  label = "I m -3",
		  hall = "-I 2 2 3\0"
		},
		{
		  index = 206,
		  setting= "0",
		  norigins = 1,
		  label = "I a -3",
		  hall = "-I 2b 2c 3\0"
		},
		{
		  index = 211,
		  setting= "0",
		  norigins = 1,
		  label = "I 4 3 2",
		  hall = " I 4 2 3\0"
		},
		{
		  index = 214,
		  setting= "0",
		  norigins = 1,
		  label = "I 41 3 2",
		  hall = " I 4bd 2c 3\0"
		},
		{
		  index = 217,
		  setting= "0",
		  norigins = 1,
		  label = "I -4 3 m",
		  hall = " I -4 2 3\0"
		},
		{
		  index = 220,
		  setting= "0",
		  norigins = 1,
		  label = "I -4 3 d",
		  hall = " I -4bd 2c 3\0"
		},
		{
		  index = 229,
		  setting= "0",
		  norigins = 1,
		  label = "I m -3 m",
		  hall = "-I 4 2 3\0"
		},
		{
		  index = 230,
		  setting= "0",
		  norigins = 1,
		  label = "I a -3 d",
		  hall = "-I 4bd 2c 3\0"
		}
	      }
	    }
	  }
	}
      }
    }
  };
  macro symmetryUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=165.,NEy=44.>;
    link UIparent<NEportLevels={2,1},NEx=715.,NEy=44.>;
    link model_type<NEportLevels={2,1},NEx=660.,NEy=187.>;
    link panel_number<NEportLevels={2,1},NEx=429.,NEy=44.>;
    link lattice<NEportLevels={1,1},NEx=429.,NEy=253.>;
    link centre<NEportLevels={1,1},NEx=616.,NEy=253.>;
    link group<NEportLevels={2,1},NEx=803.,NEy=253.>;
    link setting<NEportLevels={2,1},NEx=836.,NEy=187.>;
    UIpanel UIpanel<NEx=176.,NEy=154.> {
      visible => (<-.panel_number == 1);
      parent => <-.UIparent;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    groups groups<NEx=187.,NEy=253.>;
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox lattice_type<NEx=286.,NEy=352.> {
      parent => <-.UIpanel;
      labels => <-.groups[<-.model_type].lattice.label;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      title = "Lattice type";
      width => parent.clientWidth;
      visible => (array_size(<-.groups[<-.model_type].lattice) > 1);
      UIradioBox {
	orientation = "horizontal";
#ifdef WIN32
	height => UIdata.UIfonts[0].lineHeight + 3;
	itemWidth = 100;
#else
	height => UIdata.UIfonts[0].lineHeight;
#endif // WIN32
      };
      y = 0;
      selectedItem => <-.lattice;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox centre_type<NEx=286.,NEy=418.> {
      parent => <-.UIpanel;
      labels => <-.groups[<-.model_type].lattice[<-.lattice].centres.label;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      title = "Centre type";
      width => parent.clientWidth;
      UIradioBox {
	orientation = "horizontal";
#ifdef WIN32
	height => UIdata.UIfonts[0].lineHeight + 3;
	itemWidth = 100;
#else
	height => UIdata.UIfonts[0].lineHeight;
#endif // WIN32
      };
      visible =>
	(array_size(<-.groups[<-.model_type].lattice[<-.lattice].centres) > 1);
      y => (<-.lattice_type.y + <-.lattice_type.height + 5);
      selectedItem => <-.centre;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist select_group<NEx=286.,NEy=495.> {
      parent => <-.UIpanel;
      y => (<-.centre_type.y + <-.centre_type.height + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      selectedItem => <-.group;
      width => parent.clientWidth;
      strings => (<-.groups[<-.model_type].lattice[<-.lattice].centres[<-.centre].groupdata.label + " (" + <-.groups[<-.model_type].lattice[<-.lattice].centres[<-.centre].groupdata.index + ")");
    };
    string default_setting<NEportLevels=1,NEx=33.,NEy=429.>[] = {
      "Default Origin Setting"
    };
    string choose_setting<NEportLevels=1,NEx=33.,NEy=495.>[] = {
      "2nd Setting",
      "1st Setting"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox origin<NEx=286.,NEy=561.> {
      parent => <-.UIpanel;
      labels => switch(<-.groups[<-.model_type].lattice[<-.lattice].centres[<-.centre].groupdata[<-.group].norigins,<-.default_setting, <-.choose_setting);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      title = "Origin setting";
      width => parent.clientWidth;
      UIradioBox {
	orientation = "horizontal";
#ifdef WIN32
	height => UIdata.UIfonts[0].lineHeight + 3;
	itemWidth = 100;
#else
	height => UIdata.UIfonts[0].lineHeight;
#endif // WIN32
      };
      visible => (<-.groups[<-.model_type].lattice[<-.lattice].centres[<-.centre].groupdata[<-.group].norigins > 1);
      y => (<-.select_group.y + <-.select_group.height + 5);
      selectedItem => <-.setting;
    };
  };
  macro latticeUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=165.,NEy=44.>;
    link UIparent<NEportLevels={2,1},NEx=715.,NEy=44.>;
    link model_type<NEportLevels={2,1},NEx=660.,NEy=187.>;
    link panel_number<NEportLevels={2,1},NEx=429.,NEy=44.>;
    link lattice<NEportLevels={2,1},NEx=836.,NEy=187.>;
    link centre<NEportLevels=1,NEx=660.,NEy=242.>;
    link group<NEportLevels={2,1},NEx=836.,NEy=242.>;
    mlink param_mask<NEportLevels=1,NEx=374.,NEy=220.> =>
      switch((model_type == 3 && lattice == 1)+1,
	     masks[model_type].lattice[lattice].centre[centre].mask,
	     mono_masks[centre].groups[group].mask);
    group masks<NEportLevels=1,NEx=418.,NEy=132.>[] {
      group lattice[] {
	group centre[] {
	  boolean mask[6];
	};
      };
    } = {
      {
	lattice = {
	  {
	    centre = {
	      {
		mask = { 0, 0, 0, 0, 0, 0 }
	      }
	    }
	  }
	}
      },
      {
	lattice = {
	  {
	    centre = {
	      {
		mask = { 1, 0, 0, 0, 0, 0 }
	      }
	    }
	  }
	}
      },
      {
	lattice = {
	  {
	    centre = {
	      {
		mask = { 1, 1, 0, 0, 0, 1 }
	      }
	    }
	  },
	  {
	    centre = {
	      {
		mask = { 1, 1, 0, 0, 0, 0 }
	      },
	      {
		mask = { 1, 1, 0, 0, 0, 0 }
	      }
	    }
	  },
	  {
	    centre = {
	      {
		mask = { 1, 0, 0, 0, 0, 0 }
	      }
	    }
	  },
	  {
	    centre = {
	      {
		mask = { 1, 0, 0, 0, 0, 0 }
	      }
	    }
	  },
	  {
	    centre = {
	      {
		mask = { 1, 0, 0, 0, 0, 0 }
	      }
	    }
	  }
	}
      },
      {
	lattice = {
	  {
	    centre = {
	      {
		mask = { 1, 1, 1, 1, 1, 1 }
	      }
	    }
	  },
	  { // Not used - bypassed by switch() for mono_mask
	    centre = {
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      }
	    }
	  },
	  {
	    centre = {
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 1, 1, 0, 0, 0 }
	      }
	    }
	  },
	  {
	    centre = {
	      {
		mask = { 1, 0, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 0, 1, 0, 0, 0 }
	      }
	    }
	  },
	  {
	    centre = {
	      {
		mask = { 1, 0, 1, 0, 0, 0 }
	      },
	      {
		mask = { 1, 0, 0, 1, 0, 0 }
	      }
	    }
	  },
	  {
	    centre = {
	      {
		mask = { 1, 0, 1, 0, 0, 0 }
	      }
	    }
	  },
	  {
	    centre = {
	      {
		mask = { 1, 0, 0, 0, 0, 0 }
	      },
	      {
		mask = { 1, 0, 0, 0, 0, 0 }
	      },
	      {
		mask = { 1, 0, 0, 0, 0, 0 }
	      }
	    }
	  }
	}
      }
    };
    group mono_masks<NEx=660.,NEy=132.>[] {
      group groups[] {
	boolean mask[6];
      };
    } = {
      { // primitive
	groups = {
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  }
	}
      },
      { // a_face
	groups = {
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  }
	}
      },
      { // b_face
	groups = {
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  }
	}
      },
      { // c_face
	groups = {
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  }
	}
      },
      { // body
	groups = {
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 1, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 0, 0, 1 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  },
	  {
	    mask = { 1, 1, 1, 1, 0, 0 }
	  }
	}
      }
    };
    UIpanel UIpanel<NEx=176.,NEy=154.> {
      visible => (<-.panel_number == 2);
      parent => <-.UIparent;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein param_a<NEx=242.,NEy=286.> {
      UIparent => <-.UIpanel;
      x = 5;
      y = 0;
      flabel => "a";
      width => (UIparent.clientWidth / 2) - 10;
      fmin = 0.1;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      panel {
	visible => <-.<-.param_mask[0];
      };
      field {
	decimalPoints = 5;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein param_b<NEx=242.,NEy=352.> {
      UIparent => <-.UIpanel;
      x = 5;
      y => (<-.param_a.y + <-.param_a.height + 5);
      flabel => "b";
      width => (UIparent.clientWidth / 2) - 10;
      fmin = 0.1;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      panel {
	visible => <-.<-.param_mask[1];
      };
      field {
	decimalPoints = 5;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein param_c<NEx=242.,NEy=429.> {
      UIparent => <-.UIpanel;
      x = 5;
      y => (<-.param_b.y + <-.param_b.height + 5);
      flabel => "c";
      width => (UIparent.clientWidth / 2) - 10;
      fmin = 0.1;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      panel {
	visible => <-.<-.param_mask[2];
      };
      field {
	decimalPoints = 5;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein param_alpha<NEx=528.,NEy=286.> {
      UIparent => <-.UIpanel;
      x => (UIparent.clientWidth / 2 + 5);
      y = 0;
      flabel => "alpha";
      width => (UIparent.clientWidth / 2) - 10;
      fmin = 0.0;
      fmax = 180.0;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      panel {
	visible => <-.<-.param_mask[3];
      };
      field {
	decimalPoints = 3;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein param_beta<NEx=528.,NEy=352.> {
      UIparent => <-.UIpanel;
      x => (UIparent.clientWidth / 2 + 5);
      y => <-.param_b.y;
      flabel => "beta";
      width => (UIparent.clientWidth / 2) - 10;
      fmin = 0.0;
      fmax = 180.0;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      panel {
	visible => <-.<-.param_mask[4];
      };
      field {
	decimalPoints = 3;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein param_gamma<NEx=528.,NEy=429.> {
      UIparent => <-.UIpanel;
      x => (UIparent.clientWidth / 2 + 5);
      y => <-.param_c.y;
      flabel => "gamma";
      width => (UIparent.clientWidth / 2) - 10;
      fmin = 0.0;
      fmax = 180.0;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      panel {
	visible => <-.<-.param_mask[5];
      };
      field {
	decimalPoints = 3;
      };
    };
  };
  macro atomUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=165.,NEy=44.>;
    link UIparent<NEportLevels={2,1},NEx=715.,NEy=44.>;
    link model_type<NEportLevels={2,1},NEx=396.,NEy=154.>;
    link panel_number<NEportLevels={2,1},NEx=429.,NEy=44.>;
    link atom<NEportLevels=1,NEx=594.,NEy=154.>;
    link charge<NEportLevels=1,NEx=759.,NEy=154.>;
    link radius<NEportLevels=1,NEx=913.,NEy=154.>;
    link x<NEportLevels=1,NEx=495.,NEy=209.>;
    link y<NEportLevels=1,NEx=660.,NEy=209.>;
    link z<NEportLevels=1,NEx=825.,NEy=209.>;
    link use_charge<NEportLevels=1,NEx=517.,NEy=99.>;
    link use_radius<NEportLevels=1,NEx=704.,NEy=99.>;
    UIpanel UIpanel<NEx=176.,NEy=154.> {
      visible => (<-.panel_number > 2);
      parent => <-.UIparent;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.File_Ops.periodic_table periodic_table<NEx=253.,
                                                               NEy=286.> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.UIpanel;
	x => (parent.clientWidth - width) / 2;
	y = 0;
      };
      atom<NEportLevels={2,1}> => <-.atom;
    };
    CCP3.Core_Modules.Model.atom_defaults atom_defaults<NEx=605.,NEy=286.> {
      trigger => <-.atom;
      charge => <-.charge;
      radius => <-.radius;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle need_charge<NEx=66.,NEy=363.> {
      parent => <-.UIpanel;
      x = 0;
      y => <-.set_charge.y;
      label = "set charge";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.use_charge;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider set_charge<NEx=253.,NEy=363.> {
      parent => <-.UIpanel;
      value => <-.charge;
      min = -8;
      max = 8;
      title = "charge";
      x => (<-.need_charge.width + 5);
      y => (<-.periodic_table.UIpanel.y + <-.periodic_table.UIpanel.height +5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.use_charge;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle need_radius<NEx=66.,NEy=440.> {
      parent => <-.UIpanel;
      x = 0;
      y => <-.set_radius.y;
      label = "set radius";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.use_radius;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider set_radius<NEx=253.,NEy=440.> {
      parent => <-.UIpanel;
      value => <-.radius;
      min = 0.02;
      max = 3.0;
      title = "radius";
      x => (<-.need_radius.width + 5);
      y => (<-.set_charge.y + <-.set_charge.height + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.use_radius;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_x<NEx=253.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.x;
      flabel => "x";
      x = 0;
      y => (<-.set_radius.y + <-.set_radius.height + 5);
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_y<NEx=451.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.y;
      flabel => "y";
      x => (UIparent.clientWidth / 3);
      y => <-.set_x.y;
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_z<NEx=649.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.z;
      flabel => "z";
      x => (2 * UIparent.clientWidth / 3);
      y => <-.set_x.y;
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
    };
  };
  macro createUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=165.,NEy=44.>;
    link UIparent<NEportLevels={2,1},NEx=715.,NEy=44.>;
    link dialog_visible<NEportLevels={2,1},NEx=451.,NEy=44.>;
    int index<NEportLevels=1,NEx=814.,NEy=165.>;
    // Todo - these can just become model_data.model_type with polymers.
    int model_choice<NEportLevels=1,NEx=407.,NEy=297.> = 2;
    int model_list<NEportLevels=1,NEx=539.,NEy=231.>[] = {0,2,3};
    int name_ok => (strlen(model_data.name) > 0);
    CCP3.Core_Modules.Model.model_data model_data<NEportLevels={0,1},
                                                  NEx=330.,NEy=231.> {
      model_type<NEportLevels={0,2}> => <-.model_list[<-.model_choice];
      new_view = 0;
      group => <-.symmetryUI.groups[.model_type].lattice[.lattice].centres[.centre].groupdata[.group_id].label;
      norigins => <-.symmetryUI.groups[.model_type].lattice[.lattice].centres[.centre].groupdata[.group_id].norigins;
      name = "";
    };
    CCP3.Core_Modules.Model.atom_data atom_data<NEportLevels=1,
      NEx=781.,NEy=231.> {
      set_charge = 0;
      set_radius = 0;
      x = 0.0;
      y = 0.0;
      z = 0.0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVdialog DLVdialog<NEx=165.,NEy=121.> {
      parent => <-.UIparent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      title = "New Structure";
      ok = 0;
      okButton => (<-.index > 2);
      okLabelString = "Finish";
      cancel = 0;
      cancelButton = 1;
      visible => <-.dialog_visible;
      width = 720;
      height = 500;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=363.,NEy=165.> {
      parent => <-.DLVdialog;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIbutton Next<NEx=627.,NEy=165.> {
#ifdef MSDOS
      parent => <-.UIpanel;
      x = 5;
      y => parent.clientHeight - height - 5;
#else
      parent => <-.DLVdialog;
#endif // MSDOS
      label => switch ((<-.index > 2) + 1, "Next", "Add Atom");
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => ((<-.index == 0 && <-.name_ok) ||
		  (<-.index == 1 && is_valid(<-.model_data.group_id)) ||
		  (<-.index > 1));
      //x => (parent.clientWidth - width) / 2;
    };
    CCP3.Core_Macros.UI.File_Ops.typeUI typeUI<NEx=176.,NEy=385.> {
      UIparent => <-.UIpanel;
      preferences => <-.preferences;
      panel_number => <-.index;
      model_choice => <-.model_choice;
      name => <-.model_data.name;
      //model_choices {
      //visible => <-.<-.name_ok;
      //};
      DLVtoggle {
	//visible => <-.<-.name_ok;
	set => <-.<-.model_data.new_view;
      };
    };
    CCP3.Core_Macros.UI.File_Ops.symmetryUI symmetryUI<NEx=352.,NEy=385.> {
      preferences => <-.preferences;
      UIparent => <-.UIpanel;
      model_type => <-.model_data.model_type;
      panel_number => <-.index;
      lattice => <-.model_data.lattice;
      centre => <-.model_data.centre;
      group => <-.model_data.group_id;
      setting => <-.model_data.setting;
    };
    CCP3.Core_Macros.UI.File_Ops.latticeUI latticeUI<NEx=528.,NEy=385.> {
      preferences => <-.preferences;
      UIparent => <-.UIpanel;
      model_type => <-.model_data.model_type;
      panel_number => <-.index;
      lattice => <-.model_data.lattice;
      centre => <-.model_data.centre;
      group => <-.model_data.group_id;
      param_a {
	fval => <-.<-.model_data.a;
      };
      param_b {
	fval => <-.<-.model_data.b;
      };
      param_c {
	fval => <-.<-.model_data.c;
      };
      param_alpha {
	fval => <-.<-.model_data.alpha;
      };
      param_beta {
	fval => <-.<-.model_data.beta;
      };
      param_gamma {
	fval => <-.<-.model_data.gamma;
      };
    };
    CCP3.Core_Macros.UI.File_Ops.atomUI atomUI<NEx=704.,NEy=385.> {
      UIparent => <-.UIpanel;
      preferences => <-.preferences;
      panel_number => <-.index;
      model_type => <-.model_data.model_type;
      atom => <-.atom_data.atomic_number;
      charge => <-.atom_data.charge;
      radius => <-.atom_data.radius;
      x => <-.atom_data.x;
      y => <-.atom_data.y;
      z => <-.atom_data.z;
      use_charge => <-.atom_data.set_charge;
      use_radius => <-.atom_data.set_radius;
    };
    CCP3.Core_Modules.Model.create create<NEx=418.,NEy=495.> {
      trigger => <-.Next.do;
      index => <-.index;
      model => <-.model_data;
      atom => <-.atom_data;
    };
    GMOD.parse_v initialise<NEx=55.,NEy=231.> {
      v_commands = "index = 0; model_data.name = \"\"; model_data.new_view = 0; model_data.lattice = 0; model_data.centre = 0; model_data.setting = 0; model_data.a = 10.0; model_data.b = 10.0; model_data.c = 10.0; model_data.alpha = 0.0; model_data.beta = 0.0; model_data.gamma = 0.0; atom_data.x = 0.0; atom_data.y = 0.0; atom_data.z = 0.0; atom_data.atomic_number = ;";
      relative => <-;
    };
  };
  macro Create_Model {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=198.,NEy=55.>;
    link active_menu<NEportLevels={2,1},NEx=440.,NEy=55.>;
    int dialog_visible<NEportLevels=1,NEx=671.,NEy=55.> = 0;
    link UIparent<NEportLevels={2,1},NEx=858.,NEy=55.>;
    int bond_all<NEportLevels=1,NEx=855.,NEy=126.>;
    int ok<NEportLevels=1,NEx=231.,NEy=209.> = 0;
    int cancel<NEportLevels=1,NEx=528.,NEy=220.> = 0;
    link model_type;
    UIcmd UIcmd<NEx=198.,NEy=121.> {
      label = "New Structure";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => <-.active_menu;
      do => <-.dialog_visible;
    };
    GMOD.instancer instancer<NEx=517.,NEy=165.> {
      Value => <-.dialog_visible;
      Group => createUI;
    };
    createUI createUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      dialog_visible => <-.dialog_visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    CCP3.Core_Modules.Model.complete_model complete_model<NEx=275.,NEy=550.> {
      trigger => <-.ok;
      bond_all => <-.bond_all;
    };
    CCP3.Core_Modules.Model.cancel_model cancel_model<NEx=605.,NEy=550.> {
      trigger => <-.cancel;
      index => <-.createUI.index;
      new_view => <-.createUI.model_data.new_view;
      model_type => <-.model_type;
    };
  };
  macro loadUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
#ifdef USE_ESCDF
    string file_type_list<NEportLevels=1,NEx=55.,NEy=66.>[] = {
      "CIF", "XYZ", "XTL", "XR/CSSR", "ShelX", "PDB", "Gaussian CUBE",
      "XCrySDen XSF", "ESCDF"
    };
    string file_patterns<NEportLevels=1,NEx=55.,NEy=121.>[] = {
      "*.cif", "*.xyz", "*.xtl", "*.cssr", "*.res", "*.pdb", "*.cube",
      "*.xsf", "*.h5"
    };
#else
    string file_type_list<NEportLevels=1,NEx=55.,NEy=66.>[] = {
      "CIF", "XYZ", "XTL", "XR/CSSR", "ShelX", "PDB", "Gaussian CUBE",
      "XCrySDen XSF"
    };
    string file_patterns<NEportLevels=1,NEx=55.,NEy=121.>[] = {
      "*.cif", "*.xyz", "*.xtl", "*.cssr", "*.res", "*.pdb", "*.cube",
      "*.xsf"
    };
#endif // ESCDF
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 500;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      title = "Load Structure";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel UIlabel<NEx=660.,NEy=165.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y = 0;
      alignment = "center";
      width => parent.clientWidth;
      label = "Structure name";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=671.,NEy=242.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 5;
      y => (<-.UIlabel.y + <-.UIlabel.height + 5);
      width => (parent.clientWidth - 10);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox RadioButtons<NEx=418.,NEy=341.> {
      parent => <-.panel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      title = "File Type";
      labels => <-.file_type_list;
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
      width => parent.clientWidth;
      selectedItem => <-.data.filetype;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.panel;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}>;
      pattern<NEportLevels={2,0}>;
      parent1<NEportLevels={2,0}> => <-.panel;
      title = "Load Model filename";
      file_write = 0;
      x = 0;
      y => (<-.RadioButtons.y + <-.RadioButtons.height + 5);
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.preferences;
      filename<NEportLevels={2,0}>;
      pattern<NEportLevels={2,0}> => <-.file_patterns[<-.data.filetype];
      parent => <-.panel;
      title = "Load Structure filename";
      x = 0;
      y => (<-.RadioButtons.y + <-.RadioButtons.height + 5);
    };
    CCP3.Core_Modules.Model.LoadStructData &data<NEx=814.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLViSlider pdb_models<NEx=220.,NEy=495.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 1;
      value => <-.data.pdb_model;
      visible => (<-.data.filetype == 5);
      width => parent.clientWidth;
      y => (<-.new_view.y + <-.new_view.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider pdb_conformers<NEx=220.,NEy=572.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 1;
      value => <-.data.pdb_conformer;
      visible => (<-.data.filetype == 5);
      width => parent.clientWidth;
      y => (<-.pdb_models.y + <-.pdb_models.height + 5);
    };
    /* Todo? needs, to know if a model already exists
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle cube_model {
      parent => <-.panel;
      x = 0;
      y => <-.pdb_models.y;
      width => parent.clientWidth;
      label = "Also Load Model";
      set => <-.data.cube_model;
      visible => (<-.data.filetype == 6);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    */
    GMOD.parse_v reset_params<NEx=583.,NEy=77.>;
  };
  macro Load_Model {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=143.,NEy=11.>;
    link active_menu<NEportLevels={2,1},NEx=319.,NEy=22.>;
    int visible<NEportLevels=1,NEx=572.,NEy=22.> = 0;
    int bond_all<NEportLevels=1,NEx=756.,NEy=117.>;
    link UIparent<NEportLevels={2,1},NEx=847.,NEy=33.>;
    CCP3.Core_Modules.Model.LoadStructData data<NEx=473.,NEy=121.> {
      name = "";
      filename = "";
      filetype = 0;
      type_string => <-.load.file_type_list[filetype];
      new_view = 0;
      pdb_model = 1;
      pdb_conformer = 1;
      cube_model = 1;
    };
    UIcmd load_menu<NEportLevels={0,2},NEx=143.,NEy=66.> {
      label = "Load Structure";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => <-.active_menu;
      do => <-.visible;
    };
    GMOD.instancer instancer<NEx=308.,NEy=176.> {
      Value => <-.visible;
      Group => load;
    };
    loadUI load<NEx=330.,NEy=286.,instanced=0,NEportLevels={0,1}> {
      data => <-.data;
      FileB {
	filename => <-.data.filename;
	//pattern => <-.file_patterns[<-.data.file_type];
      };
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      DLVtext {
	text => <-.data.name;
      };
      RadioButtons {
	selectedItem => <-.data.filetype;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
      new_view {
	set => <-.data.new_view;
      };
      pdb_models {
	max => <-.<-.nmodels;
      };
      pdb_conformers {
	max => <-.<-.nconformers;
      };
      reset_params {
	v_commands = "new_view = 0; name = \"\";";
	relative => <-.<-.data;
      };
    };
    int nmodels<NlEportLevels=1,NEx=33.,NEy=319.> = 1;
    int nconformers<NEportLevels=1,NEx=209.,NEy=319.> = 1;
    CCP3.Core_Modules.Model.check_pdb check_pdb<NEx=110.,NEy=385.> {
      filename => <-.data.filename;
      file_type => <-.data.filetype;
      model => <-.data.pdb_model;
      nmodels => <-.nmodels;
      nconformers => <-.nconformers;
    };
    CCP3.Core_Modules.Model.load load_model<NEx=605.,NEy=286.> {
      run => <-.load.dialog.ok;
      data => <-.data;
      nconformers => <-.nconformers;
      bond_all => <-.bond_all;
    };
  };
  macro saveUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
#ifdef USE_ESCDF
    string file_type_list<NEportLevels=1,NEx=55.,NEy=66.>[] = {
      "XYZ", "CIF", "ESCDF"
    };
    string file_patterns<NEportLevels=1,NEx=55.,NEy=121.>[] = {
      "*.xyz", "*.cif", "*.h5"
    };
#else
    string file_type_list<NEportLevels=1,NEx=55.,NEy=66.>[] = {
      "XYZ", "CIF"
    };
    string file_patterns<NEportLevels=1,NEx=55.,NEy=121.>[] = {
      "*.xyz", "*.cif"
    };
#endif // ESCDF
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      title = "Export Structure";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox RadioButtons<NEx=418.,NEy=341.> {
      parent => <-.panel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      title = "File Type";
      labels => <-.file_type_list;
      x = 0;
      y = 5;
      width => parent.clientWidth;
      selectedItem => <-.data.filetype;
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.preferences;
      filename<NEportLevels={2,0}>;
      pattern<NEportLevels={2,0}> => <-.file_patterns[<-.data.filetype];
      parent => <-.panel;
      title = "Save Structure filename";
      x = 0;
      y => (<-.RadioButtons.y + <-.RadioButtons.height + 5);
    };
    CCP3.Core_Modules.Model.LoadStructData &data<NEx=814.,NEy=77.,
      NEportLevels={2,0}>;
  };
  macro Save_Model {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=143.,NEy=11.>;
    link active_menu<NEportLevels={2,1},NEx=319.,NEy=22.>;
    int visible<NEportLevels=1,NEx=572.,NEy=22.> = 0;
    link UIparent<NEportLevels={2,1},NEx=847.,NEy=33.>;
    CCP3.Core_Modules.Model.LoadStructData data<NEx=473.,NEy=121.> {
      name = "";
      filename = "";
      filetype = 0;
      type_string => <-.save.file_type_list[filetype];
      new_view = 0;
      pdb_model = 1;
      pdb_conformer = 1;
      cube_model = 1;
    };
    UIcmd save_menu<NEportLevels={0,2},NEx=143.,NEy=66.> {
      label = "Save Structure";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => <-.active_menu;
      do => <-.visible;
    };
    GMOD.instancer instancer<NEx=308.,NEy=176.> {
      Value => <-.visible;
      Group => save;
    };
    saveUI save<NEx=330.,NEy=286.,instanced=0,NEportLevels={0,1}> {
      data => <-.data;
      FileB {
	filename => <-.data.filename;
	//pattern => <-.file_patterns[<-.data.file_type];
      };
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      RadioButtons {
	selectedItem => <-.data.filetype;
      };
    };
    CCP3.Core_Modules.Model.save save_model<NEx=605.,NEy=286.> {
      run => <-.save.dialog.ok;
      data => <-.data;
    };
  };
  macro menu_file {
    link preferences<NEportLevels={2,1},NEx=209.,NEy=66.>;
    int active_menus<NEportLevels={2,1},NEx=418.,NEy=66.>;
    int dialog_visible<NEportLevels=1,NEx=605.,NEy=66.> =>
      ((Load_Model.visible == 1) || (Create_Model.dialog_visible == 1) ||
       (Save_Model.visible == 1));
    link UIparent<NEportLevels={2,1},NEx=792.,NEy=66.>;
    link bond_all<NEportLevels=1,NEx=612.,NEy=117.>;
    link model_type;
    CCP3.Core_Macros.UI.File_Ops.Load_Model Load_Model<NEx=242.,NEy=176.> {
      preferences => <-.preferences;
      active_menu => <-.active_menus;
      bond_all => <-.bond_all;
      UIparent => <-.UIparent;
      load {
	preferences => <-.preferences;
      };
    };
    CCP3.Core_Macros.UI.File_Ops.Create_Model Create_Model<NEx=495.,NEy=176.> {
      preferences => <-.preferences;
      active_menu => <-.active_menus;
      bond_all => <-.bond_all;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.File_Ops.Save_Model Save_Model<NEx=243.,NEy=252.> {
      preferences => <-.preferences;
      active_menu => <-.active_menus;
      UIparent => <-.UIparent;
      save {
	preferences => <-.preferences;
      };
    };
  };
};
