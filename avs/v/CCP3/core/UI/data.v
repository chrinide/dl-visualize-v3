
flibrary Data<NEeditable=1> {
  macro DataUI {
    link preferences<NEportLevels={2,1},NEx=649.,NEy=77.>;
    link model_type<NEportLevels={2,1},NEx=297.,NEy=45.>;
    CCP3.Core_Macros.UI.ViewData.menu_display menu_display<NEx=429.,NEy=132.> {
      preferences => <-.preferences;
    };
    CCP3.Core_Macros.UI.EditData.menu_edit menu_edit<NEx=176.,NEy=132.> {
      preferences => <-.preferences;
      model_type => <-.model_type;
    };
  };
};
