
flibrary EditData<NEeditable=1> {
  macro EnergyUI<NEx=441.,NEy=144.> {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=342.,NEy=72.>;
    link parent<NEportLevels={2,1},NEx=99.,NEy=54.>;
    CCP3.Renderers.Edits.plot_params &params<NEx=576.,NEy=72.>;
    int visible;
    UIpanel UIpanel<NEx=216.,NEy=189.> {
      parent => <-.parent;
      y = 0;
      visible => <-.visible;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => (<-.DLVlist.y + <-.DLVlist.height + 5);
    };
    CCP3.Core_Modules.EditData.related_properties listobj<NEx=756.,NEy=207.> {
      trust_user => <-.params.use_property; // force notify
      ptype = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein Shift<NEx=387.,NEy=306.> {
      UIparent => <-.UIpanel;
      y = 5;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.params.shift;
      flabel => "Shift";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=648.,NEy=306.> {
      parent => <-.UIpanel;
      y => (<-.Shift.y + <-.Shift.height + 5);
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Import shift from data";
      set => <-.params.use_property;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist DLVlist<NEx=648.,NEy=405.> {
      parent => <-.UIpanel;
      y => (<-.DLVtoggle.y + <-.DLVtoggle.height + 5);
      selectedItem => <-.params.object;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      visible => <-.params.use_property;
      strings => <-.listobj.labels;
    };
    CCP3.Core_Modules.EditData.update_reals update_reals<NEx=423.,NEy=513.> {
      shift => <-.params.shift;
      selection => <-.params.object;
      use_prop => <-.params.use_property;
    };
  };
  macro OrthoSliceUI {
    ilink in_fld;
    DV_Param_ortho_slice &param<NEportLevels={2,0}>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.plane_slider.height + <-.plane_slider.y + 5);
    };
    string axes3D[] = { "x", "y", "z" };
    string axes2D[] = { "x", "y" };
    UIlabel axis_label {
      y = 5;
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      label = "axis";
    };
    //CCP3.Core_Macros.UI.UIobjs.DLVradioBox axis {
    CCP3.Core_Macros.UI.UIobjs.DLVlist axis {
      y => ((<-.axis_label.y + <-.axis_label.height) + 4);
      parent => <-.UIpanel;
      //labels+nres => switch (((in_fld.ndim == 2) + 1), axes3D, axes2D);
      strings+nres => switch (((in_fld.ndim == 2) + 1), axes3D, axes2D);
      selectedItem<NEportLevels={2,0}> => param.axis;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (3 * UIdata.UIfonts[0].lineHeight);
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider plane_slider {
      y => ((<-.axis.y + <-.axis.height) + 4);
      parent => <-.UIpanel;
      max+nres<NEportLevels={2,0}> => (in_fld.dims[param.axis] - 1);
      value<NEportLevels={2,0}> => param.plane;
      title => "plane";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
  };
  macro ExtensionUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.c_slider.height + <-.c_slider.y + 5);
    };
    int model_type;
    CCP3.Renderers.Edits.ExtendParams &params;
    CCP3.Core_Macros.UI.UIobjs.DLViSlider a_slider<NEx=209.,NEy=110.> {
      parent => <-.UIpanel;
      max+nres => <-.params.max_a;
      min+nres => <-.params.min_a;
      value => params.a;
      title => "a";
      visible => (<-.model_type > 0);
      y = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider b_slider<NEx=220.,NEy=176.> {
      parent => <-.UIpanel;
      y => ((<-.a_slider.y + <-.a_slider.height) + 5);
      max+nres => <-.params.max_b;
      min+nres => <-.params.min_b;
      value => params.b;
      title => "b";
      visible => (<-.model_type > 1);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider c_slider<NEx=220.,NEy=231.> {
      parent => <-.UIpanel;
      y => ((<-.b_slider.y + <-.b_slider.height) + 5);
      max+nres => <-.params.max_c;
      min+nres => <-.params.min_c;
      value => params.c;
      title => "c";
      visible => (<-.model_type == 3) && (params.dim == 3);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
    CCP3.Core_Modules.EditData.update_extension update<NEx=441.,NEy=405.> {
      a => params.a;
      b => params.b;
      c => params.c;
    };
  };
  macro SliceUI {
    ilink in_fld;
    CCP3.Renderers.Edits.SliceParam &param<NEportLevels={2,0}>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    string planes[];
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.plane_dist.y + <-.plane_dist.height + 5);
    };
    CCP3.Core_Modules.EditData.update_slice update<NEx=423.,NEy=450.> {
      plane => <-.param.plane;
    };
    DVnode_data_labels DVnode_data_labels {
      in => in_fld;
      int+nres ncomp => in.nnode_data;
    };
    DVcell_data_labels DVcell_data_labels {
      in => in_fld;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist slice<NEx=231.,NEy=264.> {
      parent => <-.UIpanel;
      y = 5;
      selectedItem => <-.param.plane;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      strings => <-.planes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider plane_dist {
      y => ((<-.slice.y + <-.slice.height) + 5);
      width => ((<-.UIpanel.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      min+nres<NEportLevels={2,0}> =>
	cache((-0.5 * magnitude((in_fld.coordinates.max_vec
				 - in_fld.coordinates.min_vec))));
      max+nres<NEportLevels={2,0}> =>
	cache((0.5 * magnitude((in_fld.coordinates.max_vec
				- in_fld.coordinates.min_vec))));
      value<NEportLevels={2,0}> => param.dist;
      title => "plane distance";
      visible => (array_size(<-.slice.strings) > 0);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein plane_dist_typein {
      slider => plane_dist;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
  };
  macro ClampUI {
    ilink in_fld<NEy=143,NEx=66>;
    DV_Param_clamp &param<NEportLevels={2,0},NEy=33,NEx=253>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.reset_toggle.y + <-.reset_toggle.height + 5);
    };
    //DVnode_data_labels DVnode_data_labels<NEy=209,NEx=88> {
    //  in => in_fld;
    //  int+nres ncomp => in.nnode_data;
    //};
    /*
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox UIradioBoxLabel {
      parent => <-.UIpanel;
      labels<NEportLevels={2,0}> => DVnode_data_labels.labels;
      &selectedItem => param.vector;
      title => "data component";
      y = 5;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    */
    CCP3.Core_Macros.UI.UIobjs.DLViSlider vec_comp<NEy=176,NEx=253> {
      //y => ((<-.UIradioBoxLabel.y + <-.UIradioBoxLabel.height) + 4);
      y = 5;
      visible+nres => min_array(val_array);
      parent => <-.UIpanel;
      max+nres => max_array(val_array);
      value<NEportLevels={2,0}> => param.component;
      title => "vector sub-component";
      int+nres val_array[2] => {1,(in_fld.node_data[param.vector].veclen - 1)};
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle below_toggle<NEy=242,NEx=253> {
      y => ((<-.vec_comp.y + <-.vec_comp.height) + 6);
      parent => <-.UIpanel;
      label => "Clamp below min value";
      set<NEportLevels={2,0}> => param.below;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider min_slider<NEy=308,NEx=253> {
      y => ((<-.below_toggle.y + <-.below_toggle.height) + 4);
      parent => <-.UIpanel;
      min+nres =>
	cache(in_fld.node_data[param.vector].min_vec[param.component]);
      max+nres =>
	cache(in_fld.node_data[param.vector].max_vec[param.component]);
      value<NEportLevels={2,0}> => param.min_value;
      title => "min value";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein min_slider_typein {
      slider => min_slider;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle above_toggle<NEy=209,NEx=253> {
      y => ((<-.min_slider.y + <-.min_slider.height) + 6);
      width => <-.UIpanel.clientWidth;
      parent => <-.UIpanel;
      label => "Clamp above max value";
      set<NEportLevels={2,0}> => param.above;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider max_slider<NEy=341,NEx=253> {
      y => ((<-.above_toggle.y + <-.above_toggle.height) + 4);
      parent => <-.UIpanel;
      min+nres =>
	cache(in_fld.node_data[param.vector].min_vec[param.component]);
      max+nres =>
	cache(in_fld.node_data[param.vector].max_vec[param.component]);
      value<NEportLevels={2,0}> => param.max_value;
      title => "max value";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein max_slider_typein {
      slider => max_slider;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle reset_toggle<NEy=275,NEx=253> {
      y => ((<-.max_slider.y + <-.max_slider.height) + 6);
      parent => <-.UIpanel;
      label => "Reset min-max";
      set<NEportLevels={2,0}> => param.reset_minmax;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
  };
  macro DownsizeUI {
    ilink in_fld<NEy=110,NEx=66>;
    DV_Param_downsize &param<NEportLevels={2,0},NEy=11,NEx=275>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    UIpanel panel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.factor2_slider.y + <-.factor2_slider.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle int_toggle {
      y = 5;
      width => <-.panel.clientWidth;
      parent => <-.panel;
      label => "Integer Sliders";
      set<NEportLevels={2,0}> = 1;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider factor0_slider<NEy=220,NEx=275> {
      y => ((<-.int_toggle.y + <-.int_toggle.height) + 4);
      width => <-.panel.clientWidth;
      parent => <-.panel;
      min => <-.int_toggle.set;
      max = 12.;
      value<NEportLevels={2,0}> => param.factor0;
      mode => <-.int_toggle.set;
      title => "I downsize factor";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider factor1_slider<NEy=253,NEx=275> {
      y => ((<-.factor0_slider.y + <-.factor0_slider.height) + 4);
      width => <-.panel.clientWidth;
      visible+nres => (panel.visible & (in_fld.ndim > 1));
      parent => <-.panel;
      min => <-.int_toggle.set;
      max = 12.;
      value<NEportLevels={2,0}> => param.factor1;
      mode => <-.int_toggle.set;
      title => "J downsize factor";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider factor2_slider<NEy=286,NEx=275> {
      y => ((<-.factor1_slider.y + <-.factor1_slider.height) + 4);
      width => <-.panel.clientWidth;
      visible+nres => (panel.visible & (in_fld.ndim > 2));
      parent => <-.panel;
      min => <-.int_toggle.set;
      max = 12.;
      value<NEportLevels={2,0}> => param.factor2;
      mode => <-.int_toggle.set;
      title => "K downsize factor";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
  };
  macro CropUI {
    ilink in_fld<NEy=121,NEx=66>;
    DV_Param_crop &param<NEportLevels={2,0},NEx=264,NEy=22>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    UIpanel panel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.max2_slider.y + <-.max2_slider.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider min0_slider<NEy=198,NEx=253> {
      width => <-.panel.clientWidth;
      y = 5;
      parent => <-.panel;
      max+nres => (in_fld.dims[0] - 1);
      value+nres<NEportLevels={2,0}> => param.min[0];
      title => "I min";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider max0_slider<NEy=231,NEx=253> {
      y => ((<-.min0_slider.y + <-.min0_slider.height) + 4);
      width => <-.panel.clientWidth;
      parent => <-.panel;
      max+nres => (in_fld.dims[0] - 1);
      value+nres<NEportLevels={2,0}> => param.max[0];
      title => "I max";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider min1_slider<NEy=264,NEx=253> {
      y => ((<-.max0_slider.y + <-.max0_slider.height) + 4);
      width => <-.panel.clientWidth;
      visible+nres => (panel.visible & (in_fld.ndim > 1));
      parent => <-.panel;
      max+nres => (in_fld.dims[1] - 1);
      value+nres<NEportLevels={2,0}> => param.min[1];
      title => "J min";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider max1_slider<NEy=297,NEx=253> {
      y => ((<-.min1_slider.y + <-.min1_slider.height) + 4);
      width => <-.panel.clientWidth;
      visible+nres => (panel.visible & (in_fld.ndim > 1));
      parent => <-.panel;
      max+nres => (in_fld.dims[1] - 1);
      value+nres<NEportLevels={2,0}> => param.max[1];
      title => "J max";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider min2_slider<NEy=330,NEx=253> {
      y => ((<-.max1_slider.y + <-.max1_slider.height) + 4);
      width => <-.panel.clientWidth;
      visible+nres => (panel.visible & (in_fld.ndim > 2));
      parent => <-.panel;
      max+nres => (in_fld.dims[2] - 1);
      value+nres<NEportLevels={2,0}> => param.min[2];
      title => "K min";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider max2_slider<NEy=363,NEx=253> {
      y => ((<-.min2_slider.y + <-.min2_slider.height) + 4);
      width => <-.panel.clientWidth;
      visible+nres => (panel.visible & (in_fld.ndim > 2));
      parent => <-.panel;
      max+nres => (in_fld.dims[2] - 1);
      value+nres<NEportLevels={2,0}> => param.max[2];
      title => "K max";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
  };
  macro CutUI {
    ilink in_fld<NEy=121,NEx=66>;
    CCP3.Renderers.Edits.CutParam &param<NEportLevels={2,0}>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    string planes[];
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.plane_dist.y + <-.plane_dist.height + 5);
    };
    CCP3.Core_Modules.EditData.update_cut update<NEx=639.,NEy=423.> {
      box => <-.param.plane;
    };
    DVnode_data_labels DVnode_data_labels {
      in => in_fld;
      int+nres ncomp => in.nnode_data;
    };
    DVcell_data_labels DVcell_data_labels {
      in => in_fld;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist slice<NEx=231.,NEy=264.> {
      parent => <-.UIpanel;
      y = 5;
      selectedItem => <-.param.plane;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      strings => <-.planes;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle above_toggle {
      y => ((<-.slice.y + slice.height) + 5);
      width => <-.UIpanel.clientWidth;
      parent => <-.UIpanel;
      label => "Above";
      set<NEportLevels={2,0}> => param.above;
      visible => (array_size(<-.slice.strings) > 0);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider plane_dist {
      y => ((<-.above_toggle.y + <-.above_toggle.height) + 4);
      width => ((<-.UIpanel.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      min+nres<NEportLevels={2,0}> =>
	cache((-0.5 * magnitude((in_fld.coordinates.max_vec
				 - in_fld.coordinates.min_vec))));
      max+nres<NEportLevels={2,0}> =>
	cache((0.5 * magnitude((in_fld.coordinates.max_vec
				- in_fld.coordinates.min_vec))));
      value<NEportLevels={2,0}> => param.dist;
      title => "plane distance";
      visible => (array_size(<-.slice.strings) > 0);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein plane_dist_typein {
      slider => plane_dist;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
  };
  macro DataMathUI {
    //string oper;
    CCP3.Renderers.Edits.data_math_params &param;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.sel4.y + <-.sel4.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext Operation {
      y = 5;
      height = 34;
      parent => <-.UIpanel;
      &text<NEportLevels={2,0}> => <-.param.operation;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle UItoggle<NEx=143.,NEy=209.> {
      parent => <-.UIpanel;
      y => ((<-.Operation.y + <-.Operation.height) + 5);
      label = "Match Grid, not Coordinates";
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.param.trust_user;
    };
    CCP3.Core_Modules.EditData.related_properties listobj<NEx=621.,NEy=189.> {
      trust_user => <-.param.trust_user;
      ptype = 0;
    };
    string labels<NEportLevels=1>[] = {
      "data #2",
      "data #3",
      "data #4"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox UIradioBoxLabel<NEx=308.,NEy=220.> {
      parent => <-.UIpanel;
      labels => <-.labels;
      y => ((<-.UItoggle.y + <-.UItoggle.height) + 5);
      title => "Choose Data";
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      selectedItem = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist sel2<NEx=143.,NEy=374.> {
      parent => <-.UIpanel;
      strings => <-.listobj.labels;
      selectedItem => <-.param.selection2;
      visible => (<-.UIradioBoxLabel.selectedItem == 0);
      y => ((<-.UIradioBoxLabel.y + <-.UIradioBoxLabel.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist sel3<NEx=330.,NEy=374.> {
      parent => <-.UIpanel;
      strings => <-.listobj.labels;
      selectedItem => <-.param.selection3;
      visible => (<-.UIradioBoxLabel.selectedItem == 1);
      y => ((<-.UIradioBoxLabel.y + <-.UIradioBoxLabel.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist sel4<NEx=539.,NEy=374.> {
      parent => <-.UIpanel;
      strings => <-.listobj.labels;
      selectedItem => <-.param.selection4;
      visible => (<-.UIradioBoxLabel.selectedItem == 2);
      y => ((<-.UIradioBoxLabel.y + <-.UIradioBoxLabel.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
    /*
    GMOD.copy_on_change copy_on_change<NEx=66.,NEy=275.> {
      //Todoinput => <-.list.selection2;
    };
    GMOD.copy_on_change copy_on_change#1<NEx=341.,NEy=275.> {
      //Todoinput => <-.list.selection3;
    };
    GMOD.copy_on_change copy_on_change#2<NEx=627.,NEy=275.> {
      //Todoinput => <-.list.selection4;
    };
    int select2<NEportLevels=1,NEx=154.,NEy=319.> => .copy_on_change.output;
    int select3<NEportLevels=1,NEx=352.,NEy=319.> => .copy_on_change#1.output;
    int select4<NEportLevels=1,NEx=550.,NEy=319.> => .copy_on_change#2.output;
    */
    CCP3.Core_Modules.EditData.update_math select_math2 {
      selection => <-.param.selection2;
      trust_user => <-.param.trust_user;
      index = 0;
    };
    CCP3.Core_Modules.EditData.update_math select_math3 {
      selection => <-.param.selection3;
      trust_user => <-.param.trust_user;
      index = 1;
    };
    CCP3.Core_Modules.EditData.update_math select_math4 {
      selection => <-.param.selection4;
      trust_user => <-.param.trust_user;
      index = 2;
    };
  };
  macro VolRUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    int model_type;
  };
  macro SpectraUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height = 450;
      //height+nres => (<-.harmonics.y + <-.harmonics.height + 5);
    };
    CCP3.Core_Modules.EditData.spectral_data &params<NEx=616.,NEy=66.,
      NEportLevels={0,1}>;
    string spectra_types<NEportLevels=1,NEx=737.,NEy=231.>[] = {
      "Infra-Red Active",
      "Raman Active"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext info<NEx=517.,NEy=242.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      outputOnly = 1;
      text = "Only the line positions will be correct if\n you haven't"
	+ " calculated intensities";
      width => parent.clientWidth;
      rows = 2;
      y = 5;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox ir_raman<NEx=275.,NEy=275.> {
      parent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      labels => <-.spectra_types;
      selectedItem => <-.params.ir_or_raman;
      width => parent.clientWidth;
      y => (<-.info.y + <-.info.height + 5);
      title = "Spectrum Type";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider emin<NEx=275.,NEy=352.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 0.0;
      max => <-.params.emax;
      value => <-.params.emin;
      width => ((parent.clientWidth * 11) / 12);
      y => (<-.ir_raman.y + <-.ir_raman.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein emin_typein {
      slider => emin;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider emax<NEx=275.,NEy=418.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min => <-.params.emin;
      max = 5000.0;
      width => ((parent.clientWidth * 11) / 12);
      value => <-.params.emax;
      y => (<-.emin.y + <-.emin.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein emax_typein {
      slider => emax;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein points<NEx=275.,NEy=484.> {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      fmin = 10;
      fmax = 1000;
      fval => <-.params.npoints;
      width => UIparent.clientWidth;
      y => (<-.emax.y + <-.emax.height + 5);
      flabel = "Npoints";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider broadening<NEx=275.,NEy=550.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 0.0;
      max = 10.0;
      value => <-.params.broaden;
      width => parent.clientWidth;
      y => (<-.npoints.y + <-.npoints.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider harmonics<NEx=275.,NEy=605.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 0.0;
      max = 1.0;
      value => <-.params.harmonic_scale;
      width => parent.clientWidth;
      y => (<-.broadening.y + <-.broadening.height + 5);
    };
    CCP3.Core_Modules.EditData.update_spectrum update<NEx=605.,NEy=352.> {
      data => <-.params;
    };
  };
  macro PhononUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link parent<NEportLevels={2,1}>;
    int visible<NEportLevels=1>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      y = 0;
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height = 220;
    };
    CCP3.Core_Modules.EditData.phonon_data &params<NEx=616.,NEy=66.,
      NEportLevels={0,1}>;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein frames<NEx=275.,NEy=484.> {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      fmin = 2;
      fval => <-.params.nframes;
      width => UIparent.clientWidth;
      y = 5;
      flabel = "N Frames";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider scale<NEx=275.,NEy=550.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 0.01;
      max = 1.0;
      value => <-.params.magnitude;
      width => (11 * parent.clientWidth / 12);
      y => (<-.frames.y + <-.frames.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein emax_typein {
      slider => scale;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
#ifdef DLV_DL
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle toggle {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.use_temp;
      width => parent.clientWidth;
      label = "Apply temperature";
      y => (<-.scale.y + <-.scale.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider temp<NEx=275.,NEy=550.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 1;
      max = 1000;
      value => <-.params.temperature;
      width => (11 * parent.clientWidth / 12);
      y => (<-.toggle.y + <-.toggle.height + 5);
      title = "temperature";
      visible => <-.params.use_temp;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein temp_typein {
      slider => temp;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      visible => <-.params.use_temp;      
    };
#endif // DLV_DL
    CCP3.Core_Modules.EditData.update_phonon update<NEx=605.,NEy=352.> {
      data => <-.params;
    };
  };
  macro edit_panels {
    link model_type<NEportLevels=1,NEx=306.,NEy=99.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels=1,
      NEx=308.,NEy=44.>;
    UIframe UIframe<NEx=99.,NEy=121.> {
      parent<NEportLevels={3,0}>;
      width => parent.clientWidth;
    };
    UIbutton Delete<NEx=539.,NEy=275.> {
      parent => <-.UIframe;
      y = 5;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      //visible => is_valid(<-.edit_object);
      visible = 0; // Todo
    };
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow scroll<NEx=121.,NEy=344.> {
      parent => <-.UIframe;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      visible => is_valid(<-.<-.edit_object);
      // Todo y => (<-.Delete.y + <-.Delete.height + 5);
      y = 0;
      height => (parent.clientHeight - y);
    };
    int+nres type_info<NEportLevels=1,NEx=517.,NEy=44.>;
    int+nres obj_index;
    CCP3.Core_Macros.UI.EditData.OrthoSliceUI OrthoSliceUI<NEx=429.,NEy=275.,
      instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 0);
      parent => <-.scroll;
    };
    GMOD.instancer instance_ortho<NEx=429.,NEy=209.> {
      Value => (<-.type_info == 0);
      Group => OrthoSliceUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.ExtensionUI ExtensionUI<NEx=429.,NEy=385.,
      instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 1);
      parent => <-.scroll;
      model_type => <-.model_type;
      update {
	object => obj_index;
      };
    };
    GMOD.instancer instance_ext<NEx=429.,NEy=330.> {
      Value => (<-.type_info == 1);
      Group => ExtensionUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.SliceUI SliceUI<NEx=429.,NEy=495.,
      instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 3);
      parent => <-.scroll;
      update {
	object => obj_index;
      };
    };
    GMOD.instancer instance_slice<NEx=429.,NEy=440.> {
      Value => (<-.type_info == 3);
      Group => SliceUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.ClampUI ClampUI<NEx=429.,NEy=616.,
      instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 5);
      parent => <-.scroll;
    };
    GMOD.instancer instance_clamp<NEx=429.,NEy=561.> {
      Value => (<-.type_info == 5);
      Group => ClampUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.DownsizeUI DownsizeUI<NEx=660.,NEy=264.,
      instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 6);
      parent => <-.scroll;
    };
    GMOD.instancer instance_down<NEx=660.,NEy=209.> {
      Value => (<-.type_info == 6);
      Group => DownsizeUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.CropUI CropUI<NEx=671.,NEy=385.,instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 4);
      parent => <-.scroll;
    };
    GMOD.instancer instance_crop<NEx=671.,NEy=330.> {
      Value => (<-.type_info == 4);
      Group => CropUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.CutUI CutUI<NEx=671.,NEy=495.,instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 2);
      parent => <-.scroll;
      update {
	object => obj_index;
      };
    };
    GMOD.instancer instance_cut<NEx=671.,NEy=440.> {
      Value => (<-.type_info == 2);
      Group => CutUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.DataMathUI DataMathUI<NEx=671.,NEy=616.,
      instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 7);
      parent => <-.scroll;
      listobj {
	selection => obj_index;
      };
      select_math2 {
	object => obj_index;
      };
      select_math3 {
	object => obj_index;
      };
      select_math4 {
	object => obj_index;
      };
    };
    GMOD.instancer instance_math<NEx=671.,NEy=550.> {
      Value => (<-.type_info == 7);
      Group => DataMathUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.VolRUI VolRUI<NEx=869.,NEy=385.,instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 8);
      parent => <-.scroll;
    };
    GMOD.instancer instance_vol<NEx=869.,NEy=330.> {
      Value => (<-.type_info == 8);
      Group => VolRUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.PhononUI PhononUI<NEx=143.,NEy=451.,
      instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 9);
      parent => <-.scroll;
    };
    GMOD.instancer instance_phonon<NEx=143.,NEy=407.> {
      Value => (<-.type_info == 9);
      Group => PhononUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.SpectraUI SpectraUI<NEx=143.,NEy=550.,
      instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 10);
      parent => <-.scroll;
    };
    GMOD.instancer instance_spectra<NEx=143.,NEy=495.> {
      Value => (<-.type_info == 10);
      Group => SpectraUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.EditData.EnergyUI EnergyUI<NEx=873.,NEy=495.,
      instanced=0> {
      prefs => <-.preferences;
      visible => (<-.type_info == 11);
      parent => <-.scroll;
      listobj {
	selection => obj_index;
      };
      update_reals {
	object => obj_index;
      };
    };
    GMOD.instancer instance_energy<NEx=873.,NEy=441.> {
      Value => (<-.type_info == 11);
      Group => EnergyUI;
      active = 2;
    };
  };
  macro editUI {
    link model_type<NEportLevels={2,1},NEx=810.,NEy=36.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels=1,
      NEx=275.,NEy=22.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=66.,NEy=110.> {
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      title = "Edit Data";
      UIshell {
	height = 700;
      };
    };
    UIlabel title<NEx=187.,NEy=187.> {
      parent => <-.DLVshell.UIpanel;
      y = 0;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Data Objects";
    };
    link data_list<NEportLevels=1,NEx=286.,NEy=99.>;
    link data_object<NEportLevels={0,1},NEx=440.,NEy=99.>;
    CCP3.Core_Macros.UI.UIobjs.DLVlist data_choices<NEx=363.,NEy=374.> {
      parent => <-.DLVshell.UIpanel;
      y => ((<-.title.y + <-.title.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      selectedItem => <-.data_object;
      strings+nres => <-.data_list.label;
    };
    int+nres select_method =>
      ((5 * data_list[data_object].vector[sub_list.selectedItem])
       + data_type + 1);
    // Not ideal, but at least it works.
    GMOD.parse_v set_sub_object<NEx=176.,NEy=440.> {
      v_commands =
	"sub_list.selectedItem => data_list[data_object].selection;";
      trigger => <-.data_object;
      relative => <-;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist sub_list<NEx=363.,NEy=451.> {
      parent => <-.DLVshell.UIpanel;
      y => ((<-.data_choices.y + <-.data_choices.height) + 5);
      visible+nres => (array_size(<-.data_list[data_object].data_list) > 1);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      //selectedItem+nres => <-.data_list[<-.data_object].selection;
      strings+nres => <-.data_list[<-.data_object].data_list;
    };
    link data_type<NEx=590.,NEy=99.> => data_list[data_object].type_data;
    string null_methods<NEportLevels=1>[] = {
      "Null"
    };
    string plot_scalar<NEportLevels=1>[] = {
      "Shift Energy"
    };
    string phonon_scalar<NEportLevels=1,NEx=11.,NEy=396.>[] = {
      "Trajectory"
    };
    string intens_scalar<NEportLevels=1,NEx=11.,NEy=341.>[] = {
      "Trajectory",
      "Spectra"
    };
    string surface_scalar<NEportLevels=1,NEx=11.,NEy=462.>[] = {
      "Orthoslice",
      "Slice",
      "Clamp",
      "Downsize",
      "Crop",
      "Cut",
      "Math"
    };
    string surface_vector<NEportLevels=1,NEx=11.,NEy=506.>[] = {
      "Orthoslice",
      "Slice",
      "Clamp",
      "Downsize",
      "Crop",
      "Cut",
      "Math"
    };
#ifdef DLV_DL
    string volume_scalar<NEportLevels=1,NEx=11.,NEy=550.>[] = {
      "Orthoslice",
      "Extend",
      "Slice",
      "Clamp",
      "Downsize",
      "Crop",
      "Cut",
      "Math",
      "Volume"
    };
#else
    string volume_scalar<NEportLevels=1,NEx=11.,NEy=550.>[] = {
      "Orthoslice",
      "Extend",
      "Slice",
      "Clamp",
      "Downsize",
      "Crop",
      "Cut",
      "Math"
    };
#endif // DLV_DL
    string volume_vector<NEportLevels=1,NEx=11.,NEy=594.>[] = {
      "Orthoslice",
      "Slice",
      "Clamp",
      "Downsize",
      "Crop",
      "Cut",
      "Math"
    };
    mlink method_list<NEportLevels=1,NEx=11.,NEy=638.> =>
      switch(select_method, null_methods, surface_scalar, volume_scalar,
	     phonon_scalar, intens_scalar, plot_scalar, null_methods,
	     surface_vector, volume_vector, null_methods, null_methods,
	     null_methods);
    int data_method<NEportLevels=1,NEx=740.,NEy=99.>;
    UIlabel method_title<NEx=363.,NEy=528.> {
      parent => <-.DLVshell.UIpanel;
      y => ((<-.sub_list.y + <-.sub_list.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Edit As";
      visible => (is_valid(<-.sub_list.selectedItem) &&
		  (array_size(<-.method_list) > 1));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist display_methods<NEx=363.,NEy=605.> {
      parent => <-.DLVshell.UIpanel;
      y => ((<-.method_title.y + <-.method_title.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      selectedItem => <-.data_method;
      visible => (is_valid(<-.sub_list.selectedItem) &&
		  (array_size(<-.method_list) > 1));
      strings => <-.method_list;
    };
    UIbutton Edit<NEx=858.,NEy=187.> {
      parent => <-.DLVshell.UIpanel;
      x => ((parent.clientWidth - width) / 2);
      y => ((<-.display_methods.y + <-.display_methods.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (is_valid(<-.sub_list.selectedItem) &&
		  (is_valid(<-.data_method) || array_size(method_list) == 1));
    };
    CCP3.Core_Modules.EditData.edit_data edit_data<NEx=891.,NEy=253.> {
      render => <-.Edit.do;
      object => <-.data_object;
      component+nres => <-.data_list[<-.data_object].selection;
      edit_type => <-.data_method;
    };
    CCP3.Renderers.Base.display_list &edit_list[];
    int edit_object;
    CCP3.Core_Macros.UI.UIobjs.DLVlist edit_objects<NEx=682.,NEy=605.> {
      parent => <-.DLVshell.UIpanel;
      y => ((<-.Edit.y + <-.Edit.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => array_size(<-.edit_list) > 0;
      strings => edit_list.label;
      selectedItem => <-.edit_object;
    };
    edit_panels edit_panels<NEx=825.,NEy=429.> {
      model_type => <-.model_type;
      UIframe {
	parent => <-.<-.DLVshell.UIpanel;
	y => (<-.<-.edit_objects.y + <-.<-.edit_objects.height + 5);
	height => (parent.clientHeight - y);
      };
      preferences => <-.preferences;
      obj_index => <-.edit_list[<-.edit_object].index;
      type_info => <-.edit_list[<-.edit_object].display_type;
    };
    CCP3.Core_Modules.EditData.select_edit select_edit<NEx=836.,NEy=649.> {
      object+nres => edit_list[<-.edit_object].index;
    };
  };
  macro menu_edit {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=649.,NEy=88.>;
    // Todo - should this be a UIcmd? might be cmdList eventually
    link model_type<NEportLevels={2,1},NEx=396.,NEy=45.>;
    UIoption data<NEx=165.,NEy=110.> {
      label = "Data";
      set = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    GMOD.instancer instancer<NEx=605.,NEy=165.> {
      Value => <-.data.set;
      Group => editUI;
    };
    CCP3.Core_Macros.UI.EditData.editUI editUI<NEx=352.,NEy=176.,instanced=0> {
      preferences => <-.preferences;
      model_type => <-.model_type;
      DLVshell {
	UIshell {
	  visible => <-.<-.<-.data.set;
	};
      };
    };
  };
};
