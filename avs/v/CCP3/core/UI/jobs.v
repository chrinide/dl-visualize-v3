
flibrary Jobs<NEeditable=1> {
  macro job_listUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    link list_of_jobs<NEportLevels=1,NEx=726.,NEy=99.>;
    link selection<NEportLevels=1,NEx=517.,NEy=165.>;
    link job_index => list_of_jobs[selection].index;
    int file_list<NEportLevels=1,NEx=671.,NEy=231.>[];
    CCP3.Core_Macros.UI.UIobjs.DLVshell CCP3shell<NEx=242.,NEy=110.> {
      title = "Job List";
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      UIshell {
	width = 500;
	height = 300;
      };
    };
    string+nres stuff[] => (list_of_jobs.label + ": " + list_of_jobs.status);
    CCP3.Core_Macros.UI.UIobjs.DLVlist job_list<NEx=209.,NEy=220.> {
      parent => <-.CCP3shell.UIpanel;
      //strings+nres =>
      //	(<-.list_of_jobs.label + ": " + <-.list_of_jobs.status);
      strings => <-.stuff;
      selectedItem => <-.selection;
      width => parent.clientWidth;
      y = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIbutton recover<NEx=154.,NEy=374.> {
      parent => <-.CCP3shell.UIpanel;
      active => <-.job_select.active_recover;
      label = "Recover Files";
      y => ((files.y + files.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIbutton delete<NEx=539.,NEy=374.> {
      parent => <-.CCP3shell.UIpanel;
      active => (<-.job_select.is_running == 0);
      x = 0;
      y => ((job_list.y + job_list.height) + 5);
      label = "Delete";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVoptionBox files<NEx=649.,NEy=319.> {
      parent => <-.CCP3shell.UIpanel;
      //labels => <-.job_select.file_list;
      selectedItems => <-.file_list;
      visible => <-.job_select.active_list;
      y => ((delete.y + delete.height) + 5);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      title => "File list";
    };
    UIbutton kill<NEx=693.,NEy=374.> {
      parent => <-.CCP3shell.UIpanel;
      active => <-.job_select.is_running;
      x => (parent.clientWidth / 2);
      y => <-.delete.y;
      label = "Kill";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Modules.JobInfo.job_select job_select<NEx=385.,NEy=330.> {
      selection => <-.job_index;
      trigger1 => <-.job_recover.update;
      trigger2 => <-.selection;
    };
    CCP3.Core_Modules.JobInfo.job_recover job_recover<NEx=286.,NEy=462.> {
      selection => <-.job_index;
      do => <-.recover.do;
      file_list => <-.file_list;
      use_list => <-.job_select.active_list;
    };
    CCP3.Core_Modules.JobInfo.job_kill job_kill<NEx=704.,NEy=462.> {
      selection => <-.job_index;
      do => <-.kill.do;
    };
    CCP3.Core_Modules.JobInfo.job_delete job_delete<NEx=539.,NEy=462.> {
      selection => <-.job_index;
      do => <-.delete.do;
    };
  };
  macro localUI {
    CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=176.,NEy=154.> {
      title = "Job Options";
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=99.,NEy=66.>;
    CCP3.Core_Modules.JobPrefs.job_data &data<NEx=836.,NEy=66.>;
    CCP3.Core_Macros.UI.UIobjs.DLVtextTypein scratch_location<NEx=385.,
                                                              NEy=330.> {
      UIparent => <-.DLVshell.UIpanel;
      slabel = "Scratch directory";
      stext => <-.data.scratch_dir;
      width => UIparent.clientWidth;
      x = 0;
      //y => (<-.job_type.y + <-.job_type.height + 5);
      y = 0;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      visible = 1;
      text_label {
	width = 120;
      };
      text {
	width => (<-.panel.width - 130);
	x = 125;
      };
    };
  };
  macro PrefsUI {
    CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=176.,NEy=154.> {
      title = "Job Options";
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      UIshell {
	height = 575;
      };
    };
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=99.,NEy=66.>;
    int remote<NEportLevels=1,NEx=759.,NEy=176.> =>
      (strcmp(data.hostname, "localhost") != 0);
    string options<NEportLevels=1,NEx=11.,NEy=341.>[] = {
      "Serial", "Parallel"
    };
    CCP3.Core_Modules.JobPrefs.job_data &data<NEx=836.,NEy=66.>;
    CCP3.Core_Macros.UI.UIobjs.DLVtextTypein machine<NEx=176.,NEy=242.> {
      UIparent => <-.DLVshell.UIpanel;
      slabel = "Machine name";
      stext => <-.data.hostname;
      width => UIparent.clientWidth;
      visible = 1;
      x = 0;
      y = 0;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      text_label {
	width = 120;
      };
      text {
	width => (<-.panel.width - 130);
	x = 125;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtextTypein work_location<NEx=176.,NEy=330.> {
      UIparent => <-.DLVshell.UIpanel;
      slabel = "Work directory";
      stext => <-.data.work_dir;
      width => UIparent.clientWidth;
      x = 0;
      y => (<-.machine.y + <-.machine.height + 5);
      //y => (<-.job_type.y + <-.job_type.height + 5);
      visible => <-.remote;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      text_label {
	width = 120;
      };
      text {
	width => (<-.panel.width - 130);
	x = 125;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtextTypein scratch_location<NEx=385.,
                                                              NEy=330.> {
      UIparent => <-.DLVshell.UIpanel;
      slabel = "Scratch directory";
      stext => <-.data.scratch_dir;
      width => UIparent.clientWidth;
      x = 0;
      y => (<-.work_location.y + <-.work_location.height + 5);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      visible = 1;
      text_label {
	width = 120;
      };
      text {
	width => (<-.panel.width - 130);
	x = 125;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox parallel<NEx=176.,NEy=396.> {
      parent => <-.DLVshell.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.scratch_location.y + <-.scratch_location.height + 5);
      selectedItem => <-.data.is_parallel;
      width => parent.clientWidth;
      labels => <-.options;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein num_cpus<NEx=176.,NEy=473.> {
      UIparent => <-.DLVshell.UIpanel;
      x = 0;
      y => (<-.queue_name.y + <-.queue_name.height + 5);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      int visible => <-.data.is_parallel;
      fval => <-.data.nprocs;
      flabel = "N Processors";
      width => UIparent.clientWidth;
      panel {
	visible => <-.visible;
      };
      label {
	width = 120;
      };
      field {
	width => (<-.panel.width - 130);
	x = 125;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein walltime<NEx=385.,NEy=473.> {
      UIparent => <-.DLVshell.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.id.y + <-.id.height + 5);
      int visible => <-.remote;
      fval => data.hours;
      flabel = "Time (hours)";
      width => UIparent.clientWidth;
      panel {
	visible => <-.visible;
      };
      label {
	width = 120;
      };
      field {
	width => (<-.panel.width - 130);
	x = 125;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtextTypein id<NEx=176.,NEy=539.> {
      UIparent => <-.DLVshell.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      width => UIparent.clientWidth;
      visible => <-.remote;
      x = 0;
      y => (<-.parallel.y + <-.parallel.height + 5);
      stext => <-.data.account_id;
      slabel = "Project";
      text_label {
	width = 120;
      };
      text {
	width => (<-.panel.width - 130);
	x = 125;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein host_count<NEx=176.,NEy=605.> {
      UIparent => <-.DLVshell.UIpanel;
      x = 0;
      y => (<-.num_cpus.y + <-.num_cpus.height + 5);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      int visible => <-.data.is_parallel;
      fval => <-.data.nnodes;
      flabel = "N Nodes";
      width => UIparent.clientWidth;
      panel {
	visible => <-.visible;
      };
      label {
	width = 120;
      };
      field {
	width => (<-.panel.width - 130);
	x = 125;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein memory<NEx=385.,NEy=605.> {
      UIparent => <-.DLVshell.UIpanel;
      x = 0;
      y => (<-.host_count.y + <-.host_count.height + 5);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      int visible => <-.data.is_parallel;
      fval => <-.data.memory_gb;
      flabel = "Memory/Proc (GB)";
      width => UIparent.clientWidth;
      panel {
	visible => <-.visible;
      };
      label {
	width = 120;
      };
      field {
	width => (<-.panel.width - 130);
	x = 125;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtextTypein queue_name<NEx=385.,NEy=539.> {
      UIparent => <-.DLVshell.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      width => UIparent.clientWidth;
      visible => <-.remote;
      x = 0;
      y => (<-.walltime.y + <-.walltime.height + 5);
      stext => <-.data.queue;
      slabel = "Queue";
      text_label {
	width = 120;
      };
      text {
	width => (<-.panel.width - 130);
	x = 125;
      };
    };
  };
  macro menu_prefs {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=649.,NEy=88.>;
    int panel_open<NEportLevels=1,NEx=385.,NEy=33.> = 0;
    CCP3.Core_Modules.JobPrefs.job_data job_data<NEx=253.,NEy=165.> {
      hostname = "localhost";
      is_parallel = 0;
      nprocs = 1;
      hours = 0;
      work_dir = "";
      scratch_dir = "";
      account_id = "";
      nnodes = 0;
      memory_gb = 0;
      string queue = "";
    };
    UIoption UIoption<NEx=484.,NEy=165.> {
      set => <-.panel_open;
      label = "Jobs";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    // Todo - not so many copies?
    PrefsUI PrefsUI<NEx=616.,NEy=396.,instanced=0> {
      preferences => <-.preferences;
      data => <-.job_data;
      DLVshell {
	UIshell {
	  visible => <-.<-.<-.panel_open;
	};
      };
    };
    localUI localUI<NEx=275.,NEy=396.,instanced=0> {
      preferences => <-.preferences;
      data => <-.job_data;
      DLVshell {
	UIshell {
	  visible => <-.<-.<-.panel_open;
	};
      };
    };
    GMOD.instancer local_instance<NEx=154.,NEy=308.> {
      Group => <-.localUI;
      active = 2;
    };
    GMOD.instancer remote_instance<NEx=781.,NEy=297.> {
      Group => <-.PrefsUI;
      active = 2;
    };
  };
  macro menu_display {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=649.,NEy=88.>;
    int panel_open<NEportLevels=1,NEx=385.,NEy=33.> = 0;
    UIoption UIoption<NEx=253.,NEy=154.> {
      set => <-.panel_open;
      label = "Jobs";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    job_listUI job_listUI<NEx=363.,NEy=330.,instanced=0> {
      preferences => <-.preferences;
      CCP3shell {
	UIshell {
	  visible => <-.<-.<-.panel_open;
	};
      };
    };
    GMOD.instancer instancer<NEx=363.,NEy=253.> {
      Value => <-.panel_open;
      Group => <-.job_listUI;
      active = 2;
    };
  };
  macro JobUI {
    link preferences<NEportLevels={2,1},NEx=649.,NEy=77.>;
    menu_display menu_display<NEx=429.,NEy=132.> {
      preferences => <-.preferences;
    };
    menu_prefs menu_prefs<NEx=176.,NEy=132.> {
      preferences => <-.preferences;
    };
  };
};
