
flibrary Error<NEeditable=1> {
  macro error_handler {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=132.,NEy=44.>;
    GMOD.err_handler err_handler<NEx=330.,NEy=99.>;
    GMOD.copy_on_change copy_on_change<NEx=330.,NEy=176.> {
      trigger => <-.err_handler.err_message;
      input => 1;
      on_inst = 0;
    };
    UIerrorDialog UIerrorDialog<NEx=330.,NEy=264.> {
      visible => <-.copy_on_change.output;
      message => <-.err_handler.err_message;
      title => "DL Visualize Error";
      isModal = 1;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
  };
};
