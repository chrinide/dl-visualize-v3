
flibrary Model<NEeditable=1> {
  macro commonUI {
    boolean k_space<NEportLevels=1,NEx=561.,NEy=22.>;
    UIframe UIframe<NEx=154.,NEy=99.> {
      parent<NEportLevels={3,0}>;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.k_space == 0);
    };
    CCP3.Core_Modules.Render.common_options &data<NEx=484.,NEy=88.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=726.,NEy=88.>;
    CCP3.Core_Macros.UI.UIobjs.DLViSlider na<NEx=297.,NEy=176.> {
      parent => <-.UIframe;
      value => <-.data.na;
      y = 0;
      width => parent.clientWidth;
      active => (<-.model_dim > 0);
      min = 1.;
      max = 16.;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider nb<NEx=297.,NEy=253.> {
      parent => <-.UIframe;
      value => <-.data.nb;
      y => ((<-.na.y + <-.na.height) + 5);
      width => parent.clientWidth;
      active => (<-.model_dim > 1);
      min = 1.;
      max = 16.;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider nc<NEx=297.,NEy=319.> {
      parent => <-.UIframe;
      value => <-.data.nc;
      y => ((<-.nb.y + <-.nb.height) + 5);
      width => parent.clientWidth;
      active => (<-.model_dim > 2);
      min = 1.;
      max = 16.;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    link model_dim<NEportLevels={2,1},NEx=330.,NEy=22.>;
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox cell_type<NEx=297.,NEy=396.> {
      parent => <-.UIframe;
      labels => <-.cell_labels;
      selectedItem => <-.data.conventional_cell;
      width => parent.clientWidth;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.model_dim > 0);
      y => ((<-.nc.y + <-.nc.height) + 5);
    };
    string cell_labels<NEportLevels=1,NEx=88.,NEy=352.>[] => {
      "Primitive cell",
      "Conventional cell",
      "Asymmetric unit"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle centred<NEx=297.,NEy=462.> {
      parent => <-.UIframe;
      label => "Centre cell about origin";
      set => <-.data.centre_cell;
      y => ((<-.cell_type.y + <-.cell_type.height) + 5);
      width => parent.clientWidth;
      visible => (<-.model_dim > 0);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider tolerance<NEx=308.,NEy=308.> {
      visible => (<-.model_dim > 0);
      parent => <-.UIframe;
      value => <-.data.tolerance;
      y => ((<-.centred.y + <-.centred.height) + 5);
      max = 0.5;
      min = 0.0001;
      width => parent.clientWidth;
      decimalPoints = 4;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
  };
  macro atomUI {
    boolean k_space<NEportLevels=1,NEx=583.,NEy=33.>;
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow UIframe<NEx=165.,NEy=99.> {
      parent<NEportLevels={3,0}>;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.k_space == 0);
#ifdef MSDOS
      virtualHeight = 550;
#endif // MSDOS
    };
    CCP3.Core_Modules.Render.atom_options &data<NEx=407.,NEy=99.,
      NEportLevels={2,0}>;
    string groups<NEportLevels={2,1},NEx=873.,NEy=99.>[];
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=671.,NEy=99.>;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle draw_atoms<NEx=308.,NEy=176.> {
      parent => <-.UIframe;
      y = 0;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.data.draw;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider scale<NEx=308.,NEy=242.> {
      visible => <-.data.draw;
      parent => <-.UIframe;
      value => <-.data.scale;
      y => ((<-.draw_atoms.y + <-.draw_atoms.height) + 5);
      max = 1.5;
      min = 0.01;
      width => parent.clientWidth;
      decimalPoints = 2;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    string methods<NEportLevels=1,NEx=66.,NEy=352.>[] = {
      "Invert colour of selection",
      "Set Transparency on selection",
      "Set Transparency on unselected"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox select_method<NEx=308.,NEy=375.> {
      parent => <-.UIframe;
      labels => <-.methods;
      title = "Selection Method";
      //selectedItem => <-.data.conventional_cell;
      width => parent.clientWidth;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      y => ((<-.scale.y + <-.scale.height) + 5);
      selectedItem => <-.data.selection_method;
      visible => <-.data.draw;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle labels<NEx=308.,NEy=440.> {
      parent => <-.UIframe;
      label => "Label selections";
      y => ((<-.select_method.y + <-.select_method.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.data.label_selects;
      visible => <-.data.draw;
    };
    UIlabel object_label<NEx=308.,NEy=506.> {
      parent => <-.UIframe;
      label => "Object to Select";
      width => parent.clientWidth;
      y => ((<-.labels.y + <-.labels.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.data.draw;
    };
    UIlist what_to_select<NEx=308.,NEy=572.> {
      parent => <-.UIframe;
      y => ((<-.object_label.y + <-.object_label.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      selectedItem => <-.data.object_groups;
      strings => <-.groups;
      visible => <-.data.draw;
      active => (array_size(<-.groups) > 1);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider opacity<NEx=308.,NEy=627.> {
      parent => <-.UIframe;
      y => ((<-.what_to_select.y + <-.what_to_select.height) + 5);
      max = 1.;
      min = 0.;
      width => parent.clientWidth;
      decimalPoints = 2;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      //value => <-.data.opacity;
      active => (<-.data.selection_method != 0);
      visible => <-.data.draw;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider subdiv<NEx=506.,NEy=627.> {
      parent => <-.UIframe;
      y => ((<-.opacity.y + <-.opacity.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      //value => <-.data.sphere_subdiv;
      min = 4;
      max = 48;
      visible => <-.data.draw;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext flag_text<NEx=605.,NEy=528.> {
      parent => <-.UIframe;
      x = 10;
      width => parent.clientWidth - 20;
      y => ((<-.subdiv.y + <-.subdiv.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      text => <-.data.flag_data_label;
      outputOnly = 1;
      visible => <-.data.flag_data_available;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle use_flags<NEx=605.,NEy=473.> {
      parent => <-.UIframe;
      width => parent.clientWidth;
      y => ((<-.flag_text.y + <-.flag_text.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.data.use_flag_data;
      visible => <-.data.flag_data_available;
    };
    string pmethods<NEportLevels=1,NEx=803.,NEy=187.>[] = {
      "Decrease Radius",
      "Traffic Light Colours",
      "Use Transparency"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox property_method<NEx=605.,NEy=264.> {
      parent => <-.UIframe;
      y => ((<-.use_flags.y + <-.use_flags.height) + 5);
      width => parent.clientWidth;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      labels => <-.pmethods;
      selectedItem => <-.data.flag_method;
      visible => (<-.data.flag_data_available && <-.data.use_flag_data);
    };
    /*UIlabel property_label<NEx=605.,NEy=330.> {
      parent => <-.UIframe;
      label => "Property to select";
      width => parent.clientWidth;
      y => ((<-.property_method.y + <-.property_method.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIlist property<NEx=605.,NEy=407.> {
      parent => <-.UIframe;
      width => parent.clientWidth;
      y => ((<-.property_label.y + <-.property_label.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      selectedItem => <-.data.display_property;
      };*/
    AU.AUoptionTool select_atom<NEx=825.,NEy=330.> {
      tool_order = 22;
      pixmap.filename => switch(set + 1,
      				"$DLV_ROOT/icons/symoff.x",
      				"$DLV_ROOT/icons/nosym.x");
      hconn_list.offer = "";
      message = "Show selected atoms";
      //set => <-.data.select_exact_atom;
    };
    AU.AUoptionTool select_translations<NEx=825.,NEy=407.> {
      tool_order = 21;
      pixmap.filename => switch(set + 1,
				"$DLV_ROOT/icons/symoff.x",
				"$DLV_ROOT/icons/trans.x");
      hconn_list.offer = "";
      message = "Show selected atoms and translation copies";
      //set => <-.data.select_translation_copies;
    };
    AU.AUoptionTool select_symmetry<NEx=825.,NEy=484.> {
      tool_order = 20;
      pixmap.filename => switch(set + 1,
				"$DLV_ROOT/icons/symoff.x",
				"$DLV_ROOT/icons/symon.x");
      hconn_list.offer = "";
      message = "Show selected atoms and all symmetry copies";
      //set => <-.data.select_symmetry_copies;
    };
    AU.AUoptionToolList AUoptionToolList<NEx=825.,NEy=561.> {
      cmdList => {
	<-.select_symmetry,
	<-.select_translations,
	<-.select_atom
      };
      &selectedItem => <-.data.symmetry_copies;
    };
  };
  macro latticeUI_base {
    boolean k_space<NEportLevels=1,NEx=528.,NEy=77.>;
    int model_dim<NEportLevels={2,1},NEx=264.,NEy=66.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=781.,NEy=77.>;
    UIframe UIframe<NEx=253.,NEy=198.> {
      parent<NEportLevels={3,0}>;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle draw<NEx=429.,NEy=286.> {
      parent => <-.UIframe;
      set => <-.data.draw;
      y = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Modules.Render.lattice_options &data<NEx=451.,NEy=132.,
      NEportLevels={2,0}>;
  };
  macro latticeUI_base {
    boolean k_space<NEportLevels=1,NEx=528.,NEy=77.>;
    int model_dim<NEportLevels={2,1},NEx=264.,NEy=66.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=781.,NEy=77.>;
    UIframe UIframe<NEx=253.,NEy=198.> {
      parent<NEportLevels={3,0}>;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle draw<NEx=429.,NEy=286.> {
      parent => <-.UIframe;
      set => <-.data.draw;
      y = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
    CCP3.Core_Modules.Render.lattice_options &data<NEx=451.,NEy=132.,
      NEportLevels={2,0}>;
  };
  latticeUI_base latticeUI_r {
    UIframe {
      visible => (<-.k_space == 0 && <-.model_dim > 0);
    };
    draw {
      label = "Draw Real Space Lattice";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle label<NEx=429.,NEy=385.> {
      parent => <-.UIframe;
      label => "Label lattice";
      set => <-.data.label;
      y => ((<-.draw.y + <-.draw.height) + 5);
      width => <-.parent.clientWidth;
      visible => <-.data.draw;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
  };
  latticeUI_base latticeUI_k {
    UIframe {
      visible => (<-.k_space == 1 && <-.model_dim > 0);
    };
    draw {
      label = "Draw k-space Lattice";
    };
  };
  macro bondUI {
    UIframe UIframe<NEx=165.,NEy=99.> {
      parent<NEportLevels={3,0}>;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.k_space == 0);
    };
    CCP3.Core_Modules.Render.bond_options &data<NEx=407.,NEy=99.,
      NEportLevels={2,0}>;
    link k_space<NEportLevels=1,NEx=528.,NEy=44.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=671.,NEy=99.>;
    string bstyles<NEportLevels=1,NEx=671.,NEy=176.>[] = {
      "Lines","Tubes"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle draw_bonds<NEx=308.,NEy=176.> {
      y = 0;
      width => parent.clientWidth;
      parent => <-.UIframe;
      set => <-.data.draw;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle draw_polyhedra<NEx=306.,NEy=234.> {
      parent => <-.UIframe;
      y => ((<-.draw_bonds.y + <-.draw_bonds.height) + 5);
      width => parent.clientWidth;
      parent => <-.UIframe;
      set => <-.data.polyhedra;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIbutton do_bond<NEx=627.,NEy=319.> {
      parent => <-.UIframe;
      y => ((<-.draw_polyhedra.y + <-.draw_polyhedra.height) + 5);
      active => (<-.data.draw || <-.data.polyhedra);
      label => "Draw Selected Bond";
      width => (parent.clientWidth - 10);
      x = 5;
      do = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIbutton do_type<NEx=627.,NEy=363.> {
      parent => <-.UIframe;
      y => ((<-.del_bond.y + <-.del_bond.height) + 5);
      active => (<-.data.draw || <-.data.polyhedra);
      label => "Draw Bonds of Selected Type";
      width => (parent.clientWidth - 10);
      x = 5;
      do = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIbutton do_all<NEx=627.,NEy=407.> {
      parent => <-.UIframe;
      y => ((<-.del_type.y + <-.del_type.height) + 5);
      active => (<-.data.draw || <-.data.polyhedra);
      label => "Draw All Bonds";
      width => (parent.clientWidth - 10);
      x = 5;
      do = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIbutton del_bond<NEx=627.,NEy=462.> {
      parent => <-.UIframe;
      y => ((<-.do_bond.y + <-.do_bond.height) + 5);
      active => (<-.data.draw || <-.data.polyhedra);
      width => (parent.clientWidth - 10);
      label => "Deselect Bond";
      x = 5;
      do = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIbutton del_type<NEx=627.,NEy=506.> {
      parent => <-.UIframe;
      y => ((<-.do_type.y + <-.do_type.height) + 5);
      active => (<-.data.draw || <-.data.polyhedra);
      label => "Deselect Bonds of Type";
      width => (parent.clientWidth - 10);
      x = 5;
      do = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIbutton del_all<NEx=627.,NEy=550.> {
      parent => <-.UIframe;
      y => ((<-.do_all.y + <-.do_all.height) + 5);
      active => (<-.data.draw || <-.data.polyhedra);
      label => "Deselect All Bonds";
      width => (parent.clientWidth - 10);
      x = 5;
      do = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider atom_overlap<NEx=627.,NEy=407.> {
      y => ((<-.del_all.y + <-.del_all.height) + 5);
      width => parent.clientWidth;
      active => (<-.data.draw || <-.data.polyhedra);
      parent => <-.UIframe;
      min = 0.01;
      max = 5.;
      value => <-.data.overlap;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox bond_style<NEx=517.,NEy=220.> {
      parent => <-.UIframe;
      labels => <-.bstyles;
      selectedItem => <-.data.tubes;
      active => <-.data.draw;
      y => ((<-.atom_overlap.y + <-.atom_overlap.height) + 5);
      width => parent.clientWidth;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider bond_subdiv<NEx=517.,NEy=451.> {
      y => ((<-.bond_style.y + <-.bond_style.height) + 5);
      active => (<-.data.draw & (<-.data.tubes == 1));
      parent => <-.UIframe;
      min = 4.;
      max = 32.;
      value => <-.data.tube_subdiv;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider bond_radius<NEx=517.,NEy=495.> {
      y => ((<-.bond_subdiv.y + <-.bond_subdiv.height) + 5);
      active => (<-.data.draw & (<-.data.tubes == 1));
      parent => <-.UIframe;
      min = 0.01;
      max = 2.;
      value => <-.data.tube_radius;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle toggle_outline<NEx=459.,NEy=585.> {
      parent => <-.UIframe;
      y => ((<-.bond_radius.y + <-.bond_radius.height) + 5);
      label = "Outline polyhedra";
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => <-.data.polyhedra;
      set => <-.data.outline_poly;
    };
  };
  macro lineUI {
    UIframe UIframe<NEx=253.,NEy=165.> {
      parent<NEportLevels={3,0}>;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=594.,NEy=77.>;
    CCP3.Core_Macros.UI.UIobjs.DLViSlider line_width<NEx=429.,NEy=253.> {
      parent => <-.UIframe;
      y = 0;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle smooth_lines<NEx=429.,NEy=341.> {
      parent => <-.UIframe;
      y => ((<-.line_width.y + <-.line_width.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
  };
  macro renderUI {
    link model_dim<NEportLevels=1,NEx=286.,NEy=22.>;
    boolean k_space<NEportLevels=1,NEx=550.,NEy=11.> = 0;
    link visible<NEportLevels=1,NEx=99.,NEy=22.>;
    link parent<NEx=99.,NEy=88.,NEportLevels={2,1}>;
    CCP3.Core_Modules.Render.common_options &common<NEportLevels={0,2},
      NEx=297.,NEy=66.>;
    CCP3.Renderers.Model.structure_rspace &rspace<NEx=462.,NEy=55.,
      NEportLevels={2,0}> {
      atoms<NEportLevels={0,2}>;
      lattice<NEportLevels={0,2}>;
      bonds<NEportLevels={0,2}>;
    };
    CCP3.Renderers.Model.structure_kspace &kspace<NEx=858.,NEy=55.,
      NEportLevels={2,0}> {
      lattice<NEportLevels={0,2}>;
    };
    mlink groups<NEportLevels={2,1},NEx=693.,NEy=117.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels=1,
      NEx=693.,NEy=55.>;
    CCP3.Core_Modules.Render.props &Props<NEx=759.,NEy=11.,NEportLevels={2,1}>;
    /*string non_periodic<NEportLevels=1,NEx=22.,NEy=154.>[] = {"Real Space"};
    string periodic<NEportLevels=1,NEx=198.,NEy=154.>[] = {
      "Real Space",
      "Reciprocal Space"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox space_type<NEx=99.,NEy=286.> {
      parent => <-.parent;
      labels => switch(((<-.model_dim > 0) + 1),<-.non_periodic,<-.periodic);
      selectedItem => (<-.k_space);
      fontAttributes => <-.fontAttributes;
      visible => <-.visible;
      UIpanel {
	visible => <-.visible;
      };
      UIradioBox {
	orientation = "horizontal";
      };
      };*/
    UIpanel main_frame<NEx=231.,NEy=209.> {
      parent => <-.parent;
      //y => (<-.space_type.y + <-.space_type.height + 5);
      y = 0;
      width => parent.clientWidth;
      //height => (parent.clientHeight - y);
      height => parent.clientHeight;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.visible;
    };
    CCP3.Core_Macros.UI.Model.commonUI commonUI<NEx=308.,NEy=264.> {
      UIframe {
	parent => <-.<-.main_frame;
	y = 0;
	width => (parent.clientWidth / 3);
	height => (2 * parent.clientHeight / 3);
      };
      data => <-.common;
      preferences => <-.preferences;
      model_dim => <-.model_dim;
      k_space => <-.k_space;
    };
    CCP3.Core_Macros.UI.Model.atomUI atomUI<NEx=517.,NEy=264.> {
      UIframe {
	parent => <-.<-.main_frame;
	x => (parent.clientWidth / 3);
	y = 0;
	width => (parent.clientWidth / 3);
	height => parent.clientHeight;
      };
      select_method {
	visible => (<-.data.draw && (<-.<-.rspace.bonds.polyhedra == 0));
      };
      opacity {
	value => <-.<-.Props.spheres.opacity;
      };
      subdiv {
	value => <-.<-.Props.spheres.subdiv;
      };
      data => <-.rspace.atoms;
      preferences => <-.preferences;
      k_space => <-.k_space;
      groups => <-.groups;
    };
    latticeUI_r latticeUI_r<NEx=308.,NEy=352.> {
      model_dim => <-.model_dim;
      preferences => <-.preferences;
      UIframe {
	parent => <-.<-.main_frame;
	width => (parent.clientWidth / 3);
	height => (parent.clientHeight / 6);
	x = 0;
	y => (2 * parent.clientHeight / 3);
      };
      data => <-.rspace.lattice;
      k_space => <-.k_space;
    };
    latticeUI_k latticeUI_k<NEx=517.,NEy=352.> {
      model_dim => <-.model_dim;
      preferences => <-.preferences;
      UIframe {
	parent => <-.<-.main_frame;
	width => (parent.clientWidth / 3);
	height => (parent.clientHeight / 6);
	x = 0;
	y => (2 * parent.clientHeight / 3);
      };
      data => <-.kspace.lattice;
      k_space => <-.k_space;
    };
    CCP3.Core_Macros.UI.Model.lineUI lineUI<NEx=770.,NEy=352.> {
      UIframe {
	parent => <-.<-.main_frame;
	width => (parent.clientWidth / 3);
	height => (parent.clientHeight / 6);
	x = 0;
	y => (5 * parent.clientHeight / 6);
      };
      line_width {
	value => <-.<-.Props.lines.width;
      };
      smooth_lines {
	set => <-.<-.Props.lines.smooth;
      };
      preferences => <-.preferences;
    };
    CCP3.Core_Macros.UI.Model.bondUI bondUI<NEx=374.,NEy=462.> {
      UIframe {
	parent => <-.<-.main_frame;
	x => (2 * parent.clientWidth / 3);
	y = 0;
	width => (parent.clientWidth / 3);
	height => parent.clientHeight;
      };
      data => <-.rspace.bonds;
      preferences => <-.preferences;
      k_space => <-.k_space;
    };
  };
  macro atom_model {
    link preferences<NEportLevels={2,1},NEx=627.,NEy=66.>;
    link model_type<NEportLevels={2,1},NEx=99.,NEy=121.>;
    link parent<NEportLevels={2,1},NEx=374.,NEy=121.>;
    link props<NEportLevels=1,NEx=737.,NEy=132.>;
    mlink groups<NEportLevels=1,NEx=378.,NEy=180.>;
    //GMOD.instancer instancer<NEx=363.,NEy=297.> {
    //  Value => <-.Structure.set;
    //  Group => renderUI;
    //};
    CCP3.Core_Modules.Render.common_options &common<NEx=165.,NEy=66.,
      NEportLevels={0,1}>;
    CCP3.Renderers.Model.structure_rspace+nres &rspace<NEx=374.,NEy=66.,
      NEportLevels={0,1}>;
    CCP3.Renderers.Model.structure_kspace &kspace<NEx=825.,NEy=66.,
      NEportLevels={0,1}>;
    renderUI renderUI<NEx=605.,NEy=385.> { // was instanced = 0
      common => <-.common;
      rspace => <-.rspace;
      kspace => <-.kspace;
      groups => <-.groups;
      preferences => <-.preferences;
      model_dim => <-.model_type;
      parent => <-.parent;
      visible => (model_dim >= 0 && model_dim < 6);
    };
    CCP3.Core_Modules.Render.update_cell update_cell<NEx=231.,NEy=440.> {
      data => <-.common;
    };
    CCP3.Core_Modules.Render.update_lattice update_r<NEx=66.,NEy=528.> {
      data => <-.rspace.lattice;
      space = 0;
    };
    CCP3.Core_Modules.Render.update_lattice update_k<NEx=242.,NEy=528.> {
      data => <-.kspace.lattice;
      space = 1;
    };
    CCP3.Core_Modules.Render.update_atoms update_atoms<NEx=440.,NEy=440.> {
      data => <-.rspace.atoms;
    };
    CCP3.Core_Modules.Render.update_bonds update_bonds<NEx=451.,NEy=528.> {
      data => <-.rspace.bonds;
      do_add_bond => <-.renderUI.bondUI.do_bond.do;
      do_del_bond => <-.renderUI.bondUI.del_bond.do;
      do_add_type => <-.renderUI.bondUI.do_type.do;
      do_del_type => <-.renderUI.bondUI.del_type.do;
      do_add_all => <-.renderUI.bondUI.do_all.do;
      do_del_all => <-.renderUI.bondUI.del_all.do;
    };
  };
  macro outlineUI {
    link model_type<NEportLevels={2,1},NEx=286.,NEy=22.>;
    link visible<NEx=99.,NEy=22.>;
    link parent<NEx=99.,NEy=88.,NEportLevels={2,1}>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=693.,NEy=55.>;
    CCP3.Core_Modules.Render.common_options &common<NEportLevels={0,2},
      NEx=297.,NEy=66.>;
    UIpanel main_frame<NEx=231.,NEy=209.> {
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.parent;
    };
    CCP3.Core_Modules.Render.outline_data &data<NEx=506.,NEy=44.,
                                                NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.Model.commonUI commonUI<NEx=308.,NEy=264.> {
      UIframe {
	parent => <-.<-.main_frame;
	y = 0;
	width => (parent.clientWidth / 3);
	height => (2 * parent.clientHeight / 3);
      };
      cell_type {
	visible = 0;
      };
      centred {
       visible = 0;
      };
      tolerance {
      visible = 0;
      };
      na {
      active => (<-.model_dim > 8);
      };
      nb {
      active => (<-.model_dim > 9);
      };
      nc {
      active => (<-.model_dim > 10);
      };
      data => <-.common;
      preferences => <-.preferences;
      model_dim => <-.model_type;
      k_space = 0;
    };
    
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle outline<NEx=451.,NEy=275.> {
      parent => <-.main_frame;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.data.draw_lines;
      width => opacity.width;y = 25;
      x => parent.clientWidth / 2;
      y = 25;
      x => parent.clientWidth / 2;
      label = "Draw Outline";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle surface<NEx=451.,NEy=330.> {
      parent => <-.main_frame;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.data.draw_surface;
      width => opacity.width;
      y = 50;
      x => parent.clientWidth / 2;
      label = "Draw Surface";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider opacity<NEx=451.,NEy=396.> {
      parent => <-.main_frame;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 0.0;
      max = 1.0;
      y = 75;
      x => parent.clientWidth / 2;
      value => <-.data.opacity;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider red_slider<NEx=451.,NEy=473.> {
      parent => <-.main_frame;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 0.0;
      max = 1.0;
      y = 175;
      x => parent.clientWidth / 2;
      value => <-.data.red;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider green_slider<NEx=451.,NEy=539.> {
      parent => <-.main_frame;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 0.0;
      max = 1.0;
      y = 275;
      x => parent.clientWidth / 2;
      value => <-.data.green;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider blue_slider<NEx=451.,NEy=605.> {
      parent => <-.main_frame;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 0.0;
      max = 1.0;
      y = 375;
      x => parent.clientWidth / 2;
      value => <-.data.blue;
    };
  };
  macro outline_model {
    link preferences<NEportLevels={2,1},NEx=627.,NEy=66.>;
    link model_type<NEportLevels={2,1},NEx=99.,NEy=121.>;
    link parent<NEportLevels={2,1},NEx=374.,NEy=121.>;
    CCP3.Core_Modules.Render.outline_data &data<NEx=682.,NEy=231.,
                                                NEportLevels={0,1}>;
    CCP3.Core_Modules.Render.update_outline update_outline<NEx=440.,NEy=506.> {
      data => <-.data;
    };
    CCP3.Core_Modules.Render.common_options &common<NEx=165.,NEy=66.,
      NEportLevels={0,1}>;
    GMOD.instancer instancer<NEx=363.,NEy=297.> {
      Group => outlineUI;
    };
    outlineUI outlineUI<instanced=0,NEx=605.,NEy=385.> {
      common => <-.common;
      preferences => <-.preferences;
      model_type => <-.model_type;
      parent => <-.parent;
      visible => (model_type == 8);
      data => <-.data;
    };
    CCP3.Core_Modules.Render.update_cell update_cell<NEx=231.,NEy=440.> {
      data => <-.common;
    };
  };
  macro shellUI {
    link model_type<NEportLevels={2,1},NEx=286.,NEy=22.>;
    link visible<NEx=99.,NEy=22.>;
    link parent<NEx=99.,NEy=88.,NEportLevels={2,1}>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=693.,NEy=55.>;
    UIpanel main_frame<NEx=231.,NEy=209.> {
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.parent;
    };
    link data<NEx=506.,NEy=44.,NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLViSlider subdivisions<NEx=451.,NEy=473.> {
      parent => <-.main_frame;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      min = 4;
      max = 32;
      value => <-.data;
    };
  };
  macro shell_model {
    link preferences<NEportLevels={2,1},NEx=627.,NEy=66.>;
    link model_type<NEportLevels={2,1},NEx=99.,NEy=121.>;
    link parent<NEportLevels={2,1},NEx=374.,NEy=121.>;
    link data<NEx=682.,NEy=231.,NEportLevels={0,1}>;
    GMOD.instancer instancer<NEx=363.,NEy=297.> {
      Group => shellUI;
    };
    shellUI shellUI<instanced=0,NEx=605.,NEy=385.> {
      preferences => <-.preferences;
      model_type => <-.model_type;
      parent => <-.parent;
      visible => (model_type == 7);
      data => <-.data;
    };
  };
  /*macro k_UI {
    link fontinfo<NEportLevels={2,1},NEx=286.,NEy=22.>;
    link create<NEportLevels={2,1},NEx=88.,NEy=66.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &fontAttributes => <-.fontinfo;
      title = "Create Reciprocal Space View";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &fontAttributes => <-.fontinfo;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.panel;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => <-.create;
    };
    };*/
  macro k_space<NEx=803.,NEy=198.> {
    int new_view<NEportLevels=1,NEx=264.,NEy=165.> = 1;
    link model_type<NEportLevels={2,1},NEx=198.,NEy=88.>;
    link parent<NEportLevels={2,1},NEx=418.,NEy=88.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=638.,NEy=88.>;
    int visible<NEportLevels=1,NEx=495.,NEy=165.> = 0;
    link active_menu<NEportLevels={2,1},NEx=825.,NEy=88.>;
    UIcmd UIcmd<NEx=55.,NEy=165.> {
      //active => <-.active_menu;
      label = "Create k space view";
      do => <-.visible;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    /*CCP3.Core_Macros.UI.Model.k_UI k_UI<NEx=275.,NEy=341.> {
      fontinfo => <-.font_info;
      create => <-.new_view;
      };*/
    CCP3.Core_Modules.Render.reciprocal_space reciprocal_space<NEx=187.,
                                                               NEy=330.> {
      data => <-.UIcmd.do;
    };
  };
  macro Loop {
    int visible<NEportLevels=1,NEx=198.,NEy=165.> = 0;
    UIoption Animate<NEx=374.,NEy=176.> {
      set => <-.visible;
    };
    int reset<NEportLevels={2,0},export=2> = 0;
    int reset_back<NEportLevels={1,0},export=2,NEx=187.,NEy=55.> = 0;
    int run<NEportLevels={1,0},export=2> = 0;
    int run_back<NEportLevels={1,0},export=2,NEx=506.,NEy=55.> = 0;
    int step<NEportLevels={1,0},export=2> = 0;
    int step_back<NEportLevels={1,0},export=2> = 0;
    int cycle<NEportLevels={1,0},export=2> = 0;
    int start<NEportLevels={1,0},export=2> = 0;
    int end<NEportLevels={2,0},export=2> = 0;
    int incr<NEportLevels={1,0},export=2,NEx=682.,NEy=187.> = 1;
    int count<NEportLevels={0,2},export=2> = 0;
    int done<NEportLevels={0,2},export=2> = 0;
    link prefs<NEportLevels={2,1},NEx=715.,NEy=33.>;
    GMOD.loop loop<NEx=429.,NEy=319.> {
      &reset => <-.reset;
      &reset_back => <-.reset_back;
      &run => <-.run;
      &run_back => <-.run_back;
      &step => <-.step;
      &step_back => <-.step_back;
      &cycle => <-.cycle;
      &done => <-.done;
      &start_val => <-.start;
      &end_val => <-.end;
      &incr => <-.incr;
      &count => <-.count;
    };
    GMOD.instancer instancer<NEx=550.,NEy=396.> {
      Value => <-.visible;
      Group => <-.LoopUI;
      active = 2;
    };
    macro LoopUI<instanced=0,NEx=418.,NEy=473.> {
      CCP3.Core_Macros.Core.preferences &preferences<NEx=594.,NEy=33.,
	NEportLevels={2,1}> => <-.prefs;
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle run_toggle<NEx=165.,NEy=110.> {
	y = 0;
	width = 125;
	parent => <-.DLVshell.UIpanel;
	label => "Run";
	set<NEportLevels={2,0}> => <-.<-.run;
	&color => <-.preferences.colour;
	&fontAttributes => <-.preferences.fontAttributes;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle run_back_toggle {
	x = 125;
	y = 0;
	width = 125;
	parent => <-.DLVshell.UIpanel;
	label => "Run Backwards";
	set<NEportLevels={2,0}> => <-.<-.run_back;
	&color => <-.preferences.colour;
	&fontAttributes => <-.preferences.fontAttributes;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle step_toggle {
	y => ((<-.run_toggle.y + <-.run_toggle.height) + 4);
	width = 125;
	parent => <-.DLVshell.UIpanel;
	label => "Step";
	set<NEportLevels={2,0}> => <-.<-.step;
	&color => <-.preferences.colour;
	&fontAttributes => <-.preferences.fontAttributes;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle step_back_toggle {
	x = 125;
	y => ((<-.run_toggle.y + <-.run_toggle.height) + 4);
	width = 125;
	parent => <-.DLVshell.UIpanel;
	label => "Step Backwards";
	set<NEportLevels={2,0}> => <-.<-.step_back;
	&color => <-.preferences.colour;
	&fontAttributes => <-.preferences.fontAttributes;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle reset_toggle {
	y => ((<-.step_toggle.y + <-.step_toggle.height) + 4);
	width = 125;
	parent => <-.DLVshell.UIpanel;
	label => "Reset";
	set<NEportLevels={2,0}> => <-.<-.reset;
	&color => <-.preferences.colour;
	&fontAttributes => <-.preferences.fontAttributes;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle reset_back_toggle {
	x = 125;
	y => ((<-.step_toggle.y + <-.step_toggle.height) + 4);
	width = 125;
	parent => <-.DLVshell.UIpanel;
	label => "Reset Back";
	set<NEportLevels={2,0}> => <-.<-.reset_back;
	&color => <-.preferences.colour;
	&fontAttributes => <-.preferences.fontAttributes;
      };
      UIoption Once {
	set = 1;
      };
      UIoption Cycle;
      UIoption Bounce;
      UIoptionMenu cycle_toggle {
	y => ((<-.reset_toggle.y + <-.reset_toggle.height) + 4);
	width => <-.DLVshell.UIpanel.width;
	parent => <-.DLVshell.UIpanel;
	label => "Cycle Options";
	cmdList => {
	  <-.Once,
	  <-.Cycle,
	  <-.Bounce
	};
	selectedItem => <-.<-.cycle;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein start_typein<NExOffset=12.,
	NEyOffset=4.> {
	UIparent => <-.DLVshell.UIpanel;
	flabel => "Start Value";
	fval<NEportLevels={2,0}> => <-.<-.start;
	panel {
	  width => <-.<-.DLVshell.UIpanel.width;
	};
	field {
	  outputOnly = 1;
	};
	y => ((<-.cycle_toggle.y + <-.cycle_toggle.height) + 4);
	color => <-.preferences.colour;
	fontAttributes => <-.preferences.fontAttributes;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein end_typein<NExOffset=2.,
	NEyOffset=9.> {
	UIparent => <-.DLVshell.UIpanel;
	flabel => "End Value";
	fval<NEportLevels={2,0}> => <-.<-.end;
	panel {
	  width => <-.<-.DLVshell.UIpanel.width;
	};
	field {
	  outputOnly = 1;
	};
	y => ((<-.start_typein.y + <-.start_typein.height) + 4);
	color => <-.preferences.colour;
	fontAttributes => <-.preferences.fontAttributes;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein incr_typein {
	UIparent => <-.DLVshell.UIpanel;
	flabel => "Increment";
	fval<NEportLevels={2,0}> => <-.<-.incr;
	panel {
	  width => <-.<-.DLVshell.UIpanel.width;
	};
	field {
	  mode = "integer";
	  updateMode = 7;
	};
	y => ((<-.end_typein.y + <-.end_typein.height) + 4);
	color => <-.preferences.colour;
	fontAttributes => <-.preferences.fontAttributes;
      };
      CCP3.Core_Macros.UI.UIobjs.DLViSlider count_typein {
	parent => <-.DLVshell.UIpanel;
	title = "Loop count";
	value => <-.<-.count;
	width => ((<-.DLVshell.UIpanel.width * 11) / 12);
	mode = "integer";
	y => ((<-.incr_typein.y + <-.incr_typein.height) + 4);
	min => <-.<-.start;
	max => <-.<-.end;
	&color => <-.preferences.colour;
	&fontAttributes => <-.preferences.fontAttributes;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVslider_typein count_extra {
	slider => <-.count_typein;
	color => <-.preferences.colour;
	fontAttributes => <-.preferences.fontAttributes;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=33.,NEy=11.> {
	title = "Animate Trajectory";
	color => <-.preferences.colour;
	fontAttributes => <-.preferences.fontAttributes;
	UIshell {
	  visible => <-.<-.<-.visible;
	  height = 375;
	};
      };
    };
  };
  macro menu_display {
    UIoption Structure<NEx=143.,NEy=198.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=627.,NEy=66.>;
    link model_type<NEportLevels={2,1},NEx=99.,NEy=121.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=385.,NEy=198.> {
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      title = "Structure Display";
      UIshell {
	visible<NEportLevels={3,0}> => <-.<-.Structure.set;
	width = 768;
	height = 700;
	//height = 620;
      };
    };
    CCP3.Core_Macros.UI.Model.atom_model atoms<NEx=187.,NEy=319.> {
      parent => <-.DLVshell.UIpanel;
      preferences => <-.preferences;
      model_type => <-.model_type;
      renderUI {
	Props => <-.props;
	//	space_type {
	//  width => parent.clientWidth;
	//};
      };
    };
    CCP3.Core_Macros.UI.Model.outline_model outline<NEx=396.,NEy=319.> {
      preferences => <-.preferences;
      model_type => <-.model_type;
      parent => <-.DLVshell.UIpanel;
      instancer {
	Value => (<-.<-.Structure.set && (<-.model_type >= 8) && (<-.model_type <= 11));
      };
    };
    CCP3.Core_Macros.UI.Model.shell_model shell<NEx=638.,NEy=319.> {
      preferences => <-.preferences;
      model_type => <-.model_type;
      parent => <-.DLVshell.UIpanel;
      instancer {
	Value => (<-.<-.Structure.set && (<-.model_type == 7));
      };
    };
    k_space k_space<NEx=803.,NEy=198.> {
      model_type => <-.model_type;
      parent => <-.DLVshell.UIpanel;
      preferences => <-.preferences;
    };
  };
  macro ModelUI {
    link preferences<NEportLevels={2,1},NEx=231.,NEy=77.>;
    int active_menus<NEportLevels={2,1},NEx=429.,NEy=77.>;
    link model_type<NEportLevels={2,1},NEx=110.,NEy=143.>;
    int dialog_visible<NEportLevels=1,NEx=660.,NEy=77.> =>
      ((menu_file.dialog_visible == 1) || (menu_edit.dialog_visible == 1));
       // || (menu_display.k_space.visible == 1));
    link UIparent<NEportLevels={2,1},NEx=858.,NEy=77.>;
    CCP3.Core_Macros.UI.File_Ops.menu_file menu_file<NEx=297.,NEy=198.> {
      preferences => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.Model.menu_display menu_display<NEx=495.,NEy=198.> {
      preferences => <-.preferences;
      model_type => <-.model_type;
      k_space {
	active_menu => (<-.<-.active_menus &&
			(model_type > 0) && (model_type < 5));
      };
    };
    CCP3.Core_Macros.UI.EditModel.menu_edit menu_edit<NEx=715.,NEy=209.> {
      preferences => <-.preferences;
      active_menu => (<-.active_menus && (model_type > -1));
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.Model.Loop Loop<NEx=495.,NEy=341.> {
      prefs => <-.preferences;
    };
  };
};
