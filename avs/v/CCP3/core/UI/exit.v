
flibrary Exit<NEeditable=1> {
  macro exit_app {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=187.,NEy=55.>;
    GMOD.exit_process exit_process<NEx=451.,NEy=352.> {
      do_exit => <-.finalise.update;
    };
    UIcmd UIcmd<NEx=88.,NEy=154.,NEportLevels={0,2}> {
      label => "E&xit";
    };
    UIquestionDialog UIquestionDialog<NEx=418.,NEy=198.> {
      width = 300;
      height = 114;
      visible => <-.copy_on_change.output;
      title => "Exit DL Visualise?";
      message => messages[<-.check_save.warn];
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      okButton = 1;
      cancelButton = 1;
      ok<NEportLevels={1,2}> = 0;
      cancel = 0;
    };
    int exit_check<NEportLevels=1,NEx=418.,NEy=33.>;
    GMOD.copy_on_change copy_on_change<NEx=451.,NEy=88.> {
      input => <-.exit_check;
      output = 0;
    };
    link user_project<NEportLevels=1,NEx=693.,NEy=44.>;
    CCP3.Core_Modules.Project.check_save check_save<NEx=649.,NEy=143.> {
      test => ((<-.copy_on_change.output == 1) && (<-.user_project == 0));
      warn = 0;
    };
    string messages<NEportLevels=1,NEx=825.,NEy=198.>[] = {
      "Exit DL Visualize?",
      "Project is unsaved and will be lost,\n really exit?"
    };
    GMOD.parse_v parse_v_app<NEx=627.,NEy=209.> {
      v_commands = "UIapp.visible = 1;";
      trigger => <-.UIquestionDialog.cancel;
      on_inst = 0;
    };
    CCP3.Core_Modules.Exit.finalise finalise<NEx=440.,NEy=275.> {
      finish => <-.UIquestionDialog.ok;
      named => <-.user_project;
    };
  };
};
