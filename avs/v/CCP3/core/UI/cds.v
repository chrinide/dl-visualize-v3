
flibrary CDS<NEeditable=1> {
  macro CDS {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=143.,NEy=11.>;
    link active_menu<NEportLevels={2,1},NEx=319.,NEy=22.>;
    int visible<NEportLevels=1,NEx=572.,NEy=22.> = 0;
    link UIparent<NEportLevels={2,1},NEx=847.,NEy=33.>;
    int new_view = 1;
    link bond_all;
    UIoption+GMOD.hconnect cds_menu<NEportLevels={0,2},NEx=143.,NEy=66.> {
      label = "Access CDS";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => <-.active_menu;
      set => <-.visible;
      offer = "calcCmds";
      offer_name = ".";
      order = 101;
      direction = 1;
    };
    GMOD.instancer instancer<NEx=308.,NEy=176.> {
      Value => <-.visible;
      Group => cdsUI;
    };
    macro cdsUI<NEx=423.,NEy=207.,instanced=0> {
      CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
      link bond_all => <-.bond_all;
      CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=180.,NEy=90.> {
	color => <-.preferences.colour;
	fontAttributes => <-.preferences.fontAttributes;
	title = "Chemical Database";
	UIshell {
	  visible => <-.<-.<-.visible;
	};
	parse_v {
	  v_commands = "visible = 0;";
	  relative => <-.<-.<-;
	};
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
	parent => <-.DLVshell.UIpanel;
	x = 0;
	y = 0;
	width => parent.clientWidth;
	label = "Open in new viewer";
	&color => <-.preferences.colour;
	&fontAttributes => <-.preferences.fontAttributes;
	set => <-.<-.new_view;
      };
      CCP3.Core_Modules.Model.cds_control cds_control<NEx=369.,NEy=306.> {
	visible => <-.<-.visible;
	trigger = 0;
	new_view => <-.<-.new_view;
	bond_all => <-.bond_all;
      };
    };
    string urlfile<NEportLevels=1,NEx=774.,NEy=72.> =>
      (getenv("DLVRC") + "/cdsurl");
    group url<NEx=765.,NEy=216.> {
      string url<NEportLevels={1,2}>;
    };
    GMOD.load_v_script load_v_script<NEx=765.,NEy=144.> {
      filename => <-.urlfile;
      relative => <-.url;
    };
#ifdef USE_LINUX
    // could search for BrowserApplication in ~/.kde/share/config/kdeglobals
    GMOD.shell_command shell_command<NEx=468.,NEy=288.> {
      command = "gconftool-2 -g /desktop/gnome/url-handlers/http/command";
      on_inst = 1;
    };
    string string<NEportLevels=1,NEx=468.,NEy=360.>[] =>
      str_array(shell_command.stdout_string, " ");
    string+nres browser<NEx=468.,NEy=405.> =>
      switch((array_size(string)>0)+1, "firefox", string[0]);
    string browse_cmd<NEportLevels=1,NEx=468.,NEy=450.> =>
      browser + " -new-window ";
#else
    string browse_cmd<NEportLevels=1,NEx=468.,NEy=414.>;
#endif // linux
    CCP3.Core_Modules.Utils.browse browse<NEx=765.,NEy=468.> {
      trigger => <-.visible;
      run = 0;
      monitor = 1;
      browser => <-.browse_cmd;
      url => <-.url.url;
    };
  };
};
