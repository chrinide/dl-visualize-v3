
flibrary Tools<NEeditable=1> {
  macro Tool_bar {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=462.,NEy=33.>;
    link parent<NEportLevels={2,1},NEx=132.,NEy=88.>;
    AU.AUtoolbar tool_bar<NEx=242.,NEy=209.,hconnect=2> {
      x = 0;
      y = 0;
      width => parent.clientWidth;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
#ifdef DLV_V3_1
    AU.AUoptionTool transforms<NEx=429.,NEy=352.> {
      message = "Toggles application of back/forward buttons to transforms";
      pixmap.filename => switch(set + 1,
				"$DLV_ROOT/icons/off.x",
				"$DLV_ROOT/icons/on.x");
      isUIoption = 1;
      tool_order = 24;
      tool_space = 1;
    };
    AU.AUoptionTool backward<NEx=429.,NEy=429.> {
      message = "Back";
      pixmap.filename = "$DLV_ROOT/icons/backward.x";
      isUIoption = 0;
      tool_order = 26;
      tool_space = 0;
    };
    AU.AUoptionTool forward<NEx=429.,NEy=506.> {
      message = "Forward";
      pixmap.filename = "$DLV_ROOT/icons/forward.x";
      isUIoption = 0;
      tool_order = 27;
      tool_space = 1;
    };
#endif // DLV_V3_1
  };
};
