
flibrary Macros<NEeditable=1,
#ifndef DLV_NO_DLLS
		dyn_libs="libCCP3gulp",
#endif // DLV_NO_DLLS
		export_cxx=1,
		build_dir="avs/src/express",
		cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/core/env_gen.hxx avs/src/express/model.hxx avs/src/gulp/str_gen.hxx avs/src/gulp/extr_gen.hxx avs/src/gulp/save_gen.hxx avs/src/gulp/disp_gen.hxx avs/src/gulp/dos_gen.hxx avs/src/gulp/vec_gen.hxx avs/src/gulp/traj_gen.hxx avs/src/gulp/run_gen.hxx avs/src/express/data.hxx avs/src/gulp/path_gen.hxx",
		out_hdr_file="gulp.hxx",
		out_src_file="gulp.cxx"> {
  macro calcBase {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=198.,NEy=66.>;
    link active_menus<NEportLevels=1,NEx=385.,NEy=66.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=143.> {
      active => <-.active_menus;
      do => <-.visible;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
    };
  };
  // Bascially a copy of core/UI/file_ops LoadUI
  macro loadstrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.GULP.Modules.StructureData &data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Load GULP Structure";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel UIlabel<NEx=660.,NEy=165.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      alignment = "center";
      width => parent.clientWidth;
      label = "Model name";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=671.,NEy=242.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y => (<-.UIlabel.y + <-.UIlabel.height + 5);
      width => (parent.clientWidth - 10);
      text => <-.data.name;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => <-.data.new_view;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*.str";
      parent1<NEportLevels={2,0}> => <-.panel;
      title = "Load GULP Structure filename";
      file_write = 0;
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*.gstr";
      parent => <-.panel;
      title = "Load GULP Structure filename";
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
    };
    GMOD.parse_v reset_params<NEx=583.,NEy=77.> {
      v_commands = "data.new_view = 0; data.name = \"\";";
      relative => <-;
    };
  };
  calcBase Load_Structure {
    UIcmd {
      label = "Load Structure";
    };
    CCP3.GULP.Modules.StructureData data<NEx=627.,NEy=143.,
      NEportLevels={0,1}> {
      name = "";
      file = "";
      new_view = 0;
    };
    instancer {
      Group => <-.loadstrUI;
    };
    loadstrUI loadstrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.GULP.Modules.load_structure load_structure<NEx=407.,NEy=484.> {
      data => <-.data;
      do_load => <-.loadstrUI.dialog.ok;
    };
  };
  macro extract_strUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.GULP.Modules.StructureData &data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Extract GULP Structure";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel UIlabel<NEx=660.,NEy=165.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      alignment = "center";
      width => parent.clientWidth;
      label = "Model name";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=671.,NEy=242.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y => (<-.UIlabel.y + <-.UIlabel.height + 5);
      width => (parent.clientWidth - 10);
      text => <-.data.name;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => <-.data.new_view;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*.ginp";
      parent1<NEportLevels={2,0}> => <-.panel;
      title = "Load GULP Structure filename";
      file_write = 0;
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*.ginp";
      parent => <-.panel;
      title = "Load GULP Structure filename";
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
    };
    GMOD.parse_v reset_params<NEx=583.,NEy=77.> {
      v_commands = "data.new_view = 0; data.name = \"\";";
      relative => <-;
    };
  };
  calcBase Extract_Structure {
    UIcmd {
      label = "Extract Structure";
    };
    CCP3.GULP.Modules.StructureData data<NEx=627.,NEy=143.,
      NEportLevels={0,1}> {
      name = "";
      file = "";
      new_view = 0;
    };
    instancer {
      Group => <-.extract_strUI;
    };
    extract_strUI extract_strUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.GULP.Modules.extract_structure extract_structure<NEx=407.,NEy=484.> {
      data => <-.data;
      do_load => <-.extract_strUI.dialog.ok;
    };
  };
  macro savestrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Save GULP Structure";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent1<NEportLevels={2,0}> => <-.panel;
      title = "Save GULP Structure filename";
      file_write = 1;
      x = 0;
      y = 0;
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent => <-.panel;
      title = "Save GULP Structure filename";
      file_write = 1;
      x = 0;
      y = 0;
    };
  };
  calcBase Save_Structure {
    UIcmd {
      label = "Save Structure - Check";
    };
    instancer {
      Group => <-.savestrUI;
    };
    savestrUI savestrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      FileB {
	filename => <-.<-.data;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    string data<NEx=627.,NEy=143.> = "";
    CCP3.GULP.Modules.save_structure save_structure<NEx=407.,NEy=484.> {
      filename => <-.data;
      do_save => <-.savestrUI.dialog.ok;
    };
  };
  macro common_calcUI {
    int visible<NEportLevels={2,1},NEx=154.,NEy=33.>;
    int model_type<NEx=352.,NEy=33.,NEportLevels={2,0}>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=539.,NEy=33.>;
    link UIparent<NEportLevels={2,1},NEx=715.,NEy=33.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell CCP3shell<NEx=220.,NEy=99.> {
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      UIshell {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
	title = "GULP General options";
	height = 600;
      };
    };
    CCP3.GULP.Modules.gen_params &params<NEx=484.,NEy=77.,NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle props<NEx=220.,NEy=187.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Calculate Properties";
      width => parent.clientWidth;
      set => <-.params.calc_props;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle fe<NEx=220.,NEy=187.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Calculate Free Energy";
      width => parent.clientWidth;
      set => <-.params.gibbs_fe;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle usep {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth / 2;
      x = 0;
      y => ((<-.fe.y + <-.fe.height) + 5);
      set => <-.params.set_pressure;
      label => "Pressure (GPa)";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield pressure<NEx=231.,NEy=352.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth / 2;
      min = 0.0;
      value => <-.params.pressure;
      x => parent.clientWidth / 2;
      y => ((<-.fe.y + <-.fe.height) + 5);
      visible => (<-.params.set_pressure == 1);
      decimalPoints = 5;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle uset {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth / 2;
      x = 0;
      y => ((<-.pressure.y + <-.pressure.height) + 5);
      set => <-.params.set_temperature;
      label => "Temperature (K)";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield temperature<NEx=220.,NEy=231.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth / 2;
      min = 0.0;
      value => <-.params.temperature;
      x => parent.clientWidth / 2;
      y => ((<-.pressure.y + <-.pressure.height) + 5);
      visible => (<-.params.set_temperature == 1);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox phonons<NEx=220.,NEy=264.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "Phonon Calculation";
      width => parent.clientWidth;
      labels => { "No Phonon Calculation", "Phonon DOS", "Phonon Dispersion"};
      y => ((<-.temperature.y + <-.temperature.height) + 5);
      selectedItem => <-.params.calc_phonons;
      visible => (<-.model_type > 0);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein sh1 {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "shrink a";
      fmin => 1;
      fval => <-.params.shrink1;
      width => UIparent.clientWidth;
      y => ((<-.phonons.y + <-.phonons.height) + 5);
      panel {
	visible => ((<-.<-.params.calc_phonons == 1) & (<-.<-.model_type > 0));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein sh2 {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "shrink b";
      fmin => 1;
      fval => <-.params.shrink2;
      width => UIparent.clientWidth;
      y => ((<-.sh1.y + <-.sh1.height) + 5);
      panel {
	visible => ((<-.<-.params.calc_phonons == 1) & (<-.<-.model_type > 0));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein sh3 {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "shrink c";
      fmin => 1;
      fval => <-.params.shrink3;
      width => UIparent.clientWidth;
      y => ((<-.sh2.y + <-.sh2.height) + 5);
      panel {
	visible => ((<-.<-.params.calc_phonons == 1) & (<-.<-.model_type > 0));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein npoints {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Npoints";
      y => ((<-.phonons.y + <-.phonons.height) + 5);
      fval => params.npoints;
      fmin => 2;
      panel {
	visible => ((<-.<-.params.calc_phonons == 2) &
		    (<-.<-.model_type > 0));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext path {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.npoints.y + <-.npoints.height) + 5);
      text => params.bandpath;
      multiLine = 1;
      updateMode = 7;
      rows = 8;
      width => parent.clientWidth;
      visible => ((<-.params.calc_phonons == 2) & (<-.model_type > 0));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle eigenvectors {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y => ((<-.path.y + <-.path.height) + 5);
      set => <-.params.calc_eigenvectors;
      visible => ((<-.params.calc_phonons == 2) & (<-.model_type > 0));
      label => "Calculate Phonon Eigenvectors";
    };
  };
  macro optUI {
    int visible<NEportLevels={2,1},NEx=132.,NEy=22.>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=308.,NEy=22.>;
    link UIparent<NEportLevels={2,1},NEx=528.,NEy=22.>;
    CCP3.GULP.Modules.opt_params &params<NEx=484.,NEy=77.,NEportLevels={2,0}>;
    CCP3.GULP.Modules.gen_params &gparams<NEportLevels={2,0},NEx=660.,NEy=77.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell CCP3shell<NEx=220.,NEy=99.> {
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      UIshell {
	visible => <-.<-.visible;
	parent => <-.<-.UIparent;
	title = "GULP Optimisation parameters";
      };
    };
    string options[] = {
      "Constant Pressure", "Constant Volume"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox constraints<NEx=220.,NEy=187.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "Constraints";
      width => parent.clientWidth;
      labels => <-.options;
      selectedItem => <-.params.constraint;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle free_energy<NEx=220.,NEy=264.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Optimise Gibbs free energy";
      width => parent.clientWidth;
      y => ((<-.constraints.y + <-.constraints.height) + 5);
      set => <-.gparams.gibbs_fe;
    };
  };
  macro libUI {
    int visible<NEportLevels={2,1},NEx=143.,NEy=22.>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=363.,NEy=22.>;
    link UIparent<NEportLevels=1,NEx=594.,NEy=22.>;
    CCP3.GULP.Modules.lib_params &params<NEx=484.,NEy=77.,NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell CCP3shell<NEx=220.,NEy=99.> {
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      UIshell {
	visible => <-.<-.visible;
	parent => <-.<-.UIparent;
	title = "GULP Potential parameters";
      };
    };
    string base_dir<NEportLevels=1,NEx=639.,NEy=126.>;
    CCP3.Core_Macros.UI.UIobjs.FilePanel FileB<NEx=407.,NEy=429.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.params.libname;
      pattern => <-.base_dir + "/Libraries/*.lib";
      parent => <-.CCP3shell.UIpanel;
      title = "GULP libraries";
      x = 0;
      y = 5;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle shells<NEx=220.,NEy=264.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Include Shells";
      width => parent.clientWidth;
      y => ((<-.FileB.y + <-.FileB.height) + 5);
      set => <-.params.use_shells;
    };
    /*
    int selection<NEportLevels=1,NEx=484.,NEy=176.>;
    GMOD.parse_v parse_v {
      v_commands = "selection = switch((is_valid(list_libs.selection) + 1), 0, list_libs.selection);";
      //Todo trigger => <-.list_libs.selection;
      relative => <-;
    };
    */
  };
  macro mdUI {
    int visible<NEportLevels={2,1},NEx=110.,NEy=33.>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=319.,NEy=33.>;
    link UIparent<NEportLevels={2,1},NEx=528.,NEy=33.>;
    CCP3.GULP.Modules.MD_params &params<NEx=484.,NEy=88.,NEportLevels={2,0}>;
    CCP3.GULP.Modules.gen_params &gparams<NEportLevels={2,0},NEx=671.,NEy=88.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell CCP3shell<NEx=220.,NEy=99.> {
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      UIshell {
	visible => <-.<-.visible;
	parent => <-.<-.UIparent;
	title = "GULP MD parameters";
	height = 600;
      };
    };
    // Todo - why?
    string ensemble_list1<NEx=22.,NEy=132.>[] = {
      "NVE", "NVT"
    };
    string ensemble_list2<NEx=22.,NEy=176.>[] = {
      "NVE", "NVT", "NPT"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox ensemble<NEx=231.,NEy=286.> {
      parent => <-.CCP3shell.UIpanel;
      width => parent.clientWidth;
      selectedItem => <-.params.ensemble;
      x = 0;
      y = 0;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein thermostat {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => UIparent.clientWidth;
      fmin = 0.0000;
      flabel => "Nose-Hoover Thermostat";
      fval => <-.params.thermostat;
      y => ((<-.ensemble.y + <-.ensemble.height) + 5);
      panel {
	visible => (<-.<-.params.ensemble > 0);
      };
      field {
	decimalPoints = 5;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein barostat {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => UIparent.clientWidth;
      fmin = 0.0000;
      flabel => "NPT Barostat";
      fval => <-.params.barostat;
      y => ((<-.thermostat.y + <-.thermostat.height) + 5);
      panel {
	visible => (<-.<-.params.ensemble == 2);
      };
      field {
	decimalPoints = 5;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle usep {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth / 2;
      x = 0;
      y => ((<-.barostat.y + <-.barostat.height) + 5);
      set => <-.gparams.set_pressure;
      label => "Pressure (GPa)";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield pressure {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth / 2;
      min = 0.0;
      value => <-.gparams.pressure;
      x => parent.clientWidth / 2;
      y => ((<-.barostat.y + <-.barostat.height) + 5);
      visible => (<-.gparams.set_pressure == 1);
      decimalPoints = 5;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle uset {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth / 2;
      x = 0;
      y => ((<-.pressure.y + <-.pressure.height) + 5);
      set => <-.gparams.set_temperature;
      label => "Temperature (K)";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield temperature {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth / 2;
      min = 0.0;
      value => <-.gparams.temperature;
      x => parent.clientWidth / 2;
      y => ((<-.pressure.y + <-.pressure.height) + 5);
      visible => (<-.gparams.set_temperature == 1);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein timestep<NEx=209.,NEy=352.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => UIparent.clientWidth;
      fmin = 0.0001;
      flabel => "Time Step (fs)";
      fval => <-.params.timestep;
      y => ((<-.temperature.y + <-.temperature.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein equilibrate<NEx=418.,NEy=352.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => UIparent.clientWidth;
      fmin => 1;
      fval => <-.params.equilib;
      flabel => "Equilibration steps";
      y => ((<-.timestep.y + <-.timestep.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein production<NEx=220.,NEy=407.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => UIparent.clientWidth;
      fmin => 1;
      fval => <-.params.prod;
      flabel => "Production steps";
      y => ((<-.equilibrate.y + <-.equilibrate.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein sampling<NEx=429.,NEy=407.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => UIparent.clientWidth;
      fmin => 1;
      fval => <-.params.sample;
      flabel => "Sampling steps";
      y => ((<-.production.y + <-.production.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein write_traj<NEx=429.,NEy=407.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => UIparent.clientWidth;
      fmin => 1;
      fval => <-.params.traj_step;
      flabel => "Trajectory write steps";
      y => ((<-.sampling.y + <-.sampling.height) + 5);
    };
  };
  macro executeUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=143.,NEy=22.>;
    link UIparent<NEportLevels={2,1},NEx=726.,NEy=22.>;
    link model_type<NEx=319.,NEy=22.,NEportLevels={2,0}>;
    link visible<NEportLevels={2,1},NEx=858.,NEy=22.>;
    CCP3.GULP.Modules.params &params<NEx=517.,NEy=22.,NEportLevels={2,0}> {
      general<NEportLevels={0,2}> ;
      optimise<NEportLevels={0,2}>;
      md<NEportLevels={0,2}>;
      potentials<NEportLevels={0,2}>;
    };
    int visible_gen<NEportLevels=1,NEx=154.,NEy=66.> = 0;
    int visible_opt<NEportLevels=1,NEx=319.,NEy=66.> = 0;
    int visible_lib<NEportLevels=1,NEx=506.,NEy=66.> = 0;
    int visible_md<NEportLevels=1,NEx=682.,NEy=66.> = 0;
    GMOD.parse_v parse_v {
      v_commands = "visible_gen = 0; visible_opt = 0;"
	+ " visible_lib = 0; visible_md = 0;";
      trigger => <-.visible;
      on_inst = 1;
      relative => <-;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=242.,NEy=132.> {
      parent => <-.UIparent;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 400;
      height = 300;
      title = "Execute GULP";
      ok = 0;
      okButton = 1;
      okLabelString = "Run";
      cancel = 0;
      cancelButton = 1;
      visible => <-.visible;
    };
    UIpanel UIpanel<NEx=319.,NEy=220.> {
      parent => <-.UItemplateDialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle user_job {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "I want to run this job without DLV";
      y => ((MD_opts.y + MD_opts.height) + 10);
      set => <-.params.user_calc;
    };
    CCP3.Core_Macros.UI.UIobjs.DirSave FileB {
      preferences => <-.prefs;
      filename => <-.params.user_dir;
      pattern = "*";
      title = "Run GULP Job in Dir";
      parent => <-.UIpanel;
      y => ((user_job.y + user_job.height) + 5);
      UIpanel {
	visible => <-.<-.params.user_calc;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox UIradioBoxLabel<NEx=154.,NEy=308.> {
      parent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => {"Single Point", "Optimisation", "Molecular Dynamics"};
      selectedItem => <-.params.calc_type;
      width => parent.clientWidth;
      title = "Calculation type";
    };
    UIbutton General_opts<NEx=121.,NEy=385.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 150;
      y => ((<-.UIradioBoxLabel.y + <-.UIradioBoxLabel.height) + 5);
      do => <-.visible_gen;
    };
    UIbutton Optimisation_opts<NEx=308.,NEy=385.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 150;
      y => <-.MD_opts.y;
      x => ((parent.clientWidth / 2) + 5);
      visible => (<-.params.calc_type == 1);
      do => visible_opt;
    };
    UIbutton MD_opts<NEx=121.,NEy=451.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 150;
      y => ((<-.General_opts.y + <-.General_opts.height) + 5);
      visible => (<-.params.calc_type == 2);
      do => <-.visible_md;
    };
    UIbutton Select_potentials<NEx=308.,NEy=451.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => <-.General_opts.y;
      width = 150;
      x => ((parent.clientWidth / 2) + 5);
      do => visible_lib;
    };
    CCP3.GULP.Modules.kpath_info kpath_info<NEx=810.,NEy=144.> {
      model_type => <-.model_type;
    };
    CCP3.Core_Modules.Data.band_paths paths;
    string curpath => switch ((is_valid(model_type) + 1), "?", switch ((model_type + 1), "?", "?", paths.path2D, paths.path3D.lattices[kpath_info.lattice].centers[kpath_info.centre].path));
    GMOD.parse_v parse_bands {
      v_commands = "params.general.bandpath = curpath;";
      //trigger => (<-.model_type & <-.params.general.calc_phonons);
      trigger => curpath;
      on_inst = 0;
      //relative => <-;
    };
    GMOD.instancer instance_gen<NEx=572.,NEy=385.,weight=2> {
      Value => (<-.visible_gen && <-.visible);
      Group => GeneralUI;
    };
    GMOD.instancer instance_opt<NEx=759.,NEy=385.,weight=2> {
      Value => (<-.visible_opt && <-.visible);
      Group => OptUI;
    };
    GMOD.instancer instance_lib<NEx=572.,NEy=440.,weight=2> {
      Value => (<-.visible_lib && <-.visible);
      Group => LibUI;
    };
    GMOD.instancer instance_md<NEx=759.,NEy=440.,weight=2> {
      Value => (<-.visible_md && <-.visible);
      Group => MDUI;
    };
    CCP3.GULP.Macros.common_calcUI GeneralUI<NEx=55.,NEy=495.,instanced=0> {
      visible => ((<-.visible == 1) && (<-.visible_gen == 1));
      params => <-.params.general;
      model_type => <-.model_type;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      CCP3shell {
	parse_v {
	  v_commands = "visible_gen = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.GULP.Macros.optUI OptUI<NEx=231.,NEy=495.,instanced=0> {
      visible => ((<-.visible == 1) && (<-.visible_opt == 1));
      params => <-.params.optimise;
      gparams => <-.params.general;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      CCP3shell {
	parse_v {
	  v_commands = "visible_opt = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.GULP.Macros.libUI LibUI<NEx=396.,NEy=495.,instanced=0> {
      visible => ((<-.visible == 1) && (<-.visible_lib == 1));
      params => <-.params.potentials;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      CCP3shell {
	parse_v {
	  v_commands = "visible_lib = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.GULP.Macros.mdUI MDUI<NEx=561.,NEy=495.,instanced=0> {
      visible => ((<-.visible == 1) && (<-.visible_md == 1));
      params => <-.params.md;
      gparams => <-.params.general;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      // Todo - why
      ensemble {
	labels => <-.ensemble_list2;
      };
      CCP3shell {
	parse_v {
	  v_commands = "visible_md = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
  };
  calcBase execute {
    UIcmd {
      label = "Run Calculation";
    };
    link model_type<NEportLevels=1,NEx=583.,NEy=66.>;
    //Todo link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    int run<NEportLevels=1,NEx=517.,NEy=220.> = 0;
    int save<NEportLevels=1,NEx=682.,NEy=220.> = 0;
    //string filename<NEportLevels=1,NEx=836.,NEy=220.>;
    link job_data<NEportLevels={2,1},NEx=770.,NEy=132.>;
    CCP3.GULP.Modules.params params<NEx=550.,NEy=286.,NEportLevels={0,1}> {
      calc_type = 0;
      general {
	calc_props = 0;
	gibbs_fe = 0;
	calc_phonons = 0;
	shrink1 = 1;
	shrink2 = 1;
	shrink3 = 1;
	set_pressure = 0;
	pressure = 0.0;
	set_temperature = 0;
	temperature = 300.0;
	calc_eigenvectors = 0;
	npoints = 20;
	bandpath = "?";
      };
      optimise {
	constraint = 0;
      };
      md {
	ensemble = 0;
	equilib = 5;
	prod = 5;
	timestep = 1.0;
	sample = 5;
	thermostat = 0.05;
	barostat = 0.05;
	traj_step = 5;
      };
      potentials {
	libname = "";
	use_shells = 1;
      };
      user_calc = 0;
      user_dir = "";
    };
    CCP3.GULP.Macros.executeUI executeUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      params => <-.params;
      visible => <-.visible;
      model_type => <-.model_type;
      UItemplateDialog {
	ok => <-.<-.run;
      };
      FileB {
	UIfileDialog {
	  ok => <-.<-.<-.save;
	};
      };
      parse_bands {
	v_commands = "params.general.bandpath = executeUI.curpath;";
	relative => <-.<-;
      };
    };
    instancer {
      Group => executeUI;
    };
    CCP3.GULP.Modules.Run_GULP Run_GULP<NEx=583.,NEy=484.> {
      run => <-.run;
      data => <-.params;
      job_data => <-.job_data;
    };
  };
  macro propsUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent1<NEportLevels={2,0}> => <-.panel;
      file_write = 0;
      x = 0;
      y = 0;
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent => <-.panel;
      x = 0;
      y = 0;
    };
  };
  propsUI phonBandUI {
    dialog {
      title = "Load Phonon Bands";
    };
    FileB {
      title = "Load Phonon Bands filename";
      pattern = "*.disp";
    };
  };
  calcBase Phonon_Bands {
    UIcmd {
      label = "Phonon Dispersion";
    };
    string data<NEportLevels=1,NEx=649.,NEy=231.> = "";
    instancer {
      Group => <-.phonBandUI;
    };
    CCP3.GULP.Macros.phonBandUI phonBandUI<NEx=264.,NEy=385.> {
      prefs => <-.prefs;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      FileB {
	filename => <-.<-.data;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.GULP.Modules.load_bands load_bands<NEx=572.,NEy=385.> {
      filename => <-.data;
      do_load => <-.phonBandUI.dialog.ok;
    };
  };
  propsUI phonDOSUI {
    dialog {
      title = "Load Phonon DOS";
    };
    FileB {
      title = "Load Phonon DOS filename";
      pattern = "*.dens";
    };
  };
  calcBase Phonon_DOS {
    UIcmd {
      label = "Phonon DOS";
    };
    string data<NEportLevels=1,NEx=649.,NEy=231.> = "";
    instancer {
      Group => <-.phonDOSUI;
    };
    CCP3.GULP.Macros.phonDOSUI phonDOSUI<NEx=264.,NEy=385.> {
      prefs => <-.prefs;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      FileB {
	filename => <-.<-.data;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.GULP.Modules.load_DOS load_DOS<NEx=572.,NEy=385.> {
      filename => <-.data;
      do_load => <-.phonDOSUI.dialog.ok;
    };
  };
  propsUI phonVecUI {
    dialog {
      title = "Load Phonon Vectors";
    };
    FileB {
      title = "Load Phonon Vectors filename";
      pattern = "*.eig";
    };
  };
  calcBase Phonon_Vectors {
    UIcmd {
      label = "Phonon Vectors";
    };
    string data<NEportLevels=1,NEx=649.,NEy=231.> = "";
    instancer {
      Group => <-.phonVecUI;
    };
    CCP3.GULP.Macros.phonVecUI phonVecUI<NEx=264.,NEy=385.> {
      prefs => <-.prefs;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      FileB {
	filename => <-.<-.data;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.GULP.Modules.load_vectors load_vectors<NEx=572.,NEy=385.> {
      filename => <-.data;
      do_load => <-.phonVecUI.dialog.ok;
    };
  };
  propsUI MDTrajUI {
    dialog {
      title = "Load MD Trajectory";
    };
    FileB {
      title = "Load Trajectory filename";
      pattern = "*.trg";
    };
  };
  calcBase MD_Trajectory {
    UIcmd {
      label = "MD Trajectory";
    };
    string data<NEportLevels=1,NEx=649.,NEy=231.> = "";
    instancer {
      Group => <-.MDTrajUI;
    };
    CCP3.GULP.Macros.MDTrajUI MDTrajUI<NEx=264.,NEy=385.> {
      prefs => <-.prefs;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      FileB {
	filename => <-.<-.data;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.GULP.Modules.load_trajectory load_trajectory<NEx=572.,NEy=385.> {
      filename => <-.data;
      do_load => <-.MDTrajUI.dialog.ok;
    };
  };
  macro Properties<NEx=385.,NEy=110.> {
    UIcmdList UIcmdList<NEx=187.,NEy=110.> {
      label = "Load Property";
      cmdList => {
	Phonon_DOS.UIcmd,
	Phonon_Bands.UIcmd,
	Phonon_Vectors.UIcmd,
	MD_Trajectory.UIcmd
      };
    };
    link prefs<NEportLevels=1,NEx=330.,NEy=44.>;
    link UIparent<NEportLevels=1,NEx=528.,NEy=44.>;
    link model_type<NEportLevels=1,NEx=759.,NEy=44.>;
    link active_menus<NEportLevels=1,NEx=418.,NEy=110.>;
    int dialog_visible<NEportLevels=1,NEx=649.,NEy=110.> =>
      ((Phonon_Bands.visible == 1) ||
       (Phonon_DOS.visible == 1) ||
       (Phonon_Vectors.visible == 1) ||
       (MD_Trajectory.visible == 1));
    CCP3.GULP.Macros.Phonon_Bands Phonon_Bands<NEx=143.,NEy=330.> {
      active_menus => (<-.active_menus && (<-.model_type >= 0) &&
		       (<-.model_type < 4));
      UIparent => <-.UIparent;
      prefs => <-.prefs;
    };
    CCP3.GULP.Macros.Phonon_DOS Phonon_DOS<NEx=319.,NEy=330.> {
      active_menus => (<-.active_menus && (<-.model_type >= 0) &&
		       (<-.model_type < 4));
      UIparent => <-.UIparent;
      prefs => <-.prefs;
    };
    CCP3.GULP.Macros.Phonon_Vectors Phonon_Vectors<NEx=517.,NEy=330.> {
      active_menus => (<-.active_menus && (<-.model_type >= 0) &&
		       (<-.model_type < 4));
      UIparent => <-.UIparent;
      prefs => <-.prefs;
    };
    CCP3.GULP.Macros.MD_Trajectory MD_Trajectory<NEx=715.,NEy=330.> {
      active_menus => (<-.active_menus && (<-.model_type >= 0) &&
		       (<-.model_type < 4));
      UIparent => <-.UIparent;
      prefs => <-.prefs;
    };
  };
  macro prefsUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=748.,NEy=66.>;
    link visible<NEportLevels={2,1},NEx=154.,NEy=66.>;
    link location<NEportLevels={2,1},NEx=748.,NEy=143.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=374.,NEy=154.> {
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "GULP Preferences";
      UIshell {
	visible => <-.<-.visible;
      };
    };
    UIlabel label<NEx=385.,NEy=308.> {
      y => ((UIradioBoxLabel.y + UIradioBoxLabel.height) + 5);
      width => parent.clientWidth;
      parent => <-.DLVshell.UIpanel;
      label => "Location of GULP installation";
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DirectoryBrowser DBrowser<NEx=726.,NEy=363.> {
      preferences => <-.prefs;
      y => ((label.y + label.height) + 5);
      location => <-.location;
      title = "GULP install location";
      UIpanel {
	parent => <-.<-.DLVshell.UIpanel;
      };
    };
  };
  macro Prefs {
    link location<NEx=693.,NEy=143.,NEportLevels={0,1}>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=693.,NEy=44.>;
    link UIparent<NEportLevels=1,NEx=187.,NEy=77.>;
    UIoption UIoption<NEx=440.,NEy=33.> {
      label = "Preferences";
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer {
      Value => <-.UIoption.set;
      Group => <-.prefsUI;
    };
    prefsUI prefsUI<NEx=396.,NEy=330.,instanced=0> {
      prefs => <-.prefs;
      visible => <-.UIoption.set;
      location => <-.location;
      DLVshell {
	UIshell {
	  parent => <-.<-.<-.UIparent;
	};
      };
    };
  };
  CCP3.Core_Macros.CalcObjs.calculation GULP {
    UIcmdList {
      label = "GULP";
      order = 12;
      // DLV_DL and Save_Structure etc
      cmdList => {
	Load_Structure.UIcmd,
	Extract_Structure.UIcmd,
	execute.UIcmd,
	Properties.UIcmdList,
	//Save_Structure.UIcmd,
	Prefs.UIoption
      };
    };
    dialog_visible => ((Load_Structure.visible == 1) ||
		       (Extract_Structure.visible == 1) ||
		       (execute.visible == 1) ||
		       (Properties.dialog_visible == 1) /*||
		       (Save_Structure.visible == 1)*/);
    string exec_location<NEx=308.,NEy=187.,NEportLevels={1,1}> = "";
    CCP3.Core_Modules.Calcs.exec_environ exec_environ<NEx=308.,NEy=264.> {
      location => <-.exec_location;
      default_var = "DLV_DEF_GULP";
      search_path = 0;
      main_var = "DLV_GULP";
      binary = "gulp";
    };
    Load_Structure Load_Structure<NEx=110.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
    CCP3.GULP.Macros.execute execute<NEx=528.,NEy=462.> {
      active_menus => (<-.active_menus && (model_type >= 0) &&
		       (<-.model_type < 4));
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      model_type => <-.model_type;
      job_data => <-.job_data;
      executeUI {
	LibUI {
	  base_dir => <-.<-.<-.exec_location;
	};
      };
    };
#ifdef DLV_DL
    CCP3.GULP.Macros.Save_Structure Save_Structure<NEx=319.,NEy=374.> {
      active_menus => (<-.active_menus && (<-.model_type >= 0) &&
		       (<-.model_type < 4));
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
#endif // DLV_DL
    CCP3.GULP.Macros.Extract_Structure Extract_Structure<NEx=517.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
    CCP3.GULP.Macros.Properties Properties<NEx=319.,NEy=462.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      model_type => <-.model_type;
    };
    Prefs Prefs<NEx=836.,NEy=539.> {
      prefs => <-.preferences;
      location => <-.exec_location;
    };
  };
};
