
flibrary Modules<cxx_name="view"
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3dlv"
#endif // DLV_NO_DLLS
		 > {
  module select<build_dir="avs/src/core/viewer",
    src_file="express.cxx",
    out_src_file="sel_gen.cxx",
    out_hdr_file="sel_gen.hxx"> {
    cxxmethod+req select<status=1>(view_type+read+req+notify,
				   index+read+req+notify);
    int view_type<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
  };
  module remove<build_dir="avs/src/core/viewer",
    src_file="express.cxx",
    out_src_file="rem_gen.cxx",
    out_hdr_file="rem_gen.hxx"> {
    cxxmethod+req close<status=1>(trigger+read+req+notify,
				  view_type+read+req,
				  index+read+req);
    int trigger<NEportLevels={2,0}>;
    int view_type<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
  };
  module save_transform<build_dir="avs/src/core/viewer",
    src_file="express.cxx",
    out_src_file="xfrm_gen.cxx",
    out_hdr_file="xfrm_gen.hxx"> {
    cxxmethod+req save<status=1>(flag+read+req,
				 button+read+req,
				 state+notify+read+req);
    int flag<NEportLevels={2,0}>;
    int button<NEportLevels={2,0}>;
    int state<NEportLevels={2,0}>;
  };
};
