
flibrary KKR<NEeditable=1
#ifndef DLV_NO_DLLS
	     ,dyn_libs="libCCP3kkr"
#endif // DLV_NO_DLLS
	     > {
  module create_ui<build_dir="avs/src/kkr",
    out_src_file="kkr_gen.cxx",
    out_hdr_file="kkr_gen.hxx",
    need_objs="CCP3.KKR CCP3.Core_Macros.CalcObjs.calculation CCP3.Core_Modules.Calcs.exec_environ",
    src_file="create_xp.cxx"> {
    cxxmethod+notify_inst create<status=1>(make+req);
    int make<NEportLevels={2,0}>;
  };
};
