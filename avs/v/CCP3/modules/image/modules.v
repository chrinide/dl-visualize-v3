
flibrary Modules<NEeditable=1
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3im"
#endif // DLV_NO_DLLS
		 > {
  module load_data<build_dir="avs/src/image",
    out_src_file="load_gen.cxx",
    out_hdr_file="load_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(filename+read+req,
				 do_load+notify+req);
    string filename<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
};
