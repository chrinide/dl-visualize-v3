
flibrary K_P<NEeditable=1
#ifndef DLV_NO_DLLS
	     ,dyn_libs="libCCP3k_p"
#endif // DLV_NO_DLLS
	     > {
  module create_ui<build_dir="avs/src/k.p",
    out_src_file="k_p_gen.cxx",
    out_hdr_file="k_p_gen.hxx",
    need_objs="CCP3.K_P CCP3.Core_Macros.CalcObjs.calculation CCP3.Renderers.Base.outline",
    src_file="create_xp.cxx"> {
    cxxmethod+notify_inst create<status=1>(make+req);
    int make<NEportLevels={2,0}>;
  };
};
