
flibrary Modules<NEeditable=1
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3rod"
#endif // DLV_NO_DLLS
		 > {
  // Kind of a copy of CRYSTAL? Good? Bad?
  group LoadData {
    string name;  //Not currently set
    string file_bulk; 
    string file_surf; 		  
    string file_exp_data;
    string file_fit;
    string file_par;		  
    boolean new_view;  
    boolean got_exp_data;
    boolean got_fit_data;
    boolean got_par_data;		  
  };
  group CreateData {
    boolean get_list;
    boolean new_view; 
 //   string bulk_names[];
    string bulk_names;		    
    int list_size;
    boolean created_bulk;
    int current_bulk;
    boolean created_surface;
    int surf_h;
    int surf_k;
    int surf_l;
    int nlayers;
  };	
  group CalculateRodData {
    float h;	  
    float k;
    float lstart;
    float lend;
    int nl;
    boolean plot_bulk;
    boolean plot_surf;
    boolean plot_both;
    boolean plot_exp;
    boolean new_view;
  };
  group CalculateFfactorsData {
    int select_fs;
    boolean plot_calc;
    boolean plot_exp;   
    boolean new_view;
    float h_start;	  
    float k_start;
    float h_end;	  
    float k_end;
    float h_step;	  
    float k_step;
    float l;
    float maxq;
  };	
  group ParamsData {
    int fit_method;
    float atten;
    float scale;
    float scale_min;
    float scale_max;
    boolean scale_fit;
    float scale2;
    float scale2_min;
    float scale2_max;
    boolean scale2_fit;
    float beta;
    float beta_min;
    float beta_max;
    boolean beta_fit;
    float sfrac;
    float sfrac_min;
    float sfrac_max;
    boolean sfrac_fit;
    float fit_results[];
    float chisqr;
    float norm;
    float quality;
    boolean use_scale2;
    int ndisttot;
    float dist[];
    float dist_min[];
    float dist_max[];
    int dist_fit[];
    int ndwtot;
    float dw1[];
    float dw1_min[];
    float dw1_max[];
    int dw1_fit[];  
    int ndwtot2;
    float dw2[];
    float dw2_min[];
    float dw2_max[];
    int dw2_fit[];
    int nocctot;
    float occup[];
    float occup_min[];
    float occup_max[];
    int occup_fit[];
    boolean show_fit_results;
  };
  group SaveData {
    string file_bulk; 
    string file_surf; 		  
    string file_par;
    string file_fit;		  
  };
  group EditModelData {
    boolean first_edit;
    boolean cancel_edit;
  };	    
  module load_rod_files<build_dir="avs/src/rod",
    out_src_file="rod_load_gen.cxx", 
    out_hdr_file="rod_load_gen.hxx", 
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(load_data+read+req,
				 params+write+req,
				 do_load+notify+req);
    LoadData &load_data<NEportLevels={2,0}>;
    ParamsData &params<NEportLevels={2,0}>;
    //boolean use_scale2<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };

  module list_bulks<build_dir="avs/src/rod",
    out_src_file="rod_createlist_gen.cxx", 
    out_hdr_file="rod_createlist_gen.hxx", 
    src_file="express.cxx"> {
    cxxmethod+req create<status=1>(trigger+notify+read+req,
				   list_size+read+write+req,
				   bulk_names+write);
    int trigger<NEportLevels={2,0}>;
    int list_size<NEportLevels={2,0}>;
    string bulk_names<NEportLevels={2,0}>;
  };
  module create_model<build_dir="avs/src/rod",
    out_src_file="rod_create_gen.cxx", 
    out_hdr_file="rod_create_gen.hxx", 
    src_file="express.cxx"> {
    cxxmethod+req create_bulk<status=1>(trigger_bulk+notify+read+req,
					selected_bulk+read+req,
					create_data+read+write+req);
    cxxmethod+req create_surf<status=1>(trigger_surf+notify+read+req,
					selected_bulk+read+req,
					create_data+read+write+req);
    cxxmethod+req accept<status=1>(trigger_accept+notify+read+req);
    cxxmethod+req cancel<status=1>(trigger_cancel+notify+read+req,
				   create_data+read+req);
    int trigger_bulk<NEportLevels={2,0}>;
    int trigger_surf<NEportLevels={2,0}>;
    int trigger_accept<NEportLevels={2,0}>;
    int trigger_cancel<NEportLevels={2,0}>;
    int selected_bulk<NEportLevels={2,0}>;			     
    CreateData &create_data<NEportLevels={2,0}>;
  };
  module run_fit<build_dir="avs/src/rod",
    out_src_file="rod_fit_gen.cxx",
    out_hdr_file="rod_fit_gen.hxx",
    cxx_hdr_files="rod_load_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start_no_opt<status=1>(trigger_no_opt+notify+read+req,
					 load_data+read+req,
					 params+read+write+req);			     
    cxxmethod+req start_lm<status=1>(trigger_lm+notify+read+req,
				     load_data+read+req,
				     params+read+write+req);
    cxxmethod+req start_ana<status=1>(trigger_ana+notify+read+req,
				      load_data+read+req,
				      params+read+write+req);
    cxxmethod+req start<status=1>(load_data+read+req,
				  params+read+write+req);
    int trigger_no_opt<NEportLevels={2,0}>;			     
    int trigger_lm<NEportLevels={2,0}>;
    int trigger_ana<NEportLevels={2,0}>;
    ParamsData &params<NEportLevels={2,0}>;
    LoadData &load_data<NEportLevels={2,0}>;	     
  };	
  module get_rod_data<build_dir="avs/src/rod",
    out_src_file="rod_fitdata_gen.cxx",
    out_hdr_file="rod_fitdata_gen.hxx",
    cxx_hdr_files="rod_load_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start<status=1>(trigger+notify+read+req,
				  params+read+write+req,
				  load_data+read+req);
    int trigger<NEportLevels={2,0}>;
    ParamsData &params<NEportLevels={2,0}>;
    LoadData &load_data<NEportLevels={2,0}>;			     
  };
  module plot_rod<build_dir="avs/src/rod",
    out_src_file="rod_plot_gen.cxx",
    out_hdr_file="rod_plot_gen.hxx",
    cxx_hdr_files="rod_load_gen.hxx rod_fit_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start<status=1>(trigger+notify+read+req,
				  rod_data+read+req,
				  load_data+read+req,
				  params+read+req);
    int trigger<NEportLevels={2,0}>;		     
    CalculateRodData &rod_data<NEportLevels={2,0}>;
    ParamsData &params<NEportLevels={2,0}>;
    LoadData &load_data<NEportLevels={2,0}>;
  };
  module plot_ffactors<build_dir="avs/src/rod",
    out_src_file="ffactors_plot_gen.cxx",
    out_hdr_file="ffactors_plot_gen.hxx",
    cxx_hdr_files="rod_load_gen.hxx rod_plot_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start<status=1>(trigger+notify+read+req,
				  ffac_data+read+req,
				  load_data+read+req,
				  params+read+req);
    int trigger<NEportLevels={2,0}>;		     
    CalculateFfactorsData &ffac_data<NEportLevels={2,0}>;
    ParamsData &params<NEportLevels={2,0}>;
    LoadData &load_data<NEportLevels={2,0}>;
  };
  module view_rod_files<build_dir="avs/src/rod",
    out_src_file="rod_view_gen.cxx", 
    out_hdr_file="rod_view_gen.hxx", 
    src_file="express.cxx"> {
    cxxmethod+req view<status=1>(trigger+notify+read+req,
				 load_data+read,
				 text+write);
    int trigger<NEportLevels={2,0}>;
    string text<NEportLevels={0,2}>[];
    string data_file<NEportLevels={2,0}>;
  };	
  module save_rod_files<build_dir="avs/src/rod",
    out_src_file="rod_save_gen.cxx", 
    out_hdr_file="rod_save_gen.hxx", 
    cxx_hdr_files="rod_load_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req save<status=1>(save_data+read+req,
				 params+write+req,
				 do_save+notify+req);
    SaveData &save_data<NEportLevels={2,0}>;
    ParamsData &params<NEportLevels={2,0}>;
    int do_save<NEportLevels={2,0}>;
  };
  module show_lists<build_dir="avs/src/rod",
    out_src_file="rod_lists_gen.cxx", 
    out_hdr_file="rod_lists_gen.hxx", 
    src_file="express.cxx"> {
    cxxmethod+req dis_on<status=1>(trigger_dis_on+notify+read+req,
				   visible_dis+read+write+req);
    cxxmethod+req dis_off<status=1>(trigger_dis_off+notify+read+req,
				    visible_dis+read+write+req);			     
    cxxmethod+req dw1_on<status=1>(trigger_dw1_on+notify+read+req,
				   visible_dw1+read+write+req);
    cxxmethod+req dw1_off<status=1>(trigger_dw1_off+notify+read+req,
				    visible_dw1+read+write+req);
    cxxmethod+req dw2_on<status=1>(trigger_dw2_on+notify+read+req,
				   visible_dw2+read+write+req);
    cxxmethod+req dw2_off<status=1>(trigger_dw2_off+notify+read+req,
				    visible_dw2+read+write+req);
    cxxmethod+req occ_on<status=1>(trigger_occ_on+notify+read+req,
				   visible_occ+read+write+req);
    cxxmethod+req occ_off<status=1>(trigger_occ_off+notify+read+req,
				    visible_occ+read+write+req);  
    int trigger_dis_on<NEportLevels={2,0}>;
    int trigger_dis_off<NEportLevels={2,0}>;
    int trigger_dw1_on<NEportLevels={2,0}>;
    int trigger_dw1_off<NEportLevels={2,0}>;
    int trigger_dw2_on<NEportLevels={2,0}>;
    int trigger_dw2_off<NEportLevels={2,0}>;
    int trigger_occ_on<NEportLevels={2,0}>;
    int trigger_occ_off<NEportLevels={2,0}>;
    int visible_dis<NEportLevels={2,0}>;			     
    int visible_dw1<NEportLevels={2,0}>;
    int visible_dw2<NEportLevels={2,0}>;
    int visible_occ<NEportLevels={2,0}>;
  };	
  module edit_model<build_dir="avs/src/rod",
    out_src_file="rod_model_gen.cxx", 
    out_hdr_file="rod_model_gen.hxx", 
    cxx_hdr_files="rod_load_gen.hxx",
    src_file="express.cxx"> {  
    cxxmethod+req edit<status=1, weight=2>(trigger_edit+notify+read+req,
					   params+read+write+req,
					   load_data+read+req,
					   edit_model_data+read+write+req);
    cxxmethod+req cancel<status=1, weight=3>(trigger_cancel+notify+read+req,
						  params+read+write+req,
						  load_data+read+req,
						  edit_model_data+read+write+req);
    cxxmethod+req accept<status=1, weight=3>(trigger_accept+notify+read+req,
						  params+read+write+req,
						  load_data+read+req,
						  edit_model_data+read+write+req);
    cxxmethod+req update<status=1>(params+read+write+req,
				   load_data+read+req,
				   edit_model_data+read+write+req);
    float trigger_edit<NEportLevels={2,0}>;
    int trigger_cancel<NEportLevels={2,0}>;
    int trigger_accept<NEportLevels={2,0}>;			       
    ParamsData &params<NEportLevels={2,0}>;
    LoadData &load_data<NEportLevels={2,0}>;
    EditModelData &edit_model_data<NEportLevels={2,0}>;	     
  };
  module atom_labels<build_dir="avs/src/rod",
    out_src_file="rod_labels_gen.cxx", 
    out_hdr_file="rod_labels_gen.hxx", 
    cxx_hdr_files="rod_load_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req show<status=1>(trigger_show+notify+read+req);
    cxxmethod+req edit_dw1<status=1>(trigger_dw1+notify+read+req,
				     new_dw1+read+req, use_dw1,
				     trigger_dw2+read+req,
				     new_dw2+read+req, use_dw2,
				     trigger_occ+read+req,
				     new_occ+read+req, use_occ,
				     trigger_show+read+req,
				     params+read+write+req);
    cxxmethod+req edit_dw2<status=1>(trigger_dw1+read+req,
				     new_dw1+read+req, use_dw1,
				     trigger_dw2+notify+read+req,
				     new_dw2+read+req, use_dw2,
				     trigger_occ+read+req,
				     new_occ+read+req, use_occ,
				     trigger_show+read+req,
				     params+read+write+req);
    cxxmethod+req edit_occ<status=1>(trigger_dw1+read+req,
				     new_dw1+read+req, use_dw1,
				     trigger_dw2+read+req,
				     new_dw2+read+req, use_dw2,
				     trigger_occ+notify+read+req,
				     new_occ+read+req, use_occ,
				     trigger_show+read+req,
				     params+read+write+req);
    cxxmethod+req edit<status=1>(trigger_dw1+read+req,
				 new_dw1+read+req, use_dw1,
				 trigger_dw2+read+req,
				 new_dw2+read+req, use_dw2,
				 trigger_occ+read+req,
				 new_occ+read+req, use_occ,
				 trigger_show+read+req,
				 params+read+write+req);
    int trigger_show<NEportLevels={2,0}>;
    int trigger_dw1<NEportLevels={2,0}>;
    int new_dw1<NEportLevels={2,0}>;
    int trigger_dw2<NEportLevels={2,0}>;
    int new_dw2<NEportLevels={2,0}>;
    int trigger_occ<NEportLevels={2,0}>;
    int new_occ<NEportLevels={2,0}>;		
    boolean use_dw1<NEportLevels={2,0}>;
    boolean use_dw2<NEportLevels={2,0}>;
    boolean use_occ<NEportLevels={2,0}>;
    ParamsData &params<NEportLevels={2,0}>;		     
  };
  module update_params<build_dir="avs/src/rod",
    out_src_file="rod_params_fit_gen.cxx", 
    out_hdr_file="rod_params_fit_gen.hxx", 
    cxx_hdr_files="rod_load_gen.hxx",
    src_file="express.cxx"> {  
    cxxmethod+req update_dis<status=1>(trigger_dis+notify+read+req,
				       params+read+write+req);
    cxxmethod+req update_dw1<status=1>(trigger_dw1+notify+read+req,
				       params+read+write+req);
    cxxmethod+req update_dw2<status=1>(trigger_dw2+notify+read+req,
				       params+read+write+req);
    cxxmethod+req update_occ<status=1>(trigger_occ+notify+read+req,
				       params+read+write+req);
    int trigger_dis<NEportLevels={2,0}>;
    int trigger_dw1<NEportLevels={2,0}>;
    int trigger_dw2<NEportLevels={2,0}>;
    int trigger_occ<NEportLevels={2,0}>;
    ParamsData &params<NEportLevels={2,0}>;
  };		    
};
