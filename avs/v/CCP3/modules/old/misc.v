
flibrary Misc<NEeditable=1> {
  module CCP3write_image {
    Mesh_Unif+Dim2+Space2+Node_Data+Iparam &in;      /* input field */
    string+nonotify+Iparam  filename;		/* name of file to write */
    int+opt        output;			/* oneshot write the file */
    int+IPort      flip;			/* pre-3.0 param: no change! */
    int+nonotify+opt       format;
    int+nonotify+opt       filetype;
    int+nonotify+opt       depth;
    int+nonotify+opt       colortype;
    int+nonotify+opt       compresstype;
    float+nonotify+opt     compressqual;
    int+nonotify+opt       reducetype;
    int+IPort+nonotify+opt overwrite;
    ptr+nonotify           local_ptr<NEvisible=0>;
    method+notify_val write_image_update<status=1> = "DVwrite_image_update";
    method+notify_deinst write_image_delete = "DVwrite_image_delete";
  };
};
