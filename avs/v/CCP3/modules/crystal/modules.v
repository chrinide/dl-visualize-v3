
flibrary Modules<NEeditable=1
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3crystal"
#endif // DLV_NO_DLLS
		 > {
  group StructureData {
    string name;
    string file;
    boolean new_view;
    boolean fractional;
  };
  module load_structure<build_dir="avs/src/crystal",
    out_src_file="str_gen.cxx",
    out_hdr_file="str_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(data+read+req,
				 do_load+notify+req);
    StructureData &data<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module load_data<build_dir="avs/src/crystal",
    out_src_file="load_gen.cxx",
    out_hdr_file="load_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(file_type+read+req,
				 filename+read+req,
				 do_load+notify+req,
				 version+read+req);
    int file_type<NEportLevels={2,0}>;
    string filename<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
  };
  module save_str_file<build_dir="avs/src/crystal",
    out_src_file="save_gen.cxx",
    out_hdr_file="save_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req save<status=1>(filename+read+req,
				 geometry+read+req,
				 do_save+notify+req);
    string filename<NEportLevels={2,0}>;
    boolean geometry<NEportLevels={2,0}>;
    int do_save<NEportLevels={2,0}>;
  };
  module Structure<build_dir="avs/src/crystal",
    out_src_file="ex_gen.cxx",
    out_hdr_file="ex_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx",
    src_file="express.cxx"> {
    cxxmethod+req extract<status=1>(run+notify+req,
				    filename+read+req,
				    strname+read,
				    new_view+read+req,
				    job_data+read+req);
    int run<NEportLevels={2,0}>;
    string filename<NEportLevels={2,0}>;
    string strname<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  module Run_File<build_dir="avs/src/crystal",
    out_src_file="run_gen.cxx",
    out_hdr_file="run_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx",
    src_file="express.cxx"> {
    cxxmethod+req execute<status=1>(run+notify+req,
				    filename+read+req,
				    strname+read,
				    job_data+read+req,
				    use_mpp+read+req);
    int run<NEportLevels={2,0}>;
    string filename<NEportLevels={2,0}>;
    string strname<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
    boolean use_mpp<NEportLevels={2,0}>;
  };
  module Analyse<build_dir="avs/src/crystal",
    out_src_file="curw_gen.cxx",
    out_hdr_file="curw_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx",
    src_file="express.cxx"> {
    cxxmethod+req analyse<status=1>(run+notify+req,
				    version+read+req,
				    job_data+read+req);
    int run<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  module Wavefn<build_dir="avs/src/crystal",
    out_src_file="wfn_gen.cxx",
    out_hdr_file="wfn_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx",
    src_file="express.cxx"> {
    cxxmethod+req extract<status=1>(run+read+notify+req,
				    filename+read+req,
				    new_view+read+req,
				    version+read+req,
				    job_data+read+req,
				    tddft_also+read+req,
				    tddft_file+read+req,
				    binary_wvfn+read+req);
    int run<NEportLevels={2,0}>;
    string filename<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
    int tddft_also<NEportLevels={2,0}>;
    string tddft_file<NEportLevels={2,0}>;
    int binary_wvfn<NEportLevels={2,0}>;
  };
  group HamiltonianParams {
    int Htype;
    int Hoptions;
    boolean use_spins;
    int functional;
    int correlation;
    int exchange;
    int dft_grids;
    int aux_basis;
    boolean hybrid;
    int mixing;
  };
  group BasisParams {
    int basis;
    //boolean pseudo;
  };
  group KPointParams {
    boolean asym;
    int is1;
    int is2;
    int is3;
    int isp;
  };
  group TolParams {
    int itol1;
    int itol2;
    int itol3;
    int itol4;
    int itol5;
    int method;
    int eigenval;
    int energy;
    int deltap;
  };
  group ConvergeParams {
    int method;
    int mixing;
    int maxcycles;
    boolean use_levshift;
    int levshift;
    boolean lock_levshift;
    boolean use_smearing;
    float fermi_smear;
    boolean spinlock;
    int spin;
    int spin_cycles;
  };
  group OptParams {
    int optimiser;
    int opttype;
    int etol;
    float xtol;
    float gtol;		   
    float iscale;
    int dlf_type;
    int dlf_sstype;
    int dlf_initpop;
    int dlf_pop;
    float dlf_radius;
    float dlf_minradius;
    float dlf_contractradius;
    int dlf_cycles;
    int dlf_nsaves;
    float dlf_mutation;
    float dlf_death;
    float dlf_scalef;
    int dlf_reset;		   
  };
  group PrintParams {
    boolean calc_mulliken;
    boolean calc_exchange;
  };
  group JobParams {
    boolean direct;
    boolean mondirect;
    boolean deltap;
    int dp_tol;
    int biesplit;
    int monsplit;
    boolean gradcalc;
    boolean restart;
    string fock_file;
  };
  group PhononParams {
    float step;
    int deriv;
    boolean intensity;
    boolean dispersion;
    int cell[3][3];
  };
  group NebParams {
    int nimages;
    int iters;
    int climb;
    float start_ene;
    float final_ene;
    int separation_int;
    int optimiser_int;
    int convergence_int;
    float tol_energy;
    float tol_step;
    float tol_grad;
    boolean symmpath;
    boolean usestrsym;
    boolean adapt;
    boolean restart;
    string restart_file;
    int end_model;
    int first_model;
    int mapping[];
    int natoms;
    float dist;
    boolean make91;
    string images_dir;
    boolean change_nim;
    boolean finalstr_button;
    boolean images_button;
    boolean nocalc;		   
  };
  group CPHFParams {
    int mixing;
    int cycles;
    int alpha;
    int udik;
  };
  group TDDFTParams {
    boolean do_scf;
    boolean tamm_dancoff;
    boolean calc_jdos;
    boolean calc_pes;
    boolean fastresponse;
    int diag;
    int davidson_state;
    int davidson_niters;
    int davidson_space;
    float davidson_conv;
  };
  group SCFParams {
    int task;
    int analyse;
    HamiltonianParams Hamiltonian;
    BasisParams Basis;
    KPointParams KPoints;
    TolParams Tolerances;
    ConvergeParams Convergence;
    OptParams Optimize;
    PrintParams Print;
    JobParams Job;
    PhononParams phonon;
    NebParams neb;
    CPHFParams cphf;
    TDDFTParams tddft;
    boolean user_calc;
    string user_dir;
  };
  module Run_SCF<build_dir="avs/src/crystal",
    out_src_file="scf_gen.cxx",
    out_hdr_file="scf_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx",
    src_file="express.cxx"> {
    cxxmethod+req close_excess_views<status=1>(run+notify+req,
					data+read+req);
    cxxmethod+req create<status=1>(make+notify+req,
				   data+read+req,
				   version+read+req);
    cxxmethod+req inherit_basis<status=1>(inherit+notify);
    cxxmethod+req add_basis<status=1>(file+read+notify+req,
				      use_default+read+req);
    cxxmethod+req execute<status=1, weight=2>(run+notify+req,
				    data+read+req,
				    version+read+req,
				    job_data+read+req,
				    use_mpp+read+req);
    cxxmethod+req stop<status=1, weight=2>(cancel+notify+req);
    cxxmethod+req close_excess_views2<status=1>(cancel+notify+req,
					data+read+req);			     
    int make<NEportLevels={2,0}>;
    int run<NEportLevels={2,0}>;
    int cancel<NEportLevels={2,0}>;
    int inherit<NEportLevels={2,0}>;
    SCFParams &data<NEportLevels={2,0}>;
    string file<NEportLevels={2,0}>;
    int use_default<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
    boolean use_mpp<NEportLevels={2,0}>;
  };
  group Density_params {
    int projection;
    float emin;
    float emax;
    int bmin;
    int bmax;
  };
  group NewK_params {
    int calc;
    int new_shrink;
    int asym_shrink;
    int is1;
    int is2;
    int is3;
    int isp;
    int calc_fermi;
  };
  group user_job_data {
    boolean user_calc;
    string user_dir;
  };
  user_job_data Grid3D_Data {
    boolean calc_charge;
    boolean calc_potential;
    int tddft_prop;
    int excitation;
    int npoints;
    int non_periodic;
    int convcell;
    int pot_itol;
    double x_min;
    double x_max;
    double y_min;
    double y_max;
    double z_min;
    double z_max;
  };
  module Grid3D<build_dir="avs/src/crystal",
    out_src_file="grid_gen.cxx",
    out_hdr_file="grid_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    Grid3D_Data &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  user_job_data Topond3D_Data {
    boolean calc_electrons;
    boolean calc_spin;
    boolean calc_laplacian;
    boolean calc_negative;
    boolean calc_gradient;
    boolean calc_hamiltonian;
    boolean calc_lagrangian;
    boolean calc_virial;
    int calc_elf;
    int npoints;
    double x_min;
    double x_max;
    double y_min;
    double y_max;
    double z_min;
    double z_max;
  };
  module Topond3D<build_dir="avs/src/crystal",
    out_src_file="topo3d_gen.cxx",
    out_hdr_file="topo3d_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    Topond3D_Data &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  user_job_data CD2D_params {
    int npoints;
    int selection;
  };
  module CDensity_2D<build_dir="avs/src/crystal",
    out_src_file="echg_gen.cxx",
    out_hdr_file="echg_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    CD2D_params &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  user_job_data POT_params {
    int npoints;
    int selection;
    int multipoles;
    int tolerance;
  };
  module Potential_2D<build_dir="avs/src/crystal",
    out_src_file="potm_gen.cxx",
    out_hdr_file="potm_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    POT_params &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  user_job_data DOS_params {
    int projection;
    int nprojections;
    int in_viewer;
    int npoints;
    int bmin;
    int bmax;
    float emin;
    float emax;
    int npoly;
    boolean energy_limits;
    boolean project_all;
    boolean expand_atoms;
    boolean expand_states;
    int orbitals[];
    string labels[];
  };
  module density_of_states<build_dir="avs/src/crystal",
    out_src_file="dos_gen.cxx",
    out_hdr_file="dos_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req create<status=1>(make+notify+req,
				   version+read+req);
    cxxmethod+req init_proj<status=1>(init+notify+read+req,
				      data+write,
				      labels+write);
    cxxmethod+req list_orbitals<status=1>(make+read+req,
					  atoms+read+notify+req,
					  states+read+notify+req,
					  nselections+read+notify+req,
					  data+read,
					  labels+write);
    cxxmethod+req add_proj<status=1>(add+notify+read+req,
				     data+read+write+req);
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    cxxmethod+req stop<status=1>(cancel+notify+req);
    int make<NEportLevels={2,0}>;
    int init<NEportLevels={2,0}>;
    int atoms<NEportLevels={2,0}>;
    int states<NEportLevels={2,0}>;
    int nselections<NEportLevels={2,0}>;
    int add<NEportLevels={2,0}>;
    int run<NEportLevels={2,0}>;
    int cancel<NEportLevels={2,0}>;
    DOS_params &data<NEportLevels={2,0}>;
    string labels<NEportLevels={0,2}>[];
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  user_job_data BandParams {
    string path;
    int npoints;
    int bmin;
    int bmax;
  };
  module band_structure<build_dir="avs/src/crystal",
    out_src_file="band_gen.cxx",
    out_hdr_file="band_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data);
    cxxmethod+req show<status=1>(display+notify+req,
				 data+read+req);
    int run<NEportLevels={2,0}>;
    int display<NEportLevels={2,0}>;
    BandParams &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  // A lot like dos
  module bands_and_dos<build_dir="avs/src/crystal",
    out_src_file="bdos_gen.cxx",
    out_hdr_file="bdos_gen.hxx",
    cxx_hdr_files="dos_gen.hxx band_gen.hxx avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req create<status=1>(make+notify+req,
				   version+read+req);
    cxxmethod+req init_proj<status=1>(init+notify+read+req,
				      Ddata+write,
				      labels+write);
    cxxmethod+req list_orbitals<status=1>(make+read+req,
					  atoms+read+notify+req,
					  states+read+notify+req,
					  nselections+read+notify+req,
					  Ddata+read,
					  labels+write);
    cxxmethod+req add_proj<status=1>(add+notify+read+req,
				     Ddata+read+write+req);
    cxxmethod+req calc<status=1>(run+notify+req,
				 Bdata+read+req,
				 Ddata+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    cxxmethod+req stop<status=1>(cancel+notify+req);
    int make<NEportLevels={2,0}>;
    int init<NEportLevels={2,0}>;
    int atoms<NEportLevels={2,0}>;
    int states<NEportLevels={2,0}>;
    int nselections<NEportLevels={2,0}>;
    int add<NEportLevels={2,0}>;
    int run<NEportLevels={2,0}>;
    int cancel<NEportLevels={2,0}>;
    BandParams &Bdata<NEportLevels={2,0}>;
    DOS_params &Ddata<NEportLevels={2,0}>;
    string labels<NEportLevels={0,2}>[];
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  module mulliken<build_dir="avs/src/crystal",
    out_src_file="ppan_gen.cxx",
    out_hdr_file="ppan_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 user_data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    int data<NEportLevels={2,0}>;
    int rotate<NEportLevels={2,0}>;
    user_job_data &user_data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  user_job_data dlv_3D_data {
    int npoints;
    float spacing;
    boolean calc_charge;
    boolean calc_spin;
    boolean calc_pot;
    boolean calc_field;
    boolean calc_density;
    boolean use_points;
    int box;
    int pol_tol;
    int pot_tol;
    int min_band;
    int max_band;
    boolean calc_ldr;
    boolean calc_particle;
    boolean calc_hole;
    boolean calc_overlap;
    int min_excite;
    int max_excite;
  };
  module dlv3D<build_dir="avs/src/crystal",
    out_src_file="dlv3_gen.cxx",
    out_hdr_file="dlv3_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    dlv_3D_data &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  user_job_data bloch_data {
    int npoints;
    int grid;
    int min_band;
    int max_band;
    int kpoints[];
  };
  module bloch<build_dir="avs/src/crystal",
    out_src_file="bloc_gen.cxx",
    out_hdr_file="bloc_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    bloch_data &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  user_job_data wann3D_data {
    int npoints;
    int grid;
    int min_band;
    int max_band;
    int gstars;
    float spacing;
    float region;
    boolean symmetrize;
    boolean gen_grid;
    boolean use_points;
  };
  module wannier3D<build_dir="avs/src/crystal",
    out_src_file="wn3d_gen.cxx",
    out_hdr_file="wn3d_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    wann3D_data &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  user_job_data brillouin_data {
    int min_band;
    int max_band;
    int shrink;
  };
  module brillouin<build_dir="avs/src/crystal",
    out_src_file="bril_gen.cxx",
    out_hdr_file="bril_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    brillouin_data &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  module wannier2D<build_dir="avs/src/crystal",
    out_src_file="wn2d_gen.cxx",
    out_hdr_file="wn2d_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx avs/src/crystal/wn3d_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    wann3D_data &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  module nebtol<build_dir="avs/src/crystal",
    out_src_file="neb_gen.cxx",
    out_hdr_file="neb_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req update<status=1>(convergence_int+notify+read+req,
				 tol_energy+write,
				 tol_step+write,
				 tol_grad+write);
    int convergence_int<NEportLevels={2,0}>;
    float tol_energy<NEportLevels={2,0}>;
    float tol_step<NEportLevels={2,0}>;
    float tol_grad<NEportLevels={2,0}>;
  };
  module list_neb_models<build_dir="avs/src/crystal",
    out_src_file="neb_list_gen.cxx",
    out_hdr_file="neb_list_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req+notify_inst start(trigger+notify+read+req,
				    first_model+write,
				    labels+write);
    int trigger<NEportLevels={2,0}>;
    int first_model<NEportLevels={2,0}>;
    string labels<NEportLevels={0,2}>[];
  };
    module nebview<build_dir="avs/src/crystal",
    out_src_file="neb_view_gen.cxx",
    out_hdr_file="neb_view_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(trigger+notify+read+req,
			mapping_status+write,
			natoms+write,
			mapping+write,
			first_model+read,
			end_model+write,
			pdated_end_model+write);
    int trigger<NEportLevels={2,0}>;
    int mapping_status<NEportLevels={0,2}>;
    int natoms<NEportLevels={2,0}>;
    int mapping<NEportLevels={2,0}>[];
    int first_model<NEportLevels={2,0}>;
    int end_model<NEportLevels={2,0}>;
    int updated_end_model<NEportLevels={2,0}>;			     
  };
    module nebreset<build_dir="avs/src/crystal",
    out_src_file="neb_reset_gen.cxx",
    out_hdr_file="neb_reset_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(trigger+notify+read+req,
			nimages+read+req,			
			end_model+write+req,
			first_model+write+req,
			make91+write+req,
			images_dir+write+req);
    int trigger<NEportLevels={2,0}>;
    int nimages<NEportLevels={2,0}>;			     
    int first_model<NEportLevels={2,0}>;
    int end_model<NEportLevels={2,0}>;
    boolean make91<NEportLevels={2,0}>;	   
    string images_dir<NEportLevels={2,0}>;
  };
    module nebhide<build_dir="avs/src/crystal",
    out_src_file="neb_hide_gen.cxx",
    out_hdr_file="neb_hide_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(trigger+notify+read+req,
			dist+read+req,
			mapping+read+req,
		       	end_model+read+req);
    int trigger<NEportLevels={2,0}>;
    float dist<NEportLevels={2,0}>;
    int end_model<NEportLevels={2,0}>;
    int mapping<NEportLevels={0,2}>[];
  };
    module nebedit<build_dir="avs/src/crystal",
    out_src_file="neb_edit_gen.cxx",
    out_hdr_file="neb_edit_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(trigger+notify+read+req,
			mapping+write+req,
			dist+read+req,
			hide_atoms+read+req,
		       	end_model+read+req);
    int trigger<NEportLevels={2,0}>;
    int end_model<NEportLevels={2,0}>;
    float dist<NEportLevels={2,0}>;
    int hide_atoms<NEportLevels={2,0}>;
    int mapping<NEportLevels={2,0}>[];
  };
    module nebcreate<build_dir="avs/src/crystal",
    out_src_file="neb_create_gen.cxx",
    out_hdr_file="neb_create_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/scf_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(trigger+notify+read+req,
			data+read+req,
			job_data+read+req);
    int trigger<NEportLevels={2,0}>;
    SCFParams &data<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
    module nebresetim<build_dir="avs/src/crystal",
    out_src_file="neb_resetim_gen.cxx",
    out_hdr_file="neb_resetim_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(trigger+notify+read+req,
			images_button+write,
			nimages+write,			
			make91+write);
    int trigger<NEportLevels={2,0}>;
    boolean images_button<NEportLevels={2,0}>;
    int nimages<NEportLevels={2,0}>;
    boolean make91<NEportLevels={2,0}>;			     
  };		    
    module nebviewimage<build_dir="avs/src/crystal",
    out_src_file="neb_viewimage_gen.cxx",
    out_hdr_file="neb_viewimage_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(trigger+notify+read+req,
			job_data+read+req,
			images_dir+read+req,
			image+read+req);
    int trigger<NEportLevels={2,0}>;
    int image<NEportLevels={2,0}>;
    string images_dir<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
    module nebedimage<build_dir="avs/src/crystal",
    out_src_file="neb_edimage_gen.cxx",
    out_hdr_file="neb_edimage_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(trigger+notify+read+req,
			image+read);
    int trigger<NEportLevels={2,0}>;
    int image<NEportLevels={2,0}>;
   };
    module nebwrite91<build_dir="avs/src/crystal",
    out_src_file="neb_write91_gen.cxx",
    out_hdr_file="neb_write91_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/scf_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(trigger+notify+read+req,
			data+read+req,
			job_data+write+req,
			make91+read+req);
    int trigger<NEportLevels={2,0}>;
    SCFParams &data<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
    boolean make91<NEportLevels={2,0}>;
  };
  module edit_model<build_dir="avs/src/crystal",
    out_src_file="neb_editmod_gen.cxx",
    out_hdr_file="neb_editmod_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req finish(trigger_ok+req+notify,
			 image+read+req);
    cxxmethod+req cancel(trigger_cancel+req+notify);
    int trigger_ok<NEportLevels={2,0}>;
    int trigger_cancel<NEportLevels={2,0}>;
    int image<NEportLevels={2,0}>;
  };
  module move_atom<build_dir="avs/src/crystal",
    out_src_file="neb_accept_gen.cxx",
    out_hdr_file="neb_accept_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req accept<status=1>(trigger+notify+req,
				   image+read+req);
    int trigger<NEportLevels={2,0}>;
    int image<NEportLevels={2,0}>;
  };
  module TDDFT3D<build_dir="avs/src/crystal",
    out_src_file="td3d_gen.cxx",
    out_hdr_file="td3d_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx avs/src/crystal/grid_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(run+notify+req,
				 data+read+req,
				 DMparams+read+req,
				 NewK+read+req,
				 version+read+req,
				 job_data+read+req);
    int run<NEportLevels={2,0}>;
    Grid3D_Data &data<NEportLevels={2,0}>;
    Density_params &DMparams<NEportLevels={2,0}>;
    NewK_params &NewK<NEportLevels={2,0}>;
    int version<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
};
