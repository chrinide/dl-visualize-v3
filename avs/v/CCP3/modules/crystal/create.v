
flibrary CRYSTAL<NEeditable=1
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3crystal"
#endif // DLV_NO_DLLS
		 > {
  module create_ui<build_dir="avs/src/crystal",
    out_src_file="base_gen.cxx",
    out_hdr_file="base_gen.hxx",
    //need_objs="CCP3.CRYSTAL.Macros.CRYSTAL",
    need_objs="CCP3.CRYSTAL CCP3.Core_Macros.CalcObjs.calculation CCP3.Core_Modules.Calcs.exec_environ CCP3.Core_Macros.UI.UIobjs UImultiList CCP3.Core_Modules.Utils.select_str_arr CCP3.Core_Modules.Utils.map_selection",
    src_file="create_xp.cxx"> {
    cxxmethod+notify_inst create<status=1>(make+req);
    int make<NEportLevels={2,0}>;
  };
};
