
flibrary ONETEP<NEeditable=1
#ifndef DLV_NO_DLLS
		,dyn_libs="libCCP3onetep"
#endif // DLV_NO_DLLS
		> {
  module create_ui<build_dir="avs/src/onetep",
    out_src_file="onetep_gen.cxx",
    out_hdr_file="onetep_gen.hxx",
    need_objs="CCP3.ONETEP CCP3.Core_Macros.CalcObjs.calculation CCP3.Core_Modules.Calcs.exec_environ",
    src_file="create_xp.cxx"> {
    cxxmethod+notify_inst create<status=1>(make+req);
    int make<NEportLevels={2,0}>;
  };
};
