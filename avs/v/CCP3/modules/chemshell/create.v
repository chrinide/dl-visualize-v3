
flibrary CHEMSHELL<NEeditable=1
#ifndef DLV_NO_DLLS
		,dyn_libs="libCCP3chemshell"
#endif // DLV_NO_DLLS
		> {
  module create_ui<build_dir="avs/src/chemshell",
    out_src_file="chemshell_gen.cxx",
    out_hdr_file="chemshell_gen.hxx",
    need_objs="CCP3.CHEMSHELL CCP3.Core_Macros.CalcObjs.calculation",
    src_file="create_xp.cxx"> {
    cxxmethod+notify_inst create<status=1>(make+req);
    int make<NEportLevels={2,0}>;
  };
};
