
flibrary Render<NEeditable=1> {
  group common_options {
    int na;
    int nb;
    int nc;
    boolean centre_cell;
    int conventional_cell;
    float tolerance;
  };
  module update_cell<build_dir="avs/src/core/render",
    src_file="express.cxx",
    out_src_file="cell_gen.cxx",
    out_hdr_file="cell_gen.hxx"> {
    cxxmethod+req update<status=1>(data+notify);
    common_options &data<NEportLevels={2,0}>;
  };
  group lattice_options {
    boolean draw;
    boolean label;
  };
  module update_lattice<build_dir="avs/src/core/render",
    src_file="express.cxx",
    out_src_file="latt_gen.cxx",
    out_hdr_file="latt_gen.hxx"> {
    cxxmethod+req update<status=1>(data+notify,
				   space+read+req);
    lattice_options &data<NEportLevels={2,0}>;
    boolean space;
  };
  group atom_options {
    boolean draw;
    float scale;
    int selection_method;
    boolean label_selects;
    int object_groups;
    boolean flag_data_available;
    boolean use_flag_data;
    string flag_data_label;
    int flag_method;
    //int property_method;
    //int display_property;
    // Todo - move these to make calls to update_selected_atoms more efficient?
    //boolean select_exact_atom;
    //boolean select_translation_copies;
    //boolean select_symmetry_copies;
    int symmetry_copies;
  };
  module update_atoms<build_dir="avs/src/core/render",
    src_file="express.cxx",
    out_src_file="atom_gen.cxx",
    out_hdr_file="atom_gen.hxx"> {
    cxxmethod+req update<status=1>(data+notify);
    atom_options &data<NEportLevels={2,0}>;
  };
  group bond_options {
    boolean draw;
    boolean polyhedra;
    boolean tubes;
    float overlap;
    float tube_radius;
    int tube_subdiv;
    int outline_poly;
  };
  module update_bonds<build_dir="avs/src/core/render",
    src_file="express.cxx",
    out_src_file="bond_gen.cxx",
    out_hdr_file="bond_gen.hxx"> {
    cxxmethod+req update<status=1>(data+notify);
    cxxmethod+req add_bond<status=1>(do_add_bond+notify);
    cxxmethod+req del_bond<status=1>(do_del_bond+notify);
    cxxmethod+req add_type<status=1>(do_add_type+notify);
    cxxmethod+req del_type<status=1>(do_del_type+notify);
    cxxmethod+req add_all<status=1>(do_add_all+notify);
    cxxmethod+req del_all<status=1>(do_del_all+notify);
    bond_options &data<NEportLevels={2,0}>;
    int do_add_bond<NEportLevels={2,0}>;
    int do_del_bond<NEportLevels={2,0}>;
    int do_add_type<NEportLevels={2,0}>;
    int do_del_type<NEportLevels={2,0}>;
    int do_add_all<NEportLevels={2,0}>;
    int do_del_all<NEportLevels={2,0}>;
  };
  module select_atom<build_dir="avs/src/core/render",
    src_file="express.cxx",
    out_src_file="sel_gen.cxx",
    out_hdr_file="sel_gen.hxx"> {
    cxxmethod+req select<status=1>(coords+read,
				   event+notify+read+req,
				   npicked+read+req,
				   id+read+req,
				   nselects+write);
    float coords<NEportLevels={2,0}>[3];
    int event<NEportLevels={2,0}>;
    int npicked<NEportLevels={2,0}>;
    int id = 0;
    int nselects<NEportLevels={0,2}>;
  };
  group outline_data {
    boolean draw_lines;
    boolean draw_surface;
    float opacity;
    float red;
    float green;
    float blue;
  };
  module update_outline<build_dir="avs/src/core/render",
    src_file="express.cxx",
    out_src_file="outl_gen.cxx",
    out_hdr_file="outl_gen.hxx"> {
    cxxmethod+req update<status=1>(data+notify);
    outline_data &data<NEportLevels={2,0}>;
  };
  group sphere_props {
    int subdiv;
    float opacity;
  };
  // Dummy - non-avs would have update_spheres module
  group line_props {
    int width;
    boolean smooth;
  };
  // Dummy - non-avs would have update_lines module
  group props {
    sphere_props spheres;
    line_props lines;
  };
  module reciprocal_space<build_dir="avs/src/core/render",
    src_file="express.cxx",
    out_src_file="ksp_gen.cxx",
    out_hdr_file="ksp_gen.hxx"> {
    cxxmethod+req draw<status=1>(data+notify);
    int data<NEportLevels={2,0}>;
  };
  module connector<build_dir="avs/src/core/render",
    out_src_file="conn_gen.cxx",
    out_hdr_file="conn_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req+notify_inst+notify_deinst connect(trigger+read+req+notify,
						    from_obj+read+req,
						    to_obj+read+req,
						    ref+read+req,
						    connected+read+write);
    int trigger<NEportLevels={2,0}>;
    string from_obj;
    string to_obj;
    boolean ref;
    int connected = 0;
  };
  // Mappings for 3D planes into 2D views.
  module slice_to_2D<build_dir="avs/src/core/render",
    out_src_file="sl_gen.cxx",
    out_hdr_file="sl_gen.hxx",
    src_file="express.cxx",
    cxx_hdr_files="fld/Xfld.h"> {
    cxxmethod+req reduce_dim(in+read+notify+req,
                             //axis+read+notify+req,
                             out+write,
                             maxx+write,
                             maxy+write);
    Mesh_Struct &in<NEportLevels={2,0}>;
    //int axis<NEportLevels={2,0}>;
    Mesh_Struct out<NEportLevels={0,2}>;
    float maxx<NEportLevels={0,2}>[2];
    float maxy<NEportLevels={0,2}>[2];
  };
  module plane_to_2Dstruct<build_dir="avs/src/core/render",
    out_src_file="st2D_gen.cxx",
    out_hdr_file="st2D_gen.hxx",
    cxx_hdr_files="fld/Xfld.h",
    src_file="express.cxx"> {
    cxxmethod+req reduce_dim(in+read+notify+req,
                             out+write);
    Mesh_Struct &in<NEportLevels={2,0}>;
    Mesh_Struct out<NEportLevels={0,2}>;
  };
  module copy_slice<build_dir="avs/src/core/render",
    need_objs="Mesh_Struct Node_Data",
    cxx_hdr_files="fld/Xfld.h cell_gen.hxx",
    out_src_file="stag_gen.cxx",
    out_hdr_file="stag_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req agcopy(in+read+notify+req,
			 //axis+read+notify+req,
			 comp+read+notify+req,
			 context+read+notify+req,
			 out+write);
    Mesh_Struct+Node_Data &in<NEportLevels={2,0}>;
    //int axis<NEportLevels={2,0}>;
    int comp<NEportLevels={2,0}>;
    common_options &context<NEportLevels={2,0}>;
    Mesh_Struct+Node_Data out<NEportLevels={0,2}>;
  };
  module copy_rect<build_dir="avs/src/core/render",
    need_objs="Mesh_Rect Node_Data",
    cxx_hdr_files="fld/Xfld.h cell_gen.hxx",
    out_src_file="rtag_gen.cxx",
    out_hdr_file="rtag_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req agcopy(in+read+notify+req,
			 //axis+read+notify+req,
			 comp+read+notify+req,
			 context+read+notify+req,
			 out+write);
    Mesh_Rect+Node_Data &in<NEportLevels={2,0}>;
    //int axis<NEportLevels={2,0}>;
    int comp<NEportLevels={2,0}>;
    common_options &context<NEportLevels={2,0}>;
    Mesh_Rect+Node_Data out<NEportLevels={0,2}>;
  };
  module attach_plane<build_dir="avs/src/core/render",
    src_file="express.cxx",
    out_src_file="c2D_gen.cxx",
    out_hdr_file="c2D_gen.hxx"> {
    cxxmethod+notify_inst create();
    cxxmethod+req close(trigger+notify,
			visible+write);
    int trigger<NEportLevels={2,0}>;
    int visible<NEportLevels={2,0}>;
  };
  module read_file<build_dir="avs/src/core/render",
    src_file="express.cxx",
    out_src_file="file_gen.cxx",
    out_hdr_file="file_gen.hxx"> {
    cxxmethod+notify_inst read(trigger+notify,
			       filename+read+req,
			       text+write);
    int trigger<NEportLevels={2,0}>;
    string filename<NEportLevels={2,0}>;
    string text<NEportLevels={0,2}>[];
  };
  module k_replicate<build_dir="avs/src/core/render",
    src_file="express.cxx",
    cxx_hdr_files="fld/Xfld.h",
    out_src_file="krep_gen.cxx",
    out_hdr_file="krep_gen.hxx"> {
    cxxmethod+notify_inst transform(na+read+notify+req,
				    nb+read+notify+req,
				    nc+read+notify+req,
				    sa+read+req,
				    sb+read+req,
				    sc+read+req,
				    n+write,
				    Xform+write);
    int na<NEportLevels={2,0}>;
    int nb<NEportLevels={2,0}>;
    int nc<NEportLevels={2,0}>;
    int sa<NEportLevels={2,0}>;
    int sb<NEportLevels={2,0}>;
    int sc<NEportLevels={2,0}>;
    int n<NEportLevels={0,2}>;
    Xform Xform<NEportLevels={0,2}>[];
  };
};
