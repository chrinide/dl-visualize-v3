
flibrary Objects<NEeditable=1> {
  module create_point<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    need_objs="CCP3.Renderers.Points.point CCP3.Renderers.Points.point_data",
    out_src_file="cpnt_gen.cxx",
    out_hdr_file="cpnt_gen.hxx"> {
    cxxmethod+req create<status=1>(name+read+req,
				   space+read+req,
				   trigger+read+req+notify);
    cxxmethod+req cancel<status=1>(trigger+read+req,
				   reject+notify);
    cxxmethod+req accept<status=1>(ok+notify);
    string name<NEportLevels={2,0}>;
    int space<NEportLevels={2,0}>;
    int trigger<NEportLevels={2,0}>;
    int reject<NEportLevels={2,0}>;
    int ok<NEportLevels={2,0}>;
  };
  module update_point<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    out_src_file="upnt_gen.cxx",
    out_hdr_file="upnt_gen.hxx"> {
    cxxmethod+req set_fractional<status=1>(fractional+notify,
					   x+read+req,
					   y+read+req,
					   z+read+req,
					   conventional+read+req);
    cxxmethod+req set_cartesian<status=1>(cartesian+notify,
					  x+read+req,
					  y+read+req,
					  z+read+req);
    int fractional<NEportLevels={2,0}>;
    int cartesian<NEportLevels={2,0}>;
    float x<NEportLevels={2,0}>;
    float y<NEportLevels={2,0}>;
    float z<NEportLevels={2,0}>;
    int conventional<NEportLevels={2,0}>;
  };
  module create_line<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    need_objs="CCP3.Renderers.Lines.line CCP3.Renderers.Lines.line_data",
    out_src_file="clne_gen.cxx",
    out_hdr_file="clne_gen.hxx"> {
    cxxmethod+req create<status=1>(name+read+req,
				   space+read+req,
				   trigger+read+req+notify);
    cxxmethod+req cancel<status=1>(trigger+read+req,
				   reject+notify);
    cxxmethod+req accept<status=1>(ok+notify);
    string name<NEportLevels={2,0}>;
    int space<NEportLevels={2,0}>;
    int trigger<NEportLevels={2,0}>;
    int reject<NEportLevels={2,0}>;
    int ok<NEportLevels={2,0}>;
  };
  module update_line<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    out_src_file="ulne_gen.cxx",
    out_hdr_file="ulne_gen.hxx"> {
    cxxmethod+req inherit<status=1>(object+read+req,
				    copy_from+read+req+notify);
    cxxmethod+req update_atom_oa<status=1>(atom_oa+read+req+notify);
    cxxmethod+req update_atom_o<status=1>(atom_o+read+req+notify);
    cxxmethod+req update_atom_a<status=1>(atom_a+read+req+notify);
    cxxmethod+req update_coord_o<status=1>(coord_o+read+req+notify,
					   x+read+req,
					   y+read+req,
					   z+read+req,
					   fractional+read+req,
					   conventional+read+req,
					   parent_lattice+read+req);
    cxxmethod+req update_coord_a<status=1>(coord_a+read+req+notify,
					   x+read+req,
					   y+read+req,
					   z+read+req,
					   fractional+read+req,
					   conventional+read+req,
					   parent_lattice+read+req);
    cxxmethod+req set_miller<status=1>(miller+read+req+notify,
				       h+read+req,
				       k+read+req,
				       l+read+req,
				       conventional+read+req,
				       parent_lattice+read+req);
    int parent_lattice<NEportLevels={2,0}>;
    int object<NEportLevels={2,0}>;
    int copy_from<NEportLevels={2,0}>;
    int coord_o<NEportLevels={2,0}>;
    int coord_a<NEportLevels={2,0}>;
    float x<NEportLevels={2,0}>;
    float y<NEportLevels={2,0}>;
    float z<NEportLevels={2,0}>;
    int fractional<NEportLevels={2,0}>;
    int atom_oa<NEportLevels={2,0}>;
    int atom_o<NEportLevels={2,0}>;
    int atom_a<NEportLevels={2,0}>;
    int miller<NEportLevels={2,0}>;
    int h<NEportLevels={2,0}>;
    int k<NEportLevels={2,0}>;
    int l<NEportLevels={2,0}>;
    int conventional<NEportLevels={2,0}>;
  };
  module create_plane<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    need_objs="CCP3.Renderers.Planes.plane CCP3.Renderers.Planes.plane_data",
    out_src_file="cpln_gen.cxx",
    out_hdr_file="cpln_gen.hxx"> {
    cxxmethod+req create<status=1>(name+read+req,
				   space+read+req,
				   trigger+read+req+notify);
    cxxmethod+req cancel<status=1>(trigger+read+req,
				   reject+notify);
    cxxmethod+req accept<status=1>(ok+notify);
    string name<NEportLevels={2,0}>;
    int space<NEportLevels={2,0}>;
    int trigger<NEportLevels={2,0}>;
    int reject<NEportLevels={2,0}>;
    int ok<NEportLevels={2,0}>;
  };
  module update_plane<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    out_src_file="upln_gen.cxx",
    out_hdr_file="upln_gen.hxx"> {
    cxxmethod+req inherit<status=1>(object+read+req,
				    copy_from+req+notify);
    cxxmethod+req update_oa<status=1>(oa+req+notify);
    cxxmethod+req update_ob<status=1>(ob+req+notify);
    cxxmethod+req update_a<status=1>(a+req+notify);
    cxxmethod+req update_b<status=1>(b+req+notify);
    cxxmethod+req set_origin<status=1>(origin+req+notify);
    cxxmethod+req set_centre<status=1>(centre+req+notify);
    cxxmethod+req set_miller<status=1>(miller+req+notify,
				       h+read+req,
				       k+read+req,
				       l+read+req,
				       conventional+read+req);
    cxxmethod+req align_l<status=1>(align_lattice+req+notify,
				    conventional+read+req,
				    parent_obj+read+req);
    cxxmethod+req align_c<status=1>(align_xyz+req+notify,
				    parent_obj+read+req);
    cxxmethod+req set_l<status=1>(lattice+req+notify,
				  conventional+read+req,
				  parent_obj+read+req);
    cxxmethod+req set_c<status=1>(cartesian+req+notify,
				  conventional+read+req,
				  parent_obj+read+req);
    cxxmethod+req update_boa<status=1>(boa+read+req+notify);
    cxxmethod+req coord_o<status=1>(pos_o+req+notify,
				    x+read+req,
				    y+read+req,
				    z+read+req,
				    frac+read+req,
				    conventional+read+req,
				    parent_obj+read+req);
    cxxmethod+req coord_a<status=1>(pos_a+req+notify,
				    x+read+req,
				    y+read+req,
				    z+read+req,
				    frac+read+req,
				    conventional+read+req,
				    parent_obj+read+req);
    cxxmethod+req coord_b<status=1>(pos_b+req+notify,
				    x+read+req,
				    y+read+req,
				    z+read+req,
				    frac+read+req,
				    conventional+read+req,
				    parent_obj+read+req);
    cxxmethod+req coord_centre<status=1>(pcentre+req+notify,
					 x+read+req,
					 y+read+req,
					 z+read+req,
					 frac+read+req,
					 conventional+read+req,
					 parent_obj+read+req);
    int object<NEportLevels={2,0}>;
    int copy_from<NEportLevels={2,0}>;
    int oa<NEportLevels={2,0}>;
    int ob<NEportLevels={2,0}>;
    int a<NEportLevels={2,0}>;
    int b<NEportLevels={2,0}>;
    int origin<NEportLevels={2,0}>;
    int centre<NEportLevels={2,0}>;
    int miller<NEportLevels={2,0}>;
    int h<NEportLevels={2,0}>;
    int k<NEportLevels={2,0}>;
    int l<NEportLevels={2,0}>;
    int align_lattice<NEportLevels={2,0}>;
    int align_xyz<NEportLevels={2,0}>;
    int lattice<NEportLevels={2,0}>;
    int cartesian<NEportLevels={2,0}>;
    int conventional<NEportLevels={2,0}>;
    int boa<NEportLevels={2,0}>;
    float x<NEportLevels={2,0}>;
    float y<NEportLevels={2,0}>;
    float z<NEportLevels={2,0}>;
    int pos_a<NEportLevels={2,0}>;
    int pos_b<NEportLevels={2,0}>;
    int pos_o<NEportLevels={2,0}>;
    int pcentre<NEportLevels={2,0}>;
    int frac<NEportLevels={2,0}>;
    int parent_obj<NEportLevels={2,0}>;
  };
  module create_box<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    need_objs="CCP3.Renderers.Volumes.region3D CCP3.Renderers.Volumes.box_data",
    out_src_file="cbox_gen.cxx",
    out_hdr_file="cbox_gen.hxx"> {
    cxxmethod+req create<status=1>(name+read+req,
				   space+read+req,
				   trigger+read+req+notify);
    cxxmethod+req cancel<status=1>(trigger+read+req,
				   reject+notify);
    cxxmethod+req accept<status=1>(ok+notify);
    string name<NEportLevels={2,0}>;
    int space<NEportLevels={2,0}>;
    int trigger<NEportLevels={2,0}>;
    int reject<NEportLevels={2,0}>;
    int ok<NEportLevels={2,0}>;
  };
  module update_box<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    out_src_file="ubox_gen.cxx",
    out_hdr_file="ubox_gen.hxx"> {
    cxxmethod+req inherit<status=1>(object+read+req,
				    copy_from+req+notify);
    cxxmethod+req update_oa<status=1>(oa+req+notify);
    cxxmethod+req update_ob<status=1>(ob+req+notify);
    cxxmethod+req update_oc<status=1>(ob+req+notify);
    cxxmethod+req update_a<status=1>(a+req+notify);
    cxxmethod+req update_b<status=1>(b+req+notify);
    cxxmethod+req update_c<status=1>(b+req+notify);
    cxxmethod+req set_origin<status=1>(origin+req+notify);
    cxxmethod+req set_centre<status=1>(centre+req+notify);
    cxxmethod+req set_miller<status=1>(miller+req+notify,
				       h+read+req,
				       k+read+req,
				       l+read+req,
				       conventional+read+req);
    cxxmethod+req align_l<status=1>(align_lattice+req+notify,
				    conventional+read+req,
				    parent_obj+read+req);
    cxxmethod+req align_c<status=1>(align_xyz+req+notify,
				    parent_obj+read+req);
    cxxmethod+req set_l<status=1>(lattice+req+notify,
				  conventional+read+req,
				  parent_obj+read+req);
    cxxmethod+req set_c<status=1>(cartesian+req+notify,
				  conventional+read+req,
				  parent_obj+read+req);
    cxxmethod+req coord_o<status=1>(pos_o+req+notify,
				    x+read+req,
				    y+read+req,
				    z+read+req,
				    frac+read+req,
				    conventional+read+req,
				    parent_obj+read+req);
    cxxmethod+req coord_a<status=1>(pos_a+req+notify,
				    x+read+req,
				    y+read+req,
				    z+read+req,
				    frac+read+req,
				    conventional+read+req,
				    parent_obj+read+req);
    cxxmethod+req coord_b<status=1>(pos_b+req+notify,
				    x+read+req,
				    y+read+req,
				    z+read+req,
				    frac+read+req,
				    conventional+read+req,
				    parent_obj+read+req);
    cxxmethod+req coord_c<status=1>(pos_c+req+notify,
				    x+read+req,
				    y+read+req,
				    z+read+req,
				    frac+read+req,
				    conventional+read+req,
				    parent_obj+read+req);
    cxxmethod+req coord_centre<status=1>(pcentre+req+notify,
					 x+read+req,
					 y+read+req,
					 z+read+req,
					 frac+read+req,
					 conventional+read+req,
					 parent_obj+read+req);
    int object<NEportLevels={2,0}>;
    int copy_from<NEportLevels={2,0}>;
    int oa<NEportLevels={2,0}>;
    int ob<NEportLevels={2,0}>;
    int oc<NEportLevels={2,0}>;
    int a<NEportLevels={2,0}>;
    int b<NEportLevels={2,0}>;
    int c<NEportLevels={2,0}>;
    int origin<NEportLevels={2,0}>;
    int centre<NEportLevels={2,0}>;
    int miller<NEportLevels={2,0}>;
    int h<NEportLevels={2,0}>;
    int k<NEportLevels={2,0}>;
    int l<NEportLevels={2,0}>;
    int align_lattice<NEportLevels={2,0}>;
    int align_xyz<NEportLevels={2,0}>;
    int lattice<NEportLevels={2,0}>;
    int cartesian<NEportLevels={2,0}>;
    int conventional<NEportLevels={2,0}>;
    float x<NEportLevels={2,0}>;
    float y<NEportLevels={2,0}>;
    float z<NEportLevels={2,0}>;
    int pos_a<NEportLevels={2,0}>;
    int pos_b<NEportLevels={2,0}>;
    int pos_o<NEportLevels={2,0}>;
    int pos_c<NEportLevels={2,0}>;
    int pcentre<NEportLevels={2,0}>;
    int frac<NEportLevels={2,0}>;
    int parent_obj<NEportLevels={2,0}>;
  };
  module list_lines<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    out_src_file="llne_gen.cxx",
    out_hdr_file="llne_gen.hxx"> {
    cxxmethod+req generate<status=1>(trigger+notify,
				     labels+write);
    int trigger<NEportLevels={2,0}>;
    string labels<NEportLevels={2,0}>[];
  };
  module list_planes<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    out_src_file="lpln_gen.cxx",
    out_hdr_file="lpln_gen.hxx"> {
    cxxmethod+req generate<status=1>(trigger+notify,
				     labels+write);
    int trigger<NEportLevels={2,0}>;
    string labels<NEportLevels={2,0}>[];
  };
  module list_volumes<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    out_src_file="lbox_gen.cxx",
    out_hdr_file="lbox_gen.hxx"> {
    cxxmethod+req generate<status=1>(trigger+notify,
				     labels+write);
    int trigger<NEportLevels={2,0}>;
    string labels<NEportLevels={2,0}>[];
  };
  module list_wulff<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    out_src_file="lwlf_gen.cxx",
    out_hdr_file="lwlf_gen.hxx"> {
    cxxmethod+req+notify_inst generate<status=1>(trigger+notify,
						 labels+write);
    int trigger<NEportLevels={2,0}>;
    string labels<NEportLevels={2,0}>[];
  };
  group wulff_data {
    int h;
    int k;
    int l;
    float energy;
    float r;
    float g;
    float b;
    boolean conventional;
    string filename;
  };
  module create_wulff<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    need_objs="CCP3.Renderers.Volumes.wulff_plot CCP3.Renderers.Volumes.wulff_plot_data",
    out_src_file="wlff_gen.cxx",
    out_hdr_file="wlff_gen.hxx"> {
    cxxmethod+req create<status=1>(name+read+req,
				   trigger+read+req+notify);
    cxxmethod+req add<status=1>(do_add+req+notify,
				data+read+req);
    cxxmethod+req load<status=1>(do_load+req+notify,
				 data+read+req);
    cxxmethod+req cancel<status=1>(trigger+read+req,
				   reject+notify);
    cxxmethod+req accept<status=1>(ok+notify);
    string name<NEportLevels={2,0}>;
    int trigger<NEportLevels={2,0}>;
    int do_add<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
    wulff_data &data<NEportLevels={2,0}>;
    int reject<NEportLevels={2,0}>;
    int ok<NEportLevels={2,0}>;
  };
  module create_leed<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    need_objs="CCP3.Renderers.Plots.LEED_pattern CCP3.Renderers.Plots.leed_data",
    out_src_file="leed_gen.cxx",
    out_hdr_file="leed_gen.hxx"> {
    cxxmethod+req create<status=1>(trigger+read+req+notify);
    cxxmethod+req update<status=1>(domains+read+req+notify);
    cxxmethod+req cancel<status=1>(trigger+read+req,
				   reject+notify);
    cxxmethod+req accept<status=1>(ok+notify);
    int trigger<NEportLevels={2,0}>;
    int domains<NEportLevels={2,0}>;
    int reject<NEportLevels={2,0}>;
    int ok<NEportLevels={2,0}>;
  };
  module edit_object<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    out_src_file="edit_gen.cxx",
    out_hdr_file="edit_gen.hxx"> {
    cxxmethod+req attach(trigger+read+notify+req,
			 active+read+req);
    int trigger<NEportLevels={2,0}>;
    int active<NEportLevels={2,0}>;
  };
  module create_sphere<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    need_objs="CCP3.Renderers.Volumes.sphere CCP3.Renderers.Volumes.sphere_data",
    out_src_file="csph_gen.cxx",
    out_hdr_file="csph_gen.hxx"> {
    cxxmethod+req create<status=1>(name+read+req,
				   space+read+req,
				   trigger+read+req+notify);
    cxxmethod+req cancel<status=1>(trigger+read+req,
				   reject+notify);
    cxxmethod+req accept<status=1>(ok+notify);
    string name<NEportLevels={2,0}>;
    int space<NEportLevels={2,0}>;
    int trigger<NEportLevels={2,0}>;
    int reject<NEportLevels={2,0}>;
    int ok<NEportLevels={2,0}>;
  };
  module update_sphere<build_dir="avs/src/core/objects",
    src_file="express.cxx",
    out_src_file="usph_gen.cxx",
    out_hdr_file="usph_gen.hxx"> {
    cxxmethod+req set_origin<status=1>(origin+read+req+notify);
    cxxmethod+req set_radius<status=1>(radius+read+req+notify);
    int origin<NEportLevels={2,0}>;
    float radius<NEportLevels={2,0}>;
  };
};
