
flibrary Core_Modules<cxx_name="core"
#ifndef DLV_NO_DLLS
		      ,dyn_libs="libCCP3dlv"
#endif // DLV_NO_DLLS
		      > {
  "../avs/v/CCP3/modules/core/exit.v" Exit;
  "../avs/v/CCP3/modules/core/init.v" Init;
  "../avs/v/CCP3/modules/core/demo.v" Demo;
  "../avs/v/CCP3/modules/core/job_list.v" JobInfo;
  "../avs/v/CCP3/modules/core/job_prefs.v" JobPrefs;
  "../avs/v/CCP3/modules/core/model.v" Model;
  "../avs/v/CCP3/modules/core/objects.v" Objects;
  "../avs/v/CCP3/modules/core/prefs.v" Prefs;
  "../avs/v/CCP3/modules/core/project.v" Project;
  "../avs/v/CCP3/modules/core/render.v" Render;
  "../avs/v/CCP3/modules/core/view_data.v" ViewData;
  "../avs/v/CCP3/modules/core/edit_data.v" EditData;
  "../avs/v/CCP3/modules/core/calcs.v" Calcs;
  "../avs/v/CCP3/modules/core/data.v" Data;
  "../avs/v/CCP3/modules/core/utils.v" Utils;
};
