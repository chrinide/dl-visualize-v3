
flibrary Calcs<NEeditable=1> {
  module exec_environ<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="env_gen.cxx",
    out_hdr_file="env_gen.hxx"> {
    cxxmethod+req+notify_inst init(default_var+read+req,
				   search_path+read+req,
				   main_var+read+req,
				   binary+read+req,
				   location+write);
    cxxmethod+req set(location+read+req+notify);
    string default_var;
    boolean search_path;
    string main_var;
    string binary;
    string location<NEportLevels={2,0}>;
  };
};
