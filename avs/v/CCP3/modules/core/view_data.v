
flibrary ViewData<NEeditable=1> {
  module display_data<build_dir="avs/src/core/properties",
    src_file="express.cxx",
    libdeps="AG",
    need_objs="CCP3.Viewers.Scenes.DLV2Dscene Field_Rect Field_Struct CCP3.Renderers.Volumes.isosurface CCP3.Renderers.Volumes.isosurface_rep CCP3.Renderers.Points.text_message CCP3.Renderers.Points.real_scalar CCP3.Renderers.Points.text_message CCP3.Renderers.Plots.plot_dos CCP3.Renderers.Plots.plot_band CCP3.Renderers.Plots.plot_band_dos CCP3.Renderers.Plots.dos_data CCP3.Renderers.Plots.band_data CCP3.Renderers.Points.file_view_data CCP3.Renderers.Points.file_viewer CCP3.Renderers.Volumes.iso_wavefn CCP3.Renderers.Volumes.iso_wavefn_rep CCP3.Renderers.Planes.AGisoline CCP3.Renderers.Planes.AGisoline_rep CCP3.Renderers.Planes.isoline CCP3.Renderers.Planes.isoline_rep CCP3.Renderers.Planes.disoline CCP3.Renderers.Planes.disoline_rep CCP3.Renderers.Planes.solid_contour CCP3.Renderers.Planes.solid_contour_rep CCP3.Renderers.Planes.shaded_contour CCP3.Renderers.Planes.shaded_contour_rep CCP3.Renderers.Planes.surf_plot CCP3.Renderers.Planes.surf_plot_rep CCP3.Core_Modules.Render.attach_plane CCP3.Core_Modules.Render.plane_to_2Dstruct CCP3.Core_Modules.Render.slice_to_2D CCP3.Core_Modules.Render.copy_slice CCP3.Core_Modules.Render.copy_rect CCP3.Renderers.Points.vector_glyphs CCP3.Renderers.Points.trajectory CCP3.Renderers.Points.labels CCP3.Renderers.Plots.plot_bspin CCP3.Renderers.Points.buffer_view_data CCP3.Renderers.Points.buffer_view CCP3.Renderers.Plots.plot_rod1d CCP3.Renderers.Plots.plot_rod2d CCP3.Renderers.Plots.multi_grid_data",
    out_src_file="disp_gen.cxx",
    out_hdr_file="disp_gen.hxx"> {
    cxxmethod+req display<status=1>(render+notify,
				    object+read+req,
				    component+read+req,
				    display_type+read);
    int render<NEportLevels={2,0}>;
    int object<NEportLevels={2,0}>;
    int component<NEportLevels={2,0}>;
    int display_type<NEportLevels={2,0}>;
  };
  module select_display<build_dir="avs/src/core/properties",
    src_file="express.cxx",
    out_src_file="sel_gen.cxx",
    out_hdr_file="sel_gen.hxx"> {
    cxxmethod+req display<status=1>(object+read+notify+req);
    int object<NEportLevels={2,0}>;
  };  
  module hide_display<build_dir="avs/src/core/properties",
    src_file="express.cxx",
    out_src_file="hide_gen.cxx",
    out_hdr_file="hide_gen.hxx"> {
    cxxmethod+req update<status=1>(visible+read+req+notify,
				   object+read+req);
    int visible<NEportLevels={2,0}>;
    int object<NEportLevels={2,0}>;
  };
  module delete_display<build_dir="avs/src/core/properties",
    src_file="express.cxx",
    out_src_file="del_gen.cxx",
    out_hdr_file="del_gen.hxx"> {
    cxxmethod+req remove<status=1>(trigger+notify,
				   object+read+req);
    int trigger<NEportLevels={2,0}>;
    int object<NEportLevels={2,0}>;
  };  
  module update_display<build_dir="avs/src/core/properties",
    src_file="express.cxx",
    out_src_file="upd_gen.cxx",
    out_hdr_file="upd_gen.hxx"> {
    cxxmethod+req update<status=1>(object+read+req,
				   plane+read+req+notify);
    int object<NEportLevels={2,0}>;
    int plane<NEportLevels={2,0}>;
  };
  module delete_data<build_dir="avs/src/core/properties",
    src_file="express.cxx",
    out_src_file="rem_gen.cxx",
    out_hdr_file="rem_gen.hxx"> {
    cxxmethod+req remove<status=1>(trigger+notify,
				   object+read+req);
    int trigger<NEportLevels={2,0}>;
    int object<NEportLevels={2,0}>;
  };  
};
