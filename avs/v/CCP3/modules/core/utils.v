
flibrary Utils<NEeditable=1> {
  module select_str_arr<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="str_gen.cxx",
    out_hdr_file="str_gen.hxx"> {
    cxxmethod+req+notify_inst select(selections+read+notify+req,
				     in_strings+read+req,
				     out_strings+write);
    int selections<NEportLevels={2,0}>[];
    string in_strings<NEportLevels={2,0}>[];
    string out_strings<NEportLevels={0,2}>[];
  };
  module map_selection<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="map_gen.cxx",
    out_hdr_file="map_gen.hxx"> {
    cxxmethod+req+notify_inst map(selections+read+notify+req,
				  selected_item+read+notify+req,
				  index+write);
    int selections<NEportLevels={2,0}>[];
    int selected_item<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
  };
  /*group FBdata {
    link parent1;
    link parent2;
    string filename;
    string title;
    string pattern;
    int file_write;
    int x;
    int y;
#ifdef MSDOS // Todo - set for MSDOS?
    int height;
#else // height matches DLVtext object height
    int height = 34;
#endif // MSDOS
    boolean onlyButton;
    string label;
    int width;
    int ok;
    };*/
  /*module manageFB<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="fb_gen.cxx",
    out_hdr_file="fb_gen.hxx"> {
    cxxmethod+req+notify_inst+notify_deinst manage(connect+read+req+notify,
						   data+read+req);
    int connect<NEportLevels={2,0}>;
    FBdata &data<NEportLevels={2,0}>;
    };*/
  module manageFB<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="fb_gen.cxx",
    out_hdr_file="fb_gen.hxx"> {
    cxxmethod+req manage(trigger+req+read+notify,
			 filename+req+notify+read,
			 wildcard+read+req,
			 funit+read+req,
			 use_funit+read+req,
			 pattern+write);
    int trigger<NEportLevels={2,0}>;
    string filename<NEportLevels={2,0}>;
    string wildcard<NEportLevels={2,0}>;
    string funit;
    int use_funit<NEportLevels={2,0}>;
    string pattern<NEportLevels={0,2}>;
  };
  module browse<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="http_gen.cxx",
    out_hdr_file="http_gen.hxx"> {
    cxxmethod+req open(trigger+notify+read+req,
		       run+read+write+req,
		       monitor+read+req,
		       browser+read,
		       url+req+read);
    int trigger<NEportLevels={2,0}>;
    int run;
    int monitor;
    string browser<NEportLevels={2,0}>;
    string url<NEportLevels={2,0}>;
  };
  module toggle_renderer<build_dir="avs/src/core",
			 src_file="express.cxx",
			 out_src_file="rend_gen.cxx",
			 out_hdr_file="rend_gen.hxx"> {
    cxxmethod+req toggle(trigger+notify+read+req,
			 value+read+req,
			 prev+read+write,
			 run+write);
    int trigger<NEportLevels={2,0}>;
    int value;
    int prev;
    int run<NEportLevels={0,2}>;
  };
};
