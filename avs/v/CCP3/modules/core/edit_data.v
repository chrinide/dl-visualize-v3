
flibrary EditData<NEeditable=1> {
  module edit_data<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    need_objs="CCP3.Renderers.Edits.orthoslice CCP3.Renderers.Edits.spectrum_data CCP3.Renderers.Edits.phonon_trajectory CCP3.Renderers.Edits.edit_dos CCP3.Renderers.Edits.edit_bands CCP3.Renderers.Edits.data_math CCP3.Renderers.Edits.clamp CCP3.Renderers.Edits.downsize CCP3.Renderers.Edits.cut CCP3.Renderers.Edits.slice CCP3.Renderers.Edits.extend_obj CCP3.Renderers.Edits.crop",
    out_src_file="edit_gen.cxx",
    out_hdr_file="edit_gen.hxx"> {
    cxxmethod+req edit<status=1>(render+notify,
				 object+read+req,
				 component+read+req,
				 edit_type+read);
    int render<NEportLevels={2,0}>;
    int object<NEportLevels={2,0}>;
    int component<NEportLevels={2,0}>;
    int edit_type<NEportLevels={2,0}>;
  };
  module select_edit<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    out_src_file="sel_gen.cxx",
    out_hdr_file="sel_gen.hxx"> {
    cxxmethod+req edit<status=1>(object+read+notify+req);
    int object<NEportLevels={2,0}>;
  };
  module delete_edit<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    out_src_file="del_gen.cxx",
    out_hdr_file="del_gen.hxx"> {
    cxxmethod+req remove<status=1>(trigger+notify,
				   object+read+req);
    int trigger<NEportLevels={2,0}>;
    int object<NEportLevels={2,0}>;
  };
  group spectral_data {
    int index;
    int ir_or_raman;
    float emin;
    float emax;
    int npoints;
    float broaden;
    float harmonic_scale;
  };
  module update_spectrum<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    out_src_file="spec_gen.cxx",
    out_hdr_file="spec_gen.hxx"> {
    cxxmethod+req update<status=1>(data+read+req+notify);
    spectral_data &data<NEportLevels={2,0}>;
  };
  group phonon_data {
    int index;
    int nframes;
    float magnitude;
    int use_temp;
    float temperature;
  };
  module update_phonon<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    out_src_file="phon_gen.cxx",
    out_hdr_file="phon_gen.hxx"> {
    cxxmethod+req update<status=1>(data+read+req+notify);
    phonon_data &data<NEportLevels={2,0}>;
  };
  module update_slice<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    out_src_file="slce_gen.cxx",
    out_hdr_file="slce_gen.hxx"> {
    cxxmethod+req update<status=1>(object+read+req,
				   plane+read+req+notify);
    int object<NEportLevels={2,0}>;
    int plane<NEportLevels={2,0}>;
  };
  module update_cut<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    out_src_file="cut_gen.cxx",
    out_hdr_file="cut_gen.hxx"> {
    cxxmethod+req update<status=1>(object+read+req,
				   box+read+req+notify);
    int object<NEportLevels={2,0}>;
    int box<NEportLevels={2,0}>;
  };
  module update_extension<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    out_src_file="extd_gen.cxx",
    out_hdr_file="extd_gen.hxx"> {
    cxxmethod+req update<status=1>(object+read+req,
				   a+read+req+notify,
				   b+read+req+notify,
				   c+read+req+notify);
    int object<NEportLevels={2,0}>;
    int a<NEportLevels={2,0}>;
    int b<NEportLevels={2,0}>;
    int c<NEportLevels={2,0}>;
  };
  module related_properties<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    out_hdr_file="rel_gen.hxx",
    out_src_file="rel_gen.cxx"> {
    cxxmethod+req+notify_inst list(selection+read+notify+req,
				   trust_user+read+notify+req,
				   ptype+read+req,
				   labels+write);
    int selection<NEportLevels={2,0}>;
    int trust_user<NEportLevels={2,0}>;
    int ptype;
    string labels<NEportLevels={0,2}>[];
  };
  module update_math<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    out_hdr_file="math_gen.hxx",
    out_src_file="math_gen.cxx"> {
    cxxmethod+req update(object+read+req,
			 selection+read+notify+req,
			 trust_user+read+req,
			 index+read+req);
    int object<NEportLevels={2,0}>;
    int selection<NEportLevels={2,0}>;
    int trust_user<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
  };
  module update_reals<build_dir="avs/src/core/edits",
    src_file="express.cxx",
    out_hdr_file="real_gen.hxx",
    out_src_file="real_gen.cxx"> {
    cxxmethod+req update(object+read+req,
			 shift+read+notify+req,
			 selection+read+notify,
			 use_prop+read+req);
    int object<NEportLevels={2,0}>;
    float shift<NEportLevels={2,0}>;
    int selection<NEportLevels={2,0}>;
    int use_prop<NEportLevels={2,0}>;
  };
};
