
flibrary JobInfo<NEeditable=1> {
  module job_select<build_dir="avs/src/core/jobs",
    src_file="express.cxx",
    out_hdr_file="sel_gen.hxx",
    out_src_file="sel_gen.cxx"> {
    cxxmethod+req select(selection+read,
			 trigger1+notify,
			 trigger2+notify,
			 info+write,
			 file_list+write,
			 active_list+write,
			 active_recover+write,
			 active_log+write,
			 is_running+write);
    int selection<NEportLevels={2,0}>;
    int trigger1<NEportLevels={2,0}>;
    int trigger2<NEportLevels={2,0}>;
    string info<NEportLevels={0,2}>;
    string file_list<NEportLevels={0,2}>[];
    int active_list<NEportLevels={0,2}>;
    int active_recover<NEportLevels={0,2}>;
    int active_log<NEportLevels={0,2}>;
    int is_running<NEportLevels={0,2}>;
  };
  module job_recover<build_dir="avs/src/core/jobs",
    src_file="express.cxx",
    out_hdr_file="rcv_gen.hxx",
    out_src_file="rcv_gen.cxx"> {
    cxxmethod+req recover(selection+read+req,
			  do+notify+req,
			  file_list+read,
			  use_list+read+req,
			  update+write);
    int selection<NEportLevels={2,0}>;
    int do<NEportLevels={2,0}>;
    int file_list<NEportLevels={2,0}>[];
    int use_list<NEportLevels={2,0}>;
    int update<NEportLevels={0,2}>;
  };
  module job_kill<build_dir="avs/src/core/jobs",
    src_file="express.cxx",
    out_hdr_file="kill_gen.hxx",
    out_src_file="kill_gen.cxx"> {
    cxxmethod+req kill(selection+read+req,
		       do+notify+req);
    int selection<NEportLevels={2,0}>;
    int do<NEportLevels={2,0}>;
  };
  module job_delete<build_dir="avs/src/core/jobs",
    src_file="express.cxx",
    out_hdr_file="del_gen.hxx",
    out_src_file="del_gen.cxx"> {
    cxxmethod+req remove(selection+read+req,
			 do+notify+req);
    int selection<NEportLevels={2,0}>;
    int do<NEportLevels={2,0}>;
  };
};
