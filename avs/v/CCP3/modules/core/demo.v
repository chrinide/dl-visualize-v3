
flibrary Demo<NEeditable=1> {
  module DLV_demo<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="demo_gen.cxx",
    out_hdr_file="demo_gen.hxx"> {
    cxxmethod+notify_inst check(month+req+read,
                                year+req+read,
                                expired+write,
                                warning+write);
    int month<NEportLevels={2,0}>;
    int year<NEportLevels={2,0}>;
    int expired<NEportLevels={0,2}>;
    int warning<NEportLevels={0,2}>;
  };
};
