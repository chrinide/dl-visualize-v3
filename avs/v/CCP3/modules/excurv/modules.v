
flibrary Modules<NEeditable=1
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3excurv"
#endif // DLV_NO_DLLS
		 > {
  // Import Stanko's code from DLV v2.5
  module close_excurv<build_dir="avs/src/excurv",
    //cxx_hdr_files="../../src/excurv/calcs.hxx",
    out_src_file="end_gen.cxx",
    out_hdr_file="end_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req close<status=1>(trigger+notify+read);
    int trigger<NEportLevels={2,0}>;
  };
  module create_model<build_dir="avs/src/excurv",
    out_src_file="make_gen.cxx",
    out_hdr_file="make_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req create<status=1>(atom+read+req+notify,
				   model_build+read+req);
    int atom<NEportLevels={2,0}>;
    int model_build<NEportLevels={2,0}>;
  };
  group expt_data_type {
    string file_name<NEportLevels={1,1},NEx=715.,NEy=539.>;
    int sampling_frequency<NEportLevels={1,1},NEx=704.,NEy=297.>;
    int selected_button<NEportLevels={1,1},NEx=528.,NEy=550.>;
    int x_column<NEportLevels={1,1},NEx=396.,NEy=385.>;
    int y_column<NEportLevels={1,1},NEx=242.,NEy=528.>;
  };
  module read_expt<build_dir="avs/src/excurv",
    //cxx_hdr_files="../../src/excurv/calcs.hxx",
    out_src_file="expt_gen.cxx",
    out_hdr_file="expt_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx",
    need_objs="CCP3.Renderers.Model.Shell",
    src_file="express.cxx"> {
    cxxmethod+req read<status=1>(trigger+notify+read+req,
				 kweight+read+req,
				 atom_type+read+req,
				 edge+read,
				 data+read+req,
				 prog_ok+write,
				 job_data+read+req);
    int trigger<NEportLevels={2,0}>;
    int kweight<NEportLevels={2,0}>;
    int atom_type<NEportLevels={2,0}>;
    string edge<NEportLevels={2,0}>;
    expt_data_type &data<NEportLevels={2,0}>;
    int prog_ok<NEportLevels={0,2}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  group neighbour_data {
    int atom_type;
    int neighbour;
    string atom_symbol;
    string filename;
  };
  group pot_data_type {
    int central_neighb;
    int no_atom_types;
    neighbour_data atom_pairs[no_atom_types];
    int constant_V0;
    int method;
    string filename;
    int SPR_KKR_Potential_File;
  };
  module pot_phase<build_dir="avs/src/excurv",
    //cxx_hdr_files="../../src/excurv/calcs.hxx",
    out_src_file="pot_gen.cxx",
    out_hdr_file="pot_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req calc<status=1>(trigger+notify,
				 atom_type+read+req,
				 data+read+req);
    int trigger<NEportLevels={2,0}>;
    int atom_type<NEportLevels={2,0}>;
    pot_data_type &data<NEportLevels={2,0}>;
  };
  module plot<build_dir="avs/src/excurv",
    //cxx_hdr_files="../../src/excurv/calcs.hxx",
    out_src_file="plot_gen.cxx",
    out_hdr_file="plot_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req plot<status=1>(do_plot+notify,
				 x+read+req,
				 y+read+req);
    cxxmethod+req print<status=1>(do_print+notify,
				  filename+read+req,
				  x+read+req,
				  y+read+req);
    int do_plot<NEportLevels={2,0}>;
    int do_print<NEportLevels={2,0}>;
    string filename<NEportLevels={2,0}>;
    int x<NEportLevels={2,0}>;
    int y<NEportLevels={2,0}>[];
  };
  module save_refine<build_dir="avs/src/excurv",
    //cxx_hdr_files="../../src/excurv/calcs.hxx",
    out_src_file="save_gen.cxx",
    out_hdr_file="save_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req save<status=1>(do_print+notify,
				 filename+read+req);
    int do_print<NEportLevels={2,0}>;
    string filename<NEportLevels={2,0}>;
  };
  group shell_data {
    int atom_type = 0;
    float number_of_atoms = 1;
    int refine_number = 0;
    float radius = 1.0;
    int refine_radius = 0;
    float debye_waller = 0.01;
    int refine_dw = 0;
    // Info for multiple scattering to position atoms, default (r, 0, 0)
    float theta = 90.0;
    float phi = 0.0;
  };
  group refinement_data {
    int number_of_shells;
    int step_size;
    int number_of_iterations;
    float central_dw;
    int refine_cdw;
    int user_defined_ref;
    int refine_all_atom_num;
    int refine_all_radii;
    int refine_all_dw;
    int refine_efermi;
    int correlations;
    shell_data shells[number_of_shells];
    float fermi_energy;
    string fit_factor;
    string R_factor;
    int max_shells;
    int multiple_scattering;
    int max_atoms_path;
    float max_path_length;
    float float_plmin;
    float float_minang;
    float float_minmag;
    int int_dlmax;
    int int_tlmax;
    int int_numax;
    int int_omin;
    int int_omax;
    float float_output;
    int theory;
    string symmetry;
  };
  module refine<build_dir="avs/src/excurv",
    //cxx_hdr_files="../../src/excurv/calcs.hxx",
    out_src_file="ref_gen.cxx",
    out_hdr_file="ref_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req update<status=1>(do_update+notify,
				   data+read+req);
    cxxmethod+req resume<status=1>(do_resume+notify,
				   data+read+req,
				   complete+write);
    cxxmethod+req refine<status=1>(do_refine+notify,
				   data+read+req,
				   complete+write);
    cxxmethod+req list<status=1>(do_list+notify+read+req,
				 visible+read+req,
				 nshells+notify+read+req);
    cxxmethod+req import<status=1>(do_import+notify,
				   visible+read+req,
				   cluster+read+req,
				   data+read+req);
    int do_update<NEportLevels={2,0}>;
    int do_resume<NEportLevels={2,0}>;
    int do_refine<NEportLevels={2,0}>;
    int do_list<NEportLevels={2,0}>;
    int nshells<NEportLevels={2,0}>;
    int do_import<NEportLevels={2,0}>;
    int visible<NEportLevels={2,0}>;
    int cluster<NEportLevels={2,0}>;
    refinement_data &data<NEportLevels={2,0}>;
    int complete<NEportLevels={0,2}>;
  };
  module set_params<build_dir="avs/src/excurv",
    cxx_hdr_files="ref_gen.hxx",
    out_src_file="list_gen.cxx",
    out_hdr_file="list_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req update<status=1>(params+read+notify+req,
				   data+write);
    string params<NEportLevels={2,0}>;
    refinement_data &data<NEportLevels={2,0}>;
  };
  module set_kweight<build_dir="avs/src/excurv",
    //cxx_hdr_files="../../src/excurv/calcs.hxx",
    out_src_file="kw_gen.cxx",
    out_hdr_file="kw_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req update<status=1>(kweight+read+req+notify);
    int kweight<NEportLevels={2,0}>;
  };
  module set_minmax<build_dir="avs/src/excurv",
    //cxx_hdr_files="../../src/excurv/calcs.hxx",
    out_src_file="ek_gen.cxx",
    out_hdr_file="ek_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req update<status=1>(emin+read+req+notify,
				   emax+read+req+notify,
				   kmin+read+req+notify,
				   kmax+read+req+notify);
    float emin<NEportLevels={2,0}>;
    float emax<NEportLevels={2,0}>;
    float kmin<NEportLevels={2,0}>;
    float kmax<NEportLevels={2,0}>;
  };
  module set_multiple_scat<build_dir="avs/src/excurv",
    //cxx_hdr_files="../../src/excurv/calcs.hxx",
    out_src_file="ms_gen.cxx",
    out_hdr_file="ms_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req set<status=1>(ms+read+req+notify,
				symmetry+read+req,
				model_type+read+req);
    int ms<NEportLevels={2,0}>;
    string symmetry<NEportLevels={2,0}>;
    int model_type<NEportLevels={2,0}>;
  };
  module excurv_errors<build_dir="avs/src/excurv",
    //cxx_hdr_files="../../src/excurv/calcs.hxx",
    out_src_file="err_gen.cxx",
    out_hdr_file="err_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req update<status=1>(params+read+notify+req,
				   do_potential+read+req,
				   do_expt+read+req,
				   potential_ok+write,
				   expt_ok+write);
    string params<NEportLevels={2,0}>;
    int do_potential<NEportLevels={2,0}>;
    int do_expt<NEportLevels={2,0}>;
    int potential_ok<NEportLevels={0,2}>;
    int expt_ok<NEportLevels={0,2}>;
  };
  module excurv_cluster<build_dir="avs/src/excurv",
    cxx_hdr_files="ref_gen.hxx pot_gen.hxx",
    out_src_file="info_gen.cxx",
    out_hdr_file="info_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req gen_info<status=1>(trigger+read+req+notify,
				     visible+read+req,
				     //in+read,
				     atom_type+write,
				     pot_data+write,
				     ref_data+write);
    int trigger<NEportLevels={2,0}>;
    int visible<NEportLevels={2,0}>;
    int atom_type<NEportLevels={2,0}>;
    pot_data_type &pot_data<NEportLevels={2,0}>;
    refinement_data &ref_data<NEportLevels={2,0}>;
  };
  module excurv_model<build_dir="avs/src/excurv",
    cxx_hdr_files="ref_gen.hxx",
    out_src_file="str_gen.cxx",
    out_hdr_file="str_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req update<status=1>(convert+read+req+notify,
				   visible+read+req,
				   potentials+read+req,
				   atom+read+req,
				   atom_types+read+req,
				   data+read+req+notify);
    int convert<NEportLevels={2,0}>;
    int visible<NEportLevels={2,0}>;
    int potentials<NEportLevels={2,0}>;
    int atom<NEportLevels={2,0}>;
    int atom_types<NEportLevels={2,0}>[];
    refinement_data &data<NEportLevels={2,0}>;
  };
  // End of import.
  module kkr <build_dir="avs/src/excurv",
    out_src_file="scf_gen.cxx",
    out_hdr_file="scf_gen.hxx",
    src_file="express.cxx">{
    cxxmethod read(filename+read+req,
		   run+read+notify,
		   atoms+write);
    string filename<NEportLevels={2,0}>;
    int run<NEportLevels={2,0}>;
    string atoms<NEportLevels={0,2}>[];
  };
};
