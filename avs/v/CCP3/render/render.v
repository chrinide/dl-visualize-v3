
flibrary Renderers {
  //"../avs/v/CCP3/modules/viewer/modules.v" Modules;
  "../avs/v/CCP3/render/base.v" Base;
  "../avs/v/CCP3/render/model.v" Model;
  "../avs/v/CCP3/render/points.v" Points;
  "../avs/v/CCP3/render/lines.v" Lines;
  "../avs/v/CCP3/render/plots.v" Plots;
  "../avs/v/CCP3/render/planes.v" Planes;
  "../avs/v/CCP3/render/volumes.v" Volumes;
  "../avs/v/CCP3/render/edit.v" Edits;
};
