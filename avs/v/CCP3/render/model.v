
flibrary Model<NEeditable=1,
	       export_cxx=1,
	       build_dir="avs/src/express",
	       cxx_hdr_files="fld/Xfld.h avs/src/express/calcs.hxx avs/src/express/rbase.hxx avs/src/core/render/cell_gen.hxx avs/src/express/display.hxx avs/src/core/render/latt_gen.hxx avs/src/core/render/atom_gen.hxx avs/src/core/render/bond_gen.hxx avs/src/core/render/sel_gen.hxx avs/src/core/render/outl_gen.hxx",
	       out_hdr_file="model.hxx",
	       out_src_file="model.cxx"> {
  group structure_rspace {
    CCP3.Core_Modules.Render.atom_options atoms;
    CCP3.Core_Modules.Render.lattice_options lattice;
    CCP3.Core_Modules.Render.bond_options bonds;
  };
  group structure_kspace {
    CCP3.Core_Modules.Render.lattice_options lattice;
  };
  group model_render_data {
    CCP3.Core_Modules.Render.common_options common;
    structure_rspace rspace;
    structure_kspace kspace;
    CCP3.Core_Modules.Render.props props;
    CCP3.Renderers.Base.background background;
    int bond_all;
  };
  macro draw_atoms {
    link nframes<NEportLevels=1,NEx=33.,NEy=99.>;
    link ncoords<NEportLevels=1,NEx=33.,NEy=176.>;
    link index<NEportLevels=1,NEx=880.,NEy=99.>;
    float sequence<NEportLevels=1,NEx=209.,NEy=99.>[nframes][ncoords][3];
    float coords<NEportLevels=1,NEx=220.,NEy=165.>[ncoords][3] =>
      sequence[index];
    float radii<NEportLevels=1,NEx=451.,NEy=99.>[ncoords];
    float colours<NEportLevels=1,NEx=682.,NEy=99.>[ncoords][3];
    group point_mesh<NEx=297.,NEy=264.> {
      float &coord<NEportLevels={2,0}>[][] => <-.coords;
      int coord_dims[] => array_dims(coord);
      int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "point_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!error),<-.coord_dims[1]);
	nspace => switch((!error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Point cell_set {
	  ncells => <-.nnodes;
	  node_connect_list =>
	    switch((!error),init_array(ncells,0,(ncells - 1)));
	};
      };
    };
    FLD_MAP.radius_data radius_data<NEx=473.,NEy=209.> {
      in_data => <-.radii;
    };
    FLD_MAP.node_colors node_colors<NEx=682.,NEy=209.> {
      in_data => <-.colours;
    };
    FLD_MAP.combine_node_datas combine_node_datas<NEx=605.,NEy=275.> {
      in => {
	<-.radius_data.out,
	<-.node_colors.out
      };
    };
    FLD_MAP.combine_mesh_data combine_mesh_data<NEx=418.,NEy=374.> {
      in_mesh => <-.point_mesh.out;
      in_nd => <-.combine_node_datas.out;
    };
    link obj<NEportLevels={1,2},NEx=418.,NEy=506.> => .combine_mesh_data.obj;
  };
  draw_atoms pick_atoms {
    link event<NEportLevels=1,NEx=891.,NEy=198.>;
    GDpick_process GDpick_process<NEx=660.,NEy=451.> {
      obj_in => <-.combine_mesh_data.obj;
    };
    CCP3.Core_Modules.Render.select_atom select_atom<NEx=660.,NEy=539.> {
      coords => <-.sequence[0][<-.combine_mesh_data.DataObject.PickInfo.pick_data[0].verti];
      event => <-.event;
      npicked => <-.combine_mesh_data.DataObject.PickInfo.npicked;
      nselects = 0;
    };
  };
  macro obj_base {
    int nframes<NEportLevels=1,NEx=187.,NEy=66.> = 1;
    int index<NEportLevels=1,NEx=473.,NEy=66.> = 0;
    GMOD.instancer instance_opaque<NEx=176.,NEy=275.>;
    GMOD.instancer instance_transparent<NEx=473.,NEy=275.>;
    GMOD.instancer instance_edit<NEx=726.,NEy=275.>;
    GroupObject GroupObject<NEx=319.,NEy=517.> {
      Top {
	xform_mode = "Parent";
      };
      obj<NEportLevels={1,3}>;
    };
  };
  obj_base atom_base {
    long nopaque<NEportLevels=1,NEx=187.,NEy=176.> = 0;
    long ntransparent<NEportLevels=1,NEx=473.,NEy=176.> = 0;
    long nedit<NEportLevels=1,NEx=726.,NEy=176.> = 0;
    instance_opaque {
      Value => (<-.nopaque > 0);
    };
    instance_transparent {
      Value => (<-.ntransparent > 0);
    };
    instance_edit {
      Value => (<-.nedit > 0);
    };
  };
  atom_base atoms {
    instance_opaque {
      Group => <-.opaque;
    };
    instance_transparent {
      Group => <-.transparent;
    };
    instance_edit {
      Group => <-.edit;
    };
    CCP3.Renderers.Model.pick_atoms opaque<NEx=176.,NEy=352.,instanced=0> {
      nframes => <-.nframes;
      index => <-.index;
      ncoords => <-.nopaque;
      combine_mesh_data {
	DataObject {
	  Obj {
	    name => "opaque";
	  };
	};
      };
    };
    // Was draw_atoms, but probably want pick
    CCP3.Renderers.Model.pick_atoms transparent<NEx=473.,NEy=352.,
      instanced=0> {
      nframes => <-.nframes;
      index => <-.index;
      ncoords => <-.ntransparent;
      combine_mesh_data {
	DataObject {
	  Obj {
	    name => "transparent";
	  };
	};
      };
    };
    CCP3.Renderers.Model.pick_atoms edit<NEx=737.,NEy=352.,instanced=0> {
      nframes => <-.nframes;
      index => <-.index;
      ncoords => <-.nedit;
      combine_mesh_data {
	DataObject {
	  Obj {
	    name => "edit";
	  };
	};
      };
    };
    GroupObject {
      Top {
	name => "Atoms";
      };
    };
  };
  macro draw_lattice {
    float initial_coords<NEportLevels=1,NEx=363.,NEy=77.>[nlines][3];
    float final_coords<NEportLevels=1,NEx=594.,NEy=77.>[nlines][3];
    int nlines<NEportLevels=1,NEx=44.,NEy=77.>;
    FLD_MAP.line_disjoint_mesh line_disjoint_mesh<NEx=396.,NEy=220.> {
      coord1 => <-.initial_coords;
      coord2 => <-.final_coords;
      DataObject {
	Obj {
	  xform_mode = "Parent";
	  name = "Lines";
	};
      };
    };
    GroupObject GroupObject<NEx=396.,NEy=374.> {
      child_objs => {
         <-.line_disjoint_mesh.obj
      };
      Top {
	xform_mode = "Parent";
	name => "Lattice";
      };
    };
    link obj<NEportLevels={1,2},NEx=396.,NEy=524.> => GroupObject.obj;
  };
  macro lattice_labels {
    int nlabels<NEx=187.,NEy=99.>;
    float coords<NEx=407.,NEy=99.>[nlabels][3];
    string text<NEx=607.,NEy=99.>[nlabels];
    group TextValues<NEx=539.,NEy=275.,export_all=2> {
      int align_horiz = 1;
      int align_vert = 2;
      int drop_shadow = 0;
      int background = 0;
      int bounds = 0;
      int underline = 0;
      int lead_line = 0;
      int radial = 0;
      int do_offset = 1;
      float offset[3] = {0.,0.,0.3};
      int xform_mode;
      int color;
      string text_values[] => <-.text;
      int stroke = 0;
      group StrokeTextAttribs {
	int font_type;
	int style;
	int plane;
	int orient;
	int path;
	int space_mode;
	float spacing;
	float angle;
	float height;
	float expansion;
	float width;
      };
    };
    group point_mesh<NEx=308.,NEy=275.> {
      float &coord<NEportLevels={2,0}>[][] => <-.coords;
      int coord_dims[] => array_dims(coord);
      int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "point_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!error),<-.coord_dims[1]);
	nspace => switch((!error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Point cell_set {
	  ncells => <-.nnodes;
	  node_connect_list =>
	    switch((!error),init_array(ncells,0,(ncells - 1)));
	};
      };
    };
    Grid+Xform &TextField<NEx=473.,NEy=363.> =>
      merge(TextValues, point_mesh.out);
    DataObjectNoTexture labels<NEx=429.,NEy=541.> {
      in => <-.TextField;
      Obj {
	xform_mode = "Parent";
	name = "Labels";
      };
      obj<NEportLevels={1,3}>;
    };
  };
  macro brillouin_zone {
    int nvertices<NEportLevels=1,NEx=176.,NEy=88.>;
    float vertices<NEportLevels=1,NEx=385.,NEy=88.>[nvertices][3];
    long connects<NEportLevels=1,NEx=616.,NEy=88.>[];
    FLD_MAP.line_mesh line_mesh<NEx=451.,NEy=242.> {
      coord => <-.vertices;
      connect => <-.connects;
      DataObject {
	Obj {
	  xform_mode = "Parent";
	  name = "Lattice";
	};
      };
    };
    link obj<NEportLevels={1,2},NEx=440.,NEy=451.> => .line_mesh.obj;
  };
  macro draw_line_bonds {
    link nframes<NEportLevels=1,NEx=33.,NEy=99.>;
    link index<NEportLevels=1,NEx=880.,NEy=99.>;
    group sequence<NEportLevels=1,NEx=209.,NEy=99.>[nframes] {
      long ndata;
      float coords[ndata][3];
      long connects[];
      float colours[ndata][3];
    };
    float coords<NEportLevels=1,NEx=220.,NEy=165.>[][3] =>
      sequence[index].coords;
    long connections<NEportLevels=1,NEx=528.,NEy=55.>[] =>
      sequence[index].connects;
    float colours<NEportLevels=1,NEx=759.,NEy=55.>[][3] =>
      sequence[index].colours;
    group polyline_mesh<NEx=330.,NEy=176.> {
      float &coord<NEportLevels={2,0}>[][] => <-.coords;
      long &connect<NEportLevels={2,0}>[] => <-.connections;
      int coord_dims[] => array_dims(coord);
      int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "polyline_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!error),<-.coord_dims[1]);
	nspace => switch((!error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Polyline cell_set {
	  npolys => switch((!error),(array_size(<-.<-.connect) / 2));
	  poly_connect_list => switch((!error),<-.<-.connect);
	};
      };
    };
    FLD_MAP.node_colors node_colors<NEx=671.,NEy=176.> {
      in_data => <-.colours;
    };
    FLD_MAP.combine_mesh_data combine_mesh_data<NEx=473.,NEy=297.> {
      in_mesh => <-.polyline_mesh.out;
      in_nd => <-.node_colors.out;
      DataObject {
	Obj {
	  xform_mode = "Parent";
	};
      };
    };
    link obj<NEportLevels={1,2},NEx=484.,NEy=451.> => .combine_mesh_data.obj;
  };
  obj_base bond_base {
    group nopaque<NEportLevels=1,NEx=187.,NEy=176.>[nframes] {
      long n = 0;
    };
    group ntransparent<NEportLevels=1,NEx=473.,NEy=176.>[nframes] {
      long n = 0;
    };
    group nedit<NEportLevels=1,NEx=726.,NEy=176.>[nframes] {
      long n = 0;
    };
    instance_opaque {
      Value => (<-.nopaque[index].n > 0);
    };
    instance_transparent {
      Value => (<-.ntransparent[index].n > 0);
    };
    instance_edit {
      Value => (<-.nedit[index].n > 0);
    };
  };
  bond_base line_bonds {
    instance_opaque {
      Group => <-.opaque;
    };
    instance_transparent {
      Group => <-.transparent;
    };
    instance_edit {
      Group => <-.edit;
    };
    CCP3.Renderers.Model.draw_line_bonds opaque<NEx=176.,NEy=352.,
      instanced=0> {
      nframes => <-.nframes;
      index => <-.index;
      combine_mesh_data {
	DataObject {
	  Obj {
	    name => "opaque";
	  };
	};
      };
    };
    CCP3.Renderers.Model.draw_line_bonds transparent<NEx=473.,NEy=352.,
      instanced=0> {
      nframes => <-.nframes;
      index => <-.index;
      combine_mesh_data {
	DataObject {
	  Obj {
	    name => "transparent";
	  };
	};
      };
    };
    CCP3.Renderers.Model.draw_line_bonds edit<NEx=737.,NEy=352.,
      instanced=0> {
      nframes => <-.nframes;
      index => <-.index;
      combine_mesh_data {
	DataObject {
	  Obj {
	    name => "edit";
	  };
	};
      };
    };
    GroupObject {
      Top {
	name => "Bonds - Lines";
      };
    };
  };
  macro draw_tube_bonds {
    link nframes<NEportLevels=1,NEx=33.,NEy=99.>;
    link index<NEportLevels=1,NEx=880.,NEy=99.>;
    group sequence<NEportLevels=1,NEx=209.,NEy=99.>[nframes] {
      long ndata;
      float coords[ndata][3];
      long connects[];
      float colours[ndata][3];
    };
    float coords<NEportLevels=1,NEx=220.,NEy=165.>[][3] =>
      sequence[index].coords;
    long connections<NEportLevels=1,NEx=528.,NEy=55.>[] =>
      sequence[index].connects;
    float colours<NEportLevels=1,NEx=759.,NEy=55.>[][3] =>
      sequence[index].colours;
    group polytri_mesh<NEx=385.,NEy=209.> {
      float &coord<NEportLevels={2,0}>[][] => <-.coords;
      long &connect<NEportLevels={2,0}>[] => <-.connections;
      int coord_dims[] => array_dims(coord);
      int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "polyltry_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!error),<-.coord_dims[1]);
	nspace => switch((!error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Polytri cell_set {
	  npolys => switch((!error),(array_size(<-.<-.connect) / 2));
	  poly_connect_list => switch((!error),<-.<-.connect);
	};
      };
    };
    FLD_MAP.node_colors node_colors<NEx=616.,NEy=209.> {
      in_data => <-.colours;
    };
    FLD_MAP.combine_mesh_data combine_mesh_data<NEx=462.,NEy=319.> {
      in_mesh => <-.polytri_mesh.out;
      in_nd => <-.node_colors.out;
      DataObject {
	Obj {
	  xform_mode = "Parent";
	};
      };
    };
    link obj<NEportLevels={1,2},NEx=407.,NEy=440.> => .combine_mesh_data.obj;
  };
  bond_base tube_bonds {
    instance_opaque {
      Group => <-.opaque;
    };
    instance_transparent {
      Group => <-.transparent;
    };
    instance_edit {
      Group => <-.edit;
    };
    CCP3.Renderers.Model.draw_tube_bonds opaque<NEx=176.,NEy=352.,
      instanced=0> {
      nframes => <-.nframes;
      index => <-.index;
      combine_mesh_data {
	DataObject {
	  Obj {
	    name => "opaque";
	  };
	};
      };
    };
    CCP3.Renderers.Model.draw_tube_bonds transparent<NEx=473.,NEy=352.,
      instanced=0> {
      nframes => <-.nframes;
      index => <-.index;
      combine_mesh_data {
	DataObject {
	  Obj {
	    name => "transparent";
	  };
	};
      };
    };
    CCP3.Renderers.Model.draw_tube_bonds edit<NEx=737.,NEy=352.,
      instanced=0> {
      nframes => <-.nframes;
      index => <-.index;
      combine_mesh_data {
	DataObject {
	  Obj {
	    name => "edit";
	  };
	};
      };
    };
    GroupObject {
      Top {
	name => "Bonds - Tubes";
      };
    };
  };
  macro draw_polyhedra {
    link nframes<NEportLevels=1,NEx=33.,NEy=99.>;
    link index<NEportLevels=1,NEx=880.,NEy=99.>;
    group sequence<NEportLevels=1,NEx=209.,NEy=99.>[nframes] {
      long ndata;
      float coords[ndata][3];
      long connects[];
      float colours[ndata][3];
    };
    float coords<NEportLevels=1,NEx=220.,NEy=165.>[][3] =>
      sequence[index].coords;
    long connections<NEportLevels=1,NEx=528.,NEy=55.>[] =>
      sequence[index].connects;
    float colours<NEportLevels=1,NEx=759.,NEy=55.>[][3] =>
      sequence[index].colours;
    group tri_mesh<NEx=385.,NEy=209.> {
      float &coord<NEportLevels={2,0}>[][] => <-.coords;
      long &connect<NEportLevels={2,0}>[] => <-.connections;
      int coord_dims[] => array_dims(coord);
      int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "tri_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!error),<-.coord_dims[1]);
	nspace => switch((!error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Tri cell_set {
	  ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
	  node_connect_list => switch((!error),<-.<-.connect);
	};
      };
    };
    FLD_MAP.node_colors node_colors<NEx=616.,NEy=209.> {
      in_data => <-.colours;
    };
    FLD_MAP.combine_mesh_data combine_mesh_data<NEx=462.,NEy=319.> {
      in_mesh => <-.tri_mesh.out;
      in_nd => <-.node_colors.out;
      DataObject {
	Obj {
	  xform_mode = "Parent";
	};
      };
    };
    link obj<NEportLevels={1,2},NEx=407.,NEy=440.> => .combine_mesh_data.obj;
  };
  macro polyhedra {
    int nframes<NEportLevels=1,NEx=187.,NEy=66.> = 1;
    int index<NEportLevels=1,NEx=473.,NEy=66.> = 0;
    CCP3.Renderers.Model.draw_polyhedra opaque<NEx=176.,NEy=352.> {
      nframes => <-.nframes;
      index => <-.index;
      combine_mesh_data {
	DataObject {
	  Obj {
	    name => "opaque";
	  };
	};
      };
    };
    GroupObject GroupObject<NEx=319.,NEy=517.> {
      child_objs => {<-.opaque.obj};
      Modes {
	mode = {
	  0, 2, 0, 0, 0
	};
      };
      Props {
	hi1_col = {
	  0.5, 0.5, 0.5
	};
      };
      Top {
	name => "Polyhedra";
	xform_mode = "Parent";
      };
      obj<NEportLevels={1,3}>;
    };
  };
  CCP3.Renderers.Base.props Model_Base {
    GroupObject {
      Top {
	name => "Structure";
	xform_mode = "Parent";
      };
    };
  };
  Model_Base Atom_Base {
    GMOD.instancer instance_lattice<NEx=88.,NEy=264.>;
  };
  Atom_Base Model_R {
    link event<NEportLevels=1,NEx=528.,NEy=143.>;
    CCP3.Core_Modules.Render.props &props<NEx=77.,NEy=143.>;
    CCP3.Renderers.Model.structure_rspace options<NEx=286.,NEy=143.> {
      atoms {
	draw = 1;
	scale = 0.75;
	selection_method = 0;
	label_selects = 0;
	object_groups = 0;
	//select_exact_atom = 0;
	//select_translation_copies = 0;
	//select_symmetry_copies = 1;
	symmetry_copies = 0;
      };
      lattice {
	draw = 0;
	label = 0;
      };
      bonds {
	draw = 0;
	polyhedra = 0;
	tubes = 1;
	overlap = 1.0;
	tube_radius = 0.1;
	tube_subdiv = 8;
	outline_poly = 0;
      };
    };
    int nframes<NEportLevels=1,NEx=286.,NEy=198.> = 1;
    int end_index<NEportLevels=1,NEx=495.,NEy=198.> => (nframes - 1);
    int cur_frame<NEportLevels=1,NEx=682.,NEy=198.> = 0;
    CCP3.Renderers.Model.draw_lattice draw_lattice<NEx=88.,NEy=330.,
      instanced=0> {
      line_disjoint_mesh {
	DataObject {
	  Props {
	    line_width => <-.<-.<-.<-.props.lines.width;
	    line_aa => <-.<-.<-.<-.props.lines.smooth;
	  };
	};
      };
      GroupObject {
      	Props {
	  line_width => <-.<-.<-.props.lines.width;
	  line_aa => <-.<-.<-.props.lines.smooth;
	};
      };
      //GMOD.instancer instance_labels<NEx=682.,NEy=231.> {
      //Value => <-.<-.options.lattice.label;
      //Group => <-.lattice_labels;
      //};
      CCP3.Renderers.Model.lattice_labels lattice_labels<NEx=682.,NEy=319.>;
    };
    instance_lattice {
      Value => <-.options.lattice.draw;
      Group => <-.draw_lattice;
    };
    CCP3.Renderers.Model.atoms atoms<NEx=286.,NEy=330.,instanced=0> {
      nframes => <-.nframes;
      index => min_array({<-.end_index, <-.cur_frame});
      opaque {
	event => <-.<-.event;
	combine_mesh_data {
	  DataObject {
	    Props {
	      subdiv => <-.<-.<-.<-.<-.props.spheres.subdiv;
	    };
	    Obj {
	      xform_mode = "Parent";
	    };
	  };
	};
	select_atom {
	  id => (<-.<-.<-.options.atoms.selection_method != 2);
	};
      };
      transparent {
	event => <-.<-.event;
	combine_mesh_data {
	  DataObject {
	    Props {
	      subdiv => <-.<-.<-.<-.<-.props.spheres.subdiv;
	      trans => <-.<-.<-.<-.<-.props.spheres.opacity;
	      inherit = 0;
	    };
	    Obj {
	      xform_mode = "Parent";
	    };
	  };
	};
	select_atom {
	  id => (<-.<-.<-.options.atoms.selection_method == 2);
	};
      };
      edit {
	event => <-.<-.event;
	combine_mesh_data {
	  DataObject {
	    Props {
	      subdiv => <-.<-.<-.<-.<-.props.spheres.subdiv;
	      inherit = 0;
	    };
	  };
	};
      };
      GroupObject {
      	Props {
	  subdiv => <-.<-.<-.props.spheres.subdiv;
	};
      };
    };
    GMOD.instancer instance_atoms<NEx=286.,NEy=264.> {
      Value => <-.options.atoms.draw;
      Group => <-.atoms;
    };
    CCP3.Renderers.Model.tube_bonds tube_bonds<NEx=682.,NEy=330.,
      instanced=0> {
      nframes => <-.nframes;
      index => min_array({<-.end_index, <-.cur_frame});
      opaque {
	combine_mesh_data {
	  DataObject {
	    Obj {
	      xform_mode = "Parent";
	    };
	  };
	};
      };
      transparent {
	combine_mesh_data {
	  DataObject {
	    Props {
	      trans => <-.<-.<-.<-.<-.props.spheres.opacity;
	      inherit = 0;
	    };
	    Obj {
	      xform_mode = "Parent";
	    };
	  };
	};
      };
    };
    CCP3.Renderers.Model.line_bonds line_bonds<NEx=495.,NEy=330.,
      instanced=0> {
      nframes => <-.nframes;
      index => min_array({<-.end_index, <-.cur_frame});
      opaque {
	combine_mesh_data {
	  DataObject {
	    Obj {
	      xform_mode = "Parent";
	    };
	  };
	};
      };
      transparent {
	combine_mesh_data {
	  DataObject {
	    Props {
	      trans => <-.<-.<-.<-.<-.props.spheres.opacity;
	      inherit = 0;
	    };
	    Obj {
	      xform_mode = "Parent";
	    };
	  };
	};
      };
      GroupObject {
      	Props {
	  line_width => <-.<-.<-.props.lines.width;
	  line_aa => <-.<-.<-.props.lines.smooth;
	};
      };
    };
    GMOD.instancer instance_lines<NEx=495.,NEy=264.> {
      Value => ((<-.options.bonds.draw == 1) && (<-.options.bonds.tubes == 0));
      Group => <-.line_bonds;
    };
    GMOD.instancer instance_tubes<NEx=682.,NEy=264.> {
      Value => ((<-.options.bonds.draw == 1) && (<-.options.bonds.tubes == 1));
      Group => <-.tube_bonds;
    };
    GMOD.instancer instance_poly<NEx=864.,NEy=261.> {
      Value => (<-.options.bonds.polyhedra == 1);
      Group => <-.polyhedra;
    };
    CCP3.Renderers.Model.polyhedra polyhedra<NEx=864.,NEy=324.,
      instanced=0> {
      nframes => <-.nframes;
      index => min_array({<-.end_index, <-.cur_frame});
      GroupObject {
	Modes {
	  outline => !(<-.<-.<-.options.bonds.outline_poly);
	};
      };
    };
    GroupObject EditAtoms<NEx=522.,NEy=549.> {
      Props {
	line_width => <-.<-.props.lines.width;
	line_aa => <-.<-.props.lines.smooth;
      };
      Top {
	name => "Edits";
	xform_mode = "Parent";
      };
      obj<NEportLevels={1,3}>;
    };
    GroupObject {
      Props {
	line_width => <-.<-.props.lines.width;
	line_aa => <-.<-.props.lines.smooth;
      };
    };
  };
  Atom_Base Model_K {
    CCP3.Core_Modules.Render.line_props &props<NEx=88.,NEy=143.>;
    CCP3.Renderers.Model.structure_kspace options<NEx=286.,NEy=143.> {
      lattice {
	draw = 1;
	label = 0;
      };
    };
    CCP3.Renderers.Model.brillouin_zone draw_lattice<NEx=88.,NEy=330.> {
      line_mesh {
	DataObject {
	  Props {
	    line_width => <-.<-.<-.<-.props.width;
	    line_aa => <-.<-.<-.<-.props.smooth;
	  };
	};
      };
    };
    instance_lattice {
      Value => <-.options.lattice.draw;
      Group => <-.draw_lattice;
    };
    GroupObject {
      Props {
	line_width => <-.<-.props.width;
	line_aa => <-.<-.props.smooth;
      };
    };
  };
  Model_Base Outline {
    CCP3.Core_Modules.Render.outline_data data<NEx=873.,NEy=36.> {
      draw_lines = 1;
      draw_surface = 0;
      opacity = 1.0;
      red = 0.0;
      green = 0.0;
      blue = 0.0;
    };
    int n_vertices<NEx=9.,NEy=45.>;
    int n_cylinders<NEx=171.,NEy=45.>;
    int n_spheres<NEportLevels=1,NEx=9.,NEy=99.>;
    float vertices<NEportLevels=1,NEx=342.,NEy=45.>[n_vertices][3];
    int nodes<NEportLevels=1,NEx=513.,NEy=45.>[];
    long connects<NEportLevels=1,NEx=693.,NEy=45.>[];
    long lines<NEportLevels=1,NEx=603.,NEy=144.>[];
    FLD_MAP.polyhedron_mesh polyhedron_mesh<NEx=252.,NEy=135.> {
      coord => <-.vertices;
      poly_nodes => <-.nodes;
      connect => <-.connects;
      DataObject {
	Props {
	  col => {<-.<-.<-.data.red, <-.<-.<-.data.green,<-.<-.<-.data.blue};
	  trans => <-.<-.<-.data.opacity;
	};
	Obj {
	  visible => <-.<-.<-.data.draw_surface;
	  xform_mode = "Parent";
	};
      };
    };
    // Todo - remove?
    /*
    GroupObject surfaces<NEx=207.,NEy=387.> {
      child_objs => { <-.polyhedron_mesh.obj};
      Props {
	col => {<-.<-.data.red, <-.<-.data.green,<-.<-.data.blue};
	trans => <-.<-.data.opacity;
      };
      Top {
	visible => <-.<-.data.draw_surface;
	xform_mode = "Parent";
      };
    };
    */
    FLD_MAP.line_mesh line_mesh<NEx=567.,NEy=369.> {
      coord => <-.vertices;
      connect => <-.lines;
      DataObject {
	Props {
	  col => {<-.<-.<-.data.red, <-.<-.<-.data.green,<-.<-.<-.data.blue};
	  trans => <-.<-.<-.data.opacity;
	};
	Obj {
	  visible => <-.<-.<-.data.draw_lines;
	  xform_mode = "Parent";
	};
      };
    };
    // Todo - remove?
    /*
    GroupObject edges<NEx=660.,NEy=517.> {
      child_objs => { <-.line_mesh.obj };
      Props {
	col => {<-.<-.data.red, <-.<-.data.green,<-.<-.data.blue};
	trans => <-.<-.data.opacity;
      };
      Top {
	visible => <-.<-.data.draw_lines;
	xform_mode = "Parent";
      };
    };
    */
    float rmin<NEportLevels=1,NEx=468.,NEy=198.>[n_cylinders];
    float rmax<NEportLevels=1,NEx=630.,NEy=198.>[n_cylinders];
    float zmin<NEportLevels=1,NEx=801.,NEy=198.>[n_cylinders];
    float zmax<NEportLevels=1,NEx=963.,NEy=198.>[n_cylinders];
    int orientation<NEportLevels=1,NEx=792.,NEy=144.>[];
    GMOD.instancer instancer<NEx=378.,NEy=270.> {
      Value => (<-.n_cylinders > 0);
      Group => <-.cylinder;
    };
    macro cylinder<NEx=819.,NEy=261.,instanced=0> {
      link n_cylinders => <-.n_cylinders;
      float matx[4][4] = {{0.0,0.0,1.0,0.0},
			  {0.0,1.0,0.0,0.0},
			  {1.0,0.0,0.0,0.0},
			  {0.0,0.0,0.0,1.0}};
      float maty[4][4] = {{1.0,0.0,0.0,0.0},
			  {0.0,0.0,1.0,0.0},
			  {0.0,1.0,0.0,0.0},
			  {0.0,0.0,0.0,1.0}};
      DefaultXform Xform_x {
	mat => <-.matx;
      };
      DefaultXform Xform_y {
	mat => <-.maty;
      };
      DefaultXform Xform_z;
      DefaultXform Xforms[3] => { Xform_x, Xform_y, Xform_z };
      macro cylinder<NEx=819.,NEy=261.>[n_cylinders] {
	float rmin<NEportLevels={1,1},NEx=117.,NEy=45.> =>
					<-.<-.rmin[index_of(<-.cylinder)];
	float rmax<NEportLevels={1,1},NEx=351.,NEy=36.> =>
					<-.<-.rmax[index_of(<-.cylinder)];
	float zmin<NEportLevels={1,1},NEx=585.,NEy=45.> =>
					<-.<-.zmin[index_of(<-.cylinder)];
	float zmax<NEportLevels={1,1},NEx=855.,NEy=54.> =>
					<-.<-.zmax[index_of(<-.cylinder)];
	int orientation<NEportLevels=1,NEx=594.,NEy=126.> =>
	  <-.<-.orientation[index_of(<-.cylinder)];
	Mesh_Cyl_Unif Mesh_Cyl_Unif<NEx=252.,NEy=198.,NEportLevels={0,1}> {
	  dims = {2,32,2};
	  ndim = 3;
	  nspace = 3;
	  points => {{<-.rmin,0.0,<-.zmin},{<-.rmax,6.283185,<-.zmax}};
	};
	DVbounds DVbounds<NEx=387.,NEy=297.> {
	  in => <-.Mesh_Cyl_Unif;
	  hull = 0;
	  edges => <-.<-.<-.data.draw_lines;
	  faces => <-.<-.<-.data.draw_surface;
	  imin = 1;
	  imax = 1;
	  jmin = 0;
	  jmax = 0;
	  kmin = 1;
	  kmax = 1;
	  data = 0;
	};
	// Todo - transform for orientation of cylinder!
	DataObjectNoTexture DataObjectNoTexture<NEx=387.,NEy=432.> {
	  in => <-.DVbounds.out;
	  Obj {
	    xform => <-.<-.<-.Xforms[<-.<-.orientation];
	  };
	  obj<NEportLevels={1,3}>;
	};
      };
      mlink mlink<NEportLevels=1,NEx=864.,NEy=315.> => 
	cylinder.DataObjectNoTexture.obj;
      GroupObject cylinder_gp<NEx=819.,NEy=333.> {
	child_objs => mlink;
	Props {
	  col => {<-.<-.<-.data.red, <-.<-.<-.data.green,<-.<-.<-.data.blue};
	  trans => <-.<-.<-.data.opacity;
	};
	Top {
	  visible => (<-.<-.n_cylinders > 0);
	  xform_mode = "Parent";
	};
      };
    };
    float rsph<NEportLevels=1,NEx=99.,NEy=180.>[n_spheres];
    macro spheres<NEx=99.,NEy=243.> {
      link n_spheres<NEportLevels=1,NEx=144.,NEy=63.> => <-.n_spheres;
      CCP3.Renderers.Volumes.sphere sphere<NEx=378.,NEy=171.>[n_spheres] {
	radii => <-.<-.rsph[index_of(<-.sphere)];
	colours => {<-.<-.data.red, <-.<-.data.green, <-.<-.data.blue};
	coords => {0.0, 0.0, 0.0};
	draw {
	  Props {
	    col => {<-.<-.<-.<-.data.red, <-.<-.<-.<-.data.green,
		    <-.<-.<-.<-.data.blue};
	    trans => <-.<-.<-.<-.data.opacity;
	  };
	};
      };
      mlink mlink<NEportLevels=1,NEx=396.,NEy=243.> => .sphere.obj;
      GroupObject sphere_gp<NEx=369.,NEy=306.> {
	child_objs => <-.mlink;
	Props {
	  col => {<-.<-.<-.data.red, <-.<-.<-.data.green,<-.<-.<-.data.blue};
	  trans => <-.<-.<-.data.opacity;
	};
	Top {
	  visible => (<-.<-.n_spheres > 0);
	  xform_mode = "Parent";
	};
      };
    };
    link n<NEx=639.,NEy=459.>;
    Xform &Xform<NEx=792.,NEy=459.>[];
    GroupObject qdot<NEx=306.,NEy=428.>[n] {
      //child_objs => {<-.surfaces.obj, edges.obj, cylinder_gp.obj};
      child_objs => switch ((n_spheres> 0)+1,
			    switch ((n_cylinders > 0)+1,
			      {<-.polyhedron_mesh.obj, line_mesh.obj},
			      {<-.polyhedron_mesh.obj, line_mesh.obj,
			       <-.cylinder.cylinder_gp.obj}),
			    switch ((n_cylinders > 0)+1,
			      {<-.polyhedron_mesh.obj, line_mesh.obj,
			       <-.spheres.sphere_gp.obj},
			      {<-.polyhedron_mesh.obj, line_mesh.obj,
				<-.cylinder.cylinder_gp.obj,
				<-.spheres.sphere_gp.obj}));
      Props {
	col => {<-.<-.data.red, <-.<-.data.green,<-.<-.data.blue};
	trans => <-.<-.data.opacity;
      };
      Top {
	xform_mode = "Parent";
	xform => <-.<-.Xform[index_of(<-.<-.qdot)].xform;
      };
    };
    mlink obj_linker<NEportLevels={1,1},NEx=306.,NEy=498.> => qdot.obj;
    GroupObject<NEx=306.,NEy=548.> {
      child_objs => <-.obj_linker;
      //child_objs+nres => { <-.qdot.obj };
    };
  };
  group shell_data {
    float radius;
    float red;
    float green;
    float blue;
  };
  macro atom_shell {
    CCP3.Renderers.Model.shell_data &data<NEx=451.,NEy=77.>;
    link subdivisions<NEportLevels=1,NEx=121.,NEy=77.>;
    Field_Spher_Unif atom_field<NEx=121.,NEy=154.,NEportLevels={0,1}> {
      dims => {2,<-.subdivisions,<-.subdivisions};
      ndim = 3;
      points => {
	<-.data.radius,1.5708,0.,
	(<-.data.radius + 0.001),3.14159,6.28318
      };
      nspace = 3;
    };
    DVbounds DVbounds<NEx=99.,NEy=242.> {
      in => <-.atom_field;
      hull = 0;
      edges = 0;
      faces = 1;
      imin = 0;
      imax = 1;
      jmin = 0;
      jmax = 0;
      kmin = 0;
      kmax = 0;
      data = 0;
      component = 0;
    };
    DataObjectNoTexture shell<NEx=143.,NEy=330.> {
      in => <-.DVbounds.out;
      Props {
	col+nres => {
	  <-.<-.data.red,
	  <-.<-.data.green,
	  <-.<-.data.blue
	};
	inherit = 0;
      };
      Obj {
	xform_mode = "Parent";
      };
      obj<NEportLevels={1,3}>;
    };
  };
  Model_Base Shell {
    int subdivisions<NEportLevels=1,NEx=165.,NEy=99.> = 8;
    CCP3.Renderers.Model.shell_data central_data<NEx=385.,NEy=99.>;
    int nshells<NEportLevels=1,NEx=605.,NEy=99.> = 0;
    CCP3.Renderers.Model.shell_data shell_data<NEx=825.,NEy=99.>[nshells];
    Field_Spher_Unif Field_Spher_Unif<NEx=176.,NEy=198.,NEportLevels={0,1}> {
      dims => {2,<-.subdivisions,<-.subdivisions};
      ndim = 3;
      points = {0.5,0.,0.,0.501,3.14159,6.28318};
      nspace = 3;
    };
    DVbounds DVbounds<NEx=176.,NEy=286.> {
      in => <-.Field_Spher_Unif;
      hull = 0;
      edges = 0;
      faces = 1;
      imin = 0;
      imax = 1;
      jmin = 0;
      jmax = 0;
      kmin = 0;
      kmax = 0;
      data = 0;
      component = 0;
    };
    DataObjectNoTexture central_atom<NEx=176.,NEy=363.> {
      in => <-.DVbounds.out;
      Props {
	col+nres => {
	  <-.<-.central_data.red,
	  <-.<-.central_data.green,
	  <-.<-.central_data.blue
	};
	inherit = 0;
      };
      Obj {
	xform_mode = "Parent";
      };
    };
    atom_shell atom_shell<NEx=605.,NEy=275.,NEportLevels={0,1}>[nshells] {
      subdivisions => <-.subdivisions;
      data => <-.shell_data[index_of(<-.atom_shell)];
      shell {
	Obj {
	  name => str_format("atom_shell %d", index_of(<-.<-.<-.atom_shell)+1);
	};
      };
    };
    mlink mlink<NEportLevels=1,NEx=603.,NEy=333.> => atom_shell.shell.obj;
    GroupObject shells<NEx=605.,NEy=396.> {
      //child_objs => {
      //	<-.atom_shell.shell.obj
      //};
      Top {
	xform_mode = "Parent";
      };
    };
    GroupObject {
      child_objs => {<-.central_atom.obj, <-.shells.obj};
      Top {
	xform_mode = "Parent";
      };
    };
  };
};
