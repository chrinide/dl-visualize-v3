
flibrary Planes<NEeditable=1,
		export_cxx=1,
		build_dir="avs/src/express",
		libdeps="GD",
		cxx_hdr_files="avs/src/express/model.hxx avs/src/express/points.hxx avs/src/express/plots.hxx avs/src/express/misc.hxx avs/src/core/render/st2D_gen.hxx avs/src/core/render/stag_gen.hxx avs/src/core/render/rtag_gen.hxx avs/src/express/display.hxx avs/src/express/scenes.hxx ag/ag_omx.hxx avs/src/core/render/c2D_gen.hxx avs/src/core/render/sl_gen.hxx",
		need_objs="GroupObject",
		out_hdr_file="planes.hxx",
		out_src_file="planes.cxx"> {
  group plane_data {
    float ox;
    float oy;
    float oz;
    float ax;
    float ay;
    float az;
    float bx;
    float by;
    float bz;
    float matrix[4][4];
  };
  group plane_params {
    float opacity;
    float red;
    float green;
    float blue;
  };
  macro plane {
    plane_params params<NEx=385.,NEy=132.> {
      opacity = 1.0;
      red = 1.0;
      green = 1.0;
      blue = 1.0;
    };
    string name;
    int visible<NEportLevels=1,NEx=363.,NEy=33.> = 1;
    link data<NEportLevels=1,NEx=143.,NEy=132.>;
    float+nres bstep[3] => {
      (data.bx - data.ox), (data.by - data.oy), (data.bz - data.oz)
    };
    float+nres points<NEportLevels=1,NEx=253.,NEy=209.>[4][3] => {
      data.ox, data.oy, data.oz,
      data.bx, data.by, data.bz,
      (data.ax + bstep[0]), (data.ay + bstep[1]), (data.az + bstep[2]),
      data.ax, data.ay, data.az
    };
    long+nres connections<NEportLevels=1,NEx=506.,NEy=209.>[4] => {
      0, 1, 2, 3
    };
    FLD_MAP.quad_mesh quad_mesh<NEx=396.,NEy=308.> {
      coord => <-.points;
      connect => <-.connections;
      DataObject {
	Props {
	  col => {
	    <-.<-.<-.params.red, <-.<-.<-.params.green, <-.<-.<-.params.blue
	  };
	  trans => <-.<-.<-.params.opacity;
	  inherit = 0;
	};
	Obj {
	  xform_mode = "Parent";
	  visible => <-.<-.<-.visible;
	  name => <-.<-.<-.name;
	};
      };
    };
    link obj<NEportLevels={1,2},NEx=308.,NEy=429.> =>
				  quad_mesh.DataObject.obj;
  };
  macro slice_plane {
   CCP3.Renderers.Planes.plane_data &plane_data<NEx=108.,NEy=45.,
     NEportLevels={2,0}>;
   int dims<NEportLevels=1,NEx=121.,NEy=143.>[2] = {8,8};
    float xlate<NEportLevels=1,NEx=540.,NEy=63.>[3] =>
      {plane_data.ox,plane_data.oy,plane_data.oz};
    float mat<NEportLevels=1,NEx=324.,NEy=63.>[4][4] => plane_data.matrix;
    group uniform_mesh<NEx=319.,NEy=231.> {
      int in_dims<NEportLevels={2,0}>[] => <-.dims;
      Mesh_Unif out<NEportLevels={0,2},export_all=2> {
        nspace => ndim;
	ndim => array_size(<-.in_dims);
	dims => <-.in_dims;
	points+nres => {0.,0.,1.0,1.0};
	xform {
	  mat => <-.<-.<-.mat;
	  xlate => <-.<-.<-.xlate;
	};
      };
    };
    link obj<NEportLevels={1,2},NEx=315.,NEy=414.> => .uniform_mesh.out;
    //link obj<NEportLevels={1,2},NEx=315.,NEy=414.> => .uniform_mesh;
  };
  macro plane_to_2D {
    link in<NEportLevels={2,1},NEx=99.,NEy=55.>;
    DVfld_match DVfld_match<NEx=385.,NEy=55.> {
      in => <-.in;
    };
    Mesh gen2D;
    CCP3.Core_Modules.Render.plane_to_2Dstruct struct2D {
      in => <-.DVfld_match.links.link2;
    };
    Mesh_Rect rect2D;
    Mesh_Unif unif2D;
    DVswitch DVswitch<NEx=308.,NEy=352.> {
      in => {
	<-.unif2D,
	<-.rect2D,
	<-.struct2D.out,
	<-.gen2D
      };
      index => DVfld_match.index;
    };
    link out<NEportLevels={1,2},NEx=308.,NEy=451.> => .DVswitch.out;
  };
  macro slice_to_2D {
    link in<NEportLevels={2,1},NEx=99.,NEy=55.>;
    DVfld_match DVfld_match<NEx=385.,NEy=55.> {
      in => <-.in;
    };
    int axis;
    CCP3.Core_Modules.Render.slice_to_2D struct2D {
      in => <-.DVfld_match.links.link2;
      //axis => <-.axis;
    };
    Mesh_Struct+Node_Data &Mesh_Struct<NEportLevels={0,1}>;
    DVswitch DVswitch<NEx=308.,NEy=352.> {
      in => {
	<-.in,
	<-.Mesh_Struct
      };
      index => DVfld_match.index - 1;
    };
    link out<NEportLevels={1,2},NEx=308.,NEy=451.> => .DVswitch.out;
    float maxx[2] => struct2D.maxx;
    float maxy[2] => struct2D.maxy;
  };
  macro copy_slice {
    link in<NEportLevels={2,1},NEx=99.,NEy=55.>;
    link out<NEportLevels={1,2},NEx=308.,NEy=451.> => .DVswitch.out;
    DVswitch DVswitch<NEx=308.,NEy=352.> {
      in => {<-.copy_rect.out,
	     <-.copy_slice.out};
      index => (DVfld_match.index - 1);
    };
    DVfld_match DVfld_match<NEx=385.,NEy=55.> {
      in => <-.in;
    };
    CCP3.Core_Modules.Render.copy_slice copy_slice<NEx=121.,NEy=198.> {
      in => <-.DVfld_match.links.link2;
      //axis => <-.axis;
      comp => <-.comp;
      context => <-.context;
    };
    CCP3.Core_Modules.Render.copy_rect copy_rect<NEx=363.,NEy=198.> {
      in => <-.DVfld_match.links.link1;
      //axis => <-.axis;
      comp => <-.comp;
      context => <-.context;
    };
    link context<NEportLevels={2,1},NEx=561.,NEy=110.>;
    int axis<NEportLevels=1,NEx=242.,NEy=110.>;
    int comp<NEportLevels=1,NEx=396.,NEy=110.>;
  };
  DV_Param_contour IsolineParam {
    int log;
  };
  macro AGisoline_base {
    Mesh_Struct+Node_Data &in_field<NEportLevels={2,1},NEx=33.,NEy=55.>;
    string name;
    int visible3D<NEportLevels=1,NEx=473.,NEy=66.> = 1;
    int visible2D<NEportLevels=1,NEx=649.,NEy=66.> = 0;
    link context<NEx=649.,NEy=121.,NEportLevels={0,1}>;
    int axis<NEportLevels=1,NEx=121.,NEy=11.>;
    float linear_levels<NEportLevels=1,NEx=286.,NEy=11.>[] =>
      init_array(.IsoParam.ncontours,.IsoParam.level_min,.IsoParam.level_max);
    float log_levels<NEportLevels=1,NEx=473.,NEy=11.>[] =>
      ((exp(init_array(.IsoParam.ncontours,0.,
		       log(((.IsoParam.level_max - .IsoParam.level_min)
			    + 1.)))) - 1.) + .IsoParam.level_min);
    //int model_type<NEportLevels=1,NEx=649.,NEy=11.>;
    string backcolour<NEportLevels=1,NEx=774.,NEy=72.> =>
      switch((((contour2D.DLV2Dscene.View.View.back_col[0] > 0.5) ||
	       (contour2D.DLV2Dscene.View.View.back_col[1] > 0.5) ||
	       (contour2D.DLV2Dscene.View.View.back_col[2] > 0.5)) + 1),
	     "black","white");
    string anticolour<NEportLevels=1,NEx=774.,NEy=72.> =>
      switch((((contour2D.DLV2Dscene.View.View.back_col[0] > 0.5) ||
	       (contour2D.DLV2Dscene.View.View.back_col[1] > 0.5) ||
	       (contour2D.DLV2Dscene.View.View.back_col[2] > 0.5)) + 1),
	     "white","black");
    IsolineParam IsoParam<export_all=2,NEx=110.,NEy=121.> {
      contour_comp = 0;
      ncontours = 10;
      level_min+nres => cache(in_field.node_data[.contour_comp].min);
      level_max+nres => cache(in_field.node_data[.contour_comp].max);
      color = 1;
      log = 0;
    };
    DVisoline DVisoline<NEx=44.,NEy=187.> {
      in => <-.in_field;
      &component => param.contour_comp;
      &level => switch((param.log + 1),<-.linear_levels,<-.log_levels);
      &color => param.color;
      IsolineParam &param<NEportLevels={2,0}> => <-.IsoParam;
    };
    olink out_obj<NEportLevels={1,2},NEx=121.,NEy=473.>;
    GMOD.instancer instancer<NEx=803.,NEy=231.> {
      Value => <-.visible2D;
      Group => contour2D;
      active = 2;
    };
    macro contour2D<instanced=0> {
      link visible2D => <-.visible2D;
      string+read+opt Font<NEx=783.,NEy=153.,NEportLevels={0,1}> =
	"roman-simplex";
      AGContour AGContour<NEx=495.,NEy=319.> {
	classValues => <-.<-.DVisoline.level;
	contourType => switch((<-.<-.IsoParam.color + 1),
			      "isolines", "datamappedisolines");
      };
      AGContourLegend AGContourLegend<NEx=330.,NEy=374.> {
	geometry = {5.,-3.};
	contour => <-.AGContour;
	fillColor => "transparent";
	frameColor => <-.<-.anticolour;
	labelColor => <-.<-.anticolour;
	labelFont => <-.Font;
	titleColor => <-.<-.anticolour;
	titleFont => <-.Font;
      };
      AGText AGText<NEx=671.,NEy=374.> {
	geometry = {0.,6.};
	text => {<-.title};
	height = 8.;
	color => <-.<-.anticolour;
	font => <-.Font;
      };
      string title<NEportLevels=1,NEx=286.,NEy=66.> => <-.name;
      AGContourWorld AGContourWorld<NEx=495.,NEy=374.> {
	children => {<-.AGContour,
		     <-.AGXAxis,<-.AGYAxis};
	majorTickColor => <-.<-.anticolour;
	axleColor => <-.<-.anticolour;
	labelColor => <-.<-.anticolour;
	labelFont => <-.Font;
	textColor => <-.<-.anticolour;
	textFont => <-.Font;
	isoLineLabelFont => <-.Font;
      };
      AGXAxis AGXAxis<NEx=682.,NEy=253.>;
      AGYAxis AGYAxis<NEx=682.,NEy=297.>;
      AGContourViewportObj AGContourViewportObj<NEx=594.,NEy=451.> {
	macro dataObject {
	  AGGeometryPrimitive &in<NEportLevels={2,1},
	    NEnumColors=2,NEcolor1=255,NEy=55,NEx=55> => <-.contourViewport;
	    DefaultLinear Datamap<NEy=132,NEx=77> {
	      dataMin => <-.<-.<-.<-.IsoParam.level_min;
	      dataMax => <-.<-.<-.<-.IsoParam.level_max;
	    };
	    DefaultProps Props<NEy=143,NEx=308> {
	      inherit = 0;
	    };
	    DefaultObject Obj<NEportLevels={0,1},NEy=352,NEx=77> {
	      input => <-.in;
	      dmap => <-.Datamap;
	      xform => <-.Xform;
	      props => <-.Props;
	      modes => <-.Modes;
	      string name => name_of(<-.<-.<-);
	      pick_info => <-.PickInfo;
	    };
	    olink obj<NEy=352,NEx=297,NEportLevels={1,3}> => .Obj;
	    DefaultModes Modes<NEy=110,NEx=363>;
	    DefaultXform Xform<NEy=176,NEx=253>;
	    DefaultPickInfo PickInfo<NEy=77,NEx=429>;
	};
	contourViewport {
	  geometry => { -5.0, -4.0, 5.0, 4.0 };
	  fillColor = "transparent";
	  children => {<-.<-.AGText,
		       <-.<-.AGContourLegend,<-.<-.AGContourWorld};
	  frameColor => <-.<-.<-.anticolour;
	};
      };
      link out_obj<NEportLevels=1,NEx=627.,NEy=495.> =>
	AGContourViewportObj.dataObject.obj;
      CCP3.Core_Modules.Render.attach_plane attach<NEx=682.,NEy=99.> {
	trigger => <-.DLV2Dscene.View.ViewUI.ViewPanel.UI.UIshell.cancel;
	visible => <-.visible2D;
      };
      CCP3.Viewers.Scenes.DLV2Dscene DLV2Dscene<NEx=792.,NEy=572.,
	NEportLevels={0,1}> {
	Top {
	  child_objs => {<-.<-.out_obj};
	};
	View {
	  ViewUI {
	    ViewPanel {
	      UI {
		UIshell {
		  visible => <-.<-.<-.<-.<-.<-.visible2D;
		};
	      };
	    };
	  };
	};
      };
    };
  };
  AGisoline_base AGisoline {
    out_obj => .obj.obj;
    macro obj<locked=0,NEx=110.,NEy=396.> {
      group &in<NEportLevels={2,1},NEnumColors=4,NEcolor0=255,NEy=132,NEx=55> => <-.DVisoline.out;
      imlink child_objs<NEy=77,NEx=132>;
      DefaultLinear Datamap<NEy=231,NEx=11,export_all=1> {
	dataMin => <-.<-.IsoParam.level_min;
	dataMax => <-.<-.IsoParam.level_max;
      };
      DefaultProps Props<NEy=198,NEx=297,export_all=1>;
      DefaultModes Modes<NEy=165,NEx=341,export_all=1>;
      DefaultPickInfo PickInfo<NEy=99,NEx=473,export_all=1>;
      AltObject AltObject<export_all=1,instanced=0> {
	alt_in => <-.in;
      };
      DefaultObject Obj<NEportLevels={0,1},NEy=385,NEx=110,export_all=2> {
	input => <-.in;
	dmap => <-.Datamap;
	xform => in.xform;
	props => <-.Props;
	modes => <-.Modes;
	objects => <-.child_objs;
	altobj => <-.AltObject.obj;
	xform_mode = "Parent";
	name => <-.<-.name;
	pick_info => <-.PickInfo;
	visible => <-.<-.visible3D;
      };
      GMOD.instancer instancer {
	Value => <-.Obj.use_altobj;
	Group => <-.AltObject;
      };
      olink obj<NEy=385,NEx=341> => .Obj;
    };
    contour2D {
      plane_to_2D plane_to_2D<NEx=473.,NEy=187.> {
	in => <-.<-.in_field;
      };
      DVcomb_mesh_and_data DVcomb_mesh_and_data<NEx=451.,NEy=253.> {
	in_mesh => <-.plane_to_2D.out;
	in_nd => <-.DVextract_comp.out;
	out => merge(.in_mesh,.in_nd);
      };
      DVextract_comp DVextract_comp<NEx=649.,NEy=187.> {
	component => <-.<-.IsoParam.contour_comp;
	in => <-.<-.in_field;
      };
      AGContour {
	field => <-.DVcomb_mesh_and_data.out;
      };
    };
  };
  AGisoline_base AGisoline_rep {
    // Todo - what about axis?
    link n<NEportLevels=1,NEx=451.,NEy=176.>;
    Xform &Xform<NEx=627.,NEy=176.>[];
    Mesh+Node_Data Mesh<NEx=66.,NEy=319.,NEportLevels={0,1}>[.n] {
      nnodes => <-.DVisoline.out.nnodes;
      nspace => <-.DVisoline.out.nspace;
      &coordinates => <-.DVisoline.out.coordinates;
      ncell_sets => <-.DVisoline.out.ncell_sets;
      &cell_set => <-.DVisoline.out.cell_set;
      xform {
	mat => <-.<-.DVisoline.out.xform.mat;
	xlate => <-.<-.DVisoline.out.xform.xlate
	  + <-.<-.Xform[index_of(<-.<-.Mesh)].xform.xlate;
	center => <-.<-.DVisoline.out.xform.center;
      };
      nnode_data => <-.DVisoline.out.nnode_data;
      &node_data[] => <-.DVisoline.out.node_data;
    };
    DataObjectNoTexture DataObjectNoTexture<NEx=165.,NEy=385.>[.n] {
      group &in<NEportLevels={2,1},NEnumColors=4,NEcolor0=255,NEy=132,NEx=55> {
	GDxform_templ &xform;
	method render;
      } => <-.Mesh[index_of(<-.DataObjectNoTexture)];
      imlink child_objs<NEy=77,NEx=132>;
      DefaultLinear Datamap<NEy=231,NEx=11,export_all=1> {
	dataMin => <-.<-.IsoParam.level_min;
	dataMax => <-.<-.IsoParam.level_max;
      };
      DefaultProps &Props<NEy=198,NEx=297,export_all=1> => <-.obj.Props;
      DefaultModes Modes<NEy=165,NEx=341,export_all=1>;
      DefaultPickInfo PickInfo<NEy=99,NEx=473,export_all=1>;
      AltObject AltObject<export_all=1,instanced=0> {
	alt_in => in;
      };
      DefaultObject Obj<NEportLevels={0,1},NEy=385,NEx=110,export_all=2> {
	input => in;
	dmap => Datamap;
	xform => in.xform;
	props => Props;
	modes => Modes;
	use_altobj => <-.<-.obj.Top.use_altobj;
	objects => child_objs;
	altobj => AltObject.obj;
	xform_mode = "Parent";
	name => name_of(<-.<-.<-);
	pick_info => PickInfo;
      };
      GMOD.instancer instancer {
	Value => Obj.use_altobj;
	Group => AltObject;
      };
      olink obj<NEy=385,NEx=341> => .Obj;
    };
    GroupObject obj<NEx=242.,NEy=440.> {
      child_objs => <-.DataObjectNoTexture.obj;
      Top {
	xform_mode = "Parent";
	name => <-.<-.name;
	visible => <-.<-.visible3D;
      };
    };
    out_obj => .obj.obj;
    contour2D {
      copy_slice copy_slice<NEx=451.,NEy=220.> {
	in => <-.<-.in_field;
	//axis => <-.<-.axis;
	comp => <-.<-.IsoParam.contour_comp;
	context => <-.<-.context;
      };
      AGContour {
	field => <-.copy_slice.out;
      };
    };
  };
  macro isoline_base {
    Mesh+Node_Data &in_field<NEportLevels={2,1},NEx=33.,NEy=55.>;
    string name;
    // Todo - axis
    int axis;
    IsolineParam IsoParam<export_all=2,NEx=110.,NEy=121.> {
      contour_comp = 0;
      ncontours = 10;
      level_min+nres => cache(in_field.node_data[.contour_comp].min);
      level_max+nres => cache(in_field.node_data[.contour_comp].max);
      color = 1;
      log = 0;
    };
    DVisoline DVisoline<NEx=44.,NEy=187.> {
      in => <-.in_field;
      &component => param.contour_comp;
      &level => switch((param.log + 1),<-.linear_levels,<-.log_levels);
      &color => param.color;
      IsolineParam &param<NEportLevels={2,0}> => <-.IsoParam;
    };
    olink out_obj<NEportLevels={1,2},NEx=121.,NEy=473.>;
    int visible3D<NEportLevels=1,NEx=473.,NEy=66.> = 1;
    int visible2D<NEportLevels=1,NEx=649.,NEy=66.> = 0;
    //DVnode_data_labels DVnode_data_labels<NEx=231.,NEy=187.> {
    //  in => <-.in_field;
    //  int+nres ncomp => in_field.nnode_data;
    //};
    string title<NEportLevels=1,NEx=286.,NEy=66.>;
    GMOD.instancer instancer {
      Value => <-.visible2D;
      Group => contour2D;
      active = 2;
    };
    macro contour2D<instanced=0> {
      link visible2D => <-.visible2D;
      macro isoline2D<NEx=484.,NEy=319.> {
	ilink in_field<export_all=1>;
	IsolineParam &IsoParam => <-.<-.IsoParam;
	DVisoline DVisoline {
	  in => in_field;
	  &component => param.contour_comp;
	  &level => switch((param.log + 1), <-.<-.<-.linear_levels,
			   <-.<-.<-.log_levels);
	  &color => param.color;
	  out<NEportLevels={0,3}>;
	  DV_Param_contour &param<NEportLevels={2,0}> => IsoParam;
	};
	olink out_fld<export_all=2> => DVisoline.out;
      };
      link out_obj<NEportLevels=1,NEx=627.,NEy=495.>;
      CCP3.Core_Modules.Render.attach_plane attach<NEx=682.,NEy=99.> {
	trigger => <-.DLV2Dscene.View.ViewUI.ViewPanel.UI.UIshell.cancel;
	visible => <-.visible2D;
      };
      CCP3.Viewers.Scenes.DLV2Dscene DLV2Dscene<NEx=792.,NEy=572.,
	NEportLevels={0,1}> {
	Top {
	  child_objs => {<-.<-.out_obj};
	};
	View {
	  ViewUI {
	    ViewPanel {
	      UI {
		UIshell {
		  visible => <-.<-.<-.<-.<-.<-.visible2D;
		};
	      };
	    };
	  };
	};
      };
    };
    float linear_levels<NEportLevels=1,NEx=286.,NEy=11.>[] =>
      init_array(.IsoParam.ncontours,.IsoParam.level_min,.IsoParam.level_max);
    float log_levels<NEportLevels=1,NEx=473.,NEy=11.>[] =>
      ((exp(init_array(.IsoParam.ncontours,0.,
		       log(((.IsoParam.level_max - .IsoParam.level_min) + 1.)))
	    ) - 1.) + .IsoParam.level_min);
  };
  isoline_base isoline {
    out_obj => .obj.obj;
    macro obj<locked=0,NEx=110.,NEy=396.> {
      group &in<NEportLevels={2,1},NEnumColors=4,NEcolor0=255,NEy=132,NEx=55> => <-.DVisoline.out;
      imlink child_objs<NEy=77,NEx=132>;
      DefaultLinear Datamap<NEy=231,NEx=11,export_all=1> {
	dataMin => <-.<-.IsoParam.level_min;
	dataMax => <-.<-.IsoParam.level_max;
      };
      DefaultProps Props<NEy=198,NEx=297,export_all=1>;
      DefaultModes Modes<NEy=165,NEx=341,export_all=1>;
      DefaultPickInfo PickInfo<NEy=99,NEx=473,export_all=1>;
      AltObject AltObject<export_all=1,instanced=0> {
	alt_in => <-.in;
      };
      DefaultObject Obj<NEportLevels={0,1},NEy=385,NEx=110,export_all=2> {
	input => <-.in;
	dmap => <-.Datamap;
	xform => in.xform;
	props => <-.Props;
	modes => <-.Modes;
	objects => <-.child_objs;
	altobj => <-.AltObject.obj;
	xform_mode = "Parent";
	name => name_of(<-.<-);
	pick_info => <-.PickInfo;
      };
      GMOD.instancer instancer {
	Value => <-.Obj.use_altobj;
	Group => <-.AltObject;
      };
      olink obj<NEy=385,NEx=341> => .Obj;
    };
    contour2D {
      isoline2D {
	in_field => <-.DVcomb_mesh_and_data.out;
      };
      DataObjectNoTexture obj2D<NEx=473.,NEy=374.> {
	in => <-.isoline2D.out_fld;
	Datamap {
	  dataMin => <-.<-.<-.IsoParam.level_min;
	  dataMax => <-.<-.<-.IsoParam.level_max;
	};
      };
      plane_to_2D plane_to_2D<NEx=473.,NEy=187.> {
	in => <-.<-.in_field;
      };
      DVcomb_mesh_and_data DVcomb_mesh_and_data<NEx=451.,NEy=253.> {
	in_mesh => <-.plane_to_2D.out;
	in_nd => <-.<-.in_field;
	out => merge(.in_mesh,.in_nd);
      };
      out_obj => obj2D.obj;
    };
  };
  isoline_base isoline_rep<NEx=363.,NEy=132.> {
    int n<NEportLevels=1,NEx=451.,NEy=176.>;
    Xform &Xform<NEx=627.,NEy=176.>[];
    Mesh+Node_Data Mesh<NEx=66.,NEy=319.,NEportLevels={0,1}>[.n] {
      nnodes => <-.DVisoline.out.nnodes;
      nspace => <-.DVisoline.out.nspace;
      &coordinates => <-.DVisoline.out.coordinates;
      ncell_sets => <-.DVisoline.out.ncell_sets;
      &cell_set => <-.DVisoline.out.cell_set;
      xform {
	mat => <-.<-.DVisoline.out.xform.mat;
	xlate => <-.<-.DVisoline.out.xform.xlate
	  + <-.<-.Xform[index_of(<-.<-.Mesh)].xform.xlate;
	center => <-.<-.DVisoline.out.xform.center;
      };
      nnode_data => <-.DVisoline.out.nnode_data;
      &node_data[] => <-.DVisoline.out.node_data;
    };
    DataObjectNoTexture DataObjectNoTexture<NEx=165.,NEy=385.>[.n] {
      in => <-.Mesh[index_of(<-.DataObjectNoTexture)];
      Datamap {
	dataMin => <-.<-.IsoParam.level_min;
	dataMax => <-.<-.IsoParam.level_max;
      };
      &Props => <-.obj.Props;
      Obj {
	use_altobj => <-.<-.obj.Top.use_altobj;
	xform_mode = "Parent";
	name => name_of(<-.<-.<-);
      };
    };
    GroupObject obj<NEx=242.,NEy=440.> {
      child_objs => <-.DataObjectNoTexture.obj;
      Top {
	xform_mode = "Parent";
      };
    };
    out_obj => .obj.obj;
    contour2D {
      // Todo - doesn't work
      CCP3.Core_Modules.Render.common_options context {
	na = 1;
	nb = 1;
	nc = 1;
	centre_cell = 0;
	conventional_cell = 0;
	tolerance = 0.000001;
      };
      copy_slice plane_to_2D<NEx=473.,NEy=187.> {
	in => <-.<-.in_field;
	comp => <-.<-.IsoParam.contour_comp;
	context => <-.context;
      };
      DVcomb_mesh_and_data DVcomb_mesh_and_data<NEx=451.,NEy=253.> {
	in_mesh => <-.plane_to_2D.out;
	in_nd => <-.<-.in_field;
	out => merge(.in_mesh,.in_nd);
      };
      isoline2D {
	in_field => <-.DVcomb_mesh_and_data.out;
      };
      /*
      slice_to_2D slice_to_2D<NEx=374.,NEy=275.> {
	in => <-.<-.in_field;
	//axis => <-.<-.axis;
      };
      isoline2D {
	in_field => <-.<-.in_field;
      };
      */
      int n2D<NEportLevels=1,NEx=462.,NEy=231.> = 1;
      //Xform Xform2D<NEx=638.,NEy=231.>;
      Mesh+Node_Data Mesh2D<NEx=605.,NEy=330.,NEportLevels={0,1}>[.n2D] {
	nnodes => <-.isoline2D.out_fld.nnodes;
	nspace => <-.isoline2D.out_fld.nspace;
	&coordinates => <-.isoline2D.out_fld.coordinates;
	ncell_sets => <-.isoline2D.out_fld.ncell_sets;
	&cell_set => <-.isoline2D.out_fld.cell_set;
	//&xform => <-.Xform2D.xform[index_of(<-.Mesh2D)];
	nnode_data => <-.isoline2D.out_fld.nnode_data;
	&node_data[] => <-.isoline2D.out_fld.node_data;
      };
      macro contour2D<NEx=473.,NEy=385.>[.n2D] {
	group &in<NEportLevels={2,1},NEnumColors=4,
		  NEcolor0=255,NEy=132,NEx=55> {
	  GDxform_templ &xform;
	  method render;
	} => <-.Mesh2D[index_of(<-.contour2D)];
	imlink child_objs<NEy=77,NEx=132>;
	DefaultLinear Datamap<NEy=231,NEx=11,export_all=1> {
	  dataMin => <-.<-.<-.IsoParam.level_min;
	  dataMax => <-.<-.<-.IsoParam.level_max;
	};
	DefaultProps Props<NEy=198,NEx=297,export_all=1>;
	DefaultModes Modes<NEy=165,NEx=341,export_all=1>;
	DefaultPickInfo PickInfo<NEy=99,NEx=473,export_all=1>;
	AltObject AltObject<export_all=1,instanced=0> {
	  alt_in => <-.in;
	};
	DefaultObject Obj<NEportLevels={0,1},NEy=385,NEx=110,export_all=2> {
	  input => <-.in;
	  dmap => <-.Datamap;
	  xform => in.xform;
	  //xform_mode = "None";
	  props => <-.Props;
	  modes => <-.Modes;
	  objects => <-.child_objs;
	  altobj => <-.AltObject.obj;
	  name => name_of(<-.<-);
	  pick_info => <-.PickInfo;
	};
	GMOD.instancer instancer {
	  Value => <-.Obj.use_altobj;
	  Group => <-.AltObject;
	};
	olink obj<NEy=385,NEx=341> => .Obj;
      };
      GroupObject obj2D<NEx=539.,NEy=440.> {
	child_objs => <-.contour2D.obj;
      };
      out_obj => obj2D.obj;
    };
  };
  macro disoline_base {
    Mesh+Node_Data &in_field<NEportLevels={2,1},NEx=33.,NEy=55.>;
    string name;
    DV_Param_clamp ClampParam {
      vector => <-.IsoParam.contour_comp;
      component = 0;
      below = 1;
      above = 1;
      reset_minmax = 1;
      min_value => <-.IsoParam.level_min;
      max_value => <-.IsoParam.level_max;
    };
    Clamp Clamp {
      in_fld => <-.in_field;
      param => <-.ClampParam;
    };
    Clamp clamp_p {
      in_fld => <-.Clamp.out_fld;
      DVextract_comp {
	component = 0;
      };
      DVclamp {
	component = 0;
	below = 1;
	above = 0;
	reset_minmax = 0;
	min_value => switch (((<-.<-.IsoParam.level_min < 0.0) + 1),
			    <-.<-.IsoParam.level_min, 0.0);
	max_value => <-.<-.IsoParam.level_max;
      };
    };
    Clamp clamp_n {
      in_fld => <-.Clamp.out_fld;
      DVextract_comp {
	component = 0;
      };
      DVclamp {
	component = 0;
	below = 0;
	above = 1;
	reset_minmax = 0;
	min_value => <-.<-.IsoParam.level_min;
	max_value => switch (((<-.<-.IsoParam.level_max > 0.0) + 1),
			     <-.<-.IsoParam.level_max, 0.0);
      };
    };
    IsolineParam IsoParam<export_all=2,NEx=110.,NEy=121.> {
      contour_comp = 0;
      ncontours = 10;
      level_min+nres => cache(in_field.node_data[.contour_comp].min);
      level_max+nres => cache(in_field.node_data[.contour_comp].max);
      color = 1;
      log = 0;
    };
    DVisoline DVisoline_p<NEx=44.,NEy=187.> {
      in => <-.clamp_p.out_fld;
      component = 0;
      &level => switch((param.log + 1),<-.linear_levels,<-.log_levels);
      &color => param.color;
      IsolineParam &param<NEportLevels={2,0}> => <-.IsoParam;
    };
    DVisoline DVisoline_n<NEx=44.,NEy=187.> {
      in => <-.clamp_n.out_fld;
      component = 0;
      &level => switch((param.log + 1),<-.linear_levels,<-.log_levels);
      &color => param.color;
      IsolineParam &param<NEportLevels={2,0}> => <-.IsoParam;
    };
    olink out_obj<NEportLevels={1,2},NEx=121.,NEy=473.>;
    int visible3D<NEportLevels=1,NEx=473.,NEy=66.> = 1;
    int visible2D<NEportLevels=1,NEx=649.,NEy=66.> = 0;
    string title<NEportLevels=1,NEx=286.,NEy=66.>;
    GMOD.instancer instancer<NEx=803.,NEy=231.> {
      Value => <-.visible2D;
      Group => contour2D;
      active = 2;
    };
    macro contour2D<instanced=0> {
      link visible2D => <-.visible2D;
      macro isoline2D_p<NEx=484.,NEy=319.> {
	ilink in_field<export_all=1>;
	IsolineParam &IsoParam => <-.<-.IsoParam;
	DVisoline DVisoline {
	  in => in_field;
	  &component = 0;
	  &level => switch((param.log + 1), <-.<-.linear_levels, <-.<-.log_levels);
	  &color => param.color;
	  DV_Param_contour &param<NEportLevels={2,0}> => IsoParam;
	};
	olink out_fld<export_all=2> => DVisoline.out;
      };
      macro isoline2D_n<NEx=484.,NEy=319.> {
	ilink in_field<export_all=1>;
	IsolineParam &IsoParam => <-.<-.IsoParam;
	DVisoline DVisoline {
	  in => in_field;
	  &component = 0;
	  &level => switch((param.log + 1), <-.<-.linear_levels, <-.<-.log_levels);
	  &color => param.color;
	  DV_Param_contour &param<NEportLevels={2,0}> => IsoParam;
	};
	olink out_fld<export_all=2> => DVisoline.out;
      };
      link out_obj<NEportLevels=1,NEx=627.,NEy=495.>;
      CCP3.Core_Modules.Render.attach_plane attach<NEx=682.,NEy=99.> {
	trigger => <-.DLV2Dscene.View.ViewUI.ViewPanel.UI.UIshell.cancel;
	visible => <-.visible2D;
      };
      CCP3.Viewers.Scenes.DLV2Dscene DLV2Dscene<NEx=792.,NEy=572.,
	NEportLevels={0,1}> {
	Top {
	  child_objs => {<-.<-.out_obj};
	};
	View {
	  ViewUI {
	    ViewPanel {
	      UI {
		UIshell {
		  visible => <-.<-.<-.<-.<-.<-.visible2D;
		};
	      };
	    };
	  };
	};
      };
    };
    float linear_levels<NEportLevels=1,NEx=286.,NEy=11.>[] =>
      init_array(.IsoParam.ncontours,.IsoParam.level_min,.IsoParam.level_max);
    float log_levels<NEportLevels=1,NEx=473.,NEy=11.>[] =>
      ((exp(init_array(.IsoParam.ncontours,0.,
	    log(((.IsoParam.level_max - .IsoParam.level_min) + 1.)))) - 1.)
       + .IsoParam.level_min);
    int model_type<NEportLevels=1,NEx=649.,NEy=11.>;
    int axis<NEportLevels=1,NEx=121.,NEy=11.>;
  };
  disoline_base disoline {
    out_obj => .obj.obj;
    macro plines<locked=0,NEx=110.,NEy=396.> {
      group &in<NEportLevels={2,1},NEnumColors=4,NEcolor0=255,NEy=132,NEx=55> => <-.DVisoline_p.out;
      imlink child_objs<NEy=77,NEx=132>;
      DefaultLinear Datamap<NEy=231,NEx=11,export_all=1> {
	dataMin => <-.<-.IsoParam.level_min;
	dataMax => <-.<-.IsoParam.level_max;
      };
      DefaultProps Props<NEy=198,NEx=297,export_all=1>;
      DefaultModes Modes<NEy=165,NEx=341,export_all=1>;
      DefaultPickInfo PickInfo<NEy=99,NEx=473,export_all=1>;
      AltObject AltObject<export_all=1,instanced=0> {
	alt_in => <-.in;
      };
      DefaultObject Obj<NEportLevels={0,1},NEy=385,NEx=110,export_all=2> {
	input => <-.in;
	dmap => <-.Datamap;
	xform => in.xform;
	props => <-.Props;
	modes => <-.Modes;
	objects => <-.child_objs;
	altobj => <-.AltObject.obj;
	xform_mode = "Parent";
	name => name_of(<-.<-);
	pick_info => <-.PickInfo;
	visible => (<-.<-.IsoParam.level_max >= 0.0);
      };
      GMOD.instancer instancer {
	Value => <-.Obj.use_altobj;
	Group => <-.AltObject;
      };
      olink obj<NEy=385,NEx=341> => .Obj;
    };
    macro nlines<locked=0> {
      group &in<NEportLevels={2,1},NEnumColors=4,NEcolor0=255,NEy=132,NEx=55> => <-.DVisoline_n.out;
      imlink child_objs<NEy=77,NEx=132>;
      DefaultLinear Datamap<NEy=231,NEx=11,export_all=1> {
	dataMin => <-.<-.IsoParam.level_min;
	dataMax => <-.<-.IsoParam.level_max;
      };
      DefaultProps Props<NEy=198,NEx=297,export_all=1> {
	line_style = "Dashed";
      };
      DefaultModes Modes<NEy=165,NEx=341,export_all=1>;
      DefaultPickInfo PickInfo<NEy=99,NEx=473,export_all=1>;
      AltObject AltObject<export_all=1,instanced=0> {
	alt_in => <-.in;
      };
      DefaultObject Obj<NEportLevels={0,1},NEy=385,NEx=110,export_all=2> {
	input => <-.in;
	dmap => <-.Datamap;
	xform => in.xform;
	props => <-.Props;
	modes => <-.Modes;
	objects => <-.child_objs;
	altobj => <-.AltObject.obj;
	xform_mode = "Parent";
	name => name_of(<-.<-);
	pick_info => <-.PickInfo;
	visible => (<-.<-.IsoParam.level_min < 0.0);
      };
      GMOD.instancer instancer {
	Value => <-.Obj.use_altobj;
	Group => <-.AltObject;
      };
      olink obj<NEy=385,NEx=341> => .Obj;
    };
    GroupObject obj {
      child_objs => {
	<-.plines.obj,
	<-.nlines.obj
      };
      Top {
	xform_mode = "Parent";
      };
    };
    contour2D {
      DVcomb_mesh_and_data mesh2Dp<NEx=451.,NEy=253.> {
	in_mesh => <-.plane_to_2Dp.out;
	in_nd => <-.<-.clamp_p.out_fld;
	out => merge(.in_mesh,.in_nd);
      };
      DVcomb_mesh_and_data mesh2Dn {
	in_mesh => <-.plane_to_2Dn.out;
	in_nd => <-.<-.clamp_n.out_fld;
	out => merge(.in_mesh,.in_nd);
      };
      isoline2D_p {
	in_field => mesh2Dp.out;
      };
      isoline2D_n {
	in_field => mesh2Dn.out;
      };
      DataObjectNoTexture plines2D {
	in => <-.isoline2D_p.out_fld;
	Datamap {
	  dataMin => <-.<-.<-.IsoParam.level_min;
	  dataMax => <-.<-.<-.IsoParam.level_max;
	};
	Obj {
	  visible => (<-.<-.<-.IsoParam.level_max >= 0.0);
	};
      };
      DataObjectNoTexture nlines2D {
	in => <-.isoline2D_n.out_fld;
	Datamap {
	  dataMin => <-.<-.<-.IsoParam.level_min;
	  dataMax => <-.<-.<-.IsoParam.level_max;
	};
	Props {
	  line_style = "Dashed";
	};
	Obj {
	  visible => (<-.<-.<-.IsoParam.level_min < 0.0);
	};
      };
      GroupObject obj2D<NEx=572.,NEy=440.> {
	child_objs => {
	  <-.plines2D.obj,
	  <-.nlines2D.obj
	};
	Top {
	  xform_mode = "Parent";
	};
      };
      plane_to_2D plane_to_2Dp<NEx=473.,NEy=187.> {
	in => <-.<-.clamp_p.out_fld;
      };
      plane_to_2D plane_to_2Dn<NEx=473.,NEy=187.> {
	in => <-.<-.clamp_n.out_fld;
      };
      out_obj => obj2D.obj;
    };
  };
  disoline_base disoline_rep_base {
    int n<NEportLevels=1,NEx=451.,NEy=176.>;
    Xform Xform<NEx=627.,NEy=176.>[];
    Mesh+Node_Data Meshp<NEx=66.,NEy=319.,NEportLevels={0,1}>[.n] {
      nnodes => <-.DVisoline_p.out.nnodes;
      nspace => <-.DVisoline_p.out.nspace;
      &coordinates => <-.DVisoline_p.out.coordinates;
      ncell_sets => <-.DVisoline_p.out.ncell_sets;
      &cell_set => <-.DVisoline_p.out.cell_set;
      xform {
	mat => <-.<-.DVisoline_p.out.xform.mat;
	xlate => <-.<-.DVisoline_p.out.xform.xlate + <-.<-.Xform.xform[index_of(<-.<-.Meshp)].xlate;
	center => <-.<-.DVisoline_p.out.xform.center;
      };
      nnode_data => <-.DVisoline_p.out.nnode_data;
      &node_data[] => <-.DVisoline_p.out.node_data;
    };
    Mesh+Node_Data Meshn<NEportLevels={0,1}>[.n] {
      nnodes => <-.DVisoline_n.out.nnodes;
      nspace => <-.DVisoline_n.out.nspace;
      &coordinates => <-.DVisoline_n.out.coordinates;
      ncell_sets => <-.DVisoline_n.out.ncell_sets;
      &cell_set => <-.DVisoline_n.out.cell_set;
      xform {
	mat => <-.<-.DVisoline_n.out.xform.mat;
	xlate => <-.<-.DVisoline_n.out.xform.xlate + <-.<-.Xform.xform[index_of(<-.<-.Meshn)].xlate;
	center => <-.<-.DVisoline_n.out.xform.center;
      };
      nnode_data => <-.DVisoline_n.out.nnode_data;
      &node_data[] => <-.DVisoline_n.out.node_data;
    };
    DataObjectNoTexture plines<NEx=165.,NEy=385.>[.n] {
      in => <-.Meshp[index_of(<-.plines)];
      Datamap {
	dataMin => <-.<-.IsoParam.level_min;
	dataMax => <-.<-.IsoParam.level_max;
      };
      &Props => <-.pobj.Props;
      Obj {
	use_altobj => <-.<-.pobj.Top.use_altobj;
	xform_mode = "Parent";
	visible => (<-.<-.IsoParam.level_max >= 0.0);
	name => ("p" + name_of(<-.<-.<-));
      };
    };
    DataObjectNoTexture nlines<NEx=165.,NEy=385.>[.n] {
      in => <-.Meshn[index_of(<-.nlines)];
      Datamap {
	dataMin => <-.<-.IsoParam.level_min;
	dataMax => <-.<-.IsoParam.level_max;
      };
      &Props => <-.nobj.Props;
      Obj {
	use_altobj => <-.<-.nobj.Top.use_altobj;
	xform_mode = "Parent";
	visible => (<-.<-.IsoParam.level_min < 0.0);
	name => ("n" + name_of(<-.<-.<-));
      };
    };
    GroupObject pobj {
      child_objs => <-.plines.obj;
      Top {
	xform_mode = "Parent";
      };
    };
    GroupObject nobj {
      child_objs => <-.nlines.obj;
      Props {
	line_style = "Dashed";
      };
      Top {
	xform_mode = "Parent";
      };
    };
    GroupObject obj<NEx=242.,NEy=440.> {
      child_objs => {
	<-.pobj.obj,
	<-.nobj.obj
      };
      Top {
	xform_mode = "Parent";
      };
    };
    out_obj => .obj.obj;
  };
  disoline_rep_base disoline_rep {
    // There seems to be too many contained objects to contruct in the
    // constructor for the Compaq if you do 2D and 3D in the disoline_rep_base
    // Todo axis
    int axis;
    contour2D {
      int n2D<NEportLevels=1,NEx=462.,NEy=231.>;
      Xform Xform2D<NEx=638.,NEy=231.>;
      slice_to_2D slice_to_2D<NEx=374.,NEy=275.> {
	in => <-.<-.Clamp.out_fld;
	//axis => <-.<-.axis;
      };
      isoline2D_p {
	in_field => <-.<-.clamp_p.out_fld;
      };
      isoline2D_n {
	in_field => <-.<-.clamp_n.out_fld;
      };
      Mesh+Node_Data Mesh2Dp<NEx=605.,NEy=330.,NEportLevels={0,1}>[.n2D] {
	nnodes => <-.isoline2D_p.out_fld.nnodes;
	nspace => <-.isoline2D_p.out_fld.nspace;
	&coordinates => <-.isoline2D_p.out_fld.coordinates;
	ncell_sets => <-.isoline2D_p.out_fld.ncell_sets;
	&cell_set => <-.isoline2D_p.out_fld.cell_set;
	&xform => <-.Xform2D.xform[index_of(<-.Mesh2Dp)];
	nnode_data => <-.isoline2D_p.out_fld.nnode_data;
	&node_data[] => <-.isoline2D_p.out_fld.node_data;
      };
      Mesh+Node_Data Mesh2Dn<NEportLevels={0,1}>[.n2D] {
	nnodes => <-.isoline2D_n.out_fld.nnodes;
	nspace => <-.isoline2D_n.out_fld.nspace;
	&coordinates => <-.isoline2D_n.out_fld.coordinates;
	ncell_sets => <-.isoline2D_n.out_fld.ncell_sets;
	&cell_set => <-.isoline2D_n.out_fld.cell_set;
	&xform => <-.Xform2D.xform[index_of(<-.Mesh2Dn)];
	nnode_data => <-.isoline2D_n.out_fld.nnode_data;
	&node_data[] => <-.isoline2D_n.out_fld.node_data;
      };
      DataObjectNoTexture contour2Dp<NEx=165.,NEy=385.>[.n2D] {
	in => <-.Mesh2Dp[index_of(<-.contour2Dp)];
	Datamap {
	  dataMin => <-.<-.<-.IsoParam.level_min;
	  dataMax => <-.<-.<-.IsoParam.level_max;
	};
	Obj {
	  visible => (<-.<-.IsoParam.level_max >= 0.0);
	  name => ("p" + name_of(<-.<-.<-));
	};
      };
      DataObjectNoTexture contour2Dn<NEx=165.,NEy=385.>[.n2D] {
	in => <-.Mesh2Dn[index_of(<-.contour2Dn)];
	Datamap {
	  dataMin => <-.<-.<-.IsoParam.level_min;
	  dataMax => <-.<-.<-.IsoParam.level_max;
	};
	Props {
	  line_style = "Dashed";
	};
	Obj {
	  visible => (<-.<-.IsoParam.level_min < 0.0);
	  name => ("n" + name_of(<-.<-.<-));
	};
      };
      GroupObject pobj2D {
	child_objs => <-.contour2Dp.obj;
      };
      GroupObject nobj2D {
	child_objs => <-.contour2Dn.obj;
      };
      GroupObject obj2D<NEx=539.,NEy=440.> {
	child_objs => {
	  <-.pobj2D.obj,
	  <-.nobj2D.obj
	};
      };
      out_obj => obj2D.obj;
    };
  };
  DV_Param_contour ContourParam {
    int colour_data;
  };
  macro solid_contour_base {
    Mesh+Node_Data &in_field<NEportLevels={2,1}>;
    string name;
    ContourParam ContourParam<export_all=2> {
      contour_comp = 0;
      level_min+nres => cache(in_field.node_data[contour_comp].min);
      level_max+nres => cache(in_field.node_data[contour_comp].max);
      colour_data = 0;
    };
    /*DVnode_data_labels DVnode_data_labels {
      in => <-.in_field;
      int+nres ncomp => in_field.nnode_data;
      };*/
    DVcontour DVcontour {
      in => in_field;
      &contour_comp => param.contour_comp;
      map_comp => { param.colour_data };
      &level_min => param.level_min;
      &level_max => param.level_max;
      DV_Param_contour &param<NEportLevels={2,0},NEy=22,NEx=176> => ContourParam;
    };
    olink out_obj;
    int visible3D<NEportLevels=1,NEx=44.,NEy=99.> = 1;
    int visible2D<NEportLevels=1,NEx=209.,NEy=99.> = 0;
    string title<NEportLevels=1,NEx=363.,NEy=121.>;
    GMOD.instancer instancer<NEx=803.,NEy=231.> {
      Value => <-.visible2D;
      Group => contour2D;
      active = 2;
    };
    macro contour2D<instanced=0> {
      macro contour2D<NEx=550.,NEy=330.> {
	ilink in_field<export_all=1>;
	DV_Param_contour &ContourParam => <-.<-.ContourParam;
	DVcontour DVcontour {
	  in => in_field;
	  &contour_comp => param.contour_comp;
	  map_comp => { param.colour_data };
	  &level_min => param.level_min;
	  &level_max => param.level_max;
	  DV_Param_contour &param<NEportLevels={2,0},NEy=22,NEx=176> =>
						       ContourParam;
	};
	olink out_fld => DVcontour.out;
      };
      CCP3.Core_Modules.Render.attach_plane attach<NEx=682.,NEy=99.> {
	trigger => <-.DLV2Dscene.View.ViewUI.ViewPanel.UI.UIshell.cancel;
	visible => <-.<-.visible2D;
      };
      CCP3.Viewers.Scenes.DLV2Dscene DLV2Dscene<NEx=792.,NEy=572.,
	NEportLevels={0,1}> {
	Top {
	  child_objs => {<-.<-.out_obj};
	};
	View {
	  ViewUI {
	    ViewPanel {
	      UI {
		UIshell {
		  visible => <-.<-.<-.<-.<-.<-.<-.visible2D;
		};
	      };
	    };
	  };
	};
      };
      link out_obj<NEportLevels=1,NEx=649.,NEy=506.>;
    };
    int model_type<NEportLevels=1>;
    int axis<NEportLevels=1>;
  };
  solid_contour_base solid_contour {
    out_obj => .obj.obj;
    DataObjectNoTexture obj {
      in => DVcontour.out;
      Obj {
	xform_mode = "Parent";
	visible => <-.<-.visible3D;
      };
    };
    contour2D {
      plane_to_2D plane_to_2D<NEx=473.,NEy=187.> {
	in => <-.<-.in_field;
      };
      Mesh+Node_Data &Mesh_Struct<NEx=539.,NEy=264.,NEportLevels={0,1}> =>
	merge(plane_to_2D.out,in_field);
      contour2D {
	in_field => <-.Mesh_Struct;
      };
      DataObjectNoTexture obj2D<NEx=484.,NEy=385.> {
	in => <-.contour2D.out_fld;
      };
      out_obj => obj2D.obj;
    };
  };
  solid_contour_base solid_contour_rep {
    int n<NEportLevels=1,NEx=451.,NEy=176.>;
    // Todo axis
    int axis;
    Xform Xform[];
    Mesh+Node_Data Mesh<NEx=66.,NEy=319.,NEportLevels={0,1}>[.n] {
      nnodes => <-.DVcontour.out.nnodes;
      nspace => <-.DVcontour.out.nspace;
      &coordinates => <-.DVcontour.out.coordinates;
      ncell_sets => <-.DVcontour.out.ncell_sets;
      &cell_set => <-.DVcontour.out.cell_set;
      xform {
	mat => <-.<-.DVcontour.out.xform.mat;
	xlate => <-.<-.DVcontour.out.xform.xlate + <-.<-.Xform.xform[index_of(<-.<-.Mesh)].xlate;
	center => <-.<-.DVcontour.out.xform.center;
      };
      nnode_data => <-.DVcontour.out.nnode_data;
      &node_data[] => <-.DVcontour.out.node_data;
    };
    DataObjectNoTexture DataObjectNoTexture<NEx=165.,NEy=385.>[.n] {
      in => <-.Mesh[index_of(<-.DataObjectNoTexture)];
      Datamap {
	dataMin+nres => <-.in.node_data[0].min;
	dataMax+nres => <-.in.node_data[0].max;
      };
      &Props => <-.obj.Props;
      Obj {
	use_altobj => <-.<-.obj.Top.use_altobj;
	xform_mode = "Parent";
	name => name_of(<-.<-.<-);
      };
    };
    GroupObject obj<NEx=242.,NEy=440.> {
      child_objs => <-.DataObjectNoTexture.obj;
      Top {
	xform_mode = "Parent";
	visible => <-.<-.visible3D;
      };
    };
    out_obj => .obj.obj;
    contour2D {
      slice_to_2D slice_to_2D<NEx=374.,NEy=275.> {
	in => <-.<-.in_field;
	//axis => <-.<-.axis;
      };
      contour2D {
	in_field => <-.<-.in_field;
      };
      int n2D<NEportLevels=1,NEx=462.,NEy=231.>;
      Xform Xform2D<NEx=638.,NEy=231.>;
      Mesh+Node_Data Mesh2D<NEx=605.,NEy=330.,NEportLevels={0,1}>[.n2D] {
	nnodes => <-.contour2D.out_fld.nnodes;
	nspace => <-.contour2D.out_fld.nspace;
	&coordinates => <-.contour2D.out_fld.coordinates;
	ncell_sets => <-.contour2D.out_fld.ncell_sets;
	&cell_set => <-.contour2D.out_fld.cell_set;
	&xform => <-.Xform2D.xform[index_of(<-.Mesh2D)];
	nnode_data => <-.contour2D.out_fld.nnode_data;
	&node_data[] => <-.contour2D.out_fld.node_data;
      };
      macro contour_obj2D<NEx=473.,NEy=385.>[.n2D] {
	group &in<NEportLevels={2,1},NEnumColors=4,NEcolor0=255,NEy=132,NEx=55> {
	  GDxform_templ &xform;
	  method render;
	} => <-.Mesh2D[index_of(<-.contour_obj2D)];
	imlink child_objs<NEy=77,NEx=132>;
	DefaultLinear Datamap<NEy=231,NEx=11,export_all=1> {
	  dataMin+nres => <-.in.node_data[0].min;
	  dataMax+nres => <-.in.node_data[0].max;
	};
	DefaultProps Props<NEy=198,NEx=297,export_all=1>;
	DefaultModes Modes<NEy=165,NEx=341,export_all=1>;
	DefaultPickInfo PickInfo<NEy=99,NEx=473,export_all=1>;
	AltObject AltObject<export_all=1,instanced=0> {
	  alt_in => <-.in;
	};
	DefaultObject Obj<NEportLevels={0,1},NEy=385,NEx=110,export_all=2> {
	  input => <-.in;
	  dmap => <-.Datamap;
	  xform => in.xform;
	  //xform_mode = "None";
	  props => <-.Props;
	  modes => <-.Modes;
	  objects => <-.child_objs;
	  altobj => <-.AltObject.obj;
	  name => name_of(<-.<-);
	  pick_info => <-.PickInfo;
	};
	GMOD.instancer instancer {
	  Value => <-.Obj.use_altobj;
	  Group => <-.AltObject;
	};
	olink obj<NEy=385,NEx=341> => .Obj;
      };
      GroupObject obj2D<NEx=539.,NEy=440.> {
	child_objs => <-.contour_obj2D.obj;
      };
      out_obj => obj2D.obj;
    };
  };
  macro shaded_contour_base {
    Mesh+Node_Data &in_field<NEportLevels={2,1}>;
    string name;
    IsolineParam ContourParam<export_all=2> {
      contour_comp = 0;
      ncontours = 4;
      level_min+nres => cache(in_field.node_data[contour_comp].min);
      level_max+nres => cache(in_field.node_data[contour_comp].max);
      color = 0;
      contour_lines = 0;
      log = 0;
    };
    // Todo - remove lines from DVsolid_contours?
    DVsolid_contour DVsolid_contour {
      in => in_field;
      &contour_comp => param.contour_comp;
      &level => switch((param.log + 1), <-.linear_levels, <-.log_levels);
      &contour_lines => param.contour_lines;
      &color_lines => param.color;
      DV_Param_contour &param<NEportLevels={2,0},NEy=22,NEx=176> => ContourParam;
    };
    olink out_obj;
    int visible3D<NEportLevels=1,NEx=44.,NEy=99.> = 1;
    int visible2D<NEportLevels=1,NEx=209.,NEy=99.> = 0;
    string title<NEportLevels=1,NEx=363.,NEy=121.>;
    GMOD.instancer instancer<NEx=803.,NEy=231.> {
      Value => <-.visible2D;
      Group => contour2D;
      active = 2;
    };
    macro contour2D<instanced=0> {
      macro contour2D<NEx=550.,NEy=330.> {
	ilink in_field<export_all=1>;
	IsolineParam &ContourParam => <-.<-.ContourParam;
	DVsolid_contour DVsolid_contour {
	  in => in_field;
	  &contour_comp => param.contour_comp;
	  &level => switch((param.log + 1), <-.<-.<-.linear_levels,
			   <-.<-.<-.log_levels);
	  &contour_lines => param.contour_lines;
	  &color_lines => param.color;
	  DV_Param_contour &param<NEportLevels={2,0},NEy=22,NEx=176> =>
						       ContourParam;
	};
	olink out_fld => DVsolid_contour.out;
      };
      link out_obj<NEportLevels=1,NEx=649.,NEy=506.>;
      CCP3.Core_Modules.Render.attach_plane attach<NEx=682.,NEy=99.> {
	trigger => <-.DLV2Dscene.View.ViewUI.ViewPanel.UI.UIshell.cancel;
	visible => <-.<-.visible2D;
      };
      CCP3.Viewers.Scenes.DLV2Dscene DLV2Dscene<NEx=792.,NEy=572.,
	NEportLevels={0,1}> {
	Top {
	  child_objs => {<-.<-.out_obj};
	};
	View {
	  ViewUI {
	    ViewPanel {
	      UI {
		UIshell {
		  visible => <-.<-.<-.<-.<-.<-.<-.visible2D;
		};
	      };
	    };
	  };
	};
      };
    };
    int model_type<NEportLevels=1>;
    float linear_levels<NEportLevels=1,NEx=286.,NEy=11.>[] =>
      init_array(.ContourParam.ncontours,.ContourParam.level_min,.ContourParam.level_max);
    float log_levels<NEportLevels=1,NEx=473.,NEy=11.>[] =>
      ((exp(init_array(.ContourParam.ncontours,0.,
		       log(((.ContourParam.level_max - .ContourParam.level_min)
			    + 1.)))) - 1.) + .ContourParam.level_min);
  };
  shaded_contour_base shaded_contour {
    out_obj => .obj.obj;
    DataObjectNoTexture obj {
      in => DVsolid_contour.out;
      Datamap {
	dataMin => <-.<-.ContourParam.level_min;
	dataMax => <-.<-.ContourParam.level_max;
      };
      Obj {
	xform_mode = "Parent";
	visible => <-.<-.visible3D;
      };
    };
    contour2D {
      plane_to_2D plane_to_2D<NEx=473.,NEy=187.> {
	in => <-.<-.in_field;
      };
      Mesh+Node_Data &Mesh_Struct<NEx=539.,NEy=264.,NEportLevels={0,1}> =>
	merge(plane_to_2D.out,in_field);
      contour2D {
	in_field => <-.Mesh_Struct;
      };
      DataObjectNoTexture obj2D<NEx=484.,NEy=385.> {
	in => <-.contour2D.out_fld;
      };
      out_obj => obj2D.obj;
    };
  };
  shaded_contour_base shaded_contour_rep {
    int n<NEportLevels=1,NEx=451.,NEy=176.>;
    // Todo axis
    int axis;
    Xform Xform<NEx=627.,NEy=176.>[];
    Mesh+Cell_Data Mesh<NEx=66.,NEy=319.,NEportLevels={0,1}>[.n] {
      nnodes => <-.DVsolid_contour.out.nnodes;
      nspace => <-.DVsolid_contour.out.nspace;
      &coordinates => <-.DVsolid_contour.out.coordinates;
      ncell_sets => <-.DVsolid_contour.out.ncell_sets;
      &cell_set => <-.DVsolid_contour.out.cell_set;
      xform {
	mat => <-.<-.DVsolid_contour.out.xform.mat;
	xlate => <-.<-.DVsolid_contour.out.xform.xlate + <-.<-.Xform.xform[index_of(<-.<-.Mesh)].xlate;
	center => <-.<-.DVsolid_contour.out.xform.center;
      };
    };
    DataObjectNoTexture DataObjectNoTexture<NEx=165.,NEy=385.>[.n] {
      in => <-.Mesh[index_of(<-.DataObjectNoTexture)];
      &Props => <-.obj.Props;
      Datamap {
	dataMin => <-.<-.ContourParam.level_min;
	dataMax => <-.<-.ContourParam.level_max;
      };
      Obj {
	use_altobj => <-.<-.obj.Top.use_altobj;
	xform_mode = "Parent";
	name => name_of(<-.<-.<-);
      };
    };
    GroupObject obj<NEx=242.,NEy=440.> {
      child_objs => <-.DataObjectNoTexture.obj;
      Top {
	xform_mode = "Parent";
	visible => <-.<-.visible3D;
      };
    };
    out_obj => .obj.obj;
    contour2D {
      slice_to_2D slice_to_2D<NEx=374.,NEy=275.> {
	in => <-.<-.in_field;
	//axis => <-.<-.axis;
      };
      contour2D {
	in_field => <-.<-.in_field;
      };
      int n2D<NEportLevels=1,NEx=462.,NEy=231.>;
      Xform Xform2D<NEx=638.,NEy=231.>;
      Mesh+Cell_Data Mesh2D<NEx=605.,NEy=330.,NEportLevels={0,1}>[.n2D] {
	nnodes => <-.contour2D.out_fld.nnodes;
	nspace => <-.contour2D.out_fld.nspace;
	&coordinates => <-.contour2D.out_fld.coordinates;
	ncell_sets => <-.contour2D.out_fld.ncell_sets;
	&cell_set => <-.contour2D.out_fld.cell_set;
	&xform => <-.Xform2D.xform;
      };
      DataObjectNoTexture contour_obj2D<NEx=165.,NEy=385.>[.n2D] {
	in => <-.Mesh2D[index_of(<-.contour_obj2D)];
	&Props => <-.<-.obj.Props;
	Obj {
	  name => name_of(<-.<-.<-);
	};
      };
      GroupObject obj2D<NEx=539.,NEy=440.> {
	child_objs => <-.contour_obj2D.obj;
      };
      out_obj => obj2D.obj;
    };
  };
  // Todo - DVsurf_plot doesn't preserve the position of the 2D object
  // particularly well so replicants don't often match up since slices that
  // aren't in the xy plane are mapped into it. So not very useful.
  macro surf_plot_base {
    ilink in_field<export_all=1>;
    string name;
    DV_Param_surf_plot SurfPlotParam<export_all=2> {
      component = 0;
      nspace+nres =>
	cache(min_array({<-.in_field.nspace,
			 (3 - <-.in_field.node_data[component].veclen)},0,0));
      scale = 1.0;
      offset = 0.0;
    };
    DVsurf_plot DVsurf_plot {
      in => in_field;
      &component => param.component;
      &scale => param.scale;
      &offset => param.offset;
      &nspace => param.nspace;
      DV_Param_surf_plot &param<NEportLevels={2,0}> => SurfPlotParam;
    };
    macro reset_minmax<NEx=378.,NEy=342.> {
      ilink in_field<export_all=1> => <-.DVsurf_plot.out;
      int comp<NEportLevels={0,1}>[] => init_array(in_field.nnode_data,0,
						   in_field.nnode_data);
      DVreset_minmax DVreset_minmax {
	in => in_field;
	&comp => <-.comp;
      };
      DVcomb_mesh_and_data DVcomb_mesh_and_data {
	in_mesh => in_field;
	in_nd => DVreset_minmax.out;
      };
      olink out_fld<export_all=2,NEx=567.,NEy=387.> => DVcomb_mesh_and_data.out;
    };
    /*DVnode_data_labels DVnode_data_labels {
      in => <-.in_field;
      int+nres ncomp => in_field.nnode_data;
      };*/
    int visible<NEportLevels=1,NEx=44.,NEy=99.> = 1;
    olink out_obj;
    //int model_type<NEportLevels=1>;
    //int axis<NEportLevels=1>;
  };
  surf_plot_base surf_plot {
    DataObjectNoTexture obj {
      in => DVsurf_plot.out;
      Obj {
	xform_mode = "Parent";
      };
    };
    out_obj => obj.obj;
  };
  surf_plot_base surf_plot_rep {
    int n<NEportLevels=1,NEx=451.,NEy=176.>;
    Xform &Xform<NEx=627.,NEy=176.>;
    Mesh+Cell_Data+Node_Data Mesh<NEx=66.,NEy=319.,NEportLevels={0,1}>[.n] {
      nnodes => <-.DVsurf_plot.out.nnodes;
      nspace => <-.DVsurf_plot.out.nspace;
      &coordinates => <-.DVsurf_plot.out.coordinates;
      ncell_sets => <-.DVsurf_plot.out.ncell_sets;
      &cell_set => <-.DVsurf_plot.out.cell_set;
      xform {
	mat => <-.<-.DVsurf_plot.out.xform.mat;
	xlate => <-.<-.DVsurf_plot.out.xform.xlate
	  + <-.<-.Xform.xform[index_of(<-.<-.Mesh)].xlate;
	center => <-.<-.DVsurf_plot.out.xform.center;
      };
      nnode_data => <-.reset_minmax.out_fld.nnode_data;
      node_data => <-.reset_minmax.out_fld.node_data;
    };
    DataObjectNoTexture DataObjectNoTexture<NEx=165.,NEy=385.>[.n] {
      in => <-.Mesh[index_of(<-.DataObjectNoTexture)];
      MinMax {
	min_value+nres => <-.in.node_data[0].min;
	max_value+nres => <-.in.node_data[0].max;
      };
      &Props => <-.obj.Props;
      Obj {
	use_altobj => <-.<-.obj.Top.use_altobj;
	xform_mode = "Parent";
	name => name_of(<-.<-.<-);
      };
    };
    GroupObject obj<NEx=242.,NEy=440.> {
      child_objs => <-.DataObjectNoTexture.obj;
      Top {
	xform_mode = "Parent";
      };
    };
    out_obj => .obj.obj;
  };
};
