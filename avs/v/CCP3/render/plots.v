
flibrary Plots<NEeditable=1,
	       export_cxx=1,
	       build_dir="avs/src/express",
	       cxx_hdr_files="ag/ag_omx.hxx avs/src/express/misc.hxx avs/src/express/display.hxx avs/src/express/scenes.hxx avs/src/core/render/c2D_gen.hxx",
	       out_hdr_file="plots.hxx",
	       out_src_file="plots.cxx"> {
  Field_Rect dos_field {
    group myid[nnode_data] {
      int id;
    };
  };
  macro dos_data {
    dos_field field<NEx=297.,NEy=154.>;
    dos_field x<NEx=561.,NEy=154.>;
  };
  macro plot_params {
    int plot_select;
    string components[];
    float width;
    int plot_vis[];
  };
  macro plot_base {
    link in;
    Field_Rect+nres &field<NEx=286.> => in.field;
    Field_Rect+nres &x<NEx=484.> => in.x;
    int visible<NEportLevels=1,NEx=528.,NEy=22.> = 1;
    string title<NEportLevels=1,NEx=33.>;
    string colours<NEportLevels=1,NEx=209.>[] => {
      "red",
      "blue",
      "green",
      "purple",
      "yellow",
      "orange",
      "brown",
      "light blue",
      "light green",
      "red",
      "blue",
      "green",
      "orange",
      "yellow",
      "purple",
      "brown",
      "light blue",
      "light green",
      anticolour
    };
    string backcolour<NEportLevels=1,NEx=774.,NEy=72.> =>
      switch((((DLV2Dscene.View.View.back_col[0] > 0.5) ||
	       (DLV2Dscene.View.View.back_col[1] > 0.5) ||
	       (DLV2Dscene.View.View.back_col[2] > 0.5)) + 1),"black","white");
    string anticolour<NEportLevels=1,NEx=774.,NEy=72.> =>
      switch((((DLV2Dscene.View.View.back_col[0] > 0.5) ||
	       (DLV2Dscene.View.View.back_col[1] > 0.5) ||
	       (DLV2Dscene.View.View.back_col[2] > 0.5)) + 1),"white","black");
    string xlabel<NEportLevels=1,NEx=385.>;
    string ylabel<NEportLevels=1,NEx=550.>;
    plot_params parameters {
      components => field.node_data.labels;
      width = 0.1;
    };
    GMOD.copy_on_change copy_on_change<NEx=216.,NEy=144.> {
      input => <-.anticolour;
    };
    string+read+opt Font<NEx=783.,NEy=153.,NEportLevels={0,1}> =
      "roman-simplex";
    AGText AGText<NEx=77.,NEy=198.> {
      text => { <-.title };
      geometry = { 0.0, 6.0 };
      height = 8;
      color => <-.copy_on_change.output;
      font => <-.Font;
    };
    macro graph1 {
      macro graphs<NEx=308.,NEy=88.>[<-.field.nnode_data] {
	GMOD.copy_on_change copy_on_change<NEx=315.,NEy=54.> {
	  input => colours[<-.<-.<-.field.myid[index_of(<-.<-.graphs)].id];
	};
	AGGraph AGGraph<NEx=319.,NEy=121.,NEportLevels={0,2}> {
	  valuesX => <-.<-.<-.field.coordinates.values;
	  valuesY => <-.<-.<-.field.node_data[index_of(<-.<-.graphs)].values;
	  color => <-.copy_on_change.output;
	  legendText =>
	    <-.<-.<-.field.node_data[index_of(<-.<-.graphs)].labels;
	  visibility => <-.<-.<-.parameters.plot_vis[index_of(<-.<-.graphs)];
	  graphType = "curve";
	};
      };
      macro xlines<NEx=495.,NEy=88.>[<-.x.nnode_data] {
	GMOD.copy_on_change copy_on_change<NEx=315.,NEy=54.> {
	  input => colours[<-.<-.<-.x.myid[index_of(<-.<-.xlines)].id];
	};
	AGGraph AGGraph<NEx=319.,NEy=121.,NEportLevels={0,2}> {
	  valuesX => <-.<-.<-.x.node_data[index_of(<-.<-.xlines)].values;
	  valuesY => <-.<-.<-.x.coordinates.values;
	  color => <-.copy_on_change.output;
	  //color = "antibackground"; use myid = 18!
	  legendText = "";
	};
      };
      AGXAxis AGXAxis<NEx=77.,NEy=88.> {
	text => <-.<-.xlabel;
      };
      AGYAxis AGYAxis<NEx=77.,NEy=132.> {
	text => <-.<-.ylabel;
      };
      AGGraphWorld AGGraphWorld<NEx=297.,NEy=231.> {
	children => <-.AGCombineGraphWorldInput.output;
	lineStyle = "solid";
	lineWidth => <-.<-.parameters.width;
	niceLimits = 0;
	majorTickColor => <-.<-.anticolour;
	axleColor => <-.<-.anticolour;
	labelColor => <-.<-.anticolour;
	labelFont => <-.<-.Font;
	textColor => <-.<-.anticolour;
	textFont => <-.<-.Font;
	symbolFont => <-.<-.Font;
	// Remove this if problems with ROD plots
	clip = 1;
      };
      AGGraphViewportObj AGGraphViewportObj<NEx=297.,NEy=297.> {
	graphViewport {
	  geometry => { -5.0, -4.0, 5.0, 4.0 };
	  fillColor = "transparent";
	  children => {
	    <-.<-.AGGraphWorld,
	    <-.<-.<-.AGText
	  };
	};
      };
      AGCombineGraphWorldInput AGCombineGraphWorldInput<NEx=286.,NEy=176.> {
	input2 => {<-.AGYAxis,
		   <-.AGXAxis};
      };
    };
    mlink obj<NEportLevels={1,1},NEx=319.,NEy=396.> =>
      {graph1.AGGraphViewportObj.dataObject.obj};
    CCP3.Core_Modules.Render.attach_plane attach {
      trigger => <-.DLV2Dscene.View.ViewUI.ViewPanel.UI.UIshell.cancel;
      visible => <-.visible;
    };
    CCP3.Viewers.Scenes.DLV2Dscene DLV2Dscene<NEx=572.,NEy=495.> {
      Top {
	child_objs => <-.<-.obj;
      };
      View {
	ViewUI {
	  ViewPanel {
	    UI {
	      UIshell {
		visible => <-.<-.<-.<-.<-.<-.visible;
	      };
	    };
	  };
	};
      };
    };
  };
  plot_base plot_dos {
    graph1 {
      AGCombineGraphWorldInput {
	input1 =>
	  switch((array_size(<-.xlines) > 0) + 1,<-.graphs.AGGraph,
		 combine_array(<-.graphs.AGGraph,<-.xlines.AGGraph));
	//<-.AGXlabels));
      };
      AGGraphWorld {
	clip = 1;
      };
      AGGraphViewportObj {
	graphViewport {
	  children => {
	    <-.<-.AGGraphWorld,
	    <-.<-.<-.AGText,
	    <-.<-.AGGraphLegendObj.graphLegend
	  };
	  frameColor => <-.<-.<-.anticolour;
	};
      };
      AGGraphLegendObj AGGraphLegendObj<NEx=550.,NEy=231.> {
	graphLegend {
	  graphs => <-.<-.graphs.AGGraph;
	  fillColor = "transparent";
	  frameColor => <-.<-.<-.anticolour;
	  labelColor => <-.<-.<-.anticolour;
	  labelFont => <-.<-.<-.Font;
	  titleColor => <-.<-.<-.anticolour;
	  titleFont => <-.<-.<-.Font;
	};
      };
    };
  };
  plot_base plot_multi { 
    Field_Rect+nres &field2 => in.field2;
    int+nres select_grid[] => in.select_grid;
    int+nres select_index[] => in.select_index;	       
    graph1 {
      macro graphs<NEx=308.,NEy=88.>[<-.field.nnode_data+<-.field2.nnode_data] {
	int sel_grid => <-.<-.select_grid[index_of(graphs)] + 1;
	int sel_index => <-.<-.select_index[index_of(graphs)];
	GMOD.copy_on_change copy_on_change<NEx=315.,NEy=54.> {
	  input => colours[index_of(graphs)];
	};
	AGGraph AGGraph<NEx=319.,NEy=121.,NEportLevels={0,2}> {
	  valuesX => switch(<-.sel_grid, <-.<-.<-.field.coordinates.values, 
			    <-.<-.<-.field2.coordinates.values);
	  valuesY => switch(sel_grid,
			    <-.<-.<-.field.node_data[sel_index].values,
			    <-.<-.<-.field2.node_data[sel_index].values); 
	  color => <-.copy_on_change.output;
	  legendText => switch(sel_grid,
			       <-.<-.<-.field.node_data[sel_index].labels,
			       <-.<-.<-.field2.node_data[sel_index].labels);
	  visibility => switch(sel_grid,
			       <-.<-.<-.parameters.plot_vis[sel_index],
			       <-.<-.<-.parameters.plot_vis[sel_index]);
	  graphType = "curve";
	};
      };
    };
  };
  plot_multi plot_rod1d {  
    graph1 {
      AGGraphWorld {
	scaleTypeY = "log10";
      };
      AGCombineGraphWorldInput {
	input1 =>
	  switch((array_size(<-.xlines) > 0) + 1,<-.graphs.AGGraph,
		 combine_array(<-.graphs.AGGraph,<-.xlines.AGGraph));
	//<-.AGXlabels));
      };
      AGGraphViewportObj {
	graphViewport {
	  children => {
	    <-.<-.AGGraphWorld,
	    <-.<-.<-.AGText,
	    <-.<-.AGGraphLegendObj.graphLegend
	  };
	  frameColor => <-.<-.<-.anticolour;
	};
      };
      AGGraphLegendObj AGGraphLegendObj<NEx=550.,NEy=231.> {
	graphLegend {
	  graphs => <-.<-.graphs.AGGraph;
	  title = "";
	  geometry = {<-.<-.AGGraphViewportObj.graphViewport.limitsX[1]+0.5, 
		      <-.<-.AGGraphViewportObj.graphViewport.limitsY[1]-2.0};
	  fillColor = "transparent";
	  frameColor => <-.<-.<-.anticolour;
	  labelColor => <-.<-.<-.anticolour;
	  labelFont => <-.<-.<-.Font;
	  titleColor => <-.<-.<-.anticolour;
	  titleFont => <-.<-.<-.Font;
	};
      };
    };
  };
  plot_base plot_rod2d {
    graph1 {
      AGGraphWorld<NEx=300.,NEy=320.> {
	scaleTypeY = "linear";
	uniformScaling = 1;
      };
      AGGraphWorld AGGraphWorld2<NEx=297.,NEy=231.> {
	children => <-.AGCombineGraphWorldInput#1.output;
	lineStyle = "solid";
	//lineWidth => <-.<-.parameters.width;
	lineWidth = -0.3;
	niceLimits = 0;
	majorTickColor => <-.<-.anticolour;
	axleColor => <-.<-.anticolour;
	labelColor => <-.<-.anticolour;
	labelFont => <-.<-.Font;
	textColor => <-.<-.anticolour;
	textFont => <-.<-.Font;
	symbolFont => <-.<-.Font;
	uniformScaling = 1;
	limitsX => {<-.<-.field.coordinates.min, <-.<-.field.coordinates.max};
	limitsY => {<-.<-.field.node_data[0].min, <-.<-.field.node_data[0].max};
      };
      AGCombineGraphWorldInput<NEx=300.,NEy=200.> {
	input1 =>
	  switch((array_size(<-.xlines) > 0) + 1,<-.graphs.AGGraph,
		 combine_array(<-.graphs.AGGraph,<-.xlines.AGGraph));
	input2 => {<-.AGYAxis, <-.AGXAxis};
      };
      AGCombineGraphWorldInput AGCombineGraphWorldInput#1<NEx=300.,NEy=260.> {
	input1 =>
	  switch((array_size(<-.xlines) > 0) + 1,<-.graphs.AGGraph,
		 combine_array(<-.graphs.AGGraph,<-.xlines.AGGraph));
	input2 => <-.AGEllipse;
      };
      AGGraphViewportObj<NEx=300.,NEy=380.> {
	graphViewport {
	  children => {
	    <-.<-.AGGraphWorld,
	    <-.<-.AGGraphWorld2,
	    <-.<-.<-.AGText,
	    <-.<-.AGGraphLegendObj.graphLegend
	  };
	  frameColor => <-.<-.<-.anticolour;
	};
      };
      AGGraphLegendObj AGGraphLegendObj<NEx=550.,NEy=231.> {
	graphLegend {
	  visibility = 0;  
	  //	  title = "";
	  //geometry = {<-.<-.AGGraphViewportObj.graphViewport.limitsX[1]+0.5, 
	  //	      <-.<-.AGGraphViewportObj.graphViewport.limitsY[1]-2.0};
	  graphs => <-.<-.graphs.AGGraph;
	  fillColor = "transparent";
	  frameColor => <-.<-.<-.anticolour;
	  labelColor => <-.<-.<-.anticolour;
	  labelFont => <-.<-.<-.Font;
	  titleColor => <-.<-.<-.anticolour;
	  titleFont => <-.<-.<-.Font;
	};
      };
      AGEllipse AGEllipse<NEx=242.,NEy=253.>[<-.field.nnodes*(<-.field.nnode_data-1)] {
	int data_set => (index_of(<-.AGEllipse))/(<-.<-.field.nnodes);
	int data_item => index_of(<-.AGEllipse)%(<-.<-.field.nnodes);
	geometry => {<-.<-.field.coordinates.values[data_item][0]
		     + <-.<-.field.node_data[data_set+1].values[data_item][0],
		     <-.<-.field.node_data[0].values[data_item][0],
		     <-.<-.field.coordinates.values[data_item][0],
		     <-.<-.field.node_data[0].values[data_item][0]
		     + <-.<-.field.node_data[data_set+1].values[data_item][0],
		     <-.<-.field.coordinates.values[data_item][0],
		     <-.<-.field.node_data[0].values[data_item][0]};
	frameColor => <-.<-.colours[data_set];
	};
    };
  };
  dos_data band_data {
    Field_Rect y<NEx=792.,NEy=154.>;
    float xmin;
    float xmax;
  };
  dos_data multi_grid_data {
       dos_field field2<NEx=297.,NEy=154.>;
       int select_grid[];
       int select_index[];
  };
  plot_base plot_band_base {
    Field_Rect+nres &y<NEx=600.,NEy=-132.> => in.y;
    //float ymin<NEportLevels=1> => x.coordinates.min;
    float xtext_pos<NEportLevels=1>[] => x.node_data.min;
    string xlabels<NEportLevels=1>[] => x.node_data.labels;
    float+nres xmin<NEportLevels=1,NEx=414.,NEy=153.> => in.xmin;
    float+nres xmax<NEportLevels=1,NEx=612.,NEy=153.> => in.xmax;
    graph1 {
      macro ylines<NEx=649.,NEy=88.>[y.nnode_data] {
	GMOD.copy_on_change copy_on_change<NEx=315.,NEy=54.> {
	  input => <-.<-.<-.anticolour;
	};
	AGGraph AGGraph<NEx=319.,NEy=121.,NEportLevels={0,2}> {
	  valuesX => <-.<-.<-.y.coordinates.values;
	  valuesY => <-.<-.<-.y.node_data[index_of(<-.<-.ylines)].values;
	  color => <-.copy_on_change.output;
	  legendText = "";
	};
      };
      xlines {
	copy_on_change {
	  input =>
	    switch (((index_of(<-.<-.xlines) == <-.<-.<-.x.nnode_data - 1) &&
		     (<-.<-.<-.x.myid[<-.<-.<-.x.nnode_data - 1].id == 18))+ 1,
		    colours[<-.<-.<-.x.myid[index_of(<-.<-.xlines)].id],
		    <-.<-.<-.backcolour);
	};
      };
      float yshift => (AGGraphViewportObj.graphViewport.limitsY[0] - 0.3);
      float+nres xscale => (AGGraphViewportObj.graphViewport.geometry[2]
			    - AGGraphViewportObj.graphViewport.geometry[0]) /
	(<-.xmax - <-.xmin);
      AGText AGXlabels[x.nnode_data] {
	text => { <-.<-.xlabels[index_of(<-.AGXlabels)] };
	geometry => { ((<-.<-.xtext_pos[index_of(<-.AGXlabels)] -
			<-.<-.xmin) * <-.xscale)
		      + AGGraphViewportObj.graphViewport.geometry[0],
		      <-.yshift };
	height = 4;
	color => <-.<-.anticolour;
	font => <-.<-.Font;
      };
      AGXAxis {
	visibility = 0;
	//positionY => <-.<-.ymin;
      };
      AGGraphWorld {
	clip = 1;
      };
      mlink base_ag_objs => { AGGraphWorld, <-.AGText };
      AGGraphViewportObj {
	graphViewport {
	  frameColor => <-.<-.<-.anticolour;
	};
      };
    };
  };
  plot_band_base plot_band {
    //float xmax<NEportLevels=1> => y.coordinates.max;
    float xpos<NEportLevels=1> => (xmax + 0.01);
    float ytext_pos<NEportLevels=1>[] => y.node_data.min;
    string ylabels<NEportLevels=1>[] => y.node_data.labels;
    graph1 {
      // Todo - decouple colours?
      AGText AGYlabels[y.nnode_data] {
	text => { <-.<-.ylabels[index_of(<-.AGYlabels)] };
	geometry => { <-.<-.xpos, <-.<-.ytext_pos[index_of(<-.AGYlabels)] };
	height = 2;
	color => <-.<-.anticolour;
	font => <-.<-.Font;
      };
      AGCombineGraphWorldInput {
	input1 =>
	  switch((array_size(<-.xlines) > 0) + 1,
		 switch((array_size(<-.ylines) > 0) + 1,
			<-.graphs.AGGraph,
			combine_array(<-.graphs.AGGraph,<-.ylines.AGGraph,
				      <-.AGYlabels)),
		 switch((array_size(<-.ylines) > 0) + 1,
			combine_array(<-.graphs.AGGraph,<-.xlines.AGGraph),
			combine_array(<-.graphs.AGGraph,<-.xlines.AGGraph,
				      <-.ylines.AGGraph,<-.AGYlabels)));
      };
      AGGraphViewportObj {
	graphViewport {
	  children => switch((array_size(<-.<-.xlines) > 0) + 1,
			     <-.<-.base_ag_objs,
			     combine_array(<-.<-.base_ag_objs,
					   <-.<-.AGXlabels));
	};
      };
    };
  };
  plot_band plot_bspin {
    field => alpha.combine_mesh_data.out;
    Field_Rect+nres &field2<NEx=288.,NEy=99.> => beta.combine_mesh_data.out;
    macro alpha<NEx=216.,NEy=531.> {
      Field_Rect+nres &in<NEx=189.,NEy=72.,NEportLevels={2,1}> => <-.in.field;
      DVextract_mesh DVextract_mesh<NEx=333.,NEy=171.> {
	in => <-.in;
      };
      DVextract_comps DVextract_comps<NEx=540.,NEy=171.> {
	in => <-.in;
	comps => init_array((<-.in.nnode_data / 2),0,
			    ((<-.in.nnode_data / 2) - 1));
      };
      group combine_mesh_data<NEx=441.,NEy=297.> {
	Mesh &in_mesh<NEportLevels={2,0}> => <-.DVextract_mesh.out;
	Node_Data+nres &in_nd<NEportLevels={2,0}> => <-.DVextract_comps.out;
	int error => ((is_valid(in_mesh.nnodes) && is_valid(in_nd.nnodes)) &&
		      (in_mesh.nnodes != in_nd.nnodes));
	Mesh+Node_Data &out<NEportLevels={0,3}> =>
	  switch((!.error),merge(.in_nd,.in_mesh));
      };
    };
    macro beta<NEx=378.,NEy=531.> {
      Field_Rect+nres &in<NEx=189.,NEy=72.,NEportLevels={2,1}> => <-.in.field;
      DVextract_mesh DVextract_mesh<NEx=333.,NEy=171.> {
	in => <-.in;
      };
      DVextract_comps DVextract_comps<NEx=540.,NEy=171.> {
	in => <-.in;
	comps => init_array((<-.in.nnode_data / 2),(<-.in.nnode_data / 2),
			    (<-.in.nnode_data - 1));
      };
      group combine_mesh_data<NEx=441.,NEy=297.> {
	Mesh &in_mesh<NEportLevels={2,0}> => <-.DVextract_mesh.out;
	Node_Data+nres &in_nd<NEportLevels={2,0}> => <-.DVextract_comps.out;
	int error => ((is_valid(in_mesh.nnodes) && is_valid(in_nd.nnodes)) &&
		      (in_mesh.nnodes != in_nd.nnodes));
	Mesh+Node_Data &out<NEportLevels={0,3}> =>
	  switch((!.error),merge(.in_nd,.in_mesh));
      };
    };
    AGText {
      geometry => { 0.0, 6.0 };
    };
    graph1 {
      AGGraphViewportObj {
	graphViewport {
	  geometry => { -6.0, -4.0, -0.2, 4.0 };
	  limitsX => { -6.0, -0.2 };
	  limitsY => { -5.0, 5.0 };
	};
      };
    };
    CCP3.Renderers.Plots.plot_band_base.graph1 graph2<NEx=756.,NEy=252.> {
      graphs {
	AGGraph {
	  valuesX => <-.<-.<-.field2.coordinates.values;
	  valuesY => <-.<-.<-.field2.node_data[index_of(<-.<-.graphs)].values;
	  legendText =>
	    <-.<-.<-.field2.node_data[index_of(<-.<-.graphs)].labels;
	  /*
	  visibility =>
	    <-.<-.<-.parameters.plot_vis[index_of(<-.<-.graphs)
					 + <-.<-.<-.field.nnode_data];
	  */
	};
      };
      AGText AGYlabels[y.nnode_data] {
	text => { <-.<-.ylabels[index_of(<-.AGYlabels)] };
	geometry => { <-.<-.xpos, <-.<-.ytext_pos[index_of(<-.AGYlabels)] };
	height = 2;
	color => <-.<-.anticolour;
	font => <-.<-.Font;
      };
      AGCombineGraphWorldInput {
	input2 => {<-.AGXAxis};
      };
      AGCombineGraphWorldInput {
	input1 =>
	  switch((array_size(<-.xlines) > 0) + 1,
		 switch((array_size(<-.ylines) > 0) + 1,
			<-.graphs.AGGraph,
			combine_array(<-.graphs.AGGraph,<-.ylines.AGGraph,
				      <-.AGYlabels)),
		 switch((array_size(<-.ylines) > 0) + 1,
			combine_array(<-.graphs.AGGraph,<-.xlines.AGGraph),
			combine_array(<-.graphs.AGGraph,<-.xlines.AGGraph,
				      <-.ylines.AGGraph,<-.AGYlabels)));
      };
      AGGraphWorld {
	limitsY+nres => <-.<-.graph1.AGGraphWorld.limitsY;
      };
      mlink base_ag_objs => { AGGraphWorld };
      AGGraphViewportObj {
	graphViewport {
	  children => switch((array_size(<-.<-.xlines) > 0) + 1,
			     <-.<-.base_ag_objs,
			     combine_array(<-.<-.base_ag_objs,
					   <-.<-.AGXlabels));
	};
	graphViewport {
	  geometry => { 0.2, -4.0, 6.0, 4.0 };
	  limitsX => { 0.2, 6.0 };
	  limitsY => { -5.0, 5.0 };
	};
      };
    };
    obj => {
      graph1.AGGraphViewportObj.dataObject.obj,
      graph2.AGGraphViewportObj.dataObject.obj
    };
  };
  group leed_data {
    float base_points[][2];
    float domain_points[][4];
    int cindex[];
    int ndomains;
  };
  group leed_params {
    int domains;
  };
  macro LEED_pattern {
    int visible<NEportLevels=1,NEx=528.,NEy=22.> = 1;
    leed_params params<NEx=165.,NEy=55.,NEportLevels={2,1}> {
      domains = 0;
    };
    leed_data &data;
    float points<NEportLevels=1,NEx=451.,NEy=176.>[n][4] => data.domain_points;
    int n<NEportLevels=1,NEx=517.,NEy=99.> => data.ndomains;
    string colours[] = {
      "blue", "red", "green", "orange", "purple", "yellow"
    };
    AGPoints AGPoints<NEx=242.,NEy=253.> {
      geometry => <-.data.base_points;
      color = <-.colours[0];
      priority = 2;
    };
    AGCombineWorldInput AGCombineWorldInput<NEx=275.,NEy=341.> {
      input1 => <-.domains.AGEllipse;
      input2 => {<-.AGPoints};
    };
    AGViewportObj AGViewportObj<NEx=275.,NEy=429.> {
      dataObject {
	obj<NEportLevels={1,3}>;
      };
      viewport {
	children => <-.<-.AGCombineWorldInput.output;
	//children => <-.<-.AGPoints;
	frameColor = "black";
      };
    };
    macro domains<NEx=495.,NEy=253.>[n] {
      AGEllipse AGEllipse<NEx=209.,NEy=165.,NEportLevels={0,2}> {
	geometry => <-.<-.points[index_of(<-.<-.domains)];
	frameColor => switch (<-.<-.params.domains+1, <-.<-.colours[0],
		 <-.<-.colours[<-.<-.data.cindex[index_of(<-.<-.domains)]]);
	priority = 1;
      };
    };
    CCP3.Viewers.Scenes.DLV2Dscene DLV2Dscene {
      Top {
	child_objs => {<-.<-.AGViewportObj.dataObject.obj};
      };
      View {
	View {
	  back_col = { 1.0, 1.0, 1.0 };
	};
	ViewUI {
	  ViewPanel {
	    UI {
	      UIshell {
		visible => <-.<-.<-.<-.<-.<-.visible;
	      };
	    };
	  };
	};
      };
    };
    /*
    GMOD.instancer instancer {
      Value => <-.visible;
      Group => <-.DLV2Dscene;
    };
    */
  };
};
