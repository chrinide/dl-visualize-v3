
flibrary Points<NEeditable=1,
		export_cxx=1,
		build_dir="avs/src/express",
		libdeps="GD",
		need_objs="GroupObject",
		cxx_hdr_files="fld/Xfld.h avs/src/express/model.hxx avs/src/core/render/conn_gen.hxx avs/src/core/render/file_gen.hxx",
		out_hdr_file="points.hxx",
		out_src_file="points.cxx"> {
  group text_data {
    string data;
  };
  group text_params {
    string text;
  };
  group text_message {
    string name;
    int visible = 1;
    text_params params {
      text => <-.data.data;
    };
    text_data &data;
  };
  group real_data {
    double data;
  };
  group real_params {
    int precision;
    double value;
  };
  macro real_scalar {
    string name;
    int visible = 1;
    real_params params {
      precision = 15;
      value => <-.data.data;
    };
    real_data &data;
  };
  macro text_view {
    link data<NEportLevels={1,1},NEx=220.,NEy=121.>;
    int visible<NEportLevels=1,NEx=616.,NEy=121.> = 1;
    string name<NEportLevels=1,NEx=770.,NEy=33.>;
    macro viewer<NEx=506.,NEy=275.,NEportLevels={0,1}> {
      // Todo - can't get font info!
      CCP3.Core_Macros.UI.UIobjs.DLVlist DLVlist<NEx=627.,NEy=264.> {
	parent => <-.DLVshell.UIpanel;
        width => parent.clientWidth;
        height => parent.clientHeight;
        listIsEditable = 0;
        //strings => <-.read_file.text;
        fontAttributes {
          family = "courier";
        };
      };
      CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=286.,NEy=176.> {
	//title => (<-.filename + " viewer");
	UIshell {
	  visible => <-.<-.<-.visible;
	  width = 600;
	  height = 750;
	};
      };
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
      Group => <-.viewer;
    };
  };
  group buffer_view_data {
    string buffer[];
  };
  text_view buffer_view {
    //string+nres buffer[] => str_array(data.buffer, "\n");
    viewer {
      DLVlist {
        strings+nres => <-.<-.data.buffer;
      };
      DLVshell {
	title => (<-.<-.name + " viewer");
      };
    };
  };
  group file_view_data {
    string filename;
  };
  text_view file_viewer {
    viewer {
      string+nres filename<NEportLevels=1,NEx=583.,NEy=66.> =>
	<-.data.filename;
      CCP3.Core_Modules.Render.read_file read_file<NEx=583.,NEy=165.> {
	trigger => <-.<-.visible;
	filename => <-.filename;
      };
      DLVlist {
        strings => <-.read_file.text;
      };
      DLVshell {
	title => (<-.filename + " viewer");
      };
    };
  };
  group atom_bond_text_data {
    int natoms;
    string sym1;
    string sym2;
    string sym3;
    double x;
    double y;
    double z;
    double length;
    double x2;
    double y2;
    double z2;
    double angle;
    float rad1;
    float rad2;
    float rad3;
    string groupname;
    string formula;
  };
  group atom_bond_params {
    boolean in_viewer;
    string formula;
    int precision;
    string text;
    string panel_text;
    boolean has_offset;
    // for atom labels object
    float offset;
    string text_list[];
  };
  macro label {
    string text;
    float coord<NEportLevels=1>[1][3];
    float radius = 0.0;
    group point_mesh<NEx=209.,NEy=330.> {
      float &coord<NEportLevels={2,0}>[][] => <-.coord;
      int coord_dims[] => array_dims(.coord);
      int error =>
	((array_size(.coord) > 0) && (array_size(.coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "point_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!<-.error),<-.coord_dims[1]);
	nspace => switch((!<-.error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Point cell_set {
	  ncells => <-.nnodes;
	  node_connect_list =>
	    switch((!error),init_array(ncells,0,(ncells - 1)));
	};
      };
    };
    /*FLD_MAP.point_mesh point_mesh {
      coord => <-.coord;
      };*/
    group TextValues<export_all=2> {
      int align_horiz = 1;
      int align_vert = 2;
      int drop_shadow = 0;
      int background = 0;
      int bounds = 0;
      int underline = 0;
      int lead_line = 0;
      int radial = 0;
      int do_offset = 1;
      float offset[3] => {0.0, 0.0, (0.1 + <-.radius) };
      //float offset[3] = {0.0, 0.0, 1.0};
      int xform_mode;
      int color;
      string text_values[] => {text};
      int stroke = 0;
      group StrokeTextAttribs {
	int font_type;
	int style;
	int plane;
	int orient;
	int path;
	int space_mode;
	float spacing;
	float angle;
	float height;
	float expansion;
	float width;
      };
    };
    Grid+Xform &TextField => merge(.TextValues,point_mesh.out,,,);
    //Mesh+group &TextField => merge(TextValues, point_mesh.out);
    DataObjectNoTexture label {
      in => <-.TextField;
      Obj {
	xform_mode = "Parent";
      };
    };
    olink obj<NEportLevels={1,2}> => label.obj;
  };
  macro length {
    string text;
    float coord1<NEportLevels=1>[1][3];
    float coord2<NEportLevels=1>[1][3];
    float middle<NEportLevels=1>[1][3] => {
      (coord1[0][0] + coord2[0][0]) / 2.0,
      (coord1[0][1] + coord2[0][1]) / 2.0,
      (coord1[0][2] + coord2[0][2]) / 2.0
    };
    group point_mesh<NEx=209.,NEy=330.> {
      float &coord<NEportLevels={2,0}>[][] => <-.middle;
      int coord_dims[] => array_dims(.coord);
      int error =>
	((array_size(.coord) > 0) && (array_size(.coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "point_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!<-.error),<-.coord_dims[1]);
	nspace => switch((!<-.error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Point cell_set {
	  ncells => <-.nnodes;
	  node_connect_list =>
	    switch((!error),init_array(ncells,0,(ncells - 1)));
	};
      };
    };
    group TextValues<export_all=2> {
      int align_horiz = 0;
      int align_vert = 2;
      int drop_shadow = 0;
      int background = 0;
      int bounds = 0;
      int underline = 0;
      int lead_line = 0;
      int radial = 0;
      int do_offset = 1;
      float offset[3] => {0.0, 0.0, 0.2};
      int xform_mode;
      int color;
      string text_values[] => {text};
      int stroke = 0;
      group StrokeTextAttribs {
	int font_type;
	int style;
	int plane;
	int orient;
	int path;
	int space_mode;
	float spacing;
	float angle;
	float height;
	float expansion;
	float width;
      };
    };
    Grid+Xform &TextField => merge(.TextValues,point_mesh.out,,,);
    DataObjectNoTexture bond_length {
      in => <-.TextField;
      Obj {
	xform_mode = "Parent";
	name = "label";
      };
    };
    FLD_MAP.line_disjoint_mesh line_disjoint_mesh<NEx=330.,NEy=440.> {
      coord1 => <-.coord1;
      coord2 => <-.coord2;
      DataObject {
	Obj {
	  xform_mode = "Parent";
	  name = "line";
	};
      };
    };
    GroupObject GroupObject<NEx=440.,NEy=495.> {
      child_objs => {
	<-.bond_length.obj, <-.line_disjoint_mesh.obj
      };
      Top {
	xform_mode = "Parent";
	name = "bond_line";
      };
    };
    olink obj<NEportLevels={1,2}> => GroupObject.obj;
  };
  macro angle {
    string text;
    float coord<NEportLevels=1>[1][3];
    group point_mesh<NEx=209.,NEy=330.> {
      float &coord<NEportLevels={2,0}>[][] => <-.coord;
      int coord_dims[] => array_dims(.coord);
      int error =>
	((array_size(.coord) > 0) && (array_size(.coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "point_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!<-.error),<-.coord_dims[1]);
	nspace => switch((!<-.error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Point cell_set {
	  ncells => <-.nnodes;
	  node_connect_list =>
	    switch((!error),init_array(ncells,0,(ncells - 1)));
	};
      };
    };
    group TextValues<export_all=2> {
      int align_horiz = 0;
      int align_vert = 2;
      int drop_shadow = 0;
      int background = 0;
      int bounds = 0;
      int underline = 0;
      int lead_line = 0;
      int radial = 0;
      int do_offset = 1;
      float offset[3] = {0.0, 0.0, 0.2};
      int xform_mode;
      int color;
      string text_values[] => {text};
      int stroke = 0;
      group StrokeTextAttribs {
	int font_type;
	int style;
	int plane;
	int orient;
	int path;
	int space_mode;
	float spacing;
	float angle;
	float height;
	float expansion;
	float width;
      };
    };
    Grid+Xform &TextField => merge(.TextValues,point_mesh.out,,,);
    DataObjectNoTexture label {
      in => <-.TextField;
      Obj {
	xform_mode = "Parent";
	name = "angle label";
      };
    };
    olink obj<NEportLevels={1,2}> => label.obj;
  };
  macro viewer_bonds<NEx=517.,NEy=363.> {
    CCP3.Renderers.Points.atom_bond_text_data &data<NEportLevels={2,1},
      NEx=231.,NEy=77.>;
    label label1<NEx=143.,NEy=209.> {
      text => <-.data.sym1;
      coord => {<-.data.x,<-.data.y,<-.data.z};
      radius => <-.data.rad1;
    };
    GMOD.instancer instance_length<NEx=407.,NEy=132.> {
      Value => (<-.data.natoms > 1);
      Group => <-.length;
    };
    length length<NEx=407.,NEy=209.> {
      //text => <-.data.sym1;
      coord1 => {<-.data.x,<-.data.y,<-.data.z};
      coord2 => {<-.data.x2,<-.data.y2,<-.data.z2};
    };
    GMOD.instancer instance_angle<NEx=638.,NEy=132.> {
      Value => (<-.data.natoms > 2);
      Group => <-.angle;
    };
    angle angle<NEx=638.,NEy=209.> {
      coord => {<-.data.x2,<-.data.y2,<-.data.z2};
    };
    CCP3.Core_Modules.Render.connector label1_connect<NEx=143.,NEy=297.> {
      trigger => (<-.data.natoms > 0);
      from_obj = "<-.label1.obj";
      to_obj = "<-.GroupObject.child_objs";
      ref = 1;
    };
    CCP3.Core_Modules.Render.connector length_connect<NEx=407.,NEy=297.> {
      trigger => (<-.data.natoms > 1);
      from_obj = "<-.length.obj";
      to_obj = "<-.GroupObject.child_objs";
      ref = 1;
    };
    CCP3.Core_Modules.Render.connector angle_connect<NEx=638.,NEy=297.> {
      trigger => (<-.data.natoms > 2);
      from_obj = "<-.angle.obj";
      to_obj = "<-.GroupObject.child_objs";
      ref = 1;
    };
    GroupObject GroupObject<NEx=308.,NEy=561.> {
      //child_objs => mlink;
      Top {
	name => "Atom and Bond info";
	xform_mode = "Parent";
      };
      obj<NEportLevels={1,3}>;
    };
  };
  macro atom_bond_text {
    link data<NEportLevels={1,1},NEx=220.,NEy=121.>;
    int visible<NEportLevels=1,NEx=616.,NEy=121.> = 1;
    string name<NEportLevels=1,NEx=770.,NEy=33.>;
    string format<NEx=11.,NEy=176.> =>
      str_format("%1d.%1df", params.precision+5, params.precision);
    string fm1<NEx=176.,NEy=176.> =>
      "%s at %" + format + ", %" + format + ", %" + format;
    string fm2<NEx=341.,NEy=176.> => "%s to %s is %" + format + "A";
    string fm3<NEx=495.,NEy=176.> => "%s - %s - %s is %" + format + " degrees";
    string str1<NEx=176.,NEy=220.> =>
      str_format(fm1, data.sym1, data.x, data.y, data.z);
    string str2<NEx=341.,NEy=220.> =>
      str_format(fm2, data.sym1, data.sym2, data.length);
    string str3<NEx=495.,NEy=220.> =>
      str_format(fm3, data.sym1, data.sym2, data.sym3,data.angle);
    string gp<NEportLevels=1,NEx=171.,NEy=275.> =>
      switch ((strlen(data.groupname) > 0)+1, "",
	      ("\nSelecting: " + data.groupname));
    //string gp2<NEportLevels=1,NEx=342.,NEy=275.> =>
    //string gp3<NEportLevels=1,NEx=495.,NEy=275.> =>
    string info1<NEx=176.,NEy=324.> => switch ((data.natoms > 0)+1, "", str1);
    string info2<NEx=341.,NEy=324.> => switch ((data.natoms > 1)+1, "", str2);
    string info3<NEx=495.,NEy=324.> => switch ((data.natoms > 2)+1, "", str3);
    CCP3.Renderers.Points.atom_bond_params params<NEx=429.,NEy=121.> {
      in_viewer = 0;
      precision = 3;
      text => switch ((<-.data.natoms)+1, "", <-.info1 + gp,
		      <-.info1 + "\n" + <-.info2 + gp,
		      <-.info1 + "\n" + <-.info2 + "\n" + <-.info3 + gp);
      panel_text => switch ((<-.data.natoms)+1, "", <-.info1 + gp,
			    <-.info1 + "; " + <-.info2 + gp,
			    <-.info1 + "; " + <-.info2 + "; " + <-.info3 + gp);
      has_offset = 0;
      offset = 0.0;
      formula+nres => <-.data.formula;
    };
    GMOD.instancer instancer<NEx=748.,NEy=363.> {
      Value => <-.params.in_viewer;
      Group => (<-.viewer_bonds);
    };
    viewer_bonds viewer_bonds<NEx=517.,NEy=363.,instanced=0> {
      data => <-.data;
      length {
	text => str_format("%" + <-.<-.format + "A", <-.data.length);
      };
      angle {
	text => str_format("%" + <-.<-.format + " degrees", <-.data.angle);
      };
    };
    CCP3.Core_Modules.Render.connector connector<NEx=99.,NEy=396.> {
      trigger => <-.params.in_viewer;
      from_obj = "<-.viewer_bonds.GroupObject.obj";
      to_obj = "<-.out_obj";
      ref = 0;
    };
    olink out_obj<NEportLevels={1,2},NEx=242.,NEy=495.>;
    // => viewer_bonds.GroupObject.obj;
  };
  group point_data {
    float x;
    float y;
    float z;
  };
  group point_params {
    float red;
    float green;
    float blue;
  };
  macro point {
    int visible<NEportLevels=1,NEx=528.,NEy=88.> = 1;
    point_params params<NEx=385.,NEy=132.> {
      red = 0.0;
      green = 0.0;
      blue = 0.0;
    };
    string name;
    link data<NEportLevels=1,NEx=143.,NEy=132.>;
    float+nres points<NEportLevels=1,NEx=253.,NEy=209.>[1][3] => {
      data.x, data.y, data.z
    };
    FLD_MAP.point_mesh point_mesh {
      coord+nres => <-.points;
    };
    group TextValues<export_all=2> {
      int align_horiz = 1;
      int align_vert = 2;
      int drop_shadow = 0;
      int background = 0;
      int bounds = 0;
      int underline = 0;
      int lead_line = 0;
      int radial = 0;
      int do_offset = 1;
      float offset[3] = {0.0, 0.0, 0.0};
      int xform_mode;
      int color;
      string text_values[] => {name};
      int stroke = 0;
      group StrokeTextAttribs {
	int font_type;
	int style;
	int plane;
	int orient;
	int path;
	int space_mode;
	float spacing;
	float angle;
	float height;
	float expansion;
	float width;
      };
    };
    Mesh+group &TextField => merge(TextValues, point_mesh.out);
    DataObjectNoTexture labels {
      in => <-.TextField;
      Props {
	col => {
	  <-.<-.params.red, <-.<-.params.green, <-.<-.params.blue
	};
	inherit = 0;
      };
      Obj {
	xform_mode = "Parent";
	visible => <-.<-.visible;
	name => <-.<-.name;
      };
    };
    olink obj<NEportLevels={1,2}> => labels.obj;
  };
  group label_field {
    int n;
    int natoms;
    string names[n];
    group data[n] {
      string labels[<-.natoms];
    };
    float coords[natoms][3];
    int index;
  };
  macro glyph_base {
    string name;
    int visible<NEportLevels=1,NEx=572.,NEy=55.> = 1;
    int visible_text = 0;
    link out_obj<NEportLevels={1,2},NEx=209.,NEy=429.>;
  };
  atom_bond_params label_params {
    int component;
  };    
  glyph_base labels {
    label_field &glyph_field<NEx=176.,NEy=55.> {
      coords<NEportLevels={0,2}>;
    };
    group display_data[glyph_field.n] {
      string+nres data =>
	(<-.glyph_field.names[index_of(<-.display_data)] + " - " +
 <-.glyph_field.data[index_of(<-.display_data)].labels[<-.glyph_field.index]);
    };
    string display_text[] => display_data.data;
    label_params params {
      in_viewer = 0;
      precision = 3;
      has_offset = 1;
      text_list+nres => <-.display_text;
      offset = 0.3;
    };
    group TextValues<NEx=143.,NEy=165.,export_all=2> {
      int align_horiz = 1;
      int align_vert = 2;
      int drop_shadow = 0;
      int background = 0;
      int bounds = 0;
      int underline = 0;
      int lead_line = 0;
      int radial = 0;
      int do_offset = 1;
      float offset[3] => {0.,0.,<-.params.offset};
      int xform_mode;
      int color;
      string text_values[] =>
	<-.glyph_field.data[<-.params.component].labels;
      int stroke = 0;
      group StrokeTextAttribs {
	int font_type;
	int style;
	int plane;
	int orient;
	int path;
	int space_mode;
	float spacing;
	float angle;
	float height;
	float expansion;
	float width;
      };
    };
    group point_mesh<NEx=385.,NEy=132.> {
      float+nres &coord<NEportLevels={2,0}>[][] => <-.glyph_field.coords;
      int coord_dims[] => array_dims(.coord);
      int error =>
	((array_size(.coord) > 0) && (array_size(.coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "point_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!<-.error),<-.coord_dims[1]);
	nspace => switch((!<-.error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!<-.<-.error),<-.<-.coord);
	};
	ncell_sets = 1;
	Point cell_set {
	  ncells => <-.nnodes;
	  node_connect_list =>
	    switch((!<-.<-.error),init_array(.ncells,0,(.ncells - 1)));
	};
      };
    };
    Grid+Xform &TextField<NEx=341.,NEy=231.,NEportLevels={0,1}> =>
      merge(.TextValues,.point_mesh.out,);
    DataObjectNoTexture DataObjectNoTexture<NEx=198.,NEy=352.> {
      in => <-.TextField;
      Obj {
	xform_mode = "Parent";
	visible => <-.<-.visible;
	name => <-.<-.name;
      };
    };
    CCP3.Core_Modules.Render.connector connector<NEx=99.,NEy=396.> {
      trigger => <-.params.in_viewer;
      from_obj = "<-.DataObjectNoTexture.obj";
      to_obj = "<-.out_obj";
      ref = 0;
    };
  };
  macro trajectory {
    int visible = 1;
  };
  /*group vector_field {
    Mesh+Node_Data field;
  };
  */
  Mesh+Node_Data vector_field;
  glyph_base vector_base {
    vector_field &glyph_field<NEx=165.,NEy=99.,NEportLevels={2,1}>;
    float scale = 10.0;
    int component = 0;
    macro glyph<NEx=220.,NEy=275.> {
      string name; // Todo => <-.name;
      //ilink in_field<export_all=1> => <-.glyph_field.field;
      ilink in_field<export_all=1> => <-.glyph_field;
      ilink in_glyph<export_all=1>;
      DV_Param_glyph GlyphParam<export_all=2> {
	glyph_comp => <-.<-.component;
	map_comp => <-.<-.component;
	scale_comp => <-.<-.component;
	vector = 1;
	scale => <-.<-.scale;
	scale_x = 1;
	scale_y = 1;
	scale_z = 1;
	normalize = 0;
      };
      DVglyph DVglyph {
	in => in_field;
	glyph => in_glyph;
	vector => param.vector;
	glyph_comp => param.glyph_comp;
	map_comp => param.map_comp;
	scale_comp => param.scale_comp;
	scale => param.scale;
	scale_x => param.scale_x;
	scale_y => param.scale_y;
	scale_z => param.scale_z;
	normalize => param.normalize;
	DV_Param_glyph &param<NEportLevels={2,0}> => GlyphParam;
      };
      DataObjectNoTexture vectors {
	in => DVglyph.out;
	Modes {
	  mode = {0,2,4,0,0};
	};
	Obj {
	  xform_mode = "Parent";
	  visible => <-.<-.<-.visible;
	  name => <-.<-.name;
	};
      };
      olink out_obj<NEportLevels={1,2}> => vectors.obj;
    };
    out_obj => .glyph.out_obj;
  };
  vector_base vector_glyphs {
    //GEOMS.Arrow1 Arrow1<NEx=429.,NEy=143.>;
    int colour = 1;
    /*macro Arrow1<NEx=451.,NEy=154.> {
      Line set1 {
    	ncells = 3;
    	node_connect_list = {0,1,1,2,1,3};
      };
      Mesh glyph1 {
    	int nnodes = 4;
	int nspace = 3;
	coordinates {
	  float values[nvals][veclen] = {
	    0.,0.,0.,1.,0.,0.,0.7,0.15,0.,0.7,-0.15,0.
	  };
	};
	int ncell_sets = 1;
	cell_set => {set1};
      };
      olink out_fld => glyph1;
      };*/
    macro Arrow4<NEx=451.,NEy=154.> {
      float radius<export=2> = 0.15;
      int subdiv<export=2> = 16;
      float cone_height<export=2> = 0.3;
      float height<export=2> = 1.;
      macro cone {
	float ang[] => init_array((subdiv + 1),0,(2 * 3.141592653));
	float x[subdiv+1][1] => init_array((subdiv + 1),(height - cone_height),
				  (height - cone_height));
	float y[subdiv+1][1] => (radius * cos(ang));
	float z[subdiv+1][1] => (radius * sin(ang));
	float xyz_circle[][] => combine_array(x,y,z);
	float xyz_tip[] => {height,0,0};
	int conn0[subdiv+1][1] => init_array((subdiv + 1),0,0);
	int conn1[subdiv+1][1] => init_array((subdiv + 1),1,(subdiv + 1));
	int c21[subdiv][] => conn1[1:subdiv][];
	int c22[1][1] = {1};
	int conn2[subdiv+1][1] => concat_array(c21,c22);
	int nxyz<NEportLevels={0,2}> => (subdiv + 2);
	int ntri<NEportLevels={0,2}> => (subdiv + 1);
	float xyz<NEportLevels={0,2}>[][] => concat_array(xyz_tip,
							  xyz_circle);
	int connect<NEportLevels={0,2}>[] => combine_array(conn0,conn1,
							   conn2);
      };
      macro line {
	int nlines<NEportLevels={0,2}> = 2;
	int nxyz<NEportLevels={0,2}> = 2;
	float xyz[2][3] => {{0,0,0},
			    {height,0,0}};
	int connect<NEportLevels={0,2}>[] => {<-.cone.nxyz,
					      (<-.cone.nxyz + 1)};
      };
      Line line_set {
	ncells = 1;
	node_connect_list => <-.line.connect;
      };
      Tri tri_set {
	ncells => <-.cone.ntri;
	node_connect_list => <-.cone.connect;
      };
      Mesh arrow4 {
	int nnodes => (<-.line.nxyz + <-.cone.nxyz);
	int nspace = 3;
	coordinates {
	  values => concat_array(cone.xyz,line.xyz);
	};
	int ncell_sets = 2;
	cell_set => {line_set,tri_set};
      };
      olink out_fld => arrow4;
    };
    glyph {
      in_glyph => <-.Arrow4.out_fld;
      vectors {
	Modes {
	  outline = "Off";
	};
      };
    };
  };
  macro dlv_glyph {
    ilink in_field<export_all=1>;
    ilink in_glyph<export_all=1>;
    DV_Param_glyph GlyphParam<export_all=2> {
      glyph_comp = 0;
      map_comp = 0;
      scale_comp = 0;
      vector = 0;
      scale = 1.;
      scale_x = 1;
      scale_y = 1;
      scale_z = 1;
      normalize = 0;
    };
    DVglyph DVglyph {
      in => in_field;
      glyph => in_glyph;
      vector => param.vector;
      glyph_comp => param.glyph_comp;
      map_comp => param.map_comp;
      scale_comp => param.scale_comp;
      scale => param.scale;
      scale_x => param.scale_x;
      scale_y => param.scale_y;
      scale_z => param.scale_z;
      normalize => param.normalize;
      DV_Param_glyph &param<NEportLevels={2,0}> => GlyphParam;
    };
    macro obj<NEx=121.,NEy=143.,locked=0> {
      group &in<NEportLevels={2,1},NEnumColors=4,NEcolor0=255,NEy=132,NEx=55> {
	GDxform_templ &xform;
	method render;
      } => DVglyph.out;
      imlink child_objs<NEy=77,NEx=132>;
      DefaultLinear Datamap<NEy=231,NEx=11,export_all=1> {
	dataMin = -1.0;
	dataMax = 1.0;
      };
      DefaultProps Props<NEy=198,NEx=297,export_all=1>;
      DefaultModes Modes<NEy=165,NEx=341,export_all=1> {
	mode = {0,2,4,0,0};
	outline = "Off";
      };
      DefaultPickInfo PickInfo<NEy=99,NEx=473,export_all=1>;
      AltObject AltObject<export_all=1,instanced=0> {
	alt_in => in;
      };
      DefaultObject Obj<NEportLevels={0,1},NEy=385,NEx=110,export_all=2> {
	input => in;
	dmap => Datamap;
	xform => in.xform;
	xform_mode = "Parent";
	props => Props;
	modes => Modes;
	objects => child_objs;
	altobj => AltObject.obj;
	name => name_of(<-.<-.<-);
	pick_info => PickInfo;
      };
      GMOD.instancer instancer {
	Value => Obj.use_altobj;
	Group => AltObject;
      };
      olink obj<NEy=385,NEx=341> => Obj;
    };
    olink out_obj => obj.obj;
  };
  /*
  group orbital_field {
    Field orbital[9]; // s, px, py, pz, dz2r2, dxz, dyz, dx2y2, dxy
  };
  glyph_base orbital_glyphs {
    orbital_field &orbs<NEx=22.,NEy=77.,NEportLevels={2,1}>;
    float scale = 1.0;
    GMOD.instancer s_instancer<NEx=143.,NEy=154.> {
      Value => (<-.orbs.orbital[0].nnodes > 0);
      Group => <-.s_orb;
      active = 2;
    };
    macro s_orb<instanced=0> {
      dlv_glyph glyph<NEx=110.,NEy=220.> {
	GlyphParam {
	  map_comp = 1;
	  scale => <-.<-.<-.scale;
	};
	in_field => <-.<-.orbs.orbital[0];
	in_glyph => <-.Sphere.bounds.out;
      };
      macro Sphere<NEx=176.,NEy=143.> {
	int subdiv<export=2> = 8;
	Field_Spher_Unif sphere {
	  nspace = 3;
	  ndim = 3;
	  dims => {2,subdiv,subdiv};
	  points = {0.,0.,0.,0.7,
		    3.14159,6.28318};
	};
	DVbounds bounds {
	  in => <-.sphere;
	  hull = 0;
	  edges = 0;
	  faces = 1;
	  imin = 0;
	  imax = 1;
	  jmin = 0;
	  jmax = 0;
	  kmin = 0;
	  kmax = 0;
	  data = 0;
	  component = 0;
	  out<NEportLevels={0,3}>;
	  method+notify_val+notify_inst upd_bounds = "DVbounds_update";
	};
      };
    };
    GMOD.instancer px_instancer {
      Value => (<-.orbs.orbital[1].nnodes > 0);
      Group => <-.px_orb;
      active = 2;
    };
    macro px_orb<instanced=0> {
      // grids cut out of AVS with $get_array then attached to a tri_mesh.
      group pxp_mesh {
	float coord<NEportLevels={2,0}>[138][3] = {
   {0.705569,-0.176392,-0.385632},{0.705569,-0.260822,-0.352785},{0.598015,-0.176392,
-0.352785},{0.881962,-0.176392,-0.35331},{0.881962,-0.177641,-0.352785},{0.882951,-0.176392,
-0.352785},{0.529177,-5.96046e-08,-0.353635},{0.529177,-0.00776697,-0.352785},{0.528204,
-5.96046e-08,-0.352785},{0.705569,-5.96046e-08,-0.407148},{0.881962,-5.96046e-08,-0.379518},
{0.931839,-5.96046e-08,-0.352785},{0.529177,0.00776686,-0.352785},{0.705569,0.176392,
-0.385632},{0.598015,0.176392,-0.352785},{0.881962,0.176392,-0.35331},{0.882951,0.176392,
-0.352785},{0.705569,0.260822,-0.352785},{0.881962,0.177641,-0.352785},{0.529177,-0.352785,
-0.267029},{0.529177,-0.453912,-0.176392},{0.420868,-0.352785,-0.176392},{0.705569,
-0.352785,-0.308162},{0.705569,-0.487765,-0.176392},{0.881962,-0.352785,-0.253337},
{0.881962,-0.426455,-0.176392},{0.967189,-0.352785,-0.176392},{0.352785,-0.176392,-0.221411},
{0.352785,-0.269206,-0.176392},{0.317021,-0.176392,-0.176392},{0.529177,-0.176392,-0.337793},
{1.05835,-0.176392,-0.196359},{1.05835,-0.205155,-0.176392},{1.07301,-0.176392,-0.176392},
{0.352785,-5.96046e-08,-0.243605},{0.299466,-5.96046e-08,-0.176392},{1.05835,-5.96046e-08,
-0.238382},{1.10373,-5.96046e-08,-0.176392},{0.352785,0.176392,-0.221411},{0.317021,
0.176392,-0.176392},{0.529177,0.176392,-0.337793},{1.05835,0.176392,-0.196359},{1.07301,
0.176392,-0.176392},{0.352785,0.269206,-0.176392},{0.529177,0.352785,-0.267029},{0.420868,
0.352785,-0.176392},{0.705569,0.352785,-0.308162},{0.881962,0.352785,-0.253337},{0.967189,
0.352785,-0.176392},{1.05835,0.205155,-0.176392},{0.529177,0.453912,-0.176392},{0.705569,
0.487765,-0.176392},{0.881962,0.426455,-0.176392},{0.705569,-0.529177,-0.0280969},{0.705569,
-0.537095,-5.96046e-08},{0.664337,-0.529177,-5.96046e-08},{0.733066,-0.529177,-5.96046e-08},
{0.352785,-0.352785,-0.0634506},{0.352785,-0.385299,-5.96046e-08},{0.331881,-0.352785,
-5.96046e-08},{0.529177,-0.510515,-5.96046e-08},{0.881962,-0.480252,-5.96046e-08},{1.02431,
-0.352785,-5.96046e-08},{0.235788,-0.176392,-5.96046e-08},{1.05835,-0.294584,-5.96046e-08},
{1.11786,-0.176392,-5.96046e-08},{0.192139,-5.96046e-08,-5.96046e-08},{1.14508,-5.96046e-08,
-5.96046e-08},{0.235788,0.176392,-5.96046e-08},{1.11786,0.176392,-5.96046e-08},{0.331881,
0.352785,-5.96046e-08},{0.352785,0.352785,-0.0634506},{1.02431,0.352785,-5.96046e-08},
{1.05835,0.294584,-5.96046e-08},{0.352785,0.385299,-5.96046e-08},{0.529177,0.510515,
-5.96046e-08},{0.664337,0.529177,-5.96046e-08},{0.705569,0.529177,-0.0280969},{0.733066,
0.529177,-5.96046e-08},{0.881962,0.480252,-5.96046e-08},{0.705569,0.537095,-5.96046e-08},
{0.705569,-0.529177,0.0280968},{0.352785,-0.352785,0.0634504},{0.529177,-0.453912,0.176392},
{0.420868,-0.352785,0.176392},{0.705569,-0.487765,0.176392},{0.881962,-0.426455,0.176392},
{0.967189,-0.352785,0.176392},{0.352785,-0.269206,0.176392},{0.317021,-0.176392,0.176392},
{1.05835,-0.205155,0.176392},{1.07301,-0.176392,0.176392},{0.299466,-5.96046e-08,0.176392},
{1.10373,-5.96046e-08,0.176392},{0.317021,0.176392,0.176392},{1.07301,0.176392,0.176392},
{0.352785,0.269206,0.176392},{0.352785,0.352785,0.0634504},{0.420868,0.352785,0.176392},
{0.967189,0.352785,0.176392},{1.05835,0.205155,0.176392},{0.529177,0.453912,0.176392},
{0.705569,0.487765,0.176392},{0.705569,0.529177,0.0280968},{0.881962,0.426455,0.176392},
{0.529177,-0.352785,0.267029},{0.705569,-0.352785,0.308162},{0.881962,-0.352785,0.253337},
{0.352785,-0.176392,0.221411},{0.529177,-0.176392,0.337793},{0.705569,-0.260822,0.352785},
{0.598015,-0.176392,0.352785},{0.881962,-0.177641,0.352785},{1.05835,-0.176392,0.196359},
{0.882951,-0.176392,0.352785},{0.352785,-5.96046e-08,0.243605},{0.529177,-0.00776697,
0.352785},{0.528204,-5.96046e-08,0.352785},{1.05835,-5.96046e-08,0.238382},{0.931839,
-5.96046e-08,0.352785},{0.352785,0.176392,0.221411},{0.529177,0.00776686,0.352785},
{0.529177,0.176392,0.337793},{0.598015,0.176392,0.352785},{1.05835,0.176392,0.196359},
{0.882951,0.176392,0.352785},{0.529177,0.352785,0.267029},{0.705569,0.260822,0.352785},
{0.705569,0.352785,0.308162},{0.881962,0.177641,0.352785},{0.881962,0.352785,0.253337},
{0.705569,-0.176392,0.385632},{0.881962,-0.176392,0.35331},{0.529177,-5.96046e-08,0.353635},
{0.705569,-5.96046e-08,0.407147},{0.881962,-5.96046e-08,0.379518},{0.705569,0.176392,
0.385632},{0.881962,0.176392,0.35331}
};
	int connect<NEportLevels={2,0}>[816] = {
   0,1,2,3,4,1,3,1,0,5,4,3,6,7,8,9,0,2,9,2,7,9,7,6,10,3,0,10,0,9,11,5,
3,11,3,10,12,6,8,13,9,6,13,6,12,13,12,14,15,10,9,15,9,13,16,11,10,16,10,
15,17,13,14,18,15,13,18,13,17,18,16,15,19,20,21,22,23,20,22,20,19,24,25,
23,24,23,22,26,25,24,27,28,29,30,19,21,30,21,28,30,28,27,2,1,22,2,22,19,
2,19,30,1,4,24,1,24,22,4,5,31,4,31,32,4,32,24,32,26,24,33,32,31,34,27,29,
34,29,35,8,7,30,8,30,27,8,27,34,7,2,30,5,11,36,5,36,31,37,33,31,37,31,36,
38,34,35,38,35,39,12,8,34,12,34,38,12,38,40,14,12,40,11,16,41,11,41,36,
42,37,36,42,36,41,43,38,39,44,40,38,44,38,43,44,43,45,17,14,40,17,40,44,
17,44,46,18,17,46,18,46,47,16,18,47,16,47,48,16,48,41,48,49,41,49,42,41,
50,44,45,51,46,44,51,44,50,52,47,46,52,46,51,52,48,47,53,54,55,56,54,53,
57,58,59,21,20,60,21,60,58,21,58,57,20,23,53,20,53,55,20,55,60,23,25,61,
23,61,56,23,56,53,25,26,62,25,62,61,29,28,57,29,57,59,29,59,63,28,21,57,
26,32,64,26,64,62,32,33,65,32,65,64,35,29,63,35,63,66,33,37,67,33,67,65,
39,35,66,39,66,68,37,42,69,37,69,67,43,39,68,43,68,70,43,70,71,45,43,71,
49,48,72,49,72,73,42,49,73,42,73,69,74,71,70,50,45,71,50,71,74,50,74,75,
51,50,75,51,75,76,51,76,77,52,51,77,52,77,78,52,78,79,48,52,79,48,79,72,
80,77,76,80,78,77,55,54,81,54,56,81,59,58,82,58,60,83,58,83,84,58,84,82,
60,55,81,60,81,85,60,85,83,56,61,86,56,86,85,56,85,81,61,62,87,61,87,86,
63,59,82,63,82,88,63,88,89,84,88,82,62,64,90,62,90,87,64,65,91,64,91,90,
66,63,89,66,89,92,65,67,93,65,93,91,68,66,92,68,92,94,67,69,95,67,95,93,
70,68,94,70,94,96,70,96,97,96,98,97,73,72,99,73,99,100,69,73,100,69,100,
95,74,70,97,75,74,97,75,97,98,75,98,101,76,75,101,76,101,102,76,102,103,
79,78,103,79,103,102,79,102,104,72,79,104,72,104,99,80,76,103,78,80,103,
84,83,105,83,85,106,83,106,105,85,86,107,85,107,106,86,87,107,89,88,108,
88,84,105,88,105,109,88,109,108,106,110,111,106,111,109,106,109,105,107,
112,110,107,110,106,87,90,113,87,113,114,87,114,107,114,112,107,90,91,113,
92,89,108,92,108,115,109,116,117,109,117,115,109,115,108,111,116,109,113,
118,119,113,119,114,91,93,118,91,118,113,94,92,115,94,115,120,121,122,120,
121,120,115,121,115,117,121,123,122,118,124,125,118,125,119,93,95,124,93,
124,118,96,94,120,98,96,120,98,120,122,98,122,126,127,128,126,127,126,122,
127,122,123,129,130,128,129,128,127,100,99,130,100,130,129,100,129,124,129,
125,124,95,100,124,101,98,126,102,101,126,102,126,128,104,102,128,104,128,
130,99,104,130,111,110,131,110,112,132,110,132,131,112,114,132,117,116,133,
116,111,131,116,131,134,116,134,133,132,135,134,132,134,131,114,119,135,
114,135,132,121,117,133,123,121,133,123,133,134,123,134,136,135,137,136,
135,136,134,119,125,137,119,137,135,127,123,136,129,127,136,129,136,137,
125,129,137
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	};
      };
      group pxn_mesh {
	float coord<NEportLevels={2,0}>[138][3] = {
   {-0.881962,-0.176392,-0.35331},{-0.881962,-0.177641,-0.352785},{-0.882951,-0.176392,
-0.352785},{-0.705569,-0.176392,-0.385632},{-0.705569,-0.260822,-0.352785},{-0.598015,
-0.176392,-0.352785},{-0.881962,-5.96046e-08,-0.379518},{-0.931839,-5.96046e-08,-0.352785},
{-0.705569,-5.96046e-08,-0.407148},{-0.529177,-5.96046e-08,-0.353635},{-0.529177,-0.00776697,
-0.352785},{-0.528204,-5.96046e-08,-0.352785},{-0.881962,0.176392,-0.35331},{-0.882951,
0.176392,-0.352785},{-0.705569,0.176392,-0.385632},{-0.529177,0.00776686,-0.352785},
{-0.598015,0.176392,-0.352785},{-0.881962,0.177641,-0.352785},{-0.705569,0.260822,
-0.352785},{-0.881962,-0.352785,-0.253337},{-0.881962,-0.426455,-0.176392},{-0.967189,
-0.352785,-0.176392},{-0.705569,-0.352785,-0.308162},{-0.705569,-0.487765,-0.176392},
{-0.529177,-0.352785,-0.267029},{-0.529177,-0.453912,-0.176392},{-0.420868,-0.352785,
-0.176392},{-1.05835,-0.176392,-0.196359},{-1.05835,-0.205155,-0.176392},{-1.07301,
-0.176392,-0.176392},{-0.529177,-0.176392,-0.337793},{-0.352785,-0.176392,-0.221411},
{-0.352785,-0.269206,-0.176392},{-0.317021,-0.176392,-0.176392},{-1.05835,-5.96046e-08,
-0.238382},{-1.10373,-5.96046e-08,-0.176392},{-0.352785,-5.96046e-08,-0.243605},{-0.299466,
-5.96046e-08,-0.176392},{-1.05835,0.176392,-0.196359},{-1.07301,0.176392,-0.176392},
{-0.529177,0.176392,-0.337793},{-0.352785,0.176392,-0.221411},{-0.317021,0.176392,
-0.176392},{-1.05835,0.205155,-0.176392},{-0.967189,0.352785,-0.176392},{-0.881962,0.352785,
-0.253337},{-0.705569,0.352785,-0.308162},{-0.529177,0.352785,-0.267029},{-0.352785,
0.269206,-0.176392},{-0.420868,0.352785,-0.176392},{-0.881962,0.426455,-0.176392},{-0.705569,
0.487765,-0.176392},{-0.529177,0.453912,-0.176392},{-0.705569,-0.529177,-0.0280969},
{-0.705569,-0.537095,-5.96046e-08},{-0.733066,-0.529177,-5.96046e-08},{-0.664337,-0.529177,
-5.96046e-08},{-0.881962,-0.480252,-5.96046e-08},{-1.02431,-0.352785,-5.96046e-08},{-0.529177,
-0.510515,-5.96046e-08},{-0.352785,-0.352785,-0.0634506},{-0.352785,-0.385299,-5.96046e-08},
{-0.331881,-0.352785,-5.96046e-08},{-1.05835,-0.294584,-5.96046e-08},{-1.11786,-0.176392,
-5.96046e-08},{-0.235788,-0.176392,-5.96046e-08},{-1.14508,-5.96046e-08,-5.96046e-08},
{-0.192139,-5.96046e-08,-5.96046e-08},{-1.11786,0.176392,-5.96046e-08},{-0.235788,0.176392,
-5.96046e-08},{-1.05835,0.294584,-5.96046e-08},{-1.02431,0.352785,-5.96046e-08},{-0.352785,
0.352785,-0.0634506},{-0.331881,0.352785,-5.96046e-08},{-0.881962,0.480252,-5.96046e-08},
{-0.733066,0.529177,-5.96046e-08},{-0.705569,0.529177,-0.0280969},{-0.664337,0.529177,
-5.96046e-08},{-0.529177,0.510515,-5.96046e-08},{-0.352785,0.385299,-5.96046e-08},{-0.705569,
0.537095,-5.96046e-08},{-0.705569,-0.529177,0.0280968},{-0.881962,-0.426455,0.176392},
{-0.967189,-0.352785,0.176392},{-0.705569,-0.487765,0.176392},{-0.529177,-0.453912,
0.176392},{-0.352785,-0.352785,0.0634504},{-0.420868,-0.352785,0.176392},{-1.05835,
-0.205155,0.176392},{-1.07301,-0.176392,0.176392},{-0.352785,-0.269206,0.176392},{-0.317021,
-0.176392,0.176392},{-1.10373,-5.96046e-08,0.176392},{-0.299466,-5.96046e-08,0.176392},
{-1.07301,0.176392,0.176392},{-0.317021,0.176392,0.176392},{-1.05835,0.205155,0.176392},
{-0.967189,0.352785,0.176392},{-0.352785,0.269206,0.176392},{-0.352785,0.352785,0.0634504},
{-0.420868,0.352785,0.176392},{-0.881962,0.426455,0.176392},{-0.705569,0.487765,0.176392},
{-0.705569,0.529177,0.0280968},{-0.529177,0.453912,0.176392},{-0.881962,-0.352785,0.253337},
{-0.705569,-0.352785,0.308162},{-0.529177,-0.352785,0.267029},{-1.05835,-0.176392,0.196359},
{-0.881962,-0.177641,0.352785},{-0.882951,-0.176392,0.352785},{-0.705569,-0.260822,
0.352785},{-0.529177,-0.176392,0.337793},{-0.598015,-0.176392,0.352785},{-0.352785,-0.176392,
0.221411},{-1.05835,-5.96046e-08,0.238382},{-0.931839,-5.96046e-08,0.352785},{-0.529177,
-0.00776697,0.352785},{-0.352785,-5.96046e-08,0.243605},{-0.528204,-5.96046e-08,0.352785},
{-1.05835,0.176392,0.196359},{-0.882951,0.176392,0.352785},{-0.529177,0.00776686,0.352785},
{-0.529177,0.176392,0.337793},{-0.598015,0.176392,0.352785},{-0.352785,0.176392,0.221411},
{-0.881962,0.177641,0.352785},{-0.881962,0.352785,0.253337},{-0.705569,0.260822,0.352785},
{-0.705569,0.352785,0.308162},{-0.529177,0.352785,0.267029},{-0.881962,-0.176392,0.35331},
{-0.705569,-0.176392,0.385632},{-0.881962,-5.96046e-08,0.379518},{-0.705569,-5.96046e-08,
0.407147},{-0.529177,-5.96046e-08,0.353635},{-0.881962,0.176392,0.35331},{-0.705569,
0.176392,0.385632}
};
	int connect<NEportLevels={2,0}>[816] = {
   0,1,2,3,4,1,3,1,0,5,4,3,6,0,2,6,2,7,8,3,0,8,0,6,9,10,5,9,5,3,9,3,8,
11,10,9,12,6,7,12,7,13,14,8,6,14,6,12,15,9,8,15,8,14,15,14,16,15,11,9,
17,12,13,18,14,12,18,12,17,18,16,14,19,20,21,22,23,20,22,20,19,24,25,23,
24,23,22,26,25,24,27,28,29,2,1,19,2,19,21,2,21,27,21,28,27,1,4,22,1,22,
19,4,5,30,4,30,24,4,24,22,31,32,26,31,26,24,31,24,30,33,32,31,34,27,29,
34,29,35,7,2,27,7,27,34,5,10,30,10,11,36,10,36,31,10,31,30,37,33,31,37,
31,36,38,34,35,38,35,39,13,7,34,13,34,38,15,16,40,11,15,40,11,40,41,11,
41,36,42,37,36,42,36,41,43,38,39,17,13,38,17,38,43,17,43,45,43,44,45,18,
17,45,18,45,46,16,18,46,16,46,47,16,47,40,48,41,40,48,40,47,48,47,49,48,
42,41,50,45,44,51,46,45,51,45,50,52,47,46,52,46,51,52,49,47,53,54,55,56,
54,53,21,20,57,21,57,58,20,23,53,20,53,55,20,55,57,23,25,59,23,59,56,23,
56,53,25,26,60,25,60,61,25,61,59,62,61,60,29,28,63,29,63,64,28,21,58,28,
58,63,26,32,60,32,33,65,32,65,62,32,62,60,35,29,64,35,64,66,33,37,67,33,
67,65,39,35,66,39,66,68,37,42,69,37,69,67,43,39,68,43,68,70,44,43,70,44,
70,71,48,49,72,42,48,72,42,72,73,42,73,69,50,44,71,50,71,74,51,50,74,51,
74,75,51,75,76,52,51,76,52,76,77,52,77,78,49,52,78,49,78,79,49,79,72,79,
73,72,80,76,75,80,77,76,55,54,81,54,56,81,58,57,82,58,82,83,57,55,81,57,
81,84,57,84,82,56,59,85,56,85,84,56,84,81,59,61,86,59,86,87,59,87,85,61,
62,86,64,63,88,64,88,89,63,58,83,63,83,88,86,90,87,62,65,91,62,91,90,62,
90,86,66,64,89,66,89,92,65,67,93,65,93,91,68,66,92,68,92,94,67,69,95,67,
95,93,70,68,94,70,94,96,71,70,96,71,96,97,98,99,100,69,73,99,69,99,98,69,
98,95,74,71,97,74,97,101,75,74,101,75,101,102,75,102,103,78,77,103,78,103,
102,78,102,104,79,78,104,79,104,100,79,100,99,73,79,99,80,75,103,77,80,
103,83,82,105,82,84,106,82,106,105,84,85,107,84,107,106,85,87,107,89,88,
108,88,83,105,88,105,109,88,109,108,109,110,108,106,111,109,106,109,105,
107,112,113,107,113,111,107,111,106,87,90,114,87,114,112,87,112,107,90,
91,114,92,89,108,92,108,115,110,116,115,110,115,108,112,117,113,114,118,
119,114,119,117,114,117,112,91,93,118,91,118,114,94,92,115,94,115,120,116,
121,120,116,120,115,122,123,124,118,125,123,118,123,122,118,122,119,93,
95,125,93,125,118,96,94,120,97,96,120,97,120,121,97,121,127,121,126,127,
128,129,127,128,127,126,123,130,129,123,129,128,123,128,124,98,100,130,98,
130,123,98,123,125,95,98,125,101,97,127,102,101,127,102,127,129,104,102,
129,104,129,130,100,104,130,110,109,131,109,111,132,109,132,131,111,113,
132,116,110,131,116,131,133,132,134,133,132,133,131,113,117,135,113,135,
134,113,134,132,117,119,135,121,116,133,121,133,136,134,137,136,134,136,
133,122,124,137,122,137,134,122,134,135,119,122,135,126,121,136,128,126,
136,128,136,137,124,128,137
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	};
      };
      dlv_glyph pglyph {
	in_field => <-.<-.orbs.orbital[1];
	in_glyph => <-.pxp_mesh.out;
	GlyphParam {
	  map_comp = 1;
	  scale => <-.<-.<-.scale;
	};
      };
      dlv_glyph nglyph {
	in_field => <-.<-.orbs.orbital[1];
	in_glyph => pxn_mesh.out;
	GlyphParam {
	  map_comp = 2;
	  scale => <-.<-.<-.scale;
	};
      };
    };
    GMOD.instancer py_instancer {
      Value => (<-.orbs.orbital[2].nnodes > 0);
      Group => <-.py_orb;
      active = 2;
    };
    macro py_orb<instanced=0> {
      // grids cut out of AVS with $get_array then attached to a tri_mesh.
      group pyp_mesh {
	float coord<NEportLevels={2,0}>[138][3] = {
   {-5.96046e-08,0.529177,-0.353635},{-5.96046e-08,0.528204,-0.352785},{-0.00776697,0.529177,
-0.352785},{0.00776686,0.529177,-0.352785},{-0.176392,0.705569,-0.385632},{-0.176392,
0.598015,-0.352785},{-0.260822,0.705569,-0.352785},{-5.96046e-08,0.705569,-0.407148},
{0.176392,0.705569,-0.385632},{0.176392,0.598015,-0.352785},{0.260822,0.705569,-0.352785},
{-0.176392,0.881962,-0.35331},{-0.177641,0.881962,-0.352785},{-5.96046e-08,0.881962,
-0.379518},{0.176392,0.881962,-0.35331},{0.177641,0.881962,-0.352785},{-0.176392,0.882951,
-0.352785},{-5.96046e-08,0.931839,-0.352785},{0.176392,0.882951,-0.352785},{-0.176392,
0.352785,-0.221411},{-0.176392,0.317021,-0.176392},{-0.269206,0.352785,-0.176392},{-5.96046e-08,
0.352785,-0.243605},{-5.96046e-08,0.299466,-0.176392},{0.176392,0.352785,-0.221411},
{0.176392,0.317021,-0.176392},{0.269206,0.352785,-0.176392},{-0.352785,0.529177,-0.267029},
{-0.352785,0.420868,-0.176392},{-0.453912,0.529177,-0.176392},{-0.176392,0.529177,
-0.337793},{0.176392,0.529177,-0.337793},{0.352785,0.529177,-0.267029},{0.352785,0.420868,
-0.176392},{0.453912,0.529177,-0.176392},{-0.352785,0.705569,-0.308162},{-0.487765,0.705569,
-0.176392},{0.352785,0.705569,-0.308162},{0.487765,0.705569,-0.176392},{-0.352785,0.881962,
-0.253337},{-0.426455,0.881962,-0.176392},{0.352785,0.881962,-0.253337},{0.426455,0.881962,
-0.176392},{-0.352785,0.967189,-0.176392},{-0.205155,1.05835,-0.176392},{-0.176392,1.05835,
-0.196359},{-5.96046e-08,1.05835,-0.238382},{0.176392,1.05835,-0.196359},{0.205155,
1.05835,-0.176392},{0.352785,0.967189,-0.176392},{-0.176392,1.07301,-0.176392},{-5.96046e-08,
1.10373,-0.176392},{0.176392,1.07301,-0.176392},{-0.352785,0.352785,-0.0634506},{-0.352785,
0.331881,-5.96046e-08},{-0.385299,0.352785,-5.96046e-08},{-0.176392,0.235788,-5.96046e-08},
{-5.96046e-08,0.192139,-5.96046e-08},{0.176392,0.235788,-5.96046e-08},{0.352785,0.352785,
-0.0634506},{0.352785,0.331881,-5.96046e-08},{0.385299,0.352785,-5.96046e-08},{-0.510515,
0.529177,-5.96046e-08},{0.510515,0.529177,-5.96046e-08},{-0.529177,0.705569,-0.0280969},
{-0.529177,0.664337,-5.96046e-08},{-0.537095,0.705569,-5.96046e-08},{0.529177,0.705569,
-0.0280969},{0.529177,0.664337,-5.96046e-08},{0.537095,0.705569,-5.96046e-08},{-0.529177,
0.733066,-5.96046e-08},{-0.480252,0.881962,-5.96046e-08},{0.480252,0.881962,-5.96046e-08},
{0.529177,0.733066,-5.96046e-08},{-0.352785,1.02431,-5.96046e-08},{-0.294584,1.05835,
-5.96046e-08},{0.294584,1.05835,-5.96046e-08},{0.352785,1.02431,-5.96046e-08},{-0.176392,
1.11786,-5.96046e-08},{-5.96046e-08,1.14508,-5.96046e-08},{0.176392,1.11786,-5.96046e-08},
{-0.352785,0.352785,0.0634504},{-0.176392,0.317021,0.176392},{-0.269206,0.352785,0.176392},
{-5.96046e-08,0.299466,0.176392},{0.176392,0.317021,0.176392},{0.352785,0.352785,0.0634504},
{0.269206,0.352785,0.176392},{-0.352785,0.420868,0.176392},{-0.453912,0.529177,0.176392},
{0.352785,0.420868,0.176392},{0.453912,0.529177,0.176392},{-0.529177,0.705569,0.0280968},
{-0.487765,0.705569,0.176392},{0.529177,0.705569,0.0280968},{0.487765,0.705569,0.176392},
{-0.426455,0.881962,0.176392},{0.426455,0.881962,0.176392},{-0.352785,0.967189,0.176392},
{-0.205155,1.05835,0.176392},{0.205155,1.05835,0.176392},{0.352785,0.967189,0.176392},
{-0.176392,1.07301,0.176392},{-5.96046e-08,1.10373,0.176392},{0.176392,1.07301,0.176392},
{-0.176392,0.352785,0.221411},{-5.96046e-08,0.352785,0.243605},{0.176392,0.352785,
0.221411},{-0.352785,0.529177,0.267029},{-0.176392,0.529177,0.337793},{-5.96046e-08,0.528204,
0.352785},{-0.00776697,0.529177,0.352785},{0.176392,0.529177,0.337793},{0.00776686,0.529177,
0.352785},{0.352785,0.529177,0.267029},{-0.352785,0.705569,0.308162},{-0.176392,0.598015,
0.352785},{-0.260822,0.705569,0.352785},{0.176392,0.598015,0.352785},{0.352785,0.705569,
0.308162},{0.260822,0.705569,0.352785},{-0.352785,0.881962,0.253337},{-0.177641,0.881962,
0.352785},{0.352785,0.881962,0.253337},{0.177641,0.881962,0.352785},{-0.176392,0.882951,
0.352785},{-0.176392,1.05835,0.196359},{-5.96046e-08,0.931839,0.352785},{-5.96046e-08,1.05835,
0.238382},{0.176392,0.882951,0.352785},{0.176392,1.05835,0.196359},{-5.96046e-08,0.529177,
0.353635},{-0.176392,0.705569,0.385632},{-5.96046e-08,0.705569,0.407147},{0.176392,
0.705569,0.385632},{-0.176392,0.881962,0.35331},{-5.96046e-08,0.881962,0.379518},{0.176392,
0.881962,0.35331}
};
	int connect<NEportLevels={2,0}>[816] = {
   0,1,2,3,1,0,4,5,6,7,0,2,7,2,5,7,5,4,8,9,3,8,3,0,8,0,7,10,9,8,11,4,6,
11,6,12,13,7,4,13,4,11,14,8,7,14,7,13,15,10,8,15,8,14,16,11,12,17,13,11,
17,11,16,18,14,13,18,13,17,18,15,14,19,20,21,22,23,20,22,20,19,24,25,23,
24,23,22,26,25,24,27,28,29,30,19,21,30,21,28,30,28,27,2,1,22,2,22,19,2,
19,30,1,3,31,1,31,24,1,24,22,32,33,26,32,26,24,32,24,31,34,33,32,35,27,
29,35,29,36,6,5,30,6,30,27,6,27,35,5,2,30,3,9,31,9,10,37,9,37,32,9,32,31,
38,34,32,38,32,37,39,35,36,39,36,40,12,6,35,12,35,39,10,15,41,10,41,37,
42,38,37,42,37,41,43,39,40,16,12,39,16,39,43,16,43,45,43,44,45,17,16,45,
17,45,46,18,17,46,18,46,47,15,18,47,15,47,48,15,48,41,48,49,41,49,42,41,
50,45,44,51,46,45,51,45,50,52,47,46,52,46,51,52,48,47,53,54,55,21,20,56,
21,56,54,21,54,53,20,23,57,20,57,56,23,25,58,23,58,57,25,26,59,25,59,60,
25,60,58,61,60,59,29,28,53,29,53,55,29,55,62,28,21,53,26,33,59,33,34,63,
33,63,61,33,61,59,64,65,66,36,29,62,36,62,65,36,65,64,34,38,67,34,67,68,
34,68,63,69,68,67,70,64,66,40,36,64,40,64,70,40,70,71,38,42,72,38,72,73,
38,73,67,73,69,67,43,40,71,43,71,74,44,43,74,44,74,75,49,48,76,49,76,77,
42,49,77,42,77,72,50,44,75,50,75,78,51,50,78,51,78,79,52,51,79,52,79,80,
48,52,80,48,80,76,55,54,81,54,56,82,54,82,83,54,83,81,56,57,84,56,84,82,
57,58,85,57,85,84,58,60,86,58,86,87,58,87,85,60,61,86,62,55,81,62,81,88,
62,88,89,83,88,81,86,90,87,61,63,91,61,91,90,61,90,86,66,65,92,65,62,89,
65,89,93,65,93,92,63,68,94,63,94,95,63,95,91,68,69,94,70,66,92,71,70,92,
71,92,93,71,93,96,73,72,97,73,97,95,73,95,94,69,73,94,74,71,96,74,96,98,
75,74,98,75,98,99,77,76,100,77,100,101,72,77,101,72,101,97,78,75,99,78,
99,102,79,78,102,79,102,103,80,79,103,80,103,104,76,80,104,76,104,100,83,
82,105,82,84,106,82,106,105,84,85,107,84,107,106,85,87,107,89,88,108,88,
83,105,88,105,109,88,109,108,106,110,111,106,111,109,106,109,105,107,112,
113,107,113,110,107,110,106,87,90,114,87,114,112,87,112,107,90,91,114,93,
89,108,93,108,115,109,116,117,109,117,115,109,115,108,111,116,109,112,118,
113,114,119,120,114,120,118,114,118,112,91,95,119,91,119,114,96,93,115,96,
115,121,117,122,121,117,121,115,119,123,124,119,124,120,95,97,123,95,123,
119,98,96,121,99,98,121,99,121,122,99,122,126,122,125,126,127,128,126,127,
126,125,129,130,128,129,128,127,101,100,130,101,130,129,101,129,123,129,
124,123,97,101,123,102,99,126,103,102,126,103,126,128,104,103,128,104,128,
130,100,104,130,111,110,131,110,113,131,117,116,132,116,111,131,116,131,
133,116,133,132,113,118,134,113,134,133,113,133,131,118,120,134,122,117,
132,122,132,135,133,136,135,133,135,132,134,137,136,134,136,133,120,124,
137,120,137,134,125,122,135,127,125,135,127,135,136,129,127,136,129,136,
137,124,129,137
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	};
      };
      group pyn_mesh {
	float coord<NEportLevels={2,0}>[138][3] = {
   {-0.176392,-0.881962,-0.35331},{-0.176392,-0.882951,-0.352785},{-0.177641,-0.881962,
-0.352785},{-5.96046e-08,-0.881962,-0.379518},{-5.96046e-08,-0.931839,-0.352785},{0.176392,
-0.881962,-0.35331},{0.176392,-0.882951,-0.352785},{0.177641,-0.881962,-0.352785},{-0.176392,
-0.705569,-0.385632},{-0.260822,-0.705569,-0.352785},{-5.96046e-08,-0.705569,-0.407148},
{0.176392,-0.705569,-0.385632},{0.260822,-0.705569,-0.352785},{-0.176392,-0.598015,
-0.352785},{-5.96046e-08,-0.529177,-0.353635},{-0.00776697,-0.529177,-0.352785},{0.176392,
-0.598015,-0.352785},{0.00776686,-0.529177,-0.352785},{-5.96046e-08,-0.528204,-0.352785},
{-0.176392,-1.05835,-0.196359},{-0.176392,-1.07301,-0.176392},{-0.205155,-1.05835,
-0.176392},{-5.96046e-08,-1.05835,-0.238382},{-5.96046e-08,-1.10373,-0.176392},{0.176392,
-1.05835,-0.196359},{0.176392,-1.07301,-0.176392},{0.205155,-1.05835,-0.176392},{-0.352785,
-0.881962,-0.253337},{-0.352785,-0.967189,-0.176392},{-0.426455,-0.881962,-0.176392},
{0.352785,-0.881962,-0.253337},{0.352785,-0.967189,-0.176392},{0.426455,-0.881962,-0.176392},
{-0.352785,-0.705569,-0.308162},{-0.487765,-0.705569,-0.176392},{0.352785,-0.705569,
-0.308162},{0.487765,-0.705569,-0.176392},{-0.352785,-0.529177,-0.267029},{-0.453912,
-0.529177,-0.176392},{-0.176392,-0.529177,-0.337793},{0.176392,-0.529177,-0.337793},
{0.352785,-0.529177,-0.267029},{0.453912,-0.529177,-0.176392},{-0.352785,-0.420868,
-0.176392},{-0.176392,-0.352785,-0.221411},{-0.269206,-0.352785,-0.176392},{-5.96046e-08,
-0.352785,-0.243605},{0.176392,-0.352785,-0.221411},{0.352785,-0.420868,-0.176392},
{0.269206,-0.352785,-0.176392},{-0.176392,-0.317021,-0.176392},{-5.96046e-08,-0.299466,
-0.176392},{0.176392,-0.317021,-0.176392},{-0.176392,-1.11786,-5.96046e-08},{-0.294584,
-1.05835,-5.96046e-08},{-5.96046e-08,-1.14508,-5.96046e-08},{0.176392,-1.11786,-5.96046e-08},
{0.294584,-1.05835,-5.96046e-08},{-0.352785,-1.02431,-5.96046e-08},{-0.480252,-0.881962,
-5.96046e-08},{0.352785,-1.02431,-5.96046e-08},{0.480252,-0.881962,-5.96046e-08},{-0.529177,
-0.705569,-0.0280969},{-0.529177,-0.733066,-5.96046e-08},{-0.537095,-0.705569,-5.96046e-08},
{0.529177,-0.705569,-0.0280969},{0.529177,-0.733066,-5.96046e-08},{0.537095,-0.705569,
-5.96046e-08},{-0.529177,-0.664337,-5.96046e-08},{-0.510515,-0.529177,-5.96046e-08},{0.510515,
-0.529177,-5.96046e-08},{0.529177,-0.664337,-5.96046e-08},{-0.385299,-0.352785,-5.96046e-08},
{-0.352785,-0.352785,-0.0634506},{0.352785,-0.352785,-0.0634506},{0.385299,-0.352785,
-5.96046e-08},{-0.352785,-0.331881,-5.96046e-08},{-0.176392,-0.235788,-5.96046e-08},{-5.96046e-08,
-0.192139,-5.96046e-08},{0.176392,-0.235788,-5.96046e-08},{0.352785,-0.331881,-5.96046e-08},
{-0.176392,-1.07301,0.176392},{-0.205155,-1.05835,0.176392},{-5.96046e-08,-1.10373,
0.176392},{0.176392,-1.07301,0.176392},{0.205155,-1.05835,0.176392},{-0.352785,-0.967189,
0.176392},{-0.426455,-0.881962,0.176392},{0.352785,-0.967189,0.176392},{0.426455,-0.881962,
0.176392},{-0.529177,-0.705569,0.0280968},{-0.487765,-0.705569,0.176392},{0.529177,
-0.705569,0.0280968},{0.487765,-0.705569,0.176392},{-0.453912,-0.529177,0.176392},
{0.453912,-0.529177,0.176392},{-0.352785,-0.420868,0.176392},{-0.352785,-0.352785,0.0634504},
{-0.269206,-0.352785,0.176392},{0.352785,-0.420868,0.176392},{0.352785,-0.352785,0.0634504},
{0.269206,-0.352785,0.176392},{-0.176392,-0.317021,0.176392},{-5.96046e-08,-0.299466,
0.176392},{0.176392,-0.317021,0.176392},{-0.176392,-1.05835,0.196359},{-5.96046e-08,-1.05835,
0.238382},{0.176392,-1.05835,0.196359},{-0.352785,-0.881962,0.253337},{-0.176392,-0.882951,
0.352785},{-0.177641,-0.881962,0.352785},{-5.96046e-08,-0.931839,0.352785},{0.176392,
-0.882951,0.352785},{0.352785,-0.881962,0.253337},{0.177641,-0.881962,0.352785},{-0.352785,
-0.705569,0.308162},{-0.260822,-0.705569,0.352785},{0.352785,-0.705569,0.308162},{0.260822,
-0.705569,0.352785},{-0.352785,-0.529177,0.267029},{-0.176392,-0.598015,0.352785},
{-0.176392,-0.529177,0.337793},{-0.00776697,-0.529177,0.352785},{0.176392,-0.598015,
0.352785},{0.176392,-0.529177,0.337793},{0.00776686,-0.529177,0.352785},{0.352785,-0.529177,
0.267029},{-0.176392,-0.352785,0.221411},{-5.96046e-08,-0.528204,0.352785},{-5.96046e-08,
-0.352785,0.243605},{0.176392,-0.352785,0.221411},{-0.176392,-0.881962,0.35331},{-5.96046e-08,
-0.881962,0.379518},{0.176392,-0.881962,0.35331},{-0.176392,-0.705569,0.385632},{-5.96046e-08,
-0.705569,0.407147},{0.176392,-0.705569,0.385632},{-5.96046e-08,-0.529177,0.353635}
};
	int connect<NEportLevels={2,0}>[816] = {
   0,1,2,3,4,1,3,1,0,5,6,4,5,4,3,7,6,5,8,0,2,8,2,9,10,3,0,10,0,8,11,5,
3,11,3,10,12,7,5,12,5,11,13,8,9,14,10,8,14,8,13,14,13,15,16,11,10,16,10,
14,16,14,17,16,12,11,18,14,15,18,17,14,19,20,21,22,23,20,22,20,19,24,25,
23,24,23,22,26,25,24,27,28,29,2,1,19,2,19,21,2,21,27,21,28,27,1,4,22,1,
22,19,4,6,24,4,24,22,6,7,30,6,30,31,6,31,24,31,26,24,32,31,30,33,27,29,
33,29,34,9,2,27,9,27,33,7,12,35,7,35,30,36,32,30,36,30,35,37,33,34,37,34,
38,13,9,33,13,33,37,13,37,39,15,13,39,16,17,40,12,16,40,12,40,41,12,41,
35,42,36,35,42,35,41,43,37,38,44,39,37,44,37,43,44,43,45,18,15,39,18,39,
44,18,44,46,17,18,46,17,46,47,17,47,40,48,41,40,48,40,47,48,47,49,48,42,
41,50,44,45,51,46,44,51,44,50,52,47,46,52,46,51,52,49,47,21,20,53,21,53,
54,20,23,55,20,55,53,23,25,56,23,56,55,25,26,57,25,57,56,29,28,58,29,58,
59,28,21,54,28,54,58,26,31,60,26,60,57,31,32,61,31,61,60,62,63,64,34,29,
59,34,59,63,34,63,62,32,36,65,32,65,66,32,66,61,67,66,65,68,62,64,38,34,
62,38,62,68,38,68,69,36,42,70,36,70,71,36,71,65,71,67,65,43,38,69,43,69,
72,43,72,73,45,43,73,48,49,74,42,48,74,42,74,75,42,75,70,76,73,72,50,45,
73,50,73,76,50,76,77,51,50,77,51,77,78,52,51,78,52,78,79,49,52,79,49,79,
80,49,80,74,80,75,74,54,53,81,54,81,82,53,55,83,53,83,81,55,56,84,55,84,
83,56,57,85,56,85,84,59,58,86,59,86,87,58,54,82,58,82,86,57,60,88,57,88,
85,60,61,89,60,89,88,64,63,90,63,59,87,63,87,91,63,91,90,61,66,92,61,92,
93,61,93,89,66,67,92,68,64,90,69,68,90,69,90,91,69,91,94,71,70,95,71,95,
93,71,93,92,67,71,92,72,69,94,72,94,96,72,96,97,96,98,97,99,100,101,70,
75,100,70,100,99,70,99,95,76,72,97,77,76,97,77,97,98,77,98,102,78,77,102,
78,102,103,79,78,103,79,103,104,80,79,104,80,104,101,80,101,100,75,80,100,
82,81,105,81,83,106,81,106,105,83,84,107,83,107,106,84,85,107,87,86,108,
86,82,105,86,105,109,86,109,108,109,110,108,106,111,109,106,109,105,107,
112,111,107,111,106,85,88,113,85,113,114,85,114,107,114,112,107,88,89,113,
91,87,108,91,108,115,110,116,115,110,115,108,113,117,118,113,118,114,89,
93,117,89,117,113,94,91,115,94,115,119,120,121,119,120,119,115,120,115,116,
120,122,121,123,124,125,117,126,124,117,124,123,117,123,118,93,95,126,93,
126,117,96,94,119,98,96,119,98,119,121,98,121,127,128,129,127,128,127,121,
128,121,122,124,130,129,124,129,128,124,128,125,99,101,130,99,130,124,99,
124,126,95,99,126,102,98,127,103,102,127,103,127,129,104,103,129,104,129,
130,101,104,130,110,109,131,109,111,132,109,132,131,111,112,133,111,133,
132,112,114,133,116,110,131,116,131,134,132,135,134,132,134,131,133,136,
135,133,135,132,114,118,136,114,136,133,120,116,134,122,120,134,122,134,
135,122,135,137,123,125,137,123,137,135,123,135,136,118,123,136,128,122,
137,125,128,137
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	};
      };
      dlv_glyph pglyph {
	in_field => <-.<-.orbs.orbital[2];
	in_glyph => <-.pyp_mesh.out;
	GlyphParam {
	  map_comp = 1;
	  scale => <-.<-.<-.scale;
	};
      };
      dlv_glyph nglyph {
	in_field => <-.<-.orbs.orbital[2];
	in_glyph => pyn_mesh.out;
	GlyphParam {
	  map_comp = 2;
	  scale => <-.<-.<-.scale;
	};
      };
    };
    GMOD.instancer pz_instancer {
      Value => (<-.orbs.orbital[3].nnodes > 0);
      Group => <-.pz_orb;
      active = 2;
    };
    macro pz_orb<instanced=0> {
      // grids cut out of AVS with $get_array then attached to a tri_mesh.
      group pzp_mesh {
	float coord<NEportLevels={2,0}>[158][3] = {
	  -5.96046e-08,-0.352785,0.333623,-5.96046e-08,-0.38259,0.352785,
	  -0.111512,-0.352785,0.352785,0.111512,-0.352785,0.352785,-0.176392,
	  -0.176392,0.2711,-0.176392,-0.335054,0.352785,-0.335054,-0.176392,
	  0.352785,-5.96046e-08,-0.176392,0.237089,0.176392,-0.176392,0.2711,
	  0.176392,-0.335054,0.352785,0.335054,-0.176392,0.352785,-0.352785,
	  -5.96046e-08,0.333623,-0.352785,-0.111512,0.352785,-0.38259,
	  -5.96046e-08,0.352785,-0.176392,-5.96046e-08,0.237089,-5.96046e-08,
	  -5.96046e-08,0.193368,0.176392,-5.96046e-08,0.237089,0.352785,
	  -5.96046e-08,0.333623,0.352785,-0.111512,0.352785,0.38259,
	  -5.96046e-08,0.352785,-0.352785,0.111512,0.352785,-0.176392,0.176392,
	  0.2711,-0.335054,0.176392,0.352785,-5.96046e-08,0.176392,0.237089,
	  0.176392,0.176392,0.2711,0.352785,0.111512,0.352785,0.335054,
	  0.176392,0.352785,-0.176392,0.335054,0.352785,-5.96046e-08,0.352785,0.333623,-0.111512,0.352785,0.352785,0.176392,
	  0.335054,0.352785,0.111512,0.352785,0.352785,-5.96046e-08,0.38259,0.352785,-0.352785,
	  -0.352785,0.518727,-0.352785,-0.363085,0.529177,-0.363085,-0.352785,0.529177,
	  -0.176392,-0.352785,0.373902,-0.176392,-0.47765,0.529177,-5.96046e-08,-0.508153,
	  0.529177,0.176392,-0.352785,0.373902,0.176392,-0.47765,0.529177,0.352785,-0.352785,
	  0.518727,0.352785,-0.363085,0.529177,0.363085,-0.352785,0.529177,-0.352785,-0.176392,
	  0.373902,-0.47765,-0.176392,0.529177,0.352785,-0.176392,0.373902,0.47765,-0.176392,
	  0.529177,-0.508153,-5.96046e-08,0.529177,0.508153,-5.96046e-08,0.529177,-0.47765,
	  0.176392,0.529177,-0.352785,0.176392,0.373902,0.352785,0.176392,0.373902,0.47765,
	  0.176392,0.529177,-0.352785,0.352785,0.518727,-0.363085,0.352785,0.529177,-0.176392,
	  0.352785,0.373902,0.176392,0.352785,0.373902,0.352785,0.352785,0.518727,0.363085,
	  0.352785,0.529177,-0.352785,0.363085,0.529177,-0.176392,0.47765,0.529177,-5.96046e-08,
	  0.508153,0.529177,0.176392,0.47765,0.529177,0.352785,0.363085,0.529177,-5.96046e-08,
	  -0.529177,0.681446,-5.96046e-08,-0.533809,0.705569,-0.0256936,-0.529177,0.705569,
	  0.0256935,-0.529177,0.705569,-0.352785,-0.399804,0.705569,-0.399804,-0.352785,
	  0.705569,-0.176392,-0.504134,0.705569,0.176392,-0.504134,0.705569,0.352785,-0.399804,
	  0.705569,0.399804,-0.352785,0.705569,-0.504134,-0.176392,0.705569,0.504134,-0.176392,
	  0.705569,-0.529177,-5.96046e-08,0.681446,-0.529177,-0.0256936,0.705569,-0.533809,
	  -5.96046e-08,0.705569,0.529177,-5.96046e-08,0.681446,0.529177,-0.0256936,0.705569,
	  0.533809,-5.96046e-08,0.705569,-0.529177,0.0256935,0.705569,-0.504134,0.176392,
	  0.705569,0.504134,0.176392,0.705569,0.529177,0.0256935,0.705569,-0.399804,0.352785,
	  0.705569,0.399804,0.352785,0.705569,-0.352785,0.399804,0.705569,-0.176392,0.504134,
	  0.705569,-0.0256936,0.529177,0.705569,-5.96046e-08,0.529177,0.681446,0.0256935,
	  0.529177,0.705569,0.176392,0.504134,0.705569,0.352785,0.399804,0.705569,-5.96046e-08,
	  0.533809,0.705569,-5.96046e-08,-0.529177,0.721657,-0.352785,-0.352785,0.823066,
	  -0.176392,-0.442092,0.881962,-0.31517,-0.352785,0.881962,-5.96046e-08,-0.476503,
	  0.881962,0.176392,-0.442092,0.881962,0.352785,-0.352785,0.823066,0.31517,-0.352785,
	  0.881962,-0.352785,-0.31517,0.881962,-0.442092,-0.176392,0.881962,0.352785,-0.31517,
	  0.881962,0.442092,-0.176392,0.881962,-0.529177,-5.96046e-08,0.721657,-0.476503,
	  -5.96046e-08,0.881962,0.529177,-5.96046e-08,0.721657,0.476503,-5.96046e-08,0.881962,
	  -0.442092,0.176392,0.881962,0.442092,0.176392,0.881962,-0.352785,0.31517,0.881962,
	  -0.352785,0.352785,0.823066,-0.31517,0.352785,0.881962,0.352785,0.31517,0.881962,
	  0.352785,0.352785,0.823066,0.31517,0.352785,0.881962,-0.176392,0.442092,0.881962,
	  -5.96046e-08,0.476503,0.881962,-5.96046e-08,0.529177,0.721657,0.176392,0.442092,
	  0.881962,-0.176392,-0.352785,0.983426,-5.96046e-08,-0.352785,1.02013,0.176392,
	  -0.352785,0.983426,-0.352785,-0.176392,0.983426,-0.176392,-0.227949,1.05835,
	  -0.227949,-0.176392,1.05835,-5.96046e-08,-0.287425,1.05835,0.176392,-0.227949,
	  1.05835,0.352785,-0.176392,0.983426,0.227948,-0.176392,1.05835,-0.352785,-5.96046e-08,
	  1.02013,-0.287425,-5.96046e-08,1.05835,0.352785,-5.96046e-08,1.02013,0.287425,
	  -5.96046e-08,1.05835,-0.352785,0.176392,0.983426,-0.227949,0.176392,1.05835,
	  0.352785,0.176392,0.983426,0.227948,0.176392,1.05835,-0.176392,0.227948,1.05835,
	  -0.176392,0.352785,0.983426,-5.96046e-08,0.287425,1.05835,-5.96046e-08,0.352785,
	  1.02013,0.176392,0.227948,1.05835,0.176392,0.352785,0.983426,-0.176392,-0.176392,
	  1.08448,-5.96046e-08,-0.176392,1.11426,0.176392,-0.176392,1.08448,-0.176392,-5.96046e-08,
	  1.11426,-5.96046e-08,-5.96046e-08,1.14173,0.176392,-5.96046e-08,1.11426,-0.176392,
	  0.176392,1.08448,-5.96046e-08,0.176392,1.11426,0.176392,0.176392,1.08448
	};
	int connect<NEportLevels={2,0}>[936] = {
	  0,1,2,3,1,0,4,5,6,7,0,2,7,2,5,7,5,4,8,9,3,8,3,0,8,0,7,10,9,8,11,12,
	  13,14,4,6,14,6,12,14,12,11,15,7,4,15,4,14,16,8,7,16,7,15,17,18,10,17,10,
	  8,17,8,16,19,18,17,20,11,13,21,14,11,21,11,20,21,20,22,23,15,14,23,14,21,
	  24,16,15,24,15,23,25,17,16,25,16,24,25,24,26,25,19,17,27,21,22,28,23,21,
	  28,21,27,28,27,29,30,24,23,30,23,28,30,28,31,30,26,24,32,28,29,32,31,28,
	  33,34,35,36,37,34,36,34,33,2,1,38,2,38,37,2,37,36,1,3,39,1,39,40,1,40,38,
	  41,42,40,41,40,39,43,42,41,44,33,35,44,35,45,6,5,36,6,36,33,6,33,44,5,
	  2,36,3,9,39,9,10,46,9,46,41,9,41,39,47,43,41,47,41,46,13,12,44,13,44,45,
	  13,45,48,12,6,44,10,18,46,18,19,49,18,49,47,18,47,46,20,13,48,20,48,50,
	  20,50,51,22,20,51,25,26,52,19,25,52,19,52,53,19,53,49,54,51,50,54,50,55,
	  27,22,51,27,51,54,27,54,56,29,27,56,30,31,57,26,30,57,26,57,58,26,58,52,
	  59,53,52,59,52,58,60,54,55,61,56,54,61,54,60,32,29,56,32,56,61,32,61,62,
	  31,32,62,31,62,63,31,63,57,64,58,57,64,57,63,64,59,58,65,66,67,68,66,65,
	  35,34,69,35,69,70,34,37,71,34,71,69,37,38,65,37,65,67,37,67,71,38,40,72,
	  38,72,68,38,68,65,40,42,73,40,73,72,42,43,74,42,74,73,45,35,70,45,70,75,
	  43,47,76,43,76,74,77,78,79,48,45,75,48,75,78,48,78,77,47,49,80,47,80,81,
	  47,81,76,82,81,80,83,77,79,50,48,77,50,77,83,50,83,84,49,53,85,49,85,86,
	  49,86,80,86,82,80,55,50,84,55,84,87,53,59,88,53,88,85,60,55,87,60,87,89,
	  61,60,89,61,89,90,62,61,90,62,90,91,62,91,92,63,62,92,63,92,93,63,93,94,
	  64,63,94,64,94,95,59,64,95,59,95,88,96,92,91,96,93,92,67,66,97,66,68,97,
	  70,69,98,69,71,99,69,99,100,69,100,98,71,67,97,71,97,101,71,101,99,68,72,
	  102,68,102,101,68,101,97,72,73,103,72,103,104,72,104,102,73,74,103,75,70,
	  98,75,98,105,75,105,106,100,105,98,103,107,104,74,76,108,74,108,107,74,
	  107,103,79,78,109,78,75,106,78,106,110,78,110,109,76,81,111,76,111,112,76,
	  112,108,81,82,111,83,79,109,84,83,109,84,109,110,84,110,113,86,85,114,86,
	  114,112,86,112,111,82,86,111,87,84,113,87,113,115,87,115,116,115,117,116,
	  118,119,120,85,88,119,85,119,118,85,118,114,89,87,116,90,89,116,90,116,117,
	  90,117,121,91,90,121,91,121,122,91,122,123,94,93,123,94,123,122,94,122,124,
	  95,94,124,95,124,120,95,120,119,88,95,119,96,91,123,93,96,123,100,99,125,
	  99,101,126,99,126,125,101,102,127,101,127,126,102,104,127,106,105,128,105,
	  100,125,105,125,129,105,129,128,129,130,128,126,131,129,126,129,125,127,
	  132,131,127,131,126,104,107,133,104,133,134,104,134,127,134,132,127,107,
	  108,133,110,106,128,110,128,135,130,136,135,130,135,128,133,137,138,133,
	  138,134,108,112,137,108,137,133,113,110,135,113,135,139,136,140,139,136,
	  139,135,137,141,142,137,142,138,112,114,141,112,141,137,115,113,139,117,
	  115,139,117,139,140,117,140,144,140,143,144,145,146,144,145,144,143,147,
	  148,146,147,146,145,118,120,148,118,148,147,118,147,141,147,142,141,114,
	  118,141,121,117,144,122,121,144,122,144,146,124,122,146,124,146,148,120,
	  124,148,130,129,149,129,131,150,129,150,149,131,132,151,131,151,150,132,
	  134,151,136,130,149,136,149,152,150,153,152,150,152,149,151,154,153,151,
	  153,150,134,138,154,134,154,151,140,136,152,140,152,155,153,156,155,153,
	  155,152,154,157,156,154,156,153,138,142,157,138,157,154,143,140,155,145,
	  143,155,145,155,156,147,145,156,147,156,157,142,147,157
	};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	};
      };
      group pzn_mesh {
	float coord<NEportLevels={2,0}>[158][3] = {
	  {-0.176392,-0.176392,-1.08448},{-0.176392,-0.227949,-1.05835},{-0.227949,-0.176392,
									 -1.05835},{-5.96046e-08,-0.176392,-1.11426},{-5.96046e-08,-0.287425,-1.05835},{0.176392,
																			-0.176392,-1.08448},{0.176392,-0.227949,-1.05835},{0.227948,-0.176392,-1.05835},{-0.176392,
																													 -5.96046e-08,-1.11426},{-0.287425,-5.96046e-08,-1.05835},{-5.96046e-08,-5.96046e-08,-1.14173},
	  {0.176392,-5.96046e-08,-1.11426},{0.287425,-5.96046e-08,-1.05835},{-0.176392,0.176392,
									     -1.08448},{-0.227949,0.176392,-1.05835},{-5.96046e-08,0.176392,-1.11426},{0.176392,
																		       0.176392,-1.08448},{0.227948,0.176392,-1.05835},{-0.176392,0.227948,-1.05835},{-5.96046e-08,
																												      0.287425,-1.05835},{0.176392,0.227948,-1.05835},{-0.176392,-0.352785,-0.983426},{-0.176392,
																																						       -0.442092,-0.881962},{-0.31517,-0.352785,-0.881962},{-5.96046e-08,-0.352785,-1.02013},
	  {-5.96046e-08,-0.476503,-0.881962},{0.176392,-0.352785,-0.983426},{0.176392,-0.442092,
									     -0.881962},{0.31517,-0.352785,-0.881962},{-0.352785,-0.176392,-0.983426},{-0.352785,
																		       -0.31517,-0.881962},{-0.442092,-0.176392,-0.881962},{0.352785,-0.176392,-0.983426},
	  {0.352785,-0.31517,-0.881962},{0.442092,-0.176392,-0.881962},{-0.352785,-5.96046e-08,-1.02013},
	  {-0.476503,-5.96046e-08,-0.881962},{0.352785,-5.96046e-08,-1.02013},{0.476503,-5.96046e-08,
									       -0.881962},{-0.352785,0.176392,-0.983426},{-0.442092,0.176392,-0.881962},{0.352785,
																			 0.176392,-0.983426},{0.442092,0.176392,-0.881962},{-0.352785,0.31517,-0.881962},{-0.31517,
																													  0.352785,-0.881962},{-0.176392,0.352785,-0.983426},{-5.96046e-08,0.352785,-1.02013},
	  {0.176392,0.352785,-0.983426},{0.31517,0.352785,-0.881962},{0.352785,0.31517,-0.881962},
	  {-0.176392,0.442092,-0.881962},{-5.96046e-08,0.476503,-0.881962},{0.176392,0.442092,
									    -0.881962},{-5.96046e-08,-0.529177,-0.721657},{-5.96046e-08,-0.533809,-0.705569},{-0.0256936,
																			      -0.529177,-0.705569},{0.0256935,-0.529177,-0.705569},{-0.352785,-0.352785,-0.823066},
	  {-0.352785,-0.399804,-0.705569},{-0.399804,-0.352785,-0.705569},{-0.176392,-0.504134,
									   -0.705569},{0.176392,-0.504134,-0.705569},{0.352785,-0.352785,-0.823066},{0.352785,
																		     -0.399804,-0.705569},{0.399804,-0.352785,-0.705569},{-0.504134,-0.176392,-0.705569},
	  {0.504134,-0.176392,-0.705569},{-0.529177,-5.96046e-08,-0.721657},{-0.529177,-0.0256936,
									     -0.705569},{-0.533809,-5.96046e-08,-0.705569},{0.529177,-5.96046e-08,-0.721657},{0.529177,
																			      -0.0256936,-0.705569},{0.533809,-5.96046e-08,-0.705569},{-0.529177,0.0256935,-0.705569},
	  {-0.504134,0.176392,-0.705569},{0.504134,0.176392,-0.705569},{0.529177,0.0256935,-0.705569},
	  {-0.399804,0.352785,-0.705569},{-0.352785,0.352785,-0.823066},{0.352785,0.352785,-0.823066},
	  {0.399804,0.352785,-0.705569},{-0.352785,0.399804,-0.705569},{-0.176392,0.504134,-0.705569},
	  {-0.0256936,0.529177,-0.705569},{-5.96046e-08,0.529177,-0.721657},{0.0256935,0.529177,
									     -0.705569},{0.176392,0.504134,-0.705569},{0.352785,0.399804,-0.705569},{-5.96046e-08,0.533809,
																		     -0.705569},{-5.96046e-08,-0.529177,-0.681446},{-0.352785,-0.363085,-0.529177},{-0.363085,
																												    -0.352785,-0.529177},{-0.176392,-0.47765,-0.529177},{-5.96046e-08,-0.508153,-0.529177},
	  {0.176392,-0.47765,-0.529177},{0.352785,-0.363085,-0.529177},{0.363085,-0.352785,-0.529177},
	  {-0.47765,-0.176392,-0.529177},{0.47765,-0.176392,-0.529177},{-0.529177,-5.96046e-08,-0.681446},
	  {-0.508153,-5.96046e-08,-0.529177},{0.529177,-5.96046e-08,-0.681446},{0.508153,-5.96046e-08,
										-0.529177},{-0.47765,0.176392,-0.529177},{0.47765,0.176392,-0.529177},{-0.363085,0.352785,
																		       -0.529177},{0.363085,0.352785,-0.529177},{-0.352785,0.363085,-0.529177},{-0.176392,0.47765,
																												-0.529177},{-5.96046e-08,0.508153,-0.529177},{-5.96046e-08,0.529177,-0.681446},{0.176392,
																																						0.47765,-0.529177},{0.352785,0.363085,-0.529177},{-0.352785,-0.352785,-0.518727},{-0.176392,
																																																  -0.352785,-0.373902},{-5.96046e-08,-0.38259,-0.352785},{-0.111512,-0.352785,-0.352785},
	  {0.176392,-0.352785,-0.373902},{0.111512,-0.352785,-0.352785},{0.352785,-0.352785,-0.518727},
	  {-0.352785,-0.176392,-0.373902},{-0.176392,-0.335054,-0.352785},{-0.335054,-0.176392,
									   -0.352785},{0.176392,-0.335054,-0.352785},{0.352785,-0.176392,-0.373902},{0.335054,
																		     -0.176392,-0.352785},{-0.352785,-0.111512,-0.352785},{-0.38259,-5.96046e-08,-0.352785},
	  {0.352785,-0.111512,-0.352785},{0.38259,-5.96046e-08,-0.352785},{-0.352785,0.111512,
									   -0.352785},{-0.352785,0.176392,-0.373902},{-0.335054,0.176392,-0.352785},{0.352785,
																		     0.111512,-0.352785},{0.352785,0.176392,-0.373902},{0.335054,0.176392,-0.352785},{-0.352785,
																												      0.352785,-0.518727},{-0.176392,0.335054,-0.352785},{-0.176392,0.352785,-0.373902},{-0.111512,
																																							 0.352785,-0.352785},{0.176392,0.335054,-0.352785},{0.176392,0.352785,-0.373902},{0.111512,
																																																	  0.352785,-0.352785},{0.352785,0.352785,-0.518727},{-5.96046e-08,0.38259,-0.352785},
	  {-5.96046e-08,-0.352785,-0.333623},{-0.176392,-0.176392,-0.2711},{-5.96046e-08,-0.176392,
									    -0.237089},{0.176392,-0.176392,-0.2711},{-0.352785,-5.96046e-08,-0.333623},{-0.176392,
																			-5.96046e-08,-0.237089},{-5.96046e-08,-5.96046e-08,-0.193368},{0.176392,-5.96046e-08,
																										       -0.237089},{0.352785,-5.96046e-08,-0.333623},{-0.176392,0.176392,-0.2711},{-5.96046e-08,
																																				  0.176392,-0.237089},{0.176392,0.176392,-0.2711},{-5.96046e-08,0.352785,-0.333623}
	};
	int connect<NEportLevels={2,0}>[936] = {
   0,1,2,3,4,1,3,1,0,5,6,4,5,4,3,7,6,5,8,0,2,8,2,9,10,3,0,10,0,8,11,5,
3,11,3,10,12,7,5,12,5,11,13,8,9,13,9,14,15,10,8,15,8,13,16,11,10,16,10,
15,17,12,11,17,11,16,18,13,14,19,15,13,19,13,18,20,16,15,20,15,19,20,17,
16,21,22,23,24,25,22,24,22,21,26,27,25,26,25,24,28,27,26,29,30,31,2,1,
21,2,21,23,2,23,29,23,30,29,1,4,24,1,24,21,4,6,26,4,26,24,6,7,32,6,32,33,
6,33,26,33,28,26,34,33,32,35,29,31,35,31,36,9,2,29,9,29,35,7,12,37,7,37,
32,38,34,32,38,32,37,39,35,36,39,36,40,14,9,35,14,35,39,12,17,41,12,41,
37,42,38,37,42,37,41,43,39,40,18,14,39,18,39,43,18,43,45,43,44,45,19,18,
45,19,45,46,20,19,46,20,46,47,17,20,47,17,47,48,17,48,41,48,49,41,49,42,
41,50,45,44,51,46,45,51,45,50,52,47,46,52,46,51,52,48,47,53,54,55,56,54,
53,57,58,59,23,22,60,23,60,58,23,58,57,22,25,53,22,53,55,22,55,60,25,27,
61,25,61,56,25,56,53,27,28,62,27,62,63,27,63,61,64,63,62,31,30,57,31,57,
59,31,59,65,30,23,57,28,33,62,33,34,66,33,66,64,33,64,62,67,68,69,36,31,
65,36,65,68,36,68,67,34,38,70,34,70,71,34,71,66,72,71,70,73,67,69,40,36,
67,40,67,73,40,73,74,38,42,75,38,75,76,38,76,70,76,72,70,43,40,74,43,74,
77,43,77,78,44,43,78,49,48,79,42,49,79,42,79,80,42,80,75,81,78,77,50,44,
78,50,78,81,50,81,82,51,50,82,51,82,83,51,83,84,52,51,84,52,84,85,52,85,
86,48,52,86,48,86,87,48,87,79,87,80,79,88,84,83,88,85,84,55,54,89,54,56,
89,59,58,90,59,90,91,58,60,92,58,92,90,60,55,89,60,89,93,60,93,92,56,61,
94,56,94,93,56,93,89,61,63,95,61,95,94,63,64,96,63,96,95,65,59,91,65,91,
97,64,66,98,64,98,96,69,68,99,68,65,97,68,97,100,68,100,99,66,71,101,66,
101,102,66,102,98,71,72,101,73,69,99,74,73,99,74,99,100,74,100,103,76,75,
104,76,104,102,76,102,101,72,76,101,77,74,103,77,103,105,75,80,106,75,106,
104,81,77,105,81,105,107,82,81,107,82,107,108,83,82,108,83,108,109,83,109,
110,86,85,110,86,110,109,86,109,111,87,86,111,87,111,112,80,87,112,80,112,
106,88,83,110,85,88,110,91,90,113,90,92,114,90,114,113,92,93,115,92,115,
116,92,116,114,93,94,117,93,117,118,93,118,115,94,95,119,94,119,117,95,
96,119,97,91,113,97,113,120,114,121,122,114,122,120,114,120,113,116,121,
114,117,123,118,119,124,125,119,125,123,119,123,117,96,98,124,96,124,119,
100,97,120,100,120,126,100,126,127,122,126,120,124,128,125,98,102,129,98,
129,128,98,128,124,103,100,127,103,127,130,103,130,131,130,132,131,133,134,
135,102,104,134,102,134,133,102,133,129,105,103,131,105,131,136,137,138,
136,137,136,131,137,131,132,137,139,138,140,141,142,134,143,141,134,141,
140,134,140,135,104,106,143,104,143,134,107,105,136,108,107,136,108,136,
138,109,108,138,109,138,139,109,139,144,111,109,144,111,144,142,111,142,
141,112,111,141,112,141,143,106,112,143,116,115,145,115,118,145,122,121,
146,121,116,145,121,145,147,121,147,146,118,123,148,118,148,147,118,147,
145,123,125,148,127,126,149,126,122,146,126,146,150,126,150,149,147,151,
150,147,150,146,148,152,151,148,151,147,125,128,153,125,153,152,125,152,
148,128,129,153,130,127,149,132,130,149,132,149,150,132,150,154,151,155,
154,151,154,150,152,156,155,152,155,151,133,135,156,133,156,152,133,152,
153,129,133,153,137,132,154,139,137,154,139,154,155,139,155,157,140,142,
157,140,157,155,140,155,156,135,140,156,144,139,157,142,144,157
	};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	};
      };
      dlv_glyph pglyph {
	in_field => <-.<-.orbs.orbital[3];
	in_glyph => <-.pzp_mesh.out;
	GlyphParam {
	  map_comp = 1;
	  scale => <-.<-.<-.scale;
	};
      };
      dlv_glyph nglyph {
	in_field => <-.<-.orbs.orbital[3];
	in_glyph => pzn_mesh.out;
	GlyphParam {
	  map_comp = 2;
	  scale => <-.<-.<-.scale;
	};
      };
    };
    GMOD.instancer dz2r2_instancer {
      Value => (<-.orbs.orbital[4].nnodes > 0);
      Group => <-.dz2r2_orb;
      active = 2;
    };
    macro dz2r2_orb<instanced=0> {
      // grids cut out of AVS with $get_array then attached to a tri_mesh.
      group dp_mesh {
	float coord<NEportLevels={2,0}>[732][3] = {
   {-0.22049,-0.440981,-2.27076},{-0.22049,-0.542998,-2.2049},{-0.382006,-0.440981,
-2.2049},{-5.96046e-08,-0.440981,-2.2982},{-5.96046e-08,-0.586646,-2.2049},{0.22049,
-0.440981,-2.27076},{0.22049,-0.542998,-2.2049},{0.382006,-0.440981,-2.2049},{-0.440981,
-0.22049,-2.27076},{-0.440981,-0.382006,-2.2049},{-0.542998,-0.22049,-2.2049},{-0.22049,
-0.22049,-2.34764},{-5.96046e-08,-0.22049,-2.36985},{0.22049,-0.22049,-2.34764},{0.440981,
-0.22049,-2.27076},{0.440981,-0.382006,-2.2049},{0.542997,-0.22049,-2.2049},{-0.440981,
-5.96046e-08,-2.2982},{-0.586646,-5.96046e-08,-2.2049},{-0.22049,-5.96046e-08,-2.36985},
{-5.96046e-08,-5.96046e-08,-2.39053},{0.22049,-5.96046e-08,-2.36985},{0.440981,-5.96046e-08,
-2.2982},{0.586646,-5.96046e-08,-2.2049},{-0.440981,0.22049,-2.27076},{-0.542998,0.22049,
-2.2049},{-0.22049,0.22049,-2.34764},{-5.96046e-08,0.22049,-2.36985},{0.22049,0.22049,
-2.34764},{0.440981,0.22049,-2.27076},{0.542997,0.22049,-2.2049},{-0.440981,0.382006,
-2.2049},{-0.22049,0.440981,-2.27076},{-0.382006,0.440981,-2.2049},{-5.96046e-08,0.440981,
-2.2982},{0.22049,0.440981,-2.27076},{0.440981,0.382006,-2.2049},{0.382006,0.440981,
-2.2049},{-0.22049,0.542997,-2.2049},{-5.96046e-08,0.586646,-2.2049},{0.22049,0.542997,
-2.2049},{-0.440981,-0.661471,-2.00844},{-0.440981,-0.682026,-1.98441},{-0.468225,-0.661471,
-1.98441},{-0.22049,-0.661471,-2.12196},{-0.22049,-0.784409,-1.98441},{-5.96046e-08,-0.661471,
-2.15353},{-5.96046e-08,-0.814737,-1.98441},{0.22049,-0.661471,-2.12196},{0.22049,
-0.784409,-1.98441},{0.440981,-0.661471,-2.00844},{0.440981,-0.682026,-1.98441},{0.468225,
-0.661471,-1.98441},{-0.661471,-0.440981,-2.00844},{-0.661471,-0.468225,-1.98441},
{-0.682026,-0.440981,-1.98441},{-0.440981,-0.440981,-2.18236},{0.440981,-0.440981,-2.18236},
{0.661471,-0.440981,-2.00844},{0.661471,-0.468225,-1.98441},{0.682026,-0.440981,-1.98441},
{-0.661471,-0.22049,-2.12196},{-0.784409,-0.22049,-1.98441},{0.661471,-0.22049,-2.12196},
{0.784409,-0.22049,-1.98441},{-0.661471,-5.96046e-08,-2.15353},{-0.814737,-5.96046e-08,
-1.98441},{0.661471,-5.96046e-08,-2.15353},{0.814737,-5.96046e-08,-1.98441},{-0.661471,
0.22049,-2.12196},{-0.784409,0.22049,-1.98441},{0.661471,0.22049,-2.12196},{0.784409,
0.22049,-1.98441},{-0.661471,0.440981,-2.00844},{-0.682026,0.440981,-1.98441},{-0.440981,
0.440981,-2.18236},{0.440981,0.440981,-2.18236},{0.661471,0.440981,-2.00844},{0.682026,
0.440981,-1.98441},{-0.661471,0.468225,-1.98441},{-0.440981,0.661471,-2.00844},{-0.468225,
0.661471,-1.98441},{-0.22049,0.661471,-2.12196},{-5.96046e-08,0.661471,-2.15353},{0.22049,
0.661471,-2.12196},{0.440981,0.661471,-2.00844},{0.661471,0.468225,-1.98441},{0.468225,
0.661471,-1.98441},{-0.440981,0.682026,-1.98441},{-0.22049,0.784409,-1.98441},{-5.96046e-08,
0.814737,-1.98441},{0.22049,0.784409,-1.98441},{0.440981,0.682026,-1.98441},{-0.22049,
-0.881962,-1.78795},{-0.22049,-0.892601,-1.76392},{-0.247065,-0.881962,-1.76392},{-5.96046e-08,
-0.881962,-1.85432},{-5.96046e-08,-0.923346,-1.76392},{0.22049,-0.881962,-1.78795},
{0.22049,-0.892601,-1.76392},{0.247065,-0.881962,-1.76392},{-0.440981,-0.809721,-1.76392},
{-0.636079,-0.661471,-1.76392},{0.440981,-0.809721,-1.76392},{0.636079,-0.661471,-1.76392},
{-0.661471,-0.636079,-1.76392},{-0.809721,-0.440981,-1.76392},{0.661471,-0.636079,-1.76392},
{0.80972,-0.440981,-1.76392},{-0.881962,-0.22049,-1.78795},{-0.881962,-0.247065,-1.76392},
{-0.892601,-0.22049,-1.76392},{0.881962,-0.22049,-1.78795},{0.881962,-0.247065,-1.76392},
{0.892601,-0.22049,-1.76392},{-0.881962,-5.96046e-08,-1.85432},{-0.923346,-5.96046e-08,
-1.76392},{0.881962,-5.96046e-08,-1.85432},{0.923345,-5.96046e-08,-1.76392},{-0.881962,
0.22049,-1.78795},{-0.892601,0.22049,-1.76392},{0.881962,0.22049,-1.78795},{0.892601,
0.22049,-1.76392},{-0.881962,0.247065,-1.76392},{-0.809721,0.440981,-1.76392},{0.80972,
0.440981,-1.76392},{0.881962,0.247065,-1.76392},{-0.661471,0.636079,-1.76392},{-0.636079,
0.661471,-1.76392},{0.636079,0.661471,-1.76392},{0.661471,0.636079,-1.76392},{-0.440981,
0.80972,-1.76392},{-0.247065,0.881962,-1.76392},{-0.22049,0.881962,-1.78795},{-5.96046e-08,
0.881962,-1.85432},{0.22049,0.881962,-1.78795},{0.247065,0.881962,-1.76392},{0.440981,
0.80972,-1.76392},{-0.22049,0.892601,-1.76392},{-5.96046e-08,0.923345,-1.76392},{0.22049,
0.892601,-1.76392},{-0.22049,-0.932829,-1.54343},{-0.344046,-0.881962,-1.54343},{-5.96046e-08,
-0.961723,-1.54343},{0.22049,-0.932829,-1.54343},{0.344046,-0.881962,-1.54343},{-0.661471,
-0.661471,-1.65899},{-0.661471,-0.685143,-1.54343},{-0.685143,-0.661471,-1.54343},
{-0.440981,-0.846708,-1.54343},{0.440981,-0.846708,-1.54343},{0.661471,-0.661471,-1.65899},
{0.661471,-0.685143,-1.54343},{0.685143,-0.661471,-1.54343},{-0.846708,-0.440981,-1.54343},
{0.846708,-0.440981,-1.54343},{-0.881962,-0.344046,-1.54343},{-0.932829,-0.22049,-1.54343},
{0.881962,-0.344046,-1.54343},{0.932829,-0.22049,-1.54343},{-0.961723,-5.96046e-08,-1.54343},
{0.961722,-5.96046e-08,-1.54343},{-0.932829,0.22049,-1.54343},{0.932829,0.22049,-1.54343},
{-0.881962,0.344046,-1.54343},{-0.846708,0.440981,-1.54343},{0.846708,0.440981,-1.54343},
{0.881962,0.344046,-1.54343},{-0.685143,0.661471,-1.54343},{-0.661471,0.661471,-1.65899},
{0.661471,0.661471,-1.65899},{0.685143,0.661471,-1.54343},{-0.661471,0.685143,-1.54343},
{-0.440981,0.846708,-1.54343},{-0.344046,0.881962,-1.54343},{0.344046,0.881962,-1.54343},
{0.440981,0.846708,-1.54343},{0.661471,0.685143,-1.54343},{-0.22049,0.932829,-1.54343},
{-5.96046e-08,0.961722,-1.54343},{0.22049,0.932829,-1.54343},{-0.22049,-0.900773,-1.32294},
{-0.26446,-0.881962,-1.32294},{-5.96046e-08,-0.933523,-1.32294},{0.22049,-0.900773,
-1.32294},{0.26446,-0.881962,-1.32294},{-0.661471,-0.661471,-1.42859},{-0.440981,-0.81997,
-1.32294},{-0.645031,-0.661471,-1.32294},{0.440981,-0.81997,-1.32294},{0.661471,-0.661471,
-1.42859},{0.645031,-0.661471,-1.32294},{-0.661471,-0.645031,-1.32294},{-0.81997,-0.440981,
-1.32294},{0.661471,-0.645031,-1.32294},{0.81997,-0.440981,-1.32294},{-0.881962,-0.26446,
-1.32294},{-0.900773,-0.22049,-1.32294},{0.881962,-0.26446,-1.32294},{0.900773,-0.22049,
-1.32294},{-0.933523,-5.96046e-08,-1.32294},{0.933523,-5.96046e-08,-1.32294},{-0.900773,
0.22049,-1.32294},{0.900773,0.22049,-1.32294},{-0.881962,0.26446,-1.32294},{-0.81997,
0.440981,-1.32294},{0.81997,0.440981,-1.32294},{0.881962,0.26446,-1.32294},{-0.661471,
0.645031,-1.32294},{-0.661471,0.661471,-1.42859},{-0.645031,0.661471,-1.32294},{0.661471,
0.645031,-1.32294},{0.661471,0.661471,-1.42859},{0.645031,0.661471,-1.32294},{-0.440981,
0.81997,-1.32294},{-0.26446,0.881962,-1.32294},{0.26446,0.881962,-1.32294},{0.440981,
0.81997,-1.32294},{-0.22049,0.900773,-1.32294},{-5.96046e-08,0.933523,-1.32294},{0.22049,
0.900773,-1.32294},{-0.22049,-0.881962,-1.28688},{-5.96046e-08,-0.881962,-1.21647},
{0.22049,-0.881962,-1.28688},{-0.440981,-0.726104,-1.10245},{-0.522611,-0.661471,-1.10245},
{-0.22049,-0.823735,-1.10245},{-5.96046e-08,-0.849389,-1.10245},{0.22049,-0.823735,
-1.10245},{0.440981,-0.726104,-1.10245},{0.522611,-0.661471,-1.10245},{-0.661471,-0.522611,
-1.10245},{-0.726104,-0.440981,-1.10245},{0.661471,-0.522611,-1.10245},{0.726104,-0.440981,
-1.10245},{-0.881962,-0.22049,-1.28688},{-0.823735,-0.22049,-1.10245},{0.881962,-0.22049,
-1.28688},{0.823735,-0.22049,-1.10245},{-0.881962,-5.96046e-08,-1.21647},{-0.849389,
-5.96046e-08,-1.10245},{0.881962,-5.96046e-08,-1.21647},{0.849389,-5.96046e-08,-1.10245},
{-0.881962,0.22049,-1.28688},{-0.823735,0.22049,-1.10245},{0.881962,0.22049,-1.28688},
{0.823735,0.22049,-1.10245},{-0.726104,0.440981,-1.10245},{0.726104,0.440981,-1.10245},
{-0.661471,0.522611,-1.10245},{-0.522611,0.661471,-1.10245},{0.522611,0.661471,-1.10245},
{0.661471,0.522611,-1.10245},{-0.440981,0.726104,-1.10245},{-0.22049,0.823735,-1.10245},
{-0.22049,0.881962,-1.28688},{-5.96046e-08,0.849389,-1.10245},{-5.96046e-08,0.881962,
-1.21647},{0.22049,0.823735,-1.10245},{0.22049,0.881962,-1.28688},{0.440981,0.726104,
-1.10245},{-0.440981,-0.661471,-1.02648},{-0.22049,-0.671932,-0.881962},{-0.23897,-0.661471,
-0.881962},{-5.96046e-08,-0.71863,-0.881962},{0.22049,-0.671932,-0.881962},{0.440981,
-0.661471,-1.02648},{0.23897,-0.661471,-0.881962},{-0.661471,-0.440981,-1.02648},{-0.440981,
-0.559673,-0.881962},{-0.559673,-0.440981,-0.881962},{0.440981,-0.559673,-0.881962},
{0.661471,-0.440981,-1.02648},{0.559673,-0.440981,-0.881962},{-0.661471,-0.23897,-0.881962},
{-0.671932,-0.22049,-0.881962},{0.661471,-0.23897,-0.881962},{0.671932,-0.22049,-0.881962},
{-0.71863,-5.96046e-08,-0.881962},{0.71863,-5.96046e-08,-0.881962},{-0.671932,0.22049,
-0.881962},{0.671932,0.22049,-0.881962},{-0.661471,0.23897,-0.881962},{-0.661471,0.440981,
-1.02648},{-0.559673,0.440981,-0.881962},{0.661471,0.23897,-0.881962},{0.661471,0.440981,
-1.02648},{0.559673,0.440981,-0.881962},{-0.440981,0.559673,-0.881962},{-0.440981,0.661471,
-1.02648},{-0.23897,0.661471,-0.881962},{0.440981,0.559673,-0.881962},{0.440981,0.661471,
-1.02648},{0.23897,0.661471,-0.881962},{-0.22049,0.671932,-0.881962},{-5.96046e-08,0.71863,
-0.881962},{0.22049,0.671932,-0.881962},{-0.22049,-0.661471,-0.871868},{-5.96046e-08,-0.661471,
-0.824173},{0.22049,-0.661471,-0.871868},{-0.440981,-0.440981,-0.778629},{-0.22049,
-0.471724,-0.661471},{-0.262257,-0.440981,-0.661471},{-5.96046e-08,-0.532218,-0.661471},
{0.22049,-0.471724,-0.661471},{0.440981,-0.440981,-0.778629},{0.262257,-0.440981,-0.661471},
{-0.661471,-0.22049,-0.871868},{-0.440981,-0.262257,-0.661471},{-0.471724,-0.22049,
-0.661471},{0.440981,-0.262257,-0.661471},{0.661471,-0.22049,-0.871868},{0.471724,-0.22049,
-0.661471},{-0.661471,-5.96046e-08,-0.824173},{-0.532218,-5.96046e-08,-0.661471},{0.661471,
-5.96046e-08,-0.824173},{0.532218,-5.96046e-08,-0.661471},{-0.661471,0.22049,-0.871868},
{-0.471724,0.22049,-0.661471},{0.661471,0.22049,-0.871868},{0.471724,0.22049,-0.661471},
{-0.440981,0.262257,-0.661471},{-0.440981,0.440981,-0.778629},{-0.262257,0.440981,
-0.661471},{0.440981,0.262257,-0.661471},{0.440981,0.440981,-0.778629},{0.262257,0.440981,
-0.661471},{-0.22049,0.471724,-0.661471},{-0.22049,0.661471,-0.871868},{-5.96046e-08,0.532218,
-0.661471},{-5.96046e-08,0.661471,-0.824173},{0.22049,0.471724,-0.661471},{0.22049,
0.661471,-0.871868},{-0.22049,-0.440981,-0.635082},{-5.96046e-08,-0.440981,-0.579641},
{0.22049,-0.440981,-0.635082},{-0.440981,-0.22049,-0.635082},{-0.22049,-0.22049,-0.479359},
{-5.96046e-08,-0.254224,-0.440981},{-0.070155,-0.22049,-0.440981},{0.22049,-0.22049,
-0.479359},{0.0701548,-0.22049,-0.440981},{0.440981,-0.22049,-0.635082},{-0.440981,-5.96046e-08,
-0.579641},{-0.22049,-0.070155,-0.440981},{-0.254224,-5.96046e-08,-0.440981},{0.22049,
-0.070155,-0.440981},{0.440981,-5.96046e-08,-0.579641},{0.254224,-5.96046e-08,-0.440981},
{-0.440981,0.22049,-0.635082},{-0.22049,0.0701548,-0.440981},{-0.22049,0.22049,-0.479359},
{-0.070155,0.22049,-0.440981},{0.22049,0.0701548,-0.440981},{0.22049,0.22049,-0.479359},
{0.0701548,0.22049,-0.440981},{0.440981,0.22049,-0.635082},{-0.22049,0.440981,-0.635082},
{-5.96046e-08,0.254224,-0.440981},{-5.96046e-08,0.440981,-0.579641},{0.22049,0.440981,
-0.635082},{-5.96046e-08,-0.22049,-0.412831},{-0.22049,-5.96046e-08,-0.412831},{-5.96046e-08,
-5.96046e-08,-0.32557},{0.22049,-5.96046e-08,-0.412831},{-5.96046e-08,0.22049,-0.412831},
{-5.96046e-08,-0.22049,0.412831},{-5.96046e-08,-0.254224,0.440981},{-0.070155,-0.22049,
0.440981},{0.0701548,-0.22049,0.440981},{-0.22049,-5.96046e-08,0.412831},{-0.22049,
-0.070155,0.440981},{-0.254224,-5.96046e-08,0.440981},{-5.96046e-08,-5.96046e-08,0.32557},
{0.22049,-5.96046e-08,0.412831},{0.22049,-0.070155,0.440981},{0.254224,-5.96046e-08,0.440981},
{-0.22049,0.0701548,0.440981},{-5.96046e-08,0.22049,0.412831},{-0.070155,0.22049,
0.440981},{0.22049,0.0701548,0.440981},{0.0701548,0.22049,0.440981},{-5.96046e-08,0.254224,
0.440981},{-0.22049,-0.440981,0.635082},{-0.22049,-0.471724,0.661471},{-0.262257,-0.440981,
0.661471},{-5.96046e-08,-0.440981,0.579641},{-5.96046e-08,-0.532218,0.661471},{0.22049,
-0.440981,0.635082},{0.22049,-0.471724,0.661471},{0.262257,-0.440981,0.661471},{-0.440981,
-0.22049,0.635082},{-0.440981,-0.262257,0.661471},{-0.471724,-0.22049,0.661471},{-0.22049,
-0.22049,0.479358},{0.22049,-0.22049,0.479358},{0.440981,-0.22049,0.635082},{0.440981,
-0.262257,0.661471},{0.471724,-0.22049,0.661471},{-0.440981,-5.96046e-08,0.579641},
{-0.532218,-5.96046e-08,0.661471},{0.440981,-5.96046e-08,0.579641},{0.532218,-5.96046e-08,
0.661471},{-0.440981,0.22049,0.635082},{-0.471724,0.22049,0.661471},{-0.22049,0.22049,
0.479358},{0.22049,0.22049,0.479358},{0.440981,0.22049,0.635082},{0.471724,0.22049,
0.661471},{-0.440981,0.262257,0.661471},{-0.22049,0.440981,0.635082},{-0.262257,0.440981,
0.661471},{-5.96046e-08,0.440981,0.579641},{0.22049,0.440981,0.635082},{0.440981,0.262257,
0.661471},{0.262257,0.440981,0.661471},{-0.22049,0.471724,0.661471},{-5.96046e-08,0.532218,
0.661471},{0.22049,0.471724,0.661471},{-0.22049,-0.661471,0.871868},{-0.22049,-0.671932,
0.881962},{-0.23897,-0.661471,0.881962},{-5.96046e-08,-0.661471,0.824173},{-5.96046e-08,
-0.71863,0.881962},{0.22049,-0.661471,0.871868},{0.22049,-0.671932,0.881962},{0.23897,
-0.661471,0.881962},{-0.440981,-0.440981,0.778629},{-0.440981,-0.559673,0.881962},
{-0.559673,-0.440981,0.881962},{0.440981,-0.440981,0.778629},{0.440981,-0.559673,0.881962},
{0.559673,-0.440981,0.881962},{-0.661471,-0.22049,0.871868},{-0.661471,-0.23897,0.881962},
{-0.671932,-0.22049,0.881962},{0.661471,-0.22049,0.871868},{0.661471,-0.23897,0.881962},
{0.671932,-0.22049,0.881962},{-0.661471,-5.96046e-08,0.824173},{-0.71863,-5.96046e-08,
0.881962},{0.661471,-5.96046e-08,0.824173},{0.71863,-5.96046e-08,0.881962},{-0.661471,
0.22049,0.871868},{-0.671932,0.22049,0.881962},{0.661471,0.22049,0.871868},{0.671932,
0.22049,0.881962},{-0.661471,0.23897,0.881962},{-0.559673,0.440981,0.881962},{-0.440981,
0.440981,0.778629},{0.440981,0.440981,0.778629},{0.559673,0.440981,0.881962},{0.661471,
0.23897,0.881962},{-0.440981,0.559673,0.881962},{-0.23897,0.661471,0.881962},{-0.22049,
0.661471,0.871868},{-5.96046e-08,0.661471,0.824173},{0.22049,0.661471,0.871868},{0.23897,
0.661471,0.881962},{0.440981,0.559673,0.881962},{-0.22049,0.671932,0.881962},{-5.96046e-08,
0.71863,0.881962},{0.22049,0.671932,0.881962},{-0.440981,-0.661471,1.02647},{-0.440981,
-0.726104,1.10245},{-0.522611,-0.661471,1.10245},{-0.22049,-0.823735,1.10245},{-5.96046e-08,
-0.849389,1.10245},{0.22049,-0.823735,1.10245},{0.440981,-0.661471,1.02647},{0.440981,
-0.726104,1.10245},{0.522611,-0.661471,1.10245},{-0.661471,-0.440981,1.02647},{-0.661471,
-0.522611,1.10245},{-0.726104,-0.440981,1.10245},{0.661471,-0.440981,1.02647},{0.661471,
-0.522611,1.10245},{0.726104,-0.440981,1.10245},{-0.823735,-0.22049,1.10245},{0.823735,
-0.22049,1.10245},{-0.849389,-5.96046e-08,1.10245},{0.849389,-5.96046e-08,1.10245},
{-0.823735,0.22049,1.10245},{0.823735,0.22049,1.10245},{-0.726104,0.440981,1.10245},
{-0.661471,0.440981,1.02647},{0.661471,0.440981,1.02647},{0.726104,0.440981,1.10245},
{-0.661471,0.522611,1.10245},{-0.522611,0.661471,1.10245},{-0.440981,0.661471,1.02647},
{0.440981,0.661471,1.02647},{0.522611,0.661471,1.10245},{0.661471,0.522611,1.10245},
{-0.440981,0.726104,1.10245},{-0.22049,0.823735,1.10245},{-5.96046e-08,0.849389,1.10245},
{0.22049,0.823735,1.10245},{0.440981,0.726104,1.10245},{-0.22049,-0.881962,1.28688},
{-0.22049,-0.900773,1.32294},{-0.26446,-0.881962,1.32294},{-5.96046e-08,-0.881962,1.21647},
{-5.96046e-08,-0.933523,1.32294},{0.22049,-0.881962,1.28688},{0.22049,-0.900773,1.32294},
{0.26446,-0.881962,1.32294},{-0.440981,-0.81997,1.32294},{-0.645031,-0.661471,1.32294},
{0.440981,-0.81997,1.32294},{0.645031,-0.661471,1.32294},{-0.661471,-0.645031,1.32294},
{-0.81997,-0.440981,1.32294},{0.661471,-0.645031,1.32294},{0.81997,-0.440981,1.32294},
{-0.881962,-0.22049,1.28688},{-0.881962,-0.26446,1.32294},{-0.900773,-0.22049,1.32294},
{0.881962,-0.22049,1.28688},{0.881962,-0.26446,1.32294},{0.900773,-0.22049,1.32294},
{-0.881962,-5.96046e-08,1.21647},{-0.933523,-5.96046e-08,1.32294},{0.881962,-5.96046e-08,
1.21647},{0.933523,-5.96046e-08,1.32294},{-0.881962,0.22049,1.28688},{-0.900773,0.22049,
1.32294},{0.881962,0.22049,1.28688},{0.900773,0.22049,1.32294},{-0.881962,0.26446,
1.32294},{-0.81997,0.440981,1.32294},{0.81997,0.440981,1.32294},{0.881962,0.26446,
1.32294},{-0.661471,0.645031,1.32294},{-0.645031,0.661471,1.32294},{0.645031,0.661471,
1.32294},{0.661471,0.645031,1.32294},{-0.440981,0.81997,1.32294},{-0.26446,0.881962,
1.32294},{-0.22049,0.881962,1.28688},{-5.96046e-08,0.881962,1.21647},{0.22049,0.881962,
1.28688},{0.26446,0.881962,1.32294},{0.440981,0.81997,1.32294},{-0.22049,0.900773,1.32294},
{-5.96046e-08,0.933523,1.32294},{0.22049,0.900773,1.32294},{-0.22049,-0.932829,1.54343},
{-0.344046,-0.881962,1.54343},{-5.96046e-08,-0.961723,1.54343},{0.22049,-0.932829,1.54343},
{0.344046,-0.881962,1.54343},{-0.661471,-0.661471,1.42859},{-0.661471,-0.685143,1.54343},
{-0.685143,-0.661471,1.54343},{-0.440981,-0.846708,1.54343},{0.440981,-0.846708,1.54343},
{0.661471,-0.661471,1.42859},{0.661471,-0.685143,1.54343},{0.685143,-0.661471,1.54343},
{-0.846708,-0.440981,1.54343},{0.846708,-0.440981,1.54343},{-0.881962,-0.344046,1.54343},
{-0.932829,-0.22049,1.54343},{0.881962,-0.344046,1.54343},{0.932829,-0.22049,1.54343},
{-0.961723,-5.96046e-08,1.54343},{0.961722,-5.96046e-08,1.54343},{-0.932829,0.22049,
1.54343},{0.932829,0.22049,1.54343},{-0.881962,0.344046,1.54343},{-0.846708,0.440981,
1.54343},{0.846708,0.440981,1.54343},{0.881962,0.344046,1.54343},{-0.685143,0.661471,
1.54343},{-0.661471,0.661471,1.42859},{0.661471,0.661471,1.42859},{0.685143,0.661471,
1.54343},{-0.661471,0.685143,1.54343},{-0.440981,0.846708,1.54343},{-0.344046,0.881962,
1.54343},{0.344046,0.881962,1.54343},{0.440981,0.846708,1.54343},{0.661471,0.685143,
1.54343},{-0.22049,0.932829,1.54343},{-5.96046e-08,0.961722,1.54343},{0.22049,0.932829,
1.54343},{-0.22049,-0.892601,1.76392},{-0.247065,-0.881962,1.76392},{-5.96046e-08,-0.923346,
1.76392},{0.22049,-0.892601,1.76392},{0.247065,-0.881962,1.76392},{-0.661471,-0.661471,
1.65899},{-0.440981,-0.809721,1.76392},{-0.636079,-0.661471,1.76392},{0.440981,-0.809721,
1.76392},{0.661471,-0.661471,1.65899},{0.636079,-0.661471,1.76392},{-0.661471,-0.636079,
1.76392},{-0.809721,-0.440981,1.76392},{0.661471,-0.636079,1.76392},{0.80972,-0.440981,
1.76392},{-0.881962,-0.247065,1.76392},{-0.892601,-0.22049,1.76392},{0.881962,-0.247065,
1.76392},{0.892601,-0.22049,1.76392},{-0.923346,-5.96046e-08,1.76392},{0.923345,-5.96046e-08,
1.76392},{-0.892601,0.22049,1.76392},{0.892601,0.22049,1.76392},{-0.881962,0.247065,
1.76392},{-0.809721,0.440981,1.76392},{0.80972,0.440981,1.76392},{0.881962,0.247065,
1.76392},{-0.661471,0.636079,1.76392},{-0.661471,0.661471,1.65899},{-0.636079,0.661471,
1.76392},{0.661471,0.636079,1.76392},{0.661471,0.661471,1.65899},{0.636079,0.661471,
1.76392},{-0.440981,0.80972,1.76392},{-0.247065,0.881962,1.76392},{0.247065,0.881962,
1.76392},{0.440981,0.80972,1.76392},{-0.22049,0.892601,1.76392},{-5.96046e-08,0.923345,
1.76392},{0.22049,0.892601,1.76392},{-0.22049,-0.881962,1.78795},{-5.96046e-08,-0.881962,
1.85432},{0.22049,-0.881962,1.78795},{-0.440981,-0.682026,1.98441},{-0.468225,-0.661471,
1.98441},{-0.22049,-0.784409,1.98441},{-5.96046e-08,-0.814737,1.98441},{0.22049,-0.784409,
1.98441},{0.440981,-0.682026,1.98441},{0.468225,-0.661471,1.98441},{-0.661471,-0.468225,
1.98441},{-0.682026,-0.440981,1.98441},{0.661471,-0.468225,1.98441},{0.682026,-0.440981,
1.98441},{-0.881962,-0.22049,1.78795},{-0.784409,-0.22049,1.98441},{0.881962,-0.22049,
1.78795},{0.784409,-0.22049,1.98441},{-0.881962,-5.96046e-08,1.85432},{-0.814737,-5.96046e-08,
1.98441},{0.881962,-5.96046e-08,1.85432},{0.814737,-5.96046e-08,1.98441},{-0.881962,
0.22049,1.78795},{-0.784409,0.22049,1.98441},{0.881962,0.22049,1.78795},{0.784409,0.22049,
1.98441},{-0.682026,0.440981,1.98441},{0.682026,0.440981,1.98441},{-0.661471,0.468225,
1.98441},{-0.468225,0.661471,1.98441},{0.468225,0.661471,1.98441},{0.661471,0.468225,
1.98441},{-0.440981,0.682026,1.98441},{-0.22049,0.784409,1.98441},{-0.22049,0.881962,
1.78795},{-5.96046e-08,0.814737,1.98441},{-5.96046e-08,0.881962,1.85432},{0.22049,
0.784409,1.98441},{0.22049,0.881962,1.78795},{0.440981,0.682026,1.98441},{-0.440981,
-0.661471,2.00844},{-0.22049,-0.661471,2.12196},{-5.96046e-08,-0.661471,2.15353},
{0.22049,-0.661471,2.12196},{0.440981,-0.661471,2.00844},{-0.661471,-0.440981,2.00844},
{-0.440981,-0.440981,2.18236},{-0.22049,-0.542998,2.2049},{-0.382006,-0.440981,2.2049},
{-5.96046e-08,-0.586646,2.2049},{0.22049,-0.542998,2.2049},{0.440981,-0.440981,2.18236},
{0.382006,-0.440981,2.2049},{0.661471,-0.440981,2.00844},{-0.661471,-0.22049,2.12196},
{-0.440981,-0.382006,2.2049},{-0.542998,-0.22049,2.2049},{0.440981,-0.382006,2.2049},
{0.661471,-0.22049,2.12196},{0.542997,-0.22049,2.2049},{-0.661471,-5.96046e-08,2.15353},
{-0.586646,-5.96046e-08,2.2049},{0.661471,-5.96046e-08,2.15353},{0.586646,-5.96046e-08,
2.2049},{-0.661471,0.22049,2.12196},{-0.542998,0.22049,2.2049},{0.661471,0.22049,2.12196},
{0.542997,0.22049,2.2049},{-0.661471,0.440981,2.00844},{-0.440981,0.382006,2.2049},
{-0.440981,0.440981,2.18236},{-0.382006,0.440981,2.2049},{0.440981,0.382006,2.2049},
{0.440981,0.440981,2.18236},{0.382006,0.440981,2.2049},{0.661471,0.440981,2.00844},
{-0.440981,0.661471,2.00844},{-0.22049,0.542997,2.2049},{-0.22049,0.661471,2.12196},
{-5.96046e-08,0.586646,2.2049},{-5.96046e-08,0.661471,2.15353},{0.22049,0.542997,2.2049},
{0.22049,0.661471,2.12196},{0.440981,0.661471,2.00844},{-0.22049,-0.440981,2.27076},
{-5.96046e-08,-0.440981,2.2982},{0.22049,-0.440981,2.27076},{-0.440981,-0.22049,2.27076},
{-0.22049,-0.22049,2.34764},{-5.96046e-08,-0.22049,2.36985},{0.22049,-0.22049,2.34764},
{0.440981,-0.22049,2.27076},{-0.440981,-5.96046e-08,2.2982},{-0.22049,-5.96046e-08,2.36985},
{-5.96046e-08,-5.96046e-08,2.39053},{0.22049,-5.96046e-08,2.36985},{0.440981,-5.96046e-08,
2.2982},{-0.440981,0.22049,2.27076},{-0.22049,0.22049,2.34764},{-5.96046e-08,0.22049,
2.36985},{0.22049,0.22049,2.34764},{0.440981,0.22049,2.27076},{-0.22049,0.440981,2.27076},
{-5.96046e-08,0.440981,2.2982},{0.22049,0.440981,2.27076}
};
	int connect<NEportLevels={2,0}>[4368] = {
   0,1,2,3,4,1,3,1,0,5,6,4,5,4,3,7,6,5,8,9,10,11,0,2,11,2,9,11,9,8,12,3,
0,12,0,11,13,5,3,13,3,12,14,15,7,14,7,5,14,5,13,16,15,14,17,8,10,17,10,
18,19,11,8,19,8,17,20,12,11,20,11,19,21,13,12,21,12,20,22,14,13,22,13,21,
23,16,14,23,14,22,24,17,18,24,18,25,26,19,17,26,17,24,27,20,19,27,19,26,
28,21,20,28,20,27,29,22,21,29,21,28,30,23,22,30,22,29,31,24,25,32,26,24,
32,24,31,32,31,33,34,27,26,34,26,32,35,28,27,35,27,34,36,29,28,36,28,35,
36,35,37,36,30,29,38,32,33,39,34,32,39,32,38,40,35,34,40,34,39,40,37,35,
41,42,43,44,45,42,44,42,41,46,47,45,46,45,44,48,49,47,48,47,46,50,51,49,
50,49,48,52,51,50,53,54,55,56,41,43,56,43,54,56,54,53,2,1,44,2,44,41,2,
41,56,1,4,46,1,46,44,4,6,48,4,48,46,6,7,57,6,57,50,6,50,48,58,59,52,58,
52,50,58,50,57,60,59,58,61,53,55,61,55,62,10,9,56,10,56,53,10,53,61,9,
2,56,7,15,57,15,16,63,15,63,58,15,58,57,64,60,58,64,58,63,65,61,62,65,62,
66,18,10,61,18,61,65,16,23,67,16,67,63,68,64,63,68,63,67,69,65,66,69,66,
70,25,18,65,25,65,69,23,30,71,23,71,67,72,68,67,72,67,71,73,69,70,73,70,
74,31,25,69,31,69,73,31,73,75,33,31,75,36,37,76,30,36,76,30,76,77,30,77,
71,78,72,71,78,71,77,79,73,74,80,75,73,80,73,79,80,79,81,38,33,75,38,75,
80,38,80,82,39,38,82,39,82,83,40,39,83,40,83,84,37,40,84,37,84,85,37,85,
76,86,77,76,86,76,85,86,85,87,86,78,77,88,80,81,89,82,80,89,80,88,90,83,
82,90,82,89,91,84,83,91,83,90,92,85,84,92,84,91,92,87,85,93,94,95,96,97,
94,96,94,93,98,99,97,98,97,96,100,99,98,43,42,101,43,101,102,42,45,93,42,
93,95,42,95,101,45,47,96,45,96,93,47,49,98,47,98,96,49,51,103,49,103,100,
49,100,98,51,52,104,51,104,103,55,54,105,55,105,106,54,43,102,54,102,105,
52,59,107,52,107,104,59,60,108,59,108,107,109,110,111,62,55,106,62,106,110,
62,110,109,60,64,112,60,112,113,60,113,108,114,113,112,115,109,111,115,111,
116,66,62,109,66,109,115,64,68,117,64,117,112,118,114,112,118,112,117,119,
115,116,119,116,120,70,66,115,70,115,119,68,72,121,68,121,117,122,118,117,
122,117,121,123,119,120,74,70,119,74,119,123,74,123,124,72,78,125,72,125,
126,72,126,121,126,122,121,79,74,124,79,124,127,81,79,127,81,127,128,86,
87,129,86,129,130,78,86,130,78,130,125,88,81,128,88,128,131,89,88,131,89,
131,132,89,132,133,90,89,133,90,133,134,91,90,134,91,134,135,92,91,135,92,
135,136,92,136,137,87,92,137,87,137,129,138,133,132,139,134,133,139,133,
138,140,135,134,140,134,139,140,136,135,95,94,141,95,141,142,94,97,143,94,
143,141,97,99,144,97,144,143,99,100,145,99,145,144,146,147,148,102,101,149,
102,149,147,102,147,146,101,95,142,101,142,149,100,103,150,100,150,145,103,
104,151,103,151,152,103,152,150,153,152,151,106,105,146,106,146,148,106,
148,154,105,102,146,104,107,151,107,108,155,107,155,153,107,153,151,111,
110,156,111,156,157,110,106,154,110,154,156,108,113,158,108,158,155,113,
114,159,113,159,158,116,111,157,116,157,160,114,118,161,114,161,159,120,
116,160,120,160,162,118,122,163,118,163,161,123,120,162,123,162,164,124,
123,164,124,164,165,126,125,166,126,166,167,122,126,167,122,167,163,127,
124,165,127,165,168,127,168,169,128,127,169,130,129,170,125,130,170,125,
170,171,125,171,166,172,169,168,131,128,169,131,169,172,131,172,173,132,
131,173,132,173,174,137,136,175,137,175,176,129,137,176,129,176,177,129,
177,170,177,171,170,138,132,174,138,174,178,139,138,178,139,178,179,140,
139,179,140,179,180,136,140,180,136,180,175,142,141,181,142,181,182,141,
143,183,141,183,181,143,144,184,143,184,183,144,145,185,144,185,184,148,
147,186,147,149,187,147,187,188,147,188,186,149,142,182,149,182,187,145,
150,189,145,189,185,150,152,190,150,190,191,150,191,189,152,153,190,154,
148,186,154,186,192,154,192,193,188,192,186,190,194,191,153,155,195,153,
195,194,153,194,190,157,156,196,157,196,197,156,154,193,156,193,196,155,
158,198,155,198,195,158,159,199,158,199,198,160,157,197,160,197,200,159,
161,201,159,201,199,162,160,200,162,200,202,161,163,203,161,203,201,164,
162,202,164,202,204,165,164,204,165,204,205,167,166,206,167,206,207,163,
167,207,163,207,203,168,165,205,168,205,208,168,208,209,208,210,209,211,
212,213,166,171,212,166,212,211,166,211,206,172,168,209,173,172,209,173,
209,210,173,210,214,174,173,214,174,214,215,176,175,216,176,216,217,177,
176,217,177,217,213,177,213,212,171,177,212,178,174,215,178,215,218,179,
178,218,179,218,219,180,179,219,180,219,220,175,180,220,175,220,216,182,
181,221,181,183,222,181,222,221,183,184,223,183,223,222,184,185,223,188,
187,224,188,224,225,187,182,221,187,221,226,187,226,224,222,227,226,222,
226,221,223,228,227,223,227,222,185,189,229,185,229,228,185,228,223,189,
191,230,189,230,229,193,192,231,193,231,232,192,188,225,192,225,231,191,
194,233,191,233,230,194,195,234,194,234,233,197,196,235,196,193,232,196,
232,236,196,236,235,195,198,237,195,237,238,195,238,234,198,199,237,200,
197,235,200,235,239,236,240,239,236,239,235,237,241,242,237,242,238,199,
201,241,199,241,237,202,200,239,202,239,243,240,244,243,240,243,239,241,
245,246,241,246,242,201,203,245,201,245,241,204,202,243,205,204,243,205,
243,244,205,244,247,207,206,248,207,248,246,207,246,245,203,207,245,208,
205,247,208,247,249,210,208,249,210,249,250,211,213,251,211,251,252,206,
211,252,206,252,248,214,210,250,214,250,253,215,214,253,215,253,254,215,
254,255,256,257,255,256,255,254,258,259,257,258,257,256,217,216,259,217,
259,258,217,258,260,213,217,260,213,260,251,218,215,255,219,218,255,219,
255,257,220,219,257,220,257,259,216,220,259,225,224,261,224,226,262,224,
262,263,224,263,261,226,227,264,226,264,262,227,228,265,227,265,264,228,
229,266,228,266,267,228,267,265,229,230,266,232,231,268,231,225,261,231,
261,269,231,269,268,269,270,268,263,269,261,266,271,267,230,233,272,230,
272,273,230,273,266,273,271,266,233,234,272,236,232,268,236,268,274,236,
274,275,270,274,268,272,276,273,234,238,277,234,277,276,234,276,272,240,
236,275,240,275,278,238,242,279,238,279,277,244,240,278,244,278,280,242,
246,281,242,281,279,247,244,280,247,280,282,247,282,283,282,284,283,285,
286,287,246,248,286,246,286,285,246,285,281,249,247,283,250,249,283,250,
283,284,250,284,289,284,288,289,288,290,289,291,292,293,252,251,292,252,
292,291,252,291,286,291,287,286,248,252,286,253,250,289,254,253,289,254,
289,290,254,290,294,256,254,294,256,294,295,258,256,295,258,295,296,260,
258,296,260,296,293,260,293,292,251,260,292,263,262,297,262,264,298,262,
298,297,264,265,299,264,299,298,265,267,299,270,269,300,269,263,297,269,
297,301,269,301,300,301,302,300,298,303,301,298,301,297,299,304,303,299,
303,298,267,271,305,267,305,306,267,306,299,306,304,299,271,273,305,275,
274,307,274,270,300,274,300,308,274,308,307,308,309,307,302,308,300,305,
310,306,273,276,311,273,311,312,273,312,305,312,310,305,276,277,311,278,
275,307,278,307,313,309,314,313,309,313,307,311,315,316,311,316,312,277,
279,315,277,315,311,280,278,313,280,313,317,314,318,317,314,317,313,315,
319,320,315,320,316,279,281,319,279,319,315,282,280,317,284,282,317,284,
317,318,284,318,322,318,321,322,321,323,322,324,325,326,285,287,325,285,
325,324,285,324,319,324,320,319,281,285,319,288,284,322,290,288,322,290,
322,323,290,323,328,323,327,328,329,330,328,329,328,327,331,332,330,331,
330,329,291,293,332,291,332,331,291,331,325,331,326,325,287,291,325,294,
290,328,295,294,328,295,328,330,296,295,330,296,330,332,293,296,332,302,
301,333,301,303,334,301,334,333,303,304,335,303,335,334,304,306,335,309,
308,336,308,302,333,308,333,337,308,337,336,334,338,339,334,339,337,334,
337,333,335,340,341,335,341,338,335,338,334,306,310,342,306,342,340,306,
340,335,310,312,342,314,309,336,314,336,343,337,344,345,337,345,343,337,
343,336,339,344,337,340,346,341,342,347,348,342,348,346,342,346,340,312,
316,347,312,347,342,318,314,343,318,343,349,350,351,349,350,349,343,350,
343,345,350,352,351,353,354,355,347,356,354,347,354,353,347,353,348,316,
320,356,316,356,347,321,318,349,323,321,349,323,349,351,323,351,357,358,
359,357,358,357,351,358,351,352,354,360,359,354,359,358,354,358,355,324,
326,360,324,360,354,324,354,356,320,324,356,327,323,357,329,327,357,329,
357,359,331,329,359,331,359,360,326,331,360,339,338,361,338,341,361,345,
344,362,344,339,361,344,361,363,344,363,362,341,346,364,341,364,363,341,
363,361,346,348,364,350,345,362,352,350,362,352,362,363,352,363,365,353,
355,365,353,365,363,353,363,364,348,353,364,358,352,365,355,358,365,366,
367,368,369,367,366,370,371,372,373,366,368,373,368,371,373,371,370,374,
375,369,374,369,366,374,366,373,376,375,374,377,370,372,378,373,370,378,
370,377,378,377,379,380,374,373,380,373,378,380,378,381,380,376,374,382,
378,379,382,381,378,383,384,385,386,387,384,386,384,383,388,389,387,388,
387,386,390,389,388,391,392,393,394,383,385,394,385,392,394,392,391,368,
367,386,368,386,383,368,383,394,367,369,395,367,395,388,367,388,386,396,
397,390,396,390,388,396,388,395,398,397,396,399,391,393,399,393,400,372,
371,394,372,394,391,372,391,399,371,368,394,369,375,395,375,376,401,375,
401,396,375,396,395,402,398,396,402,396,401,403,399,400,403,400,404,377,
372,399,377,399,403,377,403,405,379,377,405,380,381,406,376,380,406,376,
406,407,376,407,401,408,402,401,408,401,407,409,403,404,410,405,403,410,
403,409,410,409,411,382,379,405,382,405,410,382,410,412,381,382,412,381,
412,413,381,413,406,414,407,406,414,406,413,414,413,415,414,408,407,416,
410,411,417,412,410,417,410,416,418,413,412,418,412,417,418,415,413,419,
420,421,422,423,420,422,420,419,424,425,423,424,423,422,426,425,424,427,
428,429,385,384,419,385,419,421,385,421,427,421,428,427,384,387,422,384,
422,419,387,389,424,387,424,422,389,390,430,389,430,431,389,431,424,431,
426,424,432,431,430,433,434,435,393,392,427,393,427,429,393,429,433,429,
434,433,392,385,427,390,397,430,397,398,436,397,436,437,397,437,430,437,
432,430,438,437,436,439,433,435,439,435,440,400,393,433,400,433,439,398,
402,441,398,441,436,442,438,436,442,436,441,443,439,440,443,440,444,404,
400,439,404,439,443,402,408,445,402,445,441,446,442,441,446,441,445,447,
443,444,409,404,443,409,443,447,409,447,449,447,448,449,411,409,449,414,
415,450,408,414,450,408,450,451,408,451,445,451,452,445,452,446,445,453,
449,448,416,411,449,416,449,453,416,453,455,453,454,455,417,416,455,417,
455,456,418,417,456,418,456,457,415,418,457,415,457,458,415,458,450,458,
459,450,459,451,450,460,455,454,461,456,455,461,455,460,462,457,456,462,
456,461,462,458,457,463,464,465,421,420,466,421,466,464,421,464,463,420,
423,467,420,467,466,423,425,468,423,468,467,425,426,469,425,469,470,425,
470,468,471,470,469,472,473,474,429,428,463,429,463,465,429,465,472,465,
473,472,428,421,463,426,431,469,431,432,475,431,475,476,431,476,469,476,
471,469,477,476,475,435,434,472,435,472,474,435,474,478,434,429,472,432,
437,475,437,438,479,437,479,477,437,477,475,440,435,478,440,478,480,438,
442,481,438,481,479,444,440,480,444,480,482,442,446,483,442,483,481,447,
444,482,447,482,484,447,484,485,448,447,485,452,451,486,446,452,486,446,
486,487,446,487,483,488,485,484,453,448,485,453,485,488,453,488,490,488,
489,490,454,453,490,459,458,491,451,459,491,451,491,492,451,492,486,492,
493,486,493,487,486,494,490,489,460,454,490,460,490,494,460,494,495,461,
460,495,461,495,496,462,461,496,462,496,497,458,462,497,458,497,498,458,
498,491,498,492,491,499,500,501,502,503,500,502,500,499,504,505,503,504,
503,502,506,505,504,465,464,507,465,507,508,464,466,499,464,499,501,464,
501,507,466,467,502,466,502,499,467,468,504,467,504,502,468,470,509,468,
509,506,468,506,504,470,471,510,470,510,509,474,473,511,474,511,512,473,
465,508,473,508,511,471,476,513,471,513,510,476,477,514,476,514,513,515,
516,517,478,474,512,478,512,516,478,516,515,477,479,518,477,518,519,477,
519,514,520,519,518,521,515,517,521,517,522,480,478,515,480,515,521,479,
481,523,479,523,518,524,520,518,524,518,523,525,521,522,525,522,526,482,
480,521,482,521,525,481,483,527,481,527,523,528,524,523,528,523,527,529,
525,526,484,482,525,484,525,529,484,529,530,483,487,531,483,531,532,483,
532,527,532,528,527,488,484,530,488,530,533,489,488,533,489,533,534,493,
492,535,493,535,536,487,493,536,487,536,531,494,489,534,494,534,537,495,
494,537,495,537,538,495,538,539,496,495,539,496,539,540,497,496,540,497,
540,541,498,497,541,498,541,542,498,542,543,492,498,543,492,543,535,544,
539,538,545,540,539,545,539,544,546,541,540,546,540,545,546,542,541,501,
500,547,501,547,548,500,503,549,500,549,547,503,505,550,503,550,549,505,
506,551,505,551,550,552,553,554,508,507,555,508,555,553,508,553,552,507,
501,548,507,548,555,506,509,556,506,556,551,509,510,557,509,557,558,509,
558,556,559,558,557,512,511,552,512,552,554,512,554,560,511,508,552,510,
513,557,513,514,561,513,561,559,513,559,557,517,516,562,517,562,563,516,
512,560,516,560,562,514,519,564,514,564,561,519,520,565,519,565,564,522,
517,563,522,563,566,520,524,567,520,567,565,526,522,566,526,566,568,524,
528,569,524,569,567,529,526,568,529,568,570,530,529,570,530,570,571,532,
531,572,532,572,573,528,532,573,528,573,569,533,530,571,533,571,574,533,
574,575,534,533,575,536,535,576,531,536,576,531,576,577,531,577,572,578,
575,574,537,534,575,537,575,578,537,578,579,538,537,579,538,579,580,543,
542,581,543,581,582,535,543,582,535,582,583,535,583,576,583,577,576,544,
538,580,544,580,584,545,544,584,545,584,585,546,545,585,546,585,586,542,
546,586,542,586,581,548,547,587,548,587,588,547,549,589,547,589,587,549,
550,590,549,590,589,550,551,591,550,591,590,554,553,592,553,555,593,553,
593,594,553,594,592,555,548,588,555,588,593,551,556,595,551,595,591,556,
558,596,556,596,597,556,597,595,558,559,596,560,554,592,560,592,598,560,
598,599,594,598,592,596,600,597,559,561,601,559,601,600,559,600,596,563,
562,602,563,602,603,562,560,599,562,599,602,561,564,604,561,604,601,564,
565,605,564,605,604,566,563,603,566,603,606,565,567,607,565,607,605,568,
566,606,568,606,608,567,569,609,567,609,607,570,568,608,570,608,610,571,
570,610,571,610,611,573,572,612,573,612,613,569,573,613,569,613,609,574,
571,611,574,611,614,574,614,615,614,616,615,617,618,619,572,577,618,572,
618,617,572,617,612,578,574,615,579,578,615,579,615,616,579,616,620,580,
579,620,580,620,621,582,581,622,582,622,623,583,582,623,583,623,619,583,
619,618,577,583,618,584,580,621,584,621,624,585,584,624,585,624,625,586,
585,625,586,625,626,581,586,626,581,626,622,588,587,627,587,589,628,587,
628,627,589,590,629,589,629,628,590,591,629,594,593,630,594,630,631,593,
588,627,593,627,632,593,632,630,628,633,632,628,632,627,629,634,633,629,
633,628,591,595,635,591,635,634,591,634,629,595,597,636,595,636,635,599,
598,637,599,637,638,598,594,631,598,631,637,597,600,639,597,639,636,600,
601,640,600,640,639,603,602,641,602,599,638,602,638,642,602,642,641,601,
604,643,601,643,644,601,644,640,604,605,643,606,603,641,606,641,645,642,
646,645,642,645,641,643,647,648,643,648,644,605,607,647,605,647,643,608,
606,645,608,645,649,646,650,649,646,649,645,647,651,652,647,652,648,607,
609,651,607,651,647,610,608,649,611,610,649,611,649,650,611,650,653,613,
612,654,613,654,652,613,652,651,609,613,651,614,611,653,614,653,655,616,
614,655,616,655,656,617,619,657,617,657,658,612,617,658,612,658,654,620,
616,656,620,656,659,621,620,659,621,659,660,621,660,661,662,663,661,662,
661,660,664,665,663,664,663,662,623,622,665,623,665,664,623,664,666,619,
623,666,619,666,657,624,621,661,625,624,661,625,661,663,626,625,663,626,
663,665,622,626,665,631,630,667,630,632,668,630,668,667,632,633,669,632,
669,668,633,634,670,633,670,669,634,635,671,634,671,670,635,636,671,638,
637,672,637,631,667,637,667,673,637,673,672,668,674,675,668,675,673,668,
673,667,669,676,674,669,674,668,670,677,676,670,676,669,671,678,679,671,
679,677,671,677,670,636,639,680,636,680,678,636,678,671,639,640,680,642,
638,672,642,672,681,673,682,683,673,683,681,673,681,672,675,682,673,678,
684,679,680,685,686,680,686,684,680,684,678,640,644,685,640,685,680,646,
642,681,646,681,687,683,688,687,683,687,681,685,689,690,685,690,686,644,
648,689,644,689,685,650,646,687,650,687,691,688,692,691,688,691,687,689,
693,694,689,694,690,648,652,693,648,693,689,653,650,691,653,691,695,696,
697,695,696,695,691,696,691,692,696,698,697,699,700,701,693,702,700,693,
700,699,693,699,694,652,654,702,652,702,693,655,653,695,656,655,695,656,
695,697,656,697,703,704,705,703,704,703,697,704,697,698,706,707,705,706,
705,704,708,709,707,708,707,706,700,710,709,700,709,708,700,708,701,658,
657,710,658,710,700,658,700,702,654,658,702,659,656,703,660,659,703,660,
703,705,662,660,705,662,705,707,664,662,707,664,707,709,666,664,709,666,
709,710,657,666,710,675,674,711,674,676,712,674,712,711,676,677,713,676,
713,712,677,679,713,683,682,714,682,675,711,682,711,715,682,715,714,712,
716,715,712,715,711,713,717,716,713,716,712,679,684,718,679,718,717,679,
717,713,684,686,718,688,683,714,688,714,719,715,720,719,715,719,714,716,
721,720,716,720,715,717,722,721,717,721,716,718,723,722,718,722,717,686,
690,723,686,723,718,692,688,719,692,719,724,720,725,724,720,724,719,721,
726,725,721,725,720,722,727,726,722,726,721,723,728,727,723,727,722,690,
694,728,690,728,723,696,692,724,698,696,724,698,724,725,698,725,729,726,
730,729,726,729,725,727,731,730,727,730,726,699,701,731,699,731,727,699,
727,728,694,699,728,704,698,729,706,704,729,706,729,730,708,706,730,708,
730,731,701,708,731
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	  xform {
	    mat = {
	      {1.0, 0.0, 0.0, 0.0},
	      {0.0, 1.0, 0.0, 0.0},
	      {0.0, 0.0, 1.0, 0.0},
	      {0.0, 0.0, 0.0, 1.7}
	    };
	  };
	};
      };
      group dn_mesh {
	float coord<NEportLevels={2,0}>[528][3] = {
   {-0.440981,-1.32294,-0.242121},{-0.440981,-1.37256,-0.22049},{-0.575884,-1.32294,
-0.22049},{-0.22049,-1.32294,-0.257364},{-0.22049,-1.41962,-0.22049},{-5.96046e-08,-1.32294,
-0.261288},{-5.96046e-08,-1.43557,-0.22049},{0.22049,-1.32294,-0.257364},{0.22049,
-1.41962,-0.22049},{0.440981,-1.32294,-0.242121},{0.440981,-1.37256,-0.22049},{0.575884,
-1.32294,-0.22049},{-0.881962,-1.10245,-0.235836},{-0.881962,-1.14263,-0.22049},{-0.931392,
-1.10245,-0.22049},{-0.661471,-1.10245,-0.267369},{-0.661471,-1.27938,-0.22049},{-0.440981,
-1.10245,-0.272436},{-0.22049,-1.10245,-0.268318},{-5.96046e-08,-1.10245,-0.265659},
{0.22049,-1.10245,-0.268318},{0.440981,-1.10245,-0.272436},{0.661471,-1.10245,-0.267369},
{0.661471,-1.27938,-0.22049},{0.881962,-1.10245,-0.235836},{0.881962,-1.14263,-0.22049},
{0.931392,-1.10245,-0.22049},{-1.10245,-0.881962,-0.235836},{-1.10245,-0.931392,-0.22049},
{-1.14263,-0.881962,-0.22049},{-0.881962,-0.881962,-0.271135},{-0.661471,-0.881962,
-0.265659},{-0.440981,-0.881962,-0.241307},{-0.22049,-0.906325,-0.22049},{-0.265489,
-0.881962,-0.22049},{-5.96046e-08,-0.945983,-0.22049},{0.22049,-0.906325,-0.22049},
{0.440981,-0.881962,-0.241307},{0.265488,-0.881962,-0.22049},{0.661471,-0.881962,-0.265659},
{0.881962,-0.881962,-0.271135},{1.10245,-0.881962,-0.235836},{1.10245,-0.931392,-0.22049},
{1.14263,-0.881962,-0.22049},{-1.10245,-0.661471,-0.267369},{-1.27938,-0.661471,-0.22049},
{-0.881962,-0.661471,-0.265659},{-0.661471,-0.661471,-0.225167},{-0.440981,-0.826536,
-0.22049},{-0.645759,-0.661471,-0.22049},{0.440981,-0.826536,-0.22049},{0.661471,-0.661471,
-0.225167},{0.645759,-0.661471,-0.22049},{0.881962,-0.661471,-0.265659},{1.10245,-0.661471,
-0.267369},{1.27938,-0.661471,-0.22049},{-1.32294,-0.440981,-0.242121},{-1.32294,-0.575884,
-0.22049},{-1.37256,-0.440981,-0.22049},{-1.10245,-0.440981,-0.272436},{-0.881962,-0.440981,
-0.241307},{-0.661471,-0.645759,-0.22049},{-0.826536,-0.440981,-0.22049},{0.661471,
-0.645759,-0.22049},{0.881962,-0.440981,-0.241307},{0.826536,-0.440981,-0.22049},{1.10245,
-0.440981,-0.272436},{1.32294,-0.440981,-0.242121},{1.32294,-0.575884,-0.22049},{1.37256,
-0.440981,-0.22049},{-1.32294,-0.22049,-0.257364},{-1.41962,-0.22049,-0.22049},{-1.10245,
-0.22049,-0.268318},{-0.881962,-0.265489,-0.22049},{-0.906325,-0.22049,-0.22049},{0.881962,
-0.265489,-0.22049},{1.10245,-0.22049,-0.268318},{0.906325,-0.22049,-0.22049},{1.32294,
-0.22049,-0.257364},{1.41962,-0.22049,-0.22049},{-1.32294,-5.96046e-08,-0.261288},{-1.43557,
-5.96046e-08,-0.22049},{-1.10245,-5.96046e-08,-0.265659},{-0.945983,-5.96046e-08,-0.22049},
{1.10245,-5.96046e-08,-0.265659},{0.945983,-5.96046e-08,-0.22049},{1.32294,-5.96046e-08,
-0.261288},{1.43557,-5.96046e-08,-0.22049},{-1.32294,0.22049,-0.257364},{-1.41962,0.22049,
-0.22049},{-1.10245,0.22049,-0.268318},{-0.906325,0.22049,-0.22049},{1.10245,0.22049,
-0.268318},{0.906325,0.22049,-0.22049},{1.32294,0.22049,-0.257364},{1.41962,0.22049,
-0.22049},{-1.32294,0.440981,-0.242121},{-1.37256,0.440981,-0.22049},{-1.10245,0.440981,
-0.272436},{-0.881962,0.440981,-0.241307},{-0.881962,0.265488,-0.22049},{-0.826536,0.440981,
-0.22049},{0.881962,0.440981,-0.241307},{0.881962,0.265488,-0.22049},{0.826536,0.440981,
-0.22049},{1.10245,0.440981,-0.272436},{1.32294,0.440981,-0.242121},{1.37256,0.440981,
-0.22049},{-1.32294,0.575884,-0.22049},{-1.10245,0.661471,-0.267369},{-1.27938,0.661471,
-0.22049},{-0.881962,0.661471,-0.265659},{-0.661471,0.661471,-0.225167},{-0.661471,0.645759,
-0.22049},{-0.645759,0.661471,-0.22049},{0.661471,0.661471,-0.225167},{0.661471,0.645759,
-0.22049},{0.645759,0.661471,-0.22049},{0.881962,0.661471,-0.265659},{1.10245,0.661471,
-0.267369},{1.32294,0.575884,-0.22049},{1.27938,0.661471,-0.22049},{-1.10245,0.881962,
-0.235836},{-1.14263,0.881962,-0.22049},{-0.881962,0.881962,-0.271135},{-0.661471,0.881962,
-0.265659},{-0.440981,0.881962,-0.241307},{-0.440981,0.826536,-0.22049},{-0.265489,0.881962,
-0.22049},{0.440981,0.881962,-0.241307},{0.440981,0.826536,-0.22049},{0.265488,0.881962,
-0.22049},{0.661471,0.881962,-0.265659},{0.881962,0.881962,-0.271135},{1.10245,0.881962,
-0.235836},{1.14263,0.881962,-0.22049},{-1.10245,0.931392,-0.22049},{-0.881962,1.10245,
-0.235836},{-0.931392,1.10245,-0.22049},{-0.661471,1.10245,-0.267369},{-0.440981,1.10245,
-0.272436},{-0.22049,1.10245,-0.268318},{-0.22049,0.906325,-0.22049},{-5.96046e-08,1.10245,
-0.265659},{-5.96046e-08,0.945983,-0.22049},{0.22049,1.10245,-0.268318},{0.22049,0.906325,
-0.22049},{0.440981,1.10245,-0.272436},{0.661471,1.10245,-0.267369},{0.881962,1.10245,
-0.235836},{1.10245,0.931392,-0.22049},{0.931392,1.10245,-0.22049},{-0.881962,1.14263,
-0.22049},{-0.661471,1.27938,-0.22049},{-0.440981,1.32294,-0.242121},{-0.575884,1.32294,
-0.22049},{-0.22049,1.32294,-0.257364},{-5.96046e-08,1.32294,-0.261288},{0.22049,1.32294,
-0.257364},{0.440981,1.32294,-0.242121},{0.661471,1.27938,-0.22049},{0.575884,1.32294,
-0.22049},{0.881962,1.14263,-0.22049},{-0.440981,1.37256,-0.22049},{-0.22049,1.41962,
-0.22049},{-5.96046e-08,1.43557,-0.22049},{0.22049,1.41962,-0.22049},{0.440981,1.37256,
-0.22049},{-0.22049,-1.54343,-0.0559369},{-0.22049,-1.57158,-5.96046e-08},{-0.351187,
-1.54343,-5.96046e-08},{-5.96046e-08,-1.54343,-0.085016},{-5.96046e-08,-1.58712,-5.96046e-08},
{0.22049,-1.54343,-0.0559369},{0.22049,-1.57158,-5.96046e-08},{0.351187,-1.54343,-5.96046e-08},
{-0.661471,-1.32294,-0.183548},{-0.661471,-1.44098,-5.96046e-08},{-0.87478,-1.32294,
-5.96046e-08},{-0.440981,-1.52317,-5.96046e-08},{0.440981,-1.52317,-5.96046e-08},{0.661471,
-1.32294,-0.183548},{0.661471,-1.44098,-5.96046e-08},{0.87478,-1.32294,-5.96046e-08},{-1.10245,
-1.10245,-0.0559369},{-1.10245,-1.13979,-5.96046e-08},{-1.13979,-1.10245,-5.96046e-08},
{-0.881962,-1.31839,-5.96046e-08},{0.881962,-1.31839,-5.96046e-08},{1.10245,-1.10245,
-0.0559369},{1.10245,-1.13979,-5.96046e-08},{1.13979,-1.10245,-5.96046e-08},{-1.31839,
-0.881962,-5.96046e-08},{-0.22049,-0.881962,-0.211389},{-5.96046e-08,-0.881962,-0.192323},
{0.22049,-0.881962,-0.211389},{1.31839,-0.881962,-5.96046e-08},{-1.32294,-0.661471,
-0.183548},{-1.32294,-0.87478,-5.96046e-08},{-1.44098,-0.661471,-5.96046e-08},{-0.440981,
-0.661471,-0.124374},{-0.22049,-0.661471,-0.0351378},{-5.96046e-08,-0.663425,-5.96046e-08},
{-0.00930458,-0.661471,-5.96046e-08},{0.22049,-0.661471,-0.0351378},{0.00930446,-0.661471,
-5.96046e-08},{0.440981,-0.661471,-0.124374},{1.32294,-0.661471,-0.183548},{1.32294,
-0.87478,-5.96046e-08},{1.44098,-0.661471,-5.96046e-08},{-1.52317,-0.440981,-5.96046e-08},
{-0.661471,-0.440981,-0.124374},{-0.499075,-0.440981,-5.96046e-08},{-0.440981,-0.499075,
-5.96046e-08},{-0.22049,-0.628003,-5.96046e-08},{0.22049,-0.628003,-5.96046e-08},{0.440981,
-0.499075,-5.96046e-08},{0.499075,-0.440981,-5.96046e-08},{0.661471,-0.440981,-0.124374},
{1.52317,-0.440981,-5.96046e-08},{-1.54343,-0.22049,-0.0559369},{-1.54343,-0.351187,
-5.96046e-08},{-1.57158,-0.22049,-5.96046e-08},{-0.881962,-0.22049,-0.211389},{-0.661471,
-0.22049,-0.0351378},{-0.628003,-0.22049,-5.96046e-08},{0.661471,-0.22049,-0.0351378},
{0.628003,-0.22049,-5.96046e-08},{0.881962,-0.22049,-0.211389},{1.54343,-0.22049,-0.0559369},
{1.54343,-0.351187,-5.96046e-08},{1.57158,-0.22049,-5.96046e-08},{-1.54343,-5.96046e-08,
-0.085016},{-1.58712,-5.96046e-08,-5.96046e-08},{-0.881962,-5.96046e-08,-0.192323},
{-0.661471,-0.00930458,-5.96046e-08},{-0.663425,-5.96046e-08,-5.96046e-08},{0.661471,
-0.00930458,-5.96046e-08},{0.881962,-5.96046e-08,-0.192323},{0.663425,-5.96046e-08,-5.96046e-08},
{1.54343,-5.96046e-08,-0.085016},{1.58712,-5.96046e-08,-5.96046e-08},{-1.54343,0.22049,
-0.0559369},{-1.57158,0.22049,-5.96046e-08},{-0.881962,0.22049,-0.211389},{-0.661471,
0.22049,-0.0351378},{-0.661471,0.00930446,-5.96046e-08},{-0.628003,0.22049,-5.96046e-08},
{0.661471,0.22049,-0.0351378},{0.661471,0.00930446,-5.96046e-08},{0.628003,0.22049,
-5.96046e-08},{0.881962,0.22049,-0.211389},{1.54343,0.22049,-0.0559369},{1.57158,0.22049,
-5.96046e-08},{-1.54343,0.351187,-5.96046e-08},{-1.52317,0.440981,-5.96046e-08},{-0.661471,
0.440981,-0.124374},{-0.499075,0.440981,-5.96046e-08},{0.661471,0.440981,-0.124374},
{0.499075,0.440981,-5.96046e-08},{1.52317,0.440981,-5.96046e-08},{1.54343,0.351187,
-5.96046e-08},{-1.44098,0.661471,-5.96046e-08},{-1.32294,0.661471,-0.183548},{-0.440981,
0.661471,-0.124374},{-0.440981,0.499075,-5.96046e-08},{-0.22049,0.661471,-0.0351378},
{-0.22049,0.628003,-5.96046e-08},{-0.00930458,0.661471,-5.96046e-08},{0.22049,0.661471,
-0.0351378},{0.22049,0.628003,-5.96046e-08},{0.00930446,0.661471,-5.96046e-08},{0.440981,
0.661471,-0.124374},{0.440981,0.499075,-5.96046e-08},{1.32294,0.661471,-0.183548},{1.44098,
0.661471,-5.96046e-08},{-1.32294,0.87478,-5.96046e-08},{-1.31839,0.881962,-5.96046e-08},
{-0.22049,0.881962,-0.211389},{-5.96046e-08,0.881962,-0.192323},{-5.96046e-08,0.663425,
-5.96046e-08},{0.22049,0.881962,-0.211389},{1.31839,0.881962,-5.96046e-08},{1.32294,
0.87478,-5.96046e-08},{-1.13979,1.10245,-5.96046e-08},{-1.10245,1.10245,-0.0559369},
{1.10245,1.10245,-0.0559369},{1.13979,1.10245,-5.96046e-08},{-1.10245,1.13979,-5.96046e-08},
{-0.881962,1.31839,-5.96046e-08},{-0.87478,1.32294,-5.96046e-08},{-0.661471,1.32294,
-0.183548},{0.661471,1.32294,-0.183548},{0.87478,1.32294,-5.96046e-08},{0.881962,1.31839,
-5.96046e-08},{1.10245,1.13979,-5.96046e-08},{-0.661471,1.44098,-5.96046e-08},{-0.440981,
1.52317,-5.96046e-08},{-0.351187,1.54343,-5.96046e-08},{-0.22049,1.54343,-0.0559369},
{-5.96046e-08,1.54343,-0.085016},{0.22049,1.54343,-0.0559369},{0.351187,1.54343,-5.96046e-08},
{0.440981,1.52317,-5.96046e-08},{0.661471,1.44098,-5.96046e-08},{-0.22049,1.57158,
-5.96046e-08},{-5.96046e-08,1.58712,-5.96046e-08},{0.22049,1.57158,-5.96046e-08},{-0.22049,
-1.54343,0.0559368},{-5.96046e-08,-1.54343,0.0850159},{0.22049,-1.54343,0.0559368},
{-0.661471,-1.32294,0.183547},{-0.440981,-1.37256,0.22049},{-0.575884,-1.32294,0.22049},
{-0.22049,-1.41962,0.22049},{-5.96046e-08,-1.43557,0.22049},{0.22049,-1.41962,0.22049},
{0.440981,-1.37256,0.22049},{0.661471,-1.32294,0.183547},{0.575884,-1.32294,0.22049},
{-1.10245,-1.10245,0.0559368},{-0.881962,-1.14263,0.22049},{-0.931392,-1.10245,0.22049},
{-0.661471,-1.27938,0.22049},{0.661471,-1.27938,0.22049},{0.881962,-1.14263,0.22049},
{1.10245,-1.10245,0.0559368},{0.931392,-1.10245,0.22049},{-1.10245,-0.931392,0.22049},
{-1.14263,-0.881962,0.22049},{-0.22049,-0.906325,0.22049},{-0.22049,-0.881962,0.211389},
{-0.265489,-0.881962,0.22049},{-5.96046e-08,-0.945983,0.22049},{-5.96046e-08,-0.881962,
0.192323},{0.22049,-0.906325,0.22049},{0.22049,-0.881962,0.211389},{0.265488,-0.881962,
0.22049},{1.10245,-0.931392,0.22049},{1.14263,-0.881962,0.22049},{-1.32294,-0.661471,
0.183547},{-1.27938,-0.661471,0.22049},{-0.440981,-0.826536,0.22049},{-0.440981,-0.661471,
0.124374},{-0.645759,-0.661471,0.22049},{-0.22049,-0.661471,0.0351377},{0.22049,-0.661471,
0.0351377},{0.440981,-0.826536,0.22049},{0.440981,-0.661471,0.124374},{0.645759,-0.661471,
0.22049},{1.32294,-0.661471,0.183547},{1.27938,-0.661471,0.22049},{-1.32294,-0.575884,
0.22049},{-1.37256,-0.440981,0.22049},{-0.661471,-0.645759,0.22049},{-0.661471,-0.440981,
0.124374},{-0.826536,-0.440981,0.22049},{0.661471,-0.645759,0.22049},{0.661471,-0.440981,
0.124374},{0.826536,-0.440981,0.22049},{1.32294,-0.575884,0.22049},{1.37256,-0.440981,
0.22049},{-1.54343,-0.22049,0.0559368},{-1.41962,-0.22049,0.22049},{-0.881962,-0.265489,
0.22049},{-0.881962,-0.22049,0.211389},{-0.906325,-0.22049,0.22049},{-0.661471,-0.22049,
0.0351377},{0.661471,-0.22049,0.0351377},{0.881962,-0.265489,0.22049},{0.881962,-0.22049,
0.211389},{0.906325,-0.22049,0.22049},{1.54343,-0.22049,0.0559368},{1.41962,-0.22049,
0.22049},{-1.54343,-5.96046e-08,0.0850159},{-1.43557,-5.96046e-08,0.22049},{-0.881962,
-5.96046e-08,0.192323},{-0.945983,-5.96046e-08,0.22049},{0.881962,-5.96046e-08,0.192323},
{0.945983,-5.96046e-08,0.22049},{1.54343,-5.96046e-08,0.0850159},{1.43557,-5.96046e-08,
0.22049},{-1.54343,0.22049,0.0559368},{-1.41962,0.22049,0.22049},{-0.881962,0.22049,
0.211389},{-0.906325,0.22049,0.22049},{-0.661471,0.22049,0.0351377},{0.661471,0.22049,
0.0351377},{0.881962,0.22049,0.211389},{0.906325,0.22049,0.22049},{1.54343,0.22049,
0.0559368},{1.41962,0.22049,0.22049},{-1.37256,0.440981,0.22049},{-0.881962,0.265488,
0.22049},{-0.661471,0.440981,0.124374},{-0.826536,0.440981,0.22049},{0.661471,0.440981,
0.124374},{0.881962,0.265488,0.22049},{0.826536,0.440981,0.22049},{1.37256,0.440981,
0.22049},{-1.32294,0.575884,0.22049},{-1.32294,0.661471,0.183547},{-1.27938,0.661471,
0.22049},{-0.661471,0.645759,0.22049},{-0.440981,0.661471,0.124374},{-0.645759,0.661471,
0.22049},{-0.22049,0.661471,0.0351377},{0.22049,0.661471,0.0351377},{0.440981,0.661471,
0.124374},{0.661471,0.645759,0.22049},{0.645759,0.661471,0.22049},{1.32294,0.575884,
0.22049},{1.32294,0.661471,0.183547},{1.27938,0.661471,0.22049},{-1.14263,0.881962,
0.22049},{-0.440981,0.826536,0.22049},{-0.22049,0.881962,0.211389},{-0.265489,0.881962,
0.22049},{-5.96046e-08,0.881962,0.192323},{0.22049,0.881962,0.211389},{0.440981,0.826536,
0.22049},{0.265488,0.881962,0.22049},{1.14263,0.881962,0.22049},{-1.10245,0.931392,
0.22049},{-1.10245,1.10245,0.0559368},{-0.931392,1.10245,0.22049},{-0.22049,0.906325,
0.22049},{-5.96046e-08,0.945983,0.22049},{0.22049,0.906325,0.22049},{1.10245,0.931392,
0.22049},{1.10245,1.10245,0.0559368},{0.931392,1.10245,0.22049},{-0.881962,1.14263,
0.22049},{-0.661471,1.27938,0.22049},{-0.661471,1.32294,0.183547},{-0.575884,1.32294,
0.22049},{0.661471,1.27938,0.22049},{0.661471,1.32294,0.183547},{0.575884,1.32294,
0.22049},{0.881962,1.14263,0.22049},{-0.440981,1.37256,0.22049},{-0.22049,1.41962,
0.22049},{-0.22049,1.54343,0.0559368},{-5.96046e-08,1.43557,0.22049},{-5.96046e-08,1.54343,
0.0850159},{0.22049,1.41962,0.22049},{0.22049,1.54343,0.0559368},{0.440981,1.37256,
0.22049},{-0.440981,-1.32294,0.242121},{-0.22049,-1.32294,0.257363},{-5.96046e-08,-1.32294,
0.261287},{0.22049,-1.32294,0.257363},{0.440981,-1.32294,0.242121},{-0.881962,-1.10245,
0.235836},{-0.661471,-1.10245,0.267369},{-0.440981,-1.10245,0.272436},{-0.22049,-1.10245,
0.268317},{-5.96046e-08,-1.10245,0.265659},{0.22049,-1.10245,0.268317},{0.440981,-1.10245,
0.272436},{0.661471,-1.10245,0.267369},{0.881962,-1.10245,0.235836},{-1.10245,-0.881962,
0.235836},{-0.881962,-0.881962,0.271135},{-0.661471,-0.881962,0.265659},{-0.440981,-0.881962,
0.241307},{0.440981,-0.881962,0.241307},{0.661471,-0.881962,0.265659},{0.881962,-0.881962,
0.271135},{1.10245,-0.881962,0.235836},{-1.10245,-0.661471,0.267369},{-0.881962,-0.661471,
0.265659},{-0.661471,-0.661471,0.225167},{0.661471,-0.661471,0.225167},{0.881962,-0.661471,
0.265659},{1.10245,-0.661471,0.267369},{-1.32294,-0.440981,0.242121},{-1.10245,-0.440981,
0.272436},{-0.881962,-0.440981,0.241307},{0.881962,-0.440981,0.241307},{1.10245,-0.440981,
0.272436},{1.32294,-0.440981,0.242121},{-1.32294,-0.22049,0.257363},{-1.10245,-0.22049,
0.268317},{1.10245,-0.22049,0.268317},{1.32294,-0.22049,0.257363},{-1.32294,-5.96046e-08,
0.261287},{-1.10245,-5.96046e-08,0.265659},{1.10245,-5.96046e-08,0.265659},{1.32294,
-5.96046e-08,0.261287},{-1.32294,0.22049,0.257363},{-1.10245,0.22049,0.268317},{1.10245,
0.22049,0.268317},{1.32294,0.22049,0.257363},{-1.32294,0.440981,0.242121},{-1.10245,
0.440981,0.272436},{-0.881962,0.440981,0.241307},{0.881962,0.440981,0.241307},{1.10245,
0.440981,0.272436},{1.32294,0.440981,0.242121},{-1.10245,0.661471,0.267369},{-0.881962,
0.661471,0.265659},{-0.661471,0.661471,0.225167},{0.661471,0.661471,0.225167},{0.881962,
0.661471,0.265659},{1.10245,0.661471,0.267369},{-1.10245,0.881962,0.235836},{-0.881962,
0.881962,0.271135},{-0.661471,0.881962,0.265659},{-0.440981,0.881962,0.241307},{0.440981,
0.881962,0.241307},{0.661471,0.881962,0.265659},{0.881962,0.881962,0.271135},{1.10245,
0.881962,0.235836},{-0.881962,1.10245,0.235836},{-0.661471,1.10245,0.267369},{-0.440981,
1.10245,0.272436},{-0.22049,1.10245,0.268317},{-5.96046e-08,1.10245,0.265659},{0.22049,
1.10245,0.268317},{0.440981,1.10245,0.272436},{0.661471,1.10245,0.267369},{0.881962,
1.10245,0.235836},{-0.440981,1.32294,0.242121},{-0.22049,1.32294,0.257363},{-5.96046e-08,
1.32294,0.261287},{0.22049,1.32294,0.257363},{0.440981,1.32294,0.242121}
};
	int connect<NEportLevels={2,0}>[3168] = {
   0,1,2,3,4,1,3,1,0,5,6,4,5,4,3,7,8,6,7,6,5,9,10,8,9,8,7,11,10,9,12,13,
14,15,16,13,15,13,12,17,0,2,17,2,16,17,16,15,18,3,0,18,0,17,19,5,3,19,3,
18,20,7,5,20,5,19,21,9,7,21,7,20,22,23,11,22,11,9,22,9,21,24,25,23,24,23,
22,26,25,24,27,28,29,30,12,14,30,14,28,30,28,27,31,15,12,31,12,30,32,17,
15,32,15,31,33,18,17,33,17,32,33,32,34,35,19,18,35,18,33,36,20,19,36,19,
35,37,21,20,37,20,36,37,36,38,39,22,21,39,21,37,40,24,22,40,22,39,41,42,
26,41,26,24,41,24,40,43,42,41,44,27,29,44,29,45,46,30,27,46,27,44,47,31,
30,47,30,46,48,32,31,48,31,47,48,47,49,48,34,32,50,37,38,51,39,37,51,37,
50,51,50,52,53,40,39,53,39,51,54,41,40,54,40,53,55,43,41,55,41,54,56,57,
58,59,44,45,59,45,57,59,57,56,60,46,44,60,44,59,61,47,46,61,46,60,61,60,
62,61,49,47,63,51,52,64,53,51,64,51,63,64,63,65,66,54,53,66,53,64,67,68,
55,67,55,54,67,54,66,69,68,67,70,56,58,70,58,71,72,59,56,72,56,70,73,60,
59,73,59,72,73,72,74,73,62,60,75,64,65,76,66,64,76,64,75,76,75,77,78,67,
66,78,66,76,79,69,67,79,67,78,80,70,71,80,71,81,82,72,70,82,70,80,83,74,
72,83,72,82,84,76,77,84,77,85,86,78,76,86,76,84,87,79,78,87,78,86,88,80,
81,88,81,89,90,82,80,90,80,88,91,83,82,91,82,90,92,84,85,92,85,93,94,86,
84,94,84,92,95,87,86,95,86,94,96,88,89,96,89,97,98,90,88,98,88,96,99,100,
91,99,91,90,99,90,98,101,100,99,102,103,104,105,92,93,105,93,103,105,103,
102,106,94,92,106,92,105,107,95,94,107,94,106,108,96,97,109,98,96,109,96,
108,109,108,110,111,99,98,111,98,109,112,113,101,112,101,99,112,99,111,114,
113,112,115,116,117,118,102,104,118,104,116,118,116,115,119,105,102,119,
102,118,120,106,105,120,105,119,120,119,121,120,107,106,122,109,110,122,
110,123,124,111,109,124,109,122,125,112,111,125,111,124,126,127,114,126,
114,112,126,112,125,128,127,126,129,130,131,132,115,117,132,117,130,132,
130,129,133,118,115,133,115,132,134,119,118,134,118,133,135,121,119,135,
119,134,136,122,123,137,124,122,137,122,136,137,136,138,139,125,124,139,
124,137,140,126,125,140,125,139,141,142,128,141,128,126,141,126,140,143,
144,142,143,142,141,145,146,144,145,144,143,147,129,131,147,131,146,147,
146,145,148,132,129,148,129,147,149,133,132,149,132,148,150,134,133,150,
133,149,150,149,151,150,135,134,152,137,138,153,139,137,153,137,152,154,
140,139,154,139,153,154,153,155,156,141,140,156,140,154,157,143,141,157,
141,156,158,145,143,158,143,157,159,147,145,159,145,158,160,148,147,160,
147,159,160,159,161,162,149,148,162,148,160,162,151,149,163,154,155,164,
156,154,164,154,163,165,157,156,165,156,164,166,158,157,166,157,165,167,
159,158,167,158,166,167,161,159,168,169,170,171,172,169,171,169,168,173,
174,172,173,172,171,175,174,173,176,177,178,2,1,179,2,179,177,2,177,176,
1,4,168,1,168,170,1,170,179,4,6,171,4,171,168,6,8,173,6,173,171,8,10,180,
8,180,175,8,175,173,10,11,181,10,181,182,10,182,180,183,182,181,184,185,
186,14,13,187,14,187,185,14,185,184,13,16,176,13,176,178,13,178,187,16,
2,176,11,23,181,23,25,188,23,188,183,23,183,181,25,26,189,25,189,190,25,
190,188,191,190,189,29,28,184,29,184,186,29,186,192,28,14,184,33,34,193,
35,33,193,35,193,194,36,35,194,36,194,195,38,36,195,26,42,189,42,43,196,
42,196,191,42,191,189,197,198,199,45,29,192,45,192,198,45,198,197,48,49,
200,34,48,200,34,200,201,34,201,193,202,194,193,202,193,201,202,201,203,
204,195,194,204,194,202,204,202,205,50,38,195,50,195,204,50,204,206,52,
50,206,43,55,207,43,207,208,43,208,196,209,208,207,58,57,197,58,197,199,
58,199,210,57,45,197,61,62,211,49,61,211,49,211,212,49,212,200,212,213,200,
214,201,200,214,200,213,214,203,201,215,204,205,216,206,204,216,204,215,
63,52,206,63,206,216,63,216,218,216,217,218,65,63,218,55,68,207,68,69,219,
68,219,209,68,209,207,220,221,222,71,58,210,71,210,221,71,221,220,73,74,
223,62,73,223,62,223,224,62,224,211,225,212,211,225,211,224,226,218,217,
226,217,227,75,65,218,75,218,226,75,226,228,77,75,228,69,79,229,69,229,230,
69,230,219,231,230,229,232,220,222,232,222,233,81,71,220,81,220,232,74,
83,234,74,234,223,235,224,223,235,223,234,235,234,236,235,225,224,237,226,
227,238,228,226,238,226,237,238,237,239,85,77,228,85,228,238,79,87,240,79,
240,229,241,231,229,241,229,240,242,232,233,242,233,243,89,81,232,89,232,
242,83,91,244,83,244,234,245,246,236,245,236,234,245,234,244,247,246,245,
248,249,250,251,238,239,251,239,249,251,249,248,93,85,238,93,238,251,87,
95,252,87,252,240,253,241,240,253,240,252,254,242,243,97,89,242,97,242,254,
97,254,255,91,100,244,100,101,256,100,256,245,100,245,244,257,247,245,257,
245,256,258,248,250,258,250,259,104,103,251,104,251,248,104,248,258,103,
93,251,95,107,260,95,260,261,95,261,252,261,253,252,108,97,255,108,255,262,
108,262,263,110,108,263,101,113,256,113,114,264,113,264,265,113,265,256,
265,257,256,266,267,265,266,265,264,268,267,266,269,270,271,272,273,270,
272,270,269,117,116,258,117,258,259,117,259,272,259,273,272,116,104,258,
120,121,274,107,120,274,107,274,275,107,275,260,276,263,262,123,110,263,
123,263,276,123,276,277,114,127,264,127,128,278,127,278,266,127,266,264,
279,280,268,279,268,266,279,266,278,281,269,271,281,271,280,281,280,279,
131,130,272,131,272,269,131,269,281,130,117,272,121,135,282,121,282,283,
121,283,274,283,275,274,136,123,277,136,277,284,136,284,285,138,136,285,
128,142,278,142,144,279,142,279,278,144,146,281,144,281,279,146,131,281,
150,151,286,135,150,286,135,286,287,135,287,282,288,285,284,152,138,285,
152,285,288,152,288,289,153,152,289,153,289,290,153,290,291,155,153,291,
160,161,292,162,160,292,162,292,293,162,293,294,151,162,294,151,294,295,
151,295,286,295,287,286,296,291,290,163,155,291,163,291,296,163,296,297,
164,163,297,164,297,298,164,298,299,165,164,299,165,299,300,166,165,300,
166,300,301,167,166,301,167,301,302,167,302,303,161,167,303,161,303,304,
161,304,292,304,293,292,305,299,298,306,300,299,306,299,305,307,301,300,
307,300,306,307,302,301,170,169,308,169,172,309,169,309,308,172,174,310,
172,310,309,174,175,310,178,177,311,177,179,312,177,312,313,177,313,311,
179,170,308,179,308,314,179,314,312,309,315,314,309,314,308,310,316,315,
310,315,309,175,180,317,175,317,316,175,316,310,180,182,318,180,318,319,
180,319,317,182,183,318,186,185,320,185,187,321,185,321,322,185,322,320,
187,178,311,187,311,323,187,323,321,313,323,311,318,324,319,183,188,325,
183,325,324,183,324,318,188,190,326,188,326,327,188,327,325,190,191,326,
192,186,320,192,320,328,192,328,329,322,328,320,330,331,332,333,334,331,
333,331,330,335,336,334,335,334,333,335,337,336,326,338,327,191,196,339,
191,339,338,191,338,326,199,198,340,198,192,329,198,329,341,198,341,340,
342,343,344,331,345,343,331,343,342,331,342,332,202,203,345,202,345,331,
202,331,334,205,202,334,205,334,336,205,336,346,347,348,346,347,346,336,
347,336,337,347,349,348,196,208,350,196,350,351,196,351,339,208,209,350,
210,199,340,210,340,352,210,352,353,341,352,340,354,355,356,213,212,355,
213,355,354,213,354,343,354,344,343,214,213,343,214,343,345,203,214,345,
215,205,346,216,215,346,216,346,348,217,216,348,217,348,349,217,349,358,
349,357,358,357,359,358,350,360,351,209,219,361,209,361,360,209,360,350,
222,221,362,221,210,353,221,353,363,221,363,362,364,365,366,355,367,365,
355,365,364,355,364,356,212,225,367,212,367,355,227,217,358,227,358,368,
369,370,368,369,368,358,369,358,359,369,371,370,219,230,372,219,372,373,
219,373,361,230,231,372,233,222,362,233,362,374,363,375,374,363,374,362,
365,376,377,365,377,366,235,236,376,235,376,365,235,365,367,225,235,367,
237,227,368,239,237,368,239,368,370,239,370,378,371,379,378,371,378,370,
372,380,381,372,381,373,231,241,380,231,380,372,243,233,374,243,374,382,
375,383,382,375,382,374,376,384,385,376,385,377,236,246,386,236,386,384,
236,384,376,246,247,386,250,249,387,249,239,378,249,378,388,249,388,387,
379,389,388,379,388,378,380,390,391,380,391,381,241,253,390,241,390,380,
254,243,382,255,254,382,255,382,383,255,383,392,384,393,385,386,394,395,
386,395,393,386,393,384,247,257,394,247,394,386,259,250,387,259,387,396,
388,397,398,388,398,396,388,396,387,389,397,388,261,260,399,261,399,391,
261,391,390,253,261,390,262,255,392,262,392,400,262,400,401,400,402,401,
394,403,395,257,265,404,257,404,405,257,405,394,405,403,394,265,267,406,
265,406,404,267,268,406,271,270,407,270,273,408,270,408,407,273,259,396,
273,396,409,273,409,408,409,410,408,398,409,396,411,412,413,260,275,412,
260,412,411,260,411,399,276,262,401,277,276,401,277,401,402,277,402,414,
404,415,405,406,416,417,406,417,415,406,415,404,268,280,418,268,418,416,
268,416,406,280,271,407,280,407,419,280,419,418,408,420,421,408,421,419,
408,419,407,410,420,408,283,282,422,283,422,413,283,413,412,275,283,412,
284,277,414,284,414,423,284,423,424,423,425,424,416,426,417,418,427,426,
418,426,416,419,428,427,419,427,418,421,428,419,429,430,431,282,287,430,
282,430,429,282,429,422,288,284,424,289,288,424,289,424,425,289,425,432,
290,289,432,290,432,433,290,433,434,433,435,434,436,437,438,294,293,437,
294,437,436,294,436,439,295,294,439,295,439,431,295,431,430,287,295,430,
296,290,434,297,296,434,297,434,435,297,435,440,298,297,440,298,440,441,
298,441,442,443,444,442,443,442,441,445,446,444,445,444,443,303,302,446,
303,446,445,303,445,447,304,303,447,304,447,438,304,438,437,293,304,437,
305,298,442,306,305,442,306,442,444,307,306,444,307,444,446,302,307,446,
313,312,448,312,314,449,312,449,448,314,315,450,314,450,449,315,316,451,
315,451,450,316,317,452,316,452,451,317,319,452,322,321,453,321,323,454,
321,454,453,323,313,448,323,448,455,323,455,454,449,456,455,449,455,448,
450,457,456,450,456,449,451,458,457,451,457,450,452,459,458,452,458,451,
319,324,460,319,460,459,319,459,452,324,325,461,324,461,460,325,327,461,
329,328,462,328,322,453,328,453,463,328,463,462,454,464,463,454,463,453,
455,465,464,455,464,454,330,332,465,330,465,455,330,455,456,333,330,456,
333,456,457,335,333,457,335,457,458,337,335,458,337,458,459,337,459,466,
460,467,466,460,466,459,461,468,467,461,467,460,327,338,469,327,469,468,
327,468,461,338,339,469,341,329,462,341,462,470,463,471,470,463,470,462,
464,472,471,464,471,463,342,344,472,342,472,464,342,464,465,332,342,465,
347,337,466,349,347,466,349,466,467,349,467,473,468,474,473,468,473,467,
469,475,474,469,474,468,339,351,475,339,475,469,353,352,476,352,341,470,
352,470,477,352,477,476,471,478,477,471,477,470,354,356,478,354,478,471,
354,471,472,344,354,472,357,349,473,359,357,473,359,473,474,359,474,479,
475,480,479,475,479,474,351,360,481,351,481,480,351,480,475,360,361,481,
363,353,476,363,476,482,477,483,482,477,482,476,364,366,483,364,483,477,
364,477,478,356,364,478,369,359,479,371,369,479,371,479,480,371,480,484,
481,485,484,481,484,480,361,373,485,361,485,481,375,363,482,375,482,486,
483,487,486,483,486,482,366,377,487,366,487,483,379,371,484,379,484,488,
485,489,488,485,488,484,373,381,489,373,489,485,383,375,486,383,486,490,
487,491,490,487,490,486,377,385,491,377,491,487,389,379,488,389,488,492,
489,493,492,489,492,488,381,391,493,381,493,489,392,383,490,392,490,494,
491,495,494,491,494,490,385,393,496,385,496,495,385,495,491,393,395,496,
398,397,497,397,389,492,397,492,498,397,498,497,493,499,498,493,498,492,
391,399,499,391,499,493,400,392,494,402,400,494,402,494,495,402,495,500,
496,501,500,496,500,495,395,403,502,395,502,501,395,501,496,403,405,502,
410,409,503,409,398,497,409,497,504,409,504,503,498,505,504,498,504,497,
411,413,505,411,505,498,411,498,499,399,411,499,414,402,500,414,500,506,
501,507,506,501,506,500,502,508,507,502,507,501,405,415,509,405,509,508,
405,508,502,415,417,509,421,420,510,420,410,503,420,503,511,420,511,510,
504,512,511,504,511,503,505,513,512,505,512,504,413,422,513,413,513,505,
423,414,506,425,423,506,425,506,507,425,507,514,508,515,514,508,514,507,
509,516,515,509,515,508,417,426,517,417,517,516,417,516,509,426,427,518,
426,518,517,427,428,519,427,519,518,428,421,510,428,510,520,428,520,519,
511,521,520,511,520,510,512,522,521,512,521,511,429,431,522,429,522,512,
429,512,513,422,429,513,432,425,514,433,432,514,433,514,515,435,433,515,
435,515,516,435,516,523,517,524,523,517,523,516,518,525,524,518,524,517,
519,526,525,519,525,518,520,527,526,520,526,519,436,438,527,436,527,520,
436,520,521,439,436,521,439,521,522,431,439,522,440,435,523,441,440,523,
441,523,524,443,441,524,443,524,525,445,443,525,445,525,526,447,445,526,
447,526,527,438,447,527
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	  xform {
	    mat = {
	      {1.0, 0.0, 0.0, 0.0},
	      {0.0, 1.0, 0.0, 0.0},
	      {0.0, 0.0, 1.0, 0.0},
	      {0.0, 0.0, 0.0, 1.7}
	    };
	  };
	};
      };
      dlv_glyph pglyph {
	in_field => <-.<-.orbs.orbital[4];
	in_glyph => <-.dp_mesh.out;
	GlyphParam {
	  map_comp = 1;
	  scale => <-.<-.<-.scale;
	};
      };
      dlv_glyph nglyph {
	in_field => <-.<-.orbs.orbital[4];
	in_glyph => dn_mesh.out;
	GlyphParam {
	  map_comp = 2;
	  scale => <-.<-.<-.scale;
	};
      };
    };
    GMOD.instancer dxz_instancer {
      Value => (<-.orbs.orbital[5].nnodes > 0);
      Group => <-.dxz_orb;
      active = 2;
    };
    macro dxz_orb<instanced=0> {
      // grids cut out of AVS with $get_array then attached to a tri_mesh.
      group dp_mesh {
	float coord<NEportLevels={2,0}>[356][3] = {
   {-1.10245,-0.661471,-1.14516},{-1.10245,-0.714189,-1.10245},{-1.24738,-0.661471,
-1.10245},{-0.927502,-0.661471,-1.10245},{-1.32294,-0.440981,-1.22625},{-1.32294,-0.628446,
-1.10245},{-1.4932,-0.440981,-1.10245},{-1.10245,-0.440981,-1.25567},{-0.881962,-0.440981,
-1.18892},{-0.881962,-0.642796,-1.10245},{-0.775249,-0.440981,-1.10245},{-1.54343,-0.22049,
-1.15917},{-1.54343,-0.340606,-1.10245},{-1.59178,-0.22049,-1.10245},{-1.32294,-0.22049,
-1.29374},{-1.10245,-0.22049,-1.3021},{-0.881962,-0.22049,-1.22592},{-0.741727,-0.22049,
-1.10245},{-1.54343,-5.96046e-08,-1.19185},{-1.62045,-5.96046e-08,-1.10245},{-1.32294,
-5.96046e-08,-1.31281},{-1.10245,-5.96046e-08,-1.31475},{-0.881962,-5.96046e-08,-1.23501},
{-0.737168,-5.96046e-08,-1.10245},{-1.54343,0.22049,-1.15917},{-1.59178,0.22049,-1.10245},
{-1.32294,0.22049,-1.29374},{-1.10245,0.22049,-1.3021},{-0.881962,0.22049,-1.22592},
{-0.741727,0.22049,-1.10245},{-1.54343,0.340606,-1.10245},{-1.32294,0.440981,-1.22625},
{-1.4932,0.440981,-1.10245},{-1.10245,0.440981,-1.25567},{-0.881962,0.440981,-1.18892},
{-0.775249,0.440981,-1.10245},{-1.32294,0.628446,-1.10245},{-1.10245,0.661471,-1.14516},
{-1.24738,0.661471,-1.10245},{-0.881962,0.642796,-1.10245},{-0.927502,0.661471,-1.10245},
{-1.10245,0.714189,-1.10245},{-1.32294,-0.661471,-1.05643},{-1.32294,-0.751199,-0.881962},
{-1.4221,-0.661471,-0.881962},{-1.10245,-0.852991,-0.881962},{-0.881962,-0.661471,-1.09199},
{-0.881962,-0.851168,-0.881962},{-0.661471,-0.661471,-0.916238},{-0.661471,-0.700625,
-0.881962},{-0.636909,-0.661471,-0.881962},{-1.54343,-0.440981,-0.992782},{-1.54343,
-0.499928,-0.881962},{-1.57994,-0.440981,-0.881962},{-0.661471,-0.440981,-1.0145},
{-0.562388,-0.440981,-0.881962},{-1.66009,-0.22049,-0.881962},{-0.661471,-0.22049,
-1.03693},{-0.550669,-0.22049,-0.881962},{-1.68399,-5.96046e-08,-0.881962},{-0.661471,
-5.96046e-08,-1.03828},{-0.553693,-5.96046e-08,-0.881962},{-1.66009,0.22049,-0.881962},
{-0.661471,0.22049,-1.03693},{-0.550669,0.22049,-0.881962},{-1.57994,0.440981,-0.881962},
{-1.54343,0.440981,-0.992782},{-0.661471,0.440981,-1.0145},{-0.562388,0.440981,-0.881962},
{-1.54343,0.499928,-0.881962},{-1.4221,0.661471,-0.881962},{-1.32294,0.661471,-1.05643},
{-0.881962,0.661471,-1.09199},{-0.661471,0.661471,-0.916238},{-0.636909,0.661471,-0.881962},
{-1.32294,0.751199,-0.881962},{-1.10245,0.85299,-0.881962},{-0.881962,0.851168,-0.881962},
{-0.661471,0.700625,-0.881962},{-1.32294,-0.698208,-0.661471},{-1.36064,-0.661471,-0.661471},
{-1.10245,-0.834766,-0.661471},{-0.881962,-0.866016,-0.661471},{-0.661471,-0.793884,
-0.661471},{-0.545434,-0.661471,-0.661471},{-1.54343,-0.440981,-0.765255},{-1.51736,
-0.440981,-0.661471},{-0.449486,-0.440981,-0.661471},{-1.54343,-0.368978,-0.661471},
{-1.59977,-0.22049,-0.661471},{-0.440981,-0.22049,-0.668429},{-0.440981,-0.290147,-0.661471},
{-0.43748,-0.22049,-0.661471},{-1.626,-5.96046e-08,-0.661471},{-0.445349,-5.96046e-08,
-0.661471},{-0.440981,-0.138576,-0.661471},{-1.59977,0.22049,-0.661471},{-0.440981,0.22049,
-0.668429},{-0.440981,0.138576,-0.661471},{-0.43748,0.22049,-0.661471},{-1.54343,0.368978,
-0.661471},{-1.54343,0.440981,-0.765255},{-1.51736,0.440981,-0.661471},{-0.449486,0.440981,
-0.661471},{-0.440981,0.290146,-0.661471},{-1.36064,0.661471,-0.661471},{-0.545434,0.661471,
-0.661471},{-1.32294,0.698208,-0.661471},{-1.10245,0.834766,-0.661471},{-0.881962,0.866015,
-0.661471},{-0.661471,0.793884,-0.661471},{-1.32294,-0.661471,-0.629136},{-1.10245,
-0.661471,-0.475629},{-0.881962,-0.716394,-0.440981},{-1.01931,-0.661471,-0.440981},
{-0.661471,-0.688441,-0.440981},{-0.62836,-0.661471,-0.440981},{-1.32294,-0.440981,
-0.476624},{-1.10245,-0.623351,-0.440981},{-1.28026,-0.440981,-0.440981},{-0.440981,
-0.440981,-0.58988},{-0.440981,-0.477541,-0.440981},{-0.423175,-0.440981,-0.440981},
{-1.54343,-0.22049,-0.60318},{-1.32294,-0.355041,-0.440981},{-1.38507,-0.22049,-0.440981},
{-0.374341,-0.22049,-0.440981},{-1.54343,-5.96046e-08,-0.574738},{-1.41687,-5.96046e-08,
-0.440981},{-0.440981,-5.96046e-08,-0.648254},{-0.372711,-5.96046e-08,-0.440981},{-1.54343,
0.22049,-0.60318},{-1.38507,0.22049,-0.440981},{-0.374341,0.22049,-0.440981},{-1.32294,
0.355041,-0.440981},{-1.32294,0.440981,-0.476624},{-1.28026,0.440981,-0.440981},{-0.440981,
0.440981,-0.58988},{-0.423175,0.440981,-0.440981},{-1.32294,0.661471,-0.629136},{-1.10245,
0.62335,-0.440981},{-1.10245,0.661471,-0.475629},{-1.01931,0.661471,-0.440981},{-0.62836,
0.661471,-0.440981},{-0.440981,0.477541,-0.440981},{-0.881962,0.716394,-0.440981},{-0.661471,
0.688441,-0.440981},{-0.881962,-0.661471,-0.408482},{-0.661471,-0.661471,-0.423125},
{-1.10245,-0.440981,-0.362509},{-0.881962,-0.440981,-0.309051},{-0.661471,-0.440981,
-0.302316},{-0.440981,-0.440981,-0.416743},{-1.32294,-0.22049,-0.407203},{-1.10245,
-0.22049,-0.312518},{-0.881962,-0.22049,-0.258789},{-0.661471,-0.22049,-0.236663},{-0.440981,
-0.22049,-0.31601},{-1.32294,-5.96046e-08,-0.389152},{-1.10245,-5.96046e-08,-0.29732},
{-0.881962,-5.96046e-08,-0.243092},{-0.661471,-0.065666,-0.22049},{-0.705914,-5.96046e-08,
-0.22049},{-0.440981,-5.96046e-08,-0.275361},{-0.610891,-5.96046e-08,-0.22049},{-1.32294,
0.22049,-0.407203},{-1.10245,0.22049,-0.312518},{-0.881962,0.22049,-0.258789},{-0.661471,
0.0656659,-0.22049},{-0.661471,0.22049,-0.236663},{-0.440981,0.22049,-0.31601},{-1.10245,
0.440981,-0.362509},{-0.881962,0.440981,-0.309051},{-0.661471,0.440981,-0.302316},{-0.440981,
0.440981,-0.416743},{-0.881962,0.661471,-0.408482},{-0.661471,0.661471,-0.423125},{-0.661471,
-5.96046e-08,-0.213146},{0.661471,-5.96046e-08,0.213146},{0.661471,-0.065666,0.22049},
{0.610891,-5.96046e-08,0.22049},{0.705914,-5.96046e-08,0.22049},{0.661471,0.0656659,
0.22049},{0.661471,-0.661471,0.423125},{0.661471,-0.688441,0.440981},{0.62836,-0.661471,
0.440981},{0.881962,-0.661471,0.408482},{0.881962,-0.716394,0.440981},{1.01931,-0.661471,
0.440981},{0.440981,-0.440981,0.416743},{0.440981,-0.477541,0.440981},{0.423175,-0.440981,
0.440981},{0.661471,-0.440981,0.302316},{0.881962,-0.440981,0.309051},{1.10245,-0.440981,
0.362509},{1.10245,-0.623351,0.440981},{1.28026,-0.440981,0.440981},{0.440981,-0.22049,
0.31601},{0.374341,-0.22049,0.440981},{0.661471,-0.22049,0.236663},{0.881962,-0.22049,
0.258789},{1.10245,-0.22049,0.312517},{1.32294,-0.22049,0.407203},{1.32294,-0.355041,
0.440981},{1.38507,-0.22049,0.440981},{0.440981,-5.96046e-08,0.275361},{0.372711,-5.96046e-08,
0.440981},{0.881962,-5.96046e-08,0.243092},{1.10245,-5.96046e-08,0.29732},{1.32294,
-5.96046e-08,0.389152},{1.41687,-5.96046e-08,0.440981},{0.440981,0.22049,0.31601},
{0.374341,0.22049,0.440981},{0.661471,0.22049,0.236663},{0.881962,0.22049,0.258789},
{1.10245,0.22049,0.312517},{1.32294,0.22049,0.407203},{1.38507,0.22049,0.440981},{0.440981,
0.440981,0.416743},{0.423175,0.440981,0.440981},{0.661471,0.440981,0.302316},{0.881962,
0.440981,0.309051},{1.10245,0.440981,0.362509},{1.32294,0.355041,0.440981},{1.28026,
0.440981,0.440981},{0.440981,0.477541,0.440981},{0.661471,0.661471,0.423125},{0.62836,
0.661471,0.440981},{0.881962,0.661471,0.408482},{1.10245,0.62335,0.440981},{1.01931,
0.661471,0.440981},{0.661471,0.688441,0.440981},{0.881962,0.716394,0.440981},{0.661471,
-0.793884,0.661471},{0.545434,-0.661471,0.661471},{0.881962,-0.866016,0.661471},{1.10245,
-0.661471,0.475629},{1.10245,-0.834766,0.661471},{1.32294,-0.661471,0.629136},{1.32294,
-0.698208,0.661471},{1.36064,-0.661471,0.661471},{0.440981,-0.440981,0.58988},{0.449486,
-0.440981,0.661471},{1.32294,-0.440981,0.476624},{1.51736,-0.440981,0.661471},{0.440981,
-0.290147,0.661471},{0.43748,-0.22049,0.661471},{1.54343,-0.22049,0.60318},{1.54343,
-0.368978,0.661471},{1.59977,-0.22049,0.661471},{0.440981,-0.138576,0.661471},{0.440981,
-5.96046e-08,0.648254},{0.445349,-5.96046e-08,0.661471},{1.54343,-5.96046e-08,0.574738},
{1.626,-5.96046e-08,0.661471},{0.440981,0.138576,0.661471},{0.43748,0.22049,0.661471},
{1.54343,0.22049,0.60318},{1.59977,0.22049,0.661471},{0.440981,0.290146,0.661471},{0.440981,
0.440981,0.58988},{0.449486,0.440981,0.661471},{1.32294,0.440981,0.476624},{1.51736,
0.440981,0.661471},{1.54343,0.368978,0.661471},{0.545434,0.661471,0.661471},{1.10245,
0.661471,0.475629},{1.32294,0.661471,0.629136},{1.36064,0.661471,0.661471},{0.661471,
0.793884,0.661471},{0.881962,0.866015,0.661471},{1.10245,0.834766,0.661471},{1.32294,
0.698208,0.661471},{0.661471,-0.700625,0.881962},{0.636909,-0.661471,0.881962},{0.881962,
-0.851168,0.881962},{1.10245,-0.852991,0.881962},{1.32294,-0.751199,0.881962},{1.4221,
-0.661471,0.881962},{0.562388,-0.440981,0.881962},{1.54343,-0.440981,0.765255},{1.54343,
-0.499928,0.881962},{1.57994,-0.440981,0.881962},{0.440981,-0.22049,0.668429},{0.550668,
-0.22049,0.881962},{1.66009,-0.22049,0.881962},{0.553693,-5.96046e-08,0.881962},{1.68399,
-5.96046e-08,0.881962},{0.440981,0.22049,0.668429},{0.550668,0.22049,0.881962},{1.66009,
0.22049,0.881962},{0.562388,0.440981,0.881962},{1.54343,0.440981,0.765255},{1.57994,
0.440981,0.881962},{0.636909,0.661471,0.881962},{1.4221,0.661471,0.881962},{1.54343,
0.499928,0.881962},{0.661471,0.700625,0.881962},{0.881962,0.851168,0.881962},{1.10245,
0.85299,0.881962},{1.32294,0.751199,0.881962},{0.661471,-0.661471,0.916238},{0.881962,
-0.661471,1.09199},{1.10245,-0.714189,1.10245},{0.927502,-0.661471,1.10245},{1.32294,
-0.661471,1.05643},{1.24738,-0.661471,1.10245},{0.661471,-0.440981,1.0145},{0.881962,
-0.642796,1.10245},{0.775249,-0.440981,1.10245},{1.32294,-0.628446,1.10245},{1.54343,
-0.440981,0.992782},{1.4932,-0.440981,1.10245},{0.661471,-0.22049,1.03693},{0.741727,
-0.22049,1.10245},{1.54343,-0.340606,1.10245},{1.59178,-0.22049,1.10245},{0.661471,
-5.96046e-08,1.03828},{0.737168,-5.96046e-08,1.10245},{1.62045,-5.96046e-08,1.10245},
{0.661471,0.22049,1.03693},{0.741727,0.22049,1.10245},{1.59178,0.22049,1.10245},{0.661471,
0.440981,1.0145},{0.775249,0.440981,1.10245},{1.54343,0.340606,1.10245},{1.54343,0.440981,
0.992782},{1.4932,0.440981,1.10245},{0.661471,0.661471,0.916238},{0.881962,0.642796,
1.10245},{0.881962,0.661471,1.09199},{0.927502,0.661471,1.10245},{1.32294,0.628446,
1.10245},{1.32294,0.661471,1.05643},{1.24738,0.661471,1.10245},{1.10245,0.714189,1.10245},
{1.10245,-0.661471,1.14515},{0.881962,-0.440981,1.18892},{1.10245,-0.440981,1.25567},
{1.32294,-0.440981,1.22625},{0.881962,-0.22049,1.22592},{1.10245,-0.22049,1.3021},
{1.32294,-0.22049,1.29374},{1.54343,-0.22049,1.15917},{0.881962,-5.96046e-08,1.23501},
{1.10245,-5.96046e-08,1.31475},{1.32294,-5.96046e-08,1.31281},{1.54343,-5.96046e-08,1.19185},
{0.881962,0.22049,1.22592},{1.10245,0.22049,1.3021},{1.32294,0.22049,1.29374},{1.54343,
0.22049,1.15917},{0.881962,0.440981,1.18892},{1.10245,0.440981,1.25567},{1.32294,0.440981,
1.22625},{1.10245,0.661471,1.14515}
};
	int connect<NEportLevels={2,0}>[2112] = {
   0,1,2,3,1,0,4,5,6,7,0,2,7,2,5,7,5,4,8,9,3,8,3,0,8,0,7,10,9,8,11,12,13,
14,4,6,14,6,12,14,12,11,15,7,4,15,4,14,16,8,7,16,7,15,17,10,8,17,8,16,18,
11,13,18,13,19,20,14,11,20,11,18,21,15,14,21,14,20,22,16,15,22,15,21,23,
17,16,23,16,22,24,18,19,24,19,25,26,20,18,26,18,24,27,21,20,27,20,26,28,
22,21,28,21,27,29,23,22,29,22,28,30,24,25,31,26,24,31,24,30,31,30,32,33,
27,26,33,26,31,34,28,27,34,27,33,35,29,28,35,28,34,36,31,32,37,33,31,37,
31,36,37,36,38,39,34,33,39,33,37,39,37,40,39,35,34,41,37,38,41,40,37,42,
43,44,2,1,45,2,45,43,2,43,42,1,3,46,1,46,47,1,47,45,48,49,47,48,47,46,50,
49,48,51,52,53,6,5,42,6,42,44,6,44,51,44,52,51,5,2,42,3,9,46,9,10,54,9,
54,48,9,48,46,55,50,48,55,48,54,13,12,51,13,51,53,13,53,56,12,6,51,10,17,
57,10,57,54,58,55,54,58,54,57,19,13,56,19,56,59,17,23,60,17,60,57,61,58,
57,61,57,60,25,19,59,25,59,62,23,29,63,23,63,60,64,61,60,64,60,63,30,25,
62,30,62,65,30,65,66,32,30,66,29,35,67,29,67,63,68,64,63,68,63,67,69,66,
65,36,32,66,36,66,69,36,69,71,69,70,71,38,36,71,39,40,72,35,39,72,35,72,
73,35,73,67,74,68,67,74,67,73,75,71,70,41,38,71,41,71,75,41,75,76,40,41,
76,40,76,77,40,77,72,78,73,72,78,72,77,78,74,73,44,43,79,44,79,80,43,45,
81,43,81,79,45,47,82,45,82,81,47,49,83,47,83,82,49,50,84,49,84,83,53,52,
85,52,44,80,52,80,86,52,86,85,50,55,87,50,87,84,56,53,85,56,85,88,56,88,
89,86,88,85,55,58,90,55,90,91,55,91,87,92,91,90,59,56,89,59,89,93,58,61,
94,58,94,95,58,95,90,95,92,90,62,59,93,62,93,96,61,64,97,61,97,98,61,98,
94,99,98,97,65,62,96,65,96,100,65,100,101,100,102,101,64,68,103,64,103,104,
64,104,97,104,99,97,69,65,101,70,69,101,70,101,102,70,102,105,68,74,106,
68,106,103,75,70,105,75,105,107,76,75,107,76,107,108,77,76,108,77,108,109,
78,77,109,78,109,110,74,78,110,74,110,106,80,79,111,79,81,112,79,112,111,
81,82,113,81,113,114,81,114,112,82,83,115,82,115,113,83,84,116,83,116,115,
86,80,111,86,111,117,112,118,119,112,119,117,112,117,111,114,118,112,84,
87,120,84,120,121,84,121,116,122,121,120,89,88,123,88,86,117,88,117,124,
88,124,123,124,125,123,119,124,117,87,91,120,91,92,126,91,126,122,91,122,
120,93,89,123,93,123,127,125,128,127,125,127,123,95,94,129,92,95,129,92,
129,130,92,130,126,96,93,127,96,127,131,128,132,131,128,131,127,94,98,129,
98,99,133,98,133,130,98,130,129,100,96,131,102,100,131,102,131,132,102,132,
135,132,134,135,134,136,135,104,103,137,99,104,137,99,137,138,99,138,133,
105,102,135,105,135,139,140,141,139,140,139,135,140,135,136,140,142,141,
103,106,143,103,143,144,103,144,137,144,138,137,107,105,139,108,107,139,
108,139,141,109,108,141,109,141,142,109,142,145,110,109,145,110,145,146,
106,110,146,106,146,143,114,113,147,113,115,148,113,148,147,115,116,148,
119,118,149,118,114,147,118,147,150,118,150,149,148,151,150,148,150,147,
116,121,152,116,152,151,116,151,148,121,122,152,125,124,153,124,119,149,
124,149,154,124,154,153,150,155,154,150,154,149,151,156,155,151,155,150,
152,157,156,152,156,151,122,126,157,122,157,152,128,125,153,128,153,158,
154,159,158,154,158,153,155,160,159,155,159,154,156,161,162,156,162,160,
156,160,155,157,163,164,157,164,161,157,161,156,126,130,163,126,163,157,
132,128,158,132,158,165,159,166,165,159,165,158,160,167,166,160,166,159,
168,169,167,168,167,160,168,160,162,163,170,169,163,169,168,163,168,164,
130,133,170,130,170,163,134,132,165,136,134,165,136,165,166,136,166,171,
167,172,171,167,171,166,169,173,172,169,172,167,170,174,173,170,173,169,
133,138,174,133,174,170,140,136,171,142,140,171,142,171,172,142,172,175,
173,176,175,173,175,172,144,143,176,144,176,173,144,173,174,138,144,174,
145,142,175,146,145,175,146,175,176,143,146,176,162,161,177,161,164,177,
168,162,177,164,168,177,178,179,180,181,179,178,182,178,180,182,181,178,
183,184,185,186,187,184,186,184,183,188,187,186,189,190,191,192,183,185,
192,185,190,192,190,189,193,186,183,193,183,192,194,195,188,194,188,186,
194,186,193,196,195,194,197,189,191,197,191,198,199,192,189,199,189,197,
200,193,192,200,192,199,201,194,193,201,193,200,202,203,196,202,196,194,
202,194,201,204,203,202,205,197,198,205,198,206,180,179,199,180,199,197,
180,197,205,179,181,207,179,207,200,179,200,199,208,201,200,208,200,207,
209,202,201,209,201,208,210,204,202,210,202,209,211,205,206,211,206,212,
182,180,205,182,205,211,182,211,213,181,182,213,181,213,214,181,214,207,
215,208,207,215,207,214,216,209,208,216,208,215,217,210,209,217,209,216,
218,211,212,218,212,219,220,213,211,220,211,218,221,214,213,221,213,220,
222,215,214,222,214,221,223,216,215,223,215,222,223,222,224,223,217,216,
225,218,219,226,220,218,226,218,225,226,225,227,228,221,220,228,220,226,
229,222,221,229,221,228,229,228,230,229,224,222,231,226,227,232,228,226,
232,226,231,232,230,228,185,184,233,185,233,234,184,187,235,184,235,233,
187,188,236,187,236,237,187,237,235,238,239,237,238,237,236,240,239,238,
191,190,241,190,185,234,190,234,242,190,242,241,188,195,236,195,196,243,
195,243,238,195,238,236,244,240,238,244,238,243,198,191,241,198,241,245,
198,245,246,242,245,241,196,203,243,203,204,247,203,247,248,203,248,243,
248,244,243,249,248,247,206,198,246,206,246,250,206,250,251,250,252,251,
204,210,253,204,253,247,254,249,247,254,247,253,212,206,251,212,251,255,
212,255,256,252,255,251,210,217,257,210,257,253,258,254,253,258,253,257,
219,212,256,219,256,259,219,259,260,259,261,260,223,224,262,217,223,262,
217,262,263,217,263,257,263,264,257,264,258,257,225,219,260,227,225,260,
227,260,261,227,261,265,229,230,266,224,229,266,224,266,267,224,267,262,
268,263,262,268,262,267,231,227,265,231,265,269,232,231,269,232,269,270,
230,232,270,230,270,271,230,271,266,272,267,266,272,266,271,272,268,267,
234,233,273,234,273,274,233,235,275,233,275,273,235,237,276,235,276,275,
237,239,277,237,277,276,239,240,278,239,278,277,242,234,274,242,274,279,
240,244,280,240,280,281,240,281,278,282,281,280,246,245,283,245,242,279,
245,279,284,245,284,283,244,248,280,248,249,285,248,285,282,248,282,280,
250,246,283,252,250,283,252,283,284,252,284,286,249,254,287,249,287,285,
256,255,288,255,252,286,255,286,289,255,289,288,254,258,290,254,290,287,
259,256,288,261,259,288,261,288,289,261,289,291,264,263,292,258,264,292,
258,292,293,258,293,290,265,261,291,265,291,294,263,268,295,263,295,296,
263,296,292,296,293,292,269,265,294,269,294,297,270,269,297,270,297,298,
271,270,298,271,298,299,272,271,299,272,299,300,268,272,300,268,300,295,
274,273,301,273,275,302,273,302,301,275,276,303,275,303,304,275,304,302,
276,277,305,276,305,306,276,306,303,277,278,305,279,274,301,279,301,307,
302,308,309,302,309,307,302,307,301,304,308,302,305,310,306,278,281,311,
278,311,312,278,312,305,312,310,305,281,282,311,284,279,307,284,307,313,
309,314,313,309,313,307,311,315,312,282,285,316,282,316,315,282,315,311,
286,284,313,286,313,317,314,318,317,314,317,313,285,287,319,285,319,316,
289,286,317,289,317,320,318,321,320,318,320,317,287,290,322,287,322,319,
291,289,320,291,320,323,321,324,323,321,323,320,325,326,327,290,293,326,
290,326,325,290,325,322,294,291,323,294,323,328,329,330,328,329,328,323,
329,323,324,329,331,330,332,333,334,296,295,333,296,333,332,296,332,326,
332,327,326,293,296,326,297,294,328,298,297,328,298,328,330,299,298,330,
299,330,331,299,331,335,300,299,335,300,335,334,300,334,333,295,300,333,
304,303,336,303,306,336,309,308,337,308,304,336,308,336,338,308,338,337,
306,310,339,306,339,338,306,338,336,310,312,339,314,309,337,314,337,340,
338,341,340,338,340,337,339,342,341,339,341,338,312,315,343,312,343,342,
312,342,339,315,316,343,318,314,340,318,340,344,341,345,344,341,344,340,
342,346,345,342,345,341,343,347,346,343,346,342,316,319,347,316,347,343,
321,318,344,321,344,348,345,349,348,345,348,344,346,350,349,346,349,345,
347,351,350,347,350,346,319,322,351,319,351,347,324,321,348,324,348,352,
349,353,352,349,352,348,350,354,353,350,353,349,325,327,354,325,354,350,
325,350,351,322,325,351,329,324,352,331,329,352,331,352,353,331,353,355,
332,334,355,332,355,353,332,353,354,327,332,354,335,331,355,334,335,355
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	  xform {
	    mat = {
	      {1.0, 0.0, 0.0, 0.0},
	      {0.0, 1.0, 0.0, 0.0},
	      {0.0, 0.0, 1.0, 0.0},
	      {0.0, 0.0, 0.0, 1.3}
	    };
	  };
	};
      };
      group dn_mesh {
	float coord<NEportLevels={2,0}>[356][3] = {
   {1.10245,-0.661471,-1.14516},{1.10245,-0.714189,-1.10245},{0.927502,-0.661471,-1.10245},
{1.24738,-0.661471,-1.10245},{0.881962,-0.440981,-1.18892},{0.881962,-0.642796,-1.10245},
{0.775249,-0.440981,-1.10245},{1.10245,-0.440981,-1.25567},{1.32294,-0.440981,-1.22625},
{1.32294,-0.628446,-1.10245},{1.4932,-0.440981,-1.10245},{0.881962,-0.22049,-1.22592},
{0.741727,-0.22049,-1.10245},{1.10245,-0.22049,-1.3021},{1.32294,-0.22049,-1.29374},
{1.54343,-0.22049,-1.15917},{1.54343,-0.340606,-1.10245},{1.59178,-0.22049,-1.10245},
{0.881962,-5.96046e-08,-1.23501},{0.737168,-5.96046e-08,-1.10245},{1.10245,-5.96046e-08,
-1.31475},{1.32294,-5.96046e-08,-1.31281},{1.54343,-5.96046e-08,-1.19185},{1.62045,
-5.96046e-08,-1.10245},{0.881962,0.22049,-1.22592},{0.741727,0.22049,-1.10245},{1.10245,
0.22049,-1.3021},{1.32294,0.22049,-1.29374},{1.54343,0.22049,-1.15917},{1.59178,0.22049,
-1.10245},{0.881962,0.440981,-1.18892},{0.775249,0.440981,-1.10245},{1.10245,0.440981,
-1.25567},{1.32294,0.440981,-1.22625},{1.54343,0.340606,-1.10245},{1.4932,0.440981,
-1.10245},{0.881962,0.642796,-1.10245},{1.10245,0.661471,-1.14516},{0.927502,0.661471,
-1.10245},{1.32294,0.628446,-1.10245},{1.24738,0.661471,-1.10245},{1.10245,0.714189,
-1.10245},{0.661471,-0.661471,-0.916238},{0.661471,-0.700625,-0.881962},{0.636909,-0.661471,
-0.881962},{0.881962,-0.661471,-1.09199},{0.881962,-0.851168,-0.881962},{1.10245,-0.852991,
-0.881962},{1.32294,-0.661471,-1.05643},{1.32294,-0.751199,-0.881962},{1.4221,-0.661471,
-0.881962},{0.661471,-0.440981,-1.0145},{0.562388,-0.440981,-0.881962},{1.54343,-0.440981,
-0.992782},{1.54343,-0.499928,-0.881962},{1.57994,-0.440981,-0.881962},{0.661471,-0.22049,
-1.03693},{0.550668,-0.22049,-0.881962},{1.66009,-0.22049,-0.881962},{0.661471,-5.96046e-08,
-1.03828},{0.553693,-5.96046e-08,-0.881962},{1.68399,-5.96046e-08,-0.881962},{0.661471,
0.22049,-1.03693},{0.550668,0.22049,-0.881962},{1.66009,0.22049,-0.881962},{0.661471,
0.440981,-1.0145},{0.562388,0.440981,-0.881962},{1.54343,0.440981,-0.992782},{1.57994,
0.440981,-0.881962},{0.661471,0.661471,-0.916238},{0.636909,0.661471,-0.881962},{0.881962,
0.661471,-1.09199},{1.32294,0.661471,-1.05643},{1.4221,0.661471,-0.881962},{1.54343,
0.499928,-0.881962},{0.661471,0.700625,-0.881962},{0.881962,0.851168,-0.881962},{1.10245,
0.85299,-0.881962},{1.32294,0.751199,-0.881962},{0.661471,-0.793884,-0.661471},{0.545434,
-0.661471,-0.661471},{0.881962,-0.866016,-0.661471},{1.10245,-0.834766,-0.661471},{1.32294,
-0.698208,-0.661471},{1.36064,-0.661471,-0.661471},{0.449486,-0.440981,-0.661471},{1.54343,
-0.440981,-0.765255},{1.51736,-0.440981,-0.661471},{0.440981,-0.22049,-0.668429},{0.440981,
-0.290147,-0.661471},{0.43748,-0.22049,-0.661471},{1.54343,-0.368978,-0.661471},{1.59977,
-0.22049,-0.661471},{0.440981,-0.138576,-0.661471},{0.445349,-5.96046e-08,-0.661471},
{1.626,-5.96046e-08,-0.661471},{0.440981,0.22049,-0.668429},{0.440981,0.138576,-0.661471},
{0.43748,0.22049,-0.661471},{1.59977,0.22049,-0.661471},{0.440981,0.290146,-0.661471},
{0.449486,0.440981,-0.661471},{1.54343,0.368978,-0.661471},{1.54343,0.440981,-0.765255},
{1.51736,0.440981,-0.661471},{0.545434,0.661471,-0.661471},{1.36064,0.661471,-0.661471},
{0.661471,0.793884,-0.661471},{0.881962,0.866015,-0.661471},{1.10245,0.834766,-0.661471},
{1.32294,0.698208,-0.661471},{0.661471,-0.688441,-0.440981},{0.62836,-0.661471,-0.440981},
{0.881962,-0.716394,-0.440981},{1.10245,-0.661471,-0.475629},{1.01931,-0.661471,-0.440981},
{1.32294,-0.661471,-0.629136},{0.440981,-0.440981,-0.58988},{0.440981,-0.477541,-0.440981},
{0.423175,-0.440981,-0.440981},{1.10245,-0.623351,-0.440981},{1.32294,-0.440981,-0.476624},
{1.28026,-0.440981,-0.440981},{0.374341,-0.22049,-0.440981},{1.32294,-0.355041,-0.440981},
{1.54343,-0.22049,-0.60318},{1.38507,-0.22049,-0.440981},{0.372711,-5.96046e-08,-0.440981},
{0.440981,-5.96046e-08,-0.648254},{1.54343,-5.96046e-08,-0.574738},{1.41687,-5.96046e-08,
-0.440981},{0.374341,0.22049,-0.440981},{1.54343,0.22049,-0.60318},{1.38507,0.22049,
-0.440981},{0.423175,0.440981,-0.440981},{0.440981,0.440981,-0.58988},{1.32294,0.355041,
-0.440981},{1.32294,0.440981,-0.476624},{1.28026,0.440981,-0.440981},{0.440981,0.477541,
-0.440981},{0.62836,0.661471,-0.440981},{1.10245,0.62335,-0.440981},{1.10245,0.661471,
-0.475629},{1.01931,0.661471,-0.440981},{1.32294,0.661471,-0.629136},{0.661471,0.688441,
-0.440981},{0.881962,0.716394,-0.440981},{0.661471,-0.661471,-0.423125},{0.881962,-0.661471,
-0.408482},{0.440981,-0.440981,-0.416743},{0.661471,-0.440981,-0.302316},{0.881962,
-0.440981,-0.309051},{1.10245,-0.440981,-0.362509},{0.440981,-0.22049,-0.31601},{0.661471,
-0.22049,-0.236663},{0.881962,-0.22049,-0.258789},{1.10245,-0.22049,-0.312518},{1.32294,
-0.22049,-0.407203},{0.440981,-5.96046e-08,-0.275361},{0.661471,-0.065666,-0.22049},
{0.610891,-5.96046e-08,-0.22049},{0.881962,-5.96046e-08,-0.243092},{0.705914,-5.96046e-08,
-0.22049},{1.10245,-5.96046e-08,-0.29732},{1.32294,-5.96046e-08,-0.389152},{0.440981,
0.22049,-0.31601},{0.661471,0.0656659,-0.22049},{0.661471,0.22049,-0.236663},{0.881962,
0.22049,-0.258789},{1.10245,0.22049,-0.312518},{1.32294,0.22049,-0.407203},{0.440981,
0.440981,-0.416743},{0.661471,0.440981,-0.302316},{0.881962,0.440981,-0.309051},{1.10245,
0.440981,-0.362509},{0.661471,0.661471,-0.423125},{0.881962,0.661471,-0.408482},{0.661471,
-5.96046e-08,-0.213146},{-0.661471,-5.96046e-08,0.213146},{-0.661471,-0.065666,0.22049},
{-0.705914,-5.96046e-08,0.22049},{-0.610891,-5.96046e-08,0.22049},{-0.661471,0.0656659,
0.22049},{-0.881962,-0.661471,0.408482},{-0.881962,-0.716394,0.440981},{-1.01931,-0.661471,
0.440981},{-0.661471,-0.661471,0.423125},{-0.661471,-0.688441,0.440981},{-0.62836,-0.661471,
0.440981},{-1.10245,-0.440981,0.362509},{-1.10245,-0.623351,0.440981},{-1.28026,-0.440981,
0.440981},{-0.881962,-0.440981,0.309051},{-0.661471,-0.440981,0.302316},{-0.440981,-0.440981,
0.416743},{-0.440981,-0.477541,0.440981},{-0.423175,-0.440981,0.440981},{-1.32294,-0.22049,
0.407203},{-1.32294,-0.355041,0.440981},{-1.38507,-0.22049,0.440981},{-1.10245,-0.22049,
0.312517},{-0.881962,-0.22049,0.258789},{-0.661471,-0.22049,0.236663},{-0.440981,-0.22049,
0.31601},{-0.374341,-0.22049,0.440981},{-1.32294,-5.96046e-08,0.389152},{-1.41687,-5.96046e-08,
0.440981},{-1.10245,-5.96046e-08,0.29732},{-0.881962,-5.96046e-08,0.243092},{-0.440981,
-5.96046e-08,0.275361},{-0.372711,-5.96046e-08,0.440981},{-1.32294,0.22049,0.407203},
{-1.38507,0.22049,0.440981},{-1.10245,0.22049,0.312517},{-0.881962,0.22049,0.258789},
{-0.661471,0.22049,0.236663},{-0.440981,0.22049,0.31601},{-0.374341,0.22049,0.440981},
{-1.32294,0.355041,0.440981},{-1.10245,0.440981,0.362509},{-1.28026,0.440981,0.440981},
{-0.881962,0.440981,0.309051},{-0.661471,0.440981,0.302316},{-0.440981,0.440981,0.416743},
{-0.423175,0.440981,0.440981},{-1.10245,0.62335,0.440981},{-0.881962,0.661471,0.408482},
{-1.01931,0.661471,0.440981},{-0.661471,0.661471,0.423125},{-0.440981,0.477541,0.440981},
{-0.62836,0.661471,0.440981},{-0.881962,0.716394,0.440981},{-0.661471,0.688441,0.440981},
{-1.32294,-0.661471,0.629136},{-1.32294,-0.698208,0.661471},{-1.36064,-0.661471,0.661471},
{-1.10245,-0.661471,0.475629},{-1.10245,-0.834766,0.661471},{-0.881962,-0.866016,0.661471},
{-0.661471,-0.793884,0.661471},{-0.545434,-0.661471,0.661471},{-1.32294,-0.440981,0.476624},
{-1.51736,-0.440981,0.661471},{-0.440981,-0.440981,0.58988},{-0.449486,-0.440981,0.661471},
{-1.54343,-0.22049,0.60318},{-1.54343,-0.368978,0.661471},{-1.59977,-0.22049,0.661471},
{-0.440981,-0.290147,0.661471},{-0.43748,-0.22049,0.661471},{-1.54343,-5.96046e-08,0.574738},
{-1.626,-5.96046e-08,0.661471},{-0.440981,-0.138576,0.661471},{-0.440981,-5.96046e-08,
0.648254},{-0.445349,-5.96046e-08,0.661471},{-1.54343,0.22049,0.60318},{-1.59977,0.22049,
0.661471},{-0.440981,0.138576,0.661471},{-0.43748,0.22049,0.661471},{-1.54343,0.368978,
0.661471},{-1.51736,0.440981,0.661471},{-1.32294,0.440981,0.476624},{-0.440981,0.290146,
0.661471},{-0.440981,0.440981,0.58988},{-0.449486,0.440981,0.661471},{-1.32294,0.661471,
0.629136},{-1.36064,0.661471,0.661471},{-1.10245,0.661471,0.475629},{-0.545434,0.661471,
0.661471},{-1.32294,0.698208,0.661471},{-1.10245,0.834766,0.661471},{-0.881962,0.866015,
0.661471},{-0.661471,0.793884,0.661471},{-1.32294,-0.751199,0.881962},{-1.4221,-0.661471,
0.881962},{-1.10245,-0.852991,0.881962},{-0.881962,-0.851168,0.881962},{-0.661471,-0.700625,
0.881962},{-0.636909,-0.661471,0.881962},{-1.54343,-0.440981,0.765255},{-1.54343,-0.499928,
0.881962},{-1.57994,-0.440981,0.881962},{-0.562388,-0.440981,0.881962},{-1.66009,-0.22049,
0.881962},{-0.440981,-0.22049,0.668429},{-0.550669,-0.22049,0.881962},{-1.68399,-5.96046e-08,
0.881962},{-0.553693,-5.96046e-08,0.881962},{-1.66009,0.22049,0.881962},{-0.440981,0.22049,
0.668429},{-0.550669,0.22049,0.881962},{-1.57994,0.440981,0.881962},{-1.54343,0.440981,
0.765255},{-0.562388,0.440981,0.881962},{-1.54343,0.499928,0.881962},{-1.4221,0.661471,
0.881962},{-0.636909,0.661471,0.881962},{-1.32294,0.751199,0.881962},{-1.10245,0.85299,
0.881962},{-0.881962,0.851168,0.881962},{-0.661471,0.700625,0.881962},{-1.32294,-0.661471,
1.05643},{-1.10245,-0.714189,1.10245},{-1.24738,-0.661471,1.10245},{-0.881962,-0.661471,
1.09199},{-0.927502,-0.661471,1.10245},{-0.661471,-0.661471,0.916238},{-1.54343,-0.440981,
0.992782},{-1.32294,-0.628446,1.10245},{-1.4932,-0.440981,1.10245},{-0.881962,-0.642796,
1.10245},{-0.661471,-0.440981,1.0145},{-0.775249,-0.440981,1.10245},{-1.54343,-0.340606,
1.10245},{-1.59178,-0.22049,1.10245},{-0.661471,-0.22049,1.03693},{-0.741727,-0.22049,
1.10245},{-1.62045,-5.96046e-08,1.10245},{-0.661471,-5.96046e-08,1.03828},{-0.737168,
-5.96046e-08,1.10245},{-1.59178,0.22049,1.10245},{-0.661471,0.22049,1.03693},{-0.741727,
0.22049,1.10245},{-1.54343,0.340606,1.10245},{-1.54343,0.440981,0.992782},{-1.4932,
0.440981,1.10245},{-0.661471,0.440981,1.0145},{-0.775249,0.440981,1.10245},{-1.32294,
0.628446,1.10245},{-1.32294,0.661471,1.05643},{-1.24738,0.661471,1.10245},{-0.881962,
0.642796,1.10245},{-0.881962,0.661471,1.09199},{-0.927502,0.661471,1.10245},{-0.661471,
0.661471,0.916238},{-1.10245,0.714189,1.10245},{-1.10245,-0.661471,1.14515},{-1.32294,
-0.440981,1.22625},{-1.10245,-0.440981,1.25567},{-0.881962,-0.440981,1.18892},{-1.54343,
-0.22049,1.15917},{-1.32294,-0.22049,1.29374},{-1.10245,-0.22049,1.3021},{-0.881962,
-0.22049,1.22592},{-1.54343,-5.96046e-08,1.19185},{-1.32294,-5.96046e-08,1.31281},
{-1.10245,-5.96046e-08,1.31475},{-0.881962,-5.96046e-08,1.23501},{-1.54343,0.22049,
1.15917},{-1.32294,0.22049,1.29374},{-1.10245,0.22049,1.3021},{-0.881962,0.22049,1.22592},
{-1.32294,0.440981,1.22625},{-1.10245,0.440981,1.25567},{-0.881962,0.440981,1.18892},
{-1.10245,0.661471,1.14515}
};
	int connect<NEportLevels={2,0}>[2112] ={
   0,1,2,3,1,0,4,5,6,7,0,2,7,2,5,7,5,4,8,9,3,8,3,0,8,0,7,10,9,8,11,4,6,
11,6,12,13,7,4,13,4,11,14,8,7,14,7,13,15,16,10,15,10,8,15,8,14,17,16,15,
18,11,12,18,12,19,20,13,11,20,11,18,21,14,13,21,13,20,22,15,14,22,14,21,
23,17,15,23,15,22,24,18,19,24,19,25,26,20,18,26,18,24,27,21,20,27,20,26,
28,22,21,28,21,27,29,23,22,29,22,28,30,24,25,30,25,31,32,26,24,32,24,30,
33,27,26,33,26,32,34,28,27,34,27,33,34,33,35,34,29,28,36,30,31,37,32,30,
37,30,36,37,36,38,39,33,32,39,32,37,39,37,40,39,35,33,41,37,38,41,40,37,
42,43,44,45,46,43,45,43,42,2,1,47,2,47,46,2,46,45,1,3,48,1,48,49,1,49,47,
50,49,48,51,42,44,51,44,52,6,5,45,6,45,42,6,42,51,5,2,45,3,9,48,9,10,53,
9,53,54,9,54,48,54,50,48,55,54,53,56,51,52,56,52,57,12,6,51,12,51,56,10,
16,53,16,17,58,16,58,55,16,55,53,59,56,57,59,57,60,19,12,56,19,56,59,17,
23,61,17,61,58,62,59,60,62,60,63,25,19,59,25,59,62,23,29,64,23,64,61,65,
62,63,65,63,66,31,25,62,31,62,65,34,35,67,29,34,67,29,67,68,29,68,64,69,
65,66,69,66,70,36,31,65,36,65,69,36,69,71,38,36,71,39,40,72,35,39,72,35,
72,73,35,73,67,73,74,67,74,68,67,75,69,70,76,71,69,76,69,75,41,38,71,41,
71,76,41,76,77,40,41,77,40,77,78,40,78,72,78,73,72,44,43,79,44,79,80,43,
46,81,43,81,79,46,47,82,46,82,81,47,49,83,47,83,82,49,50,84,49,84,83,52,
44,80,52,80,85,50,54,86,50,86,87,50,87,84,54,55,86,88,89,90,57,52,85,57,
85,89,57,89,88,86,91,87,55,58,92,55,92,91,55,91,86,93,88,90,60,57,88,60,
88,93,60,93,94,58,61,95,58,95,92,96,97,98,63,60,94,63,94,97,63,97,96,61,
64,99,61,99,95,100,96,98,66,63,96,66,96,100,66,100,101,102,103,104,64,68,
103,64,103,102,64,102,99,70,66,101,70,101,105,74,73,106,74,106,104,74,104,
103,68,74,103,75,70,105,75,105,107,76,75,107,76,107,108,77,76,108,77,108,
109,78,77,109,78,109,110,73,78,110,73,110,106,80,79,111,80,111,112,79,81,
113,79,113,111,81,82,114,81,114,115,81,115,113,82,83,116,82,116,114,83,
84,116,117,118,119,85,80,112,85,112,118,85,118,117,114,120,115,116,121,122,
116,122,120,116,120,114,84,87,121,84,121,116,90,89,117,90,117,119,90,119,
123,89,85,117,121,124,122,87,91,125,87,125,126,87,126,121,126,124,121,91,
92,125,93,90,123,93,123,127,93,127,128,94,93,128,125,129,130,125,130,126,
92,95,129,92,129,125,98,97,128,98,128,127,98,127,131,97,94,128,129,132,133,
129,133,130,95,99,132,95,132,129,100,98,131,100,131,134,100,134,135,101,
100,135,136,137,138,102,104,137,102,137,136,102,136,132,136,133,132,99,
102,132,139,135,134,105,101,135,105,135,139,105,139,140,141,142,143,137,
144,142,137,142,141,137,141,138,104,106,144,104,144,137,107,105,140,107,
140,145,108,107,145,108,145,146,109,108,146,109,146,143,109,143,142,110,
109,142,110,142,144,106,110,144,112,111,147,111,113,148,111,148,147,113,
115,148,119,118,149,118,112,147,118,147,150,118,150,149,148,151,150,148,
150,147,115,120,152,115,152,151,115,151,148,120,122,152,123,119,149,123,
149,153,150,154,153,150,153,149,151,155,154,151,154,150,152,156,155,152,
155,151,122,124,157,122,157,156,122,156,152,124,126,157,127,123,153,127,
153,158,154,159,160,154,160,158,154,158,153,155,161,162,155,162,159,155,
159,154,156,163,161,156,161,155,157,164,163,157,163,156,126,130,164,126,
164,157,131,127,158,131,158,165,166,167,165,166,165,158,166,158,160,161,
168,167,161,167,166,161,166,162,163,169,168,163,168,161,164,170,169,164,
169,163,130,133,170,130,170,164,134,131,165,134,165,171,167,172,171,167,
171,165,168,173,172,168,172,167,169,174,173,169,173,168,136,138,174,136,
174,169,136,169,170,133,136,170,139,134,171,140,139,171,140,171,172,140,
172,175,173,176,175,173,175,172,141,143,176,141,176,173,141,173,174,138,
141,174,145,140,175,146,145,175,146,175,176,143,146,176,160,159,177,159,
162,177,166,160,177,162,166,177,178,179,180,181,179,178,182,178,180,182,
181,178,183,184,185,186,187,184,186,184,183,188,187,186,189,190,191,192,
183,185,192,185,190,192,190,189,193,186,183,193,183,192,194,195,188,194,
188,186,194,186,193,196,195,194,197,198,199,200,189,191,200,191,198,200,
198,197,201,192,189,201,189,200,202,193,192,202,192,201,203,194,193,203,
193,202,204,196,194,204,194,203,205,197,199,205,199,206,207,200,197,207,
197,205,208,201,200,208,200,207,180,179,202,180,202,201,180,201,208,179,
181,209,179,209,203,179,203,202,210,204,203,210,203,209,211,205,206,211,
206,212,213,207,205,213,205,211,214,208,207,214,207,213,182,180,208,182,
208,214,182,214,215,181,182,215,181,215,216,181,216,209,217,210,209,217,
209,216,218,211,212,219,213,211,219,211,218,219,218,220,221,214,213,221,
213,219,222,215,214,222,214,221,223,216,215,223,215,222,224,217,216,224,
216,223,225,219,220,226,221,219,226,219,225,226,225,227,228,222,221,228,
221,226,229,223,222,229,222,228,229,228,230,229,224,223,231,226,227,232,
228,226,232,226,231,232,230,228,233,234,235,236,237,234,236,234,233,185,
184,238,185,238,237,185,237,236,184,187,239,184,239,238,187,188,240,187,
240,239,241,233,235,241,235,242,191,190,236,191,236,233,191,233,241,190,
185,236,188,195,243,188,243,244,188,244,240,195,196,243,245,246,247,199,
198,241,199,241,242,199,242,245,242,246,245,198,191,241,243,248,244,196,
204,249,196,249,248,196,248,243,250,245,247,250,247,251,206,199,245,206,
245,250,252,253,254,204,210,253,204,253,252,204,252,249,255,250,251,255,
251,256,212,206,250,212,250,255,253,257,254,210,217,258,210,258,257,210,
257,253,259,255,256,218,212,255,218,255,259,218,259,261,259,260,261,220,
218,261,262,263,264,217,224,263,217,263,262,217,262,258,265,261,260,265,
260,266,225,220,261,225,261,265,225,265,267,227,225,267,229,230,268,229,
268,264,229,264,263,224,229,263,269,265,266,270,267,265,270,265,269,231,
227,267,231,267,270,231,270,271,232,231,271,232,271,272,230,232,272,230,
272,268,235,234,273,235,273,274,234,237,275,234,275,273,237,238,276,237,
276,275,238,239,277,238,277,276,239,240,278,239,278,277,279,280,281,242,
235,274,242,274,280,242,280,279,240,244,282,240,282,278,247,246,279,247,
279,281,247,281,283,246,242,279,244,248,284,244,284,285,244,285,282,248,
249,284,251,247,283,251,283,286,252,254,287,252,287,285,252,285,284,249,
252,284,256,251,286,256,286,288,254,257,289,254,289,290,254,290,287,257,
258,289,259,256,288,259,288,291,259,291,292,260,259,292,262,264,293,262,
293,290,262,290,289,258,262,289,294,292,291,266,260,292,266,292,294,266,
294,295,264,268,296,264,296,293,269,266,295,269,295,297,270,269,297,270,
297,298,271,270,298,271,298,299,272,271,299,272,299,300,268,272,300,268,
300,296,274,273,301,273,275,302,273,302,303,273,303,301,275,276,304,275,
304,305,275,305,302,276,277,306,276,306,304,277,278,306,281,280,307,280,
274,301,280,301,308,280,308,307,308,309,307,303,308,301,304,310,305,306,
311,312,306,312,310,306,310,304,278,282,311,278,311,306,283,281,307,283,
307,313,283,313,314,309,313,307,311,315,316,311,316,312,282,285,315,282,
315,311,286,283,314,286,314,317,315,318,319,315,319,316,285,287,318,285,
318,315,288,286,317,288,317,320,318,321,322,318,322,319,287,290,321,287,
321,318,291,288,320,291,320,323,291,323,324,323,325,324,321,326,327,321,
327,322,290,293,326,290,326,321,294,291,324,295,294,324,295,324,325,295,
325,329,325,328,329,328,330,329,331,332,333,326,334,332,326,332,331,326,
331,327,293,296,334,293,334,326,297,295,329,298,297,329,298,329,330,298,
330,335,299,298,335,299,335,333,299,333,332,300,299,332,300,332,334,296,
300,334,303,302,336,302,305,336,309,308,337,308,303,336,308,336,338,308,
338,337,305,310,339,305,339,338,305,338,336,310,312,339,314,313,340,313,
309,337,313,337,341,313,341,340,338,342,341,338,341,337,339,343,342,339,
342,338,312,316,343,312,343,339,317,314,340,317,340,344,341,345,344,341,
344,340,342,346,345,342,345,341,343,347,346,343,346,342,316,319,347,316,
347,343,320,317,344,320,344,348,345,349,348,345,348,344,346,350,349,346,
349,345,347,351,350,347,350,346,319,322,351,319,351,347,323,320,348,325,
323,348,325,348,349,325,349,352,350,353,352,350,352,349,351,354,353,351,
353,350,322,327,354,322,354,351,328,325,352,330,328,352,330,352,353,330,
353,355,331,333,355,331,355,353,331,353,354,327,331,354,335,330,355,333,
335,355
} ;
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	  xform {
	    mat = {
	      {1.0, 0.0, 0.0, 0.0},
	      {0.0, 1.0, 0.0, 0.0},
	      {0.0, 0.0, 1.0, 0.0},
	      {0.0, 0.0, 0.0, 1.3}
	    };
	  };
	};
      };
      dlv_glyph pglyph {
	in_field => <-.<-.orbs.orbital[5];
	in_glyph => <-.dp_mesh.out;
	GlyphParam {
	  map_comp = 1;
	  scale => <-.<-.<-.scale;
	};
      };
      dlv_glyph nglyph {
	in_field => <-.<-.orbs.orbital[5];
	in_glyph => dn_mesh.out;
	GlyphParam {
	  map_comp = 2;
	  scale => <-.<-.<-.scale;
	};
      };
    };
    GMOD.instancer dyz_instancer {
      Value => (<-.orbs.orbital[6].nnodes > 0);
      Group => <-.dyz_orb;
      active = 2;
    };
    macro dyz_orb<instanced=0> {
      // grids cut out of AVS with $get_array then attached to a tri_mesh.
      group dp_mesh {
	float coord<NEportLevels={2,0}>[356][3] = {
   {-0.22049,-1.54343,-1.15917},{-0.22049,-1.59178,-1.10245},{-0.340606,-1.54343,-1.10245},
{-5.96046e-08,-1.54343,-1.19185},{-5.96046e-08,-1.62045,-1.10245},{0.22049,-1.54343,
-1.15917},{0.22049,-1.59178,-1.10245},{0.340606,-1.54343,-1.10245},{-0.440981,-1.32294,
-1.22625},{-0.440981,-1.4932,-1.10245},{-0.628446,-1.32294,-1.10245},{-0.22049,-1.32294,
-1.29374},{-5.96046e-08,-1.32294,-1.31281},{0.22049,-1.32294,-1.29374},{0.440981,-1.32294,
-1.22625},{0.440981,-1.4932,-1.10245},{0.628446,-1.32294,-1.10245},{-0.661471,-1.10245,
-1.14516},{-0.661471,-1.24738,-1.10245},{-0.714189,-1.10245,-1.10245},{-0.440981,-1.10245,
-1.25567},{-0.22049,-1.10245,-1.3021},{-5.96046e-08,-1.10245,-1.31475},{0.22049,-1.10245,
-1.3021},{0.440981,-1.10245,-1.25567},{0.661471,-1.10245,-1.14516},{0.661471,-1.24738,
-1.10245},{0.714189,-1.10245,-1.10245},{-0.661471,-0.927502,-1.10245},{-0.440981,-0.881962,
-1.18892},{-0.642796,-0.881962,-1.10245},{-0.22049,-0.881962,-1.22592},{-5.96046e-08,-0.881962,
-1.23501},{0.22049,-0.881962,-1.22592},{0.440981,-0.881962,-1.18892},{0.661471,-0.927502,
-1.10245},{0.642796,-0.881962,-1.10245},{-0.440981,-0.775249,-1.10245},{-0.22049,-0.741727,
-1.10245},{-5.96046e-08,-0.737168,-1.10245},{0.22049,-0.741727,-1.10245},{0.440981,
-0.775249,-1.10245},{-0.440981,-1.54343,-0.992782},{-0.440981,-1.57994,-0.881962},{-0.499928,
-1.54343,-0.881962},{-0.22049,-1.66009,-0.881962},{-5.96046e-08,-1.68399,-0.881962},
{0.22049,-1.66009,-0.881962},{0.440981,-1.54343,-0.992782},{0.440981,-1.57994,-0.881962},
{0.499928,-1.54343,-0.881962},{-0.661471,-1.32294,-1.05643},{-0.661471,-1.4221,-0.881962},
{-0.751199,-1.32294,-0.881962},{0.661471,-1.32294,-1.05643},{0.661471,-1.4221,-0.881962},
{0.751199,-1.32294,-0.881962},{-0.852991,-1.10245,-0.881962},{0.85299,-1.10245,-0.881962},
{-0.851168,-0.881962,-0.881962},{-0.661471,-0.881962,-1.09199},{0.661471,-0.881962,
-1.09199},{0.851168,-0.881962,-0.881962},{-0.661471,-0.661471,-0.916238},{-0.700625,
-0.661471,-0.881962},{-0.440981,-0.661471,-1.0145},{-0.22049,-0.661471,-1.03693},{-5.96046e-08,
-0.661471,-1.03828},{0.22049,-0.661471,-1.03693},{0.440981,-0.661471,-1.0145},{0.661471,
-0.661471,-0.916238},{0.700625,-0.661471,-0.881962},{-0.661471,-0.636909,-0.881962},
{-0.440981,-0.562388,-0.881962},{-0.22049,-0.550669,-0.881962},{-5.96046e-08,-0.553693,
-0.881962},{0.22049,-0.550669,-0.881962},{0.440981,-0.562388,-0.881962},{0.661471,-0.636909,
-0.881962},{-0.440981,-1.54343,-0.765255},{-0.22049,-1.59977,-0.661471},{-0.368978,-1.54343,
-0.661471},{-5.96046e-08,-1.626,-0.661471},{0.22049,-1.59977,-0.661471},{0.440981,-1.54343,
-0.765255},{0.368978,-1.54343,-0.661471},{-0.661471,-1.36064,-0.661471},{-0.698208,-1.32294,
-0.661471},{-0.440981,-1.51736,-0.661471},{0.440981,-1.51736,-0.661471},{0.661471,-1.36064,
-0.661471},{0.698208,-1.32294,-0.661471},{-0.834766,-1.10245,-0.661471},{0.834766,-1.10245,
-0.661471},{-0.866016,-0.881962,-0.661471},{0.866015,-0.881962,-0.661471},{-0.793884,
-0.661471,-0.661471},{0.793884,-0.661471,-0.661471},{-0.661471,-0.545434,-0.661471},
{-0.440981,-0.449486,-0.661471},{-0.290147,-0.440981,-0.661471},{-0.22049,-0.440981,
-0.668429},{-0.138576,-0.440981,-0.661471},{-5.96046e-08,-0.445349,-0.661471},{0.138576,
-0.440981,-0.661471},{0.22049,-0.440981,-0.668429},{0.290146,-0.440981,-0.661471},{0.440981,
-0.449486,-0.661471},{0.661471,-0.545434,-0.661471},{-0.22049,-0.43748,-0.661471},{0.22049,
-0.43748,-0.661471},{-0.22049,-1.54343,-0.60318},{-5.96046e-08,-1.54343,-0.574738},
{0.22049,-1.54343,-0.60318},{-0.661471,-1.32294,-0.629136},{-0.440981,-1.32294,-0.476624},
{-0.22049,-1.38507,-0.440981},{-0.355041,-1.32294,-0.440981},{-5.96046e-08,-1.41687,
-0.440981},{0.22049,-1.38507,-0.440981},{0.440981,-1.32294,-0.476624},{0.355041,-1.32294,
-0.440981},{0.661471,-1.32294,-0.629136},{-0.661471,-1.10245,-0.475629},{-0.440981,-1.28026,
-0.440981},{-0.623351,-1.10245,-0.440981},{0.440981,-1.28026,-0.440981},{0.661471,-1.10245,
-0.475629},{0.62335,-1.10245,-0.440981},{-0.661471,-1.01931,-0.440981},{-0.716394,-0.881962,
-0.440981},{0.661471,-1.01931,-0.440981},{0.716394,-0.881962,-0.440981},{-0.688441,-0.661471,
-0.440981},{0.688441,-0.661471,-0.440981},{-0.661471,-0.62836,-0.440981},{-0.477541,
-0.440981,-0.440981},{-0.440981,-0.440981,-0.58988},{-5.96046e-08,-0.440981,-0.648254},
{0.440981,-0.440981,-0.58988},{0.477541,-0.440981,-0.440981},{0.661471,-0.62836,-0.440981},
{-0.440981,-0.423175,-0.440981},{-0.22049,-0.374341,-0.440981},{-5.96046e-08,-0.372711,
-0.440981},{0.22049,-0.374341,-0.440981},{0.440981,-0.423175,-0.440981},{-0.22049,-1.32294,
-0.407203},{-5.96046e-08,-1.32294,-0.389152},{0.22049,-1.32294,-0.407203},{-0.440981,
-1.10245,-0.362509},{-0.22049,-1.10245,-0.312518},{-5.96046e-08,-1.10245,-0.29732},
{0.22049,-1.10245,-0.312518},{0.440981,-1.10245,-0.362509},{-0.661471,-0.881962,-0.408482},
{-0.440981,-0.881962,-0.309051},{-0.22049,-0.881962,-0.258789},{-5.96046e-08,-0.881962,
-0.243092},{0.22049,-0.881962,-0.258789},{0.440981,-0.881962,-0.309051},{0.661471,-0.881962,
-0.408482},{-0.661471,-0.661471,-0.423125},{-0.440981,-0.661471,-0.302316},{-0.22049,
-0.661471,-0.236663},{-5.96046e-08,-0.705914,-0.22049},{-0.065666,-0.661471,-0.22049},
{0.22049,-0.661471,-0.236663},{0.0656659,-0.661471,-0.22049},{0.440981,-0.661471,-0.302316},
{0.661471,-0.661471,-0.423125},{-0.440981,-0.440981,-0.416743},{-0.22049,-0.440981,
-0.31601},{-5.96046e-08,-0.610891,-0.22049},{-5.96046e-08,-0.440981,-0.275361},{0.22049,
-0.440981,-0.31601},{0.440981,-0.440981,-0.416743},{-5.96046e-08,-0.661471,-0.213146},
{-5.96046e-08,0.661471,0.213146},{-5.96046e-08,0.610891,0.22049},{-0.065666,0.661471,
0.22049},{0.0656659,0.661471,0.22049},{-5.96046e-08,0.705914,0.22049},{-0.440981,0.440981,
0.416743},{-0.440981,0.423175,0.440981},{-0.477541,0.440981,0.440981},{-0.22049,0.440981,
0.31601},{-0.22049,0.374341,0.440981},{-5.96046e-08,0.440981,0.275361},{-5.96046e-08,0.372711,
0.440981},{0.22049,0.440981,0.31601},{0.22049,0.374341,0.440981},{0.440981,0.440981,
0.416743},{0.440981,0.423175,0.440981},{0.477541,0.440981,0.440981},{-0.661471,0.661471,
0.423125},{-0.661471,0.62836,0.440981},{-0.688441,0.661471,0.440981},{-0.440981,0.661471,
0.302316},{-0.22049,0.661471,0.236663},{0.22049,0.661471,0.236663},{0.440981,0.661471,
0.302316},{0.661471,0.661471,0.423125},{0.661471,0.62836,0.440981},{0.688441,0.661471,
0.440981},{-0.661471,0.881962,0.408482},{-0.716394,0.881962,0.440981},{-0.440981,0.881962,
0.309051},{-0.22049,0.881962,0.258789},{-5.96046e-08,0.881962,0.243092},{0.22049,0.881962,
0.258789},{0.440981,0.881962,0.309051},{0.661471,0.881962,0.408482},{0.716394,0.881962,
0.440981},{-0.661471,1.01931,0.440981},{-0.440981,1.10245,0.362509},{-0.623351,1.10245,
0.440981},{-0.22049,1.10245,0.312517},{-5.96046e-08,1.10245,0.29732},{0.22049,1.10245,
0.312517},{0.440981,1.10245,0.362509},{0.661471,1.01931,0.440981},{0.62335,1.10245,
0.440981},{-0.440981,1.28026,0.440981},{-0.22049,1.32294,0.407203},{-0.355041,1.32294,
0.440981},{-5.96046e-08,1.32294,0.389152},{0.22049,1.32294,0.407203},{0.440981,1.28026,
0.440981},{0.355041,1.32294,0.440981},{-0.22049,1.38507,0.440981},{-5.96046e-08,1.41687,
0.440981},{0.22049,1.38507,0.440981},{-0.440981,0.440981,0.58988},{-0.22049,0.43748,
0.661471},{-0.290147,0.440981,0.661471},{-5.96046e-08,0.440981,0.648254},{-0.138576,
0.440981,0.661471},{0.22049,0.43748,0.661471},{0.138576,0.440981,0.661471},{0.440981,
0.440981,0.58988},{0.290146,0.440981,0.661471},{-0.661471,0.545434,0.661471},{-0.793884,
0.661471,0.661471},{-0.440981,0.449486,0.661471},{-5.96046e-08,0.445349,0.661471},
{0.440981,0.449486,0.661471},{0.661471,0.545434,0.661471},{0.793884,0.661471,0.661471},
{-0.866016,0.881962,0.661471},{0.866015,0.881962,0.661471},{-0.834766,1.10245,0.661471},
{-0.661471,1.10245,0.475629},{0.661471,1.10245,0.475629},{0.834766,1.10245,0.661471},
{-0.661471,1.32294,0.629136},{-0.698208,1.32294,0.661471},{-0.440981,1.32294,0.476624},
{0.440981,1.32294,0.476624},{0.661471,1.32294,0.629136},{0.698208,1.32294,0.661471},
{-0.661471,1.36064,0.661471},{-0.440981,1.51736,0.661471},{-0.368978,1.54343,0.661471},
{-0.22049,1.54343,0.60318},{-5.96046e-08,1.54343,0.574738},{0.22049,1.54343,0.60318},
{0.368978,1.54343,0.661471},{0.440981,1.51736,0.661471},{0.661471,1.36064,0.661471},
{-0.22049,1.59977,0.661471},{-5.96046e-08,1.626,0.661471},{0.22049,1.59977,0.661471},
{-0.22049,0.440981,0.668429},{0.22049,0.440981,0.668429},{-0.661471,0.636909,0.881962},
{-0.700625,0.661471,0.881962},{-0.440981,0.562388,0.881962},{-0.22049,0.550668,0.881962},
{-5.96046e-08,0.553693,0.881962},{0.22049,0.550668,0.881962},{0.440981,0.562388,0.881962},
{0.661471,0.636909,0.881962},{0.700625,0.661471,0.881962},{-0.851168,0.881962,0.881962},
{0.851168,0.881962,0.881962},{-0.852991,1.10245,0.881962},{0.85299,1.10245,0.881962},
{-0.751199,1.32294,0.881962},{0.751199,1.32294,0.881962},{-0.661471,1.4221,0.881962},
{-0.499928,1.54343,0.881962},{-0.440981,1.54343,0.765255},{0.440981,1.54343,0.765255},
{0.499928,1.54343,0.881962},{0.661471,1.4221,0.881962},{-0.440981,1.57994,0.881962},
{-0.22049,1.66009,0.881962},{-5.96046e-08,1.68399,0.881962},{0.22049,1.66009,0.881962},
{0.440981,1.57994,0.881962},{-0.661471,0.661471,0.916238},{-0.440981,0.661471,1.0145},
{-0.22049,0.661471,1.03693},{-5.96046e-08,0.661471,1.03828},{0.22049,0.661471,1.03693},
{0.440981,0.661471,1.0145},{0.661471,0.661471,0.916238},{-0.661471,0.881962,1.09199},
{-0.440981,0.775249,1.10245},{-0.642796,0.881962,1.10245},{-0.22049,0.741727,1.10245},
{-5.96046e-08,0.737168,1.10245},{0.22049,0.741727,1.10245},{0.440981,0.775249,1.10245},
{0.661471,0.881962,1.09199},{0.642796,0.881962,1.10245},{-0.661471,0.927502,1.10245},
{-0.714189,1.10245,1.10245},{0.661471,0.927502,1.10245},{0.714189,1.10245,1.10245},
{-0.661471,1.24738,1.10245},{-0.661471,1.32294,1.05643},{-0.628446,1.32294,1.10245},
{0.661471,1.24738,1.10245},{0.661471,1.32294,1.05643},{0.628446,1.32294,1.10245},{-0.440981,
1.4932,1.10245},{-0.440981,1.54343,0.992782},{-0.340606,1.54343,1.10245},{0.440981,
1.4932,1.10245},{0.440981,1.54343,0.992782},{0.340606,1.54343,1.10245},{-0.22049,1.59178,
1.10245},{-5.96046e-08,1.62045,1.10245},{0.22049,1.59178,1.10245},{-0.440981,0.881962,
1.18892},{-0.22049,0.881962,1.22592},{-5.96046e-08,0.881962,1.23501},{0.22049,0.881962,
1.22592},{0.440981,0.881962,1.18892},{-0.661471,1.10245,1.14515},{-0.440981,1.10245,
1.25567},{-0.22049,1.10245,1.3021},{-5.96046e-08,1.10245,1.31475},{0.22049,1.10245,
1.3021},{0.440981,1.10245,1.25567},{0.661471,1.10245,1.14515},{-0.440981,1.32294,1.22625},
{-0.22049,1.32294,1.29374},{-5.96046e-08,1.32294,1.31281},{0.22049,1.32294,1.29374},
{0.440981,1.32294,1.22625},{-0.22049,1.54343,1.15917},{-5.96046e-08,1.54343,1.19185},
{0.22049,1.54343,1.15917}
};
	int connect<NEportLevels={2,0}>[2112] = {
   0,1,2,3,4,1,3,1,0,5,6,4,5,4,3,7,6,5,8,9,10,11,0,2,11,2,9,11,9,8,12,3,
0,12,0,11,13,5,3,13,3,12,14,15,7,14,7,5,14,5,13,16,15,14,17,18,19,20,8,
10,20,10,18,20,18,17,21,11,8,21,8,20,22,12,11,22,11,21,23,13,12,23,12,22,
24,14,13,24,13,23,25,26,16,25,16,14,25,14,24,27,26,25,28,17,19,29,20,17,
29,17,28,29,28,30,31,21,20,31,20,29,32,22,21,32,21,31,33,23,22,33,22,32,
34,24,23,34,23,33,35,25,24,35,24,34,35,34,36,35,27,25,37,29,30,38,31,29,
38,29,37,39,32,31,39,31,38,40,33,32,40,32,39,41,34,33,41,33,40,41,36,34,
42,43,44,2,1,45,2,45,43,2,43,42,1,4,46,1,46,45,4,6,47,4,47,46,6,7,48,6,
48,49,6,49,47,50,49,48,51,52,53,10,9,42,10,42,44,10,44,51,44,52,51,9,2,
42,7,15,48,15,16,54,15,54,55,15,55,48,55,50,48,56,55,54,19,18,51,19,51,
53,19,53,57,18,10,51,16,26,54,26,27,58,26,58,56,26,56,54,28,19,57,28,57,
59,28,59,60,30,28,60,35,36,61,27,35,61,27,61,62,27,62,58,63,60,59,63,59,
64,37,30,60,37,60,63,37,63,65,38,37,65,38,65,66,39,38,66,39,66,67,40,39,
67,40,67,68,41,40,68,41,68,69,36,41,69,36,69,70,36,70,61,71,62,61,71,61,
70,72,63,64,73,65,63,73,63,72,74,66,65,74,65,73,75,67,66,75,66,74,76,68,
67,76,67,75,77,69,68,77,68,76,78,70,69,78,69,77,78,71,70,44,43,79,43,45,
80,43,80,81,43,81,79,45,46,82,45,82,80,46,47,83,46,83,82,47,49,84,47,84,
85,47,85,83,49,50,84,53,52,86,53,86,87,52,44,79,52,79,88,52,88,86,81,88,
79,84,89,85,50,55,90,50,90,89,50,89,84,55,56,91,55,91,90,57,53,87,57,87,
92,56,58,93,56,93,91,59,57,92,59,92,94,58,62,95,58,95,93,64,59,94,64,94,
96,62,71,97,62,97,95,72,64,96,72,96,98,73,72,98,73,98,99,74,73,99,74,99,
100,74,100,101,75,74,101,75,101,102,75,102,103,76,75,103,76,103,104,76,
104,105,77,76,105,77,105,106,77,106,107,78,77,107,78,107,108,71,78,108,71,
108,97,109,101,100,109,102,101,110,105,104,110,106,105,81,80,111,80,82,
112,80,112,111,82,83,113,82,113,112,83,85,113,87,86,114,86,88,115,86,115,
114,88,81,111,88,111,116,88,116,115,116,117,115,112,118,116,112,116,111,
113,119,118,113,118,112,85,89,120,85,120,121,85,121,113,121,119,113,89,
90,122,89,122,120,90,91,122,92,87,114,92,114,123,115,124,125,115,125,123,
115,123,114,117,124,115,120,126,121,122,127,128,122,128,126,122,126,120,
91,93,127,91,127,122,94,92,123,94,123,129,94,129,130,125,129,123,127,131,
128,93,95,132,93,132,131,93,131,127,96,94,130,96,130,133,95,97,134,95,134,
132,98,96,133,98,133,135,99,98,135,99,135,136,99,136,137,100,99,137,103,
102,138,104,103,138,107,106,139,108,107,139,108,139,140,108,140,141,97,
108,141,97,141,134,142,137,136,109,100,137,109,137,142,109,142,143,102,109,
143,102,143,144,102,144,138,110,104,138,110,138,144,110,144,145,106,110,
145,106,145,146,106,146,139,146,140,139,117,116,147,116,118,148,116,148,
147,118,119,149,118,149,148,119,121,149,125,124,150,124,117,147,124,147,
151,124,151,150,148,152,151,148,151,147,149,153,152,149,152,148,121,126,
154,121,154,153,121,153,149,126,128,154,130,129,155,129,125,150,129,150,
156,129,156,155,151,157,156,151,156,150,152,158,157,152,157,151,153,159,
158,153,158,152,154,160,159,154,159,153,128,131,161,128,161,160,128,160,
154,131,132,161,133,130,155,133,155,162,156,163,162,156,162,155,157,164,
163,157,163,156,158,165,166,158,166,164,158,164,157,159,167,168,159,168,
165,159,165,158,160,169,167,160,167,159,161,170,169,161,169,160,132,134,
170,132,170,161,135,133,162,136,135,162,136,162,163,136,163,171,164,172,
171,164,171,163,173,174,172,173,172,164,173,164,166,167,175,174,167,174,
173,167,173,168,169,176,175,169,175,167,141,140,176,141,176,169,141,169,
170,134,141,170,142,136,171,143,142,171,143,171,172,144,143,172,144,172,
174,145,144,174,145,174,175,146,145,175,146,175,176,140,146,176,166,165,
177,165,168,177,173,166,177,168,173,177,178,179,180,181,179,178,182,178,
180,182,181,178,183,184,185,186,187,184,186,184,183,188,189,187,188,187,
186,190,191,189,190,189,188,192,193,191,192,191,190,194,193,192,195,196,
197,198,183,185,198,185,196,198,196,195,199,186,183,199,183,198,180,179,
188,180,188,186,180,186,199,179,181,200,179,200,190,179,190,188,201,192,
190,201,190,200,202,203,194,202,194,192,202,192,201,204,203,202,205,195,
197,205,197,206,207,198,195,207,195,205,208,199,198,208,198,207,182,180,
199,182,199,208,182,208,209,181,182,209,181,209,210,181,210,200,211,201,
200,211,200,210,212,202,201,212,201,211,213,204,202,213,202,212,214,205,
206,215,207,205,215,205,214,215,214,216,217,208,207,217,207,215,218,209,
208,218,208,217,219,210,209,219,209,218,220,211,210,220,210,219,221,212,
211,221,211,220,221,220,222,221,213,212,223,215,216,224,217,215,224,215,
223,224,223,225,226,218,217,226,217,224,227,219,218,227,218,226,228,220,
219,228,219,227,228,227,229,228,222,220,230,224,225,231,226,224,231,224,
230,232,227,226,232,226,231,232,229,227,185,184,233,184,187,234,184,234,
235,184,235,233,187,189,236,187,236,237,187,237,234,189,191,238,189,238,
239,189,239,236,191,193,240,191,240,241,191,241,238,193,194,240,197,196,
242,197,242,243,196,185,233,196,233,244,196,244,242,235,244,233,236,245,
237,239,245,236,240,246,241,194,203,247,194,247,246,194,246,240,203,204,
248,203,248,247,206,197,243,206,243,249,204,213,250,204,250,248,214,206,
249,214,249,251,214,251,252,216,214,252,221,222,253,213,221,253,213,253,
254,213,254,250,255,252,251,255,251,256,223,216,252,223,252,255,223,255,
257,225,223,257,228,229,258,222,228,258,222,258,259,222,259,253,260,254,
253,260,253,259,261,255,256,262,257,255,262,255,261,230,225,257,230,257,
262,230,262,264,262,263,264,231,230,264,231,264,265,232,231,265,232,265,
266,229,232,266,229,266,267,229,267,258,267,268,258,269,259,258,269,258,
268,269,260,259,270,264,263,271,265,264,271,264,270,272,266,265,272,265,
271,272,267,266,235,234,273,234,237,273,239,238,274,238,241,274,243,242,
275,243,275,276,242,244,277,242,277,275,244,235,273,244,273,278,244,278,
277,237,245,279,237,279,278,237,278,273,245,239,274,245,274,280,245,280,
279,241,246,281,241,281,280,241,280,274,246,247,282,246,282,281,247,248,
283,247,283,282,249,243,276,249,276,284,248,250,285,248,285,283,251,249,
284,251,284,286,250,254,287,250,287,285,256,251,286,256,286,288,254,260,
289,254,289,287,261,256,288,261,288,290,262,261,290,262,290,291,262,291,
292,263,262,292,268,267,293,269,268,293,269,293,294,269,294,295,260,269,
295,260,295,289,296,292,291,270,263,292,270,292,296,270,296,297,271,270,
297,271,297,298,272,271,298,272,298,299,267,272,299,267,299,300,267,300,
293,300,294,293,276,275,301,275,277,302,275,302,301,277,278,303,277,303,
302,278,279,304,278,304,303,279,280,305,279,305,304,280,281,306,280,306,
305,281,282,307,281,307,306,282,283,307,284,276,301,284,301,308,302,309,
310,302,310,308,302,308,301,303,311,309,303,309,302,304,312,311,304,311,
303,305,313,312,305,312,304,306,314,313,306,313,305,307,315,316,307,316,
314,307,314,306,283,285,315,283,315,307,286,284,308,286,308,317,286,317,
318,310,317,308,315,319,316,285,287,320,285,320,319,285,319,315,288,286,
318,288,318,321,288,321,322,321,323,322,324,325,326,287,289,325,287,325,
324,287,324,320,290,288,322,291,290,322,291,322,323,291,323,328,323,327,
328,327,329,328,330,331,332,295,294,331,295,331,330,295,330,325,330,326,
325,289,295,325,296,291,328,297,296,328,297,328,329,297,329,333,298,297,
333,298,333,334,299,298,334,299,334,335,300,299,335,300,335,332,300,332,
331,294,300,331,310,309,336,309,311,337,309,337,336,311,312,338,311,338,
337,312,313,339,312,339,338,313,314,340,313,340,339,314,316,340,318,317,
341,317,310,336,317,336,342,317,342,341,337,343,342,337,342,336,338,344,
343,338,343,337,339,345,344,339,344,338,340,346,345,340,345,339,316,319,
347,316,347,346,316,346,340,319,320,347,321,318,341,323,321,341,323,341,
342,323,342,348,343,349,348,343,348,342,344,350,349,344,349,343,345,351,
350,345,350,344,346,352,351,346,351,345,324,326,352,324,352,346,324,346,
347,320,324,347,327,323,348,329,327,348,329,348,349,329,349,353,350,354,
353,350,353,349,351,355,354,351,354,350,330,332,355,330,355,351,330,351,
352,326,330,352,333,329,353,334,333,353,334,353,354,335,334,354,335,354,
355,332,335,355
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	  xform {
	    mat = {
	      {1.0, 0.0, 0.0, 0.0},
	      {0.0, 1.0, 0.0, 0.0},
	      {0.0, 0.0, 1.0, 0.0},
	      {0.0, 0.0, 0.0, 1.3}
	    };
	  };
	};
      };
      group dn_mesh {
	float coord<NEportLevels={2,0}>[356][3] = {
   {-0.440981,0.881962,-1.18892},{-0.440981,0.775249,-1.10245},{-0.642796,0.881962,
-1.10245},{-0.22049,0.881962,-1.22592},{-0.22049,0.741727,-1.10245},{-5.96046e-08,0.881962,
-1.23501},{-5.96046e-08,0.737168,-1.10245},{0.22049,0.881962,-1.22592},{0.22049,0.741727,
-1.10245},{0.440981,0.881962,-1.18892},{0.440981,0.775249,-1.10245},{0.642796,0.881962,
-1.10245},{-0.661471,1.10245,-1.14516},{-0.661471,0.927502,-1.10245},{-0.714189,1.10245,
-1.10245},{-0.440981,1.10245,-1.25567},{-0.22049,1.10245,-1.3021},{-5.96046e-08,1.10245,
-1.31475},{0.22049,1.10245,-1.3021},{0.440981,1.10245,-1.25567},{0.661471,1.10245,
-1.14516},{0.661471,0.927502,-1.10245},{0.714189,1.10245,-1.10245},{-0.661471,1.24738,
-1.10245},{-0.440981,1.32294,-1.22625},{-0.628446,1.32294,-1.10245},{-0.22049,1.32294,
-1.29374},{-5.96046e-08,1.32294,-1.31281},{0.22049,1.32294,-1.29374},{0.440981,1.32294,
-1.22625},{0.661471,1.24738,-1.10245},{0.628446,1.32294,-1.10245},{-0.440981,1.4932,
-1.10245},{-0.22049,1.54343,-1.15917},{-0.340606,1.54343,-1.10245},{-5.96046e-08,1.54343,
-1.19185},{0.22049,1.54343,-1.15917},{0.440981,1.4932,-1.10245},{0.340606,1.54343,
-1.10245},{-0.22049,1.59178,-1.10245},{-5.96046e-08,1.62045,-1.10245},{0.22049,1.59178,
-1.10245},{-0.661471,0.661471,-0.916238},{-0.661471,0.636909,-0.881962},{-0.700625,0.661471,
-0.881962},{-0.440981,0.661471,-1.0145},{-0.440981,0.562388,-0.881962},{-0.22049,0.661471,
-1.03693},{-0.22049,0.550668,-0.881962},{-5.96046e-08,0.661471,-1.03828},{-5.96046e-08,
0.553693,-0.881962},{0.22049,0.661471,-1.03693},{0.22049,0.550668,-0.881962},{0.440981,
0.661471,-1.0145},{0.440981,0.562388,-0.881962},{0.661471,0.661471,-0.916238},{0.661471,
0.636909,-0.881962},{0.700625,0.661471,-0.881962},{-0.661471,0.881962,-1.09199},{-0.851168,
0.881962,-0.881962},{0.661471,0.881962,-1.09199},{0.851168,0.881962,-0.881962},{-0.852991,
1.10245,-0.881962},{0.85299,1.10245,-0.881962},{-0.751199,1.32294,-0.881962},{-0.661471,
1.32294,-1.05643},{0.661471,1.32294,-1.05643},{0.751199,1.32294,-0.881962},{-0.661471,
1.4221,-0.881962},{-0.499928,1.54343,-0.881962},{-0.440981,1.54343,-0.992782},{0.440981,
1.54343,-0.992782},{0.499928,1.54343,-0.881962},{0.661471,1.4221,-0.881962},{-0.440981,
1.57994,-0.881962},{-0.22049,1.66009,-0.881962},{-5.96046e-08,1.68399,-0.881962},{0.22049,
1.66009,-0.881962},{0.440981,1.57994,-0.881962},{-0.22049,0.440981,-0.668429},{-0.22049,
0.43748,-0.661471},{-0.290147,0.440981,-0.661471},{-0.138576,0.440981,-0.661471},{0.22049,
0.440981,-0.668429},{0.22049,0.43748,-0.661471},{0.138576,0.440981,-0.661471},{0.290146,
0.440981,-0.661471},{-0.661471,0.545434,-0.661471},{-0.793884,0.661471,-0.661471},{-0.440981,
0.449486,-0.661471},{-5.96046e-08,0.445349,-0.661471},{0.440981,0.449486,-0.661471},
{0.661471,0.545434,-0.661471},{0.793884,0.661471,-0.661471},{-0.866016,0.881962,-0.661471},
{0.866015,0.881962,-0.661471},{-0.834766,1.10245,-0.661471},{0.834766,1.10245,-0.661471},
{-0.698208,1.32294,-0.661471},{0.698208,1.32294,-0.661471},{-0.661471,1.36064,-0.661471},
{-0.440981,1.51736,-0.661471},{-0.440981,1.54343,-0.765255},{-0.368978,1.54343,-0.661471},
{0.440981,1.51736,-0.661471},{0.440981,1.54343,-0.765255},{0.368978,1.54343,-0.661471},
{0.661471,1.36064,-0.661471},{-0.22049,1.59977,-0.661471},{-5.96046e-08,1.626,-0.661471},
{0.22049,1.59977,-0.661471},{-0.440981,0.440981,-0.58988},{-0.440981,0.423175,-0.440981},
{-0.477541,0.440981,-0.440981},{-0.22049,0.374341,-0.440981},{-5.96046e-08,0.440981,
-0.648254},{-5.96046e-08,0.372711,-0.440981},{0.22049,0.374341,-0.440981},{0.440981,
0.440981,-0.58988},{0.440981,0.423175,-0.440981},{0.477541,0.440981,-0.440981},{-0.661471,
0.62836,-0.440981},{-0.688441,0.661471,-0.440981},{0.661471,0.62836,-0.440981},{0.688441,
0.661471,-0.440981},{-0.716394,0.881962,-0.440981},{0.716394,0.881962,-0.440981},{-0.661471,
1.01931,-0.440981},{-0.661471,1.10245,-0.475629},{-0.623351,1.10245,-0.440981},{0.661471,
1.01931,-0.440981},{0.661471,1.10245,-0.475629},{0.62335,1.10245,-0.440981},{-0.661471,
1.32294,-0.629136},{-0.440981,1.28026,-0.440981},{-0.440981,1.32294,-0.476624},{-0.355041,
1.32294,-0.440981},{0.440981,1.28026,-0.440981},{0.440981,1.32294,-0.476624},{0.355041,
1.32294,-0.440981},{0.661471,1.32294,-0.629136},{-0.22049,1.38507,-0.440981},{-0.22049,
1.54343,-0.60318},{-5.96046e-08,1.41687,-0.440981},{-5.96046e-08,1.54343,-0.574738},
{0.22049,1.38507,-0.440981},{0.22049,1.54343,-0.60318},{-0.440981,0.440981,-0.416743},
{-0.22049,0.440981,-0.31601},{-5.96046e-08,0.440981,-0.275361},{0.22049,0.440981,-0.31601},
{0.440981,0.440981,-0.416743},{-0.661471,0.661471,-0.423125},{-0.440981,0.661471,-0.302316},
{-0.22049,0.661471,-0.236663},{-5.96046e-08,0.610891,-0.22049},{-0.065666,0.661471,
-0.22049},{0.22049,0.661471,-0.236663},{0.0656659,0.661471,-0.22049},{0.440981,0.661471,
-0.302316},{0.661471,0.661471,-0.423125},{-0.661471,0.881962,-0.408482},{-0.440981,0.881962,
-0.309051},{-0.22049,0.881962,-0.258789},{-5.96046e-08,0.705914,-0.22049},{-5.96046e-08,
0.881962,-0.243092},{0.22049,0.881962,-0.258789},{0.440981,0.881962,-0.309051},{0.661471,
0.881962,-0.408482},{-0.440981,1.10245,-0.362509},{-0.22049,1.10245,-0.312518},{-5.96046e-08,
1.10245,-0.29732},{0.22049,1.10245,-0.312518},{0.440981,1.10245,-0.362509},{-0.22049,
1.32294,-0.407203},{-5.96046e-08,1.32294,-0.389152},{0.22049,1.32294,-0.407203},{-5.96046e-08,
0.661471,-0.213146},{-5.96046e-08,-0.661471,0.213146},{-5.96046e-08,-0.705914,0.22049},
{-0.065666,-0.661471,0.22049},{0.0656659,-0.661471,0.22049},{-5.96046e-08,-0.610891,
0.22049},{-0.22049,-1.32294,0.407203},{-0.22049,-1.38507,0.440981},{-0.355041,-1.32294,
0.440981},{-5.96046e-08,-1.32294,0.389152},{-5.96046e-08,-1.41687,0.440981},{0.22049,
-1.32294,0.407203},{0.22049,-1.38507,0.440981},{0.355041,-1.32294,0.440981},{-0.440981,
-1.10245,0.362509},{-0.440981,-1.28026,0.440981},{-0.623351,-1.10245,0.440981},{-0.22049,
-1.10245,0.312517},{-5.96046e-08,-1.10245,0.29732},{0.22049,-1.10245,0.312517},{0.440981,
-1.10245,0.362509},{0.440981,-1.28026,0.440981},{0.62335,-1.10245,0.440981},{-0.661471,
-0.881962,0.408482},{-0.661471,-1.01931,0.440981},{-0.716394,-0.881962,0.440981},{-0.440981,
-0.881962,0.309051},{-0.22049,-0.881962,0.258789},{-5.96046e-08,-0.881962,0.243092},
{0.22049,-0.881962,0.258789},{0.440981,-0.881962,0.309051},{0.661471,-0.881962,0.408482},
{0.661471,-1.01931,0.440981},{0.716394,-0.881962,0.440981},{-0.661471,-0.661471,0.423125},
{-0.688441,-0.661471,0.440981},{-0.440981,-0.661471,0.302316},{-0.22049,-0.661471,0.236663},
{0.22049,-0.661471,0.236663},{0.440981,-0.661471,0.302316},{0.661471,-0.661471,0.423125},
{0.688441,-0.661471,0.440981},{-0.661471,-0.62836,0.440981},{-0.440981,-0.440981,0.416743},
{-0.477541,-0.440981,0.440981},{-0.22049,-0.440981,0.31601},{-5.96046e-08,-0.440981,
0.275361},{0.22049,-0.440981,0.31601},{0.440981,-0.440981,0.416743},{0.661471,-0.62836,
0.440981},{0.477541,-0.440981,0.440981},{-0.440981,-0.423175,0.440981},{-0.22049,-0.374341,
0.440981},{-5.96046e-08,-0.372711,0.440981},{0.22049,-0.374341,0.440981},{0.440981,
-0.423175,0.440981},{-0.22049,-1.54343,0.60318},{-0.22049,-1.59977,0.661471},{-0.368978,
-1.54343,0.661471},{-5.96046e-08,-1.54343,0.574738},{-5.96046e-08,-1.626,0.661471},
{0.22049,-1.54343,0.60318},{0.22049,-1.59977,0.661471},{0.368978,-1.54343,0.661471},
{-0.661471,-1.32294,0.629136},{-0.661471,-1.36064,0.661471},{-0.698208,-1.32294,0.661471},
{-0.440981,-1.32294,0.476624},{-0.440981,-1.51736,0.661471},{0.440981,-1.32294,0.476624},
{0.440981,-1.51736,0.661471},{0.661471,-1.32294,0.629136},{0.661471,-1.36064,0.661471},
{0.698208,-1.32294,0.661471},{-0.661471,-1.10245,0.475629},{-0.834766,-1.10245,0.661471},
{0.661471,-1.10245,0.475629},{0.834766,-1.10245,0.661471},{-0.866016,-0.881962,0.661471},
{0.866015,-0.881962,0.661471},{-0.793884,-0.661471,0.661471},{0.793884,-0.661471,0.661471},
{-0.661471,-0.545434,0.661471},{-0.440981,-0.449486,0.661471},{-0.440981,-0.440981,
0.58988},{-0.290147,-0.440981,0.661471},{-5.96046e-08,-0.445349,0.661471},{-5.96046e-08,
-0.440981,0.648254},{-0.138576,-0.440981,0.661471},{0.138576,-0.440981,0.661471},{0.440981,
-0.449486,0.661471},{0.440981,-0.440981,0.58988},{0.290146,-0.440981,0.661471},{0.661471,
-0.545434,0.661471},{-0.22049,-0.43748,0.661471},{0.22049,-0.43748,0.661471},{-0.440981,
-1.54343,0.765255},{-0.440981,-1.57994,0.881962},{-0.499928,-1.54343,0.881962},{-0.22049,
-1.66009,0.881962},{-5.96046e-08,-1.68399,0.881962},{0.22049,-1.66009,0.881962},{0.440981,
-1.54343,0.765255},{0.440981,-1.57994,0.881962},{0.499928,-1.54343,0.881962},{-0.661471,
-1.4221,0.881962},{-0.751199,-1.32294,0.881962},{0.661471,-1.4221,0.881962},{0.751199,
-1.32294,0.881962},{-0.852991,-1.10245,0.881962},{0.85299,-1.10245,0.881962},{-0.851168,
-0.881962,0.881962},{0.851168,-0.881962,0.881962},{-0.700625,-0.661471,0.881962},{0.700625,
-0.661471,0.881962},{-0.661471,-0.636909,0.881962},{-0.440981,-0.562388,0.881962},
{-0.22049,-0.550669,0.881962},{-0.22049,-0.440981,0.668429},{-5.96046e-08,-0.553693,
0.881962},{0.22049,-0.550669,0.881962},{0.22049,-0.440981,0.668429},{0.440981,-0.562388,
0.881962},{0.661471,-0.636909,0.881962},{-0.440981,-1.54343,0.992782},{-0.22049,-1.59178,
1.10245},{-0.340606,-1.54343,1.10245},{-5.96046e-08,-1.62045,1.10245},{0.22049,-1.59178,
1.10245},{0.440981,-1.54343,0.992782},{0.340606,-1.54343,1.10245},{-0.661471,-1.32294,
1.05643},{-0.440981,-1.4932,1.10245},{-0.628446,-1.32294,1.10245},{0.440981,-1.4932,
1.10245},{0.661471,-1.32294,1.05643},{0.628446,-1.32294,1.10245},{-0.661471,-1.24738,
1.10245},{-0.714189,-1.10245,1.10245},{0.661471,-1.24738,1.10245},{0.714189,-1.10245,
1.10245},{-0.661471,-0.927502,1.10245},{-0.661471,-0.881962,1.09199},{-0.642796,-0.881962,
1.10245},{0.661471,-0.927502,1.10245},{0.661471,-0.881962,1.09199},{0.642796,-0.881962,
1.10245},{-0.661471,-0.661471,0.916238},{-0.440981,-0.775249,1.10245},{-0.440981,-0.661471,
1.0145},{-0.22049,-0.741727,1.10245},{-0.22049,-0.661471,1.03693},{-5.96046e-08,-0.737168,
1.10245},{-5.96046e-08,-0.661471,1.03828},{0.22049,-0.741727,1.10245},{0.22049,-0.661471,
1.03693},{0.440981,-0.775249,1.10245},{0.440981,-0.661471,1.0145},{0.661471,-0.661471,
0.916238},{-0.22049,-1.54343,1.15917},{-5.96046e-08,-1.54343,1.19185},{0.22049,-1.54343,
1.15917},{-0.440981,-1.32294,1.22625},{-0.22049,-1.32294,1.29374},{-5.96046e-08,-1.32294,
1.31281},{0.22049,-1.32294,1.29374},{0.440981,-1.32294,1.22625},{-0.661471,-1.10245,
1.14515},{-0.440981,-1.10245,1.25567},{-0.22049,-1.10245,1.3021},{-5.96046e-08,-1.10245,
1.31475},{0.22049,-1.10245,1.3021},{0.440981,-1.10245,1.25567},{0.661471,-1.10245,1.14515},
{-0.440981,-0.881962,1.18892},{-0.22049,-0.881962,1.22592},{-5.96046e-08,-0.881962,
1.23501},{0.22049,-0.881962,1.22592},{0.440981,-0.881962,1.18892}
};
	int connect<NEportLevels={2,0}>[2112] = {
   0,1,2,3,4,1,3,1,0,5,6,4,5,4,3,7,8,6,7,6,5,9,10,8,9,8,7,11,10,9,12,13,
14,15,0,2,15,2,13,15,13,12,16,3,0,16,0,15,17,5,3,17,3,16,18,7,5,18,5,17,
19,9,7,19,7,18,20,21,11,20,11,9,20,9,19,22,21,20,23,12,14,24,15,12,24,12,
23,24,23,25,26,16,15,26,15,24,27,17,16,27,16,26,28,18,17,28,17,27,29,19,
18,29,18,28,30,20,19,30,19,29,30,29,31,30,22,20,32,24,25,33,26,24,33,24,
32,33,32,34,35,27,26,35,26,33,36,28,27,36,27,35,37,29,28,37,28,36,37,36,
38,37,31,29,39,33,34,40,35,33,40,33,39,41,36,35,41,35,40,41,38,36,42,43,
44,45,46,43,45,43,42,47,48,46,47,46,45,49,50,48,49,48,47,51,52,50,51,50,
49,53,54,52,53,52,51,55,56,54,55,54,53,57,56,55,58,42,44,58,44,59,2,1,
45,2,45,42,2,42,58,1,4,47,1,47,45,4,6,49,4,49,47,6,8,51,6,51,49,8,10,53,
8,53,51,10,11,60,10,60,55,10,55,53,61,57,55,61,55,60,14,13,58,14,58,59,
14,59,62,13,2,58,11,21,60,21,22,63,21,63,61,21,61,60,23,14,62,23,62,64,
23,64,65,25,23,65,30,31,66,22,30,66,22,66,67,22,67,63,68,65,64,32,25,65,
32,65,68,32,68,70,68,69,70,34,32,70,37,38,71,31,37,71,31,71,72,31,72,66,
72,73,66,73,67,66,74,70,69,39,34,70,39,70,74,39,74,75,40,39,75,40,75,76,
41,40,76,41,76,77,38,41,77,38,77,78,38,78,71,78,72,71,79,80,81,82,80,79,
83,84,85,86,84,83,44,43,87,44,87,88,43,46,89,43,89,87,46,48,79,46,79,81,
46,81,89,48,50,90,48,90,82,48,82,79,50,52,83,50,83,85,50,85,90,52,54,91,
52,91,86,52,86,83,54,56,92,54,92,91,56,57,93,56,93,92,59,44,88,59,88,94,
57,61,95,57,95,93,62,59,94,62,94,96,61,63,97,61,97,95,64,62,96,64,96,98,
63,67,99,63,99,97,68,64,98,68,98,100,69,68,100,69,100,101,69,101,102,101,
103,102,104,105,106,73,72,105,73,105,104,73,104,107,67,73,107,67,107,99,
74,69,102,75,74,102,75,102,103,75,103,108,76,75,108,76,108,109,77,76,109,
77,109,110,78,77,110,78,110,106,78,106,105,72,78,105,111,112,113,81,80,
114,81,114,112,81,112,111,80,82,115,80,115,116,80,116,114,85,84,117,85,
117,116,85,116,115,84,86,118,84,118,119,84,119,117,120,119,118,88,87,121,
88,121,122,87,89,111,87,111,113,87,113,121,89,81,111,82,90,115,90,85,115,
86,91,118,91,92,123,91,123,120,91,120,118,92,93,124,92,124,123,94,88,122,
94,122,125,93,95,126,93,126,124,96,94,125,96,125,127,96,127,128,127,129,
128,130,131,132,95,97,131,95,131,130,95,130,126,98,96,128,98,128,133,134,
135,133,134,133,128,134,128,129,134,136,135,137,138,139,131,140,138,131,
138,137,131,137,132,97,99,140,97,140,131,100,98,133,101,100,133,101,133,
135,103,101,135,103,135,136,103,136,142,136,141,142,143,144,142,143,142,
141,145,146,144,145,144,143,104,106,146,104,146,145,104,145,138,145,139,
138,107,104,138,107,138,140,99,107,140,108,103,142,109,108,142,109,142,144,
110,109,144,110,144,146,106,110,146,113,112,147,112,114,148,112,148,147,
114,116,149,114,149,148,116,117,150,116,150,149,117,119,151,117,151,150,
119,120,151,122,121,152,121,113,147,121,147,153,121,153,152,148,154,153,
148,153,147,149,155,156,149,156,154,149,154,148,150,157,158,150,158,155,
150,155,149,151,159,157,151,157,150,120,123,160,120,160,159,120,159,151,
123,124,160,125,122,152,125,152,161,153,162,161,153,161,152,154,163,162,
154,162,153,164,165,163,164,163,154,164,154,156,157,166,165,157,165,164,
157,164,158,159,167,166,159,166,157,160,168,167,160,167,159,124,126,168,
124,168,160,127,125,161,129,127,161,129,161,162,129,162,169,163,170,169,
163,169,162,165,171,170,165,170,163,166,172,171,166,171,165,167,173,172,
167,172,166,130,132,173,130,173,167,130,167,168,126,130,168,134,129,169,
136,134,169,136,169,170,136,170,174,171,175,174,171,174,170,172,176,175,
172,175,171,137,139,176,137,176,172,137,172,173,132,137,173,141,136,174,
143,141,174,143,174,175,145,143,175,145,175,176,139,145,176,156,155,177,
155,158,177,164,156,177,158,164,177,178,179,180,181,179,178,182,178,180,
182,181,178,183,184,185,186,187,184,186,184,183,188,189,187,188,187,186,
190,189,188,191,192,193,194,183,185,194,185,192,194,192,191,195,186,183,
195,183,194,196,188,186,196,186,195,197,198,190,197,190,188,197,188,196,
199,198,197,200,201,202,203,191,193,203,193,201,203,201,200,204,194,191,
204,191,203,205,195,194,205,194,204,206,196,195,206,195,205,207,197,196,
207,196,206,208,209,199,208,199,197,208,197,207,210,209,208,211,200,202,
211,202,212,213,203,200,213,200,211,214,204,203,214,203,213,180,179,205,
180,205,204,180,204,214,179,181,215,179,215,206,179,206,205,216,207,206,
216,206,215,217,208,207,217,207,216,218,210,208,218,208,217,219,211,212,
220,213,211,220,211,219,220,219,221,222,214,213,222,213,220,182,180,214,
182,214,222,182,222,223,181,182,223,181,223,224,181,224,215,225,216,215,
225,215,224,226,217,216,226,216,225,226,225,227,226,218,217,228,220,221,
229,222,220,229,220,228,230,223,222,230,222,229,231,224,223,231,223,230,
232,225,224,232,224,231,232,227,225,233,234,235,236,237,234,236,234,233,
238,239,237,238,237,236,240,239,238,241,242,243,244,245,242,244,242,241,
185,184,233,185,233,235,185,235,244,235,245,244,184,187,236,184,236,233,
187,189,238,187,238,236,189,190,246,189,246,247,189,247,238,247,240,238,
248,249,247,248,247,246,250,249,248,251,241,243,251,243,252,193,192,244,
193,244,241,193,241,251,192,185,244,190,198,246,198,199,253,198,253,248,
198,248,246,254,250,248,254,248,253,202,201,251,202,251,252,202,252,255,
201,193,251,199,209,253,209,210,256,209,256,254,209,254,253,212,202,255,
212,255,257,210,218,258,210,258,256,219,212,257,219,257,259,221,219,259,
221,259,260,221,260,261,260,262,261,263,264,265,263,266,264,267,268,269,
226,227,268,226,268,267,226,267,270,218,226,270,218,270,258,228,221,261,
229,228,261,229,261,262,229,262,271,230,229,271,230,271,265,230,265,264,
231,230,264,231,264,266,231,266,272,232,231,272,232,272,269,232,269,268,
227,232,268,273,274,275,235,234,276,235,276,274,235,274,273,234,237,277,
234,277,276,237,239,278,237,278,277,239,240,279,239,279,280,239,280,278,
281,280,279,243,242,282,243,282,283,242,245,273,242,273,275,242,275,282,
245,235,273,240,247,279,247,249,284,247,284,281,247,281,279,249,250,285,
249,285,284,252,243,283,252,283,286,250,254,287,250,287,285,255,252,286,
255,286,288,254,256,289,254,289,287,257,255,288,257,288,290,256,258,291,
256,291,289,259,257,290,259,290,292,260,259,292,260,292,293,262,260,293,
262,293,294,262,294,295,263,265,295,263,295,294,263,294,296,266,263,296,
266,296,297,266,297,298,267,269,298,267,298,297,267,297,299,270,267,299,
270,299,300,258,270,300,258,300,291,271,262,295,265,271,295,272,266,298,
269,272,298,275,274,301,274,276,302,274,302,303,274,303,301,276,277,304,
276,304,302,277,278,305,277,305,304,278,280,306,278,306,307,278,307,305,
280,281,306,283,282,308,282,275,301,282,301,309,282,309,308,309,310,308,
303,309,301,306,311,307,281,284,312,281,312,313,281,313,306,313,311,306,
284,285,312,286,283,308,286,308,314,286,314,315,310,314,308,312,316,313,
285,287,317,285,317,316,285,316,312,288,286,315,288,315,318,288,318,319,
318,320,319,321,322,323,287,289,322,287,322,321,287,321,317,290,288,319,
290,319,324,325,326,324,325,324,319,325,319,320,327,328,326,327,326,325,
329,330,328,329,328,327,331,332,330,331,330,329,333,334,332,333,332,331,
322,335,334,322,334,333,322,333,323,289,291,335,289,335,322,292,290,324,
293,292,324,293,324,326,294,293,326,294,326,328,296,294,328,296,328,330,
297,296,330,297,330,332,299,297,332,299,332,334,300,299,334,300,334,335,
291,300,335,303,302,336,302,304,337,302,337,336,304,305,338,304,338,337,
305,307,338,310,309,339,309,303,336,309,336,340,309,340,339,337,341,340,
337,340,336,338,342,341,338,341,337,307,311,343,307,343,342,307,342,338,
311,313,343,315,314,344,314,310,339,314,339,345,314,345,344,340,346,345,
340,345,339,341,347,346,341,346,340,342,348,347,342,347,341,343,349,348,
343,348,342,313,316,350,313,350,349,313,349,343,316,317,350,318,315,344,
320,318,344,320,344,345,320,345,351,346,352,351,346,351,345,347,353,352,
347,352,346,348,354,353,348,353,347,349,355,354,349,354,348,321,323,355,
321,355,349,321,349,350,317,321,350,325,320,351,327,325,351,327,351,352,
329,327,352,329,352,353,331,329,353,331,353,354,333,331,354,333,354,355,
323,333,355
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	  xform {
	    mat = {
	      {1.0, 0.0, 0.0, 0.0},
	      {0.0, 1.0, 0.0, 0.0},
	      {0.0, 0.0, 1.0, 0.0},
	      {0.0, 0.0, 0.0, 1.3}
	    };
	  };
	};
      };
      dlv_glyph pglyph {
	in_field => <-.<-.orbs.orbital[6];
	in_glyph => <-.dp_mesh.out;
	GlyphParam {
	  map_comp = 1;
	  scale => <-.<-.<-.scale;
	};
      };
      dlv_glyph nglyph {
	in_field => <-.<-.orbs.orbital[6];
	in_glyph => dn_mesh.out;
	GlyphParam {
	  map_comp = 2;
	  scale => <-.<-.<-.scale;
	};
      };
    };
    GMOD.instancer dx2y2_instancer {
      Value => (<-.orbs.orbital[7].nnodes > 0);
      Group => <-.dx2y2_orb;
      active = 2;
    };
    macro dx2y2_orb<instanced=0> {
      // grids cut out of AVS with $get_array then attached to a tri_mesh.
      group dp_mesh {
	float coord<NEportLevels={2,0}>[268][3] = {
   {-1.32294,-0.22049,-0.458679},{-1.32294,-0.245337,-0.440981},{-1.39787,-0.22049,
-0.440981},{-1.20262,-0.22049,-0.440981},{1.32294,-0.22049,-0.458679},{1.32294,-0.245337,
-0.440981},{1.20262,-0.22049,-0.440981},{1.39787,-0.22049,-0.440981},{-1.54343,-5.96046e-08,
-0.455658},{-1.54343,-0.0596052,-0.440981},{-1.56925,-5.96046e-08,-0.440981},{-1.32294,
-5.96046e-08,-0.513403},{-1.10245,-5.96046e-08,-0.490312},{-1.10245,-0.178974,-0.440981},
{-1.00217,-5.96046e-08,-0.440981},{1.10245,-5.96046e-08,-0.490312},{1.10245,-0.178974,
-0.440981},{1.00217,-5.96046e-08,-0.440981},{1.32294,-5.96046e-08,-0.513403},{1.54343,
-5.96046e-08,-0.455658},{1.54343,-0.0596052,-0.440981},{1.56925,-5.96046e-08,-0.440981},
{-1.54343,0.059605,-0.440981},{-1.32294,0.22049,-0.458679},{-1.39787,0.22049,-0.440981},
{-1.10245,0.178974,-0.440981},{-1.20262,0.22049,-0.440981},{1.10245,0.178974,-0.440981},
{1.32294,0.22049,-0.458679},{1.20262,0.22049,-0.440981},{1.54343,0.059605,-0.440981},
{1.39787,0.22049,-0.440981},{-1.32294,0.245337,-0.440981},{1.32294,0.245337,-0.440981},
{-1.32294,-0.440981,-0.229056},{-1.32294,-0.446055,-0.22049},{-1.35,-0.440981,-0.22049},
{-1.29453,-0.440981,-0.22049},{1.32294,-0.440981,-0.229056},{1.32294,-0.446055,-0.22049},
{1.29453,-0.440981,-0.22049},{1.35,-0.440981,-0.22049},{-1.54343,-0.22049,-0.392623},
{-1.54343,-0.386399,-0.22049},{-1.72728,-0.22049,-0.22049},{-1.10245,-0.22049,-0.429188},
{-1.10245,-0.41288,-0.22049},{-0.881962,-0.22049,-0.322399},{-0.881962,-0.309765,-0.22049},
{-0.776092,-0.22049,-0.22049},{0.881962,-0.22049,-0.322399},{0.881962,-0.309765,-0.22049},
{0.776092,-0.22049,-0.22049},{1.10245,-0.22049,-0.429188},{1.10245,-0.41288,-0.22049},
{1.54343,-0.22049,-0.392623},{1.54343,-0.386399,-0.22049},{1.72728,-0.22049,-0.22049},
{-1.76392,-5.96046e-08,-0.253725},{-1.76392,-0.0907058,-0.22049},{-1.78825,-5.96046e-08,
-0.22049},{-0.881962,-5.96046e-08,-0.395135},{-0.661471,-5.96046e-08,-0.225459},{-0.661471,
-0.010086,-0.22049},{-0.657038,-5.96046e-08,-0.22049},{0.661471,-5.96046e-08,-0.225459},
{0.661471,-0.010086,-0.22049},{0.657038,-5.96046e-08,-0.22049},{0.881962,-5.96046e-08,
-0.395135},{1.76392,-5.96046e-08,-0.253725},{1.76392,-0.0907058,-0.22049},{1.78825,
-5.96046e-08,-0.22049},{-1.76392,0.0907057,-0.22049},{-1.72728,0.22049,-0.22049},{-1.54343,
0.22049,-0.392623},{-1.10245,0.22049,-0.429188},{-0.881962,0.22049,-0.322399},{-0.661471,
0.0100859,-0.22049},{-0.776092,0.22049,-0.22049},{0.661471,0.0100859,-0.22049},{0.881962,
0.22049,-0.322399},{0.776092,0.22049,-0.22049},{1.10245,0.22049,-0.429188},{1.54343,
0.22049,-0.392623},{1.72728,0.22049,-0.22049},{1.76392,0.0907057,-0.22049},{-1.54343,
0.386399,-0.22049},{-1.35,0.440981,-0.22049},{-1.32294,0.440981,-0.229056},{-1.29453,
0.440981,-0.22049},{-1.10245,0.41288,-0.22049},{-0.881962,0.309765,-0.22049},{0.881962,
0.309765,-0.22049},{1.10245,0.41288,-0.22049},{1.29453,0.440981,-0.22049},{1.32294,
0.440981,-0.229056},{1.35,0.440981,-0.22049},{1.54343,0.386399,-0.22049},{-1.32294,
0.446055,-0.22049},{1.32294,0.446055,-0.22049},{-1.54343,-0.440981,-0.0279491},{-1.54343,
-0.44723,-5.96046e-08},{-1.55569,-0.440981,-5.96046e-08},{-1.32294,-0.495004,-5.96046e-08},
{-1.10245,-0.440981,-0.105145},{-1.10245,-0.464577,-5.96046e-08},{-1.05548,-0.440981,
-5.96046e-08},{1.10245,-0.440981,-0.105145},{1.10245,-0.464577,-5.96046e-08},{1.05548,
-0.440981,-5.96046e-08},{1.32294,-0.495004,-5.96046e-08},{1.54343,-0.440981,-0.0279491},
{1.54343,-0.44723,-5.96046e-08},{1.55569,-0.440981,-5.96046e-08},{-1.76392,-0.22049,
-0.0899033},{-1.76392,-0.252398,-5.96046e-08},{-1.78791,-0.22049,-5.96046e-08},{-0.881962,
-0.372369,-5.96046e-08},{-0.665855,-0.22049,-5.96046e-08},{0.881962,-0.372369,-5.96046e-08},
{0.665854,-0.22049,-5.96046e-08},{1.76392,-0.22049,-0.0899033},{1.76392,-0.252398,-5.96046e-08},
{1.78791,-0.22049,-5.96046e-08},{-1.84341,-5.96046e-08,-5.96046e-08},{-0.661471,-0.214841,
-5.96046e-08},{-0.539449,-5.96046e-08,-5.96046e-08},{0.661471,-0.214841,-5.96046e-08},
{0.539449,-5.96046e-08,-5.96046e-08},{1.84341,-5.96046e-08,-5.96046e-08},{-1.78791,
0.22049,-5.96046e-08},{-1.76392,0.22049,-0.0899033},{-0.665855,0.22049,-5.96046e-08},{-0.661471,
0.214841,-5.96046e-08},{0.661471,0.214841,-5.96046e-08},{0.665854,0.22049,-5.96046e-08},
{1.76392,0.22049,-0.0899033},{1.78791,0.22049,-5.96046e-08},{-1.76392,0.252398,-5.96046e-08},
{-1.55569,0.440981,-5.96046e-08},{-1.54343,0.440981,-0.0279491},{-1.10245,0.440981,
-0.105145},{-1.05548,0.440981,-5.96046e-08},{-0.881962,0.372369,-5.96046e-08},{0.881962,
0.372369,-5.96046e-08},{1.05548,0.440981,-5.96046e-08},{1.10245,0.440981,-0.105145},
{1.54343,0.440981,-0.0279491},{1.55569,0.440981,-5.96046e-08},{1.76392,0.252398,-5.96046e-08},
{-1.54343,0.44723,-5.96046e-08},{-1.32294,0.495004,-5.96046e-08},{-1.10245,0.464576,
-5.96046e-08},{1.10245,0.464576,-5.96046e-08},{1.32294,0.495004,-5.96046e-08},{1.54343,
0.44723,-5.96046e-08},{-1.54343,-0.440981,0.027949},{-1.32294,-0.446055,0.22049},
{-1.35,-0.440981,0.22049},{-1.10245,-0.440981,0.105144},{-1.29453,-0.440981,0.22049},
{1.10245,-0.440981,0.105144},{1.32294,-0.446055,0.22049},{1.29453,-0.440981,0.22049},
{1.54343,-0.440981,0.027949},{1.35,-0.440981,0.22049},{-1.76392,-0.22049,0.0899032},
{-1.54343,-0.386399,0.22049},{-1.72728,-0.22049,0.22049},{-1.10245,-0.41288,0.22049},
{-0.881962,-0.309765,0.22049},{-0.776092,-0.22049,0.22049},{0.881962,-0.309765,0.22049},
{0.776092,-0.22049,0.22049},{1.10245,-0.41288,0.22049},{1.54343,-0.386399,0.22049},
{1.76392,-0.22049,0.0899032},{1.72728,-0.22049,0.22049},{-1.76392,-0.0907058,0.22049},
{-1.78825,-5.96046e-08,0.22049},{-0.661471,-0.010086,0.22049},{-0.657038,-5.96046e-08,
0.22049},{0.661471,-0.010086,0.22049},{0.657038,-5.96046e-08,0.22049},{1.76392,-0.0907058,
0.22049},{1.78825,-5.96046e-08,0.22049},{-1.76392,0.0907057,0.22049},{-1.76392,0.22049,
0.0899032},{-1.72728,0.22049,0.22049},{-0.776092,0.22049,0.22049},{-0.661471,0.0100859,
0.22049},{0.661471,0.0100859,0.22049},{0.776092,0.22049,0.22049},{1.76392,0.0907057,
0.22049},{1.76392,0.22049,0.0899032},{1.72728,0.22049,0.22049},{-1.54343,0.386399,0.22049},
{-1.54343,0.440981,0.027949},{-1.35,0.440981,0.22049},{-1.10245,0.41288,0.22049},{-1.10245,
0.440981,0.105144},{-1.29453,0.440981,0.22049},{-0.881962,0.309765,0.22049},{0.881962,
0.309765,0.22049},{1.10245,0.41288,0.22049},{1.10245,0.440981,0.105144},{1.29453,0.440981,
0.22049},{1.54343,0.386399,0.22049},{1.54343,0.440981,0.027949},{1.35,0.440981,0.22049},
{-1.32294,0.446055,0.22049},{1.32294,0.446055,0.22049},{-1.32294,-0.440981,0.229056},
{1.32294,-0.440981,0.229056},{-1.54343,-0.22049,0.392622},{-1.32294,-0.245337,0.440981},
{-1.39787,-0.22049,0.440981},{-1.10245,-0.22049,0.429187},{-1.20262,-0.22049,0.440981},
{-0.881962,-0.22049,0.322399},{0.881962,-0.22049,0.322399},{1.10245,-0.22049,0.429187},
{1.32294,-0.245337,0.440981},{1.20262,-0.22049,0.440981},{1.54343,-0.22049,0.392622},
{1.39787,-0.22049,0.440981},{-1.76392,-5.96046e-08,0.253725},{-1.54343,-0.0596052,0.440981},
{-1.56925,-5.96046e-08,0.440981},{-1.10245,-0.178974,0.440981},{-0.881962,-5.96046e-08,
0.395135},{-1.00217,-5.96046e-08,0.440981},{-0.661471,-5.96046e-08,0.225458},{0.661471,
-5.96046e-08,0.225458},{0.881962,-5.96046e-08,0.395135},{1.10245,-0.178974,0.440981},
{1.00217,-5.96046e-08,0.440981},{1.54343,-0.0596052,0.440981},{1.76392,-5.96046e-08,0.253725},
{1.56925,-5.96046e-08,0.440981},{-1.54343,0.059605,0.440981},{-1.54343,0.22049,0.392622},
{-1.39787,0.22049,0.440981},{-1.10245,0.178974,0.440981},{-1.10245,0.22049,0.429187},
{-1.20262,0.22049,0.440981},{-0.881962,0.22049,0.322399},{0.881962,0.22049,0.322399},
{1.10245,0.178974,0.440981},{1.10245,0.22049,0.429187},{1.20262,0.22049,0.440981},{1.54343,
0.059605,0.440981},{1.54343,0.22049,0.392622},{1.39787,0.22049,0.440981},{-1.32294,
0.245337,0.440981},{-1.32294,0.440981,0.229056},{1.32294,0.245337,0.440981},{1.32294,
0.440981,0.229056},{-1.32294,-0.22049,0.458678},{1.32294,-0.22049,0.458678},{-1.54343,
-5.96046e-08,0.455658},{-1.32294,-5.96046e-08,0.513403},{-1.10245,-5.96046e-08,0.490312},
{1.10245,-5.96046e-08,0.490312},{1.32294,-5.96046e-08,0.513403},{1.54343,-5.96046e-08,
0.455658},{-1.32294,0.22049,0.458678},{1.32294,0.22049,0.458678}
};
	int connect<NEportLevels={2,0}>[1584] = {
   0,1,2,3,1,0,4,5,6,7,5,4,8,9,10,11,0,2,11,2,9,11,9,8,12,13,3,12,3,0,
12,0,11,14,13,12,15,16,17,18,4,6,18,6,16,18,16,15,19,20,7,19,7,4,19,4,
18,21,20,19,22,8,10,23,11,8,23,8,22,23,22,24,25,12,11,25,11,23,25,23,26,
25,14,12,27,15,17,28,18,15,28,15,27,28,27,29,30,19,18,30,18,28,30,28,31,
30,21,19,32,23,24,32,26,23,33,28,29,33,31,28,34,35,36,37,35,34,38,39,40,
41,39,38,42,43,44,2,1,34,2,34,36,2,36,42,36,43,42,1,3,45,1,45,46,1,46,34,
46,37,34,47,48,46,47,46,45,49,48,47,50,51,52,53,54,51,53,51,50,6,5,38,6,
38,40,6,40,53,40,54,53,5,7,55,5,55,56,5,56,38,56,41,38,57,56,55,58,59,60,
10,9,42,10,42,44,10,44,58,44,59,58,9,2,42,3,13,45,13,14,61,13,61,47,13,
47,45,62,63,49,62,49,47,62,47,61,64,63,62,65,66,67,68,50,52,68,52,66,68,
66,65,17,16,53,17,53,50,17,50,68,16,6,53,7,20,55,20,21,69,20,69,70,20,70,
55,70,57,55,71,70,69,72,58,60,22,10,58,22,58,72,22,72,74,72,73,74,24,22,
74,25,26,75,14,25,75,14,75,76,14,76,61,77,62,61,77,61,76,77,76,78,77,64,
62,79,65,67,80,68,65,80,65,79,80,79,81,27,17,68,27,68,80,27,80,82,29,27,
82,30,31,83,21,30,83,21,83,84,21,84,69,84,85,69,85,71,69,86,74,73,32,24,
74,32,74,86,32,86,88,86,87,88,26,32,88,26,88,89,26,89,75,89,90,75,91,76,
75,91,75,90,91,78,76,92,80,81,93,82,80,93,80,92,33,29,82,33,82,93,33,93,
95,93,94,95,31,33,95,31,95,96,31,96,83,96,97,83,97,84,83,98,88,87,98,89,
88,99,95,94,99,96,95,100,101,102,36,35,103,36,103,101,36,101,100,35,37,
104,35,104,105,35,105,103,106,105,104,107,108,109,40,39,110,40,110,108,40,
108,107,39,41,111,39,111,112,39,112,110,113,112,111,114,115,116,44,43,100,
44,100,102,44,102,114,102,115,114,43,36,100,37,46,104,46,48,117,46,117,106,
46,106,104,48,49,118,48,118,117,52,51,119,52,119,120,51,54,107,51,107,109,
51,109,119,54,40,107,41,56,111,56,57,121,56,121,122,56,122,111,122,113,111,
123,122,121,60,59,114,60,114,116,60,116,124,59,44,114,49,63,125,49,125,118,
63,64,126,63,126,125,67,66,127,67,127,128,66,52,120,66,120,127,57,70,121,
70,71,129,70,129,123,70,123,121,72,60,124,72,124,130,72,130,131,73,72,131,
77,78,132,77,132,133,64,77,133,64,133,126,79,67,128,79,128,134,81,79,134,
81,134,135,85,84,136,71,85,136,71,136,137,71,137,129,138,131,130,86,73,
131,86,131,138,86,138,140,138,139,140,87,86,140,90,89,141,91,90,141,91,
141,142,91,142,143,78,91,143,78,143,132,92,81,135,92,135,144,93,92,144,93,
144,145,93,145,146,94,93,146,97,96,147,84,97,147,84,147,148,84,148,136,148,
149,136,149,137,136,150,140,139,98,87,140,98,140,150,98,150,151,89,98,151,
89,151,152,89,152,141,152,142,141,153,146,145,99,94,146,99,146,153,99,153,
154,96,99,154,96,154,155,96,155,147,155,148,147,102,101,156,101,103,157,
101,157,158,101,158,156,103,105,159,103,159,160,103,160,157,105,106,159,
109,108,161,108,110,162,108,162,163,108,163,161,110,112,164,110,164,165,
110,165,162,112,113,164,116,115,166,115,102,156,115,156,167,115,167,166,
167,168,166,158,167,156,159,169,160,106,117,170,106,170,169,106,169,159,
117,118,171,117,171,170,120,119,172,120,172,173,119,109,161,119,161,174,
119,174,172,163,174,161,164,175,165,113,122,176,113,176,177,113,177,164,
177,175,164,122,123,176,124,116,166,124,166,178,124,178,179,168,178,166,
118,125,180,118,180,171,125,126,181,125,181,180,128,127,182,128,182,183,
127,120,173,127,173,182,176,184,177,123,129,185,123,185,184,123,184,176,
130,124,179,130,179,186,130,186,187,186,188,187,133,132,189,133,189,190,
126,133,190,126,190,181,134,128,183,134,183,191,135,134,191,135,191,192,
193,194,195,129,137,194,129,194,193,129,193,185,138,130,187,139,138,187,
139,187,188,139,188,197,188,196,197,196,198,197,199,200,201,143,142,200,
143,200,199,143,199,202,132,143,202,132,202,189,144,135,192,144,192,203,
145,144,203,145,203,204,145,204,205,204,206,205,207,208,209,149,148,208,
149,208,207,149,207,194,207,195,194,137,149,194,150,139,197,151,150,197,
151,197,198,151,198,210,152,151,210,152,210,201,152,201,200,142,152,200,
153,145,205,154,153,205,154,205,206,154,206,211,155,154,211,155,211,209,
155,209,208,148,155,208,158,157,212,157,160,212,163,162,213,162,165,213,
168,167,214,167,158,212,167,212,215,167,215,214,215,216,214,160,169,217,
160,217,218,160,218,212,218,215,212,169,170,219,169,219,217,170,171,219,
173,172,220,172,174,221,172,221,220,174,163,213,174,213,222,174,222,221,
222,223,221,165,175,224,165,224,225,165,225,213,225,222,213,175,177,224,
179,178,226,178,168,214,178,214,227,178,227,226,227,228,226,216,227,214,
217,229,218,219,230,231,219,231,229,219,229,217,171,180,232,171,232,230,
171,230,219,180,181,232,183,182,233,182,173,220,182,220,234,182,234,233,
221,235,236,221,236,234,221,234,220,223,235,221,224,237,225,177,184,238,
177,238,239,177,239,224,239,237,224,184,185,238,186,179,226,188,186,226,
188,226,228,188,228,241,228,240,241,240,242,241,243,244,245,230,246,244,
230,244,243,230,243,231,190,189,246,190,246,230,190,230,232,181,190,232,
191,183,233,192,191,233,192,233,234,192,234,247,248,249,247,248,247,234,
248,234,236,248,250,249,251,252,253,193,195,252,193,252,251,193,251,238,
251,239,238,185,193,238,196,188,241,198,196,241,198,241,242,198,242,255,
242,254,255,199,201,255,199,255,254,199,254,244,254,245,244,202,199,244,
202,244,246,189,202,246,203,192,247,204,203,247,204,247,249,206,204,249,
206,249,250,206,250,257,250,256,257,207,209,257,207,257,256,207,256,252,
256,253,252,195,207,252,210,198,255,201,210,255,211,206,257,209,211,257,
216,215,258,215,218,258,223,222,259,222,225,259,228,227,260,227,216,258,
227,258,261,227,261,260,218,229,262,218,262,261,218,261,258,229,231,262,
236,235,263,235,223,259,235,259,264,235,264,263,225,237,265,225,265,264,
225,264,259,237,239,265,240,228,260,242,240,260,242,260,261,242,261,266,
243,245,266,243,266,261,243,261,262,231,243,262,248,236,263,250,248,263,
250,263,264,250,264,267,251,253,267,251,267,264,251,264,265,239,251,265,
254,242,266,245,254,266,256,250,267,253,256,267
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	  xform {
	    mat = {
	      {1.0, 0.0, 0.0, 0.0},
	      {0.0, 1.0, 0.0, 0.0},
	      {0.0, 0.0, 1.0, 0.0},
	      {0.0, 0.0, 0.0, 1.2}
	    };
	  };
	};
      };
      group dn_mesh {
	float coord<NEportLevels={2,0}>[268][3] = {
   {-5.96046e-08,-1.54343,-0.455658},{-5.96046e-08,-1.56925,-0.440981},{-0.0596052,-1.54343,
-0.440981},{0.059605,-1.54343,-0.440981},{-0.22049,-1.32294,-0.458679},{-0.22049,-1.39787,
-0.440981},{-0.245337,-1.32294,-0.440981},{-5.96046e-08,-1.32294,-0.513403},{0.22049,
-1.32294,-0.458679},{0.22049,-1.39787,-0.440981},{0.245337,-1.32294,-0.440981},{-0.22049,
-1.20262,-0.440981},{-5.96046e-08,-1.10245,-0.490312},{-0.178974,-1.10245,-0.440981},
{0.22049,-1.20262,-0.440981},{0.178974,-1.10245,-0.440981},{-5.96046e-08,-1.00217,
-0.440981},{-5.96046e-08,1.10245,-0.490312},{-5.96046e-08,1.00217,-0.440981},{-0.178974,
1.10245,-0.440981},{0.178974,1.10245,-0.440981},{-0.22049,1.32294,-0.458679},{-0.22049,
1.20262,-0.440981},{-0.245337,1.32294,-0.440981},{-5.96046e-08,1.32294,-0.513403},{0.22049,
1.32294,-0.458679},{0.22049,1.20262,-0.440981},{0.245337,1.32294,-0.440981},{-0.22049,
1.39787,-0.440981},{-5.96046e-08,1.54343,-0.455658},{-0.0596052,1.54343,-0.440981},
{0.22049,1.39787,-0.440981},{0.059605,1.54343,-0.440981},{-5.96046e-08,1.56925,-0.440981},
{-5.96046e-08,-1.76392,-0.253725},{-5.96046e-08,-1.78825,-0.22049},{-0.0907058,-1.76392,
-0.22049},{0.0907057,-1.76392,-0.22049},{-0.22049,-1.54343,-0.392623},{-0.22049,-1.72728,
-0.22049},{-0.386399,-1.54343,-0.22049},{0.22049,-1.54343,-0.392623},{0.22049,-1.72728,
-0.22049},{0.386399,-1.54343,-0.22049},{-0.440981,-1.32294,-0.229056},{-0.440981,-1.35,
-0.22049},{-0.446055,-1.32294,-0.22049},{0.440981,-1.32294,-0.229056},{0.440981,-1.35,
-0.22049},{0.446055,-1.32294,-0.22049},{-0.440981,-1.29453,-0.22049},{-0.41288,-1.10245,
-0.22049},{-0.22049,-1.10245,-0.429188},{0.22049,-1.10245,-0.429188},{0.41288,-1.10245,
-0.22049},{0.440981,-1.29453,-0.22049},{-0.22049,-0.881962,-0.322399},{-0.309765,-0.881962,
-0.22049},{-5.96046e-08,-0.881962,-0.395135},{0.22049,-0.881962,-0.322399},{0.309765,
-0.881962,-0.22049},{-0.22049,-0.776092,-0.22049},{-5.96046e-08,-0.661471,-0.225459},
{-0.010086,-0.661471,-0.22049},{0.22049,-0.776092,-0.22049},{0.0100859,-0.661471,-0.22049},
{-5.96046e-08,-0.657038,-0.22049},{-5.96046e-08,0.661471,-0.225459},{-5.96046e-08,0.657038,
-0.22049},{-0.010086,0.661471,-0.22049},{0.0100859,0.661471,-0.22049},{-0.22049,0.881962,
-0.322399},{-0.22049,0.776092,-0.22049},{-0.309765,0.881962,-0.22049},{-5.96046e-08,0.881962,
-0.395135},{0.22049,0.881962,-0.322399},{0.22049,0.776092,-0.22049},{0.309765,0.881962,
-0.22049},{-0.22049,1.10245,-0.429188},{-0.41288,1.10245,-0.22049},{0.22049,1.10245,
-0.429188},{0.41288,1.10245,-0.22049},{-0.440981,1.32294,-0.229056},{-0.440981,1.29453,
-0.22049},{-0.446055,1.32294,-0.22049},{0.440981,1.32294,-0.229056},{0.440981,1.29453,
-0.22049},{0.446055,1.32294,-0.22049},{-0.440981,1.35,-0.22049},{-0.386399,1.54343,
-0.22049},{-0.22049,1.54343,-0.392623},{0.22049,1.54343,-0.392623},{0.386399,1.54343,
-0.22049},{0.440981,1.35,-0.22049},{-0.22049,1.72728,-0.22049},{-0.0907058,1.76392,
-0.22049},{-5.96046e-08,1.76392,-0.253725},{0.0907057,1.76392,-0.22049},{0.22049,1.72728,
-0.22049},{-5.96046e-08,1.78825,-0.22049},{-0.22049,-1.76392,-0.0899033},{-0.22049,
-1.78791,-5.96046e-08},{-0.252398,-1.76392,-5.96046e-08},{-5.96046e-08,-1.84341,-5.96046e-08},
{0.22049,-1.76392,-0.0899033},{0.22049,-1.78791,-5.96046e-08},{0.252398,-1.76392,-5.96046e-08},
{-0.440981,-1.54343,-0.0279491},{-0.440981,-1.55569,-5.96046e-08},{-0.44723,-1.54343,
-5.96046e-08},{0.440981,-1.54343,-0.0279491},{0.440981,-1.55569,-5.96046e-08},{0.44723,
-1.54343,-5.96046e-08},{-0.495004,-1.32294,-5.96046e-08},{0.495004,-1.32294,-5.96046e-08},
{-0.464577,-1.10245,-5.96046e-08},{-0.440981,-1.10245,-0.105145},{0.440981,-1.10245,
-0.105145},{0.464576,-1.10245,-5.96046e-08},{-0.440981,-1.05548,-5.96046e-08},{-0.372369,
-0.881962,-5.96046e-08},{0.372369,-0.881962,-5.96046e-08},{0.440981,-1.05548,-5.96046e-08},
{-0.22049,-0.665855,-5.96046e-08},{-0.214841,-0.661471,-5.96046e-08},{0.214841,-0.661471,
-5.96046e-08},{0.22049,-0.665855,-5.96046e-08},{-5.96046e-08,-0.539449,-5.96046e-08},{-5.96046e-08,
0.539449,-5.96046e-08},{-0.214841,0.661471,-5.96046e-08},{0.214841,0.661471,-5.96046e-08},
{-0.22049,0.665854,-5.96046e-08},{-0.372369,0.881962,-5.96046e-08},{0.22049,0.665854,
-5.96046e-08},{0.372369,0.881962,-5.96046e-08},{-0.440981,1.10245,-0.105145},{-0.440981,
1.05548,-5.96046e-08},{-0.464577,1.10245,-5.96046e-08},{0.440981,1.10245,-0.105145},
{0.440981,1.05548,-5.96046e-08},{0.464576,1.10245,-5.96046e-08},{-0.495004,1.32294,
-5.96046e-08},{0.495004,1.32294,-5.96046e-08},{-0.44723,1.54343,-5.96046e-08},{-0.440981,
1.54343,-0.0279491},{0.440981,1.54343,-0.0279491},{0.44723,1.54343,-5.96046e-08},{-0.440981,
1.55569,-5.96046e-08},{-0.252398,1.76392,-5.96046e-08},{-0.22049,1.76392,-0.0899033},
{0.22049,1.76392,-0.0899033},{0.252398,1.76392,-5.96046e-08},{0.440981,1.55569,-5.96046e-08},
{-0.22049,1.78791,-5.96046e-08},{-5.96046e-08,1.84341,-5.96046e-08},{0.22049,1.78791,
-5.96046e-08},{-0.22049,-1.76392,0.0899032},{-5.96046e-08,-1.78825,0.22049},{-0.0907058,
-1.76392,0.22049},{0.22049,-1.76392,0.0899032},{0.0907057,-1.76392,0.22049},{-0.440981,
-1.54343,0.027949},{-0.22049,-1.72728,0.22049},{-0.386399,-1.54343,0.22049},{0.22049,
-1.72728,0.22049},{0.440981,-1.54343,0.027949},{0.386399,-1.54343,0.22049},{-0.440981,
-1.35,0.22049},{-0.446055,-1.32294,0.22049},{0.440981,-1.35,0.22049},{0.446055,-1.32294,
0.22049},{-0.440981,-1.29453,0.22049},{-0.440981,-1.10245,0.105144},{-0.41288,-1.10245,
0.22049},{0.440981,-1.29453,0.22049},{0.440981,-1.10245,0.105144},{0.41288,-1.10245,
0.22049},{-0.309765,-0.881962,0.22049},{0.309765,-0.881962,0.22049},{-0.22049,-0.776092,
0.22049},{-0.010086,-0.661471,0.22049},{0.0100859,-0.661471,0.22049},{0.22049,-0.776092,
0.22049},{-5.96046e-08,-0.657038,0.22049},{-5.96046e-08,0.657038,0.22049},{-0.010086,
0.661471,0.22049},{0.0100859,0.661471,0.22049},{-0.22049,0.776092,0.22049},{-0.309765,
0.881962,0.22049},{0.22049,0.776092,0.22049},{0.309765,0.881962,0.22049},{-0.440981,
1.10245,0.105144},{-0.41288,1.10245,0.22049},{0.440981,1.10245,0.105144},{0.41288,
1.10245,0.22049},{-0.440981,1.29453,0.22049},{-0.446055,1.32294,0.22049},{0.440981,
1.29453,0.22049},{0.446055,1.32294,0.22049},{-0.440981,1.35,0.22049},{-0.440981,1.54343,
0.027949},{-0.386399,1.54343,0.22049},{0.440981,1.35,0.22049},{0.440981,1.54343,0.027949},
{0.386399,1.54343,0.22049},{-0.22049,1.72728,0.22049},{-0.22049,1.76392,0.0899032},{-0.0907058,
1.76392,0.22049},{0.22049,1.72728,0.22049},{0.22049,1.76392,0.0899032},{0.0907057,1.76392,
0.22049},{-5.96046e-08,1.78825,0.22049},{-5.96046e-08,-1.76392,0.253725},{-0.22049,
-1.54343,0.392622},{-5.96046e-08,-1.56925,0.440981},{-0.0596052,-1.54343,0.440981},
{0.22049,-1.54343,0.392622},{0.059605,-1.54343,0.440981},{-0.440981,-1.32294,0.229056},
{-0.22049,-1.39787,0.440981},{-0.245337,-1.32294,0.440981},{0.22049,-1.39787,0.440981},
{0.440981,-1.32294,0.229056},{0.245337,-1.32294,0.440981},{-0.22049,-1.20262,0.440981},
{-0.22049,-1.10245,0.429187},{-0.178974,-1.10245,0.440981},{0.22049,-1.20262,0.440981},
{0.22049,-1.10245,0.429187},{0.178974,-1.10245,0.440981},{-0.22049,-0.881962,0.322399},
{-5.96046e-08,-1.00217,0.440981},{-5.96046e-08,-0.881962,0.395135},{0.22049,-0.881962,
0.322399},{-5.96046e-08,-0.661471,0.225458},{-5.96046e-08,0.661471,0.225458},{-0.22049,
0.881962,0.322399},{-5.96046e-08,0.881962,0.395135},{0.22049,0.881962,0.322399},{-0.22049,
1.10245,0.429187},{-5.96046e-08,1.00217,0.440981},{-0.178974,1.10245,0.440981},{0.22049,
1.10245,0.429187},{0.178974,1.10245,0.440981},{-0.440981,1.32294,0.229056},{-0.22049,
1.20262,0.440981},{-0.245337,1.32294,0.440981},{0.22049,1.20262,0.440981},{0.440981,
1.32294,0.229056},{0.245337,1.32294,0.440981},{-0.22049,1.39787,0.440981},{-0.22049,
1.54343,0.392622},{-0.0596052,1.54343,0.440981},{0.22049,1.39787,0.440981},{0.22049,
1.54343,0.392622},{0.059605,1.54343,0.440981},{-5.96046e-08,1.56925,0.440981},{-5.96046e-08,
1.76392,0.253725},{-5.96046e-08,-1.54343,0.455658},{-0.22049,-1.32294,0.458678},{-5.96046e-08,
-1.32294,0.513403},{0.22049,-1.32294,0.458678},{-5.96046e-08,-1.10245,0.490312},{-5.96046e-08,
1.10245,0.490312},{-0.22049,1.32294,0.458678},{-5.96046e-08,1.32294,0.513403},{0.22049,
1.32294,0.458678},{-5.96046e-08,1.54343,0.455658}
};
	int connect<NEportLevels={2,0}>[1584] = {
   0,1,2,3,1,0,4,5,6,7,0,2,7,2,5,7,5,4,8,9,3,8,3,0,8,0,7,10,9,8,11,4,6,
12,7,4,12,4,11,12,11,13,14,8,7,14,7,12,14,12,15,14,10,8,16,12,13,16,15,
12,17,18,19,20,18,17,21,22,23,24,17,19,24,19,22,24,22,21,25,26,20,25,20,
17,25,17,24,27,26,25,28,21,23,29,24,21,29,21,28,29,28,30,31,25,24,31,24,
29,31,29,32,31,27,25,33,29,30,33,32,29,34,35,36,37,35,34,38,39,40,2,1,
34,2,34,36,2,36,38,36,39,38,1,3,41,1,41,42,1,42,34,42,37,34,43,42,41,44,
45,46,6,5,38,6,38,40,6,40,44,40,45,44,5,2,38,3,9,41,9,10,47,9,47,48,9,
48,41,48,43,41,49,48,47,50,44,46,11,6,44,11,44,50,11,50,52,50,51,52,13,
11,52,14,15,53,10,14,53,10,53,54,10,54,47,54,55,47,55,49,47,56,52,51,56,
51,57,16,13,52,16,52,56,16,56,58,15,16,58,15,58,59,15,59,53,60,54,53,60,
53,59,61,56,57,62,58,56,62,56,61,62,61,63,64,59,58,64,58,62,64,62,65,64,
60,59,66,62,63,66,65,62,67,68,69,70,68,67,71,72,73,74,67,69,74,69,72,74,
72,71,75,76,70,75,70,67,75,67,74,77,76,75,78,71,73,78,73,79,19,18,74,19,
74,71,19,71,78,18,20,80,18,80,75,18,75,74,81,77,75,81,75,80,82,83,84,23,
22,78,23,78,79,23,79,82,79,83,82,22,19,78,20,26,80,26,27,85,26,85,86,26,
86,80,86,81,80,87,86,85,88,82,84,28,23,82,28,82,88,28,88,90,88,89,90,30,
28,90,31,32,91,27,31,91,27,91,92,27,92,85,92,93,85,93,87,85,94,90,89,33,
30,90,33,90,94,33,94,96,94,95,96,32,33,96,32,96,97,32,97,91,97,98,91,98,
92,91,99,96,95,99,97,96,100,101,102,36,35,103,36,103,101,36,101,100,35,
37,104,35,104,105,35,105,103,106,105,104,107,108,109,40,39,100,40,100,102,
40,102,107,102,108,107,39,36,100,37,42,104,42,43,110,42,110,111,42,111,104,
111,106,104,112,111,110,46,45,107,46,107,109,46,109,113,45,40,107,43,48,
110,48,49,114,48,114,112,48,112,110,50,46,113,50,113,115,50,115,116,51,
50,116,55,54,117,49,55,117,49,117,118,49,118,114,119,116,115,57,51,116,57,
116,119,57,119,120,54,60,121,54,121,122,54,122,117,122,118,117,61,57,120,
61,120,123,63,61,123,63,123,124,64,65,125,64,125,126,60,64,126,60,126,121,
66,63,124,66,124,127,65,66,127,65,127,125,69,68,128,69,128,129,68,70,130,
68,130,128,73,72,131,73,131,132,72,69,129,72,129,131,70,76,133,70,133,130,
76,77,134,76,134,133,135,136,137,79,73,132,79,132,136,79,136,135,77,81,
138,77,138,139,77,139,134,140,139,138,84,83,135,84,135,137,84,137,141,83,
79,135,81,86,138,86,87,142,86,142,140,86,140,138,88,84,141,88,141,143,88,
143,144,89,88,144,93,92,145,87,93,145,87,145,146,87,146,142,147,144,143,
94,89,144,94,144,147,94,147,149,147,148,149,95,94,149,98,97,150,92,98,150,
92,150,151,92,151,145,151,152,145,152,146,145,153,149,148,99,95,149,99,
149,153,99,153,154,97,99,154,97,154,155,97,155,150,155,151,150,102,101,156,
101,103,157,101,157,158,101,158,156,103,105,159,103,159,160,103,160,157,
105,106,159,109,108,161,108,102,156,108,156,162,108,162,161,162,163,161,
158,162,156,159,164,160,106,111,165,106,165,166,106,166,159,166,164,159,
111,112,165,113,109,161,113,161,167,113,167,168,163,167,161,165,169,166,
112,114,170,112,170,169,112,169,165,115,113,168,115,168,171,115,171,172,
171,173,172,174,175,176,114,118,175,114,175,174,114,174,170,119,115,172,
120,119,172,120,172,173,120,173,177,122,121,178,122,178,176,122,176,175,
118,122,175,123,120,177,123,177,179,124,123,179,124,179,180,126,125,181,
126,181,182,121,126,182,121,182,178,127,124,180,127,180,183,125,127,183,
125,183,181,129,128,184,129,184,185,128,130,186,128,186,184,132,131,187,
132,187,188,131,129,185,131,185,187,130,133,189,130,189,186,133,134,190,
133,190,189,137,136,191,136,132,188,136,188,192,136,192,191,134,139,193,
134,193,194,134,194,190,139,140,193,141,137,191,141,191,195,141,195,196,
192,195,191,193,197,194,140,142,198,140,198,197,140,197,193,143,141,196,
143,196,199,143,199,200,199,201,200,202,203,204,142,146,203,142,203,202,
142,202,198,147,143,200,148,147,200,148,200,201,148,201,206,201,205,206,
205,207,206,208,209,210,152,151,209,152,209,208,152,208,203,208,204,203,
146,152,203,153,148,206,154,153,206,154,206,207,154,207,211,155,154,211,
155,211,210,155,210,209,151,155,209,158,157,212,157,160,212,163,162,213,
162,158,212,162,212,214,162,214,213,214,215,213,160,164,216,160,216,217,
160,217,212,217,214,212,164,166,216,168,167,218,167,163,213,167,213,219,
167,219,218,219,220,218,215,219,213,216,221,217,166,169,222,166,222,223,
166,223,216,223,221,216,169,170,222,171,168,218,173,171,218,173,218,220,
173,220,225,220,224,225,224,226,225,227,228,229,174,176,228,174,228,227,
174,227,222,227,223,222,170,174,222,177,173,225,177,225,230,231,232,230,
231,230,225,231,225,226,228,233,232,228,232,231,228,231,229,176,178,233,
176,233,228,179,177,230,180,179,230,180,230,232,180,232,234,182,181,234,
182,234,232,182,232,233,178,182,233,183,180,234,181,183,234,185,184,235,
184,186,235,188,187,236,187,185,235,187,235,237,187,237,236,186,189,238,
186,238,237,186,237,235,189,190,238,192,188,236,192,236,239,237,240,241,
237,241,239,237,239,236,238,242,243,238,243,240,238,240,237,190,194,242,
190,242,238,196,195,244,195,192,239,195,239,245,195,245,244,245,246,244,
241,245,239,242,247,243,194,197,248,194,248,249,194,249,242,249,247,242,
197,198,248,199,196,244,201,199,244,201,244,246,201,246,251,246,250,251,
250,252,251,253,254,255,202,204,254,202,254,253,202,253,248,253,249,248,
198,202,248,205,201,251,207,205,251,207,251,252,207,252,257,252,256,257,
208,210,257,208,257,256,208,256,254,256,255,254,204,208,254,211,207,257,
210,211,257,215,214,258,214,217,258,220,219,259,219,215,258,219,258,260,
219,260,259,217,221,261,217,261,260,217,260,258,221,223,261,224,220,259,
226,224,259,226,259,260,226,260,262,227,229,262,227,262,260,227,260,261,
223,227,261,231,226,262,229,231,262,241,240,263,240,243,263,246,245,264,
245,241,263,245,263,265,245,265,264,243,247,266,243,266,265,243,265,263,
247,249,266,250,246,264,252,250,264,252,264,265,252,265,267,253,255,267,
253,267,265,253,265,266,249,253,266,256,252,267,255,256,267
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	  xform {
	    mat = {
	      {1.0, 0.0, 0.0, 0.0},
	      {0.0, 1.0, 0.0, 0.0},
	      {0.0, 0.0, 1.0, 0.0},
	      {0.0, 0.0, 0.0, 1.2}
	    };
	  };
	};
      };
      dlv_glyph pglyph {
	in_field => <-.<-.orbs.orbital[7];
	in_glyph => <-.dp_mesh.out;
	GlyphParam {
	  map_comp = 1;
	  scale => <-.<-.<-.scale;
	};
      };
      dlv_glyph nglyph {
	in_field => <-.<-.orbs.orbital[7];
	in_glyph => dn_mesh.out;
	GlyphParam {
	  map_comp = 2;
	  scale => <-.<-.<-.scale;
	};
      };
    };
    GMOD.instancer dxy_instancer {
      Value => (<-.orbs.orbital[8].nnodes > 0);
      Group => <-.dxy_orb;
      active = 2;
    };
    macro dxy_orb<instanced=0> {
      // grids cut out of AVS with $get_array then attached to a tri_mesh.
      group dp_mesh {
	float coord<NEportLevels={2,0}>[256][3] = {
   {-1.10245,-1.10245,-0.472801},{-1.10245,-1.16561,-0.440981},{-1.16561,-1.10245,
-0.440981},{-0.881962,-1.10245,-0.497781},{-0.881962,-1.20801,-0.440981},{-0.726405,
-1.10245,-0.440981},{-1.10245,-0.881962,-0.497781},{-1.20801,-0.881962,-0.440981},{-0.881962,
-0.881962,-0.531455},{-0.661471,-0.881962,-0.474746},{-0.661471,-1.02127,-0.440981},
{-0.616876,-0.881962,-0.440981},{-1.10245,-0.726405,-0.440981},{-0.881962,-0.661471,
-0.474746},{-1.02127,-0.661471,-0.440981},{-0.661471,-0.706694,-0.440981},{-0.706694,
-0.661471,-0.440981},{-0.881962,-0.616876,-0.440981},{0.881962,0.661471,-0.474746},
{0.881962,0.616876,-0.440981},{0.706694,0.661471,-0.440981},{1.02127,0.661471,-0.440981},
{0.661471,0.881962,-0.474746},{0.661471,0.706694,-0.440981},{0.616876,0.881962,-0.440981},
{0.881962,0.881962,-0.531455},{1.10245,0.881962,-0.497781},{1.10245,0.726405,-0.440981},
{1.20801,0.881962,-0.440981},{0.661471,1.02127,-0.440981},{0.881962,1.10245,-0.497781},
{0.726405,1.10245,-0.440981},{1.10245,1.10245,-0.472801},{1.16561,1.10245,-0.440981},
{0.881962,1.20801,-0.440981},{1.10245,1.16561,-0.440981},{-1.10245,-1.32294,-0.316888},
{-1.10245,-1.39706,-0.22049},{-1.22702,-1.32294,-0.22049},{-0.881962,-1.32294,-0.34278},
{-0.881962,-1.41577,-0.22049},{-0.687475,-1.32294,-0.22049},{-1.32294,-1.10245,-0.316888},
{-1.32294,-1.22702,-0.22049},{-1.39706,-1.10245,-0.22049},{-0.661471,-1.10245,-0.41619},
{-0.661471,-1.31144,-0.22049},{-0.495393,-1.10245,-0.22049},{-1.32294,-0.881962,-0.34278},
{-1.41577,-0.881962,-0.22049},{-0.440981,-0.881962,-0.288812},{-0.440981,-1.00107,
-0.22049},{-0.399213,-0.881962,-0.22049},{-1.32294,-0.687475,-0.22049},{-1.31144,-0.661471,
-0.22049},{-1.10245,-0.661471,-0.41619},{-0.661471,-0.661471,-0.434723},{-0.440981,-0.661471,
-0.303404},{-0.377213,-0.661471,-0.22049},{-1.10245,-0.495393,-0.22049},{-1.00107,-0.440981,
-0.22049},{-0.881962,-0.440981,-0.288812},{-0.661471,-0.440981,-0.303404},{-0.440981,
-0.489642,-0.22049},{-0.489642,-0.440981,-0.22049},{-0.881962,-0.399213,-0.22049},
{-0.661471,-0.377213,-0.22049},{0.661471,0.440981,-0.303404},{0.661471,0.377212,-0.22049},
{0.489642,0.440981,-0.22049},{0.881962,0.440981,-0.288812},{0.881962,0.399213,-0.22049},
{1.00107,0.440981,-0.22049},{0.440981,0.661471,-0.303404},{0.440981,0.489642,-0.22049},
{0.377212,0.661471,-0.22049},{0.661471,0.661471,-0.434723},{1.10245,0.661471,-0.41619},
{1.10245,0.495393,-0.22049},{1.31144,0.661471,-0.22049},{0.440981,0.881962,-0.288812},
{0.399213,0.881962,-0.22049},{1.32294,0.881962,-0.34278},{1.32294,0.687475,-0.22049},
{1.41577,0.881962,-0.22049},{0.440981,1.00107,-0.22049},{0.495393,1.10245,-0.22049},
{0.661471,1.10245,-0.41619},{1.32294,1.10245,-0.316888},{1.39706,1.10245,-0.22049},
{0.661471,1.31144,-0.22049},{0.687475,1.32294,-0.22049},{0.881962,1.32294,-0.34278},
{1.10245,1.32294,-0.316888},{1.22702,1.32294,-0.22049},{1.32294,1.22702,-0.22049},{0.881962,
1.41577,-0.22049},{1.10245,1.39706,-0.22049},{-1.10245,-1.4503,-5.96046e-08},{-1.31281,
-1.32294,-5.96046e-08},{-0.881962,-1.46582,-5.96046e-08},{-0.661471,-1.32294,-0.178273},
{-0.661471,-1.37248,-5.96046e-08},{-0.615388,-1.32294,-5.96046e-08},{-1.32294,-1.31281,
-5.96046e-08},{-1.4503,-1.10245,-5.96046e-08},{-0.440981,-1.10245,-0.0188299},{-0.440981,
-1.10987,-5.96046e-08},{-0.437269,-1.10245,-5.96046e-08},{-1.46582,-0.881962,-5.96046e-08},
{-0.348794,-0.881962,-5.96046e-08},{-1.37248,-0.661471,-5.96046e-08},{-1.32294,-0.661471,
-0.178273},{-0.310812,-0.661471,-5.96046e-08},{-1.32294,-0.615388,-5.96046e-08},{-1.10987,
-0.440981,-5.96046e-08},{-1.10245,-0.440981,-0.0188299},{-0.440981,-0.440981,-0.186287},
{-0.341552,-0.440981,-5.96046e-08},{-1.10245,-0.437269,-5.96046e-08},{-0.881962,-0.348794,
-5.96046e-08},{-0.661471,-0.310812,-5.96046e-08},{-0.440981,-0.341552,-5.96046e-08},{0.440981,
0.440981,-0.186287},{0.440981,0.341552,-5.96046e-08},{0.341552,0.440981,-5.96046e-08},
{0.661471,0.310812,-5.96046e-08},{0.881962,0.348794,-5.96046e-08},{1.10245,0.440981,
-0.0188299},{1.10245,0.437269,-5.96046e-08},{1.10987,0.440981,-5.96046e-08},{0.310812,
0.661471,-5.96046e-08},{1.32294,0.661471,-0.178273},{1.32294,0.615387,-5.96046e-08},{1.37248,
0.661471,-5.96046e-08},{0.348794,0.881962,-5.96046e-08},{1.46582,0.881962,-5.96046e-08},
{0.437269,1.10245,-5.96046e-08},{0.440981,1.10245,-0.0188299},{1.4503,1.10245,-5.96046e-08},
{0.440981,1.10987,-5.96046e-08},{0.615387,1.32294,-5.96046e-08},{0.661471,1.32294,
-0.178273},{1.31281,1.32294,-5.96046e-08},{1.32294,1.31281,-5.96046e-08},{0.661471,
1.37248,-5.96046e-08},{0.881962,1.46582,-5.96046e-08},{1.10245,1.4503,-5.96046e-08},{-1.10245,
-1.39706,0.22049},{-1.22702,-1.32294,0.22049},{-0.881962,-1.41577,0.22049},{-0.661471,
-1.32294,0.178273},{-0.687475,-1.32294,0.22049},{-1.32294,-1.22702,0.22049},{-1.39706,
-1.10245,0.22049},{-0.661471,-1.31144,0.22049},{-0.440981,-1.10245,0.0188297},{-0.495393,
-1.10245,0.22049},{-1.41577,-0.881962,0.22049},{-0.440981,-1.00107,0.22049},{-0.399213,
-0.881962,0.22049},{-1.32294,-0.687475,0.22049},{-1.32294,-0.661471,0.178273},{-1.31144,
-0.661471,0.22049},{-0.377213,-0.661471,0.22049},{-1.10245,-0.495393,0.22049},{-1.10245,
-0.440981,0.0188297},{-1.00107,-0.440981,0.22049},{-0.440981,-0.489642,0.22049},{-0.440981,
-0.440981,0.186287},{-0.489642,-0.440981,0.22049},{-0.881962,-0.399213,0.22049},{-0.661471,
-0.377213,0.22049},{0.440981,0.440981,0.186287},{0.661471,0.377212,0.22049},{0.489642,
0.440981,0.22049},{0.881962,0.399213,0.22049},{1.10245,0.440981,0.0188297},{1.00107,
0.440981,0.22049},{0.440981,0.489642,0.22049},{0.377212,0.661471,0.22049},{1.10245,
0.495393,0.22049},{1.32294,0.661471,0.178273},{1.31144,0.661471,0.22049},{0.399213,
0.881962,0.22049},{1.32294,0.687475,0.22049},{1.41577,0.881962,0.22049},{0.440981,1.00107,
0.22049},{0.440981,1.10245,0.0188297},{0.495393,1.10245,0.22049},{1.39706,1.10245,
0.22049},{0.661471,1.31144,0.22049},{0.661471,1.32294,0.178273},{0.687475,1.32294,
0.22049},{1.22702,1.32294,0.22049},{1.32294,1.22702,0.22049},{0.881962,1.41577,0.22049},
{1.10245,1.39706,0.22049},{-1.10245,-1.32294,0.316888},{-0.881962,-1.32294,0.34278},
{-1.32294,-1.10245,0.316888},{-1.10245,-1.16561,0.440981},{-1.16561,-1.10245,0.440981},
{-0.881962,-1.20801,0.440981},{-0.661471,-1.10245,0.41619},{-0.726405,-1.10245,0.440981},
{-1.32294,-0.881962,0.34278},{-1.20801,-0.881962,0.440981},{-0.661471,-1.02127,0.440981},
{-0.440981,-0.881962,0.288811},{-0.616876,-0.881962,0.440981},{-1.10245,-0.726405,0.440981},
{-1.10245,-0.661471,0.41619},{-1.02127,-0.661471,0.440981},{-0.661471,-0.706694,0.440981},
{-0.661471,-0.661471,0.434723},{-0.706694,-0.661471,0.440981},{-0.440981,-0.661471,
0.303404},{-0.881962,-0.616876,0.440981},{-0.881962,-0.440981,0.288811},{-0.661471,-0.440981,
0.303404},{0.661471,0.440981,0.303404},{0.881962,0.440981,0.288811},{0.440981,0.661471,
0.303404},{0.661471,0.661471,0.434723},{0.881962,0.616876,0.440981},{0.706694,0.661471,
0.440981},{1.10245,0.661471,0.41619},{1.02127,0.661471,0.440981},{0.440981,0.881962,
0.288811},{0.661471,0.706694,0.440981},{0.616876,0.881962,0.440981},{1.10245,0.726405,
0.440981},{1.32294,0.881962,0.34278},{1.20801,0.881962,0.440981},{0.661471,1.02127,
0.440981},{0.661471,1.10245,0.41619},{0.726405,1.10245,0.440981},{1.32294,1.10245,
0.316888},{1.16561,1.10245,0.440981},{0.881962,1.20801,0.440981},{0.881962,1.32294,
0.34278},{1.10245,1.16561,0.440981},{1.10245,1.32294,0.316888},{-1.10245,-1.10245,0.472801},
{-0.881962,-1.10245,0.497781},{-1.10245,-0.881962,0.497781},{-0.881962,-0.881962,0.531455},
{-0.661471,-0.881962,0.474746},{-0.881962,-0.661471,0.474746},{0.881962,0.661471,0.474746},
{0.661471,0.881962,0.474746},{0.881962,0.881962,0.531455},{1.10245,0.881962,0.497781},
{0.881962,1.10245,0.497781},{1.10245,1.10245,0.472801}
};
	int connect<NEportLevels={2,0}>[1512] = {
   0,1,2,3,4,1,3,1,0,5,4,3,6,0,2,6,2,7,8,3,0,8,0,6,9,10,5,9,5,3,9,3,8,
11,10,9,12,6,7,13,8,6,13,6,12,13,12,14,15,9,8,15,8,13,15,13,16,15,11,9,
17,13,14,17,16,13,18,19,20,21,19,18,22,23,24,25,18,20,25,20,23,25,23,22,
26,27,21,26,21,18,26,18,25,28,27,26,29,22,24,30,25,22,30,22,29,30,29,31,
32,26,25,32,25,30,33,28,26,33,26,32,34,30,31,35,32,30,35,30,34,35,33,32,
36,37,38,39,40,37,39,37,36,41,40,39,42,43,44,2,1,36,2,36,38,2,38,42,38,
43,42,1,4,39,1,39,36,4,5,45,4,45,46,4,46,39,46,41,39,47,46,45,48,42,44,
48,44,49,7,2,42,7,42,48,5,10,45,10,11,50,10,50,51,10,51,45,51,47,45,52,
51,50,53,48,49,12,7,48,12,48,53,12,53,55,53,54,55,14,12,55,15,16,56,11,
15,56,11,56,57,11,57,50,58,52,50,58,50,57,59,55,54,17,14,55,17,55,59,17,
59,61,59,60,61,16,17,61,16,61,62,16,62,56,63,57,56,63,56,62,63,62,64,63,
58,57,65,61,60,66,62,61,66,61,65,66,64,62,67,68,69,70,71,68,70,68,67,72,
71,70,73,74,75,76,67,69,76,69,74,76,74,73,20,19,70,20,70,67,20,67,76,19,
21,77,19,77,78,19,78,70,78,72,70,79,78,77,80,73,75,80,75,81,24,23,76,24,
76,73,24,73,80,23,20,76,21,27,77,27,28,82,27,82,83,27,83,77,83,79,77,84,
83,82,85,80,81,29,24,80,29,80,85,29,85,87,85,86,87,31,29,87,28,33,88,28,
88,82,89,84,82,89,82,88,90,87,86,34,31,87,34,87,90,34,90,92,90,91,92,35,
34,92,35,92,93,33,35,93,33,93,94,33,94,88,94,95,88,95,89,88,96,92,91,97,
93,92,97,92,96,97,94,93,38,37,98,38,98,99,37,40,100,37,100,98,40,41,101,
40,101,102,40,102,100,103,102,101,44,43,104,44,104,105,43,38,99,43,99,104,
41,46,101,46,47,106,46,106,107,46,107,101,107,103,101,108,107,106,49,44,
105,49,105,109,47,51,106,51,52,110,51,110,108,51,108,106,53,49,109,53,109,
111,53,111,112,54,53,112,52,58,113,52,113,110,114,112,111,59,54,112,59,
112,114,59,114,116,114,115,116,60,59,116,63,64,117,58,63,117,58,117,118,
58,118,113,119,116,115,65,60,116,65,116,119,65,119,120,66,65,120,66,120,
121,64,66,121,64,121,122,64,122,117,122,118,117,123,124,125,69,68,126,69,
126,124,69,124,123,68,71,127,68,127,126,71,72,128,71,128,129,71,129,127,
130,129,128,75,74,123,75,123,125,75,125,131,74,69,123,72,78,128,78,79,132,
78,132,133,78,133,128,133,130,128,134,133,132,81,75,131,81,131,135,79,83,
132,83,84,136,83,136,134,83,134,132,85,81,135,85,135,137,85,137,138,86,
85,138,84,89,139,84,139,136,140,138,137,90,86,138,90,138,140,90,140,142,
140,141,142,91,90,142,95,94,143,95,143,144,89,95,144,89,144,139,145,142,
141,96,91,142,96,142,145,96,145,146,97,96,146,97,146,147,94,97,147,94,147,
143,99,98,148,99,148,149,98,100,150,98,150,148,100,102,151,100,151,152,100,
152,150,102,103,151,105,104,153,105,153,154,104,99,149,104,149,153,151,155,
152,103,107,156,103,156,157,103,157,151,157,155,151,107,108,156,109,105,
154,109,154,158,156,159,157,108,110,160,108,160,159,108,159,156,111,109,
158,111,158,161,111,161,162,161,163,162,110,113,164,110,164,160,114,111,
162,115,114,162,115,162,163,115,163,166,163,165,166,165,167,166,168,169,
170,113,118,169,113,169,168,113,168,164,119,115,166,120,119,166,120,166,
167,120,167,171,121,120,171,121,171,172,122,121,172,122,172,170,122,170,
169,118,122,169,125,124,173,124,126,174,124,174,175,124,175,173,126,127,
176,126,176,174,127,129,177,127,177,178,127,178,176,129,130,177,131,125,
173,131,173,179,131,179,180,175,179,173,177,181,178,130,133,182,130,182,
183,130,183,177,183,181,177,133,134,182,135,131,180,135,180,184,182,185,
183,134,136,186,134,186,185,134,185,182,137,135,184,137,184,187,137,187,
188,187,189,188,136,139,190,136,190,186,140,137,188,141,140,188,141,188,
189,141,189,192,189,191,192,191,193,192,144,143,194,144,194,195,139,144,
195,139,195,190,145,141,192,146,145,192,146,192,193,146,193,196,147,146,
196,147,196,197,143,147,197,143,197,194,149,148,198,148,150,199,148,199,
198,150,152,199,154,153,200,153,149,198,153,198,201,153,201,200,201,202,
200,199,203,201,199,201,198,152,155,204,152,204,205,152,205,199,205,203,
199,155,157,204,158,154,200,158,200,206,202,207,206,202,206,200,204,208,
205,157,159,209,157,209,210,157,210,204,210,208,204,159,160,209,161,158,
206,163,161,206,163,206,207,163,207,212,207,211,212,211,213,212,214,215,
216,209,217,215,209,215,214,209,214,210,160,164,217,160,217,209,165,163,
212,167,165,212,167,212,213,167,213,219,213,218,219,215,220,219,215,219,
218,215,218,216,168,170,220,168,220,215,168,215,217,164,168,217,171,167,
219,172,171,219,172,219,220,170,172,220,175,174,221,174,176,222,174,222,
221,176,178,222,180,179,223,179,175,221,179,221,224,179,224,223,222,225,
226,222,226,224,222,224,221,178,181,227,178,227,228,178,228,222,228,225,
222,181,183,227,184,180,223,184,223,229,224,230,231,224,231,229,224,229,
223,226,230,224,227,232,228,183,185,233,183,233,234,183,234,227,234,232,
227,185,186,233,187,184,229,189,187,229,189,229,231,189,231,236,231,235,
236,235,237,236,233,238,239,233,239,234,186,190,238,186,238,233,191,189,
236,193,191,236,193,236,237,193,237,241,237,240,241,242,243,241,242,241,
240,195,194,243,195,243,242,195,242,238,242,239,238,190,195,238,196,193,
241,197,196,241,197,241,243,194,197,243,202,201,244,201,203,245,201,245,
244,203,205,245,207,202,244,207,244,246,245,247,246,245,246,244,205,208,
248,205,248,247,205,247,245,208,210,248,211,207,246,213,211,246,213,246,
247,213,247,249,214,216,249,214,249,247,214,247,248,210,214,248,218,213,
249,216,218,249,226,225,250,225,228,250,231,230,251,230,226,250,230,250,
252,230,252,251,228,232,253,228,253,252,228,252,250,232,234,253,235,231,
251,237,235,251,237,251,252,237,252,254,253,255,254,253,254,252,234,239,
255,234,255,253,240,237,254,242,240,254,242,254,255,239,242,255
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	  xform {
	    mat = {
	      {1.0, 0.0, 0.0, 0.0},
	      {0.0, 1.0, 0.0, 0.0},
	      {0.0, 0.0, 1.0, 0.0},
	      {0.0, 0.0, 0.0, 1.2}
	    };
	  };
	};
      };
      group dn_mesh {
	float coord<NEportLevels={2,0}>[256][3] = {
   {0.881962,-1.10245,-0.497781},{0.881962,-1.20801,-0.440981},{0.726405,-1.10245,
-0.440981},{1.10245,-1.10245,-0.472801},{1.10245,-1.16561,-0.440981},{1.16561,-1.10245,
-0.440981},{0.661471,-0.881962,-0.474746},{0.661471,-1.02127,-0.440981},{0.616876,-0.881962,
-0.440981},{0.881962,-0.881962,-0.531455},{1.10245,-0.881962,-0.497781},{1.20801,-0.881962,
-0.440981},{0.661471,-0.706694,-0.440981},{0.881962,-0.661471,-0.474746},{0.706694,
-0.661471,-0.440981},{1.10245,-0.726405,-0.440981},{1.02127,-0.661471,-0.440981},{0.881962,
-0.616876,-0.440981},{-0.881962,0.661471,-0.474746},{-0.881962,0.616876,-0.440981},
{-1.02127,0.661471,-0.440981},{-0.706694,0.661471,-0.440981},{-1.10245,0.881962,-0.497781},
{-1.10245,0.726405,-0.440981},{-1.20801,0.881962,-0.440981},{-0.881962,0.881962,-0.531455},
{-0.661471,0.881962,-0.474746},{-0.661471,0.706694,-0.440981},{-0.616876,0.881962,
-0.440981},{-1.10245,1.10245,-0.472801},{-1.16561,1.10245,-0.440981},{-0.881962,1.10245,
-0.497781},{-0.661471,1.02127,-0.440981},{-0.726405,1.10245,-0.440981},{-1.10245,1.16561,
-0.440981},{-0.881962,1.20801,-0.440981},{0.881962,-1.32294,-0.34278},{0.881962,-1.41577,
-0.22049},{0.687475,-1.32294,-0.22049},{1.10245,-1.32294,-0.316888},{1.10245,-1.39706,
-0.22049},{1.22702,-1.32294,-0.22049},{0.661471,-1.10245,-0.41619},{0.661471,-1.31144,
-0.22049},{0.495393,-1.10245,-0.22049},{1.32294,-1.10245,-0.316888},{1.32294,-1.22702,
-0.22049},{1.39706,-1.10245,-0.22049},{0.440981,-0.881962,-0.288812},{0.440981,-1.00107,
-0.22049},{0.399213,-0.881962,-0.22049},{1.32294,-0.881962,-0.34278},{1.41577,-0.881962,
-0.22049},{0.440981,-0.661471,-0.303404},{0.377212,-0.661471,-0.22049},{0.661471,-0.661471,
-0.434723},{1.10245,-0.661471,-0.41619},{1.31144,-0.661471,-0.22049},{1.32294,-0.687475,
-0.22049},{0.440981,-0.489642,-0.22049},{0.661471,-0.440981,-0.303404},{0.489642,-0.440981,
-0.22049},{0.881962,-0.440981,-0.288812},{1.00107,-0.440981,-0.22049},{1.10245,-0.495393,
-0.22049},{0.661471,-0.377213,-0.22049},{0.881962,-0.399213,-0.22049},{-0.881962,0.440981,
-0.288812},{-0.881962,0.399213,-0.22049},{-1.00107,0.440981,-0.22049},{-0.661471,0.440981,
-0.303404},{-0.661471,0.377212,-0.22049},{-0.489642,0.440981,-0.22049},{-1.10245,0.661471,
-0.41619},{-1.10245,0.495393,-0.22049},{-1.31144,0.661471,-0.22049},{-0.661471,0.661471,
-0.434723},{-0.440981,0.661471,-0.303404},{-0.440981,0.489642,-0.22049},{-0.377213,0.661471,
-0.22049},{-1.32294,0.881962,-0.34278},{-1.32294,0.687475,-0.22049},{-1.41577,0.881962,
-0.22049},{-0.440981,0.881962,-0.288812},{-0.399213,0.881962,-0.22049},{-1.32294,1.10245,
-0.316888},{-1.39706,1.10245,-0.22049},{-0.661471,1.10245,-0.41619},{-0.495393,1.10245,
-0.22049},{-0.440981,1.00107,-0.22049},{-1.32294,1.22702,-0.22049},{-1.22702,1.32294,
-0.22049},{-1.10245,1.32294,-0.316888},{-0.881962,1.32294,-0.34278},{-0.687475,1.32294,
-0.22049},{-0.661471,1.31144,-0.22049},{-1.10245,1.39706,-0.22049},{-0.881962,1.41577,
-0.22049},{0.661471,-1.32294,-0.178273},{0.661471,-1.37248,-5.96046e-08},{0.615387,
-1.32294,-5.96046e-08},{0.881962,-1.46582,-5.96046e-08},{1.10245,-1.4503,-5.96046e-08},
{1.31281,-1.32294,-5.96046e-08},{0.440981,-1.10245,-0.0188299},{0.440981,-1.10987,
-5.96046e-08},{0.437269,-1.10245,-5.96046e-08},{1.32294,-1.31281,-5.96046e-08},{1.4503,
-1.10245,-5.96046e-08},{0.348794,-0.881962,-5.96046e-08},{1.46582,-0.881962,-5.96046e-08},
{0.310812,-0.661471,-5.96046e-08},{1.32294,-0.661471,-0.178273},{1.37248,-0.661471,
-5.96046e-08},{0.341552,-0.440981,-5.96046e-08},{0.440981,-0.440981,-0.186287},{1.10245,
-0.440981,-0.0188299},{1.10987,-0.440981,-5.96046e-08},{1.32294,-0.615388,-5.96046e-08},
{0.440981,-0.341552,-5.96046e-08},{0.661471,-0.310812,-5.96046e-08},{0.881962,-0.348794,
-5.96046e-08},{1.10245,-0.437269,-5.96046e-08},{-1.10245,0.440981,-0.0188299},{-1.10245,
0.437269,-5.96046e-08},{-1.10987,0.440981,-5.96046e-08},{-0.881962,0.348794,-5.96046e-08},
{-0.661471,0.310812,-5.96046e-08},{-0.440981,0.440981,-0.186287},{-0.440981,0.341552,
-5.96046e-08},{-0.341552,0.440981,-5.96046e-08},{-1.32294,0.661471,-0.178273},{-1.32294,
0.615387,-5.96046e-08},{-1.37248,0.661471,-5.96046e-08},{-0.310812,0.661471,-5.96046e-08},
{-1.46582,0.881962,-5.96046e-08},{-0.348794,0.881962,-5.96046e-08},{-1.4503,1.10245,
-5.96046e-08},{-0.440981,1.10245,-0.0188299},{-0.437269,1.10245,-5.96046e-08},{-1.32294,
1.31281,-5.96046e-08},{-1.31281,1.32294,-5.96046e-08},{-0.661471,1.32294,-0.178273},
{-0.615388,1.32294,-5.96046e-08},{-0.440981,1.10987,-5.96046e-08},{-1.10245,1.4503,
-5.96046e-08},{-0.881962,1.46582,-5.96046e-08},{-0.661471,1.37248,-5.96046e-08},{0.661471,
-1.32294,0.178273},{0.881962,-1.41577,0.22049},{0.687475,-1.32294,0.22049},{1.10245,
-1.39706,0.22049},{1.22702,-1.32294,0.22049},{0.440981,-1.10245,0.0188297},{0.661471,
-1.31144,0.22049},{0.495393,-1.10245,0.22049},{1.32294,-1.22702,0.22049},{1.39706,
-1.10245,0.22049},{0.440981,-1.00107,0.22049},{0.399213,-0.881962,0.22049},{1.41577,
-0.881962,0.22049},{0.377212,-0.661471,0.22049},{1.32294,-0.687475,0.22049},{1.32294,
-0.661471,0.178273},{1.31144,-0.661471,0.22049},{0.440981,-0.489642,0.22049},{0.440981,
-0.440981,0.186287},{0.489642,-0.440981,0.22049},{1.10245,-0.495393,0.22049},{1.10245,
-0.440981,0.0188297},{1.00107,-0.440981,0.22049},{0.661471,-0.377213,0.22049},{0.881962,
-0.399213,0.22049},{-1.10245,0.440981,0.0188297},{-0.881962,0.399213,0.22049},{-1.00107,
0.440981,0.22049},{-0.661471,0.377212,0.22049},{-0.440981,0.440981,0.186287},{-0.489642,
0.440981,0.22049},{-1.32294,0.661471,0.178273},{-1.10245,0.495393,0.22049},{-1.31144,
0.661471,0.22049},{-0.440981,0.489642,0.22049},{-0.377213,0.661471,0.22049},{-1.32294,
0.687475,0.22049},{-1.41577,0.881962,0.22049},{-0.399213,0.881962,0.22049},{-1.39706,
1.10245,0.22049},{-0.440981,1.00107,0.22049},{-0.440981,1.10245,0.0188297},{-0.495393,
1.10245,0.22049},{-1.32294,1.22702,0.22049},{-1.22702,1.32294,0.22049},{-0.661471,1.31144,
0.22049},{-0.661471,1.32294,0.178273},{-0.687475,1.32294,0.22049},{-1.10245,1.39706,
0.22049},{-0.881962,1.41577,0.22049},{0.881962,-1.32294,0.34278},{1.10245,-1.32294,
0.316888},{0.661471,-1.10245,0.41619},{0.881962,-1.20801,0.440981},{0.726405,-1.10245,
0.440981},{1.10245,-1.16561,0.440981},{1.32294,-1.10245,0.316888},{1.16561,-1.10245,
0.440981},{0.440981,-0.881962,0.288811},{0.661471,-1.02127,0.440981},{0.616876,-0.881962,
0.440981},{1.32294,-0.881962,0.34278},{1.20801,-0.881962,0.440981},{0.440981,-0.661471,
0.303404},{0.661471,-0.706694,0.440981},{0.661471,-0.661471,0.434723},{0.706694,-0.661471,
0.440981},{1.10245,-0.726405,0.440981},{1.10245,-0.661471,0.41619},{1.02127,-0.661471,
0.440981},{0.661471,-0.440981,0.303404},{0.881962,-0.616876,0.440981},{0.881962,-0.440981,
0.288811},{-0.881962,0.440981,0.288811},{-0.661471,0.440981,0.303404},{-1.10245,0.661471,
0.41619},{-0.881962,0.616876,0.440981},{-1.02127,0.661471,0.440981},{-0.661471,0.661471,
0.434723},{-0.706694,0.661471,0.440981},{-0.440981,0.661471,0.303404},{-1.32294,0.881962,
0.34278},{-1.10245,0.726405,0.440981},{-1.20801,0.881962,0.440981},{-0.661471,0.706694,
0.440981},{-0.440981,0.881962,0.288811},{-0.616876,0.881962,0.440981},{-1.32294,1.10245,
0.316888},{-1.16561,1.10245,0.440981},{-0.661471,1.02127,0.440981},{-0.661471,1.10245,
0.41619},{-0.726405,1.10245,0.440981},{-1.10245,1.16561,0.440981},{-1.10245,1.32294,
0.316888},{-0.881962,1.20801,0.440981},{-0.881962,1.32294,0.34278},{0.881962,-1.10245,
0.497781},{1.10245,-1.10245,0.472801},{0.661471,-0.881962,0.474746},{0.881962,-0.881962,
0.531455},{1.10245,-0.881962,0.497781},{0.881962,-0.661471,0.474746},{-0.881962,0.661471,
0.474746},{-1.10245,0.881962,0.497781},{-0.881962,0.881962,0.531455},{-0.661471,0.881962,
0.474746},{-1.10245,1.10245,0.472801},{-0.881962,1.10245,0.497781}
};
	int connect<NEportLevels={2,0}>[1512] = {
   0,1,2,3,4,1,3,1,0,5,4,3,6,7,8,9,0,2,9,2,7,9,7,6,10,3,0,10,0,9,11,5,
3,11,3,10,12,6,8,13,9,6,13,6,12,13,12,14,15,10,9,15,9,13,15,13,16,15,11,
10,17,13,14,17,16,13,18,19,20,21,19,18,22,23,24,25,18,20,25,20,23,25,23,
22,26,27,21,26,21,18,26,18,25,28,27,26,29,22,24,29,24,30,31,25,22,31,22,
29,32,26,25,32,25,31,32,31,33,32,28,26,34,29,30,35,31,29,35,29,34,35,33,
31,36,37,38,39,40,37,39,37,36,41,40,39,42,43,44,2,1,36,2,36,38,2,38,42,
38,43,42,1,4,39,1,39,36,4,5,45,4,45,46,4,46,39,46,41,39,47,46,45,48,49,
50,8,7,42,8,42,44,8,44,48,44,49,48,7,2,42,5,11,51,5,51,45,52,47,45,52,45,
51,53,48,50,53,50,54,12,8,48,12,48,53,12,53,55,14,12,55,15,16,56,11,15,
56,11,56,57,11,57,51,57,58,51,58,52,51,59,53,54,60,55,53,60,53,59,60,59,
61,17,14,55,17,55,60,17,60,62,16,17,62,16,62,63,16,63,56,63,64,56,64,57,
56,65,60,61,66,62,60,66,60,65,66,63,62,67,68,69,70,71,68,70,68,67,72,71,
70,73,74,75,20,19,67,20,67,69,20,69,73,69,74,73,19,21,76,19,76,70,19,70,
67,77,78,72,77,72,70,77,70,76,79,78,77,80,81,82,24,23,73,24,73,75,24,75,
80,75,81,80,23,20,73,21,27,76,27,28,83,27,83,77,27,77,76,84,79,77,84,77,
83,85,80,82,85,82,86,30,24,80,30,80,85,32,33,87,28,32,87,28,87,88,28,88,
83,88,89,83,89,84,83,90,85,86,34,30,85,34,85,90,34,90,92,90,91,92,35,34,
92,35,92,93,33,35,93,33,93,94,33,94,87,94,95,87,95,88,87,96,92,91,97,93,
92,97,92,96,97,94,93,98,99,100,38,37,101,38,101,99,38,99,98,37,40,102,37,
102,101,40,41,103,40,103,102,104,105,106,44,43,98,44,98,100,44,100,104,100,
105,104,43,38,98,41,46,107,41,107,103,46,47,108,46,108,107,50,49,104,50,
104,106,50,106,109,49,44,104,47,52,110,47,110,108,54,50,109,54,109,111,58,
57,112,52,58,112,52,112,113,52,113,110,59,54,111,59,111,114,59,114,115,61,
59,115,64,63,116,57,64,116,57,116,117,57,117,112,117,118,112,118,113,112,
119,115,114,65,61,115,65,115,119,65,119,120,66,65,120,66,120,121,63,66,
121,63,121,122,63,122,116,122,117,116,123,124,125,69,68,126,69,126,124,69,
124,123,68,71,127,68,127,126,71,72,128,71,128,129,71,129,127,130,129,128,
131,132,133,75,74,123,75,123,125,75,125,131,125,132,131,74,69,123,72,78,
128,78,79,134,78,134,130,78,130,128,82,81,131,82,131,133,82,133,135,81,
75,131,79,84,136,79,136,134,86,82,135,86,135,137,89,88,138,84,89,138,84,
138,139,84,139,136,90,86,137,90,137,140,91,90,140,91,140,141,95,94,142,88,
95,142,88,142,143,88,143,138,143,144,138,144,139,138,96,91,141,96,141,145,
97,96,145,97,145,146,94,97,146,94,146,147,94,147,142,147,143,142,100,99,
148,99,101,149,99,149,150,99,150,148,101,102,151,101,151,149,102,103,152,
102,152,151,106,105,153,105,100,148,105,148,154,105,154,153,154,155,153,
150,154,148,103,107,156,103,156,152,107,108,157,107,157,156,109,106,153,
109,153,158,109,158,159,155,158,153,108,110,160,108,160,157,111,109,159,
111,159,161,162,163,164,110,113,163,110,163,162,110,162,160,114,111,161,
114,161,165,114,165,166,165,167,166,168,169,170,118,117,169,118,169,168,
118,168,163,168,164,163,113,118,163,119,114,166,120,119,166,120,166,167,
120,167,171,121,120,171,121,171,172,122,121,172,122,172,170,122,170,169,
117,122,169,125,124,173,124,126,174,124,174,175,124,175,173,126,127,176,
126,176,174,127,129,177,127,177,178,127,178,176,129,130,177,133,132,179,
132,125,173,132,173,180,132,180,179,180,181,179,175,180,173,177,182,178,
130,134,183,130,183,182,130,182,177,135,133,179,135,179,184,135,184,185,
181,184,179,134,136,186,134,186,183,137,135,185,137,185,187,188,189,190,
136,139,189,136,189,188,136,188,186,140,137,187,140,187,191,141,140,191,
141,191,192,193,194,195,144,143,194,144,194,193,144,193,189,193,190,189,
139,144,189,145,141,192,145,192,196,146,145,196,146,196,197,147,146,197,
147,197,195,147,195,194,143,147,194,150,149,198,149,151,199,149,199,198,
151,152,199,155,154,200,154,150,198,154,198,201,154,201,200,201,202,200,
199,203,201,199,201,198,152,156,204,152,204,205,152,205,199,205,203,199,
156,157,204,159,158,206,158,155,200,158,200,207,158,207,206,207,208,206,
202,207,200,204,209,210,204,210,205,157,160,209,157,209,204,161,159,206,
161,206,211,212,213,211,212,211,206,212,206,208,212,214,213,215,216,217,
162,164,216,162,216,215,162,215,209,215,210,209,160,162,209,165,161,211,
167,165,211,167,211,213,167,213,218,219,220,218,219,218,213,219,213,214,
168,170,220,168,220,219,168,219,216,219,217,216,164,168,216,171,167,218,
172,171,218,172,218,220,170,172,220,175,174,221,174,176,222,174,222,221,
176,178,222,181,180,223,180,175,221,180,221,224,180,224,223,224,225,223,
222,226,227,222,227,224,222,224,221,178,182,228,178,228,226,178,226,222,
182,183,228,185,184,229,184,181,223,184,223,230,184,230,229,230,231,229,
225,230,223,226,232,227,228,233,234,228,234,232,228,232,226,183,186,233,
183,233,228,187,185,229,187,229,235,231,236,235,231,235,229,237,238,239,
188,190,238,188,238,237,188,237,233,237,234,233,186,188,233,191,187,235,
192,191,235,192,235,236,192,236,241,236,240,241,242,243,241,242,241,240,
193,195,243,193,243,242,193,242,238,242,239,238,190,193,238,196,192,241,
197,196,241,197,241,243,195,197,243,202,201,244,201,203,245,201,245,244,
203,205,245,208,207,246,207,202,244,207,244,247,207,247,246,245,248,247,
245,247,244,205,210,248,205,248,245,212,208,246,214,212,246,214,246,247,
214,247,249,215,217,249,215,249,247,215,247,248,210,215,248,219,214,249,
217,219,249,225,224,250,224,227,250,231,230,251,230,225,250,230,250,252,
230,252,251,227,232,253,227,253,252,227,252,250,232,234,253,236,231,251,
236,251,254,252,255,254,252,254,251,237,239,255,237,255,252,237,252,253,
234,237,253,240,236,254,242,240,254,242,254,255,239,242,255
};
	int coord_dims[] => array_dims(coord);
	int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
	GMOD.print_error print_error {
	  error => <-.error;
	  error_source = "tri_mesh";
	  error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	  on_inst = 1;
	};
	Mesh out<NEportLevels={0,2}> {
	  nnodes => switch((!error),<-.coord_dims[1]);
	  nspace => switch((!error),<-.coord_dims[0]);
	  coordinates {
            values => switch((!error),<-.<-.coord);
	  };
	  ncell_sets = 1;
	  Tri cell_set {
            ncells => switch((!error),(array_size(<-.<-.connect) / cell_nnodes));
            node_connect_list => switch((!error),<-.<-.connect);
	  };
	  xform {
	    mat = {
	      {1.0, 0.0, 0.0, 0.0},
	      {0.0, 1.0, 0.0, 0.0},
	      {0.0, 0.0, 1.0, 0.0},
	      {0.0, 0.0, 0.0, 1.2}
	    };
	  };
	};
      };
      dlv_glyph pglyph {
	in_field => <-.<-.orbs.orbital[8];
	in_glyph => <-.dp_mesh.out;
	GlyphParam {
	  map_comp = 1;
	  scale => <-.<-.<-.scale;
	};
      };
      dlv_glyph nglyph {
	in_field => <-.<-.orbs.orbital[8];
	in_glyph => dn_mesh.out;
	GlyphParam {
	  map_comp = 2;
	  scale => <-.<-.<-.scale;
	};
      };
    };
    GroupObject orbitals<NEx=198.,NEy=352.> {
      child_objs => {
	<-.s_orb.glyph.out_obj,
        <-.px_orb.pglyph.out_obj,
	<-.px_orb.nglyph.out_obj,
        <-.py_orb.pglyph.out_obj,
	<-.py_orb.nglyph.out_obj,
        <-.pz_orb.pglyph.out_obj,
	<-.pz_orb.nglyph.out_obj,
        <-.dz2r2_orb.pglyph.out_obj,
	<-.dz2r2_orb.nglyph.out_obj,
        <-.dxz_orb.pglyph.out_obj,
	<-.dxz_orb.nglyph.out_obj,
        <-.dyz_orb.pglyph.out_obj,
	<-.dyz_orb.nglyph.out_obj,
        <-.dx2y2_orb.pglyph.out_obj,
	<-.dx2y2_orb.nglyph.out_obj,
        <-.dxy_orb.pglyph.out_obj,
	<-.dxy_orb.nglyph.out_obj
      };
    };
    out_obj => .orbitals.obj;
  };
  */
};
