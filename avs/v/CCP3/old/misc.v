
flibrary Misc<NEeditable=1> {
  VUIslider_typein CCP3slider_typein {
    valEditor {
      UI {
	value_field {
	  updateMode = 7;
	};
	min_field {
	  updateMode = 7;
	};
	max_field {
	  updateMode = 7;
	};
      };
    };
    width => ((slider.parent.clientWidth - slider.width) - 2);
  };
  // Todo - do I need this, copied from v2 for temporary use by viewers.
  macro CCP3shell {
    // This just allows me to avoid renaming all the ones that use it.
    UIframe UIpanel<NEx=528.,NEy=165.,NEportLevels={0,2}> {
      parent => <-.panel;
      width => <-.panel.clientWidth;
      height => (<-.panel.clientHeight - 45);
    };
    UIpanel panel<NEx=407.,NEy=275.> {
      parent => <-.UIshell;
      width => <-.UIshell.clientWidth;
      height => <-.UIshell.clientHeight;
    };
    UIbutton close<NEx=264.,NEy=165.> {
      parent => <-.panel;
      x => ((<-.panel.clientWidth - .width) / 2);
      y => (<-.panel.clientHeight - 40);
      height = 35;
      width = 80;
      label => "Close";
    };
    GMOD.parse_v parse_v<NEx=242.,NEy=275.> {
      v_commands = "UIshell.visible = 0;";
      trigger => <-.close.do;
      on_inst = 0;
      relative => <-;
    };
    UIshell UIshell<NEx=132.,NEy=88.> {
      visible<NEportLevels={3,0}>;
      showStatusBar = 0;
    };
  };
};
