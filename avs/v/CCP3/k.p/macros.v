
flibrary Macros<NEeditable=1,
#ifndef DLV_NO_DLLS
		dyn_libs="libCCP3k_p",
#endif // DLV_NO_DLLS
		export_cxx=1,
		build_dir="avs/src/express",
		cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/k.p/str_gen.hxx avs/src/k.p/load_gen.hxx avs/src/k.p/mak_gen.hxx avs/src/k.p/axis_gen.hxx",
		out_hdr_file="k_p.hxx",
		out_src_file="k_p.cxx"> {
  macro calcBase {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=198.,NEy=66.>;
    link active_menus<NEportLevels=1,NEx=385.,NEy=66.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=143.> {
      active => <-.active_menus;
      do => <-.visible;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
    };
  };
  // Bascially a copy of core/UI/file_ops LoadUI
  macro loadstrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.K_P.Modules.StructureData &data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Load k.p structure file";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width  => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel UIlabel<NEx=660.,NEy=165.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      alignment = "center";
      width => parent.clientWidth;
      label = "Model name";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=671.,NEy=242.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y => (<-.UIlabel.y + <-.UIlabel.height + 5);
      width => (parent.clientWidth - 10);
      text => <-.data.name;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => <-.data.new_view;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*";
      parent1<NEportLevels={2,0}> => <-.panel;
      title = "Load k.p Structure filename";
      file_write = 0;
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*";
      parent => <-.panel;
      title = "Load k.p Structure filename";
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
    };
    GMOD.parse_v reset_params<NEx=583.,NEy=77.> {
      v_commands = "data.new_view = 0; data.name = \"\";";
      relative => <-;
    };
  };
  calcBase Load_Structure {
    UIcmd {
      label = "Load Structure";
    };
    CCP3.K_P.Modules.StructureData data<NEx=627.,NEy=143.,
      NEportLevels={0,1}> {
      name = "";
      file = "";
      new_view = 0;
    };
    instancer {
      Group => <-.loadstrUI;
    };
    loadstrUI loadstrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.K_P.Modules.load_structure load_structure<NEx=407.,NEy=484.> {
      data => <-.data;
      do_load => <-.loadstrUI.dialog.ok;
    };
  };
macro CreateStrUI<NEx=387.,NEy=90.,NExOffset=160.,NEyOffset=98.> {
  CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
					   NEx=286.,NEy=22.>;
  CCP3.K_P.Modules.InputData &data<NEx=792.,NEy=77.,
				   NEportLevels={2,0}>;
  CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
    width = 300;
    height = 600;
    &color => <-.prefs.colour;
    &fontAttributes => <-.prefs.fontAttributes;
    title => "Load k.p structure file";
    ok = 0;
    cancel = 0;
    okButton = 1;
    cancelButton = 1;
  };
  CCP3.Core_Macros.UI.UIobjs.DLVradioBox DLVradioBox<NEx=243.,NEy=54.,NExOffset=9.,NEyOffset=-250.> {
    parent => <-.UIpanel;
    labels => <-.string;
    selectedItem => <-.data.shape;
    title => "Select a Shape";
    UIradioBox<NEportLevels={0,2},NEx=495.,NEy=450.> {
      cmdList<NEportLevels={3,0}>;
    };
  };
  string string<NEportLevels={1,1},NEx=45.,NEy=54.>[] = {"Pyramidal QD",
							 "N-agonal QD", "Cylindrical QD", "Quantum Ring","QD in Quantum Wire", "Spherical QD", "Core/Shell QD", "Core/Double Shell QD"};
  CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein DLVffieldTypeinBase<NEx=216.,NEy=171.> {
    UIparent => <-.UIpanel;
    string array[]= {"Base", "Base", "Radius", "Radius", "Radius", "Radius", "Core Radius", "Core Radius"};
    label {
      label<export=3> => array[<-.<-.data.shape] ;
    };
    x = 0;
    y = 250;
    fval => <-.data.base;
    fmin => 0.1;
  };
  CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein DLVffieldTypeinHeight<NEx=216.,NEy=225.> {
    UIparent => <-.UIpanel;
    label {
      label<export=3> => switch ((<-.<-.data.shape >= 5)+1, "Height","Shell Radius");
    };
    x = 0;
    y = 300;
    fval => <-.data.height;
    fmin => 0.1;
    panel {
      visible => (<-.<-.data.shape <= 4 || <-.<-.data.shape >= 6) ;
    };
  };
  CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein DLVffieldTypeinTopSqLength<NEx=216.,NEy=279.> {
    UIparent => <-.UIpanel;
    fval => <-.data.c;
    //label {<-.<-.data.shape == 3
      // label<export=3> => switch ((<-.<-.data.shape >= 3)+1, "Top Diameter","Inner Radius", "2nd Shell Radius");
    //};
    string array1[]= {"", "Top Side Length", "", "Inner Radius","", "", "", "2nd Shell Radius"};
    label {
      label<export=3> => array1[<-.<-.data.shape] ;
    };
    x = 0;
    y = 350;
    fmin => 0.1;
    panel {
      visible => (<-.<-.data.shape == 1 || <-.<-.data.shape == 3 || <-.<-.data.shape == 7);
    };
  };
  CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein DLVifieldTypeinNumOfSides<NEx=216.,NEy=333.,NExOffset=-10.,NEyOffset=-264.> {
    UIparent => <-.UIpanel;
    label {
      label<export=3> => "Num of sides";
    };
    x = 0;
    y = 400;
    fval => <-.data.n;
    fmin => 3;
    panel {
      visible => <-.<-.data.shape == 1;
    };
  };								  
  UIpanel UIpanel<NEx=0.,NEy=-9.> {
    y = 0;
    width => parent.clientWidth;
    height => <-.UIapp.clientHeight;
    parent => <-.dialog; width<NEdisplayMode="open"> = 600;
  };
  CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=36.,NEy=153.> {
    parent => <-.UIpanel;
    set=> <-.data.WL;
    y = 450;
    width => parent.clientWidth/2;
    message<NEdisplayMode="open"> = "Wetting Layer";
    label<NEdisplayMode="open"> => "Wetting Layer";
  };
  CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggleLat<NEx=27.,NEy=234.> {
      parent => <-.UIpanel;
      label => "Lattice";
      set => <-.data.lat;
      width = 500;
      y = 25;
      x => parent.clientWidth / 2;
   };
  
 CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein DLVffieldTypeinWLthickness<NEx=216.,NEy=387.,NExOffset=-8.,NEyOffset=-1.> {
       UIparent => <-.UIpanel;
       label {
         label<export=3> => "Thickness";
         };
         x = 0;
         y = 500;
       fval => <-.data.WLthickness;
       fmin => 0.1;
       panel {
          visible => <-.<-.data.WL == 1;
       };
 };	
  CCP3.Core_Macros.UI.UIobjs.DLVradioBox DLVradioBoxLat<NEx=459.,NEy=108.> {
      parent => <-.UIpanel;
      labels => <-.array2;
      selectedItem => <-.data.dim;
      x => parent.clientWidth / 2;
      y = 50;
      title => "Select Lattice Dimensions";
      visible => <-.<-.data.lat == 1;
       
      
      };
   string array2[3] = { "1D", "2D", "3D"};
   
   CCP3.Core_Macros.UI.UIobjs.DLVradioBox DLVradioBoxDir{
      parent => <-.UIpanel;
      labels => <-.arrayDir;
      selectedItem => <-.data.dir;
      x => parent.clientWidth / 2;
      y = 550;
      title => "Select Orientation";
      visible => (<-.<-.data.lat == 1) && ((<-.<-.data.dim == 0) || (<-.<-.data.dim == 1));
       
      
      };
   string arrayDir[3] = { "X", "Y", "Z"};
   
   CCP3.Core_Macros.UI.UIobjs.DLVradioBox DLVradioBoxLatType<NEx=486.,NEy=189.> {
      parent => <-.UIpanel;
      labels => switch(<-.data.dim+1,<-.stringLatType,<-.stringLatType,<-.stringLatType2);
      selectedItem => <-.data.LatType;
      x => parent.clientWidth / 2;
      y = 200;
      width = 250; 
      title => "Lattice Type";
      visible => ((<-.<-.data.dim == 1) || (<-.<-.data.dim == 2));
      
   };
   string stringLatType<NEportLevels=1,NEx=594.,NEy=243.>[3] = {
      "Rectangular","Square","Hexagonal"
   };
   
   string stringLatType2[4] = {
   
    "Orthorhombic", "Tetragonal", "Hexagonal", "Cubic"
   
   };
   
   CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein DLVffieldTypeina<NEx=513.,NEy=324.> {
   UIparent => <-.UIpanel;
   fval => <-.data.a1;
   label {
         label<export=3> => "a";
         };
    x => UIparent.clientWidth / 2;
    y = 400;
    panel {
    visible => (<-.<-.data.lat == 1); 
         };
   
    };
    
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein DLVffieldTypeinb<NEx=513.,NEy=324.> {
   UIparent => <-.UIpanel;
   fval => <-.data.a2;
   label {
         label<export=3> => "b";
         };
    x => UIparent.clientWidth / 2;
    y = 450;
    panel {
    visible => ((<-.<-.data.dim == 1 && (<-.<-.data.LatType == 0 || <-.<-.data.LatType == 2)) || ( <-.<-.data.dim == 2 && <-.<-.data.LatType == 0));
         };
   
    };
    
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein DLVffieldTypeinc<NEx=513.,NEy=324.> {
   UIparent => <-.UIpanel;
   fval => <-.data.a3;
   label {
         label<export=3> => "c";
         };
    x => UIparent.clientWidth / 2;
    y = 500;
    panel {
    visible => <-.<-.data.dim == 2 && <-.<-.data.LatType != 3; 
         };
   
    };

};


   calcBase Create_Structure {
    UIcmd {
      label = "Create Structure";
    };
    CCP3.K_P.Modules.InputData data<NEx=627.,NEy=143.,
      NEportLevels={0,1}> {
      base = 1.0;
      height = 1.0;
      shape = 0;			   
      c = 1.0;
      WL = 0;	
      n = 3;
      WLthickness = 1;
      lat = 0;
      dim = 0;
      LatType; 
      a1 = 10;
      a2 = 10;
      a3 = 10;
      dir;
    };
    instancer {
      Group => <-.CreateStrUI;
    };
    CreateStrUI CreateStrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
	 width<NEdisplayMode="open"> = 600;
	 height = 700;
      };
    };
    CCP3.K_P.Modules.create_structure create_structure<NEx=407.,NEy=484.> {
      data => <-.data;
      do_load => <-.CreateStrUI.dialog.ok;
    };
  };

  macro LoadUI {
    link selection<NEportLevels={2,1},NEx=297.,NEy=33.>;
    CCP3.K_P.Modules.LoadData &data<NEportLevels={2,0}>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=825.,NEy=33.>;
    link load<NEportLevels={2,1},NEx=462.,NEy=33.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=110.,NEy=132.> {
      width = 300;
      height = 400;
      title = "Load Density";
      ok => <-.load;
      okButton => (strlen(<-.data.filename) > 0);
      cancel = 0;
      cancelButton = 1;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    UIpanel UIpanel<NEx=253.,NEy=198.> {
      parent => <-.UItemplateDialog;
      width => <-.parent.clientWidth;
      height => parent.clientHeight;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=297.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}> => <-.filename;
      parent1<NEportLevels={2,0}> => <-.UIpanel;
      title = "CRYSTAL Property filename";
      x = 0;
      y = 0;
      pattern = "*";
      file_write = 0;
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=715.,NEy=396.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=715.,NEy=396.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.data.filename;
      parent => <-.UIpanel;
      title = "CRYSTAL Property filename";
      x = 0;
      y = 0;
      pattern = "*";
    };
    
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein DLVifieldTypeinN_x<NEx=216.,NEy=441.> {
      UIparent => <-.UIpanel;
      fval => <-.data.N_x;
      label{
	label => "N_x";
      };
      y = 50;
      width = 100;
      
   };
   CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein DLVifieldTypeinN_y<NEx=216.,NEy=477.> {
      UIparent => <-.UIpanel;
      fval => <-.data.N_y;
      label{
	label => "N_y";
      };
      x = 100;
      y = 50;
      width = 100;
   };
   CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein DLVifieldTypeinN_z<NEx=216.,NEy=513.> {
      UIparent => <-.UIpanel;
      fval => <-.data.N_z;
      label{
	label => "N_z";
      };
      x = 200;
      y = 50;
      width = 100;
   };
   CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein DLVffieldTypeinL_x<NEx=423.,NEy=441.> {
      UIparent => <-.UIpanel;
      fval => <-.data.L_x;
      label{
	label => "L_x";
      };
      y = 100;
      width = 100;
   };
   CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein DLVffieldTypeinL_y<NEx=423.,NEy=477.> {
      UIparent => <-.UIpanel;
      fval => <-.data.L_y;
      label{
	label => "L_y";
      };
      x = 100;
      y = 100;
      width = 100;
   };
   CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein DLVffieldTypeinL_z<NEx=423.,NEy=513.> {
      UIparent => <-.UIpanel;
      fval => <-.data.L_z;
      label{
	label => "L_z";
      };
      x = 200;
      y = 100;
      width = 100;
   };
   
		
  };
  calcBase Load_File {
    UIcmd {
      label = "Load Density";
      active => (<-.active_menus && model_type >= 8);
    };
    CCP3.K_P.Modules.LoadData data<NEx=462.,NEy=506.,
				   NEportLevels={0,1}> {
      filename = ""; 
      N_x = 1;
      N_y = 1;
      N_z = 1;
      L_x = 1;
      L_y = 1;
      L_z = 1;
    };
  
    int do_load<NEportLevels=1,NEx=330.,NEy=187.> = 0;
    link model_type<NEportLevels=1,NEx=814.,NEy=55.>;

    
     
    LoadUI LoadUI<NEx=495.,NEy=374.,instanced=0> {
      data => <-.data;
      prefs => <-.prefs;
      load => <-.do_load;
      UItemplateDialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
    };
    instancer {
      Group => <-.LoadUI;
    };
    CCP3.K_P.Modules.load_data load_data<NEx=462.,NEy=506.> {
      data => <-.data;
      do_load => <-.do_load;
    };
  };
  
  calcBase Axis<NEx=261.,NEy=234.> {
    int int<NEportLevels=1> => UIcmd.do;
    UIcmd {
      label = "axis";
    };
    CCP3.K_P.Modules.axis axis<NEx=462.,NEy=506.> {
      do_load => <-.UIcmd.do;
    };
  };
     
     
  CCP3.Core_Macros.CalcObjs.calculation K_P {
    UIcmdList {
      label = "k.p";
      order = 25;
      cmdList => {
	Create_Structure.UIcmd,
	Load_Structure.UIcmd,
	Load_File.UIcmd,
	Axis.UIcmd
      };
    };
    dialog_visible => ((Load_Structure.visible == 1) &&
		       (Create_Structure.visible == 1) &&
		       (Load_File.visible == 1)&&
		       (Axis.visible == 1));
    Load_Structure Load_Structure<NEx=110.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
    CCP3.K_P.Macros.Load_File Load_File<NEx=352.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      model_type => <-.model_type;
    };
    CCP3.K_P.Macros.Create_Structure Create_Structure<NEx=567.,NEy=369.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
     CCP3.K_P.Macros.Axis Axis<NEx=567.,NEy=369.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
  };
};
