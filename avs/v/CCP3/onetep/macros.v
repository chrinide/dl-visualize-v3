
flibrary Macros<NEeditable=1,
#ifndef DLV_NO_DLLS
		dyn_libs="libCCP3onetep",
#endif // DLV_NO_DLLS
		export_cxx=1,
		build_dir="avs/src/express",
		cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/onetep/str_gen.hxx avs/src/express/model.hxx avs/src/onetep/save_gen.hxx avs/src/core/env_gen.hxx avs/src/onetep/load_gen.hxx avs/src/onetep/scf_gen.hxx",
		out_hdr_file="onetep.hxx",
		out_src_file="onetep.cxx"> {
  macro calcBase {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=198.,NEy=66.>;
    link active_menus<NEportLevels=1,NEx=385.,NEy=66.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=143.> {
      active => <-.active_menus;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
    };
  };
  // Bascially a copy of core/UI/file_ops LoadUI
  macro loadstrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.ONETEP.Modules.StructureData &data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Load Input File";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel UIlabel<NEx=660.,NEy=165.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      alignment = "center";
      width => parent.clientWidth;
      label = "Model name";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=671.,NEy=242.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y => (<-.UIlabel.y + <-.UIlabel.height + 5);
      width => (parent.clientWidth - 10);
      text => <-.data.name;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => <-.data.new_view;
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*.dat";
      parent => <-.panel;
      title = "Load ONETEP input filename";
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
    };
    GMOD.parse_v reset_params<NEx=583.,NEy=77.> {
      v_commands = "data.new_view = 0; data.name = \"\";";
      relative => <-;
    };
  };
  calcBase Load_Structure {
    UIcmd {
      label = "Load Structure";
      do => <-.visible;
    };
    CCP3.ONETEP.Modules.StructureData data<NEx=627.,NEy=143.,
      NEportLevels={0,1}> {
      name = "";
      file = "";
      new_view = 0;
    };
    instancer {
      Group => <-.loadstrUI;
    };
    loadstrUI loadstrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.ONETEP.Modules.load_structure load_structure<NEx=407.,NEy=484.> {
      data => <-.data;
      do_load => <-.loadstrUI.dialog.ok;
    };
  };
  macro savestrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Save ONETEP Cell file";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent1<NEportLevels={2,0}> => <-.panel;
      title = "Save ONETEP Cell file name";
      file_write = 1;
      x = 0;
      y = 0;
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent => <-.panel;
      title = "Save ONETEP Cell file name";
      file_write = 1;
      x = 0;
      y = 0;
    };
  };
  calcBase Save_Structure {
    UIcmd {
      label = "Save Structure";
      do => <-.visible;
    };
    instancer {
      Group => <-.savestrUI;
    };
    savestrUI savestrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      FileB {
	filename => <-.<-.data;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    string data<NEx=627.,NEy=143.> = "";
    CCP3.ONETEP.Modules.save_cell_file save_cell_file<NEx=407.,NEy=484.> {
      filename => <-.data;
      do_save => <-.savestrUI.dialog.ok;
    };
  };
  macro prefsUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=748.,NEy=66.>;
    link visible<NEportLevels={2,1},NEx=154.,NEy=66.>;
    link location<NEportLevels={2,1},NEx=748.,NEy=143.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=374.,NEy=154.> {
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "ONETEP Preferences";
      UIshell {
	visible => <-.<-.visible;
      };
    };
    UIlabel label<NEx=385.,NEy=308.> {
      y => ((UIradioBoxLabel.y + UIradioBoxLabel.height) + 5);
      width => parent.clientWidth;
      parent => <-.DLVshell.UIpanel;
      label => "Location of ONETEP binary";
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DirectoryBrowser DBrowser<NEx=726.,NEy=363.> {
      preferences => <-.prefs;
      y => ((label.y + label.height) + 5);
      location => <-.location;
      title = "ONETEP Binary location";
      UIpanel {
	parent => <-.<-.DLVshell.UIpanel;
      };
    };
  };
  macro Prefs {
    link location<NEx=693.,NEy=143.,NEportLevels={0,1}>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=693.,NEy=44.>;
    link UIparent<NEportLevels=1,NEx=187.,NEy=77.>;
    UIoption UIoption<NEx=440.,NEy=33.> {
      label = "Preferences";
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer {
      Value => <-.UIoption.set;
      Group => <-.prefsUI;
    };
    prefsUI prefsUI<NEx=396.,NEy=330.,instanced=0> {
      prefs => <-.prefs;
      visible => <-.UIoption.set;
      location => <-.location;
      DLVshell {
	UIshell {
	  parent => <-.<-.<-.UIparent;
	};
      };
    };
  };
  macro LoadUI {
    link selection<NEportLevels={2,1},NEx=297.,NEy=33.>;
    link filename<NEportLevels={2,1},NEx=605.,NEy=33.>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=825.,NEy=33.>;
    link load<NEportLevels={2,1},NEx=462.,NEy=33.>;
    link model_type;
    string file_type_list1<NEportLevels=1>[] = {
      "3D Cube file"
    };
    string file_type_list2<NEportLevels=1>[] = {
      "3D Cube file",
      "Density of States"
    };
    string file_type_list<NEportLevels=1,NEx=451.,NEy=132.>[] =>
      switch ((<-.model_type == 3)+1,file_type_list1, file_type_list2);
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=110.,NEy=132.> {
      width = 300;
      height = 300;
      title = "Load ONETEP Property";
      ok => <-.load;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
#ifdef MSDOS
      y = 100;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=253.,NEy=198.> {
      parent => <-.UItemplateDialog;
      width => <-.parent.clientWidth;
      height => parent.clientHeight;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox DLVradioBox<NEx=297.,NEy=341.> {
      parent => <-.UIpanel;
      title = "Property File type";
      labels => <-.file_type_list;
      selectedItem => <-.selection;
      width => parent.clientWidth;
      y = 0;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
    };
    string file_patterns[] = {
      "*.cube", "*.txt"
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=715.,NEy=396.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.filename;
      parent => <-.UIpanel;
      title = "ONETEP Property filename";
      x = 0;
      y => ((<-.DLVradioBox.y + <-.DLVradioBox.height) + 5);
      pattern => <-.file_patterns[<-.selection];
    };
    link new_model<NEportLevels=1,NEx=666.,NEy=135.>;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=459.,NEy=432.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      visible => (<-.model_type == 3);
      width => parent.clientWidth;
      label = "Load cube model";
      set => <-.new_model;
    };
  };
  calcBase Load_File {
    UIcmd {
      label = "Load Property";
      active => <-.active_menus;
      do => <-.visible;
    };
    int file_type<NEportLevels=1,NEx=550.,NEy=187.> = 0;
    string filename<NEportLevels=1,NEx=781.,NEy=187.> = "";
    int do_load<NEportLevels=1,NEx=330.,NEy=187.> = 0;
    link model_type<NEportLevels=1,NEx=814.,NEy=55.>;
    int new_model<NEportLevels=1,NEx=720.,NEy=126.> = 0;
    CCP3.ONETEP.Macros.LoadUI LoadUI<NEx=495.,NEy=374.,instanced=0> {
      selection => <-.file_type;
      filename => <-.filename;
      prefs => <-.prefs;
      load => <-.do_load;
      model_type => <-.model_type;
      new_model => <-.new_model;
      UItemplateDialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
    };
    instancer {
      Group => <-.LoadUI;
    };
    CCP3.ONETEP.Modules.load_data load_data<NEx=462.,NEy=506.> {
      file_type => <-.file_type;
      filename => <-.filename;
      load_model => <-.new_model;
      do_load => <-.do_load;
    };
  };
  macro baseUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels={2,1},NEx=495.,NEy=22.>;
    UIpanel panel<NEx=253.,NEy=187.> {
      parent => <-.UIparent;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
  };
  baseUI BasisUI {
    string basis_env<NEportLevels=1,NEx=473.,NEy=187.> =>
      getenv("DLV_ONETEPBAS");
    string default_dir<NEportLevels=1,NEx=660.,NEy=187.> =>
      (getenv("DLV_ROOT") + "/data/ONETEP");
    string basis_dir<NEportLevels=1,NEx=561.,NEy=253.> =>
      switch((is_valid(basis_env) + 1),default_dir,basis_env);
    CCP3.ONETEP.Modules.basis_data data {
      ngwfs = 1;
      radius = 6.0;
      filename = "";
    };
    GMOD.copy_on_change copy_on_change<NEx=561.,NEy=319.> {
      input => <-.basis_dir;
    };
    string filter => copy_on_change.output;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein ngwfs<NEx=810.,NEy=261.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "number of NGWFs";
      y = 5;
      fmin = 1;
      fval => <-.data.ngwfs;
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein radius<NEx=819.,NEy=342.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Radius";
      y => (<-.ngwfs.y + <-.ngwfs.height + 5);
      fmin = 0.1;
      fval => <-.data.radius;
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.FilePanel FileB<NEx=407.,NEy=429.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.data.filename;
      pattern => <-.filter + "/*";
      parent => <-.panel;
      title = "ONETEP Pseudopotentials";
      x = 0;
      y => (<-.radius.y + <-.radius.height + 5);
    };
    int set_default<NEportLevels=1,NEx=671.,NEy=429.> = 0;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=407.,NEy=506.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Default pseudopotential for atom type";
      y => (<-.FileB.y + <-.FileB.height + 5);
      set => <-.set_default;
      visible = 0; // Todo
    };
    int accept_pseudo = 0;
    UIbutton UIbutton<NEx=639.,NEy=504.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (parent.clientWidth - width) / 2;
      label = "Accept";
      y => (<-.DLVtoggle.y + <-.DLVtoggle.height + 15);
      do => <-.accept_pseudo;
    };
  };
  baseUI TolUI {
    CCP3.ONETEP.Modules.TolParams &params<NEx=477.,NEy=108.,NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein cutoff<NEx=468.,NEy=261.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Cutoff Energy (ev)";
      y = 5;
      fmin = 50.0;
      fval => <-.params.cutoff;
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein grid_scale<NEx=468.,NEy=342.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Fine Grid scaling factor";
      y => (<-.kernel.y + <-.kernel.height + 5);
      fmin = 0.1;
      fval => <-.params.grid;
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein kernel<NEx=468.,NEy=423.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Density Kernel Cutoff (au)";
      y => (<-.cutoff.y + <-.cutoff.height + 5);
      fmin = 0.1;
      fval => <-.params.kernel;
      width => UIparent.clientWidth;
    };
  };
  baseUI HamUI {
    CCP3.ONETEP.Modules.HamiltonianParams &params<NEx=504.,NEy=108.,
      NEportLevels={2,1}>;
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox calc_type<NEx=468.,NEy=252.> {
      parent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "Spin type";
      y = 5;
      width => parent.clientWidth;
      selectedItem => <-.params.unrestricted;
      labels => {
	"Restricted spin", "Unrestricted spin"
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle set_spin<NEx=468.,NEy=531.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Set total spin";
      y => (<-.calc_type.y + <-.calc_type.height + 5);
      set => <-.params.set_spin;
      visible => (<-.params.unrestricted == 1);
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider spin<NEx=468.,NEy=324.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      title = "Spin value";
      y => (<-.set_spin.y + <-.set_spin.height + 5);
      width => parent.clientWidth;
      visible => (<-.params.set_spin == 1);
      min = -10;
      max = 10;
      value => <-.params.spin_value;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox functional<NEx=468.,NEy=396.> {
      parent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "Functional";
      y => (<-.spin.y + <-.spin.height + 5);
      width => parent.clientWidth;
      selectedItem => <-.params.functional;
      labels => {
	"CAPZ", "VWN", "PW91", "PBE", "PBE (revised 98 -REVPBE)",
	"PBE (revised 99 - RPBE)", "BLYP", "XLYP"
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox dispersion<NEx=468.,NEy=396.> {
      parent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "Dispersion correction";
      y => (<-.functional.y + <-.functional.height + 5);
      width => parent.clientWidth;
      selectedItem => <-.params.dispersion;
      labels => {
	"No correction", "Elstner Damping function",
	"Wu and Yang first damping fuction",
	"Wu and Yang second damping fuction"
      };
    };
  };
  baseUI PropsUI {
   CCP3.ONETEP.Modules.PropParams &params<NEx=486.,NEy=99.,NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein homo_plot<NEx=486.,NEy=207.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Number of HOMO plots";
      y = 5;
      fmin = 0;
      fval => <-.params.homo_plot;
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein lumo_plot<NEx=486.,NEy=270.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Number of LUMO plots";
      y => (<-.homo_plot.y + <-.homo_plot.height + 5);
      fmin = 0;
      fval => <-.params.lumo_plot;
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein eigenvalues<NEx=486.,NEy=333.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Number of Eigenvalues";
      y => (<-.lumo_plot.y + <-.lumo_plot.height + 5);
      fmin = 0;
      fval => <-.params.num_eigs;
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle dos<NEx=486.,NEy=387.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Calculate Density of States";
      y => (<-.eigenvalues.y + <-.eigenvalues.height + 5);
      set => <-.params.do_dos;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein smearing<NEx=702.,NEy=387.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Density of States smearing";
      y => (<-.dos.y + <-.dos.height + 5);
      fmin = 0.00001;
      fval => <-.params.dos_smear;
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.params.do_dos == 1);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle density<NEx=486.,NEy=459.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Calculate Density/Potential";
      y => (<-.smearing.y + <-.smearing.height + 5);
      set => <-.params.density_plot;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle mulliken<NEx=486.,NEy=522.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Calculate Mulliken population";
      y => (<-.density.y + <-.density.height + 5);
      set => <-.params.mulliken_calc;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider neighbours<NEx=711.,NEy=522.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      title = "Mulliken Cutoff Radius";
      y => (<-.mulliken.y + <-.mulliken.height + 5);
      width => parent.clientWidth;
      visible => (<-.params.set_spin == 1);
      min = 0.1;
      max = 10;
      value => <-.params.mulliken_neighbours;
      visible => (<-.params.mulliken_calc == 1);
    };
  };
  baseUI OptUI {
    CCP3.ONETEP.Modules.OptParams &params<NEx=495.,NEy=90.,NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox method<NEx=486.,NEy=207.> {
      parent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "Optimisation Method";
      y = 5;
      width => parent.clientWidth;
      selectedItem => <-.params.method;
      labels => {
	"Cartesian coordinates",
	"Delocalized internal coordinates"
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein iterations<NEx=486.,NEy=270.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Maximum Iterations";
      y => (<-.method.y + <-.method.height + 5);
      fmin = 1;
      fval => <-.params.max_iter;
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein energy<NEx=486.,NEy=333.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Energy Tolerance (H/atom)";
      y => (<-.iterations.y + <-.iterations.height + 5);
      fmin = 0;
      fval => <-.params.energy_tol;
      width => UIparent.clientWidth;
      field {
	decimalPoints = 5;
	format = "mixed";
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein forces<NEx=486.,NEy=387.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Force Tolerance (H/Bohr)";
      y => (<-.energy.y + <-.energy.height + 5);
      fmin = 0;
      fval => <-.params.force_tol;
      width => UIparent.clientWidth;
      field {
	decimalPoints = 5;
	format = "mixed";
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein displacement<NEx=486.,NEy=450.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Displacement Tolerance (au)";
      y => (<-.forces.y + <-.forces.height + 5);
      fmin = 0;
      fval => <-.params.disp_tol;
      width => UIparent.clientWidth;
      field {
	decimalPoints = 5;
	format = "mixed";
      };
    };
  };
  macro scfUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels={2,1},NEx=495.,NEy=22.>;
    CCP3.ONETEP.Modules.SCFParams &SCFParams<NEx=517.,NEy=99.> {
      Tolerances<NEportLevels={0,2}>;
      Hamiltonian<NEportLevels={0,2}>;
      Properties<NEportLevels={0,2}>;
      Optimize<NEportLevels={0,2}>;
      /*
      Basis<NEportLevels={0,2}>;
      KPoints<NEportLevels={0,2}>;
      Convergence<NEportLevels={0,2}>;
      Print<NEportLevels={0,2}>;
      Job<NEportLevels={0,2}>;
      phonon<NEportLevels={0,2}>;
      neb<NEportLevels={0,2}>;
      cphf<NEportLevels={0,2}>;
      tddft<NEportLevels={0,2}>;
      */
    };
    /*
    int model_has_spin<NEx=506.,NEy=55.> = 0;
    int select_spin<NEx=704.,NEy=55.> =>
      switch ((SCFParams.Hamiltonian.Htype + 1),
	      switch ((model_has_spin + 1), 0, 2),model_has_spin);
    */
    int complete_pseudos<NEportLevels=1,NEx=747.,NEy=270.> = 0;
    GMOD.parse_v parse_v<NEx=319.,NEy=77.> {
      trigger => <-.visible;
      on_inst = 1;
      //relative => <-;
    };
    link job_data<NEx=803.,NEy=187.,NEportLevels={2,1}>;
    int select_tab = 0;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=275.,NEy=165.> {
      parent => <-.UIparent;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => <-.visible;
      title = "ONETEP calculation";
      width = 480;
      height = 675;
      ok<NEportLevels={1,2}> = 0;
      okButton => ((<-.complete_pseudos == 1) || (<-.SCFParams.task == 3));
      okLabelString = "Run";
      cancel = 0;
      cancelButton = 1;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=286.,NEy=242.> {
      parent => <-.UItemplateDialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIframe UIframe<NEx=459.,NEy=279.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => <-.Pseudos.height;
      width => parent.clientWidth;
      height => parent.clientHeight - Pseudos.height - 65;
    };
    UIbutton Hamiltonian<NEx=33.,NEy=363.> {
      parent => <-.UIpanel;
      color {
	foregroundColor => switch((select_tab==2)+1, 
				  <-.<-.prefs.colour.foregroundColor,
				  "light grey");
	backgroundColor => switch((select_tab==2)+1, 
				  <-.<-.prefs.colour.backgroundColor,
				  "#707070");
      };
      &fontAttributes => <-.prefs.fontAttributes;
      x => parent.clientWidth / 2;
      y = 0;
      width => parent.clientWidth / 4;
      label = "Hamiltonian";
      do => <-.visible_ham;
      visible => (<-.SCFParams.task != 3);
    };
    UIbutton Pseudos<NEx=33.,NEy=407.> {
      parent => <-.UIpanel;
      color {
	foregroundColor => switch((select_tab==0)+1, 
				  <-.<-.prefs.colour.foregroundColor,
				  "light grey");
	backgroundColor => switch((select_tab==0)+1, 
				  <-.<-.prefs.colour.backgroundColor,
				  "#707070");
      };
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth / 4;
      label = "Pseudopotentials";
      do => <-.visible_basis;
      visible => (<-.SCFParams.task != 3);
    };
    UIbutton Tolerances<NEx=198.,NEy=407.> {
      parent => <-.UIpanel;
      color {
	foregroundColor => switch((select_tab==1)+1, 
				  <-.<-.prefs.colour.foregroundColor,
				  "light grey");
	backgroundColor => switch((select_tab==1)+1, 
				  <-.<-.prefs.colour.backgroundColor,
				  "#707070");
      };
      &fontAttributes => <-.prefs.fontAttributes;
      x => parent.clientWidth / 4;
      y = 0;
      width => parent.clientWidth / 4;
      label = "Cutoffs";
      do => <-.visible_tol;
      visible => (<-.SCFParams.task != 3);
    };
    UIbutton Properties<NEx=374.,NEy=407.> {
      parent => <-.UIpanel;
      color {
	foregroundColor => switch((select_tab==3)+1, 
				  <-.<-.prefs.colour.foregroundColor,
				  "light grey");
	backgroundColor => switch((select_tab==3)+1, 
				  <-.<-.prefs.colour.backgroundColor,
				  "#707070");
      };
      &fontAttributes => <-.prefs.fontAttributes;
      x => 3 * parent.clientWidth / 4;
      y = 0;
      width => parent.clientWidth / 4;
      do => <-.visible_props;
      visible => (<-.SCFParams.task == 0 || <-.SCFParams.task == 3);
    };
    UIbutton Optimise<NEx=539.,NEy=363.> {
      parent => <-.UIpanel;
      color {
	foregroundColor => switch((select_tab==4)+1, 
				  <-.<-.prefs.colour.foregroundColor,
				  "light grey");
	backgroundColor => switch((select_tab==4)+1, 
				  <-.<-.prefs.colour.backgroundColor,
				  "#707070");
      };
      &fontAttributes => <-.prefs.fontAttributes;
      x => 3 * parent.clientWidth / 4;
      y = 0;
      width => parent.clientWidth / 4;
      visible => ((version > 0) && (SCFParams.task == 1));
      width = 150;
      do => <-.visible_opt;
      visible => (<-.SCFParams.task == 1);
    };
    /*
    UIbutton SCF<NEx=539.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "SCF Convergence";
      y => Tolerances.y;
      x => (parent.clientWidth / 2);
      width = 150;
      do => <-.visible_conv;
    };
    UIbutton Print<NEx=374.,NEy=363.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Print Options";
      x => (parent.clientWidth / 2);
      y => K_Points.y;
      width = 150;
      do => <-.visible_print;
    };
    UIbutton Job<NEx=198.,NEy=363.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((K_Points.y + K_Points.height) + 5);
      width = 150;
      do => <-.visible_job;
    };
    UIbutton Phonon<NEx=693.,NEy=363.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => Optimise.x;
      y => Optimise.y;
      visible => ((version > 1) && (SCFParams.task == 2));
      width = 150;
      do => <-.visible_phon;
    };
    UIbutton NEB<NEx=693.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => Optimise.x;
      y => Optimise.y;
      visible => ((version > 2) && (SCFParams.task == 3));
      width = 150;
      do => <-.visible_neb;
    };
    UIbutton CPHF {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => Optimise.x;
      y => Optimise.y;
      visible => ((version > 2) && (SCFParams.task == 4));
      width = 150;
      do => <-.visible_cphf;
    };
    UIbutton TDDFT {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => Optimise.x;
      y => Optimise.y;
      visible => ((version > 3) && (SCFParams.task == 5));
      width = 150;
      do => <-.visible_tddft;
    };
    */
    GMOD.instancer instance_basis<NEx=869.,NEy=55.> {
      Value => (<-.visible_basis && <-.visible);
      Group => BasisUI;
    };
    GMOD.parse_v select_pseudo {
      v_commands = "select_tab = 0;";
      trigger => <-.visible_basis;
      on_inst = 0;
      relative => <-;				    
    };
    GMOD.instancer instance_tol<NEx=869.,NEy=165.> {
      Value => (<-.visible_tol && <-.visible);
      Group => TolerancesUI;
    };
    GMOD.parse_v select_tol {
      v_commands = "select_tab = 1;";
      trigger => <-.visible_tol;
      on_inst = 0;
      relative => <-;				    
    };
    GMOD.instancer instance_ham<NEx=869.,NEy=110.> {
      Value => (<-.visible_ham && <-.visible);
      Group => <-.HamiltonianUI;
    };
    GMOD.parse_v select_ham {
      v_commands = "select_tab = 2;";
      trigger => <-.visible_ham;
      on_inst = 0;
      relative => <-;				    
    };
    GMOD.instancer instance_props<NEx=869.,NEy=220.> {
      Value => (<-.visible_props && <-.visible);
      Group => PropsUI;
    };
    GMOD.parse_v select_props {
      v_commands = "select_tab = 3;";
      trigger => <-.visible_props;
      on_inst = 0;
      relative => <-;				    
    };
    GMOD.instancer instance_opt<NEx=869.,NEy=440.> {
      Value => (<-.visible_opt && <-.visible);
      Group => OptimizeUI;
    };
    GMOD.parse_v select_opt {
      v_commands = "select_tab = 4;";
      trigger => <-.visible_opt;
      on_inst = 0;
      relative => <-;				    
    };
    /*
    GMOD.instancer instance_conv<NEx=869.,NEy=275.> {
      Value => (<-.visible_conv && <-.visible);
      Group => ConvergenceUI;
    };
    GMOD.instancer instance_print<NEx=869.,NEy=330.> {
      Value => (<-.visible_print && <-.visible);
      Group => PrintUI;
    };
    GMOD.instancer instance_job<NEx=869.,NEy=385.> {
      Value => (<-.visible_job && <-.visible);
      Group => JobUI;
    };
    GMOD.instancer instance_phon<NEx=869.,NEy=484.> {
      Value => (<-.visible_phon && <-.visible);
      Group => PhononUI;
    };
    GMOD.instancer instance_neb<NEx=869.,NEy=539.> {
      Value => (<-.visible_neb && <-.visible);
      Group => NebUI;
    };
    GMOD.instancer instance_cphf {
      Value => (<-.visible_cphf && <-.visible);
      Group => cphfUI;
    };
    GMOD.instancer instance_tddft {
      Value => (<-.visible_tddft && <-.visible);
      Group => tddftUI;
    };
    */
    int visible_basis<NEportLevels=1,NEx=33.,NEy=572.> = 1;
    int visible_tol<NEportLevels=1,NEx=374.,NEy=572.> = 0;
    int visible_ham<NEportLevels=1,NEx=198.,NEy=572.> = 0;
    int visible_props<NEportLevels=1,NEx=567.,NEy=567.> = 0;
    int visible_opt<NEportLevels=1,NEx=374.,NEy=627.> = 0;
    /*
    int visible_k<NEportLevels=1,NEx=539.,NEy=572.> = 0;
    int visible_conv<NEportLevels=1,NEx=704.,NEy=572.> = 0;
    int visible_print<NEportLevels=1,NEx=33.,NEy=627.> = 0;
    int visible_job<NEportLevels=1,NEx=198.,NEy=627.> = 0;
    int visible_phon<NEportLevels=1,NEx=539.,NEy=627.> = 0;
    int visible_neb<NEportLevels=1,NEx=704.,NEy=627.> = 0;
    int visible_cphf<NEportLevels=1> = 0;
    int visible_tddft<NEportLevels=1> = 0;
    */
    BasisUI BasisUI<NEx=55.,NEy=451.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIframe;
      visible => (<-.visible_basis == 1);
      panel {
	x = 0;
	y = 0;
	width => parent.clientWidth;
	height => parent.clientHeight;
	visible => (<-.<-.select_tab == 0);
      };
    };
    TolUI TolerancesUI<NEx=407.,NEy=451., instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIframe;
      visible => (<-.visible_tol == 1);
      params => <-.SCFParams.Tolerances;
      panel {
	x = 0;
	y = 0;
	width => parent.clientWidth;
	height => parent.clientHeight;
	visible => (<-.<-.select_tab == 1);
      };
    };
    HamUI HamiltonianUI<NEx=231.,NEy=451.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIframe;
      visible => (<-.visible_ham == 1);
      params => <-.SCFParams.Hamiltonian;
      panel {
	x = 0;
	y = 0;
	width => parent.clientWidth;
	height => parent.clientHeight;
	visible => (<-.<-.select_tab == 2);
      };
    };
    CCP3.ONETEP.Macros.PropsUI PropsUI<NEx=603.,NEy=450.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIframe;
      visible => (<-.visible_props == 1);
      params => <-.SCFParams.Properties;
      panel {
	x = 0;
	y = 0;
	width => parent.clientWidth;
	height => parent.clientHeight;
	visible => (<-.<-.select_tab == 3);
      };
    };
    CCP3.ONETEP.Macros.OptUI OptimizeUI<NEx=612.,NEy=495.> {
      prefs => <-.prefs;
      UIparent => <-.UIframe;
      visible => (<-.visible_opt == 1);
      params => <-.SCFParams.Optimize;
      panel {
	x = 0;
	y = 0;
	width => parent.clientWidth;
	height => parent.clientHeight;
	visible => (<-.<-.select_tab == 4);
      };
    };
    /*
    CCP3.ONETEP.SCF.K_PointsUI K_PointsUI<NEx=583.,NEy=451.,
                                                  instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_k == 1) &&
		  (<-.<-.<-.model_type > 0));
      Params => <-.SCFParams.KPoints;
      is1 {
	min => <-.<-.limits.min_shrink;
	max => <-.<-.limits.max_shrink;
      };
      is2 {
	min => <-.<-.limits.min_shrink;
	max => <-.<-.limits.max_shrink;
      };
      is3 {
	min => <-.<-.limits.min_shrink;
	max => <-.<-.limits.max_shrink;
      };
      isp {
	min => <-.<-.limits.min_gilat;
	max => <-.<-.limits.max_gilat;
      };
      CCP3shell {
	parse_v {
	  v_commands = "visible_k = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.ONETEP.SCF.ConvergenceUI ConvergenceUI<NEx=55.,NEy=506.,
                                                        instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_conv == 1));
      show_spin => (<-.SCFParams.Hamiltonian.Hoptions > 0);
      Params => <-.SCFParams.Convergence;
      version => <-.version;
      cycles {
	fmin => <-.<-.limits.min_cycles;
	fmax => <-.<-.limits.max_cycles;
      };
      shift_value {
	fmax => <-.<-.limits.max_shift;
      };
      CCP3shell {
	parse_v {
	  v_commands = "visible_conv = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.ONETEP.SCF.PrintUI PrintUI<NEx=407.,NEy=506.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_print == 1));
      Params => <-.SCFParams.Print;
      exchange {
	visible => ((<-.<-.SCFParams.Hamiltonian.Htype == 0) && (<-.<-.SCFParams.Hamiltonian.Hoptions > 0));
      };
      CCP3shell {
	parse_v {
	  v_commands = "visible_print = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.ONETEP.SCF.JobUI JobUI<NEx=583.,NEy=506.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_job == 1));
      Params => <-.SCFParams.Job;
      version => <-.version;
      CCP3shell {
	parse_v {
	  v_commands = "visible_job = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.ONETEP.SCF.PhononUI PhononUI<NEx=720.,NEy=506.,
                                              instanced=0> {
      model_type => <-.<-.<-.model_type;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_phon == 1));
      params => <-.SCFParams.phonon;
      version => <-.version;
      CCP3shell {
	parse_v {
	  v_commands = "visible_phon = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.ONETEP.SCF.NebUI NebUI<instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_neb == 1));
      params => <-.SCFParams.neb;
      scfparams => <-.SCFParams;
      job_data => <-.job_data;
      CCP3shell {
	parse_v {
	  v_commands = "visible_neb = 0; NebEndStrUI.DLVdialog.ok = 0;";
	  relative => <-.<-.<-;
	};
	CCP3.ONETEP.Modules.nebwrite91 nebwrite91<NEx=639.,NEy=191.>{
	    trigger => <-.close.do;
	    make91 => <-.<-.params.make91;
	    data => <-.<-.scfparams;
	    job_data => <-.<-.job_data;
	};
      };
    };
    CCP3.ONETEP.SCF.cphfUI cphfUI<instanced=0> {
      //model_type => <-.<-.<-.model_type;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_cphf == 1));
      Params => <-.SCFParams.cphf;
      //version => <-.version;
      CCP3shell {
	parse_v {
	  v_commands = "visible_cphf = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.ONETEP.SCF.tddftUI tddftUI<instanced=0> {
      //model_type => <-.<-.<-.model_type;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_tddft == 1));
      Params => <-.SCFParams.tddft;
      //version => <-.version;
      CCP3shell {
	parse_v {
	  v_commands = "visible_tddft = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    */
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle user_job {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "I want to run this job without DLV";
      y => UIpanel.clientHeight - 60;
      set => <-.SCFParams.user_calc;
    };
    CCP3.Core_Macros.UI.UIobjs.DirSave FileB {
      preferences => <-.prefs;
      filename => <-.SCFParams.user_dir;
      pattern = "*";
      title = "Run SCF Job in Dir";
      parent => <-.UIpanel;
      y => ((user_job.y + user_job.height) + 5);
      UIpanel {
	visible => <-.<-.SCFParams.user_calc;
      };
    };
  };
  calcBase SCF_calc {
    UIcmd<NEx=11.,NEy=209.> {
      label = "Single Point Energy";
      active => (<-.active_menus && (<-.model_type == 3));
      do = 0;
    };
    UIcmdList SubMenu<NEx=198.,NEy=143.> {
      label = "Calculation";
      cmdList => { UIcmd, UIopt, UIprop };
    };
    UIcmd UIopt<NEx=176.,NEy=209.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Structure Optimisation";
      active => (<-.active_menus && (<-.model_type == 3));
      do = 0;
    };
    /*
    UIcmd UImd<NEx=341.,NEy=209.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Molecular Dynamics";
      //active => (<-.active_menus && (<-.model_type == 3));
      active = 0;
      do = 0;
    };
    */
    UIcmd UIprop<NEx=506.,NEy=209.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Properties";
      active => (<-.active_wvfn && (<-.model_type == 3));
      do = 0;
    };
    /*
    UIcmd UItstate<NEx=693.,NEy=225.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "CPHF/KS";
      active => (<-.active_menus && (<-.model_type == 3));
      do = 0;
    };
    */
    GMOD.parse_v do_energy<NEx=11.,NEy=330.> {
      v_commands = "SCFParams.task = 0; visible = 1; scfUI.visible_basis = 1;";
      trigger => <-.UIcmd.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v do_opt<NEx=165.,NEy=330.> {
      v_commands = "SCFParams.task = 1; visible = 1; scfUI.visible_basis = 1;";
      trigger => <-.UIopt.do;
      on_inst = 0;
      relative => <-;
    };
    /*
    GMOD.parse_v do_md<NEx=341.,NEy=330.> {
      v_commands = "SCFParams.task = 2; visible = 1; scfUI.visible_basis = 1;";
      trigger => <-.UImd.do;
      on_inst = 0;
      relative => <-;
    };
    */
    GMOD.parse_v do_prop<NEx=506.,NEy=330.> {
      v_commands = "SCFParams.task = 3; visible = 1; scfUI.visible_props = 1;";
      trigger => <-.UIprop.do;
      on_inst = 0;
      relative => <-;
    };
    /*
    GMOD.parse_v do_tstate<NEx=684.,NEy=333.> {
      v_commands = "SCFParams.task = 4; visible = 1;";
      trigger => <-.UItstate.do;
      on_inst = 0;
      relative => <-;
    };
    */
    link model_type<NEportLevels={2,1},NEx=814.,NEy=55.>;
    link job_data<NEx=803.,NEy=187.,NEportLevels={2,1}>;
    int wavefn_set;
    int active_wvfn<NEportLevels=1,NEx=440.,NEy=110.> =>
      (active_menus && is_valid(wavefn_set) && (wavefn_set == 1));
    scfUI scfUI<NEx=495.,NEy=374.,instanced=0> {
      visible => <-.visible;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      SCFParams => <-.SCFParams;
      job_data => <-.job_data;
      complete_pseudos => <-.Run_SCF.all_pseudos;
      parse_v {
	v_commands = "Run_SCF.all_pseudos = 0; scfUI.select_tab = 0;";
	relative => <-.<-;
      };
    };
    instancer {
      Group => scfUI;
      active = 2;
    };
    CCP3.ONETEP.Modules.SCFParams SCFParams<NEportLevels={0,1}> {
      task = 0;
      Tolerances {
	cutoff = 550.0;
	grid = 2.0;
	kernel = 1000.0;
      };
      Hamiltonian {
	spin_value = 0;
	set_spin = 0;
	unrestricted = 0;
	dispersion = 0;
	functional = 0;
      };
      Properties {
	homo_plot = 0;
	lumo_plot = 0;
	num_eigs = 0;
	do_dos = 0;
	dos_smear = 0.1;
	density_plot = 0;
	mulliken_calc = 0;
	mulliken_neighbours = 3.0;
      };
      Optimize {
	method = 0;
	max_iter = 10;
	energy_tol = 1e-6;
	force_tol = 0.02;
	disp_tol = 0.001;
      };
      user_calc = 0;
      user_dir = "";
    };
    CCP3.ONETEP.Modules.Run_SCF Run_SCF {
      make => <-.visible;
      run => <-.scfUI.UItemplateDialog.ok;
      cancel => <-.scfUI.UItemplateDialog.cancel;
      inherit = 0;
      data => <-.SCFParams;
      bdata => <-.scfUI.BasisUI.data;
      use_default => <-.scfUI.BasisUI.set_default;
      //all_pseudos => <-.scfUI.complete_pseudos;
      do_pseudo => <-.scfUI.BasisUI.accept_pseudo;
      job_data => <-.job_data;
    };
  };
  CCP3.Core_Macros.CalcObjs.calculation ONETEP {
    UIcmdList {
      label = "ONETEP";
      order = 16;
      cmdList => {
	Load_Structure.UIcmd,
	SCF_calc.SubMenu,
	Load_File.UIcmd,
	Save_Structure.UIcmd,
	Prefs.UIoption
      };
    };
    dialog_visible => ((Load_Structure.visible == 1) ||
		       (SCF_calc.visible == 1) ||
		       (Load_File.visible == 1) ||
		       (Save_Structure.visible == 1));
    string exec_location<NEx=308.,NEy=187.,NEportLevels={1,1}> = "";
    CCP3.Core_Modules.Calcs.exec_environ exec_environ<NEx=308.,NEy=264.> {
      location => <-.exec_location;
      default_var = "DLV_DEF_ONETEP";
      search_path = 0;
      main_var = "DLV_ONETEP";
      binary = "onetep";
    };
    Load_Structure Load_Structure<NEx=110.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
    CCP3.ONETEP.Macros.Save_Structure Save_Structure<NEx=319.,NEy=374.> {
      active_menus => (<-.active_menus && (<-.model_type == 3));
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
    CCP3.ONETEP.Macros.Load_File Load_File<NEx=531.,NEy=369.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      model_type => <-.model_type;
    };
    CCP3.ONETEP.Macros.SCF_calc SCF_calc<NEx=729.,NEy=369.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      model_type => <-.model_type;
      job_data => <-.job_data;
    };
    Prefs Prefs<NEx=836.,NEy=539.> {
      prefs => <-.preferences;
      location => <-.exec_location;
    };
  };
};
