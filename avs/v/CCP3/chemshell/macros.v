
flibrary Macros<NEeditable=1,
#ifndef DLV_NO_DLLS
		dyn_libs="libCCP3chemshell",
#endif // DLV_NO_DLLS
		export_cxx=1,
		build_dir="avs/src/express",
		cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/chemshell/str_gen.hxx avs/src/express/model.hxx avs/src/chemshell/save_gen.hxx",
		out_hdr_file="chemshell.hxx",
		out_src_file="chemshell.cxx"> {
  macro calcBase {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=198.,NEy=66.>;
    link active_menus<NEportLevels=1,NEx=385.,NEy=66.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=143.> {
      active => <-.active_menus;
      do => <-.visible;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
    };
  };
  // Bascially a copy of core/UI/file_ops LoadUI
  macro loadstrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.CHEMSHELL.Modules.StructureData &data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Load Cell File";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel UIlabel<NEx=660.,NEy=165.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      alignment = "center";
      width => parent.clientWidth;
      label = "Model name";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=671.,NEy=242.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y => (<-.UIlabel.y + <-.UIlabel.height + 5);
      width => (parent.clientWidth - 10);
      text => <-.data.name;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => <-.data.new_view;
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*.pun";
      parent => <-.panel;
      title = "Load CHEMSHELL cell filename";
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
    };
    GMOD.parse_v reset_params<NEx=583.,NEy=77.> {
      v_commands = "data.new_view = 0; data.name = \"\";";
      relative => <-;
    };
  };
  calcBase Load_Structure {
    UIcmd {
      label = "Load Structure";
    };
    CCP3.CHEMSHELL.Modules.StructureData data<NEx=627.,NEy=143.,
      NEportLevels={0,1}> {
      name = "";
      file = "";
      new_view = 0;
    };
    instancer {
      Group => <-.loadstrUI;
    };
    loadstrUI loadstrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
    };
    CCP3.CHEMSHELL.Modules.load_structure load_structure<NEx=407.,NEy=484.> {
      data => <-.data;
      do_load => <-.loadstrUI.dialog.ok;
    };
  };
  macro savestrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Save Chemshell region file";
      ok = 0;
      okButton => is_valid(<-.DLVlist.selectedItem);
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist DLVlist<NEx=522.,NEy=315.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent => <-.panel;
      title = "Save Chemshell file name";
      file_write = 1;
      x = 0;
      y => (<-.DLVlist.y + <-.DLVlist.height + 5);
    };
  };
  calcBase Save_Structure {
    UIcmd {
      label = "Save Structure";
    };
    instancer {
      Group => <-.savestrUI;
    };
    string group_name;
    mlink groups<NEportLevels=1,NEx=819.,NEy=216.>;
    savestrUI savestrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      DLVlist {
	selectedText => <-.<-.group_name;
	strings => <-.<-.groups;
      };
      FileB {
	filename => <-.<-.data;
      };
    };
    string data<NEx=627.,NEy=143.> = "";
    boolean fractions<NEportLevels=1,NEx=605.,NEy=55.> = 0;
    CCP3.CHEMSHELL.Modules.save_pun_file save_pun_file<NEx=407.,NEy=484.> {
      filename => <-.data;
      label => <-.group_name;
      do_save => <-.savestrUI.dialog.ok;
    };
  };
  CCP3.Core_Macros.CalcObjs.calculation ChemShell {
    UIcmdList {
      label = "ChemShell";
      order = 31;
      cmdList => {
	Load_Structure.UIcmd,
	Save_Structure.UIcmd
      };
    };
    dialog_visible => ((Load_Structure.visible == 1) &&
		       (Save_Structure.visible == 1));
    mlink atom_groups<NEportLevels=1,NEx=513.,NEy=180.>;
    Load_Structure Load_Structure<NEx=110.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
    CCP3.CHEMSHELL.Macros.Save_Structure Save_Structure<NEx=319.,NEy=374.> {
      active_menus => (<-.active_menus && (<-.model_type == 0));
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      groups => <-.atom_groups;
    };
  };
};
