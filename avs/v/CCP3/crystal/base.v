
flibrary Base<NEeditable=1,
#ifndef DLV_NO_DLLS
	      dyn_libs="libCCP3crystal",
#endif // DLV_NO_DLLS
	      export_cxx=1,
	      build_dir="avs/src/express",
	      cxx_hdr_files="avs/src/express/calcs.hxx",
	      out_hdr_file="crystal_base.hxx",
	      out_src_file="crystal_base.cxx"> {
  macro simple_baseUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
  };
  simple_baseUI baseUI {
    link visible<NEportLevels={2,1},NEx=495.,NEy=22.>;
  };
  macro calcUIBase {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=198.,NEy=66.>;
    link active_menus<NEportLevels=1,NEx=385.,NEy=66.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=143.> {
      active => <-.active_menus;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
    };
  };
  calcUIBase calcBase {
    UIcmd {
      do => <-.visible;
    };
  };
};
