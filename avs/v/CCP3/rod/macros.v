
flibrary Macros<NEeditable=1,
#ifndef DLV_NO_DLLS
		dyn_libs="libCCP3rod",
#endif // DLV_NO_DLLS
		export_cxx=1,
		build_dir="avs/src/express",
		cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/rod/rod_fitdata_gen.hxx avs/src/rod/rod_load_gen.hxx  avs/src/express/rod_read.hxx avs/src/express/rod_create.hxx avs/src/express/rod_write.hxx avs/src/express/rod_calc.hxx avs/src/express/rod_params.hxx avs/src/express/rod_model.hxx avs/src/express/rod_view.hxx avs/src/core/model/ptbl_gen.hxx avs/src/core/model/make_gen.hxx avs/src/core/model/move_gen.hxx avs/src/core/model/can_gen.hxx avs/src/express/crystal_scf.hxx",
		out_hdr_file="rod.hxx",
		out_src_file="rod.cxx"> {
  macro calcBase {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=198.,NEy=66.>;
    //    CCP3.Core_Modules.Model.move_params move_params<NEportLevels=1,NEx=198.,NEy=166.>;
    link active_menus<NEportLevels=1,NEx=385.,NEy=66.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=143.> {
      active => <-.active_menus;
      do => <-.visible;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
    };
  };
  calcBase Load_Rod_Files {
    link params<NEportLevels={2,1}>; 
    UIcmd {
      label = "Load Rod Files";
    };
    link load_data; //<NEportLevels={2,1}>;	   
    instancer {
      Group => <-.loadrodUI;
    };
    CCP3.ROD.Read.loadrodUI loadrodUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      load_data => <-.load_data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
    };
    CCP3.ROD.Modules.load_rod_files load_rod_files<NEx=407.,NEy=484.> {
      load_data => <-.load_data;
      params => <-.params; 
      do_load => <-.loadrodUI.dialog.ok;
    };
  };

  calcBase Create_Rod_Model {
    link params;   
    UIcmd {
      label = "Create Rod Model";
    };
    link load_data; //is this needed?
    link create_data;
    instancer {
      Group => <-.createmodelUI;
      active = 2;
    };
    CCP3.ROD.Create.createmodelUI createmodelUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      load_data => <-.load_data; //needed?
      create_data => <-.create_data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
  };
					 
  calcBase Run_Rod {
    link params;   
    UIcmd {
      label = "Run Rod";
    };
    link load_data<NEportLevels={2,1}>;
    link rod_data<NEportLevels={2,1}>;
    link ffac_data<NEportLevels={2,1}>;
    link model_type<NEportLevels={2,1}>;
    link edit_model_data<NEportLevels={2,1}>;
    //   link move_params;
    instancer {
      Group => <-.runrodUI;
    };
    CCP3.ROD.Modules.get_rod_data get_rod_data<NEx=407.,NEy=484.> {
      trigger => <-.instancer.Value;
      params => <-.params;
      load_data => <-.load_data;
    };
    CCP3.ROD.Calc.runrodUI runrodUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      load_data => <-.load_data;
      params=> <-.params;
      rod_data=> <-.rod_data;
      ffac_data=> <-.ffac_data;	
      edit_model_data=> <-.edit_model_data;						    
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
    };
  };
  calcBase View_Rod_Files {
    link params<NEportLevels={2,1}>; 
    link load_data<NEportLevels={2,1}>;
    link model_type<NEportLevels={2,1}>;
    UIcmd {
      label = "View Rod Files";
    };
    instancer {
      Group => <-.viewrodUI;
    };
    CCP3.ROD.View.viewrodUI viewrodUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      load_data => <-.load_data;
      model_type=> <-.model_type;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
    };
  };				 
  calcBase Save_Rod_Files {
    link params<NEportLevels={2,1}>; 
    UIcmd {
      label = "Save Rod Files";
    };
    link save_data; 
    instancer {
      Group => <-.saverodUI;
    };
    CCP3.ROD.Write.saverodUI saverodUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      save_data => <-.save_data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
    };
    CCP3.ROD.Modules.save_rod_files save_rod_files<NEx=407.,NEy=484.> {
      save_data => <-.save_data;
      params => <-.params; 
      do_save => <-.saverodUI.dialog.ok;
    };
  };
  CCP3.Core_Macros.CalcObjs.calculation ROD {
    UIcmdList {
      label = "rod";
      order = 15;
      cmdList => {
	Load_Rod_Files.UIcmd,
	Create_Rod_Model.UIcmd,
	Run_Rod.UIcmd,
	View_Rod_Files.UIcmd,	  
	Save_Rod_Files.UIcmd
      };
    };
    link model_type<NEportLevels={2,1}>;
     CCP3.ROD.Modules.ParamsData params<NEportLevels={2,1}> {
      fit_method = 0;							    
      atten = 0.001;
      scale = 1.0;
      scale2 = 1.0;
      beta = 0.0;
      sfrac = 1.0;
      use_scale2 = 0;
      ndisttot = 0; //Should these be set here??
      ndwtot = 0;
      ndwtot2 = 0;
      nocctot = 0;	
      show_fit_results = 0;
    };
    CCP3.ROD.Modules.CalculateRodData rod_data<NEportLevels={2,1}> {
      h = 0.0;
      k = 0.0;
      lstart = -3.9;
      lend = 3.9;
      nl = 200;
      plot_bulk = 1;
      plot_surf = 1;
      plot_both = 1;
      plot_exp = 0;
      new_view = 0;
    };
    CCP3.ROD.Modules.CalculateFfactorsData ffac_data<NEportLevels={0,1}> {
      select_fs = 0;
      plot_calc = 1;
      plot_exp = 0;
      new_view = 0;
      h_start = 0.0;	  
      k_start = 0.0;
      h_end = 3.0;	  
      k_end = 3.0;
      h_step = 1.0;	  
      k_step = 1.0;
      l = 0.0;
      maxq = 3.0;
    };
    CCP3.ROD.Modules.LoadData load_data<NEportLevels={2,1}> {
      name = "";
      file_bulk = ""; 
      file_surf = ""; 		  
      file_exp_data = "";
      file_fit = "";
      file_par = "";							     
      new_view = 0;  
      got_exp_data = 0;
      got_fit_data = 0;
      got_par_data = 0;
    };
    CCP3.ROD.Modules.CreateData create_data<NEportLevels={2,1}> {
      created_surface = 0;
      surf_h = 0;
      surf_k = 0;
      surf_l = 1;
      nlayers = 0;
    };
    CCP3.ROD.Modules.SaveData save_data<NEportLevels={2,1}> {
      file_bulk = ""; 
      file_surf = ""; 		  
      file_par = "";
      file_fit = "";
    };
    CCP3.ROD.Modules.EditModelData edit_model_data<NEportLevels={2,1}> {
      first_edit = 1;
      cancel_edit = 0;
    };					     
    dialog_visible => ((Load_Rod_Files.visible == 1) &&
		       (Create_Rod_model.visible == 1)&&	
		       (Run_Rod.visible == 1)&&
		       (View_Rod_Files.visible == 1)&&
		       (Save_Rod_Files.visible == 1));
    Load_Rod_Files Load_Rod_Files<NEx=110.,NEy=374.> {
      active_menus => (<-.active_menus && !<-.Run_Rod.visible);
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      load_data => <-.load_data;
      params => <-.params;
    };
    Create_Rod_Model Create_Rod_Model<NEx=110.,NEy=374.> {
      active_menus => (<-.active_menus && !<-.Run_Rod.visible);
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      load_data => <-.load_data;
      create_data => <-.create_data;
    };
    Run_Rod Run_Rod {
      active_menus => (<-.active_menus && <-.model_type==4 && !<-.Run_Rod.visible);
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      load_data => <-.load_data;
      rod_data => <-.rod_data;
      ffac_data => <-.ffac_data;
      params => <-.params;
      model_type => <-.model_type;
      edit_model_data => <-.edit_model_data;
    };
    View_Rod_Files View_Rod_Files {
      active_menus => (<-.active_menus && 
		       (<-.model_type==4 || <-.load_data.got_exp_data));
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      load_data => <-.load_data;
      params => <-.params;
      model_type => <-.model_type;
    };					     
    Save_Rod_Files Save_Rod_Files {
      active_menus => (<-.active_menus && <-.model_type==4);
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      save_data => <-.save_data;
      params => <-.params;
    };					     
  };
};
