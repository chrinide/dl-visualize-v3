
flibrary Create<NEeditable=1,
#ifndef DLV_NO_DLLS
	     dyn_libs="libCCP3rod",
#endif // DLV_NO_DLLS
	     export_cxx=1,
	     build_dir="avs/src/express",
             cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/rod/rod_create_gen.hxx avs/src/rod/rod_createlist_gen.hxx",
	     out_hdr_file="rod_create.hxx",
	     out_src_file="rod_create.cxx"> {
  macro createmodelUI {  
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.ROD.Modules.LoadData &load_data<NEx=792.,NEy=77.,NEportLevels={2,0}>;
    CCP3.ROD.Modules.CreateData &create_data<NEx=702.,NEy=77.,NEportLevels={2,0}>;
    GMOD.parse_v parse_v0<NEx=319.,NEy=77.> {
      v_commands = "create_data.get_list = 1; create_data.new_view = 0; create_data.list_size = 0; create_data.created_bulk = 0;  create_data.created_surface = 0;";
      on_inst = 1;
      relative => <-;				    
};
    CCP3.ROD.Modules.list_bulks list_bulks<NEx=407.,NEy=484.> {
      trigger=> <-.create_data.get_list;  
      list_size => <-.create_data.list_size;
      bulk_names => <-.create_data.bulk_names;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 400;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
      title = "Create rod model";
      ok = 0;
      okButton = 1;
      okLabelString = "Accept";
      cancel<NEportLevels={1,2}> = 0;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtextTypein search<NEx=18.,NEy=216.> {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      slabel => "Search Database";
      visible = 1;
      width => UIparent.clientWidth;
      //     stext =>  <-.create_data.bulk_names;
      y = 0;
      text_label.width = 120;
      text_label.alignment = "left";
      text.x => <-.text_label.width;
      text.width => <-. width/2.0;
    };
    UIlabel select_crystal {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = search.y + search.height + 5;
      width => UIparent.clientWidth;
      alignment = "left";
      label = "Select bulk crystal";
    };
    UIlist list_of_models<NEx=264.,NEy=187.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      x = 5;
      y => ((<-.select_crystal.y + <-.select_crystal.height) + 5);
      height = 150;
      width => parent.clientWidth/2;
      selectedItem = -1;
      selectedText => <-.create_data.bulk_names;
      strings => <-.model_list;
    };
    string model_list<NEportLevels=1,NEx=517.,NEy=99.>[] = { //to be read in
      "Ag",
      "Cu",
      "NaCl"
    };
    UIbutton create_bulk_now<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Show Bulk";
      width => parent.clientWidth/3;
      x => parent.clientWidth*7/12;
      y => ((<-.list_of_models.y + 0.5*<-.list_of_models.height));
      active => <-.list_of_models.selectedItem + 1;
    };
    CCP3.ROD.Modules.create_model create_model<NEx=407.,NEy=484.> {
      trigger_bulk => <-.create_bulk_now.do;
      trigger_surf => <-.create_surface_now.do;
      trigger_accept => <-.dialog.ok;
      trigger_cancel => <-.dialog.cancel;
      create_data => <-.create_data;
      selected_bulk => <-.list_of_models.selectedItem;
    };
    UIlabel surface {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => 0;
      y => <-.list_of_models.y + <-.list_of_models.height + 10;
      alignment = "center";
      width => parent.clientWidth/2;
      label = "Select Surface [hkl]";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein h_val {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.create_data.surf_h;
      x = 0;
      y => <-.surface.y  +surface.height + 5;
      width=> <-.panel.clientWidth/6;
      label.width = 0;
      field.x => <-.<-.x + <-.label.width;
      field.width => <-.width - <-.label.width;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein k_val {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      //     flabel = "k";
      fval => <-.create_data.surf_k;
      x => width;
      y => <-.h_val.y;
      width=> <-.panel.clientWidth/6;
      label.width = 0;
      field.x => <-.<-.x + <-.label.width;
      field.width => <-.width - <-.label.width;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein l_val {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      //flabel = "l";
      fval => <-.create_data.surf_l;
      x => 2.0*width;
      y => <-.h_val.y;
      width=> <-.panel.clientWidth/6;
      label.width = 0;
      field.x => <-.<-.x + <-.label.width;
      field.width => <-.width - <-.label.width;
    };
    UIlabel num_layers {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => parent.clientWidth/2;
      y => <-.list_of_models.y + <-.list_of_models.height + 10;
      alignment = "center";
      width => parent.clientWidth/2;
      label = "Number of Layers";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein layers {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.create_data.nlayers;
      x => <-.panel.clientWidth*2/3;
      y => <-.surface.y  +surface.height + 5;
      width=> <-.panel.clientWidth/6;
      label.width = 0;
      field.x => <-.<-.x + <-.label.width;
      field.width => <-.width - <-.label.width;
    };
    UIbutton create_surface_now<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Create Surface";
      width => parent.clientWidth/2;
      x => parent.clientWidth/4;
      y => <-.h_val.y + <-.h_val.height + 10;
      active => <-.list_of_models.selectedItem + 1;					    
    };
/*    UIlabel add_atom {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => <-.n_layers.y + <-.n_layers.height + 10;
      alignment = "left";
      width => parent.clientWidth;
      label = "Add a surface atom";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtextTypein atom_type {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      slabel = "Element";
      visible = 1;
      //fval => <-.params.scale2_min;
      x = 0;
      y => <-.add_atom.y + <-.add_atom.height + 10;
      width=> <-.panel.clientWidth*2/8;
      //label.width => <-.width/2.0;
      //      text.width => <-.width/2.0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein atom_x {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel = "x";
      //fval => <-.params.scale2_min;
      x => <-.panel.clientWidth*2/8;
      y => <-.add_atom.y + <-.add_atom.height + 10;
      width=> <-.panel.clientWidth*2/8;
      //      label.width => <-.width/2.0;
      //     field.width => <-.width/2.0;
    };	
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein atom_y {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel = "y";
      //fval => <-.params.scale2_min;
      x => <-.panel.clientWidth*4/8;
      y => <-.atom_x.y;
      width=> <-.panel.clientWidth*2/8;
      //      label.width => <-.width/2.0;
      //     field.width => <-.width/2.0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein atom_z {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel = "z";
      //fval => <-.params.scale2_min;
      x => <-.panel.clientWidth*6/8;
      y => <-.atom_x.y;
      width=> <-.panel.clientWidth*2/8;
      //      label.width => <-.width/2.0;
      //     field.width => <-.width/2.0;
    };
    UIbutton add_atom_now<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Add Atom";
      width => parent.clientWidth/3;
      x => parent.clientWidth/3;
      y => <-.atom_x.y + <-.atom_x.height + 10;
    };	 */
  };
};
