
flibrary Fit<NEeditable=1,
#ifndef DLV_NO_DLLS
	     dyn_libs="libCCP3rod",
#endif // DLV_NO_DLLS
	     export_cxx=1,
	     build_dir="avs/src/express",
             cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/rod/rod_params_fit_gen.hxx", 
	     out_hdr_file="rod_fit.hxx",
	     out_src_file="rod_fit.cxx"> {
  macro RodFitTabUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1}>;
    CCP3.ROD.Modules.ParamsData &params<NEportLevels={2,0}>;
    CCP3.ROD.Modules.LoadData &load_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    link select_tab<NEportLevels=1>;
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow panel {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
#ifdef MSDOS
      virtualWidth => width;
      virtualHeight => height;
#endif // MSDOS
      x = 0;
      y = 0;
      visible => <-.select_tab==3;
    };
    UIlabel value {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => <-.panel.clientWidth*2/9;
      y = 0;
      alignment = "left";
      width => parent.clientWidth*2/9.0;
      label = "Value";
    };	
    UIlabel min {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => <-.panel.clientWidth*4/9;
      y = 0;
      alignment = "left";
      width => parent.clientWidth*2/9.0;
      label = "Min";
    };	
    UIlabel max {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => <-.panel.clientWidth*6/9;
      y = 0;
      alignment = "left";
      width => parent.clientWidth*2/9.0;
      label = "Max";
    };	
    UIlabel fit {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => <-.panel.clientWidth*8/9;
      y = 0;
      alignment = "left";
      width => parent.clientWidth*2/9/3.0;
      label = "Fit?";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein scale<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Scale";
      fval => <-.params.scale;
      fmin => <-.params.scale_min;
      fmax => <-.params.scale_max;
      y => (value.y + value.height + 5);
      width=> <-.panel.clientWidth*4/9;
      label.width => (<-.panel.width/2);
      field.x => <-.label.width;
      field.width => (<-.panel.width/2);
      field.decimalPoints = 4;
    };

    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein scale_min<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.params.scale_min;
      fmax=> <-.params.scale_max;
      x => <-.panel.clientWidth*4/9;
      y => (value.y + value.height + 5);
      width=> <-.panel.clientWidth*2/9;
      label.width = 0;
      field.x => <-.label.width;
      field.width => (<-.panel.width);
      field.decimalPoints = 4;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein scale_max<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.params.scale_max;
      fmin => <-.params.scale_min;
      x => <-.panel.clientWidth*6/9;
      y => (value.y + value.height + 5);
      width=> <-.panel.clientWidth*2/9;
      label.width = 0;
      field.x => <-.label.width;
      field.width => (<-.panel.width);
      field.decimalPoints = 4;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle scale_fit<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*8/9;
      y => ((value.y + value.height) + 5);
      width => parent.clientWidth/9;
      height => (UIdata.UIfonts[0].lineHeight + 9);
      label = " ";
      set => <-.params.scale_fit; 
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein scale2<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Scale2";
      fval => <-.params.scale2;
      fmin => <-.params.scale2_min;
      fmax => <-.params.scale2_max;
      y => (scale.y + scale.height + 5);
      width=> <-.panel.clientWidth*4/9;
      label.width => (<-.panel.width/2);
      field.x => <-.label.width;
      field.width => (<-.panel.width/2);
      field.decimalPoints = 4;
      panel.active => params.use_scale2;
      label.active => params.use_scale2;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein scale2_min<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.params.scale2_min;
      fmax => <-.params.scale2_max;
      x => <-.panel.clientWidth*4/9;
      y => (scale.y + scale.height + 5);
      width=> <-.panel.clientWidth*2/9;
      label.width = 0;
      field.x => <-.label.width;
      field.width => (<-.panel.width);
      field.decimalPoints = 4;
      panel.active => params.use_scale2;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein scale2_max<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.params.scale2_max;
      fmin => <-.params.scale2_min;
      x => <-.panel.clientWidth*6/9;
      y => (scale.y + scale.height + 5);
      width=> <-.panel.clientWidth*2/9;
      label.width = 0;
      field.x => <-.label.width;
      field.width => (<-.panel.width);
      field.decimalPoints = 4;
      panel.active => params.use_scale2;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle scale2_fit<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*8/9;
      y => (scale.y + scale.height + 5);
      width => parent.clientWidth/9;
      height => (UIdata.UIfonts[0].lineHeight + 9);
      label = " ";
      set => <-.params.scale2_fit; 
      active => params.use_scale2;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein beta<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Beta";
      fval => <-.params.beta;
      fmin => <-.params.beta_min;
      fmax => <-.params.beta_max;
      y => (scale2.y + scale2.height + 5);
      width=> <-.panel.clientWidth*4/9;
      label.width => (<-.panel.width/2);
      field.x => <-.label.width;
      field.width => (<-.panel.width/2);
      field.decimalPoints = 4;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein beta_min<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.params.beta_min;
      fmin = 0.0;
      fmax = <-.params.beta_max;
      x => <-.panel.clientWidth*4/9;
      y => (scale2.y + scale2.height + 5);
      width=> <-.panel.clientWidth*2/9;
      label.width = 0;
      field.x => <-.label.width;
      field.width => (<-.panel.width);
      field.decimalPoints = 4;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein beta_max<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.params.beta_max;
      fmin => <-.params.beta_min;
      fmax = 1.0;
      x => <-.panel.clientWidth*6/9;
      y => (scale2.y + scale2.height + 5);
      width=> <-.panel.clientWidth*2/9;
      label.width = 0;
      field.x => <-.label.width;
      field.width => (<-.panel.width);
      field.decimalPoints = 4;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle beta_fit<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*8/9;
      y => (scale2.y + scale2.height + 5);
      width => parent.clientWidth/9;
      height => (UIdata.UIfonts[0].lineHeight + 9);
      label = " ";
      set => <-.params.beta_fit; 
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein sfrac<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Surf. Frac.";
      fval => <-.params.sfrac;
      fmin => <-.params.sfrac_min;
      fmax => <-.params.sfrac_max;
      y => (beta.y + beta.height + 5);
      width=> <-.panel.clientWidth*4/9;
      label.width => (<-.panel.width/2);
      field.x => <-.label.width;
      field.width => (<-.panel.width/2);
      field.decimalPoints = 4;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein sfrac_min<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.params.sfrac_min;
      fmin = 0.0;
      fmax => <-.params.sfrac_max;
      x => <-.panel.clientWidth*4/9;
      y => (beta.y + beta.height + 5);
      width=> <-.panel.clientWidth*2/9;
      label.width = 0;
      field.x => <-.label.width;
      field.width => (<-.panel.width);
      field.decimalPoints = 4;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein sfrac_max<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.params.sfrac_max;
      fmin => <-.params.sfrac_min;
      fmax = 1.0;
      x => <-.panel.clientWidth*6/9;
      y => (beta.y + beta.height + 5);  
      width=> <-.panel.clientWidth*2/9;
      label.width = 0;
      field.x => <-.label.width;
      field.width => (<-.panel.width);
      field.decimalPoints = 4;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle sfrac_fit<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*8/9;
      y => (beta.y + beta.height + 5);
      width => parent.clientWidth/9;
      height => (UIdata.UIfonts[0].lineHeight + 9);
      label = " ";
      set => <-.params.sfrac_fit; 
    };
    int visible_dis_list = 0;
    int visible_dw1_list = 0;
    int visible_dw2_list = 0;
    int visible_occ_list = 0;
    CCP3.ROD.Modules.show_lists show_lists{
      trigger_dis_on => <-.dis_list.do; 
      trigger_dis_off => <-.dis_hide.do;
      trigger_dw1_on => <-.dw1_list.do; 
      trigger_dw1_off => <-.dw1_hide.do;
      trigger_dw2_on => <-.dw2_list.do; 
      trigger_dw2_off => <-.dw2_hide.do;
      trigger_occ_on => <-.occ_list.do; 
      trigger_occ_off => <-.occ_hide.do;
      visible_dis => <-.visible_dis_list;
      visible_dw1 => <-.visible_dw1_list;
      visible_dw2 => <-.visible_dw2_list;
      visible_occ => <-.visible_occ_list;
    };
    UIlabel dis_label {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (sfrac.y + sfrac.height);
      alignment = "left";
      width => parent.clientWidth*2/9;
      label = "Displacements:";
    };
    UIbutton dis_list<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "List";
      width => parent.clientWidth*2/9;
      x => parent.clientWidth*2/9;
      y => dis_label.y;
      visible => !<-.visible_dis_list;
    };
    UIbutton dis_hide<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Hide";
      width => parent.clientWidth*2/9;
      x => parent.clientWidth*2/9;
      y => dis_label.y;
      visible => <-.visible_dis_list;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle all_dis_fit<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*8/9;
      y => dis_label.y;
      width => parent.clientWidth/9;
      height => (UIdata.UIfonts[0].lineHeight);
      label = " ";
      visible => (<-.visible_dis_list && <-.params.ndisttot);
    };
    UIlabel dis_invert_label {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*6/9;
      y => dis_label.y;
      alignment = "left";
      visible => (<-.visible_dis_list && <-.params.ndisttot);
      width => parent.clientWidth*2/9;
      label = "Invert selection";
    };
    macro macro_dis<NEx=638.,NEy=286.>[params.ndisttot] {
      int index => (index_of(<-.macro_dis) + 1);
      string string => index;
      int height => <-.scale.height;
      UIlabel UIlabel<NEx=55.,NEy=220.> {
	parent => <-.<-.panel;
	label => <-.string;
	y => (dis_hide.y + dis_hide.height + (<-.index-1)*<-.height);
	width = 25;
	alignment = "left";
	visible => <-.<-.visible_dis_list;
      };
      UIbutton show<NEx=594.,NEy=252.> {
	parent => <-.<-.panel;
	&color => <-.<-.prefs.colour;
	&fontAttributes => <-.<-.prefs.fontAttributes;
	label => "Show";
	x => 30;
	y => UIlabel.y;
	width => parent.clientWidth*2/9 - x;
	visible => <-.UIlabel.visible;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein value{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.dist[<-.index-1];
	fmin => <-.<-.params.dist_min[<-.index-1];
	fmax => <-.<-.params.dist_max[<-.index-1];
	x => <-.<-.panel.clientWidth*2/9;
	y => UIlabel.y;
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
	panel.visible => <-.<-.UIlabel.visible;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein min{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.dist_min[<-.index-1];
	x => <-.<-.panel.clientWidth*4/9;
	y => UIlabel.y;
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
	panel.visible => <-.<-.UIlabel.visible;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein max{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.dist_max[<-.index-1];
	x => <-.<-.panel.clientWidth*6/9;
	y => UIlabel.y;
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
	panel.visible => <-.<-.UIlabel.visible;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle fit{
	parent => <-.<-.panel;
	&color => <-.<-.prefs.colour;
	&fontAttributes => <-.<-.prefs.fontAttributes;
	x =>  <-.<-.panel.clientWidth*8/9;
	y => UIlabel.y;
	width => parent.clientWidth/9;
	height => (UIdata.UIfonts[0].lineHeight + 9);
	label = " ";
	set => <-.<-.params.dist_fit[<-.index-1]; 
	visible => <-.UIlabel.visible;
      };
    };
    UIlabel dw1_label {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => switch((params.ndisttot>0 && <-.visible_dis_list)+1, 
		  <-.dis_list.y + <-.dis_list.height,
		  <-.macro_dis[(params.ndisttot-1)].value.y 
		  + <-.macro_dis[(params.ndisttot-1)].value.height); 
      alignment = "left";
      width => parent.clientWidth*2/9;
      label = "In-plane DW";
    };
    UIbutton dw1_list<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "List";
      width => parent.clientWidth*2/9;
      x => parent.clientWidth*2/9;
      y => dw1_label.y;
      visible => !<-.visible_dw1_list;
    };
    UIbutton dw1_hide<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Hide";
      width => parent.clientWidth*2/9;
      x => parent.clientWidth*2/9;
      y => dw1_label.y;
      visible => <-.visible_dw1_list;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle all_dw1_fit<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*8/9;
      y => dw1_label.y;
      width => parent.clientWidth/9;
      height => (UIdata.UIfonts[0].lineHeight);
      label = " ";
      visible => (<-.visible_dw1_list && <-.params.ndwtot);
    };
    UIlabel dw1_invert_label {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*6/9;
      y => dw1_label.y;
      alignment = "left";
      visible => (<-.visible_dw1_list && <-.params.ndwtot);
      width => parent.clientWidth*2/9;
      label = "Invert selection";
    };
    macro macro_dw1<NEx=638.,NEy=286.>[params.ndwtot] {
      int index => (index_of(<-.macro_dw1) + 1);
      string string => index;
      int height => <-.scale.height;
      UIlabel UIlabel<NEx=55.,NEy=220.> {
	parent => <-.<-.panel;
	label => <-.string;
	y => (dw1_hide.y + dw1_hide.height + (<-.index-1)*<-.height);
	alignment = "center";
	visible => <-.<-.visible_dw1_list;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein value{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.dw1[<-.index-1];
	fmin => <-.<-.params.dw1_min[<-.index-1];
	fmax => <-.<-.params.dw1_max[<-.index-1];
	x => <-.<-.panel.clientWidth*2/9;
	y => UIlabel.y;
	panel.visible => <-.<-.UIlabel.visible;
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein min{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.dw1_min[<-.index-1];
	x => <-.<-.panel.clientWidth*4/9;
	y => UIlabel.y;
	panel.visible => <-.<-.UIlabel.visible;				     
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein max{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.dw1_max[<-.index-1];
	x => <-.<-.panel.clientWidth*6/9;
	y => UIlabel.y;
	panel.visible => <-.<-.UIlabel.visible;	
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle fit{
	parent => <-.<-.panel;
	&color => <-.<-.prefs.colour;
	&fontAttributes => <-.<-.prefs.fontAttributes;
	x =>  <-.<-.panel.clientWidth*8/9;
	y => UIlabel.y;
	visible => <-.UIlabel.visible;
	width => parent.clientWidth/9;
	height => (UIdata.UIfonts[0].lineHeight + 9);
	label = " ";
	set => <-.<-.params.dw1_fit[<-.index-1]; 
      };
    };
    UIlabel dw2_label {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => switch((params.ndwtot>0 && <-.visible_dw1_list)+1, 
		  <-.dw1_list.y + <-.dw1_list.height,
		  <-.macro_dw1[(params.ndwtot-1)].value.y 
		  + <-.macro_dw1[(params.ndwtot-1)].value.height);
      alignment = "left";
      width => parent.clientWidth*2/9;
      label = "Out-of-plane DW";
    };
    UIbutton dw2_list<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "List";
      width => parent.clientWidth*2/9;
      x => parent.clientWidth*2/9;
      y => dw2_label.y;
      visible => !<-.visible_dw2_list;
    };
    UIbutton dw2_hide<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Hide";
      width => parent.clientWidth*2/9;
      x => parent.clientWidth*2/9;
      y => dw2_label.y;
      visible => <-.visible_dw2_list;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle all_dw2_fit<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*8/9;
      y => dw2_label.y;
      width => parent.clientWidth/9;
      height => (UIdata.UIfonts[0].lineHeight);
      label = " ";
      visible => (<-.visible_dw2_list && <-.params.ndwtot2);
    };
    UIlabel dw2_invert_label {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*6/9;
      y => dw2_label.y;
      alignment = "left";
      visible => (<-.visible_dw2_list && <-.params.ndwtot2);
      width => parent.clientWidth*2/9;
      label = "Invert selection";
    };
    macro macro_dw2<NEx=638.,NEy=286.>[params.ndwtot2] {
      int index => (index_of(<-.macro_dw2) + 1);
      string string => index;
      int height => <-.scale.height;
      UIlabel UIlabel<NEx=55.,NEy=220.> {
	parent => <-.<-.panel;
	label => <-.string;
	x = 0;				 
	y => <-.<-.dw2_hide.y + <-.<-.dw2_hide.height + 5
	  + (<-.index-1)*<-.height;
	visible => <-.<-.visible_dw2_list;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein value{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.dw2[<-.index-1];
	fmin => <-.<-.params.dw2_min[<-.index-1];
	fmax => <-.<-.params.dw2_max[<-.index-1];
	x => <-.<-.panel.clientWidth*2/9;
	y => UIlabel.y;
	panel.visible => <-.<-.UIlabel.visible;
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein min{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.dw2_min[<-.index-1];
	x => <-.<-.panel.clientWidth*4/9;
	y => UIlabel.y;
	panel.visible => <-.<-.UIlabel.visible;
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein max{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.dw2_max[<-.index-1];
	x => <-.<-.panel.clientWidth*6/9;
	y => UIlabel.y;
	panel.visible => <-.<-.UIlabel.visible;
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle fit{
	parent => <-.<-.panel;
	&color => <-.<-.prefs.colour;
	&fontAttributes => <-.<-.prefs.fontAttributes;
	x =>  <-.<-.panel.clientWidth*8/9;
	y => UIlabel.y;
	visible => <-.UIlabel.visible;
	width => parent.clientWidth/9;
	height => (UIdata.UIfonts[0].lineHeight + 9);
	label = " ";
	set => <-.<-.params.dw2_fit[<-.index-1]; 
      };
    };
    UIlabel occ_label {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => switch((params.ndwtot2>0 && <-.visible_dw2_list)+1, 
		  <-.dw2_list.y + <-.dw2_list.height,
		  <-.macro_dw2[(params.ndwtot2-1)].value.y 
		  + <-.macro_dw2[(params.ndwtot2-1)].value.height);
      alignment = "left";
      width => parent.clientWidth*2/9;
      label = "Occup. Factor";
    };
    UIbutton occ_list<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "List";
      width => parent.clientWidth*2/9;
      x => parent.clientWidth*2/9;
      y => occ_label.y;
      visible => !<-.visible_occ_list;
    };
    UIbutton occ_hide<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Hide";
      width => parent.clientWidth*2/9;
      x => parent.clientWidth*2/9;
      y => occ_label.y;
      visible => <-.visible_occ_list;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle all_occ_fit<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*8/9;
      y => occ_label.y;
      width => parent.clientWidth/9;
      height => (UIdata.UIfonts[0].lineHeight);
      label = " ";
      visible => (<-.visible_occ_list && <-.params.nocctot);
    };
    UIlabel occ_invert_label {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x =>  <-.panel.clientWidth*6/9;
      y => occ_label.y;
      alignment = "left";
      visible => (<-.visible_occ_list && <-.params.nocctot);
      width => parent.clientWidth*2/9;
      label = "Invert selection";
    };
    macro macro_occ<NEx=638.,NEy=286.>[params.nocctot] {
      int index => (index_of(<-.macro_occ) + 1);
      string string => index;
      int height => <-.scale.height;
      UIlabel UIlabel<NEx=55.,NEy=220.> {
	parent => <-.<-.panel;
	label => <-.string;
	y => <-.<-.occ_hide.y + <-.<-.occ_hide.height + 5
	  + (<-.index-1)*<-.height;
	alignment = "center";
	visible => <-.<-.visible_occ_list;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein value{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.occup[<-.index-1];
	fmin => <-.<-.params.occup_min[<-.index-1];
	fmax => <-.<-.params.occup_max[<-.index-1];
	x => <-.<-.panel.clientWidth*2/9;
	y => UIlabel.y;
	panel.visible => <-.<-.UIlabel.visible;
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein min{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.occup_min[<-.index-1];
	x => <-.<-.panel.clientWidth*4/9;
	y => UIlabel.y;
	panel.visible => <-.<-.UIlabel.visible;
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein max{
	UIparent => <-.<-.panel;
	color => <-.<-.prefs.colour;
	fontAttributes => <-.<-.prefs.fontAttributes;
	fval => <-.<-.params.occup_max[<-.index-1];
	x => <-.<-.panel.clientWidth*6/9;
	y => UIlabel.y;
	panel.visible => <-.<-.UIlabel.visible;
	width=> <-.<-.panel.clientWidth*2/9;
	label.width = 0;
	field.x => <-.label.width;
	field.width => (<-.<-.panel.width);
	field.decimalPoints = 4;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle fit{
	parent => <-.<-.panel;
	&color => <-.<-.prefs.colour;
	&fontAttributes => <-.<-.prefs.fontAttributes;
	x =>  <-.<-.panel.clientWidth*8/9;
	y => UIlabel.y;
	visible => <-.UIlabel.visible;
	width => parent.clientWidth/9;
	height => (UIdata.UIfonts[0].lineHeight + 9);
	label = " ";
	set => <-.<-.params.occup_fit[<-.index-1]; 
      };
    };
    CCP3.ROD.Modules.update_params update_params<NEx=407.,NEy=484.> {
      trigger_dis => <-.all_dis_fit.set;
      trigger_dw1 => <-.all_dw1_fit.set;
      trigger_dw2 => <-.all_dw2_fit.set;
      trigger_occ => <-.all_occ_fit.set;
      params => <-.params;
    };
    UIbutton run_lm_fit_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Run LM Fit";  
      width => parent.clientWidth/3;
      active =>  <-.load_data.got_exp_data;
      x => parent.clientWidth/6;
      y => switch((params.nocctot>0 && <-.visible_occ_list)+1, 
		  <-.occ_list.y + <-.occ_list.height,
		  <-.macro_occ[(params.nocctot-1)].value.y 
		  + <-.macro_occ[(params.nocctot-1)].value.height);
    };
    UIbutton run_ana_fit_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Run ASA Fit ";
      width => parent.clientWidth/3;
      active =>  <-.load_data.got_exp_data;
      x => parent.clientWidth/2;
      y => run_lm_fit_button.y;
    };
    CCP3.ROD.Modules.run_fit run_fit<NEx=407.,NEy=484.> {
      int update_sgl => (<-.scale.fval || <-.scale2.fval || <-.beta.fval || 
			 <-.sfrac.fval);
      int update_dist => switch((params.ndisttot>0)+1, 
				0 , <-.macro_dis[0].value.fval);
      int update_dw1 => switch((params.ndwtot>0)+1, 
			       0 , <-.macro_dw1[0].value.fval);
      int update_dw2 => switch((params.ndwtot2>0)+1, 
				0 , <-.macro_dw2[0].value.fval);
      int update_occ => switch((params.nocctot>0)+1, 
			       0 , <-.macro_occ[0].value.fval);
      trigger_no_opt => (update_sgl || update_dist || update_dw1 || update_dw2
			 || update_occ);
      trigger_lm => <-.run_lm_fit_button.do; 
      trigger_ana => <-.run_ana_fit_button.do; 
      params => <-.params;
      load_data => <-.load_data;
    };
    UIlabel results {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (run_lm_fit_button.y + run_lm_fit_button.height);
      alignment = "left";
      width => parent.clientWidth;
      label = "Fit Results:";
      visible => <-.params.show_fit_results;
    };
    UIlabel results_scale {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (results.y + results.height);
      alignment = "center";
      width => parent.clientWidth;
      label => str_format("           scale = %6.4f +/- %6.4f",
			  <-.params.fit_results[0], <-.params.fit_results[1]);
      visible => <-.params.show_fit_results;
    }; 
     UIlabel results_scale2 {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (results_scale.y + results_scale.height);
      alignment = "center";
      width => parent.clientWidth;
      label => str_format("         scale 2 = %6.4f +/- %6.4f",
			  <-.params.fit_results[2], <-.params.fit_results[3]);
      visible => <-.params.show_fit_results;
      active => params.use_scale2;
    }; 
     UIlabel results_beta {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (results_scale2.y + results_scale2.height);
      alignment = "center";
      width => parent.clientWidth;
      label => str_format("            beta = %6.4f +/- %6.4f",
			  <-.params.fit_results[4], <-.params.fit_results[5]);
      visible =>   <-.params.show_fit_results;
    }; 
     UIlabel results_sfrac {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (results_beta.y + results_beta.height);
      alignment = "center";
      width => parent.clientWidth;
      label => str_format("surface fraction = %6.4f +/- %6.4f",
			  <-.params.fit_results[6], <-.params.fit_results[7]);
      visible =>  <-.params.show_fit_results;
    }; 
  };
};
