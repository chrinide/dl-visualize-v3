
#include "avs/src/crystal/load_gen.hxx"
#include "avs/src/crystal/str_gen.hxx"
#include "avs/src/crystal/save_gen.hxx"
#include "avs/src/crystal/ex_gen.hxx"
#include "avs/src/crystal/run_gen.hxx"
#include "avs/src/crystal/curw_gen.hxx"
#include "avs/src/crystal/wfn_gen.hxx"
#include "avs/src/crystal/scf_gen.hxx"
#include "avs/src/crystal/grid_gen.hxx"
#include "avs/src/crystal/echg_gen.hxx"
#include "avs/src/crystal/potm_gen.hxx"
#include "avs/src/crystal/dos_gen.hxx"
#include "avs/src/crystal/band_gen.hxx"
#include "avs/src/crystal/bdos_gen.hxx"
#include "avs/src/crystal/ppan_gen.hxx"
#include "avs/src/crystal/dlv3_gen.hxx"
#include "avs/src/crystal/bloc_gen.hxx"
#include "avs/src/crystal/wn3d_gen.hxx"
#include "avs/src/crystal/bril_gen.hxx"
#include "avs/src/crystal/wn2d_gen.hxx"
#include "avs/src/crystal/neb_gen.hxx"
#include "avs/src/crystal/neb_list_gen.hxx"
#include "avs/src/crystal/neb_view_gen.hxx"
#include "avs/src/crystal/neb_reset_gen.hxx"
#include "avs/src/crystal/neb_hide_gen.hxx"
#include "avs/src/crystal/neb_edit_gen.hxx"
#include "avs/src/crystal/neb_create_gen.hxx"
#include "avs/src/crystal/neb_resetim_gen.hxx"
#include "avs/src/crystal/neb_viewimage_gen.hxx"
#include "avs/src/crystal/neb_edimage_gen.hxx"
#include "avs/src/crystal/neb_editmod_gen.hxx"
#include "avs/src/crystal/neb_write91_gen.hxx"
#include "avs/src/crystal/neb_accept_gen.hxx"
#include "avs/src/crystal/td3d_gen.hxx"
#include "avs/src/crystal/topo3d_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/utils.hxx"
#include "src/dlv/operation.hxx"
#include "src/dlv/calculation.hxx"
#include "src/dlv/op_model.hxx"
#include "src/dlv/op_admin.hxx"
#include "src/dlv/data_objs.hxx"
#include "src/dlv/atom_model.hxx"
#include "src/dlv/model.hxx"
#include "src/dlv/data_atoms.hxx"
#include "src/dlv/data_plots.hxx"
#include "src/dlv/data_surf.hxx"
#include "src/dlv/data_vol.hxx"
#include "src/dlv/job_setup.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "src/crystal/calcs.hxx"
#include "src/crystal/struct.hxx"
#include "src/crystal/save.hxx"
#include "src/crystal/props.hxx"
#include "src/crystal/slices.hxx"
#include "src/crystal/echg.hxx"
#include "src/crystal/potm.hxx"
#include "src/crystal/volumes.hxx"
#include "src/crystal/grid3d.hxx"
#include "src/crystal/wannier.hxx"
#include "src/crystal/brillouin.hxx"
#include "src/crystal/density.hxx"
#include "src/crystal/plots.hxx"
#include "src/crystal/dos.hxx"
#include "src/crystal/band.hxx"
#include "src/crystal/bdos.hxx"
#include "src/crystal/mulliken.hxx"
#include "src/crystal/eigvec.hxx"
#include "src/crystal/bloch.hxx"
#include "src/crystal/run_input.hxx"
#include "src/crystal/extract.hxx"
#include "src/crystal/wavefn.hxx"
#include "src/crystal/scf.hxx"
#include "src/crystal/phonon.hxx"
#include "src/crystal/neb.hxx"
#include "src/crystal/tddft.hxx"
#include "src/crystal/topond3d.hxx"

int CCP3_CRYSTAL_Modules_load_structure::load(OMevent_mask event_mask,
					      int seq_num)
{
  // data (StructureData read req)
  char *data_file = (char *) data.file;
  char *name = (char *) data.name;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  message[0] = '\0';
  OMobj_id id = OMfind_str_subobj(OMinst_obj,
				  "DLV.Display_Objs.default_data.bond_all",
				  OM_OBJ_RD);
  int set_bonds = 0;
  OMget_int_val(id, &set_bonds);
  op = CRYSTAL::load_structure::create(name, data_file,
				       ((bool)(data.fractional == 1)),
				       (set_bonds == 1), message, 256);
  int ok;
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.CRYSTAL.Model.load", message);
  else if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_WARNING,
			   "CCP3.CRYSTAL.Model.load", message);
  else
    ok = CCP3::render_in_view(op, ((bool)(data.new_view == 1)));
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_save_str_file::save(OMevent_mask event_mask,
					     int seq_num)
{
  // filename (OMXstr read req)
  // geometry (OMXint read req)
  char *data_file = (char *) filename;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;
  CCP3::show_as_busy();
  if ((int)geometry == 1)
    op = CRYSTAL::save_geometry::create(data_file, message, 256);
  else
    op = CRYSTAL::save_structure::create(data_file, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Model.write", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_load_data::load(OMevent_mask event_mask, int seq_num)
{
  // file_type (OMXint read req)
  // filename (OMXstr read req)
  // version (OMXint read req)
  int v = (int)version;
  char *data_file = (char *) filename;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  DLV::operation *op = 0;
  CCP3::show_as_busy();
  bool kspace = false;
  switch ((int)file_type) {
  case 0:
    op = CRYSTAL::load_3d_data::create(data_file, v, message, 256);
    break;
  case 1:
    op = CRYSTAL::load_wannier3D_data::create(data_file, v, message, 256);
    break;
  case 2:
    op = CRYSTAL::load_ek_3d_data::create(data_file, v, message, 256);
    kspace = true;
    break;
  case 3:
    op = CRYSTAL::load_charge_d_slice::create(data_file, v, message, 256);
    break;
  case 4:
    op = CRYSTAL::load_potential_slice::create(data_file, message, 256);
    break;
  case 5:
    op = CRYSTAL::load_wannier2D_data::create(data_file, v, message, 256);
    break;
  case 6:
    op = CRYSTAL::load_dos_data::create(data_file, v, message, 256);
    break;
  case 7:
    op = CRYSTAL::load_band_data::create(data_file, v, message, 256);
    break;
  case 8:
    op = CRYSTAL::load_mulliken_data::create(data_file, message, 256);
    break;
  case 9:
    op = CRYSTAL::load_phonon_data::create(data_file, message, 256);
    break;
  case 10:
    op = CRYSTAL::load_bloch_data::create(data_file, v, message, 256);
    break;
  case 11:
    op = CRYSTAL::load_tddft_plot::create(data_file, v, message, 256);
    break;
  case 12:
    op = CRYSTAL::load_eigenvec_data::create(data_file, message, 256);
    break;
  default:
    strncpy(message, "BUG: property type not recognised", 256);
    break;
  };
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Property.Load", message);
  else {
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_WARNING,
			     "CCP3.CRYSTAL.Property.Load", message);
    ok = CCP3::update_op_in_3Dview(op, kspace);
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Structure::extract(OMevent_mask event_mask,
					    int seq_num)
{
  // filename (OMXstr read req)
  // strname (OMXstr read)
  // new_view (OMXint read req)
  // job_data (read req)
  CCP3::show_as_busy();
  char *input_file = (char *) filename;
  char *str_file = (char *) strname;
  char message[256];
  message[0] = '\0';
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  OMobj_id id = OMfind_str_subobj(OMinst_obj,
				  "DLV.Display_Objs.default_data.bond_all",
				  OM_OBJ_RD);
  int set_bonds = 0;
  OMget_int_val(id, &set_bonds);
  bool use_structure = strname.valid_obj();
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *) job_data.queue,
			  (bool) job_data.is_parallel);
  op = CRYSTAL::extract_calc::create(input_file, str_file, use_structure,
				     (int)new_view, (set_bonds == 1),
				     job, message, 256);
  // if (op != 0)
  // Todo - draw stuff?
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Extract.run", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Analyse::analyse(OMevent_mask event_mask, int seq_num)
{
  // job_data (job_data read req)
  // version (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  message[0] = '\0';
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  op = CRYSTAL::wavefn_calc::create("", false, false, false, false, false, "",
				    job, (int)version, message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.SCF.analyse", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Wavefn::extract(OMevent_mask event_mask, int seq_num)
{
  // run (OMXint read req notify)
  // version (OMXint read req)
  // filename (OMXstr read req)
  // new_view (OMXint read req)
  // job_data (read req)
  CCP3::show_as_busy();
  char *data_file = (char *) filename;
  char message[256];
  message[0] = '\0';
  DLV::operation *op = 0;
  OMobj_id id = OMfind_str_subobj(OMinst_obj,
				  "DLV.Display_Objs.default_data.bond_all",
				  OM_OBJ_RD);
  int set_bonds = 0;
  OMget_int_val(id, &set_bonds);
  int ok = OM_STAT_SUCCESS;
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  op = CRYSTAL::wavefn_calc::create(data_file, ((bool)(binary_wvfn == 1)),
				    ((bool)(new_view == 1)), true,
				    (set_bonds == 1), ((bool)(tddft_also == 1)),
				    (char*)tddft_file, job,
				    (int) version, message, 256);
  //if (op != 0)
  //ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Wavefn.analyse", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Run_File::execute(OMevent_mask event_mask,
					   int seq_num)
{
  // filename (OMXstr read req)
  // strname (OMXstr read)
  // job_data (read req)
  // use_mpp (OMXint read req)
  CCP3::show_as_busy();
  char *input_file = (char *) filename;
  char *str_file = (char *) strname;
  char message[256];
  message[0] = '\0';
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  bool use_structure = strname.valid_obj();
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  op = CRYSTAL::run_calc::create(input_file, str_file, use_structure, job,
				 (bool)(((int)use_mpp) == 1), message, 256);
  if (op != 0)
    ok = CCP3::render_in_view(op, false);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Input.run", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Run_SCF::close_excess_views(OMevent_mask event_mask, 
						     int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  if((int)data.task == 3 && (int)data.neb.end_model > -1){  //NEB
    DLV::operation *op;
    op =  CRYSTAL::neb_calc_v8_final::get_nebop2();
    if(op != 0)
      CCP3::delete_3Dview(op, false);
    if(data.neb.make91){
      for (int i=0;i<data.neb.nimages;i++){
	op =  CRYSTAL::neb_calc_v8_final::get_neb_image_op(i);
	if(op != 0)
	  CCP3::delete_3Dview(op, false);	  
      }
    }
    CCP3::select_3Dview(DLV::operation::get_editing());
  } 
  return ok;
}

int CCP3_CRYSTAL_Modules_Run_SCF::close_excess_views2(OMevent_mask event_mask, 
						      int seq_num)
{
  return close_excess_views(event_mask, seq_num);
}

int CCP3_CRYSTAL_Modules_Run_SCF::create(OMevent_mask event_mask, int seq_num)
{
  // make (OMXint read req)
  // data (Grid3D_Data read req)
  // version (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)make == 1) {
    CCP3::show_as_busy();
    DLV::operation *op = 0;
    char message[256];
    message[0] = '\0';
    op = CRYSTAL::scf_calc::create((int)data.task, (int)version, message, 256);
    if (op != 0) {
      if ((int)data.task == 1 or (int)data.task == 3)
	ok = CCP3::render_in_view(op, false);
      else
	ok = CCP3::update_op_in_3Dview(op);
    }
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.SCF.create", message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_CRYSTAL_Modules_Run_SCF::inherit_basis(OMevent_mask event_mask,
						int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  CCP3::show_as_busy();
  if (CRYSTAL::scf_calc::copy_basis(message, 256))
    ; // ?
  else
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.SCF.basis", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Run_SCF::add_basis(OMevent_mask event_mask,
					    int seq_num)
{
  // file (OMXstr read req notify)
  // use_default (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  char *filename = (char *) file;
  bool set_def = ((bool)(use_default == 1));
  CCP3::show_as_busy();
  if (CRYSTAL::scf_calc::add_basis(filename, set_def, message, 256))
    ; // ?
  else
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.SCF.basis", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Run_SCF::stop(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::cancel_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.SCF.cancel",
			   "Problem canceling SCF calc");
  else {
    bool reset = ((int)data.task == 1 || (int)data.task == 3);
    ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					   false, reset);
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Run_SCF::execute(OMevent_mask event_mask, int seq_num)
{
  // data (SCFParams read req)
  // version (OMXint read req)
  // job_data (read req)
  // use_mpp (OMXint read req)
  CCP3::show_as_busy();
  CRYSTAL::Hamiltonian hamilt((int) data.Hamiltonian.Htype,
			      (int) data.Hamiltonian.Hoptions,
			      ((bool) (data.Hamiltonian.use_spins == 1)),
			      (int) data.Hamiltonian.functional,
			      (int) data.Hamiltonian.correlation,
			      (int) data.Hamiltonian.exchange,
			      (int) data.Hamiltonian.aux_basis,
			      ((bool) (data.Hamiltonian.hybrid == 1)),
			      (int) data.Hamiltonian.mixing,
			      (int) data.Hamiltonian.dft_grids);
  CRYSTAL::Basis basis((int) data.Basis.basis);
  CRYSTAL::Kpoints kpoints((int) data.KPoints.is1, (int) data.KPoints.is2,
			   (int) data.KPoints.is3, (int) data.KPoints.isp,
			   ((bool) (data.KPoints.asym == 1)));
  CRYSTAL::Tolerances tols((int) data.Tolerances.itol1,
			   (int) data.Tolerances.itol2,
			   (int) data.Tolerances.itol3,
			   (int) data.Tolerances.itol4,
			   (int) data.Tolerances.itol5,
			   (int) data.Tolerances.energy,
			   (int) data.Tolerances.eigenval,
			   (int) data.Tolerances.deltap,
			   (int) data.Tolerances.method);
  CRYSTAL::Convergence conv((int) data.Convergence.method,
			    (int) data.Convergence.mixing,
			    (int) data.Convergence.maxcycles,
			    ((bool) (data.Convergence.use_levshift == 1)),
			    (int) data.Convergence.levshift,
			    ((bool) (data.Convergence.lock_levshift == 1)),
			    ((bool) (data.Convergence.use_smearing == 1)),
			    (float) data.Convergence.fermi_smear,
			    ((bool) (data.Hamiltonian.Hoptions > 0)),
			    ((bool) (data.Convergence.spinlock == 1)),
			    (int) data.Convergence.spin,
			    (int) data.Convergence.spin_cycles);
  CRYSTAL::Print print(((bool) (data.Print.calc_mulliken == 1)),
		       ((bool) (data.Print.calc_exchange == 1)));
  CRYSTAL::Joboptions job(((bool) (data.Job.direct == 1)),
			  ((bool) (data.Job.mondirect == 1)),
			  false, //((bool) (data.Job.semidirect == 1)),
			  0, //(int) data.Job.nbuffers,
			  ((bool) (data.Job.deltap == 1)),
			  (int) data.Job.dp_tol, (int) data.Job.biesplit,
			  (int) data.Job.monsplit,
			  ((bool) (data.Job.gradcalc == 1)),
			  ((bool) (data.Job.restart == 1)),
			  (char *) data.Job.fock_file);
  CRYSTAL::Optimise opt((int) data.Optimize.optimiser,
			(int) data.Optimize.opttype,
			(int) data.Optimize.etol, (float) data.Optimize.xtol,
			(float) data.Optimize.gtol,
			(float) data.Optimize.iscale, 
			(int) data.Optimize.dlf_type, 
			(int) data.Optimize.dlf_sstype, 
			(int) data.Optimize.dlf_initpop, 
			(int) data.Optimize.dlf_pop, 
			(float) data.Optimize.dlf_radius, 
			(float) data.Optimize.dlf_minradius, 
			(float) data.Optimize.dlf_contractradius, 
			(int) data.Optimize.dlf_cycles, 
			(int) data.Optimize.dlf_nsaves, 
			(float) data.Optimize.dlf_mutation, 
			(float) data.Optimize.dlf_death,
			(float) data.Optimize.dlf_scalef, 
			(int) data.Optimize.dlf_reset);
  int (*cell)[3] = 0;
  if ((int)data.task == 2 and ((int)data.phonon.dispersion == 1))
    cell = (int (*)[3]) data.phonon.cell.ret_array_ptr(OM_GET_ARRAY_RD);
  CRYSTAL::Phonon phonon((float) data.phonon.step, (int) data.phonon.deriv,
			 ((bool) (data.phonon.intensity == 1)),
			 ((bool) (data.phonon.dispersion == 1)), cell);
  if (cell != 0)
    data.phonon.cell.free_array(cell);
  int *map =0;
  if((int)data.task == 3)
    map = (int *)data.neb.mapping.ret_array_ptr(OM_GET_ARRAY_RD);
  CRYSTAL::Neb neb((int) data.neb.nimages, (int) data.neb.iters, 
		   (int) data.neb.climb, (float) data.neb.start_ene, 
		   (float) data.neb.final_ene, (int) data.neb.separation_int, 
		   (int) data.neb.optimiser_int, (int) data.neb.convergence_int,
		   (float) data.neb.tol_energy, (float) data.neb.tol_step,
		   (float) data.neb.tol_grad, (bool) data.neb.symmpath, 
		   (bool) data.neb.usestrsym, (bool) data.neb.adapt,
		   (bool) data.neb.restart, (char *) data.neb.restart_file,
		   (char *) data.neb.images_dir,
                   (int) data.neb.end_model, (int *) map, 
		   (int) data.neb.natoms,
		   (bool) data.neb.nocalc);
  if((int)data.task == 3)
    data.neb.mapping.free_array(map);
  DLV::job_setup_data jd((char *) job_data.hostname,
			 (char *) job_data.work_dir,
			 (char *) job_data.scratch_dir, (int)job_data.nprocs,
			 (int)job_data.nnodes, (int)job_data.hours,
			 (int)job_data.memory_gb, (char *)job_data.account_id,
			 (char *) job_data.queue, (bool) job_data.is_parallel);
  CRYSTAL::CPHF cphf((int)data.cphf.mixing, (int)data.cphf.cycles,
		     (int)data.cphf.alpha, (int)data.cphf.udik);
  CRYSTAL::TDDFT tddft((bool)((int)data.tddft.do_scf == 1),
		       (bool)((int)data.tddft.tamm_dancoff == 1),
		       (bool)((int)data.tddft.calc_jdos == 1),
		       (bool)((int)data.tddft.calc_pes == 1),
		       (bool)((int)data.tddft.fastresponse == 1),
		       (int)data.tddft.diag, (int)data.tddft.davidson_state,
		       (int)data.tddft.davidson_niters,
		       (int)data.tddft.davidson_space,
		       (float)data.tddft.davidson_conv);
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  if (CRYSTAL::scf_calc::calculate((((int)data.analyse) == 1),
				   hamilt, basis, kpoints, tols, conv, print,
				   job, opt, phonon, neb, cphf, tddft,
				   (int)version, jd,
				   ((bool)(data.user_calc) == 1),
				   (char *)data.user_dir,
				   (bool)(((int)use_mpp) == 1), message, 256))
    ; // ?
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.SCF.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Grid3D::calc(OMevent_mask event_mask, int seq_num)
{
  // data (Grid3D_Data read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  op = CRYSTAL::grid_3d_calc::create(((bool)(data.calc_charge == 1)),
				     ((bool)(data.calc_potential == 1)),
				     (int) data.npoints,
				     (int) data.non_periodic,
				     (int) data.convcell,
				     (int) data.pot_itol,
				     (double) data.x_min,
				     (double) data.x_max,
				     (double) data.y_min,
				     (double) data.y_max,
				     (double) data.z_min,
				     (double) data.z_max, dm, newk,
				     (int)version, job,
				     ((bool)(data.user_calc == 1)),
				     (char *)data.user_dir, message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Grid3D.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_CDensity_2D::calc(OMevent_mask event_mask,
					   int seq_num)
{
  // data (CD2D_params read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  op = CRYSTAL::charge_d_calc::create((int) data.npoints,
				      (int) data.selection, dm, newk,
				      (int)version, job,
				      ((bool)(data.user_calc == 1)),
				      (char *)data.user_dir, message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.ChargeDensity2D.calc",
			   message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Potential_2D::calc(OMevent_mask event_mask,
					    int seq_num)
{
  // data (POT_params read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  op = CRYSTAL::potential_calc::create((int) data.npoints,
				       (int) data.selection,
				       (int) data.multipoles,
				       (int) data.tolerance, dm, newk,
				       (int)version, job,
				       ((bool)(data.user_calc == 1)),
				       (char *)data.user_dir, message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Potential2D.calc",
			   message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_density_of_states::create(OMevent_mask event_mask,
						   int seq_num)
{
  // make (OMXint read req)
  // version (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)make == 1) {
    CCP3::show_as_busy();
    DLV::operation *op = 0;
    char message[256];
    message[0] = '\0';
    op = CRYSTAL::dos_calc::create((int)version, message, 256);
    if (op != 0)
      ok = CCP3::update_op_in_3Dview(op);
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.DOS.create", message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_CRYSTAL_Modules_density_of_states::init_proj(OMevent_mask event_mask,
						      int seq_num)
{
  // init (OMXint read req)
  // labels (OMXstr_array write)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  if (CRYSTAL::dos_calc::reset_projections((int) data.projection,
					   message, 256)) {
    OMset_array_size(data.labels.obj_id(), 0);
    OMset_array_size(labels.obj_id(), 0);
  } else
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.DOS.projections", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_density_of_states::list_orbitals(OMevent_mask 
							  event_mask,
							  int seq_num)
{
  // atoms (OMXint read req notify)
  // states (OMXint read req notify)
  // data (DOS_params read)
  // labels (OMXstr_array write)

  int ok = OM_STAT_SUCCESS;
  if ((int)data.projection == 2 and (int)make == 1 and (int)nselections > 0) {
    CCP3::show_as_busy();
    char message[256];
    message[0] = '\0';
    int n = 0;
    DLV::string *names = 0;
    bool all_atoms = (bool)(((int)data.project_all) == 1);
    bool expand_atoms = (bool)(((int)data.expand_atoms) == 1);
    bool expand_states = (bool)(((int)data.expand_states) == 1);
    if (CRYSTAL::dos_calc::list_orbitals(n, names, all_atoms, expand_atoms,
					 expand_states, message, 256)) {
      OMset_array_size(labels.obj_id(), n);
      for (int i = 0; i < n; i++)
	OMset_str_array_val(labels.obj_id(), i, names[i].c_str()); 
      delete [] names;
    } else
      ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.DOS.projections",
			     message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_CRYSTAL_Modules_density_of_states::add_proj(OMevent_mask event_mask,
						     int seq_num)
{
  // add (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  int n = 0;
  DLV::string name;
  bool all_atoms = (bool)(((int)data.project_all) == 1);
  bool expand_atoms = (bool)(((int)data.expand_atoms) == 1);
  bool expand_states = (bool)(((int)data.expand_states) == 1);
  int *orbitals = 0;
  xp_long osize = 0;
  if ((int)data.projection == 2) {
    data.orbitals.get_array_size(&osize);
    if (osize == 0)
      return CCP3::return_info(DLV_WARNING, "CCP3.CRYSTAL.DOS.projections",
			       "No orbitals selected");
    else
      orbitals = (int *)data.orbitals.ret_array_ptr(OM_GET_ARRAY_RD);
  }
  CCP3::show_as_busy();
  if (CRYSTAL::dos_calc::add_projection(n, name, orbitals, osize,
					all_atoms, expand_atoms, expand_states,
					message, 256)) {
    OMset_array_size(data.labels.obj_id(), n);
    OMset_str_array_val(data.labels.obj_id(), n - 1, name.c_str()); 
  } else
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.DOS.projections", message);
  if (orbitals != 0)
    data.orbitals.free_array(orbitals);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_density_of_states::stop(OMevent_mask event_mask,
						 int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  //if ((int)cancel == 1) {
  CCP3::show_as_busy();
  if (DLV::operation::cancel_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.DOS.cancel",
			   "Problem canceling DOS calc");
  else
    ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					   false, false);
  CCP3::show_as_idle();
  //}
  return ok;
}

int CCP3_CRYSTAL_Modules_density_of_states::calc(OMevent_mask event_mask,
						 int seq_num)
{
  // data (DOS_params read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  if (CRYSTAL::dos_calc::calculate((int) data.npoints, (int) data.npoly,
				   (int) data.bmin, (int) data.bmax,
				   (float) data.emin, (float) data.emax,
				   (bool)(data.energy_limits == 1), dm, newk,
				   (int)version, job,
				   ((bool)(data.user_calc == 1)),
				   (char *)data.user_dir, message, 256))
    ; //ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.DOS.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_band_structure::calc(OMevent_mask event_mask,
					      int seq_num)
{
  // data (BandParams read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  DLV::operation *op = CRYSTAL::band_calc::create((int) data.npoints,
						  (int) data.bmin,
						  (int) data.bmax,
						  (char *) data.path,
						  dm, newk, (int) version,
						  job,
						  (bool)(data.user_calc == 1),
						  (char *)data.user_dir,
						  message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Band.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_band_structure::show(OMevent_mask event_mask,
					      int seq_num)
{
  // data (BandParams read req)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  DLV::operation *op = CRYSTAL::band_calc::show_path((char *) data.path,
						     message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op, true);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Band.show_path", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_bands_and_dos::create(OMevent_mask event_mask,
					       int seq_num)
{
  // version (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)make == 1) {
    CCP3::show_as_busy();
    DLV::operation *op = 0;
    char message[256];
    message[0] = '\0';
    op = CRYSTAL::band_and_dos::create((int)version, message, 256);
    if (op != 0)
      ok = CCP3::update_op_in_3Dview(op);
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Band_DOS.create",
			     message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_CRYSTAL_Modules_bands_and_dos::init_proj(OMevent_mask event_mask,
						  int seq_num)
{
  // init (OMXint read req notify)
  // Ddata (DOS_params write)
  // labels (OMXstr_array write)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  if (CRYSTAL::band_and_dos::reset_projections((int) Ddata.projection,
					       message, 256)) {
    OMset_array_size(Ddata.labels.obj_id(), 0);
    OMset_array_size(labels.obj_id(), 0);
  } else
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Band_DOS.projections",
			   message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_bands_and_dos::list_orbitals(OMevent_mask event_mask,
						      int seq_num)
{
  // atoms (OMXint read req notify)
  // states (OMXint read req notify)
  // Ddata (DOS_params read)
  // labels (OMXstr_array write)
  int ok = OM_STAT_SUCCESS;
  if ((int)Ddata.projection == 2 and (int)make == 1 and (int)nselections > 0) {
    CCP3::show_as_busy();
    char message[256];
    message[0] = '\0';
    int n = 0;
    DLV::string *names = 0;
    bool all_atoms = (bool)(((int)Ddata.project_all) == 1);
    bool expand_atoms = (bool)(((int)Ddata.expand_atoms) == 1);
    bool expand_states = (bool)(((int)Ddata.expand_states) == 1);
    if (CRYSTAL::band_and_dos::list_orbitals(n, names, all_atoms, expand_atoms,
					     expand_states, message, 256)) {
      OMset_array_size(labels.obj_id(), n);
      for (int i = 0; i < n; i++)
	OMset_str_array_val(labels.obj_id(), i, names[i].c_str()); 
      delete [] names;
    } else
      ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Band_DOS.projections",
			     message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_CRYSTAL_Modules_bands_and_dos::add_proj(OMevent_mask event_mask,
						 int seq_num)
{
  // add (OMXint read req notify)
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  int n = 0;
  DLV::string name;
  bool all_atoms = (bool)(((int)Ddata.project_all) == 1);
  bool expand_atoms = (bool)(((int)Ddata.expand_atoms) == 1);
  bool expand_states = (bool)(((int)Ddata.expand_states) == 1);
  int *orbitals = 0;
  xp_long osize = 0;
  if ((int)Ddata.projection == 2) {
    Ddata.orbitals.get_array_size(&osize);
    if (osize == 0)
      return CCP3::return_info(DLV_WARNING, "CCP3.CRYSTAL.DOS.projections",
			       "No orbitals selected");
    else
      orbitals = (int *)Ddata.orbitals.ret_array_ptr(OM_GET_ARRAY_RD);
  }
  CCP3::show_as_busy();
  if (CRYSTAL::band_and_dos::add_projection(n, name, orbitals, osize,
					    all_atoms, expand_atoms,
					    expand_states, message, 256)) {
    OMset_array_size(Ddata.labels.obj_id(), n);
    OMset_str_array_val(Ddata.labels.obj_id(), n - 1, name.c_str()); 
  } else
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Band_DOS.projections",
			   message);
  if (orbitals != 0)
    Ddata.orbitals.free_array(orbitals);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_bands_and_dos::stop(OMevent_mask event_mask,
					     int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  //if ((int)cancel == 1) {
  CCP3::show_as_busy();
  if (DLV::operation::cancel_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Band_DOS.cancel",
			   "Problem canceling Band+DOS calc");
  else
    ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					   false, false);
  CCP3::show_as_idle();
  //}
  return ok;
}

int CCP3_CRYSTAL_Modules_bands_and_dos::calc(OMevent_mask event_mask,
                                             int seq_num)
{
  // Bdata (BandParams read req)
  // Ddata (DOS_params read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
                             (float) DMparams.emax, (int) DMparams.bmin,
                             (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
                     (int) NewK.isp, ((int)NewK.calc) == 1,
                     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
                     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
                          (char *) job_data.work_dir,
                          (char *) job_data.scratch_dir, (int)job_data.nprocs,
                          (int)job_data.nnodes, (int)job_data.hours,
                          (int)job_data.memory_gb, (char *)job_data.account_id,
                          (char *)job_data.queue, (bool)job_data.is_parallel);
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  if (CRYSTAL::band_and_dos::calculate((int) Ddata.npoints, (int) Ddata.npoly,
				       (int) Bdata.bmin, (int) Bdata.bmax,
				       (int) Bdata.npoints, (char *)Bdata.path,
				       dm, newk, (int) version, job,
				       ((bool)(Ddata.user_calc == 1)),
				       (char *)Ddata.user_dir, message, 256))
    ; //ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Band_DOS.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_mulliken::calc(OMevent_mask event_mask, int seq_num)
{
  // data (OMXint read req)
  // rotate (OMXint read req)
  // user_data (user_job_data read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // job_data (read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  DLV::operation 
    *op = CRYSTAL::mulliken_calc::create((int) data, (int)rotate, dm, newk,
					 (int)version, job,
					 (bool)(user_data.user_calc == 1),
					 (char *)user_data.user_dir,
					 message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Mulliken.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_dlv3D::calc(OMevent_mask event_mask, int seq_num)
{
  // data (dlv_3D_data read req)
  // NewK (NewK_params read req)
  // job_data (job_data read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  op = CRYSTAL::dlv_3d_calc::create(((bool)(data.calc_charge == 1)),
				    ((bool)(data.calc_spin == 1)),
				    ((bool)(data.calc_pot == 1)),
				    ((bool)(data.calc_field == 1)),
				    ((bool)(data.calc_density == 1)),
				    (int) data.npoints, (int) data.box,
				    (int) data.pol_tol,
				    (int) data.pot_tol,
				    (int) data.min_band,
				    (int) data.max_band, (float) data.spacing,
				    (bool) (data.use_points == 1),
				    ((bool)(data.calc_ldr == 1)),
				    ((bool)(data.calc_particle == 1)),
				    ((bool)(data.calc_hole == 1)),
				    ((bool)(data.calc_overlap == 1)),
				    (int) data.min_excite,
				    (int) data.max_excite,
				    dm, newk, (int)version, job,
				    ((bool)(data.user_calc == 1)),
				    (char *)data.user_dir, message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.DLV3D.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_bloch::calc(OMevent_mask event_mask, int seq_num)
{
  // data (bloch_data read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (job_data read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk(0, 0, 0, 0, ((int)NewK.calc) == 1, 0, 0,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  xp_long nkp = 0;
  (void) data.kpoints.get_array_size(&nkp);
  int *kp = (int *) data.kpoints.ret_array_ptr(OM_GET_ARRAY_RD);
  op = CRYSTAL::bloch_calc::create((int) data.npoints, (int) data.grid,
				   (int) data.min_band,
				   (int) data.max_band, nkp, kp,
				   dm, newk, (int)version, job,
				   ((bool)(data.user_calc == 1)),
				   (char *)data.user_dir, message, 256);
  data.kpoints.free_array(kp);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Bloch.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_wannier3D::calc(OMevent_mask event_mask, int seq_num)
{
  // data (wann3D_data read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (job_data read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  op = CRYSTAL::wannier3D_calc::create((int) data.npoints, (int)(data.grid),
				       (int) data.min_band,
				       (int) data.max_band,
				       ((bool)(data.symmetrize == 1)),
				       (int) data.gstars, (float) data.spacing,
				       (bool) (data.use_points == 1),
				       (float) data.region,
				       (bool) (data.gen_grid == 1),
				       dm, newk, (int)version, job,
				       ((bool)(data.user_calc == 1)),
				       (char *)data.user_dir, message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Wannier3D.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_brillouin::calc(OMevent_mask event_mask, int seq_num)
{
  // data (brillouin_data read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (job_data read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  op = CRYSTAL::ek_3d_calc::create((int) data.min_band, (int) data.max_band,
				   (int) data.shrink, dm, newk, (int)version,
				   job, ((bool)(data.user_calc == 1)),
				   (char *)data.user_dir, message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op, true);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Brillouin.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_wannier2D::calc(OMevent_mask event_mask, int seq_num)
{
  // data (wann3D_data read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (job_data read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  op = CRYSTAL::wannier2D_calc::create((int) data.npoints, (int)(data.grid),
				       (int) data.min_band,
				       (int) data.max_band,
				       ((bool)(data.symmetrize == 1)),
				       (int) data.gstars,
				       dm, newk, (int)version, job,
				       ((bool)(data.user_calc == 1)),
				       (char *)data.user_dir, message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Wannier2D.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_nebcreate::start(OMevent_mask event_mask, int seq_num)
{
  // trigger+notify+read+req
  // natoms+read+req
  // mapping+write+req
  // dist+read+req
  // hide_atoms+read+req
  // end_model+read+req
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    CRYSTAL::Hamiltonian hamilt((int) data.Hamiltonian.Htype,
				(int) data.Hamiltonian.Hoptions,
				((bool) (data.Hamiltonian.use_spins == 1)),
				(int) data.Hamiltonian.functional,
				(int) data.Hamiltonian.correlation,
				(int) data.Hamiltonian.exchange,
				(int) data.Hamiltonian.aux_basis,
				((bool) (data.Hamiltonian.hybrid == 1)),
				(int) data.Hamiltonian.mixing,
				(int) data.Hamiltonian.dft_grids);
    CRYSTAL::Basis basis((int) data.Basis.basis);
    CRYSTAL::Kpoints kpoints((int) data.KPoints.is1, (int) data.KPoints.is2,
			     (int) data.KPoints.is3, (int) data.KPoints.isp,
			     ((bool) (data.KPoints.asym == 1)));
    CRYSTAL::Tolerances tols((int) data.Tolerances.itol1,
			     (int) data.Tolerances.itol2,
			     (int) data.Tolerances.itol3,
			     (int) data.Tolerances.itol4,
			     (int) data.Tolerances.itol5,
			     (int) data.Tolerances.energy,
			     (int) data.Tolerances.eigenval,
			     (int) data.Tolerances.deltap,
			     (int) data.Tolerances.method);
    CRYSTAL::Convergence conv((int) data.Convergence.method,
			      (int) data.Convergence.mixing,
			      (int) data.Convergence.maxcycles,
			      ((bool) (data.Convergence.use_levshift == 1)),
			      (int) data.Convergence.levshift,
			      ((bool) (data.Convergence.lock_levshift == 1)),
			      ((bool) (data.Convergence.use_smearing == 1)),
			      (float) data.Convergence.fermi_smear,
			      ((bool) (data.Hamiltonian.Hoptions > 0)),
			      ((bool) (data.Convergence.spinlock == 1)),
			      (int) data.Convergence.spin,
			      (int) data.Convergence.spin_cycles);
    CRYSTAL::Print print(((bool) (data.Print.calc_mulliken == 1)),
			 ((bool) (data.Print.calc_exchange == 1)));
    CRYSTAL::Joboptions job(((bool) (data.Job.direct == 1)),
			    ((bool) (data.Job.mondirect == 1)),
			    false, 0, 
			    ((bool) (data.Job.deltap == 1)),
			    (int) data.Job.dp_tol, (int) data.Job.biesplit,
			    (int) data.Job.monsplit,
			    ((bool) (data.Job.gradcalc == 1)),
			    ((bool) (data.Job.restart == 1)),
			    (char *) data.Job.fock_file);
    CRYSTAL::Optimise opt((int) data.Optimize.optimiser,
			  (int) data.Optimize.opttype,
			  (int) data.Optimize.etol, (float) data.Optimize.xtol,
			  (float) data.Optimize.gtol,
			  (float) data.Optimize.iscale, 
			  (int) data.Optimize.dlf_type, 
			  (int) data.Optimize.dlf_sstype, 
			  (int) data.Optimize.dlf_initpop, 
			  (int) data.Optimize.dlf_pop, 
			  (float) data.Optimize.dlf_radius, 
			  (float) data.Optimize.dlf_minradius, 
			  (float) data.Optimize.dlf_contractradius, 
			  (int) data.Optimize.dlf_cycles, 
			  (int) data.Optimize.dlf_nsaves, 
			  (float) data.Optimize.dlf_mutation, 
			  (float) data.Optimize.dlf_death,
			  (float) data.Optimize.dlf_scalef, 
			  (int) data.Optimize.dlf_reset);
    CRYSTAL::Phonon phonon((float) data.phonon.step, (int) data.phonon.deriv,
			   ((bool) (data.phonon.intensity == 1)), false, 0);
    int *map = (int *)data.neb.mapping.ret_array_ptr(OM_GET_ARRAY_RD);
    CRYSTAL::Neb neb((int) data.neb.nimages, (int) data.neb.iters, 
		     (int) data.neb.climb, (float) data.neb.start_ene, 
		     (float) data.neb.final_ene, (int) data.neb.separation_int, 
		     (int) data.neb.optimiser_int, 
		     (int) data.neb.convergence_int, 
		     (float) data.neb.tol_energy, (float) data.neb.tol_step,
		     (float) data.neb.tol_grad, (bool) data.neb.symmpath, 
		     (bool) data.neb.usestrsym, (bool) data.neb.adapt,
		     (bool) data.neb.restart, (char *) data.neb.restart_file,
		     (char *) data.neb.images_dir,
		     (int) data.neb.end_model, (int *) map, 
		     (int) data.neb.natoms,
		     (bool) data.neb.nocalc);
    data.neb.mapping.free_array(map);
    DLV::job_setup_data jd((char *) job_data.hostname,
			   (char *) job_data.work_dir,
			   (char *) job_data.scratch_dir, (int)job_data.nprocs,
			   (int)job_data.nnodes, (int)job_data.hours,
			   (int)job_data.memory_gb, (char *)job_data.account_id,
			   (char *) job_data.queue, 
			   (bool) job_data.is_parallel);
    DLV::operation *nebop2 =  CRYSTAL::neb_calc_v8_final::get_nebop2();
    const unsigned int len = 256;
    char work_dir[len];
    char message[len];
    message[0] = '\0';
    CRYSTAL::neb_calc::create_run1((((int)data.analyse) == 1), hamilt, basis, 
				   kpoints, tols, conv, print,
				   job, opt, phonon, neb, jd, 
				   ((bool)(data.user_calc) == 1),
				   (char *)data.user_dir, nebop2, 
				   work_dir, message, len);
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_ERROR, "CCCP3.CRYSTAL.nebcreate.start",
	  		     message);
    else {
      data.neb.make91 = true;  //To_do - specify only if images are altered?
      data.neb.images_dir = work_dir;
      CRYSTAL::neb_calc::set_images_arrays((int) data.neb.nimages, 0);
      CRYSTAL::neb_data::set_neb_editing(DLV::operation::get_editing());
      data.neb.images_button = true; 
    }
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_CRYSTAL_Modules_nebresetim::start(OMevent_mask event_mask, int seq_num)
{
  // trigger+notify+read+req
  // images_button+write
  // nimages+write
  // make91+write
  if ((int)trigger == 1) {
    images_button = 0;
    //    bool no_images = false;
    //    bool no_endstr = true;

    DLV::operation *op;
    for (int i=0; i<nimages; i++){
      op =  CRYSTAL::neb_calc_v8_final::get_neb_image_op(i);
      if(op != 0)
	CCP3::delete_3Dview(op, false);	  
      }
    CCP3::select_3Dview(DLV::operation::get_editing());
    make91 = false;
  }
  return OM_STAT_SUCCESS;
}

int CCP3_CRYSTAL_Modules_nebwrite91::start(OMevent_mask event_mask, int seq_num)
{
  // trigger+notify+read+req,
  // data+read+req,
  // job_data+write+req,
  // make91+read+req
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1 && make91) {
    CCP3::show_as_busy();
    CRYSTAL::Hamiltonian hamilt((int) data.Hamiltonian.Htype,
				(int) data.Hamiltonian.Hoptions,
				((bool) (data.Hamiltonian.use_spins == 1)),
				(int) data.Hamiltonian.functional,
				(int) data.Hamiltonian.correlation,
				(int) data.Hamiltonian.exchange,
				(int) data.Hamiltonian.aux_basis,
				((bool) (data.Hamiltonian.hybrid == 1)),
				(int) data.Hamiltonian.mixing,
				(int) data.Hamiltonian.dft_grids);
    CRYSTAL::Basis basis((int) data.Basis.basis);
    CRYSTAL::Kpoints kpoints((int) data.KPoints.is1, (int) data.KPoints.is2,
			     (int) data.KPoints.is3, (int) data.KPoints.isp,
			     ((bool) (data.KPoints.asym == 1)));
    CRYSTAL::Tolerances tols((int) data.Tolerances.itol1,
			     (int) data.Tolerances.itol2,
			     (int) data.Tolerances.itol3,
			     (int) data.Tolerances.itol4,
			     (int) data.Tolerances.itol5,
			     (int) data.Tolerances.energy,
			     (int) data.Tolerances.eigenval,
			     (int) data.Tolerances.deltap,
			     (int) data.Tolerances.method);
    CRYSTAL::Convergence conv((int) data.Convergence.method,
			      (int) data.Convergence.mixing,
			      (int) data.Convergence.maxcycles,
			      ((bool) (data.Convergence.use_levshift == 1)),
			      (int) data.Convergence.levshift,
			      ((bool) (data.Convergence.lock_levshift == 1)),
			      ((bool) (data.Convergence.use_smearing == 1)),
			      (float) data.Convergence.fermi_smear,
			      ((bool) (data.Hamiltonian.Hoptions > 0)),
			      ((bool) (data.Convergence.spinlock == 1)),
			      (int) data.Convergence.spin,
			      (int) data.Convergence.spin_cycles);
    CRYSTAL::Print print(((bool) (data.Print.calc_mulliken == 1)),
			 ((bool) (data.Print.calc_exchange == 1)));
    CRYSTAL::Joboptions job(((bool) (data.Job.direct == 1)),
			    ((bool) (data.Job.mondirect == 1)),
			    false, 0, 
			    ((bool) (data.Job.deltap == 1)),
			    (int) data.Job.dp_tol, (int) data.Job.biesplit,
			    (int) data.Job.monsplit,
			    ((bool) (data.Job.gradcalc == 1)),
			    ((bool) (data.Job.restart == 1)),
			    (char *) data.Job.fock_file);
    CRYSTAL::Optimise opt((int) data.Optimize.optimiser,
			  (int) data.Optimize.opttype,
			  (int) data.Optimize.etol, (float) data.Optimize.xtol,
			  (float) data.Optimize.gtol,
			  (float) data.Optimize.iscale, 
			  (int) data.Optimize.dlf_type, 
			  (int) data.Optimize.dlf_sstype, 
			  (int) data.Optimize.dlf_initpop, 
			  (int) data.Optimize.dlf_pop, 
			  (float) data.Optimize.dlf_radius, 
			  (float) data.Optimize.dlf_minradius, 
			  (float) data.Optimize.dlf_contractradius, 
			  (int) data.Optimize.dlf_cycles, 
			  (int) data.Optimize.dlf_nsaves, 
			  (float) data.Optimize.dlf_mutation, 
			  (float) data.Optimize.dlf_death,
			  (float) data.Optimize.dlf_scalef, 
			  (int) data.Optimize.dlf_reset);
    CRYSTAL::Phonon phonon((float) data.phonon.step, (int) data.phonon.deriv,
			   ((bool) (data.phonon.intensity == 1)), false, 0);
    int *map = (int *)data.neb.mapping.ret_array_ptr(OM_GET_ARRAY_RD);
    CRYSTAL::Neb neb((int) data.neb.nimages, (int) data.neb.iters, 
		     (int) data.neb.climb, (float) data.neb.start_ene, 
		     (float) data.neb.final_ene, (int) data.neb.separation_int, 
		     (int) data.neb.optimiser_int, 
		     (int) data.neb.convergence_int, 
		     (float) data.neb.tol_energy, (float) data.neb.tol_step,
		     (float) data.neb.tol_grad, (bool) data.neb.symmpath, 
		     (bool) data.neb.usestrsym, (bool) data.neb.adapt,
		     (bool) data.neb.restart, (char *) data.neb.restart_file,
		     (char *) data.neb.images_dir,
		     (int) data.neb.end_model, (int *) map, 
		     (int) data.neb.natoms,
		     (bool) data.neb.nocalc);
    data.neb.mapping.free_array(map);
    DLV::job_setup_data jd((char *) job_data.hostname,
			   (char *) job_data.work_dir,
			   (char *) job_data.scratch_dir, (int)job_data.nprocs,
			   (int)job_data.nnodes, (int)job_data.hours,
			   (int)job_data.memory_gb, (char *)job_data.account_id,
			   (char *) job_data.queue, 
			   (bool) job_data.is_parallel);

    DLV::operation *nebop2 =  CRYSTAL::neb_calc_v8_final::get_nebop2();
    const unsigned int len = 256;
    char work_dir[len];
    char message[len];
    message[0] = '\0';
    CRYSTAL::neb_calc::create_run2((((int)data.analyse) == 1), hamilt, basis, 
				   kpoints, tols, conv, print,
				   job, opt, phonon, neb, jd, 
				   ((bool)(data.user_calc) == 1),
				   (char *)data.user_dir, nebop2,
				   work_dir, message, len);
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_ERROR, "CCCP3.CRYSTAL.nebwrite91.start",
			     message);
    else {
      data.neb.restart = true;
      char restart_dir[256];
      char name[12] = "neb.restart";
      int j = strlen(work_dir);
      if(work_dir[0]=='/')
	strcpy(restart_dir, work_dir);
      else{
	strcpy(restart_dir, "../");
	for(int i=0;i<j;i++)
	  restart_dir[3+i] = work_dir[i];	
	j +=3;
      }
      for(int i=0;i<12;i++)
	restart_dir[j+i] = name[i];
      restart_dir[j+12] = '\0'; 
      data.neb.restart_file = restart_dir;
    }
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_CRYSTAL_Modules_nebtol::update(OMevent_mask event_mask, int seq_num)
{
  // convergence_int+notify+read+req,
  // tol_energy+write,
  // tol_step+write,
  // tol_grad+write   
  int ok = OM_STAT_SUCCESS;
  if (convergence_int==0){
    tol_energy = 0.0001;
    tol_step = 0.0018;
    tol_grad = 0.00045;
  }
  else if(convergence_int==1){
    tol_energy = 0.001;
    tol_step = 0.005;
    tol_grad = 0.005;	
  }
  return ok;
}

int CCP3_CRYSTAL_Modules_list_neb_models::start(OMevent_mask event_mask, 
						int seq_num)
{
  // trigger+notify+read+req,
  // first_model+write,
  //  labels+write
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    DLV::string *names = 0;
    first_model = DLV::operation::get_neb_first_model();
    const int len = 256;
    char message[len];
    int n = DLV::operation::list_neb_models(names, message, len);
    if (n == 0){
      CCP3::return_info(DLV_WARNING,
			"CCP3.CRYSTAL.list_neb_models.start", message);
      OMset_array_size(labels.obj_id(), 0);
    }
    else {
      OMset_array_size(labels.obj_id(), n);
      for (int i = 0; i < n; i++)
	OMset_str_array_val(labels.obj_id(), i, names[i].c_str());
      delete [] names;
    }
    CCP3::show_as_idle();
  }
  return OM_STAT_SUCCESS;
}


int CCP3_CRYSTAL_Modules_nebview::start(OMevent_mask event_mask, int seq_num)
{
  // trigger+notify+read+req,
  // mapping_status+write,
  // natoms+write,
  // mapping+write,
  // first_model+read,
  // end_model+write,
  // updated_end_model+write
  if ((int)trigger == 1) {
    int dummy = 0;
    mapping_status = 0;
    CCP3::show_as_busy();
    if(end_model >= first_model) end_model = end_model + 1;  
    DLV::model *struc1 = DLV::operation::get_current_model();
    updated_end_model = end_model;
    natoms = struc1->get_number_of_asym_atoms();
    mapping.set_array_size(natoms);
    int *map = (int *)mapping.ret_array_ptr(OM_GET_ARRAY_WR);
    mapping_status = CRYSTAL::neb_calc::map_neb_structures(map, natoms, end_model, dummy);
    mapping.free_array(map);
    int *map2 = (int *)mapping.ret_array_ptr(OM_GET_ARRAY_RD);
    CRYSTAL::neb_calc::view_mapping_str1(dummy);
    DLV::operation *op2 = DLV::operation::get_current()->get_neb_op(end_model);
    CCP3::render_in_view(op2, true); 
    CRYSTAL::neb_calc::view_mapping_str2(natoms, end_model, map2,  dummy);
    mapping.free_array(map2);
    CCP3::show_as_idle();
  }
	
  return OM_STAT_SUCCESS;
}

int CCP3_CRYSTAL_Modules_nebreset::start(OMevent_mask event_mask, int seq_num)
{
  // trigger+notify+read+req,
  // end_model+write+req,
  // first_model+write+req,
  // make91+write+req,
  // nimages+read+req,
  // images_dir+write+req
  if ((int)trigger == 1) {
    DLV::operation *op;
    op =  CRYSTAL::neb_calc_v8_final::get_nebop2();
    if(op != 0)
      CCP3::delete_3Dview(op, false);
    if(make91){
      for (int i=0; i<nimages; i++){
	op =  CRYSTAL::neb_calc_v8_final::get_neb_image_op(i);
	if(op != 0)
	  CCP3::delete_3Dview(op, false);	  
      }
    }
    CCP3::select_3Dview(DLV::operation::get_editing());
    end_model = -1;
    first_model = -1;
    make91 = false;
    images_dir ="";
  }
  return OM_STAT_SUCCESS;
}

int CCP3_CRYSTAL_Modules_nebhide::start(OMevent_mask event_mask, int seq_num)
{
  // trigger+notify+read+req
  // dist+read+req,
  // mapping+read+req,
  // end_model+read+req
 CCP3::show_as_busy();
  if(trigger == 0) 
    CRYSTAL::neb_calc::unhide_atoms(end_model);
  else {
    int *map = (int *)mapping.ret_array_ptr(OM_GET_ARRAY_RD);
    CRYSTAL::neb_calc::hide_atoms(end_model, dist, map);
    mapping.free_array(map);
  }
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_CRYSTAL_Modules_nebedit::start(OMevent_mask event_mask, int seq_num)
{
  // trigger+notify+read+req,
  // mapping+write+req,
  // dist+read+req,
  // hide_atoms+read+req,
  // end_model+read+req
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    int *map = (int *)mapping.ret_array_ptr(OM_GET_ARRAY_RW);
    const int mlen = 256;
    char message[mlen];
    bool success = CRYSTAL::neb_calc::edit_atoms(end_model, map, 
						 message, mlen, 0);
    if(!success)
      ok = CCP3::return_info(DLV_ERROR,
			     "CCP3.CRYSTAL.NEB.edit", message);
    else if(success&&hide_atoms){
      CRYSTAL::neb_calc::hide_atoms(end_model, dist, map);
    }
    mapping.free_array(map);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_CRYSTAL_Modules_nebviewimage::start(OMevent_mask event_mask, 
					     int seq_num)
{
  // trigger+notify+read+req,
  // job_data+read+req,
  // images_dir+read+req,
  // image+read+req
    int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    OMobj_id id = OMfind_str_subobj(OMinst_obj,
				    "DLV.Display_Objs.default_data.bond_all",
				    OM_OBJ_RD);
    int set_bonds = 0;
    OMget_int_val(id, &set_bonds);
    DLV::operation *op;
    op = CRYSTAL::neb_calc::get_image_op(image-1, images_dir,
					 (bool)(set_bonds == 1), 0);
    char message[256];
    message[0] = '\0';
    bool new_view = true;
    if (op == 0){
      strncpy(message, "DLV ERROR, CCP3.CRYSTAL.Model.load", 256);
      ok = CCP3::return_info(DLV_ERROR,
			     "CCP3.CRYSTAL.NEB.viewimage", message);
    }  
    else if(!CCP3::select_3Dview(op)) { 
      CCP3::render_in_view(op, new_view);
      CRYSTAL::neb_calc::label_image(op);
    }
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_CRYSTAL_Modules_nebedimage::start(OMevent_mask event_mask, int seq_num)
{
  // trigger+notify+read+req,
  // image+read
  if ((int)trigger == 1) {
    OMobj_id id = OMfind_str_subobj(OMinst_obj,
				    "DLV.Display_Objs.default_data.bond_all",
				    OM_OBJ_RD);
    int set_bonds = 0;
    OMget_int_val(id, &set_bonds);
    DLV::operation *op;
    op = CRYSTAL::neb_calc::get_image_op(image-1, "dummy", (set_bonds == 1), 0);
    if(!CCP3::select_3Dview(op))   { 
      bool new_view = true;
      CCP3::render_in_view(op, new_view);
      CRYSTAL::neb_calc::label_image(op);
    } 
    CCP3::freeze_other_3Dviews(op);
  }
  return  OM_STAT_SUCCESS;
}

int CCP3_CRYSTAL_Modules_edit_model::finish(OMevent_mask event_mask,
					    int seq_num)
{
  // trigger_ok+req+notify,
  // image+read+req
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  CRYSTAL::neb_calc::reset_editing();
  CRYSTAL::neb_calc::save_image_str(image-1, DLV::operation::get_current(), 0);
  CCP3::activate_3Dviews();
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_edit_model::cancel(OMevent_mask event_mask,
					    int seq_num)
{
  // trigger_cancel+req+notify,
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  DLV::operation::detach_pending();
  CCP3::render_in_view(DLV::operation::get_current(), false);
  if (DLV::operation::delete_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.cancel",
			   "Problem canceling new model");
  CRYSTAL::neb_calc::reset_editing();
  CCP3::activate_3Dviews();
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_move_atom::accept(OMevent_mask event_mask, int seq_num)
{
  // trigger+notify+req,
  // image+read+req
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  CRYSTAL::neb_calc::reset_editing();
  CRYSTAL::neb_calc::save_image_str(image-1, DLV::operation::get_current(), 0);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_TDDFT3D::calc(OMevent_mask event_mask, int seq_num)
{
  // data (Grid3D_Data read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (job_data read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  op = CRYSTAL::grid_3d_calc::tddft((int)data.tddft_prop,
				    (int)data.excitation,
				    (int) data.npoints,
				    (int) data.non_periodic,
				    (int) data.convcell,
				    (int) data.pot_itol,
				    (double) data.x_min,
				    (double) data.x_max,
				    (double) data.y_min,
				    (double) data.y_max,
				    (double) data.z_min,
				    (double) data.z_max, dm, newk,
				    (int)version, job,
				    ((bool)(data.user_calc == 1)),
				    (char *)data.user_dir, message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.TD-DFT3D.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CRYSTAL_Modules_Topond3D::calc(OMevent_mask event_mask, int seq_num)
{
  // data (Topond3D_Data read req)
  // DMparams (Density_params read req)
  // NewK (NewK_params read req)
  // version (OMXint read req)
  // job_data (job_data read req)
  CCP3::show_as_busy();
  CRYSTAL::Density_Matrix dm((int) DMparams.projection, (float) DMparams.emin,
			     (float) DMparams.emax, (int) DMparams.bmin,
			     (int) DMparams.bmax);
  CRYSTAL::NewK newk((int) NewK.is1, (int) NewK.is2, (int) NewK.is3,
		     (int) NewK.isp, ((int)NewK.calc) == 1,
		     ((int)NewK.new_shrink) == 1, ((int)NewK.asym_shrink) == 1,
		     ((int)NewK.calc_fermi) == 1);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  op = CRYSTAL::topond_3d_calc::create(((bool)(data.calc_electrons == 1)),
				       ((bool)(data.calc_spin == 1)),
				       ((bool)(data.calc_laplacian == 1)),
				       ((bool)(data.calc_negative == 1)),
				       ((bool)(data.calc_gradient == 1)),
				       ((bool)(data.calc_hamiltonian == 1)),
				       ((bool)(data.calc_lagrangian == 1)),
				       ((bool)(data.calc_virial == 1)),
				       ((int)data.calc_elf),
				       (int) data.npoints,
				       (double) data.x_min,
				       (double) data.x_max,
				       (double) data.y_min,
				       (double) data.y_max,
				       (double) data.z_min,
				       (double) data.z_max, dm, newk,
				       (int)version, job,
				       ((bool)(data.user_calc == 1)),
				       (char *)data.user_dir, message, 256);
  if (op != 0)
    ok = CCP3::update_op_in_3Dview(op);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CRYSTAL.Topond3D.calc", message);
  CCP3::show_as_idle();
  return ok;
}
