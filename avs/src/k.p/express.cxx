
#include <cstdio>
#include "avs/src/k.p/str_gen.hxx"
#include "avs/src/k.p/load_gen.hxx"
#include "avs/src/k.p/mak_gen.hxx"
#include "avs/src/k.p/axis_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "src/dlv/op_model.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "src/dlv/op_admin.hxx"
#include "src/dlv/data_objs.hxx"
#include "src/dlv/data_vol.hxx"
#include "src/k.p/calcs.hxx"
#include "src/k.p/struct.hxx"
#include "src/k.p/create.hxx"
#include "src/k.p/grid3d.hxx"
#include "src/k.p/axis.hxx"

int CCP3_K_P_Modules_load_structure::load(OMevent_mask event_mask, int seq_num)
{
  // data (StructureData read req)
  char *data_file = (char *) data.file;
  char *name = (char *) data.name;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = K_P::load_structure::create(name, data_file, message, 256);
  int ok;
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.k.p.Model.load", message);
  else
    ok = CCP3::render_in_view(op, ((bool)(data.new_view == 1)));
  CCP3::show_as_idle();
  return ok;
}

int CCP3_K_P_Modules_load_data::load(OMevent_mask event_mask, int seq_num)
{
  // filename (OMXstr read req)
  char *data_file = (char *) data.filename;
  DLV::int_g N_x = data.N_x; 
  DLV::int_g N_y = data.N_y;
  DLV::int_g N_z =  data.N_z;
  DLV::real_g L_x = data.L_x;
  DLV::real_g L_y = data.L_y;
  DLV::real_g L_z = data.L_z;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;
  CCP3::show_as_busy();
  op = K_P::load_3d_data::create(data_file, N_x, N_y, N_z, L_x, L_y, L_z, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.k.p.Density.Load", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_K_P_Modules_create_structure::create(OMevent_mask event_mask, int seq_num)
{
   // data (InputData read req)
  int shape = data.shape;
  float base =  data.base;
  float height = data.height;
  float c = data.c;
  bool WL = data.WL;
  int n = data.n;
  float WLthickness = data.WLthickness;
  bool lat = data.lat;
  int dim = data.dim;
  int LatType = data.LatType;
  float a1 = data.a1;
  float a2 = data.a2;
  float a3 = data.a3;
  int dir = data.dir;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = K_P::create_structure::create(shape, base, height, c, WL, n, WLthickness,lat, dim, LatType, a1, a2, a3, dir,  message, 256);
  int ok;
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.k.p.Model.create", message);
  else
    //ok = CCP3::render_in_view(op, ((bool)(data.new_view == 1))); Fix this!!!
    ok = CCP3::render_in_view(op, false);
  CCP3::show_as_idle();
  return ok;

}

int CCP3_K_P_Modules_axis::axis(OMevent_mask event_mask, int seq_num)
{

  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = K_P::axis::create(message, 256);
  int ok;
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.k.p.Model.axis", message);
  else
    //ok = CCP3::render_in_view(op, ((bool)(data.new_view == 1))); Fix this!!!
    ok = CCP3::render_in_view(op, false);
  CCP3::show_as_idle();
  return ok;
}
