
#include "avs/src/onetep/str_gen.hxx"
#include "avs/src/onetep/save_gen.hxx"
#include "avs/src/onetep/load_gen.hxx"
#include "avs/src/onetep/scf_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "src/dlv/calculation.hxx"
#include "src/dlv/op_model.hxx"
#include "src/dlv/op_admin.hxx"
#include "src/dlv/data_objs.hxx"
#include "src/dlv/data_plots.hxx"
#include "src/dlv/data_vol.hxx"
#include "src/dlv/op_data.hxx"
#include "src/dlv/job_setup.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "src/onetep/calcs.hxx"
#include "src/onetep/struct.hxx"
#include "src/onetep/save.hxx"
#include "src/onetep/dos.hxx"
#include "src/onetep/grid3d.hxx"
#include "src/onetep/scf.hxx"

int CCP3_ONETEP_Modules_load_structure::load(OMevent_mask event_mask,
					     int seq_num)
{
  // data (StructureData read req)
  char *data_file = (char *) data.file;
  char *name = (char *) data.name;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  OMobj_id id = OMfind_str_subobj(OMinst_obj,
				  "DLV.Display_Objs.default_data.bond_all",
				  OM_OBJ_RD);
  int set_bonds = 0;
  OMget_int_val(id, &set_bonds);
  op = ONETEP::load_structure::create(name, data_file, (set_bonds == 1),
				      message, 256);
  int ok;
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.ONETEP.Model.load", message);
  else
    ok = CCP3::render_in_view(op, ((bool)(data.new_view == 1)));
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ONETEP_Modules_save_cell_file::save(OMevent_mask event_mask,
					     int seq_num)
{
  // filename (OMXstr read req)
  // frac_coords (OMXint read req)
  char *data_file = (char *) filename;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = ONETEP::save_structure::create(data_file, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.ONETEP.Model.write", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ONETEP_Modules_load_data::load(OMevent_mask event_mask, int seq_num)
{
  // file_type (OMXint read req)
  // filename (OMXstr read req)
  char *data_file = (char *) filename;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  DLV::operation *op = 0;
  CCP3::show_as_busy();
  bool kspace = false;
  bool model = ((bool)((int)load_model == 1)
		or (DLV::operation::get_current() == 0));
  int set_bonds = 0;
  if (model) {
    OMobj_id id = OMfind_str_subobj(OMinst_obj,
				    "DLV.Display_Objs.default_data.bond_all",
				    OM_OBJ_RD);
    OMget_int_val(id, &set_bonds);
  }
  switch ((int)file_type) {
  case 0:
    op = ONETEP::load_cube_file::create(data_file, model, (set_bonds == 1),
					message, 256);
    break;
  case 1:
    op = ONETEP::load_dos_data::create(data_file, message, 256);
    break;
  default:
    strncpy(message, "BUG: property type not recognised", 256);
    break;
  };
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.ONETEP.Property.Load", message);
  else {
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_WARNING,
			     "CCP3.ONETEP.Property.Load", message);
    // Todo - new view?
    if (model)
      ok = CCP3::render_in_view(op, false);
    else
      ok = CCP3::update_op_in_3Dview(op, kspace);
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ONETEP_Modules_Run_SCF::create(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  if ((int)make == 1) {
    CCP3::show_as_busy();
    DLV::operation *op = 0;
    char message[256];
    message[0] = '\0';
    op = ONETEP::scf_base::create((int)data.task, message, 256);
    if (op != 0) {
      if ((int)data.task == 1)
	ok = CCP3::render_in_view(op, false);
      else
	ok = CCP3::update_op_in_3Dview(op);
    }
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.ONETEP.SCF.create", message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_ONETEP_Modules_Run_SCF::inherit_pseudo(OMevent_mask event_mask,
						int seq_num)
{
  return CCP3::return_info(DLV_ERROR, "CCP3.ONETEP.SCF.inherit",
			   "Not implemented");
}

int CCP3_ONETEP_Modules_Run_SCF::add_pseudo(OMevent_mask event_mask,
					    int seq_num)
{
  // bdata (OMXstr read req notify)
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  char *filename = (char *) bdata.filename;
  float radius = (float) bdata.radius;
  int ngwfs = (int) bdata.ngwfs;
  bool set_def = ((bool)(use_default == 1));
  CCP3::show_as_busy();
  bool complete;
  if (ONETEP::scf_base::add_pseudo(ngwfs, radius, filename,
				   set_def, complete, message, 256))
    all_pseudos = (int)complete;
  else
    ok = CCP3::return_info(DLV_ERROR, "CCP3.ONETEP.SCF.basis", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ONETEP_Modules_Run_SCF::execute(OMevent_mask event_mask, int seq_num)
{
  // data (SCFParams read req)
  // job_data (job_data read req)
  CCP3::show_as_busy();
  DLV::job_setup_data jd((char *) job_data.hostname,
			 (char *) job_data.work_dir,
			 (char *) job_data.scratch_dir, (int)job_data.nprocs,
			 (int)job_data.nnodes, (int)job_data.hours,
			 (int)job_data.memory_gb, (char *)job_data.account_id,
			 (char *) job_data.queue, (bool) job_data.is_parallel);
  ONETEP::Tolerances tol((float) data.Tolerances.cutoff,
			 (float) data.Tolerances.grid,
			 (float) data.Tolerances.kernel);
  ONETEP::Hamiltonian ham((bool) data.Hamiltonian.unrestricted,
			  (int) data.Hamiltonian.functional,
			  (int) data.Hamiltonian.dispersion,
			  (bool) data.Hamiltonian.set_spin,
			  (int) data.Hamiltonian.spin_value);
  ONETEP::Properties props((int) data.Properties.homo_plot,
			   (int) data.Properties.lumo_plot,
			   (int) data.Properties.num_eigs,
			   (bool) (data.Properties.do_dos == 1),
			   (float) data.Properties.dos_smear,
			   (bool) (data.Properties.density_plot == 1),
			   (bool) (data.Properties.mulliken_calc == 1),
			   (float) data.Properties.mulliken_neighbours);
  ONETEP::Optimise opt((int) data.Optimize.method,
		       (int) data.Optimize.max_iter,
		       (float) data.Optimize.energy_tol,
		       (float) data.Optimize.force_tol,
		       (float) data.Optimize.disp_tol);
  int ok = OM_STAT_SUCCESS;
  char message[256];
  message[0] = '\0';
  if (ONETEP::scf_base::calculate(tol, ham, props, opt, jd,
				  ((bool)(data.user_calc) == 1),
				  (char *)data.user_dir,
				  message, 256))
    ; // ?
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.ONETEP.SCF.calc", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ONETEP_Modules_Run_SCF::stop(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::cancel_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.ONETEP.SCF.cancel",
			   "Problem canceling SCF calc");
  else {
    bool reset = ((int)data.task == 1);
    ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					   false, reset);
  }
  CCP3::show_as_idle();
  return ok;
}
