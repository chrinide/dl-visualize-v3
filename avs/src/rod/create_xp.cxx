
#include "avs/src/rod/rod_gen.hxx"
#include "avs/src/express/calcs.hxx"
#include "avs/src/express/rod.hxx"
#include "avs/src/express/rod_calc.hxx"
#include "avs/src/express/rod_create.hxx"
#include "avs/src/express/rod_fit.hxx"
#include "avs/src/express/rod_model.hxx"
#include "avs/src/express/rod_params.hxx"
#include "avs/src/express/rod_plot.hxx"
#include "avs/src/express/rod_read.hxx"
#include "avs/src/express/rod_view.hxx"
#include "avs/src/express/rod_write.hxx"

int CCP3_Calcs_ROD_create_ui::create(OMevent_mask event_mask, int seq_num)
{
#ifdef DLV_RELEASE
  // Load the object definitions from binary V
  OMobj_id templates = OMfind_str_subobj(OMroot_obj,
					 "Templates.CCP3.ROD", OM_OBJ_RW);
  if (OMis_null_obj(templates)) {
    ERRverror("rod.UI", ERR_ERROR, "Failed to find template");
    return OM_STAT_ERROR;
  } else {
    if (OMopen_file(templates, "rod_mods.vo", 0) != OM_STAT_SUCCESS) {
      ERRverror("rod.UI", ERR_ERROR, "Failed to load modules");
      return OM_STAT_ERROR;
    } else {
      if (OMopen_file(templates, "rod_create.vo", 0) != OM_STAT_SUCCESS) {
	ERRverror("rodUI", ERR_ERROR, "Failed to load create macros");
	return OM_STAT_ERROR;
      } else {
	if (OMopen_file(templates, "rod_fit.vo", 0) != OM_STAT_SUCCESS) {
	  ERRverror("rodUI", ERR_ERROR, "Failed to load fit macros");
	  return OM_STAT_ERROR;
	} else {
	  if (OMopen_file(templates, "rod_model.vo", 0) != OM_STAT_SUCCESS) {
	    ERRverror("rodUI", ERR_ERROR, "Failed to load model macros");
	    return OM_STAT_ERROR;
	  } else {
	    if (OMopen_file(templates, "rod_plot.vo", 0) != OM_STAT_SUCCESS) {
	      ERRverror("rodUI", ERR_ERROR, "Failed to load plot macros");
	      return OM_STAT_ERROR;
	    } else {
	      if (OMopen_file(templates, "rod_read.vo", 0) != OM_STAT_SUCCESS) {
		ERRverror("rodUI", ERR_ERROR, "Failed to load read macros");
		return OM_STAT_ERROR;
	      } else {
		if (OMopen_file(templates, "rod_params.vo", 0) != OM_STAT_SUCCESS) {
		  ERRverror("rodUI", ERR_ERROR, "Failed to load params macros");
		  return OM_STAT_ERROR;
		} else {
		  if (OMopen_file(templates, "rod_calc.vo", 0) != OM_STAT_SUCCESS) {
		    ERRverror("rodUI", ERR_ERROR, "Failed to load calc macros");
		    return OM_STAT_ERROR;
		  } else {
		    if (OMopen_file(templates, "rod_write.vo", 0) != OM_STAT_SUCCESS) {
		      ERRverror("rodUI", ERR_ERROR, "Failed to load write macros");
		      return OM_STAT_ERROR;
		    } else {
		      if (OMopen_file(templates, "rod_view.vo", 0) != OM_STAT_SUCCESS) {
			ERRverror("rodUI", ERR_ERROR, "Failed to load view macros");
			return OM_STAT_ERROR;
		      } else {
			if (OMopen_file(templates, "rod_macs.vo", 0) != OM_STAT_SUCCESS) {
			  ERRverror("rod.UI", ERR_ERROR, "Failed to load macros");
			  return OM_STAT_ERROR;
			}
		      }
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
#endif // DLV_RELEASE
  // Create the interface's network and attach to parent objects
  OMobj_id id = OMfind_str_subobj(OMinst_obj, "DLV.calculations", OM_OBJ_RW);
  CCP3_ROD_Macros_ROD *ptr = new CCP3_ROD_Macros_ROD(id);
  if (ptr == 0) {
    ERRverror("rod.UI", ERR_ERROR, "Failed to create interface");
    return OM_STAT_ERROR;
  } else {
    // Todo
    // Base.fontinfo => <-.fontAttributes
    OMobj_id obj = OMfind_str_subobj(id, "preferences", OM_OBJ_RW);
    OMset_obj_ref(ptr->preferences.obj_id(), obj, 0); 
    obj = OMfind_str_subobj(id, "active_menus", OM_OBJ_RW);
    OMset_obj_ref(ptr->active_menus.obj_id(), obj, 0); 
    obj = OMfind_str_subobj(id, "UIparent", OM_OBJ_RW);
    OMset_obj_ref(ptr->UIparent.obj_id(), obj, 0); 
    obj = OMfind_str_subobj(id, "rod_dialog", OM_OBJ_RW);
    OMset_obj_ref(obj, ptr->dialog_visible.obj_id(), 0); 
    obj = OMfind_str_subobj(id, "model_type", OM_OBJ_RW);
    OMset_obj_ref(ptr->model_type.obj_id(), obj, 0); 
    return OM_STAT_SUCCESS;
  }
}
