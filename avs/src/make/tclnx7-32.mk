
CXX = g++
PIC = -fpic
#CXXFLAGS = -g -m32 -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -I/home/software/include
# release code
CXXFLAGS = -g -O2 -m32 -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -DDLV_RELEASE -I/home/software/include
# old non-threaded
#CXXFLAGS = -g -ansi -pedantic -Wall -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -I/usr/X11R6/include
SHARED = -shared

BOOSTDIR =
BOOSTLIB = /home/software/lib32

CCTBXDIR = ../../cctbx/include
CCTBXLIB = /home/bgs/lib/32/libcctbx.a

BABELDIR =
BABELLIB =

AVSDIR = /usr/local/avs.e

GUILIB =  $(AVSDIR)/lib/linux/fld/Xfld.o $(AVSDIR)/lib/linux/ag/ag_omx.o $(AVSDIR)/lib/linux/motif_ui/ui_core.o

LIBS = -L$(BOOSTLIB) -lboost_serialization-gcc43-mt -lboost_filesystem-gcc43-mt -lboost_thread-gcc43-mt

INCDIRS = -I$(CCTBXDIR)
AVSINCS = -I$(AVSDIR)/include -I$(AVSDIR) -I$(HOME)/express/3.0
