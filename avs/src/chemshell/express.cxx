
#include "avs/src/chemshell/str_gen.hxx"
#include "avs/src/chemshell/save_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "src/dlv/calculation.hxx"
#include "src/dlv/op_model.hxx"
#include "src/dlv/op_admin.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "src/chemshell/calcs.hxx"
#include "src/chemshell/struct.hxx"
#include "src/chemshell/save.hxx"

int CCP3_CHEMSHELL_Modules_load_structure::load(OMevent_mask event_mask,
					     int seq_num)
{
  // data (StructureData read req)
  char *data_file = (char *) data.file;
  char *name = (char *) data.name;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  OMobj_id id = OMfind_str_subobj(OMinst_obj,
				  "DLV.Display_Objs.default_data.bond_all",
				  OM_OBJ_RD);
  int set_bonds = 0;
  OMget_int_val(id, &set_bonds);
  op = CHEMSHELL::load_structure::create(name, data_file, (set_bonds == 1),
					 message, 256);
  int ok;
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.CHEMSHELL.Model.load", message);
  else
    ok = CCP3::render_in_view(op, ((bool)(data.new_view == 1)));
  CCP3::show_as_idle();
  return ok;
}

int CCP3_CHEMSHELL_Modules_save_pun_file::save(OMevent_mask event_mask,
					       int seq_num)
{
  // filename (OMXstr read req)
  // frac_coords (OMXint read req)
  char *data_file = (char *) filename;
  char *gp_name = (char *) label;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = CHEMSHELL::save_structure::create(data_file, gp_name, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.CHEMSHELL.Model.write", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}
