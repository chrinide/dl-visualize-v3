
#include "avs/src/image/im_gen.hxx"
#include "avs/src/express/calcs.hxx"
#include "avs/src/express/image.hxx"

int CCP3_Calcs_IMAGE_create_ui::create(OMevent_mask event_mask, int seq_num)
{
#ifdef DLV_RELEASE
  // Load the object definitions from binary V
  OMobj_id templates = OMfind_str_subobj(OMroot_obj,
					 "Templates.CCP3.IMAGE", OM_OBJ_RW);
  if (OMis_null_obj(templates)) {
    ERRverror("image.UI", ERR_ERROR, "Failed to find template");
    return OM_STAT_ERROR;
  } else {
    if (OMopen_file(templates, "im_mods.vo", 0) != OM_STAT_SUCCESS) {
      ERRverror("image.UI", ERR_ERROR, "Failed to load modules");
      return OM_STAT_ERROR;
    } else {
      if (OMopen_file(templates, "im_macs.vo", 0) != OM_STAT_SUCCESS) {
	ERRverror("image.UI", ERR_ERROR, "Failed to load macros");
	return OM_STAT_ERROR;
      }
    }
  }
#endif // DLV_RELEASE
  // Create the interface's network and attach to parent objects
  OMobj_id id = OMfind_str_subobj(OMinst_obj, "DLV.calculations", OM_OBJ_RW);
  CCP3_IMAGE_Macros_Image *ptr = new CCP3_IMAGE_Macros_Image(id);
  if (ptr == 0) {
    ERRverror("image.UI", ERR_ERROR, "Failed to create interface");
    return OM_STAT_ERROR;
  } else {
    // Todo
    // Base.fontinfo => <-.fontAttributes
    OMobj_id obj = OMfind_str_subobj(id, "preferences", OM_OBJ_RW);
    OMset_obj_ref(ptr->preferences.obj_id(), obj, 0); 
    obj = OMfind_str_subobj(id, "active_menus", OM_OBJ_RW);
    OMset_obj_ref(ptr->active_menus.obj_id(), obj, 0); 
    obj = OMfind_str_subobj(id, "UIparent", OM_OBJ_RW);
    OMset_obj_ref(ptr->UIparent.obj_id(), obj, 0); 
    obj = OMfind_str_subobj(id, "image_dialog", OM_OBJ_RW);
    OMset_obj_ref(obj, ptr->dialog_visible.obj_id(), 0); 
    obj = OMfind_str_subobj(id, "model_type", OM_OBJ_RW);
    OMset_obj_ref(ptr->model_type.obj_id(), obj, 0); 
    return OM_STAT_SUCCESS;
  }
}
