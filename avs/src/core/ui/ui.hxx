
#ifndef DLV_AVS_UI
#define DLV_AVS_UI

namespace CCP3 {

  // Todo - others for busy progress?
  extern void show_as_busy();
  extern void show_as_idle();

  extern int attach_op_to_UI(DLV::operation *op, const bool kspace = false);
  extern int attach_2D_UI();
  extern void set_ui_view_type(const bool kspace);

}

#endif // DLV_AVS_UI
