
#ifndef DLV_AVS_MESSAGES
#define DLV_AVS_MESSAGES

namespace CCP3 {

  int return_info(const DLVreturn_type info, const char routine[],
		  const char message[]);

}

#endif // DLV_AVS_MESSAGES
