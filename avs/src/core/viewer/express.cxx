
#include "avs/omx.hxx"
#include "avs/src/express/scenes.hxx"
#include "avs/src/core/viewer/sel_gen.hxx"
#include "avs/src/core/viewer/rem_gen.hxx"
#include "avs/src/core/viewer/xfrm_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/toolkit_obj.hxx"
#include "src/graphics/render_base.hxx"
#include "src/graphics/display_objs.hxx"
#include "src/dlv/operation.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "avs/src/core/messages.hxx"

namespace CCP3 {

  int delete_2Dview(const int index);

}

namespace {
  // Todo - improve this? anyone with more windows than this is an idiot
  const int max_windows = 100;
  CCP3_Viewers_Scenes_DLV3Dscene *list3D[max_windows];
  OMobj_id list2D[max_windows];
  //DLV::display_obj *ops2D[max_windows];
  DLV::operation *ops3D[max_windows];
  bool kspace3D[max_windows];
  int current3D = 0;
  int current2D = 0;
  int last3D = 0;
  OMobj_id view_macro = OMnull_obj;
  OMobj_id selector = OMnull_obj;
  OMobj_id top_objs = OMnull_obj;

}

void CCP3::init_view()
{
  for (int i = 0; i < max_windows; i++) {
    list3D[i] = 0;
    list2D[i] = OMnull_obj;
    //ops2D[i] = 0;
    ops3D[i] = 0;
    kspace3D[i] = false;
  }
  view_macro = OMfind_str_subobj(OMinst_obj, "DLV.Viewers", OM_OBJ_RW);
  selector = OMfind_str_subobj(OMinst_obj,
			       "DLV.Viewers.DLVview_selector.input_views",
			       OM_OBJ_RW);
  top_objs = OMfind_str_subobj(view_macro, "top_objs", OM_OBJ_RW);
  // Create initial 3D viewer
  //CCP3::create_3Drview(current3D);
}

/*int CCP3::create_2Dview(CCP3_Viewers_Scenes_DLV2Dscene * &v,
			const bool set_current)
{
  int i;
  for (i = 0; i < max_windows; i++)
    if (list2D[i] == 0)
      break;
  if (i == max_windows)
    return OM_STAT_ERROR;
  else {
    CCP3_Viewers_Scenes_DLV2Dscene *view = 
      new CCP3_Viewers_Scenes_DLV2Dscene(view_macro);
    if (view == 0)
      return OM_STAT_ERROR;
    else {
      view->View_Index = i + 1;
      list2D[i] = view;
      OMadd_obj_ref(selector, view->View.View.obj_id(), 0);
      v = view;
      if (set_current) {
	current3D = 0;
	current2D = i + 1;
	// activate view
	view->View.View.selected = 1;
      }
    }
  }
  return OM_STAT_SUCCESS;
}*/

int CCP3::add_2Dview(OMobj_id view, const bool set_current)
{
  int i;
  for (i = 0; i < max_windows; i++)
    if (OMis_null_obj(list2D[i]))
      break;
  if (i == max_windows)
    return OM_STAT_ERROR;
  else {
    OMobj_id id = OMfind_str_subobj(view, "View_Index", OM_OBJ_RW);
    if (OMis_null_obj(id))
      fprintf(stderr, "Find 1 problem\n");
    OMset_int_val(id, i + 1);
    list2D[i] = view;
    id = OMfind_str_subobj(view, "View.View", OM_OBJ_RW);
    if (OMis_null_obj(id))
      fprintf(stderr, "Find 2 problem\n");
    if (OMis_null_obj(selector))
      fprintf(stderr, "Find 3 problem\n");
    OMadd_obj_ref(selector, id, 0);
    if (set_current) {
      current3D = 0;
      current2D = i + 1;
      // activate view
      id = OMfind_str_subobj(id, "selected", OM_OBJ_RW);
      if (OMis_null_obj(id))
	fprintf(stderr, "Find 4 problem\n");
      OMset_int_val(id, 1);
    }
  }
  return OM_STAT_SUCCESS;
}

int CCP3::create_3Dview(CCP3_Viewers_Scenes_DLV3Dscene * &v,
			const bool kspace, const bool set_current)
{
  int i;
  for (i = 0; i < max_windows; i++)
    if (list3D[i] == 0)
      break;
  if (i == max_windows)
    return OM_STAT_ERROR;
  else {
    // try to position it below the main window when it opens
    OMobj_id app = OMfind_str_subobj(OMinst_obj, "DLV.app_window.UIapp",
				     OM_OBJ_RD);
    OMobj_id pos = OMfind_str_subobj(app, "y", OM_OBJ_RD);
    int y = 0;
    OMget_int_val(pos, &y);
    pos = OMfind_str_subobj(app, "height", OM_OBJ_RD);
    int h = 0;
    OMget_int_val(pos, &h);
    pos = OMfind_str_subobj(app, "x", OM_OBJ_RD);
    int x = 0;
    OMget_int_val(pos, &x);
    CCP3_Viewers_Scenes_DLV3Dscene *view = 
      new CCP3_Viewers_Scenes_DLV3Dscene(view_macro);
    if (view == 0)
      return OM_STAT_ERROR;
    else {
      view->View.ViewUI.ViewPanel.UI.UIshell.x = x;
      int yval = y + h;
      if (y + h + 512 > 1024)
	yval = 1024 - 512;
      view->View.ViewUI.ViewPanel.UI.UIshell.y = yval;
      view->View_Index = i + 1;
      if (kspace)
	view->Space = "k";
      else
	view->Space = "Real";
      list3D[i] = view;
      kspace3D[i] = kspace;
      OMadd_obj_ref(selector, view->View.View.obj_id(), 0);
      OMadd_obj_ref(top_objs, view->Top.obj.obj_id(), 0);
      v = view;
      if (set_current) {
	current2D = 0;
	current3D = i + 1;
	last3D = current3D;
	// activate view
	view->View.View.selected = 1;
	CCP3::set_ui_view_type(kspace);
      }
    }
  }
  return OM_STAT_SUCCESS;
}

/*CCP3_Viewers_Scenes_DLV2Dscene *CCP3::get_current_2Dview()
{
  return list2D[current2D - 1];
}*/

CCP3_Viewers_Scenes_DLV3Dscene *CCP3::get_current_3Dview(const bool reset,
							 const bool kspace)
{
  if (current3D == 0) {
    if (reset) {
      if (last3D == 0)
	return 0;
      else {
	current3D = last3D;
	return list3D[last3D - 1];
      }
    } else
      return 0;
  } else {
    if (kspace3D[current3D - 1] == kspace)
      return list3D[current3D - 1];
    else {
      if (reset) {
	if (last3D == 0)
	  return 0;
	else if (last3D != current3D) {
	  current3D = last3D;
	  return list3D[last3D - 1];
	} else {
	  do {
	    current3D--;
	  } while (kspace3D[current3D - 1] != kspace and current3D > 1);
	  return list3D[current3D - 1];
	}
      } else
	return 0;
    }
  }
}

int CCP3::set_current_2Dview(const int index)
{
  current3D = 0;
  current2D = index;
  // Todo - set current op or something?
  //attach_2D_UI();
  return OM_STAT_SUCCESS;
}

int CCP3::set_current_3Dview(const int index)
{
  current2D = 0;
  current3D = index;
  last3D = index;
  ops3D[index - 1]->set_current();
  CCP3::set_model_type(ops3D[index - 1]->get_model_type());
  CCP3::set_ui_view_type(kspace3D[index - 1]);
  attach_op_to_UI(ops3D[index - 1], kspace3D[index - 1]);
  return OM_STAT_SUCCESS;
}

bool CCP3::select_3Dview(const DLV::operation *op)
{
  current2D = 0;
  for(int i = 0; i < max_windows; i++){
    if(ops3D[i]==op){
      ops3D[i]->set_current();
      list3D[i]->View.View.selected = 1;
      current3D = i+1;
      return true;
    }
  }
  return false;
}

void CCP3::freeze_other_3Dviews(const DLV::operation *op)
{
  current2D = 0;
  for(int i = 0; i < max_windows; i++)
    if(ops3D[i]!=op && ops3D[i]!=0)
      list3D[i]->View.ViewUI.ViewWindow.active = 0;
  
  return;
}

void CCP3::activate_3Dviews()
{
  current2D = 0;
  for(int i = 0; i < max_windows; i++)
    if(ops3D[i]!=0)
      list3D[i]->View.ViewUI.ViewWindow.active = 1;
  return;
}

int CCP3::attach_op_to_current_3Dview(DLV::operation *op, const bool detach,
				      const bool reattach)
{
  DLVreturn_type ok = DLV_OK;

  bool reattach_me = reattach;
  bool detach_me = detach;
  int i = current3D;
  if (i > 0 and ops3D[i - 1] == op)
    return OM_STAT_SUCCESS;
  else if (i == 0 or ops3D[i - 1] != 0) {
    if (i == 0 or kspace3D[i - 1]) {
      // I'm basically assuming that this is never used with kspace
      // since its not passed as a parameter.
      for (i = i - 1; i > 0; i--)
	if (list3D[i - 1] != 0 and !kspace3D[i - 1])
	  break;
      if (i == 0) {
	for (i = i + 1; i < max_windows; i++)
	  if (list3D[i - 1] != 0 and !kspace3D[i - 1])
	    break;
      }
      if (i == max_windows) {
	detach_me = false;
	reattach_me = true;
	CCP3_Viewers_Scenes_DLV3Dscene *v = 0;
	create_3Dview(v, false, false);
      }
    }
    if (detach_me) {
      if (kspace3D[i - 1])
	ok =
	  (ops3D[i - 1])->detach_from_kviewer(list3D[i - 1]);
      else
	ok =
	  (ops3D[i - 1])->detach_from_rviewer(list3D[i - 1]);
      /*DLV::toolkit_obj obj = ops3D[i - 1]->get_r3D_obj();
	OMdel_obj_ref(list3D[i - 1]->Top.child_objs, obj.get_id(), 0);
      */
    }
  }
  ops3D[i - 1] = op;
  if (reattach_me) {
    char model_name[256];
    op->get_model_name(model_name, 256);
    CCP3::set_model_name(list3D[i - 1], model_name);
    /*DLV::toolkit_obj obj = op->get_xform_obj();
      OMset_obj_ref(list3D[i - 1]->Top.Xform, obj.get_id(), 0);
      obj = op->get_r3D_obj();*/
    if (kspace3D[i - 1])
      ok = op->attach_to_kviewer(list3D[i - 1]);
    else
      ok = op->attach_to_rviewer(list3D[i - 1]);
  }
  if (ok == DLV_OK)
    return OM_STAT_SUCCESS;
  else
    return return_info(ok, "CCP3::view_attach", "Viewer attach failed");
}

int CCP3::update_op_in_3Dview(DLV::operation *op, const bool kspace)
{
  DLV::operation *old_op = op->get_parent();
  // We also need to update the op if the other space is also viewed
  // do this first to avoid the return statements.
  int i = max_windows;
  for (i = 0; i < max_windows; i++) {
    if (kspace3D[i] == !kspace and ops3D[i] == op)
      break;
  }
  if (i == max_windows) {
    for (i = 0; i < max_windows; i++) {
      if (kspace3D[i] == !kspace and ops3D[i] == old_op)
	break;
    }
    if (i != max_windows)
      ops3D[i] = op;
  }
  // Now the actual space that was requested
  i = max_windows;
  for (i = 0; i < max_windows; i++) {
    if (kspace3D[i] == kspace and ops3D[i] == op)
      break;
  }
  if (i == max_windows) {
    for (i = 0; i < max_windows; i++) {
      if (kspace3D[i] == kspace and ops3D[i] == old_op)
	break;
    }
    if (i != max_windows)
      ops3D[i] = op;
    else if (current3D > 0) {
      // This op isn't viewing this space, need new viewer
      return render_in_view(op, true, kspace);
    }
  }
  return OM_STAT_SUCCESS;
}

int CCP3::check_3Dview(DLV::operation *op, const bool kspace)
{
  // see if a viewer for this space is attached to the op, if not open one.
  int i;
  for (i = 0; i < max_windows; i++) {
    if (kspace3D[i] == kspace and ops3D[i] == op)
      break;
  }
  if (i == max_windows)
    return render_in_view(op, true, kspace);
  else
    return OM_STAT_SUCCESS;
}

int CCP3::close_viewers()
{
  for (int i = 0; i < max_windows; i++) {
    if (ops3D[i] != 0) {
      OMdel_obj_ref(selector, list3D[i]->View.View.obj_id(), 0);
      delete list3D[i];
      list3D[i] = 0;
      kspace3D[i] = false;
    }
  }
  current3D = 0;
  last3D = 0;
  return OM_STAT_SUCCESS;
}

int CCP3::reset_viewers()
{
  for (int i = 0; i < max_windows; i++)
    if (ops3D[i] != 0)
      delete_3Dview(i + 1);
  //Todo printf("What about 2D views?\n");
  return OM_STAT_SUCCESS;
}

int CCP3::delete_2Dview(const int index)
{
  /*if (ops2D[index - 1] != 0) {
    ops2D[index - 1]->detach_from_viewer();
    ops2D[index - 1] = 0;
    }*/
  OMobj_id id = OMfind_str_subobj(list2D[index - 1], "View.View", OM_OBJ_RW);
  OMdel_obj_ref(selector, id, 0);
  //delete list2D[index - 1];
  list2D[index - 1] = OMnull_obj;
  current2D = 0;
  return OM_STAT_SUCCESS;
}

int CCP3::delete_3Dview(const int index)
{
  if (ops3D[index - 1] != 0) {
    if (kspace3D[index - 1])
      ops3D[index - 1]->detach_from_kviewer(list3D[index - 1]);
    else
      ops3D[index - 1]->detach_from_rviewer(list3D[index - 1]);
    ops3D[index - 1] = 0;
  }
  OMdel_obj_ref(selector, list3D[index - 1]->View.View.obj_id(), 0);
  delete list3D[index - 1];
  list3D[index - 1] = 0;
  kspace3D[index - 1] = false;
  current3D = 0;
  last3D = 0;
  return OM_STAT_SUCCESS;
}

int CCP3::delete_3Dview(const DLV::operation *op, const bool kspace)
{
  int index;
  for (index = 0; index < max_windows; index++) {
    if (ops3D[index] == op and kspace3D[index] == kspace) {
      delete_3Dview(index + 1);
      break;
    }
  }
  if (index == max_windows)
    return OM_STAT_ERROR;
  else
    return OM_STAT_SUCCESS;
}
int CCP3_Viewers_view_select::select(OMevent_mask event_mask, int seq_num)
{
  // view_type (OMXint read req notify)
  // index (OMXint read req notify)

  // The OMXenum is { 3D, 2D }
  if ((int)view_type == 0)
    return CCP3::set_current_3Dview((int)index);
  else
    return CCP3::set_current_2Dview((int)index);
}

int CCP3_Viewers_view_remove::close(OMevent_mask event_mask, int seq_num)
{
  // view_type (OMXint read req)
  // index (OMXint read req notify)

  if ((int)trigger == 1) {
    // The OMXenum is { 3D, 2D }
    if ((int)view_type == 0)
      return CCP3::delete_3Dview((int)index);
    else
      return CCP3::delete_2Dview((int)index);
  } else
    return OM_STAT_SUCCESS;
}

int CCP3_Viewers_view_save_transform::save(OMevent_mask event_mask,
					   int seq_num)
{
  // flag (OMXint read req)
  // button (OMXint read req)
  // state (OMXint read req notify)
  if ((int)flag == 1) {
    if ((int)state == 2) {
      // button up
      if (((int) button == 1) or ((int) button == 2)) {
	// middle or right button events.
	// Todo - store transform
      }
    }
  }
  return OM_STAT_SUCCESS;
}
