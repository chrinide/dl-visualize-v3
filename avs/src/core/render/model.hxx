
#ifndef CCP3_RENDER_MODEL
#define CCP3_RENDER_MODEL

// forward declare
class CCP3_Viewers_Scenes_DLV3Dscene;

namespace CCP3 {

  extern void set_model_type(const int m);
  extern int render_in_view(DLV::operation *op, const bool new_view,
			    const bool kspace = false);
  extern void set_model_name(CCP3_Viewers_Scenes_DLV3Dscene *view,
		      const char name[]);

}

#endif // CCP3_RENDER_MODEL
