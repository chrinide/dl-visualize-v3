
#include "avs/src/core/properties/disp_gen.hxx"
#include "avs/src/core/properties/sel_gen.hxx"
#include "avs/src/core/properties/del_gen.hxx"
#include "avs/src/core/properties/hide_gen.hxx"
#include "avs/src/core/properties/upd_gen.hxx"
#include "avs/src/core/properties/rem_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/viewer/view.hxx"

int CCP3_core_ViewData_display_data::display(OMevent_mask event_mask,
					     int seq_num)
{
  // object (OMXint read req)
  // component (OMXint read req)
  // display_type (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  bool kspace = false;
  DLVreturn_type ok = DLV::operation::display_current_data((int)object,
							   (int)component,
							   (int)display_type,
							   kspace,
							   message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.View.Display", message);
  else // make sure a viewer is open for the object we displayed.
    return CCP3::update_op_in_3Dview(DLV::operation::get_current(), kspace);
}

int CCP3_core_ViewData_select_display::display(OMevent_mask event_mask,
					       int seq_num)
{
  // object (OMXint read notify req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::select_current_display((int)object, message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.View.Select", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_ViewData_delete_display::remove(OMevent_mask event_mask,
					      int seq_num)
{
  // object (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::delete_current_display((int)object, message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.View.Delete", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_ViewData_hide_display::update(OMevent_mask event_mask,
					    int seq_num)
{
  // visible (OMXint read req notify)
  // index (OMXint read req)
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int vis = (int)visible;
  bool kspace = false;
  ok = DLV::operation::hide_current_render((int)object, vis, kspace,
					   message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Display.Close", message);
  else if (vis)
    return CCP3::check_3Dview(DLV::operation::get_current(), kspace);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_ViewData_update_display::update(OMevent_mask event_mask,
					      int seq_num)
{
  // object (OMXint read req)
  // plane (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_current_streamlines((int)object, (int)plane,
						  message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Display.Streamlines", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_ViewData_delete_data::remove(OMevent_mask event_mask, int seq_num)
{
  // object (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  bool kspace = false;
  DLVreturn_type ok = DLV::operation::delete_current_data((int)object, kspace,
							  message, 256);
  CCP3::show_as_idle();
  if (ok == DLV_OK)
    return CCP3::update_op_in_3Dview(DLV::operation::get_current(), kspace);
  else if (ok == DLV_WARNING) {
    CCP3::update_op_in_3Dview(DLV::operation::get_current(), kspace);
    return CCP3::return_info(ok, "CCP3.core.View.Delete", message);
  } else
    return CCP3::return_info(ok, "CCP3.core.View.Delete", message);
}
