
#include <string>
#include <list>
#include <iostream>
#include <fstream>
#include "avs/src/core/prefs/save_gen.hxx"
#include "avs/src/core/prefs/open_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/dlv/utils.hxx"
#include "avs/src/core/messages.hxx"

int CCP3_core_Prefs_save::save(OMevent_mask event_mask, int seq_num)
{
  OMobj_id id = OMfind_str_subobj(OMinst_obj,
				  "DLV.DLVcore.environment.DLVRC", OM_OBJ_RD);
  char buff[512];
  char *env = buff;
  OMget_str_val(id, &env, 512);
  DLV::string init_name = env;
  init_name += DIR_SEP_STR;
  init_name += "calculations";
  std::ifstream init_in;
  std::list<DLV::string> data;
  // read $DLVRC/calculations
  char line[256];
  if (DLV::open_file_read(init_in, init_name.c_str(), line, 256)) {
    while (!init_in.eof()) {
      init_in.getline(line, 256);
      if (strncmp(line, "CRYSTAL", 7) != 0 and
	  strncmp(line, "EXCURV", 6) != 0 and
	  strncmp(line, "GULP", 4) != 0 and strncmp(line, "ROD", 3) != 0 and
	  strncmp(line, "CDS", 3) != 0 and strncmp(line, "ONETEP", 3) != 0)
	data.push_back(line);
    }
    init_in.close();
  } else
    return CCP3::return_info(DLV_ERROR, "CCP3.prefs.save.in", line);
  std::ofstream init_out;
  if (DLV::open_file_write(init_out, init_name.c_str(), line, 256)) {
    OMobj_id base = OMfind_str_subobj(OMinst_obj,
				      "DLV.calculations.programs", OM_OBJ_RD);
    int value;
    id = OMfind_str_subobj(base, "CRYSTAL98", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "CRYSTAL98 = 1;\n";
    id = OMfind_str_subobj(base, "CRYSTAL03", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "CRYSTAL03 = 1;\n";
    id = OMfind_str_subobj(base, "CRYSTAL06", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "CRYSTAL06 = 1;\n";
    id = OMfind_str_subobj(base, "CRYSTAL09", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "CRYSTAL09 = 1;\n";
    id = OMfind_str_subobj(base, "CRYSTAL14", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "CRYSTAL14 = 1;\n";
    id = OMfind_str_subobj(base, "CRYSTAL17", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "CRYSTAL17 = 1;\n";
    id = OMfind_str_subobj(base, "EXCURV", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "EXCURV = 1;\n";
    id = OMfind_str_subobj(base, "GULP", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "GULP = 1;\n";
    id = OMfind_str_subobj(base, "ROD", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "ROD = 1;\n";
    id = OMfind_str_subobj(base, "CDS", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "CDS = 1;\n";
    id = OMfind_str_subobj(base, "ONETEP", OM_OBJ_RD);
    OMget_int_val(id, &value);
    if (value == 1)
      init_out << "ONETEP = 1;\n";
    std::list<DLV::string>::const_iterator x;
    for (x = data.begin(); x != data.end(); ++x)
      init_out << (*x) << std::endl;
    init_out.close();
  } else
    return CCP3::return_info(DLV_ERROR, "CCP3.prefs.save.out", line);
  // executable file locations
  init_name = env;
  init_name += DIR_SEP_STR;
#ifdef WIN32
  init_name += "executables.bat";
#else
  init_name += "executables";
#endif // WIN32
  std::ofstream exec_out;
  if (DLV::open_file_write(exec_out, init_name.c_str(), line, 256)) {
    id = OMfind_str_subobj(OMinst_obj, "DLV.calculations.CRYSTAL.exec_location",
			   OM_OBJ_RD);
    if (!OMis_null_obj(id)) {
      OMget_str_val(id, &env, 512);
      if (strlen(env) != 0) {
#ifdef WIN32
	exec_out << "set DLV_CRYSTAL=" << env << '\n';
#else
	exec_out << "setenv DLV_CRYSTAL " << env << '\n';
#endif // WIN32
      }
    }
    id = OMfind_str_subobj(OMinst_obj, "DLV.calculations.EXCURV.exec_location",
			   OM_OBJ_RD);
    if (!OMis_null_obj(id)) {
      OMget_str_val(id, &env, 512);
      if (strlen(env) != 0) {
#ifdef WIN32
	exec_out << "set DLV_EXCURV=" << env << '\n';
#else
	exec_out << "setenv DLV_EXCURV " << env << '\n';
#endif // WIN32
      }
    }
    id = OMfind_str_subobj(OMinst_obj, "DLV.calculations.GULP.exec_location",
			   OM_OBJ_RD);
    if (!OMis_null_obj(id)) {
      OMget_str_val(id, &env, 512);
      if (strlen(env) != 0) {
#ifdef WIN32
	exec_out << "set DLV_GULP=" << env << '\n';
#else
	exec_out << "setenv DLV_GULP " << env << '\n';
#endif // WIN32
      }
    }
    id = OMfind_str_subobj(OMinst_obj, "DLV.calculations.ONETEP.exec_location",
			   OM_OBJ_RD);
    if (!OMis_null_obj(id)) {
      OMget_str_val(id, &env, 512);
      if (strlen(env) != 0) {
#ifdef WIN32
	exec_out << "set DLV_ONETEP=" << env << '\n';
#else
	exec_out << "setenv DLV_ONETEP " << env << '\n';
#endif // WIN32
      }
    }
    exec_out.close();
  } else
    return CCP3::return_info(DLV_ERROR, "CCP3.prefs.save.exec", line);
  return OM_STAT_SUCCESS;
}

int CCP3_core_Prefs_open_panel::open(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req)
  DLV::string label = "DLV.calculations.";
  label += (char *)name;
  label += ".Prefs.";
  if (strcmp((char *)name, "CRYSTAL") == 0)
    label += "Version";
  else
    label += "UIoption";
  label += ".set";
  OMobj_id id = OMfind_str_subobj(OMinst_obj, label.c_str(), OM_OBJ_RW);
  OMset_int_val(id, 1);
  return OM_STAT_SUCCESS;
}
