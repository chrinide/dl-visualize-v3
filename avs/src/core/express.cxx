
#include <time.h>
#include "avs/src/core/exit_gen.hxx"
#include "avs/src/core/init_gen.hxx"
#include "avs/src/core/str_gen.hxx"
#include "avs/src/core/map_gen.hxx"
#include "avs/src/core/env_gen.hxx"
#include "avs/src/core/fb_gen.hxx"
#include "avs/src/core/sub_gen.hxx"
#include "avs/src/core/sel_gen.hxx"
#include "avs/src/core/proj_gen.hxx"
#include "avs/src/core/save_gen.hxx"
#include "avs/src/core/demo_gen.hxx"
#include "avs/src/core/http_gen.hxx"
#include "avs/src/core/rend_gen.hxx"
#include <avs/event.h>
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/utils.hxx"
#include "src/dlv/project.hxx"
#include "src/dlv/operation.hxx"
#include "src/dlv/op_admin.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "messages.hxx"
#include "avs/src/core/ui/ui.hxx"

namespace CCP3 {

  void set_project_name(const char name[]);

}

// messages
int CCP3::return_info(const DLVreturn_type info, const char routine[],
		      const char message[])
{
  switch (info) {
  case DLV_ERROR:
    ERRverror(routine, ERR_ERROR, message);
    return OM_STAT_ERROR;
  case DLV_WARNING:
    ERRverror(routine, ERR_WARNING, message);
    return OM_STAT_UNDEF;
  default:
    return OM_STAT_SUCCESS;
  }
}

void CCP3::set_project_name(const char name[])
{
  static OMobj_id label = OMnull_obj;
  static OMobj_id unnamed = OMnull_obj;
  if (OMis_null_obj(label)) {
    label = OMfind_str_subobj(OMinst_obj, "DLV.app_window.project_name",
			      OM_OBJ_RW);
    unnamed = OMfind_str_subobj(OMinst_obj,
		    "DLV.app_window.projectUI.menu_file.user_project",
				OM_OBJ_RW);
  }
  OMset_str_val(label, name);
  OMset_int_val(unnamed, 1);
}

// Modules
int CCP3_core_Init_initialise::init(OMevent_mask event_mask, int seq_num)
{
  // (atom_file OMXstr read req)
  // (display_prefs OMXstr read req)
  // (default_project OMXstr read req)
  char *atom_data_file = (char *) atom_file;
  char *prefs_file = (char *) display_prefs;
  char *def_project = (char *) default_project;
  char message[256];
  CCP3::init_view();
#ifdef ENABLE_DLV_JOB_THREADS
  // Todo - unless we wait on a socket (not windows?) I don't see how to
  // avoid essentially polling and using CPU
  EVadd_select(EV_TIMEOUT, 0, DLV::check_job_list, 0, 0, 10);
#endif // ENABLE_DLV_JOB_THREADS
  DLVreturn_type ok = DLV::project::initialise(atom_data_file, prefs_file,
					       def_project, message, 256);
  return CCP3::return_info(ok, "CCP3.core.Init.initialise", message);
}

int CCP3_core_Exit_finalise::save(OMevent_mask event_mask, int seq_num)
{
  // named (OMXint read req)
  // update (OMXint write)
  /*if ((int)named == 1) {
    char message[256];
    DLVreturn_type ok = DLV::project::save(message, 256);
    if (ok != DLV_OK)
      fprintf(stderr, "DLV.Exit - %s\n", message);
      }*/
  if ((int)named == 0)
    DLV::project::delete_unnamed();
  update = 1;
  return OM_STAT_SUCCESS;
}

int CCP3_core_Utils_select_str_arr::select(OMevent_mask event_mask,
					   int seq_num)
{
  // selections (OMXint_array read req notify)
  // in_strings (OMXstr_array read req)
  // out_strings (OMXstr_array write)
  xp_long n;
  selections.get_array_size(&n);
  if (n > 0) {
    int *sptr = (int *) selections.ret_array_ptr(OM_GET_ARRAY_RD);
    if (sptr != 0) {
      int count = 0;
      for (int i = 0; i < n; i++) {
	if (sptr[i] == 1)
	  count++;
      }
      if (count == 0) {
	OMset_array_size(out_strings.obj_id(), 0);
	OMset_obj_val(out_strings.obj_id(), OMnull_obj, 0);
      } else {
	OMset_array_size(out_strings.obj_id(), count);
	char buffer[128];
	count = 0;
	for (int i = 0; i < n; i++) {
	  if (sptr[i] == 1) {
	    (void) OMret_str_array_val(in_strings.obj_id(), i, buffer, 128);
	    OMset_str_array_val(out_strings.obj_id(), count, buffer);
	    count++;
	  }
	}
      }
      selections.free_array(sptr);
    }
  }
  return OM_STAT_SUCCESS;
}

int CCP3_core_Utils_map_selection::map(OMevent_mask event_mask, int seq_num)
{
  // selections (OMXint_array read req notify)
  // selected_item (OMXint read req notify)
  // index (OMXint write)
  xp_long n;
  selections.get_array_size(&n);
  if (n > 0) {
    int *sptr = (int *) selections.ret_array_ptr(OM_GET_ARRAY_RD);
    if (sptr != 0) {
      int v = (int) selected_item;
      int count = 0;
      int i;
      for (i = 0; i < n; i++) {
	if (sptr[i] == 1) {
	  if (count == v)
	    break;
	  count++;
	}
      }
      index = i;
      selections.free_array(sptr);
    }
  }
  return OM_STAT_SUCCESS;
}

static bool find_path(char location[], const bool search_path,
		      const char *env_force, const char *env_default,
		      const char *exec_name);

static bool find_path(char location[], const bool search_path,
		      const char *env_force, const char *env_default,
		      const char *exec_name)
{
  location[0] = '\0';
  char *value = getenv(env_force);
  if (value != NULL)
    strcpy(location, value);
  else {
    bool found = false;
#ifndef WIN32
    // The scripts don't search the path under windows.
    if (exec_name != NULL) {
      if (search_path) {
	value = getenv("PATH");
	if (value != NULL) {
	  DLV::string filename, dir, exec_path = value;
	  int i = 0, j = 0, l = exec_path.length();
	  while ((i = exec_path.find(':', i + 1)) < l) {
	    if (i < 0)
	      break;
	    dir = exec_path.substr(j, (i - j));
	    if (dir == ".")
	      filename = exec_name;
	    else
	      filename = dir + DIR_SEP_STR + exec_name;
	    if (DLV::check_exec_access(filename)) {
	      found = true;
	      strcpy(location, dir.c_str());
	    }
	    j = i + 1;
	  }
	  dir = exec_path.substr(j, (i - j));
	  if (dir == ".")
	    filename = exec_name;
	  else
	    filename = dir + DIR_SEP_STR + exec_name;
	  if (DLV::check_exec_access(filename)) {
	    found = true;
	    strcpy(location, dir.c_str());
	  }
	}
      }
    }
#endif // WIN32
    if (!found) {
      value = getenv(env_default);
      if (value != NULL)
	strcpy(location, value);
      else
	location[0] = '\0';
    }
  }
  return true;
}

int CCP3_core_Calcs_exec_environ::init(OMevent_mask event_mask, int seq_num)
{
  // default_var (OMXstr read req)
  // search_path (OMXint read req)
  // main_var (OMXstr read req)
  // binary (OMXstr read req)
  // location (OMXstr write)
  char path[1024];
  if (find_path(path, ((bool)(search_path == 1)), (char *) main_var,
		(char *)default_var, (char *) binary))
    location = path;
  return OM_STAT_SUCCESS;
}

int CCP3_core_Calcs_exec_environ::set(OMevent_mask event_mask, int seq_num)
{
  // main_var (OMXstr read req)
  // location (OMXstr read req notify)
  DLV::setenviron((char *) main_var, (char *)location);
  return OM_STAT_SUCCESS;
}

/*int CCP3_core_Utils_manageFB::manage(OMevent_mask event_mask, int seq_num)
{
  // connect (OMXint read req notify)
  // data (FBdata read req)
  OMobj_id id = OMfind_str_subobj(OMinst_obj,
				  "DLV.app_window.FileBrowser.FBdata",
				  OM_OBJ_RW);
  if ((int)connect == 0) {
    // disconnect
    OMset_obj_ref(id, OMnull_obj, 0);
  } else {
    OMset_obj_ref(id, data.obj_id(), 0);
  }
  return OM_STAT_SUCCESS;
}*/

int CCP3_core_Utils_manageFB::manage(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint notify read req)
  // filename (OMXstr read req notify)
  // wildcard (OMXstr read req)
  // pattern (OMXstr write)
  // Todo - should this change as we open projects, or will the browsers
  // associated with doing that get it right anyway.
  static DLV::string directory = "";
  if (filename.changed(seq_num)) {
    if (strlen((char *)filename) > 0)
      directory = DLV::get_directory_path((char *)filename, true);
  } else if ((int)trigger == 1) {
    DLV::string out = directory;
    if ((int)use_funit == 1)
      out += (char *)funit;
    else
      out += (char *)wildcard;
    pattern = out.c_str();
  }
  return OM_STAT_SUCCESS;
}

int CCP3_core_Project_subproject::create(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req)
  // directory (OMXstr read req)
  char *dir = (char *) directory;
  char *label = (char *) name;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = DLV::subproject::create(label, dir, message, 256);
  int ok = OM_STAT_SUCCESS;
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.Project.Sub.create", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Project_select::list(OMevent_mask event_mask, int seq_num)
{
  // do_list (OMXint read req)
  // do_filter (OMXint read req)
  // do_all (OMXint read req)
  // option1 (OMXint read req)
  // option2 (OMXint read req)
  // op (OMXint read req)
  // selection (OMXint write)
  // indices (OMXint_array read write)
  // items (OMXstr_array write)
  int ok = OM_STAT_SUCCESS;
  if (((int) do_list) == 1) {
    CCP3::show_as_busy();
    int n = DLV::operation::max_items();
    if (n == 0) {
      //OMset_obj_val(selection.obj_id(), OMnull_obj, 0);
      OMset_array_size(indices.obj_id(), 0);
      OMset_array_size(items.obj_id(), 0);
      //OMset_obj_val(indices.obj_id(), OMnull_obj, 0);
      //OMset_obj_val(items.obj_id(), OMnull_obj, 0);
    } else {
      int selected = -1;
      if (indices.valid_obj()) {
	xp_long size = 0;
	indices.get_array_size(&size);
	if (size > 0) {
	  int *indptr = (int *) indices.ret_array_ptr(OM_GET_ARRAY_RD);
	  if (indptr == 0)
	    ok = CCP3::return_info(DLV_ERROR, "CCP3.Project.Select",
				   "Unable to read indices");
	  else {
	    if (!do_list.changed(seq_num))
	      if (selection.valid_obj())
		selected = indptr[(int)selection];
	    indices.free_array(indptr);
	  }
	}
      }
      if (ok == OM_STAT_SUCCESS) {
	DLV::string *names = new DLV::string[n];
	int *ids = new int[n];
	int nitems = 0;
	if ((int)do_all == 1)
	  nitems = DLV::operation::list_all(names, ids, n);
	else {
	  DLV::op_test op1;
	  switch ((int)option1) {
	  case 0:
	    op1 = &DLV::operation::is_all;
	    break;
	  case 1:
	    op1 = &DLV::operation::is_subproject;
	    break;
	  case 2:
	    op1 = &DLV::operation::is_geometry;
	    break;
	  case 3:
	    op1 = &DLV::operation::is_geom_load;
	    break;
	  case 4:
	    op1 = &DLV::operation::is_data_load;
	    break;
	  case 5:
	    op1 = &DLV::operation::is_calc;
	    break;
	  default:
	    ok = CCP3::return_info(DLV_ERROR, "CCP3.Project.Select",
				   "BUG: op1 not recognised");
	    break;
	  }
	  if (ok == OM_STAT_SUCCESS) {
	    if ((int)op == 0)
	      nitems = DLV::operation::list_op(op1, names, ids, n, -1);
	    else {
	      DLV::op_test op2;
	      switch ((int)option2) {
	      case 0:
		op2 = &DLV::operation::is_all;
		break;
	      case 1:
		op2 = &DLV::operation::is_subproject;
		break;
	      case 2:
		op2 = &DLV::operation::is_geometry;
		break;
	      case 3:
		op2 = &DLV::operation::is_geom_load;
		break;
	      case 4:
		op2 = &DLV::operation::is_data_load;
		break;
	      case 5:
		op2 = &DLV::operation::is_calc;
		break;
	      default:
		ok = CCP3::return_info(DLV_ERROR, "CCP3.Project.Select",
				       "BUG: op2 not recognised");
		break;
	      }
	      if (ok == OM_STAT_SUCCESS) {
		if ((int)op == 1)
		  nitems = DLV::operation::list_op1_and_op2(op1, op2,
							    names, ids, n, -1);
		else
		  nitems = DLV::operation::list_op1_or_op2(op1, op2,
							   names, ids, n, -1);
	      }
	    }
	  }
	}
	if (nitems == 0) {
	  //OMset_obj_val(selection.obj_id(), OMnull_obj, 0);
	  OMset_array_size(indices.obj_id(), 0);
	  OMset_array_size(items.obj_id(), 0);
	  //OMset_obj_val(indices.obj_id(), OMnull_obj, 0);
	  //OMset_obj_val(items.obj_id(), OMnull_obj, 0);
	} else {
	  OMset_array_size(indices.obj_id(), nitems);
	  OMset_array_size(items.obj_id(), nitems);
	  int *indptr = (int *) indices.ret_array_ptr(OM_GET_ARRAY_WR);
	  if (indptr == 0)
	    ok = CCP3::return_info(DLV_ERROR, "CCP3.Project.Select",
				   "Unable to write indices");
	  else {
	    bool found = false;
	    for (int i = 0; i < nitems; i++) {
	      indptr[i] = ids[i];
	      OMset_str_array_val(items.obj_id(), i, names[i].c_str());
	      if (ids[i] == selected) {
		selected = i;
		found = true;
	      }
	    }
	    if (found)
	      selection = selected;
	    //else
	    //OMset_obj_val(selection.obj_id(), OMnull_obj, 0);
	    indices.free_array(indptr);
	  }
	}
	delete [] ids;
	delete [] names;
      }
    }
    CCP3::show_as_idle();
  }
  return ok;
}

// Todo - don't like this being a copy of list, but wait till users have
// commented on UI first. A lot of the size/n = 0 options won't exist!
int CCP3_core_Project_select::expand(OMevent_mask event_mask, int seq_num)
{
  // do_all (OMXint read req)
  // option1 (OMXint read req)
  // option2 (OMXint read req)
  // op (OMXint read req)
  // selection (OMXint read req)
  // indices (OMXint_array read write)
  // items (OMXstr_array write)
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  int n = DLV::operation::max_items();
  if (n == 0) {
    //OMset_obj_val(selection.obj_id(), OMnull_obj, 0);
    OMset_array_size(indices.obj_id(), 0);
    OMset_array_size(items.obj_id(), 0);
    //OMset_obj_val(indices.obj_id(), OMnull_obj, 0);
    //OMset_obj_val(items.obj_id(), OMnull_obj, 0);
  } else {
    int selected = -1;
    if (indices.valid_obj()) {
      xp_long size = 0;
      indices.get_array_size(&size);
      if (size > 0) {
	int *indptr = (int *) indices.ret_array_ptr(OM_GET_ARRAY_RD);
	if (indptr == 0)
	  ok = CCP3::return_info(DLV_ERROR, "CCP3.Project.Select",
				 "Unable to read indices");
	else {
	  if (!do_list.changed(seq_num))
	    if (selection.valid_obj())
	      selected = indptr[(int)selection];
	  indices.free_array(indptr);
	}
      }
    }
    if (ok == OM_STAT_SUCCESS) {
      DLV::string *names = new DLV::string[n];
      int *ids = new int[n];
      int nitems = 0;
      DLV::op_test op1;
      switch ((int)option1) {
      case 0:
	op1 = &DLV::operation::is_all;
	break;
      case 1:
	op1 = &DLV::operation::is_subproject;
	break;
      case 2:
	op1 = &DLV::operation::is_geometry;
	break;
      case 3:
	op1 = &DLV::operation::is_geom_load;
	break;
      case 4:
	op1 = &DLV::operation::is_data_load;
	break;
      case 5:
	op1 = &DLV::operation::is_calc;
	break;
      default:
	ok = CCP3::return_info(DLV_ERROR, "CCP3.Project.Select",
			       "BUG: op1 not recognised");
	break;
      }
      if (ok == OM_STAT_SUCCESS) {
	if ((int)op == 0)
	  nitems = DLV::operation::list_op(op1, names, ids, n, selected);
	else {
	  DLV::op_test op2;
	  switch ((int)option2) {
	  case 0:
	    op2 = &DLV::operation::is_all;
	    break;
	  case 1:
	    op2 = &DLV::operation::is_subproject;
	    break;
	  case 2:
	    op2 = &DLV::operation::is_geometry;
	    break;
	  case 3:
	    op2 = &DLV::operation::is_geom_load;
	    break;
	  case 4:
	    op2 = &DLV::operation::is_data_load;
	    break;
	  case 5:
	    op2 = &DLV::operation::is_calc;
	    break;
	  default:
	    ok = CCP3::return_info(DLV_ERROR, "CCP3.Project.Select",
				   "BUG: op2 not recognised");
	    break;
	  }
	  if (ok == OM_STAT_SUCCESS) {
	    if ((int)op == 1)
	      nitems = DLV::operation::list_op1_and_op2(op1, op2, names, ids,
							n, selected);
	    else
	      nitems = DLV::operation::list_op1_or_op2(op1, op2, names, ids,
						       n, selected);
	  }
	}
      }
      if (nitems == 0) {
	//OMset_obj_val(selection.obj_id(), OMnull_obj, 0);
	OMset_array_size(indices.obj_id(), 0);
	OMset_array_size(items.obj_id(), 0);
	//OMset_obj_val(indices.obj_id(), OMnull_obj, 0);
	//OMset_obj_val(items.obj_id(), OMnull_obj, 0);
      } else {
	OMset_array_size(indices.obj_id(), nitems);
	OMset_array_size(items.obj_id(), nitems);
	int *indptr = (int *) indices.ret_array_ptr(OM_GET_ARRAY_WR);
	if (indptr == 0)
	  ok = CCP3::return_info(DLV_ERROR, "CCP3.Project.Select",
				 "Unable to write indices");
	else {
	  for (int i = 0; i < nitems; i++) {
	    indptr[i] = ids[i];
	    OMset_str_array_val(items.obj_id(), i, names[i].c_str());
	  }
	  indices.free_array(indptr);
	}
      }
      delete [] ids;
      delete [] names;
    }
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Project_select::select(OMevent_mask event_mask, int seq_num)
{
  // selection (OMXint read req)
  // new_view (OMXint read req)
  // indices (OMXint_array read)
  char message[256];
  DLV::operation *op = 0;
  int ok = OM_STAT_SUCCESS;

  CCP3::show_as_busy();
  int selected = -1;
  if (indices.valid_obj()) {
    xp_long size = 0;
    indices.get_array_size(&size);
    if (size > 0) {
      int *indptr = (int *) indices.ret_array_ptr(OM_GET_ARRAY_RD);
      if (indptr == 0)
	ok = CCP3::return_info(DLV_ERROR, "CCP3.Project.Select",
			       "Unable to read indices");
      else {
	selected = indptr[(int)selection];
	indices.free_array(indptr);
      }
    }
    op = DLV::operation::select(selected);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR,
			     "CCP3.Project.select", message);
    // Todo else if (op->is_displayed())
    //  ok = CCP3::return_info(DLV_ERROR, "CCP3.Project.Select",
    //		     "Selection is already displayed");
    else
      ok = CCP3::render_in_view(op, ((bool)(new_view == 1)));
  } else
    ok = OM_STAT_ERROR;
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Project_project::create(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req)
  // directory (OMXstr read req)
  // inherit (OMXint read req)
  // atoms (OMXstr read req)
  // display (OMXstr read req)
  char *label = (char *) name;
  char *dir_name = (char *)directory;
  char *atom_data_file = (char *) atoms;
  char *prefs_file = (char *) display;
  char message[256];
  char title[256];
  CCP3::show_as_busy();
  DLVreturn_type ok = DLV::project::create(label, dir_name, atom_data_file,
					   prefs_file, (int)inherit,
					   title, message, 256);
  int result = OM_STAT_SUCCESS;
  if (ok != DLV_OK)
    result = CCP3::return_info(ok, "CCP3.Project.New", message);
  else {
    CCP3::set_project_name(title);
    result = CCP3::reset_viewers();
  }
  CCP3::show_as_idle();
  return result;
}

int CCP3_core_Project_project::load(OMevent_mask event_mask, int seq_num)
{
  // directory (OMXstr read req)
  char *dir_name = (char *)directory;
  char message[256];
  char title[256];
  CCP3::show_as_busy();
  DLVreturn_type ok = DLV::project::load(dir_name, title, message, 256);
  CCP3::close_viewers();
  int result = OM_STAT_SUCCESS;
  if (ok != DLV_OK)
    result = CCP3::return_info(ok, "CCP3.Project.Load", message);
  else {
    CCP3::set_project_name(title);
    result = CCP3::render_in_view(DLV::operation::get_current(), false);
    //result = CCP3::reset_viewers();
  }
  CCP3::show_as_idle();
  return result;
}

int CCP3_core_Project_project::save(OMevent_mask event_mask, int seq_num)
{
  char message[256];
  CCP3::show_as_busy();
  DLVreturn_type ok = DLV::project::save(message, 256);
  int result = OM_STAT_SUCCESS;
  if (ok != DLV_OK)
    result = CCP3::return_info(ok, "CCP3.Project.Save", message);
  CCP3::show_as_idle();
  return result;
}

int CCP3_core_Project_project::save_as(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req)
  // directory (OMXstr read req)
  char *label = (char *) name;
  char *dir_name = (char *)directory;
  char message[256];
  char title[256];
  CCP3::show_as_busy();
  DLVreturn_type ok = DLV::project::save_all(label, dir_name, title,
					     message, 256);
  int result = OM_STAT_SUCCESS;
  if (ok != DLV_OK)
    result = CCP3::return_info(ok, "CCP3.Project.SaveAs", message);
  else
    CCP3::set_project_name(title);
  CCP3::show_as_idle();
  return result;
}

int CCP3_core_Project_check_save::check(OMevent_mask event_mask, int seq_num)
{
  // test (OMXint notify read req)
  // warn (OMXint write)
  // ok (OMXint write)
  if (((int) test) == 1) {
    if (!DLV::project::has_been_saved())
      warn = 1;
  }
  return OM_STAT_SUCCESS;
}

int CCP3_core_Demo_DLV_demo::check(OMevent_mask event_mask, int seq_num)
{
  // month (OMXint read)
  // year (OMXint read)
  // expired (OMXint write)
  // warning (OMXint write)
  struct tm expires;
  expires.tm_sec = 0;
  expires.tm_min = 0;
  expires.tm_hour = 0;
  expires.tm_mday = 1;
  expires.tm_mon = (((int)month) % 12) + 1;
  // Correct for 0-11
  expires.tm_mon -= 1;
  if (expires.tm_mon == 0)
    expires.tm_year = ((int)year) + 1;
  else
    expires.tm_year = (int)year;
  // correct for years since 1900
  expires.tm_year -= 1900;
  expires.tm_wday = 0;
  expires.tm_yday = 0;
  expires.tm_isdst = 0;
  time_t expiry = mktime(&expires);
  time_t current = time(NULL);
  //struct tm *ptr = gmtime(&current);
  if (difftime(current, expiry) > 0.0) {
    // DLV has expired
    expired = 1;
    warning = 0;
    return OM_STAT_SUCCESS;
  } else
    expired = 0;
  expires.tm_mon = (int)month;
  expires.tm_mon -= 1;
  expires.tm_year = (int)year;
  expires.tm_year -= 1900;
  expiry = mktime(&expires);
  if (difftime(current, expiry) > 0.0) {
    // Put up warning message, only 1 month to go.
    warning = 1;
  } else
    warning = 0;
  return OM_STAT_SUCCESS;
}

int CCP3_core_Utils_browse::open(OMevent_mask event_mask, int seq_num)
{
  // browser (OMXstr read)
  // url (OMXstr read req)
  if ((int)trigger < 1) {
    run = 0;
  } else if ((int)monitor == 0 or ((int)monitor == 1 and (int)run == 0)) {
#ifdef WIN32
    (void) ShellExecuteA(0, "open", (char *)url, 0, 0, SW_SHOWNORMAL);
#elif defined(__DARWIN_UNIX03)
    char command[256];
    strcpy(command, "open ");
    strncat(command, (char *)url, 256 - 5 - 1);
    (void) DLV::system(command);
#else
    char command[256];
    strncpy(command, (char *)browser, 64);
    int n = strlen(command);
    strncat(command, (char *)url, 256 - n - 4);
    strcat(command, " &");
    (void) DLV::system(command);
#endif // WIN32
    run = 1;
  }
  return OM_STAT_SUCCESS;
}

int CCP3_core_Utils_toggle_renderer::toggle(OMevent_mask event_mask,
					    int seq_num)
{
  // trigger (OMXint read req notify)
  // value (OMXint read req)
  // prev (OMXint read write)
  // run (OMXint write)
  if ((int)trigger == 1) {
    if ((int)value == 0) {
      prev = 1;
      run = 1;
    } else
      prev = 0;
  } else {
    if ((int)prev == 1) {
      run = 1;
    }
  }
  return OM_STAT_SUCCESS;
}
