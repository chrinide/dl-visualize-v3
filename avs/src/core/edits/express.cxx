
#include "avs/src/core/edits/edit_gen.hxx"
#include "avs/src/core/edits/sel_gen.hxx"
#include "avs/src/core/edits/del_gen.hxx"
#include "avs/src/core/edits/spec_gen.hxx"
#include "avs/src/core/edits/phon_gen.hxx"
#include "avs/src/core/edits/slce_gen.hxx"
#include "avs/src/core/edits/cut_gen.hxx"
#include "avs/src/core/edits/extd_gen.hxx"
#include "avs/src/core/edits/rel_gen.hxx"
#include "avs/src/core/edits/math_gen.hxx"
#include "avs/src/core/edits/real_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/viewer/view.hxx"

int CCP3_core_EditData_edit_data::edit(OMevent_mask event_mask, int seq_num)
{
  // object (OMXint read req)
  // component (OMXint read req)
  // edit_type (OMXint read req)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = DLV::operation::edit_current_data((int)object,
							 (int)component,
							 (int)edit_type,
							 message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Data.Edit", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_EditData_select_edit::edit(OMevent_mask event_mask, int seq_num)
{
  // object (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::select_current_edit((int)object, message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Edit.Select", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_EditData_delete_edit::remove(OMevent_mask event_mask,
					   int seq_num)
{
  // object (OMXint read req)

  /***********************/
  /* Function's Body     */
  /***********************/
  ERRverror("",ERR_NO_HEADER | ERR_PRINT,"I'm in method: CCP3_core_EditData_delete_edit::remove\n");
  
  // return 1 for success
  return(1);
}

int CCP3_core_EditData_update_spectrum::update(OMevent_mask event_mask,
					       int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_current_spectrum((int)data.index,
					       (int)data.ir_or_raman,
					       (float)data.emin,
					       (float)data.emax,
					       (int)data.npoints,
					       (float)data.broaden,
					       (float)data.harmonic_scale,
					       message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Edit.Spectrum", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_EditData_update_phonon::update(OMevent_mask event_mask,
					     int seq_num)
{
  // data (phonon_data read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_current_phonon((int)data.index,
					     (int)data.nframes,
					     (float)data.magnitude,
					     (bool)((int)(data.use_temp) == 1),
					     (float)data.temperature,
					     message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Edit.Phonons", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_EditData_update_slice::update(OMevent_mask event_mask,
					    int seq_num)
{
  // object (OMXint read req)
  // plane (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_current_slice((int)object, (int)plane,
					    message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Edit.Slice", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_EditData_update_cut::update(OMevent_mask event_mask, int seq_num)
{
  // object (OMXint read req)
  // box (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_current_cut((int)object, (int)box, message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Edit.Cut", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_EditData_update_extension::update(OMevent_mask event_mask,
						int seq_num)
{
  // object (OMXint read req)
  // a (OMXint read req notify)
  // b (OMXint read req notify)
  // c (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_current_extension((int)object,
						(int)a, (int)b, (int)c,
						message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Edit.Extend", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_EditData_related_properties::list(OMevent_mask event_mask,
						int seq_num)
{
  // selection (OMXint read req notify)
  // trust_user (OMXint read req notify)
  // ptype
  // labels (OMXstr_array write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  DLV::string *names;
  int n;
  int type = (int)ptype;
  switch (type) {
  case 0:
    ok = DLV::operation::list_current_data_math((int)selection,
						((bool)((int)trust_user == 1)),
						names, n, message, 256);
    break;
  case 1:
    ok = DLV::operation::list_current_reals(names, n, message, 256);
    break;
  default:
    ok = DLV_ERROR;
    strncpy(message, "PBUG: roperty type not recognised", 256);
    break;
  }
  CCP3::show_as_idle();  
  if (ok != DLV_OK) {
    OMset_array_size(labels.obj_id(), 0);
    return CCP3::return_info(ok, "CCP3.core.Edit.Math", message);
  } else {
    OMset_array_size(labels.obj_id(), n);
    for (int i = 0; i < n; i++)
      OMset_str_array_val(labels.obj_id(), i, names[i].c_str());
    if (n > 0)
      delete [] names;
    return OM_STAT_SUCCESS;
  }
}

int CCP3_core_EditData_update_math::update(OMevent_mask event_mask,
					   int seq_num)
{
  // object (OMXint read req)
  // selection (OMXint read req)
  // trust_user (OMXint read req)
  // index (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_current_data_math((int)object, (int)selection,
						((bool)((int)trust_user == 1)),
						(int)index, message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Edit.Math", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_EditData_update_reals::update(OMevent_mask event_mask,
					    int seq_num)
{
  // object (OMXint read req)
  // shift (OMXfloat read req notify)
  // selection (OMXint read req notify)
  // use_prop (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  if (selection.changed(seq_num) and (use_prop == 1))
    ok = DLV::operation::update_current_shift((int)object, (float)shift,
					      (int)selection, true,
					      message, 256);
  else
    ok = DLV::operation::update_current_shift((int)object, (float)shift,
					      (int)selection, false,
					      message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Edit.Shift", message);
  else
    return OM_STAT_SUCCESS;  
}
