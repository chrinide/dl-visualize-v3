
#include "avs/src/core/model/ptbl_gen.hxx"
#include "avs/src/core/model/make_gen.hxx"
#include "avs/src/core/model/def_gen.hxx"
#include "avs/src/core/model/fin_gen.hxx"
#include "avs/src/core/model/can_gen.hxx"
#include "avs/src/core/model/load_gen.hxx"
#include "avs/src/core/model/pdb_gen.hxx"
//#include "avs/src/express/scenes.hxx"
#include "avs/src/core/model/edit_gen.hxx"
#include "avs/src/core/model/cell_gen.hxx"
#include "avs/src/core/model/symm_gen.hxx"
#include "avs/src/core/model/mole_gen.hxx"
#include "avs/src/core/model/latt_gen.hxx"
#include "avs/src/core/model/orig_gen.hxx"
#include "avs/src/core/model/vgap_gen.hxx"
#include "avs/src/core/model/clus_gen.hxx"
#include "avs/src/core/model/atom_gen.hxx"
#include "avs/src/core/model/prop_gen.hxx"
#include "avs/src/core/model/slab_gen.hxx"
#include "avs/src/core/model/grp_gen.hxx"
#include "avs/src/core/model/salv_gen.hxx"
#include "avs/src/core/model/del_gen.hxx"
#include "avs/src/core/model/insa_gen.hxx"
#include "avs/src/core/model/insm_gen.hxx"
#include "avs/src/core/model/move_gen.hxx"
#include "avs/src/core/model/list_gen.hxx"
#include "avs/src/core/model/reg_gen.hxx"
#include "avs/src/core/model/cds_gen.hxx"
#include "avs/src/core/model/save_gen.hxx"
#include "avs/src/core/model/wlff_gen.hxx"
#include "avs/src/core/model/mlyr_gen.hxx"
#include "avs/src/core/model/geo_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "src/dlv/op_model.hxx"
#include "src/dlv/op_admin.hxx"
#include "src/dlv/op_data.hxx"
#include "src/dlv/load_model.hxx"
#include "src/dlv/save_model.hxx"
#include "src/dlv/atom_prefs.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/ui/ui.hxx"

#include <avs/event.h>

int CCP3_core_Model_periodic_table::select(OMevent_mask event_mask,
					   int seq_num)
{
  // array (OMXint_array read write req notify)
  // current (OMXint read write)
  int ok = OM_STAT_SUCCESS;
  xp_long array_size;
  int *array_arr;
  array_arr = (int *)array.ret_array_ptr(OM_GET_ARRAY_RW,&array_size);
  if (array_arr == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.periodic_table",
			   "Oops - bug getting array");
  else {
    int c;
    int i;
    for (i = 0; i < array_size; i++) {
      if (array_arr[i] != 0) {
	c = i;
	break;
      }
    }
    if (i < array_size) {
      current = c;
      array_arr[c] = 0;
    }
    array.free_array(array_arr);
  }
  return ok;
}

int CCP3_core_Model_create::create(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint read req notify)
  // index (OMXint read write req)
  // model (model_data read)
  // atom (atom_data read)
  int ok = OM_STAT_SUCCESS;
  int idx = (int) index;
  int mtype = (int)model.model_type;
  CCP3::show_as_busy();
  char message[256];
  if ((idx == 2) or (idx == 1 and mtype == 0)) {
    if (DLV::operation::create_model((char *)model.name, (int)model.model_type,
				     (int)model.lattice, (int)model.centre,
				     (char *)model.group, (int)model.setting,
				     (int)model.norigins, (double)model.a,
				     (double)model.b, (double)model.c,
				     (double)model.alpha, (double)model.beta,
				     (double)model.gamma,
				     message, 256) == DLV_OK)
      ok = CCP3::render_in_view(DLV::operation::get_current(),
				((bool)(model.new_view == 1)));
    else
      ok = OM_STAT_ERROR;
  } else if (idx > 2) {
    DLVreturn_type v;
    bool rhomb = ((int)model.model_type == 3 and (int)model.lattice == 4 and
		  (int)model.centre == 1);
    v = DLV::operation::create_add_atom((int)atom.atomic_number + 1,
					(bool)atom.set_charge,
					(int)atom.charge,
					(bool)atom.set_radius,
					(float)atom.radius,
					(double)atom.x, (double)atom.y,
					(double)atom.z, rhomb, message, 256);
    if (v == DLV_OK)
      ok = DLV::operation::update_current_cell(message, 256);
    else
      ok = OM_STAT_ERROR;
  }
  if (ok == OM_STAT_SUCCESS) {
    idx++;
    // Skip lattice parameters for molecule.
    if (mtype == 0 and idx == 2)
      idx++;
    index = idx;
  } else
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.create", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Model_atom_defaults::set(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint read req notify)
  // charge (OMXint write)
  // radius (OMXfloat write)
  // User selected a new atom type, so need to get default charge/radius.
  charge = DLV::atomic_data->find_charge(((int)trigger) + 1);
  radius = DLV::atomic_data->find_radius(((int)trigger) + 1);
  return OM_STAT_SUCCESS;
}

int CCP3_core_Model_atom_defaults::get_radius(OMevent_mask event_mask,
					      int seq_num)
{
  // trigger (OMXint read req notify)
  // charge (OMXint read req notify)
  // radius (OMXfloat write)
  radius = DLV::atomic_data->find_radius((int)trigger, (int)charge);
  return OM_STAT_SUCCESS;
}

int CCP3_core_Model_complete_model::finish(OMevent_mask event_mask,
					   int seq_num)
{
  // Can only get here if index > 2, so will be something to save.
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::accept_pending(false, true) != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.accept",
			   "Problem accepting new model");
  else if ((int)bond_all == 1)
    (void) DLV::operation::set_current_bond_all();
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Model_cancel_model::cancel(OMevent_mask event_mask, int seq_num)
{
  // index (OMXint read req)
  // new_view (OMXint read req)
  // check that we've created something.
  int ok = OM_STAT_SUCCESS;
  if ((int)index > 2) {
    CCP3::show_as_busy();
    if ((int)new_view != 0 or DLV::operation::last_model()) {
      ok = CCP3::delete_3Dview(DLV::operation::get_current(), false);
      DLV::operation::detach_pending();
      // send a message?, but need to cancel also
    } /*New*/ else {
      DLV::operation::detach_pending();
      ok = CCP3::render_in_view(DLV::operation::get_current(), false);
    } /* New,next was cancel_pending()*/
    if (DLV::operation::delete_pending() != DLV_OK)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.cancel",
			     "Problem canceling new model");
    else if (DLV::operation::get_current() == 0) {
      model_type = -1;
    }
    CCP3::show_as_idle();
    return ok;
  }
  return ok;
}

int CCP3_core_Model_load::load(OMevent_mask event_mask, int seq_num)
{
  // data (LoadStructData read req)
  char *data_file = (char *) data.filename;
  char *label = (char *) data.name;
  char message[256];
  bool load_model = ((bool)data.cube_model == 1);
  DLV::operation *op = 0;
  int ok;
  bool set_bonds = ((bool)(bond_all == 1));
  //char *type = (char *)data.type_string;
  CCP3::show_as_busy();
  message[0] = '\0';
  bool new_model = true;
  switch ((int)data.filetype) {
  case 0:
    op = DLV::load_cif_file::create(label, data_file, set_bonds, message, 256);
    break;
  case 1:
    op = DLV::load_xyz_file::create(label, data_file, set_bonds, message, 256);
    break;
  case 2:
    op = DLV::load_xtl_file::create(label, data_file, set_bonds, message, 256);
    break;
  case 3:
    op = DLV::load_cssr_file::create(label, data_file, set_bonds, message, 256);
    break;
  case 4:
    op = DLV::load_shelx_file::create(label, data_file, set_bonds,
				      message, 256);
    break;
  case 5:
    op = DLV::load_pdb_file::create(label, data_file, (int)data.pdb_model - 1,
				    (int)data.pdb_conformer - 1,
				    (int)nconformers, set_bonds, message, 256);
    break;
  case 6:
    op = DLV::load_cube_file::create(label, data_file, load_model, set_bonds,
				     message, 256);
    break;
  case 7:
    op = DLV::load_xsf_model::create(label, data_file, set_bonds, new_model,
				     message, 256);
    break;
#ifdef USE_ESCDF
  case 8:
    op = DLV::load_escdf_model::create(label, data_file, set_bonds,
				       message, 256);
    break;
#endif // ESCDF
  }
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.core.Model.load", message);
  else {
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_WARNING,
			     "CCP3.core.Model.load", message);
    if (new_model)
      ok = CCP3::render_in_view(op, ((bool)(data.new_view == 1)));
    else
      ok = CCP3::update_op_in_3Dview(op); // kspace?
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Model_check_pdb::pdb_models(OMevent_mask event_mask, int seq_num)
{
  // filename (OMXstr read req notify)
  // file_type (OMXint read req)
  // model (OMXint read write req)
  // nmodels (OMXint write)
  char *data_file = (char *) filename;
  char message[256];
  int ok = OM_STAT_SUCCESS;

  if ((int)file_type == 5) {
    CCP3::show_as_busy();
    int count = DLV::load_pdb_file::count_models(data_file, message, 256);
    if (strlen(message) > 0) {
      ok = CCP3::return_info(DLV_ERROR,
			     "CCP3.core.Model.pdb_check", message);
    } else {
      nmodels = count;
      int cur_model = (int)model;
      if (cur_model > count)
	cur_model = count;
      model = count;
      count = DLV::load_pdb_file::count_conformers(data_file, cur_model - 1,
						   message, 256);
      nconformers = count;
    }
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Model_check_pdb::pdb_conformers(OMevent_mask event_mask,
					      int seq_num)
{
  // filename (OMXstr read req)
  // model (OMXint read req notify)
  // nconformers (OMXint write)
  char *data_file = (char *) filename;
  int cur_model = (int)model;
  char message[256];
  int ok = OM_STAT_SUCCESS;

  if ((int)file_type == 5) {
    CCP3::show_as_busy();
    int count = DLV::load_pdb_file::count_conformers(data_file, cur_model - 1,
						     message, 256);
    if (strlen(message) > 0) {
      ok = CCP3::return_info(DLV_ERROR,
			     "CCP3.core.Model.pdb_check", message);
    } else
      nconformers = count;
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Model_edit_model::finish(OMevent_mask event_mask,
				       int seq_num)
{
  // Can only get here if index > 2, so will be something to save.
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::accept_pending(true) != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.accept",
			   "Problem accepting edited model");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Model_supercell::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req)
  // index (OMXint write)
  // params (SuperCellParams write)
  // new_view (OMXint read req)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if (strlen(label) > 0) {
    CCP3::show_as_busy();
    char message[256];
    int s11;
    int s22;
    int s33;
    bool conv_cell;
    if (DLV::operation::create_supercell(label, s11, s22, s33, conv_cell,
					 message, 256) == DLV_OK) {
      ok = CCP3::render_in_view(DLV::operation::get_current(),
				((bool)((int)new_view == 1)));
      params.s11 = s11;
      params.s12 = 0;
      params.s13 = 0;
      params.s21 = 0;
      params.s22 = s22;
      params.s23 = 0;
      params.s31 = 0;
      params.s32 = 0;
      params.s33 = s33;
      params.conventional = (int)conv_cell;
      index = 3;
    } else
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.supercell", message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Model_supercell::update(OMevent_mask event_mask, int seq_num)
{
  // params (SuperCellParams read req notify)
  CCP3::show_as_busy();
  char message[256];
  int matrix[3][3];
  matrix[0][0] = (int)params.s11;
  matrix[0][1] = (int)params.s12;
  matrix[0][2] = (int)params.s13;
  matrix[1][0] = (int)params.s21;
  matrix[1][1] = (int)params.s22;
  matrix[1][2] = (int)params.s23;
  matrix[2][0] = (int)params.s31;
  matrix[2][1] = (int)params.s32;
  matrix[2][2] = (int)params.s33;
  bool conv_cell = (bool)(((int)params.conventional == 1));
  DLVreturn_type ok = DLV::operation::update_supercell(matrix, conv_cell,
						       message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.supercell", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_del_symmetry::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if (strlen(label) > 0) {
    CCP3::show_as_busy();
    char message[256];
    if (DLV::operation::delete_symmetry(label, message, 256) == DLV_OK) {
      ok = CCP3::render_in_view(DLV::operation::get_current(),
				((bool)((int)new_view == 1)));
      index = 3;
    } else
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.delete_sym", message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Model_to_molecule::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if (strlen(label) > 0) {
    CCP3::show_as_busy();
    char message[256];
    if (DLV::operation::view_to_molecule(label, message, 256) == DLV_OK) {
      ok = CCP3::render_in_view(DLV::operation::get_current(),
				((bool)((int)new_view == 1)));
      index = 3;
    } else
      ok = CCP3::return_info(DLV_ERROR,
			     "CCP3.core.Model.to_molecule", message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Model_edit_lattice::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  // params (LatticeParams write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if (strlen(label) > 0) {
    CCP3::show_as_busy();
    char message[256];
    double values[6];
    bool usage[6];
    if (DLV::operation::edit_lattice(label, values, usage,
				     message, 256) == DLV_OK) {
      ok = CCP3::render_in_view(DLV::operation::get_current(),
				((bool)((int)new_view == 1)));
      params.a = values[0];
      params.b = values[1];
      params.c = values[2];
      params.alpha = values[3];
      params.beta = values[4];
      params.gamma = values[5];
      params.use_a = usage[0];
      params.use_b = usage[1];
      params.use_c = usage[2];
      params.use_alpha = usage[3];
      params.use_beta = usage[4];
      params.use_gamma = usage[5];
      index = 3;
    } else
      ok = CCP3::return_info(DLV_ERROR,
			     "CCP3.core.Model.edit_lattice", message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Model_edit_lattice::update(OMevent_mask event_mask, int seq_num)
{
  // params (LatticeParams read req notify)
  CCP3::show_as_busy();
  char message[256];
  double values[6];
  values[0] = (double)params.a;
  values[1] = (double)params.b;
  values[2] = (double)params.c;
  values[3] = (double)params.alpha;
  values[4] = (double)params.beta;
  values[5] = (double)params.gamma;
  DLVreturn_type ok = DLV::operation::update_edit_lattice(values,
							  message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.edit_lattice", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_shift_origin::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if (strlen(label) > 0) {
    CCP3::show_as_busy();
    char message[256];
    if (DLV::operation::shift_origin(label, message, 256) == DLV_OK) {
      ok = CCP3::render_in_view(DLV::operation::get_current(),
				((bool)((int)new_view == 1)));
      index = 3;
    } else
      ok = CCP3::return_info(DLV_ERROR,
			     "CCP3.core.Model.shift_origin", message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Model_shift_origin::position(OMevent_mask event_mask,
					   int seq_num)
{
  // params (OriginParams read req notify)
  CCP3::show_as_busy();
  char message[256];
  double x = (double)params.x;
  double y = (double)params.y;
  double z = (double)params.z;
  bool frac = (bool)(((int)params.fractional == 1));
  DLVreturn_type ok = DLV::operation::update_origin(x, y, z, frac,
						    message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.shift_origin", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_shift_origin::atoms(OMevent_mask event_mask, int seq_num)
{
  // params (OriginParams write)
  CCP3::show_as_busy();
  char message[256];
  double x;
  double y;
  double z;
  bool frac = (bool)(((int)params.fractional == 1));
  DLVreturn_type ok = DLV::operation::update_origin_atoms(x, y, z, frac,
							  message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.shift_origin", message);
  else {
    params.x = x;
    params.y = y;
    params.z = z;
    // Todo - only do this if its a single atom? avoids selected atom
    // being carried to cut cluster that isn't drawn as selected.
    return DLV::operation::deselect_all_atoms(message, 256);
  }
}

int CCP3_core_Model_shift_origin::symmetry(OMevent_mask event_mask,
					   int seq_num)
{
  // params (OriginParams write)
  CCP3::show_as_busy();
  char message[256];
  double x;
  double y;
  double z;
  bool frac = (bool)(((int)params.fractional == 1));
  DLVreturn_type ok = DLV::operation::update_origin_symm(x, y, z, frac,
							 message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.shift_origin", message);
  else {
    params.x = x;
    params.y = y;
    params.z = z;
    return OM_STAT_SUCCESS;
  }
}

int CCP3_core_Model_vacuum_gap::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  // params (VacuumParams write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if (strlen(label) > 0 and index == 0) {
    CCP3::show_as_busy();
    char message[256];
    double c_axis = 20.0;
    double p = 0.0;
    if (DLV::operation::vacuum_gap(label, c_axis, p, message, 256) == DLV_OK) {
      ok = CCP3::render_in_view(DLV::operation::get_current(),
				((bool)((int)new_view == 1)));
      params.length = c_axis;
      params.position = p;
      index = 3;
    } else
      ok = CCP3::return_info(DLV_ERROR,
			     "CCP3.core.Model.vacuum_gap", message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Model_vacuum_gap::position(OMevent_mask event_mask, int seq_num)
{
  // params (VacuumParams read req notify)
  CCP3::show_as_busy();
  char message[256];
  double c_axis = (double)params.length;
  double p = (double)params.position;
  DLVreturn_type ok = DLV::operation::update_vacuum(c_axis, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.vacuum_gap", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_cut_cluster::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // nselect (OMXint notify read req)
  // new_view (OMXint read req)
  // index (OMXint write)
  // params (ClusterParams write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0 and (int)nselect > 0) {
      CCP3::show_as_busy();
      char message[256];
      int neighbours = 1;
      if (DLV::operation::cut_cluster(label, neighbours,
				      message, 256) == DLV_OK) {
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	params.neighbours = neighbours;
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.cut_cluster", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_cut_cluster::position(OMevent_mask event_mask, int seq_num)
{
  // params (ClusterParams read req notify)
  CCP3::show_as_busy();
  char message[256];
  int n = (int)params.neighbours;
  float r = (float)params.radius;
  DLVreturn_type ok = DLV_OK;
  bool neigh = params.neighbours.changed(seq_num);
  bool pos = params.radius.changed(seq_num);
  if (neigh and pos) {
    // assume confusion at intial start up
    if (n == 1)
      neigh = false;
  }
  if (neigh)
    ok = DLV::operation::update_cluster(n, message, 256);
  else
    ok = DLV::operation::update_cluster(r, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.cut_cluster", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_edit_atom::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // nselect (OMXint read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  // atom (OMXint write)
  char *label = (char *) name;
  bool all = (bool)((int)(all_atoms) == 1);
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0 and DLV::operation::continue_has_selections()) {
      CCP3::show_as_busy();
      char message[256];
      int atn;
      if (DLV::operation::edit_atom(label, atn, all, message, 256) == DLV_OK) {
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	atom = (atn - 1);
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.change_atom", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_edit_atom::update(OMevent_mask event_mask, int seq_num)
{
  // atom (OMXint read req notify)
  // all_atoms (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  int atn = (int)atom;
  bool all = (bool)((int)(all_atoms) == 1);
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_atom(atn + 1, all, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.change_atom", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_edit_atom::accept(OMevent_mask event_mask, int seq_num)
{
  // copy of accept model
  // Can only get here if index > 2, so will be something to save.
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::accept_pending(true, false, true) != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.accept",
			   "Problem accepting new model");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Model_edit_atom_props::start(OMevent_mask event_mask,
					   int seq_num)
{
  // name (OMXstr read req notify)
  // nselect (OMXint read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  // data (atom_data write)
  char *label = (char *) name;
  bool all = (bool)((int)(all_atoms) == 1);
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0 and (int)nselect > 0) {
      CCP3::show_as_busy();
      char message[256];
      int charge = 0;
      float radius = 0.0;
      float red = 0.0;
      float green = 0.0;
      float blue = 0.0;
      int spin = 0;
      bool use_charge = false;
      bool use_radius = false;
      bool use_colour = false;
      bool use_spin = false;
      if (DLV::operation::edit_props(label, charge, radius, red, green, blue,
				     spin, use_charge, use_radius, use_colour,
				     use_spin, all, message, 256) == DLV_OK) {
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	data.set_charge = (int)use_charge;
	data.charge = charge;
	data.set_radius = (int)use_radius;
	data.radius = radius;
	data.set_colour = (int)use_colour;
	data.red = red;
	data.green = green;
	data.blue = blue;
	data.set_spin = (int)use_spin;
	data.spin = spin;
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.change_properties", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_edit_atom_props::update(OMevent_mask event_mask,
					    int seq_num)
{
  // data (atom_data read write req notify)
  // all_atoms (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  bool all = (bool)((int)(all_atoms) == 1);
  DLVreturn_type ok = DLV_OK;
  int charge = (int)data.charge;
  float radius = (float)data.radius;
  float red = (float)data.red;
  float green = (float)data.green;
  float blue = (float)data.blue;
  int spin = (int)data.spin;
  bool use_charge = (bool)((int)(data.set_charge) == 1);
  bool use_radius = (bool)((int)(data.set_radius) == 1);
  bool use_colour = (bool)((int)(data.set_colour) == 1);
  bool use_spin = (bool)((int)(data.set_spin) == 1);
  ok = DLV::operation::update_props(charge, radius, red, green, blue,
				    spin, use_charge, use_radius, use_colour,
				    use_spin, all, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.change_properties", message);
  else {
    data.radius = radius;
    return OM_STAT_SUCCESS;
  }
}

int CCP3_core_Model_cut_slab::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // surface (OMXint read req)
  // index (OMXint write)
  // data (slab_params write)
  // was_3D (OMXint write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0) {
      CCP3::show_as_busy();
      char message[256];
      DLV::string *names = 0;
      int nlabels = 0;
      int nlayers = 1;
      int w3D = 1;
      if ((int)surface == 1)
	ok = DLV::operation::cut_surface(label, names, nlabels, message, 256);
      else
	ok = DLV::operation::cut_slab(label, nlayers, names, nlabels,
				      w3D, message, 256);
      if (ok == DLV_OK) {
	was_3D = w3D;
	data.h = 0;
	data.k = 0;
	data.l = 1;
	data.conventional = 1;
	data.termination = 0;
	data.tolerance = 0.001;
	data.nlayers = nlayers;
	OMset_array_size(labels.obj_id(), nlabels);
	for (int i = 0; i < nlabels; i++)
	  OMset_str_array_val(labels.obj_id(), i, names[i].c_str());
	delete [] names;
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.cut_slab", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_cut_slab::update_hkl(OMevent_mask event_mask, int seq_num)
{
  // surface (OMXint read req)
  // h (OMXint read req notify)
  // k (OMXint read req notify)
  // l (OMXint read req notify)
  // conventional (OMXint read req notify)
  // tolerance (OMXfloat read req)
  // termination (OMXint write)
  // nlayers (OMXint write)
  // labels (OMXstr_array write)
  CCP3::show_as_busy();
  char message[256];
  //int atn = (int)atom;
  DLVreturn_type ok = DLV_OK;
  int ih = (int)h;
  int ik = (int)k;
  int il = (int)l;
  if (h != 0 or k != 0 or l != 0) {
    bool conv = (bool)((int)conventional == 1);
    float tol = (float)tolerance;
    DLV::string *names = 0;
    int nlabels = 0;
    int nlayers = 1;
    if ((int)surface == 1)
      ok = DLV::operation::update_surface(ih, ik, il, conv, tol, names,
					  nlabels, message, 256);
    else
      ok = DLV::operation::update_slab(ih, ik, il, conv, tol, nlayers, names,
				       nlabels, message, 256);
    CCP3::show_as_idle();
    if (ok != DLV_OK)
      return CCP3::return_info(ok, "CCP3.core.Model.cut_slab", message);
    else {
      data.termination = 0;
      data.nlayers = nlayers;
      OMset_array_size(labels.obj_id(), nlabels);
      for (int i = 0; i < nlabels; i++)
	OMset_str_array_val(labels.obj_id(), i, names[i].c_str());
      delete [] names;
      return OM_STAT_SUCCESS;
    }
  } else
    return CCP3::return_info(DLV_WARNING, "CCP3.core.Model.cut_slab",
			     "Ignoring (0 0 0) Miller indices");
}

int CCP3_core_Model_cut_slab::update_tol(OMevent_mask event_mask, int seq_num)
{
  // surface (OMXint read req)
  // tolerance (OMXfloat read req notify)
  // nlayers (OMXint write)
  // labels (OMXstr_array write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  float tol = (float)tolerance;
  DLV::string *names = 0;
  int nlabels = 0;
  int nlayers = 1;
  if ((int)surface == 1)
    ok = DLV::operation::update_surface(tol, names, nlabels, message, 256);
  else
    ok = DLV::operation::update_slab(tol, nlayers, names, nlabels,
				     message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.cut_slab", message);
  else {
    data.nlayers = nlayers;
    OMset_array_size(labels.obj_id(), nlabels);
    for (int i = 0; i < nlabels; i++)
      OMset_str_array_val(labels.obj_id(), i, names[i].c_str());
    delete [] names;
    return OM_STAT_SUCCESS;
  }
}

int CCP3_core_Model_cut_slab::update_term(OMevent_mask event_mask, int seq_num)
{
  // surface (OMXint read req)
  // tolerance (OMXfloat read req)
  // termination (OMXint read req notify)
  // nlayers (OMXint read req)
  if ((int)index > 0) {
    CCP3::show_as_busy();
    char message[256];
    DLVreturn_type ok = DLV_OK;
    int term = (int)termination;
    if ((int)surface == 1)
      ok = DLV::operation::update_surface(term, message, 256);
    else
      ok = DLV::operation::update_slab_term(term, message, 256);
    CCP3::show_as_idle();
    if (ok != DLV_OK)
      return CCP3::return_info(ok, "CCP3.core.Model.cut_slab", message);
    else
      return OM_STAT_SUCCESS;
  } else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_cut_slab::update_layers(OMevent_mask event_mask,
					    int seq_num)
{
  // surface (OMXint read req)
  // tolerance (OMXfloat read req)
  // termination (OMXint read req)
  // nlayers (OMXint read req notify)
  if ((int)surface == 1)
    return OM_STAT_SUCCESS;
  else {
    CCP3::show_as_busy();
    char message[256];
    DLVreturn_type ok = DLV_OK;
    int n = (int)nlayers;
    ok = DLV::operation::update_slab(n, message, 256);
    CCP3::show_as_idle();
    if (ok != DLV_OK)
      return CCP3::return_info(ok, "CCP3.core.Model.cut_slab", message);
    else
      return OM_STAT_SUCCESS;
  }
}

int CCP3_core_Model_add_atom_group::update(OMevent_mask event_mask,
					   int seq_num)
{
  // trigger (OMXint read req notify)
  // label (OMXstr read req)
  // name (OMXstr read req)
  // all_atoms (OMXint read req)
  // bulk (OMXstr read req)
  CCP3::show_as_busy();
  char message[256];
  bool all = (bool)((int)all_atoms == 1);  
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::create_atom_group((char *)label, (char *)name, all,
					 (char*)bulk, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.atom_group", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_cut_salvage::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  // data (salvage_params write)
  // min_layers (OMXint write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0) {
      CCP3::show_as_busy();
      char message[256];
      int nlayers = 0;
      int min_l = 0;
      ok = DLV::operation::cut_salvage(label, nlayers, min_l, message, 256);
      if (ok == DLV_OK) {
	data.tolerance = 0.001;
	data.nlayers = nlayers;
	min_layers = min_l;
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.cut_salvage", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_cut_salvage::update_tol(OMevent_mask event_mask,
					    int seq_num)
{
  // tolerance (OMXfloat read req notify)
  // nlayers (OMXint write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  float tol = (float)tolerance;
  ok = DLV::operation::update_salvage(tol, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.cut_salvage", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_cut_salvage::update_layers(OMevent_mask event_mask,
					       int seq_num)
{
  // tolerance (OMXfloat read req)
  // nlayers (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int n = (int)nlayers;
  ok = DLV::operation::update_salvage(n, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.cut_salvage", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_delete_atom::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // nselect (OMXint read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0 and DLV::operation::continue_has_selections()) {
      CCP3::show_as_busy();
      char message[256];
      if (DLV::operation::delete_atoms(label, message, 256) == DLV_OK) {
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.delete_atoms", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_delete_atom::accept(OMevent_mask event_mask, int seq_num)
{
  // copy of accept model
  // Can only get here if index > 2, so will be something to save.
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::accept_pending(true, false, true) != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.accept",
			   "Problem accepting new model");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Model_insert_atom::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0) {
      CCP3::show_as_busy();
      char message[256];
      if (DLV::operation::insert_atom(label, message, 256) == DLV_OK) {
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	atom_type = 0;
	fractional = 0;
	x = 0.0;
	y = 0.0;
	z = 0.0;
	use_editor = 0;
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.insert_atom", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_insert_atom::update(OMevent_mask event_mask, int seq_num)
{
  // atom_type (OMXint read req notify)
  // data (move_params read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_inserted_atom((int)atom_type + 1,
					    ((int)fractional) == 1,
					    (double)x, (double)y, (double)z,
					    message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.insert_atom", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_insert_atom::change(OMevent_mask event_mask, int seq_num)
{
  // fractional (OMXint read req notify)
  // x (OMXdouble write)
  // y (OMXdouble write)
  // z (OMXdouble write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::switch_atom_coords(((int)fractional) == 1, false,
					  xc, yc, zc, message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.insert_atom", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_insert_atom::generate(OMevent_mask event_mask, int seq_num)
{
  // x (OMXdouble write)
  // y (OMXdouble write)
  // z (OMXdouble write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::update_from_selections(xc, yc, zc,
					      message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.insert_atom", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_insert_atom::edit(OMevent_mask event_mask, int seq_num)
{
  // use_editor (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  bool value = ((bool)((int)use_editor == 1));
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::set_edit_transform(value, xc, yc, zc,
					  message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.insert_atom", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_insert_atom::accept(OMevent_mask event_mask, int seq_num)
{
  // copy of accept model
  // Can only get here if index > 2, so will be something to save.
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::accept_pending(true, false, true) != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.accept",
			   "Problem accepting new model");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Model_insert_model::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  // selection (OMXint read req)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0) {
      CCP3::show_as_busy();
      char message[256];
      if (DLV::operation::insert_model(label, selection,
				       message, 256) == DLV_OK) {
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	fractional = 0;
	x = 0.0;
	y = 0.0;
	z = 0.0;
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.insert_model", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_insert_model::update(OMevent_mask event_mask, int seq_num)
{
  // data (move_params read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_inserted_model(((int)fractional) == 1,
					     (double)x, (double)y, (double)z,
					     message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.insert_model", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_insert_model::change(OMevent_mask event_mask, int seq_num)
{
  // fractional (OMXint read req notify)
  // x (OMXdouble write)
  // y (OMXdouble write)
  // z (OMXdouble write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::switch_atom_coords(((int)fractional) == 1, false,
					  xc, yc, zc, message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.insert_model", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_insert_model::generate(OMevent_mask event_mask,
					   int seq_num)
{
  // x (OMXdouble write)
  // y (OMXdouble write)
  // z (OMXdouble write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::update_from_selections(xc, yc, zc,
					      message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.insert_model", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_insert_model::position(OMevent_mask event_mask,
					   int seq_num)
{
  // x (OMXdouble write)
  // y (OMXdouble write)
  // z (OMXdouble write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::position_from_selections(xc, yc, zc,
						message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.insert_model", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_insert_model::edit(OMevent_mask event_mask, int seq_num)
{
  // x (OMXdouble write)
  // y (OMXdouble write)
  // z (OMXdouble write)
  // use_editor (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  bool value = ((bool)((int)use_editor == 1));
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::set_edit_transform(value, xc, yc, zc,
					  message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.insert_model", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_move_atom::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // nselect (OMXint read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0 and DLV::operation::continue_has_selections()) {
    if (strlen(label) > 0) {
      CCP3::show_as_busy();
      char message[256];
      double xc;
      double yc;
      double zc;
      if (DLV::operation::move_atoms(label, xc, yc, zc,
				     message, 256) == DLV_OK) {
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	fractional = 0;
	displacements = 0;
	x = xc;
	y = yc;
	z = zc;
	use_editor = 0;
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.move_atoms", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_move_atom::update(OMevent_mask event_mask, int seq_num)
{
  // fractional (OMXint read req)
  // x (OMXdouble read req notify)
  // y (OMXdouble read req notify)
  // z (OMXdouble read req notify)
  // data (move_params read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_move_atoms(((int)fractional) == 1,
					 ((int)displacements) == 1,
					 (double)x, (double)y, (double)z,
					 message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.move_atoms", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_move_atom::change(OMevent_mask event_mask, int seq_num)
{
  // fractional (OMXint read req notify)
  // x (OMXdouble write)
  // y (OMXdouble write)
  // z (OMXdouble write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::switch_atom_coords(((int)fractional) == 1,
					  ((int)displacements) == 1,
					  xc, yc, zc, message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.move_atoms", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_move_atom::generate(OMevent_mask event_mask, int seq_num)
{
  // x (OMXdouble write)
  // y (OMXdouble write)
  // z (OMXdouble write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::update_from_selections(xc, yc, zc,
					      message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.move_atoms", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_move_atom::position(OMevent_mask event_mask, int seq_num)
{
  // x (OMXdouble write)
  // y (OMXdouble write)
  // z (OMXdouble write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::position_from_selections(xc, yc, zc,
						message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.move_atoms", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_move_atom::edit(OMevent_mask event_mask, int seq_num)
{
  // x (OMXdouble write)
  // y (OMXdouble write)
  // z (OMXdouble write)
  // use_editor (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  bool value = ((bool)((int)use_editor == 1));
  double xc;
  double yc;
  double zc;
  ok = DLV::operation::set_edit_transform(value, xc, yc, zc,
					  message, 256);
  x = xc;
  y = yc;
  z = zc;
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.move_atoms", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_move_atom::accept(OMevent_mask event_mask, int seq_num)
{
  // copy of accept model
  // Can only get here if index > 2, so will be something to save.
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::accept_pending(true, false, true) != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.accept",
			   "Problem accepting new model");
  CCP3::show_as_idle();
  name = "";
  index = 0;
  return ok;
}

int CCP3_core_Model_list_models::start(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint read req notify)
  // labels (OMXstr_array write)
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    DLV::string *names = 0;
    int n = DLV::operation::list_models(names, (int)model_type);
    if (n == 0)
      OMset_array_size(labels.obj_id(), 0);
    else {
      OMset_array_size(labels.obj_id(), n);
      for (int i = 0; i < n; i++)
	OMset_str_array_val(labels.obj_id(), i, names[i].c_str());
      delete [] names;
    }
    CCP3::show_as_idle();
  }
  return OM_STAT_SUCCESS;
}

int CCP3_core_Model_add_cluster_region::update(OMevent_mask event_mask,
					       int seq_num)
{
  // radii (OMXfloat_array read req notify)
  // n (OMXint read req)
  // label (OMXstr read req)
  // avoid blank labels in radius initialisation at panel opening
  if ((char *)label == 0 or strlen((char *)label) == 0)
    return OM_STAT_SUCCESS;
  char message[256];
  DLVreturn_type ok = DLV_OK;
  float *radii_arr = (float *)radii.ret_array_ptr(OM_GET_ARRAY_RD);
  if (radii_arr == 0)
    return CCP3::return_info(DLV_ERROR, "CCP3.core.cluster_region.update",
			     "Problem getting radii");
  else {
    CCP3::show_as_busy();
    ok = DLV::operation::update_cluster_region(radii_arr, (int)n, (char *)label,
					       message, 256);
    CCP3::show_as_idle();
    if (ok != DLV_OK)
      return CCP3::return_info(ok, "CCP3.core.Model.cluster_region.update",
			       message);
    else
      return OM_STAT_SUCCESS;
  }
}

int CCP3_core_Model_add_cluster_region::accept(OMevent_mask event_mask,
					       int seq_num)
{
  // label (OMXstr read req)
  DLVreturn_type ok = DLV_OK;
  char message[256];
  ok = DLV::operation::accept_cluster_region(message, 256);
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.cluster_region.accept",
			     message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_add_cluster_region::cancel(OMevent_mask event_mask,
					       int seq_num)
{
  // label (OMXstr read req)
  DLVreturn_type ok = DLV_OK;
  char message[256];
  ok = DLV::operation::cancel_cluster_region((char *)label, message, 256);
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.cluster_region.cancel",
			     message);
  else
    return OM_STAT_SUCCESS;
}

static void read_cds(char *ptr)
{
  char message[256];
  static OMobj_id id = OMnull_obj;
  if (OMis_null_obj(id))
    id = OMfind_str_subobj(OMinst_obj,
			   "DLV.calculations.CDS.cdsUI"
			   ".cds_control.trigger", OM_OBJ_RW);
  //fprintf(stderr, "Read cds\n");
  //CCP3::show_as_busy();
  if (!DLV::cds_cif_socket::read(message, 256))
    (void) CCP3::return_info(DLV_ERROR, "CCP3.core.Model.CDS", message);
  //fprintf(stderr, "Done cds\n");
  //if (op == 0)
  //  (void) CCP3::return_info(DLV_ERROR, "CCP3.core.Model.CDS", message);
  //else 
  //  (void) CCP3::render_in_view(op, true);
  //CCP3::show_as_idle();
  OMset_int_val(id, 1);
  //return OM_STAT_SUCCESS;
}

int CCP3_core_Model_cds_control::init(OMevent_mask event_mask, int seq_num)
{
#ifdef WIN32
  static SOCKET socket = 0;
#else
  static int socket = 0;
#endif // WIn32
  // OMXint visible
  if ((int)visible == 1) {
    if ((socket = DLV::cds_cif_socket::start_listening()) != 0)
      EVadd_select(EV_SELECT0, socket, &(read_cds), 0, 0, EV_SEL_IN); 
    else
      return CCP3::return_info(DLV_ERROR, "CCP3.core.Model.CDS",
			       "Problem opening socket");
    //fprintf(stderr, "Startup\n");
  } else {
    if (socket != 0) {
      EVdel_select(EV_SELECT0, socket, &(read_cds), 0, 0, EV_SEL_IN);
      DLV::cds_cif_socket::stop_listening();
      socket = 0;
    }
    //fprintf(stderr, "Closedown\n");
  }
  return OM_STAT_SUCCESS;
}

int CCP3_core_Model_cds_control::read(OMevent_mask event_mask, int seq_num)
{
  char message[256];
  int ok;
  bool set_bonds = ((bool)(bond_all == 1));
  CCP3::show_as_busy();
  //fprintf(stderr, "Read cds\n");
  DLV::operation *op = DLV::cds_cif_socket::create(set_bonds, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.core.Model.CDS_read", message);
  else 
    ok = CCP3::render_in_view(op, ((bool)(new_view == 1)));
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Model_save::save(OMevent_mask event_mask, int seq_num)
{
  char *data_file = (char *) data.filename;
  char message[256];
  DLV::operation *op = 0;
  int ok;
  CCP3::show_as_busy();
  message[0] = '\0';
  switch ((int)data.filetype) {
  case 0:
    op = DLV::save_xyz_file::create(data_file, message, 256);
    break;
  case 1:
    op = DLV::save_cif_file::create(data_file, message, 256);
    break;
#ifdef USE_ESCDF
  case 2:
    op = DLV::save_escdf_file::create(data_file, message, 256);
    break;
#endif // USE_ESCDF
  }
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.save", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Model_wulff::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // index (OMXint write)
  // params (WulffParams write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0) {
      CCP3::show_as_busy();
      char message[256];
      double size = 10.0;
      if (DLV::operation::wulff_crystal(label, size, (int)selection,
					message, 256) == DLV_OK) {
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	params.max_dim = size;
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.wulff_crystal", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_wulff::scale(OMevent_mask event_mask, int seq_num)
{
  // params (WulffParams read req notify)
  CCP3::show_as_busy();
  char message[256];
  double r = (float)params.max_dim;
  DLVreturn_type ok = DLV::operation::wulff_update(r, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.wulff_crystal", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_multilayer::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // selection (OMXint read req)
  // index (OMXint write)
  // space (OMXfloat write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0) {
      CCP3::show_as_busy();
      char message[256];
      double size = 2.0;
      if (DLV::operation::multi_layer(label, size, (int)selection,
				      message, 256) == DLV_OK) {
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	space = size;
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.multi_layer", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}

int CCP3_core_Model_multilayer::position(OMevent_mask event_mask, int seq_num)
{
  // space (OMXfloat read req notify)
  CCP3::show_as_busy();
  char message[256];
  double r = (float)space;
  DLVreturn_type ok = DLV::operation::multilayer_update(r, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Model.multi_layer", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Model_fill_geometry::start(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req notify)
  // new_view (OMXint read req)
  // selection (OMXint read req)
  // index (OMXint write)
  char *label = (char *) name;
  int ok = OM_STAT_SUCCESS;
  if ((int)index == 0) {
    if (strlen(label) > 0) {
      CCP3::show_as_busy();
      char message[256];
      if (DLV::operation::fill_geometry(label, (int)selection,
					message, 256) == DLV_OK) {
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  ((bool)((int)new_view == 1)));
	index = 3;
      } else
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.core.Model.fill_geometry", message);
      CCP3::show_as_idle();
    }
  }
  return ok;
}
