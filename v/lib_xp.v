
"$XP_PATH<0>/v/lib_xp.v" Libraries {
  flibrary CCP3 <NEdisplayMode="opened"> {
    NElink Core_Modules <NEdisplayMode="open"> => Templates.CCP3.Core_Modules;
    NElink Core_Macros <NEdisplayMode="open"> => Templates.CCP3.Core_Macros;
    NElink Viewers <NEdisplayMode="open"> => Templates.CCP3.Viewers;
    NElink Renderers <NEdisplayMode="open"> => Templates.CCP3.Renderers;
    NElink CRYSTAL <NEdisplayMode="open"> => Templates.CCP3.CRYSTAL;
    NElink EXCURV <NEdisplayMode="open"> => Templates.CCP3.EXCURV;
    NElink GULP <NEdisplayMode="open"> => Templates.CCP3.GULP;
    NElink ONETEP <NEdisplayMode="open"> => Templates.CCP3.ONETEP;
    //NElink KKR <NEdisplayMode="open"> => Templates.CCP3.KKR;
    NElink K_P <NEdisplayMode="open"> => Templates.CCP3.K_P;
    NElink ROD <NEdisplayMode="open"> => Templates.CCP3.ROD;
    NElink CHEMSHELL <NEdisplayMode="open"> => Templates.CCP3.CHEMSHELL;
    NElink Calcs <NEdisplayMode="open"> => Templates.CCP3.Calcs;
    NElink CASTEP <NEdisplayMode="open"> => Templates.CCP3.CASTEP;
    NElink IMAGE <NEdisplayMode="open"> => Templates.CCP3.IMAGE;
#ifdef DLV_BABEL
    NElink Babel <NEdisplayMode="open"> => Templates.CCP3.Babel;
#endif // DLV_BABEL
    NElink Version2 <NEdisplayMode="open"> => Templates.CCP3.Version2;
  };
};
