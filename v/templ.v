//#define DLV_DL
//#define DLV_BGS
//#define DLV_RELEASE
//#define DLV_DEMO
#define DLV_TOOLTIP_OK
#define USE_LINUX
#ifdef MSDOS
#define DLV_TRANSFORM_EDIT_BUG
#define DLV_NO_DLLS
#undef USE_LINUX
#endif // MSDOS
#ifdef xp_darwin
#undef USE_LINUX
#endif
#define USE_ESCDF
"$XP_PATH<0>/v/templ.v" Templates {
  "$XP_PATH<1>/avs/v/CCP3/lib.v" CCP3;
  CONFIG {
    netcdf_kit_disabled = 1;
    ac_kit_disabled = 1;
    ip_kit_disabled = 1;
    cfd_kit_disabled = 1;
    gis_disabled = 1;
    w3c_disabled = 1;
    gd_xgl_disabled = 1;
    gd_pex_disabled = 1;
    gd_xil_disabled = 1;
    db_kit_disabled = 1;
    db_oracle_disabled = 1;
  };
};
